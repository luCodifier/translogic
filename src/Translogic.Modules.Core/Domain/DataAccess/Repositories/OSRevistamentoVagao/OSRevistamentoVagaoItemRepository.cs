﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao;
using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using NHibernate.Criterion;
using System.Text;
using NHibernate;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.OSRevistamentoVagao
{
    public class OSRevistamentoVagaoItemRepository : NHRepository<OSRevistamentoVagaoItem, int>, IOSRevistamentoVagaoItemRepository
    {
        public ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
            string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequenciaInicio, int sequenciaFim)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                                QRY.COD_VAGAO       AS ""NumeroVagao""
                                               ,QRY.ID_VAGAO        AS ""IdVagao""
                                               ,QRY.LOCAL           AS ""Local""
                                               ,QRY.SERIE           AS ""Serie""
                                               ,QRY.LINHA           AS ""Linha""
                                               ,QRY.SEQUENCIA       AS ""Sequencia""
                                               ,QRY.ID_LOTACAO      AS ""IdLotacao""
                                               ,QRY.DSC_LOTACAO     AS ""DescricaoLotacao""
                                               ,QRY.ID_SITUACAO     AS ""IdSituacao""
                                               ,QRY.DSC_SITUACAO    AS ""DescricaoSituacao""
                                               ,QRY.ID_COND_USO     AS ""IdCondicaoDeUso""
                                               ,QRY.DSC_COND_USO    AS ""DescricaoCondicaoDeUso""
                                               ,QRY.ID_LOCALIZACAO  AS ""IdLocalizacao""
                                               ,QRY.DSC_LOCALIZACAO AS ""DescricaoLocalizacao""
                                               ,QRY.TEMVAGAO AS ""Marcado""
       
                                          FROM ( SELECT 
                                                         EN.VG_COD_VAG AS COD_VAGAO,
                                                         VG.VG_ID_VG   AS ID_VAGAO,
                                                         AOM.AO_COD_AOP AS LOCAL,
                                                         SV.SV_COD_SER AS SERIE,
                                                         EV.EV_COD_ELV AS LINHA,
                                                         VPV.VP_NUM_SEQ AS SEQUENCIA,
                                                         EN.SH_IDT_STC AS ID_LOTACAO,
                                                         EN.SH_DSC_PT AS DSC_LOTACAO,
                                                         EN.SI_IDT_STC AS ID_SITUACAO,
                                                         EN.SI_DSC_PT AS DSC_SITUACAO,
                                                         EN.CD_IDT_CUS AS ID_COND_USO,
                                                         EN.CD_DSC_PT AS DSC_COND_USO,
                                                         EN.LO_IDT_LCL AS ID_LOCALIZACAO,
                                                         EN.LO_DSC_PT AS DSC_LOCALIZACAO, 
                                                         VMV.VM_ID_VM AS ID_VAGAO_MOTIVO,
                                                         M.MS_ID_MS AS ID_MOTIVO, 
                                                         M.MS_DSC_MOTIVO AS DSC_MOTIVO,
                                                         VPV.VP_TIMESTAMP AS DT_EMISSAO,
                                                         :temVagao
                                                    FROM VAGAO_PATIO_VIG   VPV,
                                                         ELEMENTO_VIA      EV,
                                                         AREA_OPERACIONAL  AOF,
                                                         AREA_OPERACIONAL  AOM,
                                                         ESTADO_VAGAO_NOVA EN,
                                                         VAGAO             VG,
                                                         SERIE_VAGAO       SV,
                                                         VAGAO_MOTIVO_VIG  VMV,
                                                         MOTIVOS           M 
          
                                                   WHERE VPV.AO_ID_AO = AOF.AO_ID_AO
                                                     AND AOF.AO_ID_AO_INF = AOM.AO_ID_AO                                                     
                                                     AND VPV.VG_ID_VG = EN.VG_ID_VG
                                                     AND VPV.EV_ID_ELV = EV.EV_ID_ELV
                                                     AND VPV.VG_ID_VG = VG.VG_ID_VG
                                                     AND VG.SV_ID_SV = SV.SV_ID_SV 
                                                     AND VMV.VG_ID_VG (+)= EN.VG_ID_VG
                                                     AND VMV.MS_ID_MS = M.MS_ID_MS (+)
                                                     AND EN.SI_IDT_STC  IN (10, 16, 18, 20, 21, 22, 24, 28, 30) 
                                                     :where1
                                                     

                                                  UNION ALL
                                                  SELECT EN.VG_COD_VAG AS COD_VAGAO,
                                                         VG.VG_ID_VG   AS ID_VAGAO,
                                                         SUBSTR(AOM.AO_COD_AOP, 1, 3) || '-' || TR.TR_PFX_TRM || '-' ||
                                                         TOS.X1_NRO_OS AS LOCAL,
                                                         SV.SV_COD_SER AS SERIE,
                                                         NULL AS LINHA,
                                                         CVV.CV_SEQ_CMP AS SEQUENCIA,
                                                         EN.SH_IDT_STC AS ID_LOTACAO,
                                                         EN.SH_DSC_PT AS DSC_LOTACAO,
                                                         EN.SI_IDT_STC AS ID_SITUACAO,
                                                         EN.SI_DSC_PT AS DSC_SITUACAO,
                                                         EN.CD_IDT_CUS AS ID_COND_USO,
                                                         EN.CD_DSC_PT AS DSC_COND_USO,
                                                         EN.LO_IDT_LCL AS ID_LOCALIZACAO,
                                                         EN.LO_DSC_PT AS DSC_LOCALIZACAO,
                                                         VMV.VM_ID_VM AS ID_VAGAO_MOTIVO,
                                                         M.MS_ID_MS AS ID_MOTIVO, 
                                                         M.MS_DSC_MOTIVO AS DSC_MOTIVO,
                                                         CVV.CV_TIMESTAMP AS DT_EMISSAO,
                                                         :temVagao       
                                                    FROM COMPVAGAO_VIG     CVV,
                                                         COMPOSICAO        CP,
                                                         TREM              TR,
                                                         AREA_OPERACIONAL  AOM,
                                                         ESTADO_VAGAO_NOVA EN,
                                                         VAGAO             VG,
                                                         SERIE_VAGAO       SV,
                                                         T2_OS             TOS,
                                                         VAGAO_MOTIVO_VIG  VMV,
                                                         ELEMENTO_VIA      EV,
                                                         MOTIVOS           M
                                                   WHERE TR.AO_ID_AO_PVT = AOM.AO_ID_AO                                                     
                                                     AND TR.TR_STT_TRM = 'R'
                                                     AND TR.OF_ID_OSV = TOS.X1_ID_OS
                                                     AND CP.TR_ID_TRM = TR.TR_ID_TRM
                                                     AND CP.CP_ID_CPS = CVV.CP_ID_CPS
                                                     AND CVV.VG_ID_VG = EN.VG_ID_VG
                                                     AND EN.VG_ID_VG = VG.VG_ID_VG
                                                     AND VG.SV_ID_SV = SV.SV_ID_SV
                                                     AND VMV.VG_ID_VG (+)= EN.VG_ID_VG
                                                     AND VMV.MS_ID_MS = M.MS_ID_MS (+)
                                                     :where2
                                                     AND EN.SI_IDT_STC  IN (10, 16, 18, 20, 21, 22, 24, 28, 30) 
                                                     ) QRY ");

            #endregion

            #region Where
            var sWhere1 = new StringBuilder("");
            var sWhere2 = new StringBuilder("");

            if (!String.IsNullOrWhiteSpace(listaDeVagoes))
            {
                String[] result = listaDeVagoes.Split(';');

                string vagoes = string.Empty;

                foreach (var item in result)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        vagoes += "'" + item.Trim() + "',";
                    }
                }

                sWhere1.Append(" AND EN.VG_COD_VAG IN ( " + vagoes.Substring(0, vagoes.Length - 1) + ")");
                sWhere2.Append(sWhere1);
            }

            if (!String.IsNullOrWhiteSpace(serie))
            {
                sWhere1.AppendFormat(" AND SV.SV_COD_SER like '{0}'", serie.ToUpper());
                sWhere2.Append(sWhere1);
            }

            if (!String.IsNullOrWhiteSpace(local) && local != "Todos")
            {
                sWhere1.AppendFormat(" AND AOM.AO_COD_AOP like '{0}'", local.ToUpper());
                sWhere2.Append(sWhere1);
            }

            if (!String.IsNullOrWhiteSpace(linha))
            {
                sWhere1.AppendFormat(" AND EV.EV_COD_ELV like '{0}'", linha.ToUpper());
                sWhere2.Append(sWhere1);
            }


            if (!String.IsNullOrWhiteSpace(idLotacao) && idLotacao.Trim() != "0")
            {
                sWhere1.Append(" AND EN.SH_IDT_STC = " + idLotacao);
                sWhere2.Append(sWhere1);
            }


            if (sequenciaInicio > 0)
            {
                sWhere1.Append(" AND VPV.VP_NUM_SEQ >= " + sequenciaInicio);
                sWhere2.Append(" AND CVV.CV_SEQ_CMP >= " + sequenciaInicio);
            }

            if (sequenciaFim > 0)
            {
                sWhere1.Append(" AND VPV.VP_NUM_SEQ <= " + sequenciaFim);
                sWhere2.Append(" AND CVV.CV_SEQ_CMP <= " + sequenciaFim);
            }

            if (numOs.HasValue)
            {
                if (numOs.Value > 0)
                {
                    sql = sql.Replace(":temVagao",
                        "(SELECT CASE  WHEN count(*) > 0 THEN 1  ELSE 0  END  AS TEMVAGAO FROM TB_OS_REVISTAMENTO_VAGAO_ITENS RVI WHERE RVI.ID_OS = " + numOs + "  AND RVI.ID_VAGAO = VG.VG_ID_VG) AS TEMVAGAO");
                }
                else
                {
                    sql = sql.Replace(":temVagao", "0 AS TEMVAGAO");
                }
            }
            else
            {
                sql = sql.Replace(":temVagao", "0 AS TEMVAGAO");
            }

            sql = sql.Replace(":where1", sWhere1.ToString()).Replace(":where2", sWhere2.ToString());

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            sql.Append(" ORDER BY ");
            sql.Append("          QRY.LOCAL, ");
            sql.Append("          QRY.LINHA, ");
            sql.Append("          QRY.SEQUENCIA ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OSRevistamentoVagaoItemDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<OSRevistamentoVagaoItemDto>();

                var result = new ResultadoPaginado<OSRevistamentoVagaoItemDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterVagoesDaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OSRevistamentoVagao.Id", idOs));

            var lista = ObterTodos(criteria).Select(o => new OSRevistamentoVagaoItemDto
            {
                IdItem = o.Id,
                IdOs = o.OSRevistamentoVagao.Id,
                Serie = o.Vagao.Serie.Codigo,
                NumeroVagao = o.Vagao.Codigo,
                Concluido = Convert.ToBoolean(o.Concluido.HasValue ? o.Concluido : null),
                ConcluidoDescricao = (o.Concluido.HasValue ? (o.Concluido.Value == 1 ? "Sim" : "Não") : null),
                Retirar = Convert.ToBoolean(o.Retirar.HasValue ? o.Retirar : null),
                RetirarDescricao = (o.Retirar.HasValue ? (o.Retirar.Value == 1 ? "Sim" : "Não") : null),
                Comproblema = Convert.ToBoolean(o.Comproblema.HasValue ? o.Comproblema : null),
                ComproblemaDescricao = (o.Comproblema.HasValue ? (o.Comproblema.Value == 1 ? "Sim" : "Não") : null),
                Observacao = o.Observacao
            }).ToList();

            var result = new ResultadoPaginado<OSRevistamentoVagaoItemDto>
            {
                Items = lista,
                Total = (long)lista.Count
            };

            return result;
        }

        public IList<OSVagaoItemLotacaoDto> ObterLotacoes()
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                             QRY.ID_CARGA  AS ""IdLotacao""
                                            ,QRY.DSC_CARGA AS ""DescricaoLotacao""
                                        FROM (SELECT 
                                            CG.SH_IDT_STC AS ID_CARGA, 
                                            CG.SH_DSC_PT AS DSC_CARGA
                                          FROM CARGA CG
                                        UNION
                                          SELECT
                                               0       AS ID_CARGA,
                                               'Todas' AS DSC_CARGA
                                          FROM DUAL
                                          ORDER BY ID_CARGA) QRY ");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OSVagaoItemLotacaoDto>());

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<OSVagaoItemLotacaoDto>();

                var result = itens;

                return result;
            }
        }

        public void Inserir(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens)
        {
            foreach (var item in osRevistamentoVagaoItens)
            {
                this.Inserir(item);
            }
        }

        public void Atualizar(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens)
        {
            foreach (var item in osRevistamentoVagaoItens)
            {
                this.Atualizar(item);
            }
        }

        public void Remover(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens)
        {
            foreach (var item in osRevistamentoVagaoItens)
            {
                this.Remover(item);
            }
        }

        public IList<OSRevistamentoVagaoItem> ObterVagoesDaOs(int idOs)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OSRevistamentoVagao.Id", idOs));

            var lista = ObterTodos(criteria).ToList();

            return lista;
        }
    }
}