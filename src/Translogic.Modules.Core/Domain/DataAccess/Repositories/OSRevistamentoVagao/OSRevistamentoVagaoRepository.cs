﻿

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.OSRevistamentoVagao
{
    using System;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using System.Text;
    using System.Collections.Generic;
    using NHibernate.Transform;
    
    public class OSRevistamentoVagaoRepository : NHRepository<OSRevistamentoVagao, int>, IOSRevistamentoVagaoRepository
    {
        public ResultadoPaginado<OSRevistamentoVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@" SELECT 
                    OS.ID_OS as ""IdOs"",
                    OS.ID_OS_PARCIAL as ""IdOsParcial"", 
                    OS.DT_HR as ""Data"",
                    OS.DT_HR_INICIO as ""HoraInicio"",
                    OS.DT_HR_TERMINO as ""HoraTermino"",
                    OS.DT_HR_ENTREGA as ""HoraEntrega"",
                    OS.LINHA as ""Linha"",
                    OS.CONVOCACAO as ""Convocacao"",
                    OS.QTDE_VAGOES as ""QtdeVagoes"",
                    OS.QTDE_VAGOES_VEDADOS as ""QtdeVagoesVedados"",
                    OS.QTDE_VAGOES_GAMBITADOS as ""QtdeVagoesGambitados"",
                    OS.QTDE_ENTRE_VAZIOS as ""QtdeVagoesVazios"",
                    OS.ID_FORNECEDOR_OS as ""IdFornecedor"",
                    fo.NOME_FORNECEDOR AS ""Fornecedor"",
                    OS.ID_LOCAL as ""idLocal"",
                    ao.AO_COD_AOP AS ""LocalServico"",
                    OS.ID_TIPO_SERVICO as ""IdTipo"",
                    tpS.DESC_SERVICO AS ""Tipo"",
                    OS.ID_STATUS as ""IdStatus"",
                    st.DESC_STATUS AS ""Status"",
                    OS.NOME_MANUTENCAO as ""NomeManutencao"",
                    OS.MATRICULA_MANUTENCAO as ""MatriculaManutencao"",
                    OS.NOME_ESTACAO as ""NomeEstacao"",
                    OS.MATRICULA_ESTACAO as ""MatriculaEstacao"",
                    OS.JUST_CANCE as ""JustificativaCancelamento"",
                    OS.DT_ULT_ALT as ""UltimaAlteracao"",
                    CASE
                        WHEN (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=os.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.RETIRAR=1)>0 THEN 'S'
                        ELSE 'N'
                    END AS ""VagaoRetirado"",
                    CASE
                        WHEN (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=os.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.COMPROBLEMA=1)>0 THEN 'S'
                        ELSE 'N'
                    END AS ""ProblemaVedacao"",
                    CASE 
                        WHEN OS.QTDE_ENTRE_VAZIOS > 0 THEN 'S'
                        ELSE 'N'
                    END AS ""CarregadoEntreVazios""
                    ,os.USUARIO AS ""Usuario""
                    FROM TB_OS_REVISTAMENTO_VAGAO OS
                    INNER JOIN TB_OS_LIMPEZA_VAGAO_STATUS st ON OS.ID_STATUS=st.ID_STATUS
                    LEFT JOIN AREA_OPERACIONAL ao ON OS.ID_LOCAL=ao.AO_ID_AO
                    LEFT JOIN TB_TIPOSERVICO tpS ON OS.ID_TIPO_SERVICO=tpS.ID_TIPO_SERVICO
                    LEFT JOIN TB_FORNECEDOR_OS fo ON OS.ID_FORNECEDOR_OS=fo.ID_FORNECEDOR_OS ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND os.DT_HR <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND ao.AO_COD_AOP = '{0}'", local.ToUpper());
            }

            if (!string.IsNullOrEmpty(numOs))
            {
                sqlWhere.AppendFormat(" AND os.ID_OS LIKE '%{0}%'", numOs.ToUpper());
            }

            if (!string.IsNullOrEmpty(idTipo))
            {
                sqlWhere.AppendFormat(" AND os.ID_TIPO_SERVICO = {0}", idTipo);
            }

            if (!string.IsNullOrEmpty(idStatus))
            {
                sqlWhere.AppendFormat(" AND os.ID_STATUS in ({0}) ", idStatus);
            }

            if (vagaoRetirado)
            {
                sqlWhere.Append(" AND (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=os.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.RETIRAR=1) > 0 ");
            }

            if (problemaVedacao)
            {
                sqlWhere.Append(" AND (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=os.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.COMPROBLEMA=1) > 0 ");
            }

            if (vagaoCarregado)
            {
                sqlWhere.Append(" AND QTDE_ENTRE_VAZIOS > 0 ");
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            sql.Append(" ORDER BY OS.ID_OS DESC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<OSRevistamentoVagaoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<OSRevistamentoVagaoDto>();

                var result = new ResultadoPaginado<OSRevistamentoVagaoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }
        public IList<OSRevistamentoVagaoExportaDto> ObterConsultaOsParaExportar(DateTime? dataInicial, DateTime? dataFinal,
               string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@" SELECT 
                    OS.ID_OS as ""IdOs"",
                    OS.ID_OS_PARCIAL as ""IdOsParcial"", 
                    OS.DT_HR as ""Data"",
                    OS.DT_HR_INICIO as ""HoraInicio"",
                    OS.DT_HR_TERMINO as ""HoraTermino"",
                    CASE WHEN OS.QTDE_VAGOES_VEDADOS IS NOT NULL THEN OS.QTDE_VAGOES_VEDADOS ELSE 0 END as ""QtdeVagoesVedados"",
                    CASE WHEN OS.QTDE_VAGOES_GAMBITADOS IS NOT NULL THEN OS.QTDE_VAGOES_GAMBITADOS ELSE 0 END as ""QtdeVagoesGambitados"",
                    FO.NOME_FORNECEDOR AS ""Fornecedor"",
                    AO.AO_COD_AOP AS ""Local"",
                    ST.DESC_STATUS AS ""Status"",
                    OS.NOME_MANUTENCAO as ""NomeManutencao"",
                    OS.DT_ULT_ALT as ""UltimaAlteracao"",
                    IT.RETIRAR as ""Retirar"",
                    IT.COMPROBLEMA as ""ProblemaVedacao"",
                    IT.OBSERVACAO as ""Observacao"",
                    OS.USUARIO AS ""Usuario"",
                    VA.VG_COD_VAG AS ""NumeroVagao""
                    FROM TB_OS_REVISTAMENTO_VAGAO OS
                    LEFT JOIN TB_OS_REVISTAMENTO_VAGAO_ITENS IT ON OS.ID_OS=IT.ID_OS
                    LEFT JOIN VAGAO VA ON IT.ID_VAGAO=VA.VG_ID_VG
                    LEFT JOIN VAGAO_PATIO_VIG VPV ON VPV.VG_ID_VG = VA.VG_ID_VG
                    INNER JOIN TB_OS_LIMPEZA_VAGAO_STATUS ST ON OS.ID_STATUS=ST.ID_STATUS
                    LEFT JOIN AREA_OPERACIONAL AO ON OS.ID_LOCAL=AO.AO_ID_AO
                    LEFT JOIN TB_TIPOSERVICO TPS ON OS.ID_TIPO_SERVICO=TPS.ID_TIPO_SERVICO
                    LEFT JOIN TB_FORNECEDOR_OS FO ON OS.ID_FORNECEDOR_OS=FO.ID_FORNECEDOR_OS
                    LEFT JOIN ELEMENTO_VIA EV ON VPV.EV_ID_ELV = EV.EV_ID_ELV ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND OS.DT_HR BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND OS.DT_HR >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND OS.DT_HR <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND AO.AO_COD_AOP = '{0}'", local.ToUpper());
            }

            if (!string.IsNullOrEmpty(numOs))
            {
                sqlWhere.AppendFormat(" AND OS.ID_OS LIKE '%{0}%'", numOs.ToUpper());
            }

            if (!string.IsNullOrEmpty(idTipo))
            {
                sqlWhere.AppendFormat(" AND OS.ID_TIPO_SERVICO = {0}", idTipo);
            }

            if (!string.IsNullOrEmpty(idStatus))
            {
                sqlWhere.AppendFormat(" AND OS.ID_STATUS in ({0}) ", idStatus);
            }

            if (vagaoRetirado)
            {
                sqlWhere.Append(" AND (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=OS.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.RETIRAR=1) > 0 ");
            }

            if (problemaVedacao)
            {
                sqlWhere.Append(" AND (SELECT COUNT(1) FROM TB_OS_REVISTAMENTO_VAGAO_ITENS WHERE TB_OS_REVISTAMENTO_VAGAO_ITENS.ID_OS=OS.ID_OS AND TB_OS_REVISTAMENTO_VAGAO_ITENS.COMPROBLEMA=1) > 0 ");
            }

            if (vagaoCarregado)
            {
                sqlWhere.Append(" AND QTDE_ENTRE_VAZIOS > 0 ");
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            sql.Append(@"  ORDER BY os.ID_OS DESC 
                     ,ao.AO_COD_AOP
                     , EV.EV_COD_ELV
                     ,  VPV.VP_NUM_SEQ ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OSRevistamentoVagaoExportaDto>());
                return query.List<OSRevistamentoVagaoExportaDto>();
            }
        }
    }
}