﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using NHibernate;
using System.Text;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
    public class MalhaSeguroRepository : NHRepository<MalhaSeguro, int>, IMalhaSeguroRepository
    {
        public IList<MalhaSeguroDto> ObterMalha()
        {
            #region SQL Query
            var paramaters = new List<Action<IQuery>>();
            var sql = new StringBuilder();

            sql.AppendFormat(@"SELECT M.ML_IDT_ML ""IdMalha""
                                     ,M.ML_CD_ML ""Sigla""
                                     ,ROWNUM ||'-'|| M.ML_DSC_ML ""DescricaoMalha""
                               FROM MALHA M ");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<MalhaSeguroDto>());

                var result = query.List<MalhaSeguroDto>();

                return result;
            }
        }

        
    }
}