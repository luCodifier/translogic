﻿using System.Collections.Generic;
using System.Linq;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Linq;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using System;
using System.Text;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
    /// <summary>
    ///   Implementação de repositorio de causa seguro
    /// </summary>
    public class CausaSeguroRepository : NHRepository<CausaSeguro, int>, ICausaSeguroRepository
    {
        /// <summary>
        ///   Obtem a lista enumerada de causas
        /// </summary>
        /// <returns>Lista enumerada de Causas</returns>
        public IEnumerable<CausaSeguro> ObterCausas()
        {
            using (ISession session = OpenSession())
            {
                return session.Query<CausaSeguro>()
                    .Where(cs => cs.Situacao == "S")
                    .ToList();
            }
        }

        public IList<CausaSeguroDto> ObterCausasTipoCausaV()
        {
            #region SQL Query
            var paramaters = new List<Action<IQuery>>();
            var sql = new StringBuilder();

            sql.AppendFormat(@"SELECT C.CS_ID_CS AS ""IdCausa""
                                     ,ROWNUM ||'-'|| C.CS_DSC_CAUSA as ""Descricao""
                               FROM CAUSA_SEGURO C 
                               WHERE C.CS_IND_SIT = 'S'
                               AND C.CS_TPO_CAUSA = 'V' ");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<CausaSeguroDto>());

                var result = query.List<CausaSeguroDto>();

                return result;
            }
        }
    }
}