﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de processo seguro
	/// </summary>
	public class ProcessoSeguroRepository : NHRepository<ProcessoSeguro, int>, IProcessoSeguroRepository
	{
	}
}