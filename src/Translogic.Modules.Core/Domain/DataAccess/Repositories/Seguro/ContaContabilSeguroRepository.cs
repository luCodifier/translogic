﻿using System.Collections.Generic;
using System.Linq;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Linq;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
    /// <summary>
    ///     Implementação de repositorio de conta contábil seguro
    /// </summary>
    public class ContaContabilSeguroRepository : NHRepository<ContaContabilSeguro, int>, IContaContabilSeguroRepository
    {
        /// <summary>
        ///     Obtem a lista enumerada de contas contabeis
        /// </summary>
        /// <returns>Lista enumerada de Contas Contábeis</returns>
        public IEnumerable<ContaContabilSeguro> ObterContasContabeis()
        {
            using (ISession session = OpenSession())
            {
                return session.Query<ContaContabilSeguro>()
                    .Where(cc => cc.IndicadorSituacao == "S")
                    .ToList();
            }
        }
    }
}