﻿using System.Collections.Generic;
using System.Linq;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Linq;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;
using System.Text;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de unidade seguro
	/// </summary>
	public class UnidadeSeguroRepository : NHRepository<UnidadeSeguro, int>, IUnidadeSeguroRepository
	{
		/// <summary>
		///   Obtem a lista enumerada de terminais
		/// </summary>
		/// <returns>Lista enumerada de Terminais</returns>
		public IEnumerable<UnidadeSeguro> ObterTerminais()
		{
			using (ISession session = OpenSession())
			{
				return session.Query<UnidadeSeguro>()
					.Where(u => u.Tipo == "T")
                    .OrderBy(c=>c.Descricao)
                    .ToList();
			}
		}

		/// <summary>
		///   Obtem a lista enumerada de unidades
		/// </summary>
		/// <returns>Lista enumerada de unidades</returns>
		public IEnumerable<UnidadeSeguro> ObterUnidades()
		{
			using (ISession session = OpenSession())
			{
				return session.Query<UnidadeSeguro>()
					.Where(u => u.Tipo == "P" || u.Tipo == "N")
                    .OrderBy(c=>c.Descricao)
					.ToList();
			}
		}

        /// <summary>
        ///   Obtem a lista enumerada de unidades do tipo N
        /// </summary>
        /// <returns>Lista enumerada de unidades do tipo N</returns>
        public IEnumerable<UnidadeSeguro> ObterUnidadesTipoN()
        {
            using (ISession session = OpenSession())
            {
                return session.Query<UnidadeSeguro>()
                    .Where(u => u.Tipo == "N")
                    .OrderBy(c => c.Descricao)
                    .ToList();
            }
        }

        /// <summary>
        ///   Obtem a lista enumerada de unidades do tipo N
        /// </summary>
        /// <returns>Lista enumerada de unidades do tipo N</returns>
        public IEnumerable<UnidadeSeguro> ObterUP()
        {
            using (ISession session = OpenSession())
            {
                return session.Query<UnidadeSeguro>()
                    .Where(u => u.Tipo == "P")
                    .OrderBy(c => c.Descricao)
                    .ToList();
            }
        }

        /// <summary>
        ///  Obtem a lista enumerada de unidades de acordo com o ID do Terminal
        /// </summary>
        /// <returns>Lista enumerada de Unidades</returns>
        public UnidadeSeguro ObterUnidadeSeguroPorIdTerminal(int idTerminal)
        {
            #region SQL Query

            var sql = new StringBuilder();

            sql.AppendFormat(@"SELECT US.UD_ID_UD AS ""IdUnidadeSeguro""
                                FROM   AREA_OPERACIONAL AO,
                                       UNIDADE_SEGURO US
                                WHERE  
                                        US.UP_ID_UNP = AO.UP_ID_UNP
                                AND     AO.EP_ID_EMP_OPR=4
                                AND     AO.AO_ID_AO_INF IS NOT NULL
                                AND     AO.AO_ID_AO = {0}
                                UNION ALL 
                                SELECT US.UD_ID_UD AS ""IdUnidadeSeguro""
                                FROM   AREA_OPERACIONAL AO,
                                       UNIDADE_SEGURO US
                                WHERE  
                                        US.AO_ID_AO = AO.AO_ID_AO
                                        AND AO.AO_ID_AO = {0}", idTerminal);

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.AddScalar("IdUnidadeSeguro", NHibernateUtil.Int32);
                
                //TODO: retornando ,mais de um registro, ver com o Marcell a regra
                query.SetMaxResults(1);

                int? idUnidadeSeguro = query.UniqueResult<int?>();
                if (idUnidadeSeguro.HasValue)
                {
                    return ObterPorId(idUnidadeSeguro.Value);                    
                }
            }

            return null;
        }
    }
}