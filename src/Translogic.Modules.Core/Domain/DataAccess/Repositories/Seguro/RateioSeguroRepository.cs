﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de rateio seguro
	/// </summary>
	public class RateioSeguroRepository : NHRepository<RateioSeguro, int>, IRateioSeguroRepository
	{
        /// <summary>
        /// Retorna o Rateio de Seguro para o processo informado
        /// </summary>
        /// <param name="processoSeguro">Processo Seguro</param>
        /// <returns>RateioSeguro</returns>
        public RateioSeguro ObterPorProcesso(ProcessoSeguro processoSeguro)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("ProcessoSeguro", processoSeguro));

            return ObterPrimeiro(criteria);
        }
	}
}