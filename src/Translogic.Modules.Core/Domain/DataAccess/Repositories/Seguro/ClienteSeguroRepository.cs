﻿using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
    /// <summary>
    ///     Implementação de repositorio de cliente seguro
    /// </summary>
    public class ClienteSeguroRepository : NHRepository<ClienteSeguro, int>, IClienteSeguroRepository
    {
        /// <summary>
        ///     Obtem o cliente seguro pela sigla
        /// </summary>
        /// <returns>Cliente seguro</returns>
        public ClienteSeguro ObterClientePorSigla(string sigla)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.InsensitiveLike("SiglaEmpresa", sigla, MatchMode.Anywhere));

            return ObterPrimeiro(criteria);
        }
    }
}