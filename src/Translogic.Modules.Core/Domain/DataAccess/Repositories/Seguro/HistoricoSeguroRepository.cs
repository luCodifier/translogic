﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de analise seguro
	/// </summary>
	public class HistoricoSeguroRepository : NHRepository<HistoricoSeguro, int>, IHistoricoSeguroRepository
	{
	}
}