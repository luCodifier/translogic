﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;
using System;
using NHibernate;
using System.Text;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
    /// <summary>
    ///     Implementação de repositorio de modal seguro
    /// </summary>
    public class ModalSeguroRepository : NHRepository<ModalSeguro, int>, IModalSeguroRepository
    {
        /// <summary>
        ///     Retorna lista de registros para o despacho e série informado
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <returns>lista de modais seguro</returns>
        public IList<ModalSeguro> ObterPorDespachoSerie(int despacho, string serie)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Despacho", despacho));
            criteria.Add(Restrictions.Eq("Serie", serie));

            return ObterTodos(criteria);
        }

        /// <summary>
        ///     Retorna lista de registros para o despacho e série informado
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <param name="numeroVagao">Número do Vagão</param>
        /// <returns>lista de modais seguro</returns>
        public IList<ModalSeguro> ObterPorDespachoSerieVagao(int despacho, string serie, string numeroVagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Despacho", despacho));
            criteria.Add(Restrictions.Eq("Serie", serie));
            criteria.CreateAlias("Vagao", "vg");
            criteria.Add(Restrictions.Eq("vg.Codigo", numeroVagao.PadLeft(7, '0')));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Verifica se existe numTermo na tabela
        /// </summary>
        /// <param name="numTermo"></param>
        /// <returns> Retorna TRUE se numTermo for encontrado na tabela, se não encontrar retorna FALSE</returns>
        public bool VerificaNumTermoExiste(string numTermo)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT DISTINCT(MS.MO_NUM_FALTA) 
                            FROM MODAL_SEGURO MS 
                            WHERE MS.MO_NUM_FALTA = '{0}'", numTermo);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                var result = query.UniqueResult<string>();

                //Verifica se SELECT retornou valor
                return !string.IsNullOrEmpty(result);
             }

            #endregion
        }


        /// <summary>
        /// Verifica se existe modal de seguro para o Número de Despacho e Série informado
        /// </summary>
        /// <param name="numeroDespacho">Número do Despacho</param>
        /// <param name="Serie">Número de Série</param>
        /// <param name="numeroVagao">Número do Vagão</param>
        /// <returns> Retorna TRUE se o modal seguro foi encontrado na tabela, se não encontrar retorna FALSE</returns>
        public bool VerificaDespachoSerieVagaoExiste(int numeroDespacho, string serie, string numeroVagao)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT COUNT(*) AS ""Existe"" FROM MODAL_SEGURO MO
                                    INNER JOIN PROCESSO_SEGURO PC ON (PC.PC_ID_PC = MO.PC_ID_PC)
                                    INNER JOIN DESPACHO DP ON (MO.NRO_DESP = DP.DP_NUM_DSP)
                                    INNER JOIN SERIE_DESPACHO SD ON (DP.SK_IDT_SRD = SD.SK_IDT_SRD)
                                    INNER JOIN VAGAO VG ON (VG.VG_ID_VG = MO.VG_ID_VG)
                                WHERE DP.DP_NUM_DSP = {0}
                                        AND SK_NUM_SRD = '{1}'
                                        AND VG.VG_COD_VAG = LPAD('{2}', 7, '0')", numeroDespacho, serie, numeroVagao);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                var result = query.UniqueResult<decimal>();

                //Verifica se SELECT retornou valor
                return result > 0;
            }

            #endregion
        }

        /// <summary>
        /// Retorna o Modal de Seguro para o processo informado
        /// </summary>
        /// <param name="processoSeguro">Processo Seguro</param>
        /// <returns>ModalSeguro</returns>
        public ModalSeguro ObterPorProcesso(ProcessoSeguro processoSeguro)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("ProcessoSeguro", processoSeguro));           

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Atualiza o Valor da Mercadoria do Modal de Seguro
        /// </summary>
        /// <param name="modalSeguro">objeto modal seguro à ser atualizado</param>
        public void AtualizarValorMercadoria(ModalSeguro modalSeguro)
        {
            if (modalSeguro == null)
                throw new ArgumentNullException("modalSeguro", "Parâmetro obrigatóro para a operação AtualizarValorMercadoria()");

            string hql = @"UPDATE ModalSeguro ms SET ms.ValorMercadoria = :valorMercadoria WHERE ms.Id = :idModal";
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idModal", modalSeguro.Id);
                query.SetDouble("valorMercadoria", modalSeguro.ValorMercadoria.GetValueOrDefault());
                query.ExecuteUpdate();
            }
        }
    }
}