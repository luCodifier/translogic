﻿using System;
using System.Collections.Generic;
using System.Text;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de manga
	/// </summary>
	public class MangaRepository : NHRepository<Manga, int>, IMangaRepository
	{

        public string ObterPesoMangaPorVagao(string vagao)
        {
            try
            {
                #region SQL Query

                var parameters = new List<Action<IQuery>>();
                var sql = new StringBuilder(@"SELECT LM_LIMITE_PESO  FROM LIMITE_PESO_MANGA WHERE LM_MANGA = (SELECT SUBSTR(SV.SV_COD_SER,LENGTH(SV.SV_COD_SER),1) COD_MANGA
                                            FROM VAGAO VG
                                            INNER JOIN SERIE_VAGAO SV ON (SV.SV_ID_SV = VG.SV_ID_SV)
                                            WHERE VG.VG_COD_VAG = '" + vagao + "')");
                #endregion

                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql.ToString());

                    var result = query.UniqueResult();

                    if (Convert.ToDouble(result) != null) 
                    { 
                        return Convert.ToString(result); 
                    } else { 
                        return Convert.ToString(0); 
                    } 

                   
                }
            }
            catch (Exception e)
            {
                
                throw;
            }
            
        }
	}
}