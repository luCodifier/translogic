﻿using System;
using System.Collections.Generic;
using System.Text;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro
{
	/// <summary>
	///   Implementação de repositorio de processo seguro cache
	/// </summary>
	public class ProcessoSeguroCacheRepository : NHRepository<ProcessoSeguroCache, int>, IProcessoSeguroCacheRepository
	{
		/// <summary>
		///   Retorna todos os processos em cache do usuário
		/// </summary>
		/// <param name="pagination">Detalhes da Paginação</param>
		/// <param name="usuario"> Usuário logado</param>
		/// <returns>lista de processos em cache do usuário</returns>
		public ResultadoPaginado<ProcessoSeguroCacheDto> ObterProcessosSeguroCacheUsuario(DetalhesPaginacao pagination,
			Usuario usuario)
		{
			var sb = new StringBuilder();

			sb.Append(@"  SELECT 
								{0}
							FROM ProcessoSeguroCache p
						WHERE 					
							p.Usuario = :usuario");

			ISession session = OpenSession();

			string stringHqlResult = string.Format(sb.ToString(),
                @" p.Id as ProcessoSeguroCacheId,
													p.Usuario as Usuario,
													p.Despacho as Despacho,
													p.Serie as Serie,
													p.DataSinistro as DataSinistro,
													p.Laudo as Laudo,	
                                                    p.ValorMercadoria as ValorMercadoria,
													p.Terminal as Terminal,	
													p.Perda as Perda,
												    p.Causa as Causa,							
													p.Gambit as Gambit,
													p.Lacre as Lacre,
													p.Vistoriador as Vistoriador,
													p.Avaliacao as Avaliacao,
													p.Unidade as Unidade,
													p.Rateio as Rateio,
													p.Historico as Historico,
													p.Nota as Nota,
													p.Cliente as Cliente,
													p.Produto as Produto,
													p.Peso as Peso,
													p.Tipo as Tipo,
													p.CodVagao as Vagao,
													p.OrigemDesc as Origem,
                                                    p.Piscofins as Piscofins,
                                                    p.ContaContabil as ContaContabil,
                                                    p.Sindicancia as Sindicancia");

			string stringHqlCount = string.Format(sb.ToString(), " COUNT(p.Id) ");

			IQuery query = session.CreateQuery(stringHqlResult);
			IQuery queryCount = session.CreateQuery(stringHqlCount);

			if (usuario != null)
			{
				query.SetParameter("usuario", usuario);
				queryCount.SetParameter("usuario", usuario);
			}

			var total = queryCount.UniqueResult<long>();

			query.SetResultTransformer(Transformers.AliasToBean(typeof (ProcessoSeguroCacheDto)));

			if (pagination != null)
			{
				if (pagination.Inicio.HasValue)
				{
					query.SetFirstResult(pagination.Inicio.Value);
				}

				int limit = GetLimit(pagination);
				if (limit > 0)
				{
					query.SetMaxResults(limit);
				}
			}

			IList<ProcessoSeguroCacheDto> items = query.List<ProcessoSeguroCacheDto>();

			return new ResultadoPaginado<ProcessoSeguroCacheDto> {Total = total, Items = items};
		}

		/// <summary>
		///   Retorna todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário logado</param>
		/// <returns>lista de processos em cache</returns>
		public IList<ProcessoSeguroCache> ObterPorUsuario(Usuario usuario)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Usuario", usuario));

			return ObterTodos(criteria);
		}

        /// <summary>
        ///   Retorna se o laudo já está cadastrado para o despacho e série informados
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <returns>True para cadastrado, False para não cadastrado</returns>
	    public bool ExisteDespachoSerie(int despacho, string serie)
	    {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Despacho", despacho));
            criteria.Add(Restrictions.Eq("Serie", serie));

            return Existe(criteria);
	    }

	    /// <summary>
		///   Remove determinado processo da tabela de cache
		/// </summary>
		/// <param name="processoSeguroCache">Processo a ser Removido</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		public bool RemoverProcessoSeguroCache(ProcessoSeguroCache processoSeguroCache)
		{
			try
			{
				processoSeguroCache.Usuario = null;
				processoSeguroCache.Causa = null;
				// processoSeguroCache.Terminal = null;
				processoSeguroCache.Vagao = null;
				processoSeguroCache.Unidade = null;
				processoSeguroCache.ContaContabil = null;
			    processoSeguroCache.ClienteSeguro = null;
				base.Remover(processoSeguroCache);
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		///   Remove todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		public bool RemoverPorUsuario(Usuario usuario)
		{
			try
			{
				DetachedCriteria criteria = CriarCriteria();
				criteria.Add(Restrictions.Eq("Usuario", usuario));
				this.Remover(criteria);
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		///   Salva o processo seguro em cache do usuário
		/// </summary>
		/// <param name="processoSeguroCache">Objeto a ser salvo</param>
		/// <returns></returns>
		public ProcessoSeguroCache Salvar(ProcessoSeguroCache processoSeguroCache)
		{
			return base.InserirOuAtualizar(processoSeguroCache);
		}
	}
}