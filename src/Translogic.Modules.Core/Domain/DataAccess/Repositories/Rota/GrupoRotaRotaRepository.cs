﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Rota
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Rota;
    using Translogic.Modules.Core.Domain.Model.Rota.Repositories;
    public class GrupoRotaRotaRepository : NHRepository<GrupoRotaRota, int>, IGrupoRotaRotaRepository
    {
        public GrupoRotaRota Obter(int id)
        {
            return this.Obter(id);
        }
        public void Salvar(GrupoRotaRota grupoRota)
        {
            if (grupoRota.Id == 0)
                this.InserirOuAtualizar(grupoRota);
            else
                this.Atualizar(grupoRota);
        }

        public void Excluir(GrupoRotaRota grupoRota)
        {
            if (ExistePraNaoExcluir(grupoRota.IdGrupo.ToString()))
                throw new Exception("Existe uma associação de rota para este grupo");

            this.Remover(grupoRota);
        }

        public ResultadoPaginado<GrupoRotaRotaDto> ObterAssociacoes(
            DetalhesPaginacaoWeb detalhesPaginacaoWeb, 
            DateTime? dataInicial, DateTime? dataFinal, 
            string grupoRota, string origem, 
            string destino, string rota)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT 
                             GRUPO_ROTA_RTA.GRP_ROTA_RTA_ID AS ""IdGrupoRotaRota""
                            , GRUPO_ROTA.GRP_ROTA_ID AS ""IdGrupo""
                            , GRUPO_ROTA.GRP_ROTA_NOME AS ""Grupo""
                            , ROTA.RT_ID_RTA AS ""IdRota""
                            , ROTA.RT_COD_RTA AS ""Rota""
                            , GRUPO_ROTA.GRP_ROTA_TIMESTAMP AS ""VersionDate""
                            , US_NOM_USU AS ""Usuario""
                        FROM
                            GRUPO_ROTA
                        LEFT JOIN
                            GRUPO_ROTA_RTA
                        ON
                            GRUPO_ROTA_RTA.GRP_ROTA_ID = GRUPO_ROTA.GRP_ROTA_ID
                        INNER JOIN
                            ROTA
                        ON
                            ROTA.RT_ID_RTA = GRUPO_ROTA_RTA.RT_ID_RTA
                        INNER JOIN
                            AREA_OPERACIONAL ORI
                        ON
                            ROTA.AO_ID_AO_INC = ORI.AO_ID_AO
                        INNER JOIN
                            AREA_OPERACIONAL DST
                        ON
                            ROTA.AO_ID_AO_FIM = DST.AO_ID_AO
                        INNER JOIN
                            USUARIO
                        ON
                            USUARIO.US_USR_IDU = GRUPO_ROTA.US_USR_IDU ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(grupoRota))
            {
                sqlWhere.AppendFormat(" AND GRUPO_ROTA.GRP_ROTA_ID = {0}", grupoRota);
            }

            if (!string.IsNullOrEmpty(origem))
            {
                sqlWhere.AppendFormat(" AND ROTA.RT_COD_RTA LIKE '%{0}%'", origem);
            }

            if (!string.IsNullOrEmpty(destino))
            {
                sqlWhere.AppendFormat(" AND ROTA.RT_COD_RTA LIKE  '%{0}%'", destino);
            }

            if (!string.IsNullOrEmpty(rota))
            {
                sqlWhere.AppendFormat(" AND ROTA.RT_ID_RTA =  {0}", rota);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY GRP_ROTA_NOME,RT_COD_RTA");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GrupoRotaRotaDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<GrupoRotaRotaDto>();

                var result = new ResultadoPaginado<GrupoRotaRotaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        private bool ExistePraNaoExcluir(string idGrupo)
        {
            #region SQL Query

            var sql = new StringBuilder(@" SELECT
                     ROTA.RT_ID_RTA
                FROM
                    ROTA
                INNER JOIN
                    GRUPO_ROTA_RTA
                ON
                    GRUPO_ROTA_RTA.RT_ID_RTA = ROTA.RT_ID_RTA
                INNER JOIN
                    GRP_RTA_FRT_MC_PESO
                ON
                    GRP_RTA_FRT_MC_PESO.GRP_ROTA_ID = GRUPO_ROTA_RTA.GRP_ROTA_ID ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(idGrupo))
            {
                sqlWhere.AppendFormat(" GRUPO_ROTA_RTA.GRP_ROTA_ID = {0}", idGrupo);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString();
                sql.Append(aux);
            }

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var result = query.UniqueResult<decimal>();
                return result > 0;
            }

        }
    }
}