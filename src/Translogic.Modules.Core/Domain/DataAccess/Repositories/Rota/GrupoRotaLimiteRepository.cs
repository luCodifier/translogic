﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Rota
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Rota;
    using Translogic.Modules.Core.Domain.Model.Rota.Repositories;
    public class GrupoRotaLimiteRepository : NHRepository<Limite, int>, IGrupoRotaLimiteRepository
    {
        public IList<FrotaDto> ObterFrotas()
        {
            #region SQL Query

            var sql = new StringBuilder(@"SELECT  
                                 FV_ID_FT AS ""IdFrota""
                                ,FV_COD_FRT AS ""Codigo""
                                ,FV_DRS_PT AS ""Descricao""
                            FROM 
                                FROTA_VAGAO 
                            ORDER BY
                               FV_COD_FRT  ");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<FrotaDto>());

                var result = query.List<FrotaDto>();

                return result;
            }
        }

        public ResultadoPaginado<LimiteDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal, string grupo, string frota, string mercadoria)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT 
                 GRP.GRP_RTA_FRT_MC_PESO_ID AS ""IdLimite""
                 ,GRUPO_ROTA.GRP_ROTA_ID  AS ""IdGrupo""
                ,GRUPO_ROTA.GRP_ROTA_NOME AS ""Grupo""
                ,FROTA_VAGAO.FV_ID_FT  AS ""IdFrota""
                ,FROTA_VAGAO.FV_COD_FRT||' - '||FROTA_VAGAO.FV_DRS_PT AS ""Frota""
                ,MC.MC_ID_MRC AS ""IdMercadoria""
                ,MC.MC_COD_MRC||' - '||MC.MC_DRS_PT AS ""Mercadoria""
                ,GRP.GRP_RTA_FRT_MC_PESO_MIN AS ""Peso""
                ,TOL.TOL_MERC_MIN AS ""Tolerancia""
                ,GRP.GRP_RTA_FRT_MC_PESO_TIMESTAMP   AS ""VersionDate""
                ,USUARIO.US_NOM_USU  AS ""Usuario""
            FROM
                GRP_RTA_FRT_MC_PESO     GRP
            INNER JOIN
                GRUPO_ROTA 
            ON
                GRUPO_ROTA.GRP_ROTA_ID  = GRP.GRP_ROTA_ID
            INNER JOIN
                FROTA_VAGAO
            ON
                FROTA_VAGAO.FV_ID_FT    = GRP.FV_ID_FT
            INNER JOIN
                MERCADORIA              MC
            ON
                MC.MC_ID_MRC            = GRP.MC_ID_MRC
            LEFT JOIN
                TOLERANCIA_MERCADORIA   TOL
            ON
                TOL.MC_ID_MRC           = MC.MC_ID_MRC
            INNER JOIN
                USUARIO
            ON
                USUARIO.US_USR_IDU      = GRP.US_USR_IDU");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_RTA_FRT_MC_PESO_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_RTA_FRT_MC_PESO_TIMESTAMP >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_RTA_FRT_MC_PESO_TIMESTAMP <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            if (!string.IsNullOrEmpty(grupo))
            {
                sqlWhere.AppendFormat(" AND GRUPO_ROTA.GRP_ROTA_ID = {0}", grupo);
            }
            if (!string.IsNullOrEmpty(frota))
            {
                sqlWhere.AppendFormat(" AND FROTA_VAGAO.FV_ID_FT = {0}", frota);
            }
            if (!string.IsNullOrEmpty(mercadoria))
            {
                sqlWhere.AppendFormat(" AND MC.MC_ID_MRC = {0}", mercadoria);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY GRUPO_ROTA.GRP_ROTA_NOME");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<LimiteDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<LimiteDto>();

                var result = new ResultadoPaginado<LimiteDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public Limite ObterLimite(int id)
        {
            return this.ObterPorId(id);
        }

        public void Salvar(Limite limite)
        {
            if (limite.Id == 0)
                this.InserirOuAtualizar(limite);
            else
                this.Atualizar(limite);
        }

        public void Excluir(Limite limite)
        {
            this.Remover(limite);
        }
    }
}