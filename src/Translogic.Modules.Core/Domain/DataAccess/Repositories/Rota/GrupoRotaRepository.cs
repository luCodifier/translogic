﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Rota
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Rota;
    using Translogic.Modules.Core.Domain.Model.Rota.Repositories;
    public class GrupoRotaRepository : NHRepository<GrupoRota, int>, IGrupoRotaRepository
    {
        public ResultadoPaginado<GrupoRotaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT 
                                     GRUPO_ROTA.GRP_ROTA_ID   AS  ""IdGrupo""
                                    ,GRUPO_ROTA.GRP_ROTA_NOME   AS ""Grupo""
                                    ,GRUPO_ROTA.GRP_ROTA_TIMESTAMP  AS  ""VersionDate""
                                    ,US_NOM_USU AS ""Usuario""
                                FROM 
                                    GRUPO_ROTA
                                INNER JOIN USUARIO ON USUARIO.US_USR_IDU = GRUPO_ROTA.US_USR_IDU");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY GRP_ROTA_NOME");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GrupoRotaDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<GrupoRotaDto>();

                var result = new ResultadoPaginado<GrupoRotaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }
        public GrupoRota Obter(int id)
        {
            return this.Obter(id);
        }

        public IList<GrupoRotaDto> ObterGrupoRotas()
        {
            #region SQL Query

            var sql = new StringBuilder(@"SELECT 
                                     GRUPO_ROTA.GRP_ROTA_ID   AS  ""IdGrupo""
                                    ,GRUPO_ROTA.GRP_ROTA_NOME   AS ""Grupo""
                                    ,GRUPO_ROTA.GRP_ROTA_TIMESTAMP  AS  ""VersionDate""
                                    ,US_NOM_USU AS ""Usuario""
                                FROM 
                                    GRUPO_ROTA
                                INNER JOIN USUARIO ON USUARIO.US_USR_IDU = GRUPO_ROTA.US_USR_IDU");

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY GRP_ROTA_NOME");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<GrupoRotaDto>());

                var result = query.List<GrupoRotaDto>();

                return result;
            }
        }

        public IList<RotaGrupoRotaDto> ObterRotas()
        {
            #region SQL Query

            var sql = new StringBuilder(@"SELECT 
                    R.RT_ID_RTA AS ""IdRota"",
                    R.RT_COD_RTA AS ""Codigo"",
                    R.RT_DSC_RTA AS ""Descricao""
                FROM
                     ROTA R                    
                WHERE
                    R.RT_IND_ATIV = 'S' ORDER BY R.RT_COD_RTA");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<RotaGrupoRotaDto>());

                var result = query.List<RotaGrupoRotaDto>();

                return result;
            }
        }

        public void Salvar(GrupoRota grupoRota)
        {
            if (grupoRota.Id == 0)
            {
                if (ExisteGrupo(grupoRota.Grupo)) {
                    throw new Exception("Já existe grupo com este nome");
                }
                this.InserirOuAtualizar(grupoRota);
            }
            else
                this.Atualizar(grupoRota);
        }

        public void Excluir(GrupoRota grupoRota)
        {
            if (ExistePraNaoExcluir(grupoRota.Id.ToString()))
                throw new Exception("Existe uma associação de rota para este grupo");

            if (ExistePraNaoExcluir2(grupoRota.Id.ToString()))
                throw new Exception("Existe um limite mínimo para este grupo");

            this.Remover(grupoRota);
        }      

        public ResultadoPaginado<GrupoRotaDto> ObterAssociacoes(
            DetalhesPaginacaoWeb detalhesPaginacaoWeb, 
            DateTime? dataInicial, DateTime? dataFinal, 
            string grupoRota, string origem, 
            string destino, string rota)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT 
                             GRUPO_ROTA.GRP_ROTA_ID AS ""IdGrupo""
                            ,GRUPO_ROTA.GRP_ROTA_NOME AS ""Grupo""
                            ,ROTA.RT_ID_RTA AS ""IdRota""
                            ,ROTA.RT_DSC_RTA AS ""Rota""
                            ,GRUPO_ROTA.GRP_ROTA_TIMESTAMP AS ""VersionDate""
                            ,US_NOM_USU AS ""Usuario""
                        FROM 
                            GRUPO_ROTA
                        LEFT JOIN
                            GRUPO_ROTA_RTA
                        ON
                            GRUPO_ROTA_RTA.GRP_ROTA_ID = GRUPO_ROTA.GRP_ROTA_ID
                        INNER JOIN
                            ROTA 
                        ON
                            ROTA.RT_ID_RTA              = GRUPO_ROTA_RTA.RT_ID_RTA
                        INNER JOIN
                            AREA_OPERACIONAL ORI
                        ON
                            ROTA.AO_ID_AO_INC = ORI.AO_ID_AO
                        INNER JOIN 
                            AREA_OPERACIONAL DST
                        ON
                            ROTA.AO_ID_AO_FIM = DST.AO_ID_AO
                        INNER JOIN
                            USUARIO
                        ON
                            USUARIO.US_USR_IDU         = GRUPO_ROTA.US_USR_IDU
                        
                        ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND GRP_ROTA_TIMESTAMP <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(grupoRota))
            {
                sqlWhere.AppendFormat(" AND GRUPO_ROTA.GRP_ROTA_ID =  {0}", grupoRota);
            }

            if (!string.IsNullOrEmpty(origem))
            {
                sqlWhere.AppendFormat(" AND ORI.AO_COD_AOP =  '{0}'", origem);
            }

            if (!string.IsNullOrEmpty(destino))
            {
                sqlWhere.AppendFormat(" AND ORI.AO_COD_AOP =  '{0}'", destino);
            }

            if (!string.IsNullOrEmpty(rota))
            {
                sqlWhere.AppendFormat(" AND ROTA.RT_ID_RTA =  {0}", rota);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY GRP_ROTA_NOME");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GrupoRotaDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<GrupoRotaDto>();

                var result = new ResultadoPaginado<GrupoRotaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        private bool ExistePraNaoExcluir(string idGrupo)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@" SELECT 
                    GRP_ROTA_RTA_ID
                FROM
                    GRUPO_ROTA_RTA ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(idGrupo))
            {
                sqlWhere.AppendFormat("AND GRP_ROTA_ID = {0}", idGrupo);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " where rownum = 1 " + sqlWhere.ToString();
                sql.Append(aux);
            }

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var result = query.UniqueResult<decimal>();
                return result > 0;
            }          

        }

        private bool ExistePraNaoExcluir2(string idGrupo)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@" SELECT
                    GRP_ROTA_ID
                FROM
                    GRP_RTA_FRT_MC_PESO ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(idGrupo))
            {
                sqlWhere.AppendFormat("GRP_ROTA_ID = {0}", idGrupo);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString();
                sql.Append(aux);
            }

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var result = query.UniqueResult<decimal>();
                return result > 0;
            }

        }

        public bool ExisteGrupo(string grupo)
        {
            var result = this.ObterConsulta(new DetalhesPaginacaoWeb(), Convert.ToDateTime("01/01/2000"), DateTime.Now);

            return result.Items.Any(g => g.Grupo == grupo);
        }
    }
}