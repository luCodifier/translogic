﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.LiberacaoFormacaoTrem
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem;
    using Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem.Repositories;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Extensions;


    public class LiberacaoFormacaoTremRepository : NHRepository<LiberacaoFormacaoTrem, int>, ILiberacaoFormacaoTremRepository
    {
        public ResultadoPaginado<LiberacaoFormacaoTremDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal OrdemServico, string OrigemTrem, string DestinoTrem, string LocalAtual, string NomeSol, string NomeAutorizador, DateTime dataInicial, DateTime dataFinal, string SituacaoTrava)
        {
            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT    LT.DS_NOME_SOL                                                  as ""NomeSol"",
                                                    LT.CD_ORDEM_SERVICO                                             as ""OrdemServico"", 
                                                    CONCAT(LT.CD_ORIGEM_TREM , CONCAT( '/'  , LT.CD_DESTINO_TREM )) as ""OrigemDestinoTrem"",
                                                    LT.CD_LOCAL_ATUAL                                               as ""LocalAtual"",
                                                    LT.DS_TIPO_TRAVA                                                as ""TipoTrava"",
                                                    LT.DS_MOTIVO_TRAVA                                              as ""MotivoTrava"",
                                                    LT.DT_PARTIDA                                                   as ""DtPartida"",
                                                    LT.DT_SOLICITACAO                                               as ""DtSolicitacao"",
                                                    LT.DT_RESPOSTA_AUTORIZADOR                                      as ""DtResposta"",
                                                    LT.DS_NOME_AUTORIZADOR                                          as ""NomeAutorizador"",
                                                    (CASE WHEN LT.ST_TRAVA = '1' THEN 'Pendente'
                                                          WHEN LT.ST_TRAVA = '2' THEN 'Aprovado'
                                                          WHEN LT.ST_TRAVA = '3' THEN 'Reprovado'
                                                          ELSE LT.ST_TRAVA END)                                 as ""SituacaoTrava"" 
                                                    FROM TB_LIBERACAO_TRAVAS LT");

            #endregion

            #region WHERE
            // Filtros
            var sqlWhere = new StringBuilder();
      
            //OS
            if (OrdemServico != null && OrdemServico != 0)
            {
                sqlWhere.AppendFormat(" AND LT.CD_ORDEM_SERVICO = '{0}'", OrdemServico);
            }

            //Origem
            if (!string.IsNullOrEmpty(OrigemTrem))
            {
                sqlWhere.AppendFormat(" AND LT.CD_ORIGEM_TREM = '{0}'", OrigemTrem.ToUpper());
            }

            //Destino
            if (!string.IsNullOrEmpty(DestinoTrem))
            {
                sqlWhere.AppendFormat(" AND LT.CD_DESTINO_TREM = '{0}'", DestinoTrem.ToUpper());
            }

            //Local
            if (!string.IsNullOrEmpty(LocalAtual))
            {
                sqlWhere.AppendFormat(" AND LT.CD_LOCAL_ATUAL = '{0}'", LocalAtual.ToUpper());
            }

            //Nome Solicitante
            if (!string.IsNullOrEmpty(NomeSol))
            {
                sqlWhere.AppendFormat(" AND LT.DS_NOME_SOL LIKE '%{0}%'", NomeSol);
            }

            //Nome Autorizador
            if (!string.IsNullOrEmpty(NomeAutorizador))
            {
                sqlWhere.AppendFormat(" AND LT.DS_NOME_AUTORIZADOR LIKE '%{0}%'", NomeAutorizador);
            }

            // Periodo Inicial && Periodo Final
            if (dataInicial != null && dataFinal != null)
            {
                sqlWhere.AppendFormat(" AND LT.DT_SOLICITACAO BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.ToString("dd/MM/yyyy"), dataFinal.ToString("dd/MM/yyyy"));
            }

            //Situação Trava
            if (!string.IsNullOrEmpty(SituacaoTrava) && SituacaoTrava != "Todos")
            {
                sqlWhere.AppendFormat(" AND LT.ST_TRAVA = '{0}'", StatusTrava(SituacaoTrava));
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }
                 
            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            //sql.Append(" ORDER BY os.ID_OS DESC");
            //sql.Append("          QRY.LOCAL, ");
            //sql.Append("          QRY.LINHA, ");
            //sql.Append("          QRY.SEQUENCIA ASC");

            #endregion

            #region Acessa a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<LiberacaoFormacaoTremDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<LiberacaoFormacaoTremDto>();

                var result = new ResultadoPaginado<LiberacaoFormacaoTremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;

            #endregion

            }
        }

        public IList<LiberacaoFormacaoTremDto> ObterLiberacaoFormacaoTremExportar(decimal OrdemServico, 
                                                                           string OrigemTrem, 
                                                                           string DestinoTrem, 
                                                                           string LocalAtual, 
                                                                           string NomeSol, 
                                                                           string NomeAutorizador, 
                                                                           DateTime dataInicial, 
                                                                           DateTime dataFinal,
                                                                           string SituacaoTrava)
        {

            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT    LT.DS_NOME_SOL                                                  as ""NomeSol"",
                                                    LT.CD_ORDEM_SERVICO                                             as ""OrdemServico"", 
                                                    CONCAT(LT.CD_ORIGEM_TREM , CONCAT( '/'  , LT.CD_DESTINO_TREM )) as ""OrigemDestinoTrem"",
                                                    LT.CD_LOCAL_ATUAL                                               as ""LocalAtual"",
                                                    LT.DS_TIPO_TRAVA                                                as ""TipoTrava"",
                                                    LT.DS_MOTIVO_TRAVA                                              as ""MotivoTrava"",
                                                    LT.DT_PARTIDA                                                   as ""DtPartida"",
                                                    LT.DT_SOLICITACAO                                               as ""DtSolicitacao"",
                                                    LT.DT_RESPOSTA_AUTORIZADOR                                      as ""DtResposta"",
                                                    LT.DS_NOME_AUTORIZADOR                                          as ""NomeAutorizador"",
                                                    (CASE WHEN LT.ST_TRAVA = '1' THEN 'Pendente'
                                                          WHEN LT.ST_TRAVA = '2' THEN 'Aprovado'
                                                          WHEN LT.ST_TRAVA = '3' THEN 'Reprovado'
                                                          ELSE LT.ST_TRAVA END)                                     as ""SituacaoTrava"" 
                                        FROM TB_LIBERACAO_TRAVAS LT");

            #endregion

            #region WHERE
            // Filtros
            var sqlWhere = new StringBuilder();

            //OS
            if (OrdemServico != null && OrdemServico != 0)
            {
                sqlWhere.AppendFormat(" AND LT.CD_ORDEM_SERVICO = '{0}'", OrdemServico);
            }

            //Origem
            if (!string.IsNullOrEmpty(OrigemTrem))
            {
                sqlWhere.AppendFormat(" AND LT.CD_ORIGEM_TREM = '{0}'", OrigemTrem.ToUpper());
            }

            //Destino
            if (!string.IsNullOrEmpty(DestinoTrem))
            {
                sqlWhere.AppendFormat(" AND LT.CD_DESTINO_TREM = '{0}'", DestinoTrem.ToUpper());
            }

            //Local
            if (!string.IsNullOrEmpty(LocalAtual))
            {
                sqlWhere.AppendFormat(" AND LT.CD_LOCAL_ATUAL = '{0}'", LocalAtual.ToUpper());
            }

            //Nome Solicitante
            if (!string.IsNullOrEmpty(NomeSol))
            {
                sqlWhere.AppendFormat(" AND LT.DS_NOME_SOL LIKE '%{0}%'", NomeSol);
            }

            //Nome Autorizador
            if (!string.IsNullOrEmpty(NomeAutorizador))
            {
                sqlWhere.AppendFormat(" AND LT.DS_NOME_AUTORIZADOR LIKE '%{0}%'", NomeAutorizador);
            }

            // Periodo Inicial && Periodo Final
            if (dataInicial != null && dataFinal != null)
            {
                sqlWhere.AppendFormat(" AND LT.DT_SOLICITACAO BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.ToString("dd/MM/yyyy"), dataFinal.ToString("dd/MM/yyyy"));
            }

            //Situação Trava
            if (!string.IsNullOrEmpty(SituacaoTrava) && SituacaoTrava != "Todos")
            {
                sqlWhere.AppendFormat(" AND LT.ST_TRAVA = '{0}'", StatusTrava(SituacaoTrava));
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            //sql.Append(" ORDER BY os.ID_OS DESC");
            //sql.Append("          QRY.LOCAL, ");
            //sql.Append("          QRY.LINHA, ");
            //sql.Append("          QRY.SEQUENCIA ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<LiberacaoFormacaoTremDto>());

                return query.List<LiberacaoFormacaoTremDto>();
            }
        }

        public int ObterTimmerConsultaLiberacaoFormacaoTrem()
        {
            try
            {
                #region SQL Query

                var parameters = new List<Action<IQuery>>();
                var sql = new StringBuilder(@"SELECT CG.VALOR FROM CONF_GERAL CG WHERE CG.CHAVE = 'TIMER_AUTO_CONSULTA_LIBERACAO_FORMACAO_TREM'");
                
                #endregion

                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql.ToString());

                    var result = query.UniqueResult();

                    if (Convert.ToDouble(result) != null)
                    {
                        return Convert.ToInt32(result);
                    }
                    else
                    {
                        return Convert.ToInt32(30000);
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private int StatusTrava(string SituacaoTrava)
        {
           switch (SituacaoTrava)
                {
                case ("Pendente"):
                    return 1;
                case ("Aprovado"):
                    return 2;
                case ("Reprovado"):
                    return 3;
                }
           return 0;
        }

    }
}

   