namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Codificador
{
	using ALL.Core.AcessoDados;
	using Model.Codificador;
	using Model.Codificador.Repositories;

	/// <summary>
	/// Implementação do repositório de configuração do translogic com NHibernate
	/// </summary>
	public class ConfiguracaoTranslogicRepository : NHRepository<ConfiguracaoTranslogic, string>, IConfiguracaoTranslogicRepository
	{
	}
}