namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.Repositories;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate;

    /// <summary>
    /// Repositorio para <see cref="Mercadoria"/>
    /// </summary>
    public class MercadoriaRepository : NHRepository<Mercadoria, int>, IMercadoriaRepository
    {
        /// <summary>
        /// Obt�m a mercadoria pelo c�digo
        /// </summary>
        /// <param name="codigoMercadoria">C�digo da mercadoria</param>
        /// <returns>Objeto <see cref="Mercadoria"/></returns>
        public Mercadoria ObterPorCodigo(string codigoMercadoria)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Codigo", codigoMercadoria));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Lista de mercadorias
        /// </summary>
        /// <param name="query"> Filtro para os mercadorias</param>
        /// <returns>Lista de Mercadorias</returns>
        public List<Mercadoria> ObterLista(string query)
        {
            using (var session = OpenSession())
            {
                var tq = query.Trim().ToUpper().Length > 0? query.Trim().ToUpper(): "A";
                var qr = from em in session.Query<Mercadoria>()
                         where em.Apelido.ToUpper().Contains(tq)
                            || em.Codigo.ToUpper().Contains(tq)
                            || em.DescricaoResumida.Contains(tq)
                         select em;

                return qr.ToList();
            }
        }

        public IList<Mercadoria> ObterTodasMercadorias()
        {
            using (var session = OpenSession())
            {
                /* Poderia utilizar o m�todo ObterTodos mas precisei utilizar esta query pois o banco de dev est� incorreto. 
                 * Na teoria, � para o banco de produ��o estar correto. */
                string sql = @" 
                    FROM Mercadoria MER
                   WHERE MER.ModalOrigemInformacao IS NOT NULL
                     AND MER.Produto               IS NOT NULL";

                IQuery query = session.CreateQuery(sql);

                IList<Mercadoria> resultados = query.List<Mercadoria>();
                return resultados.ToList();
            }
        }
    }
}