namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Refaturamento
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento.Repositories;

	/// <summary>
	/// Repositório para <see cref="RefaturamentoDestinoPerm"/>
	/// </summary>
	public class RefaturamentoDestinoPermRepository : NHRepository<RefaturamentoDestinoPerm, int>, IRefaturamentoDestinoPermRepository
	{
		/// <summary>
		/// Retorna todos os Destino Permitidos para Refaturamento
		/// </summary>
		/// <returns>lista de Destino Permitidos para Refaturamento.</returns>
		public IList<RefaturamentoDestinoPerm> ObterTodosAtivos()
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Ativo", true));
			return ObterTodos(criteria);
		}
	}
}