namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Ndd
{
	using System;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.FluxosComerciais.Ndd;
	using Model.FluxosComerciais.Ndd.Repositories;

	/// <summary>
	/// Reposit�rio da classe TbDatabaseInputRepository
	/// </summary>
	public class TbDatabaseInputRepository : NHRepository<TbDatabaseInput, int?>, ITbDatabaseInputRepository
	{
		/// <summary>
		/// Insere os dados na tabela utilizada pela NDD
		/// </summary>
		/// <param name="numeroDocumento">N�mero do documento a ser inserido</param>
		/// <param name="usuario">Usuario responsavel pela inser��o</param>
		/// <param name="arquivo">Dados do arquivo</param>
		public void Inserir(string numeroDocumento, Usuario usuario, StringBuilder arquivo)
		{
			UTF8Encoding utf8 = new UTF8Encoding();
			byte[] bytesArquivo = utf8.GetBytes(arquivo.ToString());

			TbDatabaseInput databaseInput = new TbDatabaseInput();
			databaseInput.DataHora = DateTime.Now;
			// databaseInput.DocumentUser = usuario.Nome;
			databaseInput.DocumentUser = "TESTE";
			// Verificar o nome desse job
			databaseInput.Job = "TESTEJOB";
			databaseInput.Kind = 0;
			databaseInput.Status = 1;
			databaseInput.NumeroDocumento = numeroDocumento;
			databaseInput.DocumentData = new byte[bytesArquivo.Length];
			bytesArquivo.CopyTo(databaseInput.DocumentData, 0);
			Inserir(databaseInput);
		}
	}
}