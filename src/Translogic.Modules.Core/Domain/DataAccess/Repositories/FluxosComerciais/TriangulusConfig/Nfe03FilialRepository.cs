namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Reposit�rio da classe Nfe03FilialRepository
    /// </summary>
    public class Nfe03FilialRepository : NHRepository<Nfe03Filial, int>, INfe03FilialRepository
    {
        /// <summary>
        /// Obt�m a filial pelo CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ a ser consultado</param>
        /// <returns>Objeto EmpresaFerrovia</returns>
        public Nfe03Filial ObterFilialPorCnpj(long cnpj)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cnpj", cnpj));
            return ObterPrimeiro(criteria);
        }
    }
}