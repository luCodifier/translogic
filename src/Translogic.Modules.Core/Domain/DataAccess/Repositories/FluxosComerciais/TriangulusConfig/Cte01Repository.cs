namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
    using System;
    using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate;
	using NHibernate.Criterion;

	/// <summary>
	/// Reposit�rio da classe Cte01
	/// </summary>
	public class Cte01Repository : NHRepository<Cte01, int>, ICte01Repository
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		public void Remover(int idFilial, int numero, int serie)
		{
			string hql = @"DELETE FROM  Cte01 WHERE CfgDoc = :CFG_DOC AND CfgSerie = :CFG_SERIE AND CfgUn = :CFG_UN";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("CFG_DOC", numero.ToString());
				query.SetString("CFG_SERIE", serie.ToString());
				query.SetString("CFG_UN", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}

        /// <summary>
        ///   Atualiza a data de emiss�o do CTE na config
        /// </summary>
        /// <param name="idFilial"> Numero da filial</param>
        /// <param name="numero"> Numero do cte</param>
        /// <param name="serie"> Serie do cte</param>
        /// <param name="dataEmissao"> Data de emiss�o</param>
        public void AtualizaDataEmissao(int idFilial, int numero, int serie, DateTime dataEmissao)
        {
            string hql = @" UPDATE ICTE01_INF set IDE_DHEMI = TO_DATE('" + dataEmissao.ToString("yyyy/MM/dd hh:mm:ss") + "','YYYY/MM/DD HH:MI:SS') WHERE CFG_DOC = :CFG_DOC AND CFG_SERIE = :CFG_SERIE AND CFG_UN = :CFG_UN ";
            using (ISession session = OpenSession())
            {
                  var query = session.CreateSQLQuery(hql);

                  query.SetString("CFG_DOC", numero.ToString());
                  query.SetString("CFG_SERIE", serie.ToString());
                  query.SetString("CFG_UN", idFilial.ToString());
                  query.ExecuteUpdate();

                  session.Flush();
            }
        }
	}
}