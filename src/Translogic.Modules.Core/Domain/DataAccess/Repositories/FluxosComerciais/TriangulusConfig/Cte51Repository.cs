namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe Cte51
	/// </summary>
	public class Cte51Repository : NHRepository<Cte51, int>, ICte51Repository
	{
		/// <summary>
		///  Remove pelo os campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		public void Remover(int idFilial, int numero, int serie)
		{
			string hql = @"DELETE FROM  Cte51 WHERE Numero = :numero AND Serie = :serie AND IdFilial = :filial";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("numero", numero.ToString());
				query.SetString("serie", serie.ToString());
				query.SetString("filial", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}
	}
}