namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;

    /// <summary>
    /// Repositório da classe Nfe19ErroClassRepository
    /// </summary>
    public class Nfe19ErroClassRepository : NHRepository<Nfe19ErroClass, int>, INfe19ErroClassRepository
    {
    }
}