namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe Nfe19ErroClassRepository
	/// </summary>
	public class Nfe20ErroLogRepository : NHRepository<Nfe20ErroLog, int>, INfe20ErroLogRepository
	{
		/// <summary>
		///  Obtem o Log de Erro pelos campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		/// <returns>Retorna o log de erro</returns>
		public Nfe20ErroLog ObterErro(int idFilial, int numero, int serie)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdFilial", idFilial));
			criteria.Add(Restrictions.Eq("NumeroCte", numero));
			criteria.Add(Restrictions.Eq("SerieCte", serie));
			criteria.AddOrder(Order.Desc("IdErroLog"));
			return ObterPrimeiro(criteria);
		}
	}
}
