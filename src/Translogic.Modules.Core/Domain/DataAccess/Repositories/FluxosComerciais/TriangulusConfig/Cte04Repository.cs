namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe Cte04
	/// </summary>
	public class Cte04Repository : NHRepository<Cte04, int>, ICte04Repository
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		public void Remover(int idFilial, int numero, int serie)
		{
			string hql = @"DELETE FROM  Cte04 WHERE CfgDoc = :CFG_DOC AND CfgSerie = :CFG_SERIE AND CfgUn = :CFG_UN";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("CFG_DOC", numero.ToString());
				query.SetString("CFG_SERIE", serie.ToString());
				query.SetString("CFG_UN", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}

		/// <summary>
		/// Obter todos teste
		/// </summary>
		/// <returns>Retorna uma lista desses objetos</returns>
		public IList<Cte04> ObterTeste()
		{
			using (var session = OpenSession("CONFIG"))
			{
				return ObterTodos();
			}
		}

		/*/// <summary>
		/// Responsável por inserir a entidade
		/// </summary>
		/// <param name="entity">Entidade que será inserida</param>
		/// <returns>
		/// A própria entidade com o Id populado (caso sequence)
		/// </returns>
		public override Cte04 Inserir(Cte04 entity)
		{
			using (var session = OpenSession("CONFIG"))
			{
				return base.Inserir(entity);
			}
		}*/
	}
}