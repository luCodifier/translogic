namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.TriangulusConfig;
    using Model.FluxosComerciais.TriangulusConfig.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Reposit�rio da classe Vw209ConsultaCteRepository
    /// </summary>
    public class Vw209ConsultaCteRepository : NHRepository<Vw209ConsultaCte, int>, IVw209ConsultaCteRepository
    {
        /// <summary>
        /// Obtem o retorno do processamento pelo id da lista de fila de processamento
        /// </summary>
        /// <param name="idLista">Identificador da lista de processamento do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterRetornoPorIdLista(int idLista)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdLista", idLista));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obtem o retorno do processamento pelo id da lista de fila de processamento
        /// </summary>
        /// <param name="idFilial"> Numero da filial</param>
        /// <param name="numero"> Numero do cte</param>
        /// <param name="serie"> Serie do cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterRetornoPorSerieNumeroUnidade(int idFilial, int numero, int serie)
        {
            string hql = @"FROM  Vw209ConsultaCte WHERE Numero = :CFG_DOC AND Serie = :CFG_SERIE AND IdFilial = :CFG_UN";

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);

                query.SetString("CFG_DOC", numero.ToString());
                query.SetString("CFG_SERIE", serie.ToString());
                query.SetString("CFG_UN", idFilial.ToString());

                IList<Vw209ConsultaCte> items = query.List<Vw209ConsultaCte>();

                if (items.Count > 0)
                {
                    return items[0];
                }

                return null;
            }
        }

        /// <summary>
        /// Obtem o retorno do processamento pelo protocolo
        /// </summary>
        /// <param name="protocolo">Identificador da lista de processamento do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterRetornoPorProtocolo(long protocolo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Protocolo", protocolo));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obtem o retorno do processamento pela chave
        /// </summary>
        /// <param name="chave">Identificador da chave do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterRetornoPorChave(string chave)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("ChaveAcesso", chave + "%"));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obtem o retorno do processamento pela chave
        /// </summary>
        /// <param name="chave">Identificador da chave do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterCteJaAutorizado(string chave)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("ChaveAcesso", chave + "%"));
            criteria.Add(Restrictions.Eq("Stat", 100));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m o retorno do processamento 
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        public Vw209ConsultaCte ObterCteJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie)
        {
            string hql = @"FROM  Vw209ConsultaCte WHERE Stat = 100 AND Numero = :CFG_DOC AND Serie = :CFG_SERIE AND IdFilial = :CFG_UN";

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);

                query.SetString("CFG_DOC", numero.ToString());
                query.SetString("CFG_SERIE", serie.ToString());
                query.SetString("CFG_UN", idFilial.ToString());

                return query.UniqueResult<Vw209ConsultaCte>();
            }
        }
    }
}