namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe Cte121
	/// </summary>
    public class Cte121Repository : NHRepository<Cte121, int>, ICte121Repository
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		public void Remover(int idFilial, int numero, int serie)
		{
			string hql = @"DELETE FROM  Cte121 WHERE CfgDoc = :CFG_DOC AND CfgSerie = :CFG_SERIE AND CfgUn = :CFG_UN";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("CFG_DOC", numero.ToString());
				query.SetString("CFG_SERIE", serie.ToString());
				query.SetString("CFG_UN", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}
	}
}