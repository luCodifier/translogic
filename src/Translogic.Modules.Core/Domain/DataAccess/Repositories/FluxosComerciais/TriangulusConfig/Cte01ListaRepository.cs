namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.TriangulusConfig;
	using Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe da fila de processamento da config
	/// </summary>
	public class Cte01ListaRepository : NHRepository<Cte01Lista, int>, ICte01ListaRepository
	{
		/// <summary>
		/// Obtém o item da fila de processamento por filial, numero cte e serie cte
		/// </summary>
		/// <param name="idFilial">Identificador da filial</param>
		/// <param name="numeroCte">Numero do Cte</param>
		/// <param name="serieCte">Serie do Cte</param>
		/// <param name="chaveAcesso">Chave de acesso do cte</param>
		/// <param name="tipo">Tipo da acao na lista</param>
		/// <param name="ascendente">Tipo da classificação ascendente</param>
		/// <returns>Retorna o objeto da fila de processamento</returns>
		public Cte01Lista ObterListaFilaProcessamento(int idFilial, int numeroCte, int serieCte, string chaveAcesso, int tipo, bool ascendente)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdFilial", idFilial));
			criteria.Add(Restrictions.Eq("Numero", numeroCte));
			criteria.Add(Restrictions.Eq("Serie", serieCte));
		    if (!string.IsNullOrEmpty(chaveAcesso))
            {
                criteria.Add(Restrictions.Eq("ChaveAcesso", chaveAcesso));
		    }
			criteria.Add(Restrictions.Eq("Tipo", tipo));

			// criteria.AddOrder(new Order("Id", false));
			criteria.AddOrder(new Order("Id", ascendente));
			return ObterPrimeiro(criteria);
		}
	}
}