namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Carregamentos
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Carregamentos;
	using Model.FluxosComerciais.Carregamentos.Repositories;

	/// <summary>
	/// Repositório do Log do tempo de carregamento
	/// </summary>
	public class LogTempoCarregamentoRepository : NHRepository<LogTempoCarregamento, int>, ILogTempoCarregamentoRepository
	{	
	}
}