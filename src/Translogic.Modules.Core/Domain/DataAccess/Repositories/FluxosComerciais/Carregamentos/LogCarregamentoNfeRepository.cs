namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Carregamentos
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Carregamentos;
	using Model.FluxosComerciais.Carregamentos.Repositories;

	/// <summary>
	/// Repositório do Log do tempo de consulta da nfe
	/// </summary>
	public class LogCarregamentoNfeRepository : NHRepository<LogCarregamentoNfe, int>, ILogCarregamentoNfeRepository
	{	
	}
}