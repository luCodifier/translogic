﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Classe do repositório de recebimento
	/// </summary>
	public class MdfeRecebimentoPoolingRepository : NHRepository<MdfeRecebimentoPooling, int?>, IMdfeRecebimentoPoolingRepository
	{
		/// <summary>
		/// Obtém os itens do pooling para serem processado o retorno
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que estão no pooling</returns>
		public IList<MdfeRecebimentoPooling> ObterListaRecebimentoPorHost(string hostName)
		{
			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM MdfeRecebimentoPooling mrp
						WHERE mrp.HostName = :host
						ORDER BY mrp.DataHora";
					IQuery query = session.CreateQuery(hql);
					query.SetString("host", hostName);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<MdfeRecebimentoPooling>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		public void RemoverPorLista(IList<MdfeRecebimentoPooling> lista)
		{
			string hql = @"DELETE FROM MdfeRecebimentoPooling WHERE Id = :idRecebimento";
			using (ISession session = OpenSession())
			{
				foreach (MdfeRecebimentoPooling recebimento in lista)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idRecebimento", recebimento.Id.Value);
					query.ExecuteUpdate();
				}
			}
		}

		/// <summary>
		/// Limpa a sessao
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Clear();
				session.Flush();
			}
		}
	}
}