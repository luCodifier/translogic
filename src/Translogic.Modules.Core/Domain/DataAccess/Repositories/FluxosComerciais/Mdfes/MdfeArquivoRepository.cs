namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Util;

    /// <summary>
    /// Classe de Mdfe Arquivo Repository.
    /// </summary>
    public class MdfeArquivoRepository : NHRepository<MdfeArquivo, int?>, IMdfeArquivoRepository
    {
        /// <summary>
        /// Insere um Cte arquivo
        /// </summary>
        /// <param name="mdfe">Cte de refer�ncia</param>
        /// <param name="nomeArquivoPdf">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto CteArquivo</returns>
        public MdfeArquivo Inserir(Mdfe mdfe, string nomeArquivoPdf)
        {
            try
            {
                MdfeArquivo mdfeArquivo = new MdfeArquivo();

                byte[] arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivoPdf);
                mdfeArquivo.Id = mdfe.Id;
                mdfeArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
                arquivoBinario.CopyTo(mdfeArquivo.ArquivoPdf, 0);
                mdfeArquivo.TamanhoPdf = arquivoBinario.Length;
                mdfeArquivo.DataHora = DateTime.Now;

                return Inserir(mdfeArquivo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Insere um mdfe arquivo (PDF)
        /// </summary>
        /// <param name="mdfe">mdfe de refer�ncia</param>
        /// <param name="nomeArquivo">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto mdfeArquivo</returns>
        public MdfeArquivo InserirOuAtualizarPdf(Mdfe mdfe, string nomeArquivo)
        {
            try
            {
                MdfeArquivo mdfeArquivo = ObterPorId(mdfe.Id);
                byte[] arquivoBinario;

                // Caso n�o exista, insere
                if (mdfeArquivo == null)
                {
                    mdfeArquivo = new MdfeArquivo();

                    arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
                    mdfeArquivo.Id = mdfe.Id;
                    mdfeArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
                    arquivoBinario.CopyTo(mdfeArquivo.ArquivoPdf, 0);
                    mdfeArquivo.TamanhoPdf = arquivoBinario.Length;
                    mdfeArquivo.DataHora = DateTime.Now;

                    return Inserir(mdfeArquivo);
                }

                // Atualiza o mdfe
                arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
                mdfeArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
                arquivoBinario.CopyTo(mdfeArquivo.ArquivoPdf, 0);
                mdfeArquivo.TamanhoPdf = arquivoBinario.Length;
                mdfeArquivo.DataHora = DateTime.Now;
                return Atualizar(mdfeArquivo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtem as informa��es do MDF-e n�o aprovado pela Sefaz
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do Terminal Recebedor</param>
        /// <param name="refat">Indicador se � refat ou normal. Se for refat, observa o hist�rico.</param>
        /// <returns>As informa��es contidas no MDF-e</returns>
        public ICollection<ManifestoCargaDto> ObterManifestoCargaPorOsRecebedor(decimal osId, decimal recebedorId, bool refat)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            #region SQL Query

            if (refat)
            {
                hql.Append(@"
                    SELECT DISTINCT
                           TOS.X1_PFX_TRE         AS ""TremPrefixo""
                         , TOS.X1_NRO_OS          AS ""OsNumero""
                         , AO_ORIG_TR.AO_COD_AOP  AS ""TremOrigem""
                         , AO_DEST_TR.AO_COD_AOP  AS ""TremDestino""
                         , AGRUPADOR.TOTAL_VAGOES AS ""TotalVagoes""
                         , RECEBEDOR.EP_DSC_DTL   AS ""RecebedorNome""
                         , RECEBEDOR.EP_CGC_EMP   AS ""RecebedorCnpj""
                         , SV.SV_COD_SER          AS ""VagaoSerie""
                         , VG.VG_COD_VAG          AS ""VagaoCodigo""
                         , CVV.CV_SEQ_CMP         AS ""VagaoSequencia""
                         , NVL(DC.DC_QTD_INF, 0)  AS ""PesoLiquidoVagao""
                         , COALESCE(VG.VG_NUM_TRA, FEV.FC_TAR, 0)     AS ""PesoTaraVagao""
                         , NVL(DC.DC_QTD_INF, 0) + COALESCE(VG.VG_NUM_TRA, FEV.FC_TAR, 0) AS ""PesoBrutoVagao""
                         , NF.NF_SER_NF           AS ""NotaFiscalSerie""
                         , NF.NF_NRO_NF           AS ""NotaFiscalNumero""
                         , NVL( NVL( NFE_EDI.NFE_DT_EMISSAO
                                   , NFE_SIM.NFE_DT_EMISSAO)
                              , NF.NF_DAT_NF)                                              AS ""NotaFiscalData""
                         , NVL(NFE_EDI.NFE_RAZAO_SOCIAL_ORI, NFE_SIM.NFE_RAZAO_SOCIAL_ORI) AS ""NotaFiscalRemetente""
                         , NVL(NFE_EDI.NFE_RAZAO_SOCIAL_DST, NFE_SIM.NFE_RAZAO_SOCIAL_DST) AS ""NotaFiscalDestinatario""
                         , NF.NF_PESO_NF          AS ""NotaFiscalPesoRateio""
                         , NF.NF_PESO_TOT_NF      AS ""NotaFiscalPesoTotal""
                         , MC.MC_DRS_PT           AS ""Mercadoria""
                         , DP.DP_DT               AS ""DataCarregamento""
                         , CTE.NRO_CTE            AS ""NumeroCte""

                      FROM T2_OS              TOS

                      JOIN TREM  TR  ON TR.OF_ID_OSV = TOS.X1_ID_OS

                      JOIN AREA_OPERACIONAL AO_ORIG_TR ON AO_ORIG_TR.AO_ID_AO = TR.AO_ID_AO_ORG
                      JOIN AREA_OPERACIONAL AO_DEST_TR ON AO_DEST_TR.AO_ID_AO = TR.AO_ID_AO_DST

                      -- Obtendo a ultima composicao da OS
                      JOIN (
                           SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                                , COUNT(DISTINCT CVV_TMP.VG_ID_VG) AS TOTAL_VAGOES
                                , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                             FROM COMPOSICAO         CP_TMP
                             JOIN COMPVAGAO          CVV_TMP    ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                             JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                             JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS = T_TMP.OF_ID_OSV
                            WHERE CP_TMP.CP_DAT_INC > SYSDATE - 30
                            GROUP BY TOS_TMP.X1_ID_OS
                         ) AGRUPADOR     ON AGRUPADOR.X1_ID_OS = TOS.X1_ID_OS

                      -- Obtendo a ultima composicao do vag�o contido na OS
                      JOIN (
                           SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                                , VPV_TMP.VB_IDT_VPT AS VB_IDT_VPT
                                , MAX(VPV_TMP_DATA.VB_IDT_VPT) AS VB_IDT_VPT_DATA
                                , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                                , CVV_TMP.VG_ID_VG      AS VG_ID_VG
                             FROM COMPOSICAO         CP_TMP
                             JOIN COMPVAGAO          CVV_TMP    ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                             JOIN VAGAO_PEDIDO_VIG   VPV_TMP    ON VPV_TMP.VG_ID_VG = CVV_TMP.VG_ID_VG --AND VPV_TMP.PT_ID_ORT = CVV_TMP.PT_ID_ORT
                        LEFT JOIN VAGAO_PEDIDO       VPV_TMP_DATA ON VPV_TMP_DATA.VG_ID_VG = CVV_TMP.VG_ID_VG AND VPV_TMP_DATA.VB_TIMESTAMP BETWEEN CP_TMP.CP_DAT_INC AND CP_DAT_FIM + 2
                             JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM   = CP_TMP.TR_ID_TRM
                             JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS  = T_TMP.OF_ID_OSV
                            WHERE CP_TMP.CP_DAT_INC > SYSDATE - 60
                            GROUP BY TOS_TMP.X1_ID_OS
                                   , CVV_TMP.VG_ID_VG
                                   , VPV_TMP.VB_IDT_VPT
                      ) CP_MAX ON CP_MAX.X1_ID_OS = TOS.X1_ID_OS
                      JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CP_MAX.CP_ID_CPS
                      JOIN COMPVAGAO          CVV ON CVV.CP_ID_CPS = CP.CP_ID_CPS AND CVV.VG_ID_VG = CP_MAX.VG_ID_VG
                      JOIN VAGAO_PEDIDO_VIG VPV ON VPV.VG_ID_VG = CVV.VG_ID_VG AND VPV.VB_IDT_VPT = NVL(CP_MAX.VB_IDT_VPT, CP_MAX.VB_IDT_VPT_DATA)

                      JOIN VAGAO      VG  ON VG.VG_ID_VG   = CVV.VG_ID_VG
                      JOIN SERIE_VAGAO SV ON SV.SV_ID_SV   = VG.SV_ID_SV
                      JOIN FOLHA_ESPECIF_VAGAO FEV ON FEV.FC_ID_FC = VG.FC_ID_FC

                      JOIN CARREGAMENTO           CG  ON CG.CG_ID_CAR = VPV.CG_ID_CAR
                      JOIN DETALHE_CARREGAMENTO   DC  ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND DC.VG_ID_VG = VPV.VG_ID_VG

                      JOIN ITEM_DESPACHO          ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                      JOIN DESPACHO               DP  ON DP.DP_ID_DP = ITD.DP_ID_DP
                      JOIN FLUXO_COMERCIAL        FC  ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                      JOIN MERCADORIA             MC  ON MC.MC_ID_MRC = FC.MC_ID_MRC
                      JOIN EMPRESA          REMETENTE ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                      JOIN EMPRESA          RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST

                      JOIN CTE                    CTE ON CTE.DP_ID_DP = DP.DP_ID_DP

                      JOIN NOTA_FISCAL             NF ON NF.DP_ID_DP       = ITD.DP_ID_DP
                 LEFT JOIN EDI.EDI2_NFE       NFE_EDI ON NFE_EDI.NFE_CHAVE = NF.NFE_CHAVE_NFE
                 LEFT JOIN NFE_SIMCONSULTAS   NFE_SIM ON NFE_SIM.NFE_CHAVE = NF.NFE_CHAVE_NFE

                     WHERE TOS.X1_ID_OS        = :osId
                       AND RECEBEDOR.EP_ID_EMP = :recebedorId

                     ORDER BY CVV.CV_SEQ_CMP ASC
                ");
            }
            else
            {
                hql.Append(@"
                    SELECT DISTINCT
                           TOS.X1_PFX_TRE         AS ""TremPrefixo""
                         , TOS.X1_NRO_OS          AS ""OsNumero""
                         , AO_ORIG_TR.AO_COD_AOP  AS ""TremOrigem""
                         , AO_DEST_TR.AO_COD_AOP  AS ""TremDestino""
                         , AGRUPADOR.TOTAL_VAGOES AS ""TotalVagoes""
                         , RECEBEDOR.EP_DSC_DTL   AS ""RecebedorNome""
                         , RECEBEDOR.EP_CGC_EMP   AS ""RecebedorCnpj""
                         , SV.SV_COD_SER          AS ""VagaoSerie""
                         , VG.VG_COD_VAG          AS ""VagaoCodigo""
                         , CVV.CV_SEQ_CMP         AS ""VagaoSequencia""
                         , NVL(DC.DC_QTD_INF, 0)  AS ""PesoLiquidoVagao""
                         , COALESCE(VG.VG_NUM_TRA, FEV.FC_TAR, 0)     AS ""PesoTaraVagao""
                         , NVL(DC.DC_QTD_INF, 0) + COALESCE(VG.VG_NUM_TRA, FEV.FC_TAR, 0) AS ""PesoBrutoVagao""
                         , NF.NF_SER_NF           AS ""NotaFiscalSerie""
                         , NF.NF_NRO_NF           AS ""NotaFiscalNumero""
                         , NVL( NVL( NFE_EDI.NFE_DT_EMISSAO
                                   , NFE_SIM.NFE_DT_EMISSAO)
                              , NF.NF_DAT_NF)                                              AS ""NotaFiscalData""
                         , NVL(NFE_EDI.NFE_RAZAO_SOCIAL_ORI, NFE_SIM.NFE_RAZAO_SOCIAL_ORI) AS ""NotaFiscalRemetente""
                         , NVL(NFE_EDI.NFE_RAZAO_SOCIAL_DST, NFE_SIM.NFE_RAZAO_SOCIAL_DST) AS ""NotaFiscalDestinatario""
                         , NF.NF_PESO_NF          AS ""NotaFiscalPesoRateio""
                         , NF.NF_PESO_TOT_NF      AS ""NotaFiscalPesoTotal""
                         , MC.MC_DRS_PT           AS ""Mercadoria""
                         , DP.DP_DT               AS ""DataCarregamento""
                         , CTE.NRO_CTE            AS ""NumeroCte""

                      FROM T2_OS TOS
                      JOIN TREM  TR  ON TR.OF_ID_OSV = TOS.X1_ID_OS

                      JOIN AREA_OPERACIONAL AO_ORIG_TR ON AO_ORIG_TR.AO_ID_AO = TR.AO_ID_AO_ORG
                      JOIN AREA_OPERACIONAL AO_DEST_TR ON AO_DEST_TR.AO_ID_AO = TR.AO_ID_AO_DST

                      -- Obtendo a ultima composicao da OS
                      JOIN (
                           SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                                , COUNT(DISTINCT CVV_TMP.VG_ID_VG) AS TOTAL_VAGOES
                                , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                             FROM COMPOSICAO         CP_TMP
                             JOIN COMPVAGAO          CVV_TMP    ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                             JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                             JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS = T_TMP.OF_ID_OSV
                            WHERE CP_TMP.CP_DAT_INC > SYSDATE - 30
                            GROUP BY TOS_TMP.X1_ID_OS
                         ) AGRUPADOR     ON AGRUPADOR.X1_ID_OS = TOS.X1_ID_OS
                      JOIN COMPOSICAO CP ON CP.CP_ID_CPS = AGRUPADOR.CP_ID_CPS

                      JOIN COMPVAGAO  CVV ON CVV.CP_ID_CPS = CP.CP_ID_CPS
                      JOIN VAGAO      VG  ON VG.VG_ID_VG   = CVV.VG_ID_VG
                      JOIN SERIE_VAGAO SV ON SV.SV_ID_SV   = VG.SV_ID_SV
                      JOIN FOLHA_ESPECIF_VAGAO FEV ON FEV.FC_ID_FC = VG.FC_ID_FC
                      
                      JOIN VAGAO_PEDIDO_VIG       VPV ON VPV.VG_ID_VG = VG.VG_ID_VG
                      JOIN CARREGAMENTO           CG  ON CG.CG_ID_CAR = VPV.CG_ID_CAR
                      JOIN DETALHE_CARREGAMENTO   DC  ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND DC.VG_ID_VG = VPV.VG_ID_VG

                      JOIN ITEM_DESPACHO          ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                      JOIN DESPACHO               DP  ON DP.DP_ID_DP = ITD.DP_ID_DP
                      JOIN FLUXO_COMERCIAL        FC  ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                      JOIN MERCADORIA             MC  ON MC.MC_ID_MRC = FC.MC_ID_MRC
                      JOIN EMPRESA          REMETENTE ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                      JOIN EMPRESA          RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST

                      JOIN CTE                    CTE ON CTE.DP_ID_DP = DP.DP_ID_DP

                      JOIN NOTA_FISCAL             NF ON NF.DP_ID_DP       = ITD.DP_ID_DP
                 LEFT JOIN EDI.EDI2_NFE       NFE_EDI ON NFE_EDI.NFE_CHAVE = NF.NFE_CHAVE_NFE
                 LEFT JOIN NFE_SIMCONSULTAS   NFE_SIM ON NFE_SIM.NFE_CHAVE = NF.NFE_CHAVE_NFE

                     WHERE TOS.X1_ID_OS        = :osId
                       AND RECEBEDOR.EP_ID_EMP = :recebedorId

                     ORDER BY CVV.CV_SEQ_CMP ASC
                ");
            }

            #endregion SQL Query

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetDecimal("osId", osId));
                parametros.Add(q => q.SetDecimal("recebedorId", recebedorId));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<ManifestoCargaDto>());
                var itens = query.List<ManifestoCargaDto>();

                return itens;
            }
        }
    }
}