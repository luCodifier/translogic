﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.FluxosComerciais.Mdfes;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Repositório da composição do MDF-e
    /// </summary>
    public class MdfeComposicaoRepository : NHRepository<MdfeComposicao, int?>, IMdfeComposicaoRepository
    {
        /// <summary>
        /// Obtem a composicao pelo mdfe
        /// </summary>
        /// <param name="mdfe">Objeto Mdfe</param>
        /// <returns>Retorna a composicao do mdfe</returns>
        public IList<MdfeComposicao> ObterPorMdfe(Mdfe mdfe)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM MdfeComposicao mc  
									INNER JOIN FETCH mc.Mdfe m
									WHERE mc.Mdfe.Id = :idMdfe ";

                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idMdfe", mdfe.Id.Value);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<MdfeComposicao>();
            }
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> ListarDetalhesDaComposicao(int idMdfe)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                SELECT mx.id_mdfe as ""Id"", 
                       mx.vg_cod_vag as ""CodigoVagao"",
                       mx.sequencia  as ""Sequencia"",
                       l1.ID_CTE  as ""IdCte"",
                       l1.SERIE_CTE as ""SerieCte"",
                       l1.NRO_CTE as ""NumeroCte"",
                       l1.SITUACAO_ATUAL as ""SituacaoCte"",
                       l1.Nf_Ser_Nf as ""SerieNota"", 
                       l1.nf_nro_nf as ""NumeroNota"", 
                       l1.nf_peso_nf as ""PesoRateio"",
                       l1.nf_peso_tot_nf as ""PesoTotalNf"",
                       l1.peso_vagao as ""PesoVagao""
                  FROM mdfe_composicao mx
                  LEFT JOIN (SELECT                    
                                    cd.ID_cTE,
                                    C.SERIE_CTE,
                                    C.NRO_CTE,
                                    C.SITUACAO_ATUAL,
                                    c.dp_id_dp,
                                    CD.Nf_Ser_Nf, 
                                    cd.nf_nro_nf, 
                                    cd.nf_peso_nf,
                                    cd.nf_peso_tot_nf,
                                    c.peso_vagao
                               FROM CTE C
                               JOIN CTE_DET CD ON CD.Id_Cte = C.ID_cTE
                               join mdfe_composicao mx1 on mx1.id_mdfe = :IdMdfe
                              WHERE C.ID_CTE = (SELECT MAX(cx.ID_CTE)
                                                  FROM CTE CX
                                                 WHERE CX.DP_ID_DP = mx1.DP_ID_DP
                                                 )) L1
                    ON mx.dp_id_dp = L1.dp_id_dp
                 where mx.id_mdfe = :IdMdfe
            ");

            parameters.Add(q => q.SetParameter("IdMdfe", idMdfe));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(DadosMdfeComposicaoDto)));

                IList<DadosMdfeComposicaoDto> retorno = query.List<DadosMdfeComposicaoDto>();

                var mdfeComposicao =
                    from x in
                        retorno.Select(
                            k =>
                            new
                                {
                                    k.Id,
                                    k.CodigoVagao,
                                    k.NumeroCte,
                                    k.Sequencia,
                                    k.SerieCte,
                                    k.IdCte,
                                    k.SituacaoCte,
                                    k.PesoVagao
                                }).Distinct()
                    select new MdfeComposicaoDto()
                        {
                            Id = x.Id,
                            CodigoVagao = x.CodigoVagao,
                            NumeroCte = x.NumeroCte,
                            Sequencia = x.Sequencia,
                            SerieCte = x.SerieCte,
                            IdCte = x.IdCte,
                            SituacaoCte = x.SituacaoCte,
                            PesoVagao = x.PesoVagao,
                            Notas =
                                retorno.Where(n => n.IdCte == x.IdCte && x.IdCte != 0)
                                       .Select(c => new MdfeComposicaoNotasDto()
                                           {
                                               NumeroNota = c.NumeroNota,
                                               SerieNota = c.SerieNota,
                                               PesoRateio = c.PesoRateio,
                                               PesoTotalNF = c.PesoTotalNf
                                           }).ToList()
                        };

                return mdfeComposicao.OrderBy(x => x.Sequencia).ToList();
            }
        }

        /// <summary>
        /// Carrega os detalhes da composicao por Mdfe para o Cte informado
        /// </summary>
        /// <param name="idCte">id do Cte a ser localizado as composicoes do Mdfe</param>
        /// <returns>resultado paginado</returns>
        public int ObterIdMdfeDaComposicaoPorCte(int idCte)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                SELECT DISTINCT MC.id_mdfe
                  FROM CTE C
                  JOIN CTE_DET CD ON CD.Id_Cte = C.ID_cTE
                  JOIN MDFE_COMPOSICAO MC ON MC.DP_ID_DP = C.DP_ID_DP
                  JOIN MDFE M ON M.ID_MDFE = MC.ID_MDFE
                 WHERE C.ID_CTE = (SELECT MAX(cx.ID_CTE)
                                     FROM CTE_ARVORE CX
                                     JOIN CTE_ARVORE CA ON CA.ID_CTE_RAIZ = CX.ID_CTE_RAIZ
                                    WHERE CA.ID_CTE = :idCte)
                  AND M.SITUACAO_ATUAL IN ('PAE','PME')
            ");

            parameters.Add(q => q.SetParameter("idCte", idCte));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                var retorno = query.UniqueResult<decimal>();

                return Convert.ToInt32(retorno);
            }
        }

        /// <summary>
        /// Carrega os detalhes da composicao por Mdfe para o Cte informado
        /// </summary>
        /// <param name="idCte">id do Cte a ser localizado as composicoes do Mdfe</param>
        /// <returns>resultado paginado</returns>
        public int ObterIdMdfeAutDaComposicaoPorCte(int idCte)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
              SELECT distinct MC.id_mdfe
                  FROM CTE C
                  JOIN CTE_DET CD ON CD.Id_Cte = C.ID_cTE
                  JOIN MDFE_COMPOSICAO MC ON MC.DP_ID_DP = C.DP_ID_DP
                  JOIN MDFE M ON M.ID_MDFE = MC.ID_MDFE
                 WHERE C.ID_CTE = (SELECT MAX(cx.ID_CTE)
                                     FROM CTE_ARVORE CX
                                     JOIN CTE_ARVORE CA ON CA.ID_CTE_RAIZ = CX.ID_CTE_RAIZ
                                    WHERE CA.ID_CTE = :idCte)
                  AND M.SITUACAO_ATUAL IN ('AUT','ENC')
                  AND MC.SEQUENCIA = (SELECT MAX(MC.SEQUENCIA) 
                                      FROM CTE C
                                      JOIN CTE_DET CD ON CD.Id_Cte = C.ID_cTE
                                      JOIN MDFE_COMPOSICAO MC ON MC.DP_ID_DP = C.DP_ID_DP
                                      JOIN MDFE M ON M.ID_MDFE = MC.ID_MDFE
                                      WHERE C.ID_CTE = (SELECT MAX(cx.ID_CTE)
                                                         FROM CTE_ARVORE CX
                                                         JOIN CTE_ARVORE CA ON CA.ID_CTE_RAIZ = CX.ID_CTE_RAIZ
                                                        WHERE CA.ID_CTE = :idCte)
                                      AND M.SITUACAO_ATUAL IN ('AUT','ENC'))
                ORDER BY id_mdfe desc");

            parameters.Add(q => q.SetParameter("idCte", idCte));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }
                // var retorno = query. UniqueResult<decimal>();
                var retorno = query.List<decimal>().Distinct().ToList().First();

                return Convert.ToInt32(retorno);
            }
        }

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idEmpresa">Id da empresa recebedora</param>
        /// <param name="dataInicial">Data inicial do período</param>
        /// <param name="dataFinal">Data final do período</param>
        /// <param name="estacao">Código da estação</param>
        /// <param name="idLinha">Id da Linha</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> ListarDetalhesDaComposicaoPorPatio(int idEmpresa, DateTime dataInicial, DateTime dataFinal, string estacao, int? idLinha)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                            SELECT  
                                  0 as ""Id"", 
                                  VPV.VP_COD_VAG as ""CodigoVagao"",
                                  c.vg_id_vg as ""IdVagao"",
                                  SV.SV_COD_SER as ""SerieVagao"",
                                  VPV.VP_NUM_SEQ as ""Sequencia"",                  
                                  cd.ID_CTE  as ""IdCte"",
                                  c.SERIE_CTE as ""SerieCte"",
                                  c.NRO_CTE as ""NumeroCte"",
                                  c.SITUACAO_ATUAL as ""SituacaoCte"",
                                  cd.Nf_Ser_Nf as ""SerieNota"", 
                                  cd.nf_nro_nf as ""NumeroNota"", 
                                  cd.nf_peso_nf as ""PesoRateio"",
                                  cd.nf_peso_tot_nf as ""PesoTotalNf"",
                                  c.peso_vagao as ""PesoVagao"",
                                  MC. Mc_Ddt_Pt as ""Mercadoria"",
                                  VNF_PROD.XPROD as ""ProdutoNfe"",
                                  AO.AO_COD_AOP as ""LocalCarregamento"",
                                  CG.CG_DTP_CRG as ""DataCarregamento"",
                                  REM.EP_DSC_RSM as ""EmpresaOrigem"",
                                  cd.nf_dat_nf as ""DataNfe"",
                                  DEST.EP_DSC_RSM as ""EmpresaDestino""
                            FROM CTE C
                                  JOIN VAGAO V ON V.VG_ID_VG = C.VG_ID_VG
                                  JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = V.SV_ID_SV
                                  JOIN CTE_DET CD ON CD.Id_Cte = C.ID_cTE
                                  JOIN CTE_EMPRESAS CE ON CE.ID_cTE = c.id_cte
                                  JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = C.FX_ID_FLX
                                  JOIN MERCADORIA MC ON MC.MC_ID_MRC = FC.MC_ID_MRC
                                  JOIN ITEM_DESPACHO ITD ON ITD.DP_ID_DP = C.DP_ID_DP
                                  JOIN DETALHE_CARREGAMENTO DCG ON DCG.DC_ID_CRG = ITD.DC_ID_CRG
                                  JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = DCG.CG_ID_CAR
                                  JOIN VAGAO_PATIO_VIG VPV ON VPV.VG_ID_VG = C.VG_ID_VG
                                  JOIN DESPACHO DSP ON DSP.DP_ID_DP = C.DP_ID_DP
                                  JOIN AREA_OPERACIONAL AO ON AO.AO_ID_AO = VPV.AO_ID_AO
                                  JOIN EMPRESA REM ON REM.EP_ID_EMP = ce.ep_id_emp_rem
                                  JOIN EMPRESA DEST ON DEST.EP_ID_EMP = ce.ep_id_emp_dest
                                  LEFT JOIN AREA_OPERACIONAL AOM ON AOM.AO_ID_AO = AO.AO_ID_AO_INF
                                  LEFT JOIN VW_NFE VNFE ON  VNFE.NFE_CHAVE =  cd.nfe_chave_nfe
                                  LEFT JOIN VW_NFE_PROD VNF_PROD ON VNF_PROD.NFE_ID_NFE = VNFE.NFE_ID_NFE
                            WHERE CE.ep_id_emp_rec = :idEmpresa
                                  AND (VPV.EV_ID_ELV = :linha OR :linha IS NULL)
                                  AND (AOM.AO_COD_AOP = :estacao OR :estacao IS NULL)
                                  AND VPV.VP_CHG_MAE BETWEEN :dataInicial AND :dataFinal
                                  AND C.ID_CTE IN (SELECT cx.ID_CTE
                                                  FROM CTE CX
                                                        INNER JOIN DESPACHO DX ON CX.DP_ID_DP = DX.DP_ID_DP
                                                        INNER JOIN VAGAO_PEDIDO_VIG VPX ON VPX.VG_ID_VG = CX.VG_ID_VG AND DX.PT_ID_ORT = VPX.PT_ID_ORT
                                                 WHERE CX.Vg_Id_Vg = c.Vg_Id_Vg and CX.data_cte BETWEEN :dataInicial AND :dataFinal
                                                 ) 
            ");

            parameters.Add(q => q.SetParameter("idEmpresa", idEmpresa));
            parameters.Add(q => q.SetParameter("linha", idLinha));
            parameters.Add(q => q.SetString("estacao", String.IsNullOrWhiteSpace(estacao) ? null : estacao));
            parameters.Add(q => q.SetDateTime("dataInicial", dataInicial));
            parameters.Add(q => q.SetDateTime("dataFinal", dataFinal));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(DadosMdfeComposicaoDto)));

                IList<DadosMdfeComposicaoDto> retorno = query.List<DadosMdfeComposicaoDto>();

                var mdfeComposicao =
                    retorno.GroupBy(
                        k =>
                        new
                        {
                            k.Id,
                            k.CodigoVagao,
                            k.SerieVagao,
                            k.IdVagao,
                            k.NumeroCte,
                            k.IdCte,
                            k.Sequencia,
                            k.SerieCte,
                            k.SituacaoCte,
                            k.PesoVagao,
                            k.Mercadoria,
                            k.DataCarregamento,
                            k.LocalCarregamento,
                            k.EmpresaOrigem,
                            k.EmpresaDestino,
                        }).Select(x =>
                    new MdfeComposicaoDto()
                    {
                        Id = x.Key.Id,
                        CodigoVagao = x.Key.CodigoVagao,
                        IdVagao = x.Key.IdVagao,
                        SerieVagao = x.Key.SerieVagao,
                        NumeroCte = x.Key.NumeroCte,
                        Sequencia = x.Key.Sequencia,
                        SerieCte = x.Key.SerieCte,
                        IdCte = x.Key.IdCte,
                        SituacaoCte = x.Key.SituacaoCte,
                        PesoVagao = x.Key.PesoVagao,
                        Mercadoria = x.Key.Mercadoria,
                        DataCarregamento = x.Key.DataCarregamento,
                        LocalCarregamento = x.Key.LocalCarregamento,
                        EmpresaOrigem = x.Key.EmpresaOrigem,
                        EmpresaDestino = x.Key.EmpresaDestino,
                        Notas = x.Select(c =>
                                            new MdfeComposicaoNotasDto
                                                        {
                                                            Produto = c.ProdutoNfe,
                                                            NumeroNota = c.NumeroNota,
                                                            SerieNota = c.SerieNota,
                                                            PesoRateio = c.PesoRateio,
                                                            PesoTotalNF = c.PesoTotalNf,
                                                            DataNfe = c.DataNfe
                                                        }).Distinct().ToList()
                    });

                return mdfeComposicao.OrderBy(x => x.Sequencia).ToList();
            }
        }
        
        /// <summary>
        /// Carrega os detalhes da última composição do trem
        /// </summary>
        /// <param name="idTrem">id do Trem</param>
        /// <param name="idEmpresa">Id da empresa</param>
        /// <param name="vagoes">Lista de vagões para filtro de impressão</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeComposicaoDto> DetalhesUltComposicaoTrem(int idTrem, int idEmpresa, List<int> vagoes)
        {
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder();
            sql.AppendFormat(
@"
WITH TAB_DS_TEMP
AS 
(
     SELECT MAX(EV0.EA_DTO_EVG) AS DT_EVENTO,
           CP0.CP_DAT_INC AS DT_INI_COMP,
           CP0.CP_DAT_FIM,
           CV0.VG_ID_VG AS ID_VAGAO
      FROM COMPVAGAO CV0, COMPOSICAO CP0, EVENTO_VAGAO EV0
     WHERE CV0.CP_ID_CPS = (SELECT MAX(CY.CP_ID_CPS)
                           FROM COMPOSICAO CY
                           WHERE CY.TR_ID_TRM = :idTrem)
       AND CV0.CP_ID_CPS = CP0.CP_ID_CPS
       AND EV0.VG_ID_VG = CV0.VG_ID_VG
       AND EV0.EA_DTO_EVG < CP0.CP_DAT_INC
       AND EV0.EN_ID_EVT = 13
     GROUP BY CP0.CP_DAT_INC, CV0.VG_ID_VG,CP0.CP_DAT_FIM
)

SELECT
     T.TR_ID_TRM as ""Id"", 
     V.VG_COD_VAG as ""CodigoVagao"",
     SV.SV_COD_SER as ""SerieVagao"",
     V.VG_ID_VG as ""IdVagao"",
     CV.CV_SEQ_CMP as ""Sequencia"",                  
     CT.ID_CTE  as ""IdCte"",
     CT.SERIE_CTE as ""SerieCte"",
     CT.NRO_CTE as ""NumeroCte"",
     CT.SITUACAO_ATUAL as ""SituacaoCte"",
     CD.NF_SER_NF as ""SerieNota"", 
     CD.NF_DAT_NF as ""DataNfe"",
     CD.NF_NRO_NF as ""NumeroNota"", 
     CD.NF_PESO_NF as ""PesoRateio"",
     CD.NF_PESO_TOT_NF as ""PesoTotalNf"",
     CT.PESO_VAGAO as ""PesoVagao"",
     MC.MC_DDT_PT as ""Mercadoria"",
     VNF_PROD.XPROD as ""ProdutoNfe"",     
     REM.EP_DSC_RSM as ""EmpresaOrigem"",
     DEST.EP_DSC_RSM as ""EmpresaDestino"",
     AO.AO_COD_AOP as ""LocalCarregamento"",
     CG.CG_DTP_CRG as ""DataCarregamento""
FROM TREM T,
     COMPOSICAO C,
     COMPVAGAO CV,
     VAGAO V,
     SERIE_VAGAO SV,
     ITEM_DESPACHO IDS,
     DESPACHO DS,
     CTE CT,
     CTE_DET CD,
     CTE_EMPRESAS CE
     ,TAB_DS_TEMP
     ,FLUXO_COMERCIAL FC
     ,EMPRESA REM
     ,EMPRESA DEST
     ,DETALHE_CARREGAMENTO DCG
     ,CARREGAMENTO CG
     ,AREA_OPERACIONAL AO                               
     ,MERCADORIA MC    
     ,VW_NFE VNFE
     ,VW_NFE_PROD VNF_PROD
WHERE T.TR_ID_TRM = :idTrem
      AND T.TR_ID_TRM = C.TR_ID_TRM
      AND CV.CP_ID_CPS = C.CP_ID_CPS
      {0}
      AND V.VG_ID_VG = CV.VG_ID_VG
      AND SV.SV_ID_SV = V.SV_ID_SV
          AND IDS.VG_ID_VG = TAB_DS_TEMP.ID_VAGAO
          AND IDS.VG_ID_VG = CV.VG_ID_VG
          AND DS.DP_ID_DP = IDS.DP_ID_DP 
          AND DS.DP_DT BETWEEN TAB_DS_TEMP.DT_EVENTO AND TAB_DS_TEMP.DT_INI_COMP
          
          AND DCG.DC_ID_CRG = IDS.DC_ID_CRG
          AND CG.CG_ID_CAR = DCG.CG_ID_CAR
          AND AO.AO_ID_AO = CG.AO_ID_AO
      AND CT.VG_ID_VG = CV.VG_ID_VG
      AND CT.DP_ID_DP = DS.DP_ID_DP
      AND CD.ID_CTE = CT.ID_CTE
      AND CE.ID_CTE = CT.ID_CTE
      AND REM.EP_ID_EMP = CE.EP_ID_EMP_REM
      AND DEST.EP_ID_EMP = CE.EP_ID_EMP_DEST
      AND FC.FX_ID_FLX = CT.FX_ID_FLX
      AND MC.MC_ID_MRC = FC.MC_ID_MRC
      AND VNFE.NFE_CHAVE(+) = CD.NFE_CHAVE_NFE
      AND VNF_PROD.NFE_ID_NFE(+) = VNFE.NFE_ID_NFE
      AND VNF_PROD.TIPO(+) = VNFE.TIPO
      AND CE.EP_ID_EMP_REC = :idEmpresa
      AND CT.ID_CTE = (SELECT MAX(CX.ID_CTE)
                       FROM CTE CX
                       WHERE CX.DP_ID_DP = DS.DP_ID_DP)
      AND C.CP_ID_CPS = (SELECT MAX(CY.CP_ID_CPS)
                         FROM COMPOSICAO CY,
                              COMPVAGAO CVY                           
                         WHERE 
                         CY.CP_ID_CPS = CVY.CP_ID_CPS                        
                         AND CY.TR_ID_TRM = T.TR_ID_TRM)                  
            ", vagoes != null && vagoes.Any() ? "AND CT.VG_ID_VG IN (:idVagoes)" : string.Empty);

            parameters.Add(q => q.SetParameter("idTrem", idTrem));
            parameters.Add(q => q.SetParameter("idEmpresa", idEmpresa));
            if (vagoes != null && vagoes.Any())
            {
                parameters.Add(q => q.SetParameterList("idVagoes", vagoes));
            }

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(DadosMdfeComposicaoDto)));

                IList<DadosMdfeComposicaoDto> retorno = query.List<DadosMdfeComposicaoDto>().Distinct().ToList();

                var mdfeComposicao =
                    from x in
                        retorno.GroupBy(
                            k =>
                            new
                            {
                                k.Id,
                                k.IdVagao,
                                k.CodigoVagao,
                                k.NumeroCte,
                                k.Sequencia,
                                k.SerieCte,
                                k.IdCte,
                                k.SituacaoCte,
                                k.PesoVagao,
                                k.EmpresaOrigem,
                                k.EmpresaDestino,
                                k.Mercadoria,
                                k.LocalCarregamento,
                                k.DataCarregamento,
                                k.SerieVagao
                            })
                    select new MdfeComposicaoDto()
                    {
                        Id = x.Key.Id,
                        IdVagao = x.Key.IdVagao,
                        CodigoVagao = x.Key.CodigoVagao,
                        SerieVagao = x.Key.SerieVagao,
                        NumeroCte = x.Key.NumeroCte,
                        Sequencia = x.Key.Sequencia,
                        SerieCte = x.Key.SerieCte,
                        IdCte = x.Key.IdCte,
                        SituacaoCte = x.Key.SituacaoCte,
                        PesoVagao = x.Key.PesoVagao,
                        Mercadoria = x.Key.Mercadoria,
                        EmpresaOrigem = x.Key.EmpresaOrigem,
                        EmpresaDestino = x.Key.EmpresaDestino,
                        DataCarregamento = x.Key.DataCarregamento,
                        LocalCarregamento = x.Key.LocalCarregamento,
                        Notas = x.Select(c => new
                                        {
                                            c.ProdutoNfe,
                                            c.NumeroNota,
                                            c.SerieNota,
                                            c.PesoRateio,
                                            c.PesoTotalNf,
                                            c.DataNfe
                                        })
                                        .Distinct()
                                        .Select(c => new MdfeComposicaoNotasDto()
                                        {
                                            Produto = c.ProdutoNfe,
                                            NumeroNota = c.NumeroNota,
                                            SerieNota = c.SerieNota,
                                            PesoRateio = c.PesoRateio,
                                            PesoTotalNF = c.PesoTotalNf,
                                            DataNfe = c.DataNfe
                                        })
                                        .ToList()
                    };

                return mdfeComposicao.OrderBy(x => x.Sequencia).ToList();
            }
        }

        /// <summary>
        /// Carrega os fluxos da composicao
        /// </summary>
        /// <param name="dataInicial">Data inicial para filtro</param>
        /// <param name="dataFinal">Data final para filtro</param>
        /// <param name="codigoEstacao">codigo da estação</param>
        /// <param name="linha">id do trem</param>
        /// <returns>resultado paginado</returns>
        public IList<FluxosMdfeDto> ListarFluxosPorPatio(DateTime dataInicial, DateTime dataFinal, string codigoEstacao, int? linha)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@" 
            SELECT DISTINCT receb.ep_dsc_dtl as ""EmpresaRecebedora"",
                            receb.ep_id_emp as ""IdEmpresa"",
                            receb.ep_cgc_emp as ""CnpjRecebedora""                                                     
                       FROM CTE C 
                       join VAGAO_PEDIDO_VIG VPV ON VPV.VG_ID_VG = C.VG_ID_VG
                       JOIN DETALHE_CARREGAMENTO DC ON DC.CG_ID_CAR = VPV.CG_ID_CAR
                       join ITEM_DESPACHO ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG AND ITD.DP_ID_DP = C.DP_ID_DP
                       join DESPACHO DSP ON DSP.DP_ID_DP = C.DP_ID_DP
                       join cte_empresas ce on ce.id_cte = c.id_cte
                       join empresa rem on rem.ep_id_emp = ce.ep_id_emp_rem
                       join empresa receb on receb.ep_id_emp = ce.ep_id_emp_rec
                       join vagao_patio_vig vptv on vptv.vg_id_vg = c.vg_id_vg
                       join elemento_via ev on ev.ev_id_elv = vptv.ev_id_elv
                       JOIN AREA_OPERACIONAL AO ON AO.AO_ID_AO = EV.AO_ID_AO
                       JOIN AREA_OPERACIONAL AOM ON AO.AO_ID_AO_INF = AOM.AO_ID_AO
                       WHERE (ev.ev_id_elv = :linha OR :linha IS NULL)
                            AND (AOM.AO_COD_AOP = :estacao OR :estacao IS NULL)
                            AND vptv.VP_CHG_MAE BETWEEN :dataInicial AND :dataFinal
                            AND C.ID_CTE = (SELECT MAX(cx.ID_CTE)
                                                  FROM CTE CX
                                                 WHERE CX.Vg_Id_Vg = c.Vg_Id_Vg and CX.data_cte BETWEEN :dataInicial AND :dataFinal
                                                 ) ");

            parameters.Add(q => q.SetParameter("linha", linha));
            parameters.Add(q => q.SetString("estacao", String.IsNullOrWhiteSpace(codigoEstacao) ? null : codigoEstacao));
            parameters.Add(q => q.SetDateTime("dataInicial", dataInicial));
            parameters.Add(q => q.SetDateTime("dataFinal", dataFinal));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(FluxosMdfeDto)));

                return query.List<FluxosMdfeDto>();
            }
        }

        /// <summary>
        /// Carrega os fluxos da composicao
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado</returns>
        public IList<FluxosMdfeDto> ListarFluxosDaComposicao(int idTrem)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
               SELECT DISTINCT receb.ep_dsc_dtl as ""EmpresaRecebedora"",
                               m.id_mdfe as ""IdMdfe"",
                               receb.ep_id_emp as ""IdEmpresa"",
                               receb.ep_cgc_emp as ""CnpjRecebedora""
                          FROM MDFE M
                          join mdfe_composicao mx1 ON mx1.ID_MDFE = M.ID_MDFE 
                          join CTE C ON C.ID_cTE = MX1.ID_CTE
                          join cte_empresas ce on ce.id_cte = c.id_cte
                          join empresa rem on rem.ep_id_emp = ce.ep_id_emp_rem
                          join empresa receb on receb.ep_id_emp = ce.ep_id_emp_rec
                          WHERE C.ID_CTE =
                               (SELECT MAX(cx.ID_CTE)
                                  FROM CTE CX
                                 WHERE CX.DP_ID_DP = mx1.DP_ID_DP)
                           AND M.SITUACAO_ATUAL = 'AUT'
                           AND M.tr_id_trm = :idTrem
                ");

            parameters.Add(q => q.SetParameter("idTrem", idTrem));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(FluxosMdfeDto)));

                return query.List<FluxosMdfeDto>();
            }
        }

        /// <summary>
        /// Busca a UF dos CTES da composição do MDFE
        /// </summary>
        /// <param name="idMdfe">ID MDFE</param>
        /// <returns>UF retornada</returns>
        public string BuscarUfDosCteComposicaoMdfe(int idMdfe, decimal idTrem)
        {
            var parameters = new List<Action<IQuery>>();
            var hql = new StringBuilder(@"  SELECT MAX(AUF.SIGLA_UF) as ""SiglaUf""
                FROM MDFE_ARVORE MA, MDFE_ARVORE_UF AUF, MDFE M
                WHERE MA.ID_MDFE = :idMdfe
                AND AUF.ID_MDFE_ARVORE = MA.ID_MDFE_ARVORE
                AND M.id_mdfe = MA.id_mdfe
                AND M.tr_id_trm = :idTrem");

            parameters.Add(q => q.SetParameter("idMdfe", idMdfe));
            parameters.Add(q => q.SetParameter("idTrem", idTrem));

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(hql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                return query.UniqueResult<string>();
            }
        }
    }
}
