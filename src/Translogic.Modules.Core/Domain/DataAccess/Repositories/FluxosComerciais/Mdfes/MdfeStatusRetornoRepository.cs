﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// Repositório da classe de status de retorno
	/// </summary>
	public class MdfeStatusRetornoRepository : NHRepository<MdfeStatusRetorno, int?>, IMdfeStatusRetornoRepository 
	{
	}
}