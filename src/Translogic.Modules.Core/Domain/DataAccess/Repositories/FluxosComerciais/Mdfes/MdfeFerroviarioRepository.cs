﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using Model.WebServices.MdfeFerroviario;
    using Model.WebServices.MdfeFerroviario.Outputs;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Classe de acesso aos dados de mdfe 
    /// </summary>
    public class MdfeFerroviarioRepository : NHRepository, IMdfeFerroviarioRepository
    {
        /// <summary>
        /// Consulta Dados Mdfe para o ws
        /// </summary>
        /// <param name="request">Objeto de input EletronicManifestInformationRailRequest</param>
        /// <returns>Lista de detalhes dos mdfes</returns>
        public List<EletronicManifestInformationRailOutputDetail> InformationMdfe(EletronicManifestInformationRailRequest request)
        {
            var maior30Dias = (request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail.EndDate - request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();
            sb.AppendFormat(
                @"  
                    SELECT 
                           M.ID_MDFE     AS ""EletronicManifestId"",
                           M.CHAVE_MDFE  AS ""EletronicManifestKey"",
                           C.CP_DAT_FIM  AS ""DateTimeOutputTrainComposition""
                    FROM 
                           MDFE          M,
                           COMPOSICAO    C
                    WHERE
                           C.CP_ID_CPS = M.CP_ID_CPS 
                           AND C.CP_DAT_FIM BETWEEN to_date('{0:dd/MM/yyyy HH:mm:ss}','dd/mm/yyyy hh24:mi:ss') 
                                                AND to_date('{1:dd/MM/yyyy HH:mm:ss}','dd/mm/yyyy hh24:mi:ss')",
                        request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail.StartDate,
                        maior30Dias ? request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail.StartDate.AddDays(31) : request.InformationEletronicManifestInformationRail.InformationEletronicManifestInformationRailType.EletronicManifestInformationRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<EletronicManifestInformationRailOutputDetail>());

            var result = query.List<EletronicManifestInformationRailOutputDetail>();

            return result.ToList();
        }
    }
}