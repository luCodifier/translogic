﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe de MDF-e status
	/// </summary>
	public class MdfeStatusRepository : NHRepository<MdfeStatus, int?>, IMdfeStatusRepository 
	{
		/// <summary>
		/// Insere o status do Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe de referencia</param>
		/// <param name="usuario">usuario de inserção</param>
		/// <param name="mdfeStatusRetorno">Status de retorno do mdfe</param>
		public void InserirStatus(Mdfe mdfe, Usuario usuario, MdfeStatusRetorno mdfeStatusRetorno)
		{
			InserirStatus(mdfe, usuario, mdfeStatusRetorno, string.Empty);
		}

		/// <summary>
		/// Insere o status do Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe de referencia</param>
		/// <param name="usuario">usuario de inserção</param>
		/// <param name="mdfeStatusRetorno">Status de retorno do mdfe</param>
		/// <param name="xmlRetorno">Xml de Retorno</param>
		public void InserirStatus(Mdfe mdfe, Usuario usuario, MdfeStatusRetorno mdfeStatusRetorno, string xmlRetorno)
		{
			MdfeStatus mdfeStatus = new MdfeStatus();
			mdfeStatus.Mdfe = mdfe;
			mdfeStatus.Usuario = usuario;
			mdfeStatus.MdfeStatusRetorno = mdfeStatusRetorno;
			mdfeStatus.DataHora = DateTime.Now;
			if (!string.IsNullOrEmpty(xmlRetorno))
			{
				mdfeStatus.XmlRetorno = xmlRetorno;
			}

			Inserir(mdfeStatus);
		}

		/// <summary>
		/// Obtém os status do Mdfe pelo Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe para obter o status</param>
		/// <returns>Retorna uma lista com os status do CTE</returns>
		public IList<MdfeStatus> ObterPorMdfe(Mdfe mdfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Mdfe", mdfe));
			criteria.AddOrder(new Order("DataHora", true));
			return ObterTodos(criteria);
		}
	}
}