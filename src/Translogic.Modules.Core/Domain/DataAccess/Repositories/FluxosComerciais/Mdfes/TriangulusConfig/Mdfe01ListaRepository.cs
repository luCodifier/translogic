namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Repositório da classe MDF-e Lista.
	/// </summary>
	public class Mdfe01ListaRepository : NHRepository<Mdfe01Lista, int>, IMdfe01ListaRepository
	{
		/// <summary>
		/// Obtém o item da fila de processamento por filial, numero mdfe e serie mdfe
		/// </summary>
		/// <param name="idFilial">Identificador da filial</param>
		/// <param name="numeroCte">Numero do mdfe</param>
		/// <param name="serieCte">Serie do mdfe</param>
		/// <param name="tipo">Tipo da acao na lista</param>
		/// <param name="ascendente">Tipo da classificação ascendente</param>
		/// <returns>Retorna o objeto da fila de processamento</returns>
		public Mdfe01Lista ObterListaFilaProcessamento(int idFilial, int numeroCte, string serieCte, int tipo, bool ascendente)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdFilial", idFilial));
			criteria.Add(Restrictions.Eq("Numero", numeroCte));
			criteria.Add(Restrictions.Eq("Serie", serieCte));
			criteria.Add(Restrictions.Eq("Tipo", tipo));

			criteria.AddOrder(new Order("Id", ascendente));
			return ObterPrimeiro(criteria);
		}
	}
}