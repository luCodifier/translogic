namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using NHibernate;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Repositório da classe Mdfe03InfPercurso.
	/// </summary>
	public class Mdfe03InfPercursoRepository : NHRepository<Mdfe03InfPercurso, int?>, IMdfe03InfPercursoRepository
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do mdfe</param>
		/// <param name="serie"> Serie do mdfe</param>
		public void Remover(int idFilial, int numero, string serie)
		{
			string hql = @"DELETE FROM  Mdfe03InfPercurso WHERE CfgDoc = :CFG_DOC AND CfgSerie = :CFG_SERIE AND CfgUn = :CFG_UN";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("CFG_DOC", numero.ToString());
				query.SetString("CFG_SERIE", serie);
				query.SetString("CFG_UN", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}
	}
}