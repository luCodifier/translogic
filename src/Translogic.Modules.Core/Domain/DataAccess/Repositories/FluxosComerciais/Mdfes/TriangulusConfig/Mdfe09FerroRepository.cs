namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using NHibernate;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Repositório da classe Mdfe09Ferro.
	/// </summary>
	public class Mdfe09FerroRepository : NHRepository<Mdfe09Ferro, int?>, IMdfe09FerroRepository
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do mdfe</param>
		/// <param name="serie"> Serie do mdfe</param>
		public void Remover(int idFilial, int numero, string serie)
		{
			string hql = @"DELETE FROM  Mdfe09Ferro WHERE CfgDoc = :CFG_DOC AND CfgSerie = :CFG_SERIE AND CfgUn = :CFG_UN";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetString("CFG_DOC", numero.ToString());
				query.SetString("CFG_SERIE", serie);
				query.SetString("CFG_UN", idFilial.ToString());
				query.ExecuteUpdate();
			}
		}
	}
}