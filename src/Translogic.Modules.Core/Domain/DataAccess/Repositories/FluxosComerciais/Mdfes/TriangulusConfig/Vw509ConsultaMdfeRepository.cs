namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Reposit�rio da classe Vw509ConsultaMdfeRepository
	/// </summary>
	public class Vw509ConsultaMdfeRepository : NHRepository<Vw509ConsultaMdfe, int>, IVw509ConsultaMdfeRepository
	{
		/// <summary>
		/// Obtem o retorno do processamento pelo id da lista de fila de processamento
		/// </summary>
		/// <param name="idLista">Identificador da lista de processamento do Cte</param>
		/// <returns>Retorna o CteProcessado</returns>
		public Vw509ConsultaMdfe ObterRetornoPorIdLista(int idLista)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdLista", idLista));
			return ObterPrimeiro(criteria);
		}

        /// <summary>
        /// Obt�m o retorno do processamento 
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o mdfe processado</returns>
	    public Vw509ConsultaMdfe ObterMdfeJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie)
	    {
            string hql = @"FROM Vw509ConsultaMdfe WHERE Stat = 100 AND Numero = :CFG_DOC AND Serie = :CFG_SERIE AND IdFilial = :CFG_UN";

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);

                query.SetInt32("CFG_DOC", numero);
                query.SetInt32("CFG_SERIE", serie);
                query.SetInt32("CFG_UN", idFilial);

                return query.UniqueResult<Vw509ConsultaMdfe>();
            }
	    }
	}
}