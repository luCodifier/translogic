﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using ALL.Core.Dominio;
    using CustomType;
    using Model.Diversos.Mdfe;
    using Model.FluxosComerciais.Mdfes;
    using Model.FluxosComerciais.Mdfes.Repositories;
    using NHibernate;
    using NHibernate.Linq;
    using NHibernate.Transform;

    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem;
    using System.Globalization;

    /// <summary>
    /// Interface do repositório da classe MDF-e
    /// </summary>
    public class MdfeRepository : NHRepository<Mdfe, int?>, IMdfeRepository
    {
        /// <summary>
        /// Fabrica de sessões injetadas
        /// </summary>
        public ISessionFactory SessionFactory { get; set; }

        /// <summary>
        /// Obtém MDF-e por status de processamento
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual o ambiente Sefaz</param>
        /// <returns>Retorna uma lista com os MDF-es</returns>
        public IList<Mdfe> ObterStatusProcessamentoEnvio(int indAmbienteSefaz)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"  FROM Mdfe m
						 INNER JOIN FETCH m.Versao v							   
						 LEFT  JOIN FETCH m.Composicao c
						 LEFT  JOIN FETCH m.OrdemServico os
						 INNER JOIN FETCH m.Origem ori						 
						 LEFT  JOIN FETCH m.Trem t					
									WHERE
                                      m.SituacaoAtual IN (:situacaoPendente, :situacaoCancelamento, :situacaoEncerramento)
									  AND NOT EXISTS(SELECT mep.Id FROM MdfeEnvioPooling mep WHERE m.Id = mep.Id)
									  AND m.AmbienteSefaz = :indAmbienteSefaz

                                      AND (
                                        (EXISTS (SELECT 1 FROM CteStatus css, MdfeComposicao mc
                                                 WHERE mc.Mdfe.Id = m.Id
                                                 AND   mc.Cte.Id = css.Cte.Id
                                                 AND   css.Usuario.Id = 62382
                                                 AND   rownum=1)
                                           AND m.Trem.Id IS NULL
                                           AND m.Composicao.Id IS NULL
                                        )
                                      OR (m.Trem.Id IS NOT NULL AND m.Composicao.Id IS NOT NULL)
                                      )

								 ORDER BY m.Id";
                    
                    IQuery query = session.CreateQuery(hql);
                    query.SetParameter("situacaoPendente", SituacaoMdfeEnum.PendenteArquivoEnvio, new SituacaoMdfeEnumCustomType());
                    query.SetParameter("situacaoCancelamento", SituacaoMdfeEnum.AguardandoCancelamento, new SituacaoMdfeEnumCustomType());
                    query.SetParameter("situacaoEncerramento", SituacaoMdfeEnum.AguardandoEncerramento, new SituacaoMdfeEnumCustomType());
                    query.SetInt32("indAmbienteSefaz", indAmbienteSefaz);

                    query.SetMaxResults(50);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);

#if DEBUG2
                    session.ConvertToSql(query.QueryString);
#endif

                    return query.List<Mdfe>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Atualiza a situacao do MDF-e por Stateless
        /// </summary>
        /// <param name="mdfe">Mdfe para ser atualizado</param>
        public void AtualizarSituacao(Mdfe mdfe)
        {
            string hql = string.Empty;
            hql = @"UPDATE Mdfe SET SituacaoAtual = :situacaoAtual WHERE Id = :idMdfe";

            using (IStatelessSession session = SessionFactory.OpenStatelessSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idMdfe", mdfe.Id.Value);
                query.SetParameter("situacaoAtual", mdfe.SituacaoAtual, new SituacaoMdfeEnumCustomType());
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Retorna o ultimo mdfe vigente para o trem
        /// </summary>
        /// <param name="idTrem">Id do trem</param>
        /// <returns>Retorna o id do mdfe</returns>
        public int ObterUltimoMdfePorTrem(int idTrem)
        {
            string hql = string.Empty;

            hql = @" SELECT MAX(MAR.Id) FROM MdfeArvore MAR, 
								    MdfeArvore MAF,
                                    Mdfe M
								    WHERE 
								    MAR.MdfeRaiz.Id = MAF.MdfeRaiz.Id 
								    AND MAF.MdfeFilho.Id = M.Id
                                    AND M.Trem.Id = :IdTrem";

            using (IStatelessSession session = SessionFactory.OpenStatelessSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("IdTrem", idTrem);
                return query.UniqueResult<int>();
            }
        }

        /// <summary>
        /// Obtém a lista dos MDF-es que estão no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <param name="segundos">Timer em segundos</param>
        /// <returns>Lista dos MDF-e</returns>
        public IList<Mdfe> ObterListaEnvioPorHost(string hostName, int segundos)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"  FROM Mdfe m
						 INNER JOIN FETCH m.Versao v							   
						 LEFT  JOIN FETCH m.Composicao c
						 LEFT  JOIN FETCH m.OrdemServico os
						 INNER JOIN FETCH m.Origem ori
						 LEFT  JOIN FETCH m.MovimentacaoTrem mt
						 LEFT  JOIN FETCH m.Trem t					
									WHERE 
										m.SituacaoAtual IN (:situacaoEnviado, :situacaoCancelamento, :situacaoEncerramento)									
										AND EXISTS(SELECT mep.Id FROM MdfeEnvioPooling mep WHERE m.Id = mep.Id AND mep.HostName = :host)
                                        AND NOT EXISTS (SELECT 1 From Mdfe me, MdfeEnvioPooling mp WHERE m.Id = me.Id AND me.Id = mp.Id AND me.SituacaoAtual = :situacaoEncerramento and mp.DataHora > SYSDATE - :segundos/(24*60*60))

                                        AND (
                                          (EXISTS (SELECT 1 FROM CteStatus css, MdfeComposicao mc
                                                   WHERE mc.Mdfe.Id = m.Id
                                                 AND   mc.Cte.Id = css.Cte.Id
                                                 AND   css.Usuario.Id = 62382
                                                 AND   rownum=1)
                                             AND m.Trem.Id IS NULL
                                             AND m.Composicao.Id IS NULL
                                          )
                                        OR (m.Trem.Id IS NOT NULL AND m.Composicao.Id IS NOT NULL)
                                        )

								 ORDER BY m.Id";

                    /*string hql = @"FROM Mdfe m
                         INNER JOIN FETCH m.Versao v
                         INNER JOIN FETCH m.Composicao c
                         INNER JOIN FETCH m.OrdemServico os
                         INNER JOIN FETCH m.Origem ori
                         INNER JOIN FETCH m.MovimentacaoTrem mt
                         INNER JOIN FETCH m.Trem t
                    LEFT OUTER JOIN FETCH m.ListaComposicao lc
                                    WHERE 
                                    EXISTS(SELECT mep.Id FROM MdfeEnvioPooling mep WHERE m.Id = mep.Id AND mep.HostName = :host)
                                    AND m.SituacaoAtual IN (:situacaoEnviado, :situacaoCancelamento, :situacaoEncerramento)
                                    ";
                    */
                    
                    IQuery query = session.CreateQuery(hql);
                    query.SetString("host", hostName);
                    query.SetParameter("situacaoEnviado", SituacaoMdfeEnum.EnviadoFilaArquivoEnvio, new SituacaoMdfeEnumCustomType());
                    query.SetParameter("situacaoCancelamento", SituacaoMdfeEnum.EnviadoFilaArquivoCancelamento, new SituacaoMdfeEnumCustomType());
                    query.SetParameter("situacaoEncerramento", SituacaoMdfeEnum.EnviadoFilaArquivoEncerramento, new SituacaoMdfeEnumCustomType());
                    query.SetParameter("segundos", segundos);

                    query.SetMaxResults(500);

                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<Mdfe>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremMdfeDto> Listar(DetalhesPaginacao detalhesPaginacao, string chaveMdfe, string numero, string serie, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, int idTrem)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                select  
                    {0}
                from 
                    Mdfe m
				    INNER JOIN m.OrdemServico os				    
				    INNER JOIN m.Origem o
				    INNER JOIN m.Trem t
                where 
                   m.Id = 
						( 
						SELECT ma.MdfeFilho.Id 
							FROM MdfeArvore ma
							WHERE ma.Id = 
							( 
							SELECT MAX(MAR.Id) FROM MdfeArvore MAR, 
							MdfeArvore MAF
							WHERE 
							MAR.MdfeRaiz.Id = MAF.MdfeRaiz.Id 
							AND MAF.MdfeFilho.Id = m.Id
							) 
						) 
            ");

            if (idTrem > 0)
            {
                hql.AppendLine(" and t.Id = :idTrem ");
                parameters.Add(q => q.SetParameter("idTrem", idTrem));
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(chaveMdfe))
                {
                    hql.AppendLine(" and m.Chave = :chaveMdfe ");
                    parameters.Add(q => q.SetParameter("chaveMdfe", chaveMdfe));
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(numero))
                    {
                        hql.AppendLine(" and m.Numero = :numero ");
                        parameters.Add(q => q.SetParameter("numero", numero));
                    }

                    if (!string.IsNullOrWhiteSpace(serie))
                    {
                        hql.AppendLine(" and m.Serie = :serie ");
                        parameters.Add(q => q.SetParameter("serie", serie));
                    }

                    if (!string.IsNullOrWhiteSpace(prefixoTrem))
                    {
                        hql.AppendLine(" and m.PrefixoTrem = :prefixoTrem ");
                        parameters.Add(q => q.SetParameter("prefixoTrem", prefixoTrem));
                    }

                    if (os.HasValue)
                    {
                        hql.AppendLine(" and os.Numero = :os ");
                        parameters.Add(q => q.SetParameter("os", os));
                    }

                    if (dataInicial.HasValue)
                    {
                        hql.AppendLine(" and m.DataHora >= :dataInicial ");
                        parameters.Add(q => q.SetParameter("dataInicial", dataInicial));
                    }

                    if (dataFinal.HasValue)
                    {
                        hql.AppendLine(" and m.DataHora <= :dataFinal ");
                        parameters.Add(q => q.SetParameter("dataFinal", dataFinal));
                    }

                    if (!string.IsNullOrWhiteSpace(origem))
                    {
                        hql.AppendLine(" and t.Origem.Codigo = :origem ");
                        parameters.Add(q => q.SetParameter("origem", origem));
                    }

                    if (!string.IsNullOrWhiteSpace(destino))
                    {
                        hql.AppendLine(" and t.Destino.Codigo = :destino ");
                        parameters.Add(q => q.SetParameter("destino", destino));
                    }
                }
            }

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(string.Format(hql.ToString(), @"distinct t.Id as Id, t.Prefixo as Prefixo, t.Origem.Codigo as Origem, t.Destino.Codigo as Destino, t.OrdemServico.Numero as OS, m.SituacaoAtual as SituacaoAtual "));

                var queryCount = session.CreateQuery(string.Format(hql.ToString(), "count(distinct t.Id)"));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<TremMdfeDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<long>();
                var itens = query.List<TremMdfeDto>();

                var result = new ResultadoPaginado<TremMdfeDto>
                {
                    Items = itens,
                    Total = total
                };

                return result;
            }
        }

        /// <summary>
        /// Lista as Mdfes e os trens sem Mdfe
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public ResultadoPaginado<TremMdfeDto> ListarSemMdfe(DetalhesPaginacao detalhesPaginacao, string chaveMdfe, string numero, string serie, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao, int idTrem)
        {
            try
            {
                var parameters = new List<Action<IQuery>>();
                var hqlCondicoes = new StringBuilder();
                var hqlCondicoes2 = new StringBuilder();
                var hql = new StringBuilder(@"
                  SELECT DISTINCT T.TR_ID_TRM AS ""Id"", T.Tr_Pfx_Trm as ""Prefixo"", AO_ORIGEM.AO_COD_AOP as ""Origem"", AO_DESTINO.Ao_Cod_Aop as ""Destino"", os.x1_nro_os as ""OS"", M.SITUACAO_ATUAL as ""SituacaoAtual"", M.SITUACAO_ATUAL as ""Informativo""

                   ,(SELECT CONCAT(CONCAT(RET.CODIGO_STATUS_RETORNO, ' - '), RET.DESC_RETORNO) 
                    FROM MDFE_STATUS_RETORNO RET
                    WHERE RET.CODIGO_STATUS_RETORNO IN 
                    (SELECT CODIGO_STATUS_RETORNO FROM (
                            SELECT STAT.CODIGO_STATUS_RETORNO, STAT.ID_MDFE
                            FROM MDFE_STATUS STAT 
                            ORDER BY STAT.ID_MDFE_STATUS DESC) 
                      WHERE ID_MDFE = M.ID_MDFE
                      AND ROWNUM = 1)) AS ""UltimoEvento"",

                    (SELECT DATA_HORA
                        FROM (SELECT STAT.DATA_HORA, STAT.ID_MDFE
                           FROM MDFE_STATUS STAT
                          ORDER BY STAT.ID_MDFE_STATUS DESC)
                    WHERE ID_MDFE = M.ID_MDFE
                    AND ROWNUM = 1) AS ""DataEvento""

                  FROM MDFE M 
                  INNER JOIN T2_OS OS ON OS.X1_ID_OS = M.X1_ID_OS 
                  INNER JOIN TREM T ON T.TR_ID_TRM = M.Tr_Id_Trm
                  INNER JOIN AREA_OPERACIONAL AO_ORIGEM ON AO_ORIGEM.AO_ID_AO =  T.AO_ID_AO_ORG
                  INNER JOIN AREA_OPERACIONAL AO_DESTINO ON AO_DESTINO.AO_ID_AO =  T.AO_ID_AO_DST
                  WHERE 
                      M.ID_MDFE = 
                      ( 
                      SELECT MA.ID_MDFE 
                        FROM MDFE_ARVORE MA
                        WHERE MA.ID_MDFE_ARVORE = 
                        ( 
                          SELECT MAX(MAR.ID_MDFE_ARVORE) 
                          FROM MDFE_ARVORE MAR, MDFE_ARVORE MAF
                          WHERE 
                          MAR.ID_MDFE_RAIZ = MAF.ID_MDFE_RAIZ 
                          AND MAF.ID_MDFE = M.ID_MDFE
                        ) 
                      ) {0} {1}");

                var hqlUnion = new StringBuilder(@"UNION 

                  SELECT DISTINCT T.TR_ID_TRM AS ""Id"", T.Tr_Pfx_Trm as ""Prefixo"", AO_ORIGEM.AO_COD_AOP as ""Origem"", AO_DESTINO.Ao_Cod_Aop as ""Destino"", OS.x1_nro_os AS ""OS"", 'SEM' as ""SituacaoAtual"", 'SEM' as ""Informativo""

                  ,(SELECT CONCAT(CONCAT(RET.CODIGO_STATUS_RETORNO, ' - '), RET.DESC_RETORNO) 
                      FROM MDFE_STATUS_RETORNO RET
                      WHERE RET.CODIGO_STATUS_RETORNO IN 
                        (SELECT CODIGO_STATUS_RETORNO FROM (
                            SELECT STAT.CODIGO_STATUS_RETORNO, MD.TR_ID_TRM
                            FROM MDFE_STATUS STAT, MDFE MD
                            WHERE MD.ID_MDFE = STAT.ID_MDFE
                            ORDER BY STAT.ID_MDFE_STATUS DESC) 
                      WHERE TR_ID_TRM = T.TR_ID_TRM 
                  AND ROWNUM = 1)) AS ""UltimoEvento"",

                  (SELECT DATA_HORA
                  FROM (SELECT STAT.DATA_HORA, MD.TR_ID_TRM
                          FROM MDFE_STATUS STAT, MDFE MD
                         WHERE MD.ID_MDFE = STAT.ID_MDFE
                         ORDER BY STAT.ID_MDFE_STATUS DESC)
                 WHERE TR_ID_TRM = T.TR_ID_TRM
                   AND ROWNUM = 1) AS ""DataEvento""     

                  FROM compvagao cv 
                   JOIN vagao_pedido_vig vpv 
                      ON vpv.vg_id_vg = cv.vg_id_vg
                   JOIN carregamento cg
                      ON cg.cg_id_car = vpv.cg_id_car
                   JOIN detalhe_carregamento dc
                      ON dc.cg_id_car = cg.cg_id_car and dc.vg_id_vg = vpv.vg_id_vg
                   JOIN item_despacho idp
                      ON idp.dc_id_crg = dc.dc_id_crg
                   JOIN cte c
                      ON c.dp_id_dp = idp.dp_id_dp
                   JOIN fluxo_comercial fc
                      ON fc.fx_id_flx = C.fx_id_flx
                   JOIN AREA_OPERACIONAL AO_DESTINO 
                      ON FC.AO_ID_EST_DS = AO_DESTINO.AO_ID_AO
                   JOIN AREA_OPERACIONAL AO_ORIGEM
                      ON FC.AO_ID_EST_OR = AO_ORIGEM.AO_ID_AO
                   JOIN  MUNICIPIO MN 
                      ON MN.MN_ID_MNC = AO_ORIGEM.MN_ID_MNC
                   JOIN ESTADO ES
                      ON ES.ES_ID_EST = MN.ES_ID_EST              
                   JOIN COMPOSICAO COMP
                      ON COMP.CP_ID_CPS = CV.CP_ID_CPS
                   JOIN TREM T 
                      ON COMP.TR_ID_TRM = T.TR_ID_TRM 
                   JOIN T2_OS OS ON OS.x1_id_os = T.OF_ID_OSV                 
                   WHERE 
					  T.TR_ID_TRM NOT IN (SELECT M.TR_ID_TRM FROM MDFE M WHERE M.TR_ID_TRM = T.TR_ID_TRM)
                        AND ES.ES_SGL_EST = 'MT'
                        AND AO_DESTINO.AO_ID_AO NOT IN (40035, 40039) 
                        AND (SELECT COUNT(*)
                               FROM MOVIMENTACAO_TREM MT, AREA_OPERACIONAL AO
                               WHERE MT.TR_ID_TRM =
                                       (SELECT TR_ID_TRM
                                               FROM COMPOSICAO CP
                                              WHERE CP.CP_ID_CPS = COMP.CP_ID_CPS)
                                AND NVL(AO.AO_ID_AO_INF, AO.AO_ID_AO) IN (21389)
                                AND AO.AO_ID_AO = MT.AO_ID_AO
                                AND MT.MT_DTS_RAL IS NOT NULL) = 0 {0}");

                if (idTrem > 0)
                {
                    hqlCondicoes.AppendLine(" AND T.TR_ID_TRM = :idTrem ");
                    hqlCondicoes2.AppendLine(" AND T.TR_ID_TRM = :idTrem ");
                    parameters.Add(q => q.SetParameter("idTrem", idTrem));
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(chaveMdfe))
                    {
                        hqlCondicoes.AppendLine(" AND M.CHAVE_MDFE = :chaveMdfe ");
                        parameters.Add(q => q.SetParameter("chaveMdfe", chaveMdfe));
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(numero))
                        {
                            hqlCondicoes.AppendLine(" and M.NRO_MDFE = :numero ");
                            parameters.Add(q => q.SetParameter("numero", numero));
                        }

                        if (!string.IsNullOrWhiteSpace(serie))
                        {
                            hqlCondicoes.AppendLine(" and M.SERIE_MDFE = :serie ");
                            parameters.Add(q => q.SetParameter("serie", serie));
                        }

                        if (!string.IsNullOrWhiteSpace(prefixoTrem))
                        {
                            hqlCondicoes.AppendLine(" AND T.TR_PFX_TRM = :prefixoTrem ");
                            hqlCondicoes2.AppendLine(" AND T.TR_PFX_TRM = :prefixoTrem ");
                            parameters.Add(q => q.SetParameter("prefixoTrem", prefixoTrem));
                        }

                        if (os.HasValue)
                        {
                            hqlCondicoes.AppendLine(" and OS.X1_NRO_OS = :os ");
                            hqlCondicoes2.AppendLine(" and OS.X1_NRO_OS = :os ");
                            parameters.Add(q => q.SetParameter("os", os));
                        }

                        if (dataInicial.HasValue)
                        {
                            hqlCondicoes.AppendLine(" and M.DATA_MDFE >= :dataInicial ");
                            hqlCondicoes2.AppendLine(" AND T.TR_DAT_LBR >= :dataInicial ");
                            parameters.Add(q => q.SetParameter("dataInicial", dataInicial));
                        }

                        if (dataFinal.HasValue)
                        {
                            hqlCondicoes.AppendLine(" and M.DATA_MDFE <= :dataFinal ");
                            hqlCondicoes2.AppendLine(" AND T.TR_DAT_LBR <= :dataFinal ");
                            parameters.Add(q => q.SetParameter("dataFinal", dataFinal));
                        }

                        if (!string.IsNullOrWhiteSpace(origem))
                        {
                            hqlCondicoes.AppendLine(" and AO_ORIGEM.AO_COD_AOP = :origem ");
                            hqlCondicoes2.AppendLine(" and AO_ORIGEM.AO_COD_AOP = :origem ");
                            parameters.Add(q => q.SetParameter("origem", origem));
                        }

                        if (!string.IsNullOrWhiteSpace(destino))
                        {
                            hqlCondicoes.AppendLine(" and AO_DESTINO.Ao_Cod_Aop = :destino ");
                            hqlCondicoes2.AppendLine(" and AO_DESTINO.Ao_Cod_Aop = :destino ");
                            parameters.Add(q => q.SetParameter("destino", destino));
                        }

                        if (!string.IsNullOrWhiteSpace(situacao))
                        {
                            hqlCondicoes.AppendLine(" and M.Situacao_Atual = :situacao ");
                            parameters.Add(q => q.SetParameter("situacao", situacao));
                        }
                    }
                }

                using (var session = OpenSession())
                {
                    string consulta;

                    if (!string.IsNullOrWhiteSpace(serie) || !string.IsNullOrWhiteSpace(numero))
                    {
                        consulta = string.Format(hql.ToString(), hqlCondicoes, "");
                    }
                    else
                    {
                        var union = string.Format(hqlUnion.ToString(), hqlCondicoes2);
                        consulta = string.Format(hql.ToString(), hqlCondicoes, union);
                    }
                    var query = session.CreateSQLQuery(consulta)
                        .AddScalar("Id", NHibernateUtil.Int32)
                        .AddScalar("Prefixo", NHibernateUtil.String)
                        .AddScalar("Origem", NHibernateUtil.String)
                        .AddScalar("Destino", NHibernateUtil.String)
                        .AddScalar("OS", NHibernateUtil.Int32)
                        .AddScalar("SituacaoAtual", new DescribedEnumStringType<SituacaoMdfeEnum>())
                        .AddScalar("Informativo", NHibernateUtil.String)
                        .AddScalar("UltimoEvento", NHibernateUtil.String)
                        .AddScalar("DataEvento", NHibernateUtil.DateTime);

                    var queryCount = session.CreateSQLQuery(string.Format(@"SELECT COUNT(0) FROM ({0})", query));

                    foreach (var p in parameters)
                    {
                        p.Invoke(query);
                        p.Invoke(queryCount);
                    }

                    query.SetResultTransformer(Transformers.AliasToBean<TremMdfeDto>());
                    query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                    var itens = query.List<TremMdfeDto>();
                    var total = queryCount.UniqueResult<decimal>();

                    var result = new ResultadoPaginado<TremMdfeDto>
                    {
                        Items = itens,
                        Total = Convert.ToInt32(total)
                    };

                    return result;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        /// <summary>
        /// Buscar registros para exportar
        /// </summary>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        public IList<TremMdfeDto> ExportarExcel(string chave, string numero, string serie, string prefixo, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao)
        {
            try
            {
                var parameters = new List<Action<IQuery>>();
                var hqlCondicoes = new StringBuilder();
                var hqlCondicoes2 = new StringBuilder();
                var hql = new StringBuilder(@"
                  SELECT DISTINCT T.TR_ID_TRM AS ""Id"", T.Tr_Pfx_Trm as ""Prefixo"", AO_ORIGEM.AO_COD_AOP as ""Origem"", AO_DESTINO.Ao_Cod_Aop as ""Destino"", os.x1_nro_os as ""OS"", M.SITUACAO_ATUAL as ""SituacaoAtual"", M.SITUACAO_ATUAL as ""Informativo""
                    ,(SELECT CONCAT(CONCAT(RET.CODIGO_STATUS_RETORNO, ' - '), RET.DESC_RETORNO) 
                    FROM MDFE_STATUS_RETORNO RET
                    WHERE RET.CODIGO_STATUS_RETORNO IN 
                    (SELECT CODIGO_STATUS_RETORNO FROM (
                            SELECT STAT.CODIGO_STATUS_RETORNO, STAT.ID_MDFE
                            FROM MDFE_STATUS STAT 
                            ORDER BY STAT.ID_MDFE_STATUS DESC) 
                      WHERE ID_MDFE = M.ID_MDFE
                      AND ROWNUM = 1)) AS ""UltimoEvento"",

                      (SELECT DATA_HORA
                        FROM (SELECT STAT.DATA_HORA, STAT.ID_MDFE
                           FROM MDFE_STATUS STAT
                          ORDER BY STAT.ID_MDFE_STATUS DESC)
                    WHERE ID_MDFE = M.ID_MDFE
                    AND ROWNUM = 1) AS ""DataEvento""               

                    FROM MDFE M 
                  INNER JOIN T2_OS OS ON OS.X1_ID_OS = M.X1_ID_OS 
                  INNER JOIN TREM T ON T.TR_ID_TRM = M.Tr_Id_Trm
                  INNER JOIN AREA_OPERACIONAL AO_ORIGEM ON AO_ORIGEM.AO_ID_AO =  T.AO_ID_AO_ORG
                  INNER JOIN AREA_OPERACIONAL AO_DESTINO ON AO_DESTINO.AO_ID_AO =  T.AO_ID_AO_DST
                  WHERE 
                      M.ID_MDFE = 
                      ( 
                      SELECT MA.ID_MDFE 
                        FROM MDFE_ARVORE MA
                        WHERE MA.ID_MDFE_ARVORE = 
                        ( 
                          SELECT MAX(MAR.ID_MDFE_ARVORE) 
                          FROM MDFE_ARVORE MAR, MDFE_ARVORE MAF
                          WHERE 
                          MAR.ID_MDFE_RAIZ = MAF.ID_MDFE_RAIZ 
                          AND MAF.ID_MDFE = M.ID_MDFE
                        ) 
                      ) {0} {1}");

                var hqlUnion = new StringBuilder(@"UNION 

                  SELECT DISTINCT T.TR_ID_TRM AS ""Id"", T.Tr_Pfx_Trm as ""Prefixo"", AO_ORIGEM.AO_COD_AOP as ""Origem"", AO_DESTINO.Ao_Cod_Aop as ""Destino"", OS.x1_nro_os AS ""OS"", 'SEM' as ""SituacaoAtual"", 'SEM' as ""Informativo""
                     ,(SELECT CONCAT(CONCAT(RET.CODIGO_STATUS_RETORNO, ' - '), RET.DESC_RETORNO) 
                  FROM MDFE_STATUS_RETORNO RET
                  WHERE RET.CODIGO_STATUS_RETORNO IN 
                    (SELECT CODIGO_STATUS_RETORNO FROM (
                        SELECT STAT.CODIGO_STATUS_RETORNO, MD.TR_ID_TRM
                        FROM MDFE_STATUS STAT, MDFE MD
                        WHERE MD.ID_MDFE = STAT.ID_MDFE
                        ORDER BY STAT.ID_MDFE_STATUS DESC) 
                  WHERE TR_ID_TRM = T.TR_ID_TRM 
                  AND ROWNUM = 1)) AS ""UltimoEvento"",

                  (SELECT DATA_HORA
                  FROM (SELECT STAT.DATA_HORA, MD.TR_ID_TRM
                          FROM MDFE_STATUS STAT, MDFE MD
                         WHERE MD.ID_MDFE = STAT.ID_MDFE
                         ORDER BY STAT.ID_MDFE_STATUS DESC)
                 WHERE TR_ID_TRM = T.TR_ID_TRM
                   AND ROWNUM = 1) AS ""DataEvento""         

                  FROM compvagao cv 
                   JOIN vagao_pedido_vig vpv 
                      ON vpv.vg_id_vg = cv.vg_id_vg
                   JOIN carregamento cg
                      ON cg.cg_id_car = vpv.cg_id_car
                   JOIN detalhe_carregamento dc
                      ON dc.cg_id_car = cg.cg_id_car and dc.vg_id_vg = vpv.vg_id_vg
                   JOIN item_despacho idp
                      ON idp.dc_id_crg = dc.dc_id_crg
                   JOIN cte c
                      ON c.dp_id_dp = idp.dp_id_dp
                   JOIN fluxo_comercial fc
                      ON fc.fx_id_flx = C.fx_id_flx
                   JOIN AREA_OPERACIONAL AO_DESTINO 
                      ON FC.AO_ID_EST_DS = AO_DESTINO.AO_ID_AO
                   JOIN AREA_OPERACIONAL AO_ORIGEM
                      ON FC.AO_ID_EST_OR = AO_ORIGEM.AO_ID_AO
                   JOIN  MUNICIPIO MN 
                      ON MN.MN_ID_MNC = AO_ORIGEM.MN_ID_MNC
                   JOIN ESTADO ES
                      ON ES.ES_ID_EST = MN.ES_ID_EST              
                   JOIN COMPOSICAO COMP
                      ON COMP.CP_ID_CPS = CV.CP_ID_CPS
                   JOIN TREM T 
                      ON COMP.TR_ID_TRM = T.TR_ID_TRM 
                   JOIN T2_OS OS ON OS.x1_id_os = T.OF_ID_OSV                 
                   WHERE 
					  T.TR_ID_TRM NOT IN (SELECT M.TR_ID_TRM FROM MDFE M WHERE M.TR_ID_TRM = T.TR_ID_TRM)
                        AND ES.ES_SGL_EST = 'MT'
                        AND AO_DESTINO.AO_ID_AO NOT IN (40035, 40039) 
                        AND (SELECT COUNT(*)
                               FROM MOVIMENTACAO_TREM MT, AREA_OPERACIONAL AO
                               WHERE MT.TR_ID_TRM =
                                       (SELECT TR_ID_TRM
                                               FROM COMPOSICAO CP
                                              WHERE CP.CP_ID_CPS = COMP.CP_ID_CPS)
                                AND NVL(AO.AO_ID_AO_INF, AO.AO_ID_AO) IN (21389)
                                AND AO.AO_ID_AO = MT.AO_ID_AO
                                AND MT.MT_DTS_RAL IS NOT NULL) = 0 {0}");

                if (!string.IsNullOrWhiteSpace(chave))
                {
                    hqlCondicoes.AppendLine(" AND M.CHAVE_MDFE = :chaveMdfe ");
                    parameters.Add(q => q.SetParameter("chaveMdfe", chave));
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(numero))
                    {
                        hqlCondicoes.AppendLine(" and M.NRO_MDFE = :numero ");
                        parameters.Add(q => q.SetParameter("numero", numero));
                    }

                    if (!string.IsNullOrWhiteSpace(serie))
                    {
                        hqlCondicoes.AppendLine(" and M.SERIE_MDFE = :serie ");
                        parameters.Add(q => q.SetParameter("serie", serie));
                    }

                    if (!string.IsNullOrWhiteSpace(prefixo))
                    {
                        hqlCondicoes.AppendLine(" AND T.TR_PFX_TRM = :prefixoTrem ");
                        hqlCondicoes2.AppendLine(" AND T.TR_PFX_TRM = :prefixoTrem ");
                        parameters.Add(q => q.SetParameter("prefixoTrem", prefixo));
                    }

                    if (os.HasValue)
                    {
                        hqlCondicoes.AppendLine(" and OS.X1_NRO_OS = :os ");
                        hqlCondicoes2.AppendLine(" and OS.X1_NRO_OS = :os ");
                        parameters.Add(q => q.SetParameter("os", os));
                    }

                    if (dataInicial.HasValue)
                    {

                        var dataIni = DateTime.ParseExact(dataInicial.Value.ToString(), "dd/MM/yyyy HH:mm:ss",
                            CultureInfo.InvariantCulture);


                        hqlCondicoes.AppendLine(" and M.DATA_MDFE >= :dataInicial ");
                        hqlCondicoes2.AppendLine(" AND T.TR_DAT_LBR >= :dataInicial ");
                        parameters.Add(q => q.SetParameter("dataInicial", dataIni));
                    }

                    if (dataFinal.HasValue)
                    {
                        var dataFin = DateTime.ParseExact(dataFinal.Value.ToString(), "dd/MM/yyyy HH:mm:ss",
                            CultureInfo.InvariantCulture).AddDays(1).AddSeconds(-1);

                        hqlCondicoes.AppendLine(" and M.DATA_MDFE <= :dataFinal ");
                        hqlCondicoes2.AppendLine(" AND T.TR_DAT_LBR <= :dataFinal ");
                        parameters.Add(q => q.SetParameter("dataFinal", dataFin));
                    }

                    if (!string.IsNullOrWhiteSpace(origem))
                    {
                        hqlCondicoes.AppendLine(" and AO_ORIGEM.AO_COD_AOP = :origem ");
                        hqlCondicoes2.AppendLine(" and AO_ORIGEM.AO_COD_AOP = :origem ");
                        parameters.Add(q => q.SetParameter("origem", origem));
                    }

                    if (!string.IsNullOrWhiteSpace(destino))
                    {
                        hqlCondicoes.AppendLine(" and AO_DESTINO.Ao_Cod_Aop = :destino ");
                        hqlCondicoes2.AppendLine(" and AO_DESTINO.Ao_Cod_Aop = :destino ");
                        parameters.Add(q => q.SetParameter("destino", destino));
                    }

                    if (!string.IsNullOrWhiteSpace(situacao))
                    {
                        hqlCondicoes.AppendLine(" and M.Situacao_Atual = :situacao ");
                        parameters.Add(q => q.SetParameter("situacao", situacao));
                    }

                }

                using (var session = OpenSession())
                {
                    string consulta;

                    if (!string.IsNullOrWhiteSpace(serie) || !string.IsNullOrWhiteSpace(numero))
                    {
                        consulta = string.Format(hql.ToString(), hqlCondicoes, "");
                    }
                    else
                    {
                        var union = string.Format(hqlUnion.ToString(), hqlCondicoes2);
                        consulta = string.Format(hql.ToString(), hqlCondicoes, union);
                    }
                    var query = session.CreateSQLQuery(consulta)
                        .AddScalar("Id", NHibernateUtil.Int32)
                        .AddScalar("Prefixo", NHibernateUtil.String)
                        .AddScalar("Origem", NHibernateUtil.String)
                        .AddScalar("Destino", NHibernateUtil.String)
                        .AddScalar("OS", NHibernateUtil.Int32)
                        .AddScalar("SituacaoAtual", new DescribedEnumStringType<SituacaoMdfeEnum>())
                        .AddScalar("Informativo", NHibernateUtil.String)
                        .AddScalar("UltimoEvento", NHibernateUtil.String)
                        .AddScalar("DataEvento", NHibernateUtil.DateTime);

                    foreach (var p in parameters)
                    {
                        p.Invoke(query);
                    }

                    query.SetResultTransformer(Transformers.AliasToBean<TremMdfeDto>());

                    var itens = query.List<TremMdfeDto>();

                    return itens;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        /// <summary>
        /// Lista Log de Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="tipoLog">Tipo de Log</param>
        /// <param name="nomeServico">Nome do Serviço</param>
        /// <returns>resultado paginado de logs de mdfe</returns>
        public ResultadoPaginado<LogPkgMdfeDto> ListarLogPkg(DetalhesPaginacao detalhesPaginacao, string tipoLog, string nomeServico)
        {
            const string SqlResult = @"SELECT M.ID_MDFE_PKG_LOG as ""Id"", M.TIPO_LOG as ""TipoLog"", M.NOME_SERVICO AS ""NomeServico"", 
                                        M.DESCRICAO AS ""Descricao"", M.DATA_HORA AS ""DataHora"", M.MT_ID_MOV AS ""MovimentacaoTrem"",
                                        M.TR_ID_TRM AS ""Trem"", M.AO_ID_AO AS ""AreaOperacional"", M.CP_ID_CPS AS ""Composicao""
                                        FROM MDFE_PKG_LOG M 
                                        WHERE M.NOME_SERVICO = :nomeServico ORDER BY M.DATA_HORA desc";

            ResultadoPaginado<LogPkgMdfeDto> result;

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(SqlResult);

                var queryCount = session.CreateSQLQuery(string.Format(@"SELECT COUNT(0) FROM ({0})", query));

                query.SetString("nomeServico", nomeServico);
                queryCount.SetString("nomeServico", nomeServico);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(LogPkgMdfeDto)));
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var itens = query.List<LogPkgMdfeDto>();
                var total = queryCount.UniqueResult<decimal>();

                result = new ResultadoPaginado<LogPkgMdfeDto>
                {
                    Items = itens,
                    Total = Convert.ToInt32(total)
                };
            }

            return result;
        }

        /// <summary>
        /// Gera uma Mdfe forçada pela tela 1253
        /// </summary>
        /// <param name="os">Número da OS</param>
        public bool GerarMdfeForcada(int os)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    var tmp = session.Query<MovimentacaoTrem>()
                        .Where(mt => mt.Trem.OrdemServico.Numero == os)
                        .OrderBy(a => a.DataSaidaRealizada)
                        .Select(mt => new
                        {
                            pIdMovimentacaoTrem = mt.Id,
                            pIdAreaOperacional = mt.Estacao.Id,
                            pIdTrem = mt.Trem.Id,
                            pIdComposicao = mt.Composicao.Id,
                            pDataSaidaRealizada = mt.DataSaidaRealizada,
                            pIdOS = mt.Trem.OrdemServico.Id,
                            pPrefixo = mt.Trem.Prefixo,
                            pOrigem = "TELA"
                        })
                        .FirstOrDefault();


                    var command = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "PKG_MDFE.SP_EXEC_GERACAO_DADOS",
                        Connection = (OracleConnection)session.Connection
                    };
                    AddParameter(command, "PIDMOVIMENTACAOTREM", OracleType.Number, tmp.pIdMovimentacaoTrem, ParameterDirection.Input);
                    AddParameter(command, "PIDAREAOPERACIONAL", OracleType.Number, tmp.pIdAreaOperacional, ParameterDirection.Input);
                    AddParameter(command, "PIDTREM", OracleType.Number, tmp.pIdTrem, ParameterDirection.Input);
                    AddParameter(command, "PIDCOMPOSICAO", OracleType.Number, tmp.pIdComposicao, ParameterDirection.Input);
                    AddParameter(command, "PDATASAIDAREALIZADA", OracleType.DateTime, tmp.pDataSaidaRealizada, ParameterDirection.Input);
                    AddParameter(command, "PIDOS", OracleType.Number, tmp.pIdOS, ParameterDirection.Input);
                    AddParameter(command, "PPREFIXO", OracleType.VarChar, tmp.pPrefixo, ParameterDirection.Input);
                    AddParameter(command, "PORIGEM", OracleType.VarChar, tmp.pOrigem, ParameterDirection.Input);

                    session.Transaction.Enlist(command);

                    command.Prepare();
                    command.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> será adicionado</param>
        /// <param name="nome">Nome do parâmetro</param>
        /// <param name="tipo">Tipo do parâmetro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Direção <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;

            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado</returns>
        public IList<MdfeDto> ListarDetalhesDaMdfe(int idTrem)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"SELECT
										m.Id as ""Id"",
										m.Chave as ""Chave"",
										m.Numero as ""Numero"",
										m.Serie as ""Serie"",
										m.SituacaoAtual as ""SituacaoAtual"",
										m.DataHora as ""DataHora"",
										(SELECT 'S' FROM MdfeArquivo ma WHERE ma.TamanhoPdf > 0 AND ma.Id = m.Id) as ""PdfGerado"",
                                        case when ((sysdate - m.DataRecibo) * 24) > 24 then 0 else 1 end as ""IndAnulacao""
									 FROM Mdfe m
							   INNER JOIN m.Trem t
									WHERE t.Id = :idTrem
								 ORDER BY m.DataHora Desc";

                    IQuery query = session.CreateQuery(hql);

                    query.SetInt32("idTrem", idTrem);

                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    query.SetResultTransformer(Transformers.AliasToBean<MdfeDto>());

                    return query.List<MdfeDto>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="paginacao">detalhes da paginacao</param>
        /// <param name="idTrem">id do trem</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no semáforo</param>
        /// <param name="periodoHorasErroSemaforo">O período de tempo selecionado no semáforo</param>
        /// <returns>resultado paginado</returns>
        public ResultadoPaginado<MdfeDto> ListarDetalhesDaMdfePaginado(DetalhesPaginacao paginacao, int idTrem, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    var hql = new StringBuilder(@"SELECT {0}
									 FROM Mdfe m
							   INNER JOIN m.Trem t
									WHERE {1}
								 ORDER BY m.DataHora Desc");

                    const string SelectStatements = @"m.Id as ""Id"",
                                        t.Id as ""TremId"",
										m.Chave as ""Chave"",
										m.Numero as ""Numero"",
										m.Serie as ""Serie"",
										m.SituacaoAtual as ""SituacaoAtual"",
										m.DataHora as ""DataHora"",
										(SELECT 'S' FROM MdfeArquivo ma WHERE ma.TamanhoPdf > 0 AND ma.Id = m.Id) as ""PdfGerado""";

                    var hqlResult = String.Empty;
                    var hqlResultCount = String.Empty;

                    IQuery query = null;
                    IQuery queryCount = null;

                    if (idTrem > 0)
                    {
                        hqlResult = String.Format(hql.ToString(), SelectStatements, "t.Id = :idTrem");
                        hqlResultCount = String.Format(hql.ToString(), "COUNT(m.Id)", "t.Id = :idTrem");

                        query = session.CreateQuery(hqlResult);
                        queryCount = session.CreateQuery(hqlResultCount);

                        query.SetInt32("idTrem", idTrem);
                        queryCount.SetInt32("idTrem", idTrem);
                    }
                    else if (!String.IsNullOrWhiteSpace(semaforo))
                    {
                        var strWhereAnd = new StringBuilder();

                        strWhereAnd.AppendLine(@" m.SituacaoAtual NOT IN (:AUT, :ENC, :INV, :CAN, :AME, :PGC)");
                        strWhereAnd.AppendLine(" AND m.AmbienteSefaz = 1");
                        strWhereAnd.AppendLine(" AND m.SiglaUfFerrovia = :siglaUfSemaforo");

                        switch (periodoHorasErroSemaforo)
                        {
                            case HorasErroEnum.DeZeroADozeHoras:
                                strWhereAnd.AppendLine(" AND ((sysdate - m.DataHora) * 24) <= 12");
                                break;
                            case HorasErroEnum.DeDozeAVinteQuatroHoras:
                                strWhereAnd.AppendLine(" AND ((sysdate - m.DataHora) * 24) > 12 AND ((sysdate - m.DataHora) * 24) <= 24");
                                break;
                            case HorasErroEnum.MaisQueVinteQuatroHoras:
                                strWhereAnd.AppendLine(" AND ((sysdate - m.DataHora) * 24) > 24");
                                break;
                        }

                        hqlResult = String.Format(hql.ToString(), SelectStatements, strWhereAnd.ToString());
                        hqlResultCount = String.Format(hql.ToString(), "COUNT(m.Id)", strWhereAnd.ToString());

                        query = session.CreateQuery(hqlResult);
                        queryCount = session.CreateQuery(hqlResultCount);

                        query.SetParameter("AUT", SituacaoMdfeEnum.Autorizado, new SituacaoMdfeEnumCustomType());
                        query.SetParameter("ENC", SituacaoMdfeEnum.Encerrado, new SituacaoMdfeEnumCustomType());
                        query.SetParameter("INV", SituacaoMdfeEnum.Invalidado, new SituacaoMdfeEnumCustomType());
                        query.SetParameter("CAN", SituacaoMdfeEnum.Cancelado, new SituacaoMdfeEnumCustomType());
                        query.SetParameter("AME", SituacaoMdfeEnum.AguardandoMdfeEncerramento, new SituacaoMdfeEnumCustomType());
                        query.SetParameter("PGC", SituacaoMdfeEnum.PendendeGeracaoChaveMdfe, new SituacaoMdfeEnumCustomType());

                        query.SetParameter("siglaUfSemaforo", siglaUfSemaforo);

                        queryCount.SetParameter("AUT", SituacaoMdfeEnum.Autorizado, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("ENC", SituacaoMdfeEnum.Encerrado, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("INV", SituacaoMdfeEnum.Invalidado, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("CAN", SituacaoMdfeEnum.Cancelado, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("AME", SituacaoMdfeEnum.AguardandoMdfeEncerramento, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("PGC", SituacaoMdfeEnum.PendendeGeracaoChaveMdfe, new SituacaoMdfeEnumCustomType());
                        queryCount.SetParameter("siglaUfSemaforo", siglaUfSemaforo);
                    }

                    query.SetFirstResult(paginacao.Inicio ?? 0);
                    query.SetMaxResults(paginacao.Limite ?? DetalhesPaginacao.LimitePadrao);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);
                    query.SetResultTransformer(Transformers.AliasToBean<MdfeDto>());

                    var itens = query.List<MdfeDto>();
                    var total = queryCount.UniqueResult<long>();

                    var result = new ResultadoPaginado<MdfeDto>
                    {
                        Items = itens,
                        Total = total
                    };

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Listar mdfes por despacho
        /// </summary>
        /// <param name="paginacao">Detalhes da paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de Mdfe do Despacho</returns>
        public ResultadoPaginado<MdfeDto> ListarPorDespacho(DetalhesPaginacao paginacao, int idDespacho)
        {
            var hql = @"SELECT		
                              {0}
						 FROM MdfeComposicao mx
                        INNER JOIN mx.Mdfe m
							WHERE mx.Despacho.Id = :idDespacho
							ORDER BY m.DataHora Desc";

            const string SelectStatements = @"m.Id as ""Id"",
                                m.Trem.Id as ""TremId"",
								m.Chave as ""Chave"",
								m.Numero as ""Numero"",
								m.Serie as ""Serie"",
								m.SituacaoAtual as ""SituacaoAtual"",
								m.DataHora as ""DataHora"",
								(SELECT 'S' FROM MdfeArquivo ma WHERE ma.TamanhoPdf > 0 AND ma.Id = mx.Id) as ""PdfGerado""";

            var hqlResult = String.Empty;
            var hqlResultCount = String.Empty;

            IQuery query = null;
            IQuery queryCount = null;

            hqlResult = String.Format(hql.ToString(), SelectStatements);
            hqlResultCount = String.Format(hql.ToString(), "COUNT(m.Id)");

            using (var session = OpenSession())
            {
                query = session.CreateQuery(hqlResult);
                queryCount = session.CreateQuery(hqlResultCount);

                query.SetInt32("idDespacho", idDespacho);
                queryCount.SetInt32("idDespacho", idDespacho);

                query.SetFirstResult(paginacao.Inicio ?? 0);
                query.SetMaxResults(paginacao.Limite ?? DetalhesPaginacao.LimitePadrao);
                query.SetResultTransformer(Transformers.DistinctRootEntity);
                query.SetResultTransformer(Transformers.AliasToBean<MdfeDto>());

                var itens = query.List<MdfeDto>();
                var total = queryCount.UniqueResult<long>();

                var result = new ResultadoPaginado<MdfeDto>
                {
                    Items = itens,
                    Total = total
                };

                return result;
            }
        }

        /// <summary>
        /// Lista o ID do  MDF-e pelo ID do despacho
        /// </summary>
        /// <param name="idDespacho">Id do Despacho</param>
        /// <returns>O ID do MDF-e</returns>
        public int ObterIdMdfePorDespacho(int idDespacho)
        {
            var hql = String.Format(@"
                                SELECT MAX(MDFE.ID_MDFE)
                                  FROM MDFE_COMPOSICAO MDFE
                                 WHERE MDFE.ID_CTE = (SELECT MAX(C.ID_CTE)
                                                        FROM CTE C
                                                       WHERE C.DP_ID_DP = {0}
                                                         AND C.SITUACAO_ATUAL = 'AUT')", idDespacho);

            using (var session = SessionFactory.OpenStatelessSession())
            {
                var query = session.CreateSQLQuery(hql);
                var id = query.UniqueResult<decimal>();
                return (int)id;
            }
        }

        /// <summary>
        /// Obtém os status de uma Mdfe
        /// </summary>
        /// <param name="idMdfe">Id da Mdfe que se deseja obter os status</param>
        /// <returns>Lista de Status da Mdfe</returns>
        public IList<MdfeStatus> ObterStatusMdfePorIdMdfe(int idMdfe)
        {
            using (ISession session = OpenSession())
            {
                var hqlQyery = @"FROM MdfeStatus ms
                                WHERE ms.Mdfe.Id = :idMdfe
                                ORDER BY ms.DataHora";

                IQuery query = session.CreateQuery(hqlQyery);
                query.SetInt32("idMdfe", idMdfe);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<MdfeStatus>();
            }
        }

        /// <summary>
        /// Retorna a lista de Mdfes com erro para o semáforo
        /// </summary>
        /// <returns>Lista de Mdfes com erro</returns>
        public IList<SemaforoMdfeDto> ObterMdfesErro()
        {
            const string SqlResult = @"SELECT SUM(QtdZeroDozeHoras) as ""QtdZeroDozeHoras"", SUM(QtdDozeVinteQuatroHoras) as ""QtdDozeVinteQuatroHoras"", SUM(QtdMaisQueVinteQuatroHoras) AS ""QtdMaisQueVinteQuatroHoras"", Uf AS ""Uf"" FROM (
SELECT COUNT(0) as QtdZeroDozeHoras, 0 as QtdDozeVinteQuatroHoras, 0 AS QtdMaisQueVinteQuatroHoras, M.Sigla_Uf_Ferrovia as Uf
                    FROM MDFE M
                   WHERE M.AMBIENTE_SEFAZ = 1
                     AND M.SITUACAO_ATUAL NOT IN ('AUT','ENC','INV','CAN','AME','PGC')
                     AND ((sysdate - M.Data_Mdfe) * 24) <= 12
                   GROUP BY M.Sigla_Uf_Ferrovia, 0
UNION ALL                   
SELECT 0 as QtdZeroDozeHoras,  COUNT(0) as QtdDozeVinteQuatroHoras, 0 AS QtdMaisQueVinteQuatroHoras, M.Sigla_Uf_Ferrovia as Uf
                    FROM MDFE M
                   WHERE M.AMBIENTE_SEFAZ = 1
                     AND M.SITUACAO_ATUAL NOT IN ('AUT','ENC','INV','CAN','AME','PGC')
                     AND ((sysdate - M.Data_Mdfe) * 24) > 12 AND ((sysdate - M.Data_Mdfe) * 24) <= 24
                   GROUP BY M.Sigla_Uf_Ferrovia, 0
UNION ALL                   
SELECT 0 as QtdZeroDozeHoras, 0 as QtdDozeVinteQuatroHoras,  COUNT(0) AS QtdMaisQueVinteQuatroHoras, M.Sigla_Uf_Ferrovia as Uf
                    FROM MDFE M
                   WHERE M.AMBIENTE_SEFAZ = 1
                   AND M.SITUACAO_ATUAL NOT IN ('AUT','ENC','INV','CAN','AME', 'PGC')
                     AND ((sysdate - M.Data_Mdfe) * 24) > 24
                   GROUP BY M.Sigla_Uf_Ferrovia, 0) GROUP BY Uf, 0,1,2 ORDER BY 3";

            IList<SemaforoMdfeDto> lstRetorno;

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(SqlResult);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(SemaforoMdfeDto)));
                lstRetorno = query.List<SemaforoMdfeDto>();
            }

            return lstRetorno;
        }

        /// <summary>
        /// Gera uma Mdfe Virtual ser vinculo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GerarMdfeVirtualSemVinculo(CteVirtuaisSemVinculoMdfeDto obj)
        {
            try
            {
                using (ISession session = OpenSession())
                {

                    var command = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "PKG_MDFE.SP_EXEC_GERACAO_DADOS",
                        Connection = (OracleConnection)session.Connection
                    };

                    AddParameter(command, "PIDMOVIMENTACAOTREM", OracleType.Number, obj.ID_MOVIMENTACAO, ParameterDirection.Input);
                    AddParameter(command, "PIDAREAOPERACIONAL", OracleType.Number, obj.ID_AREA_OPERACIONAL, ParameterDirection.Input);
                    AddParameter(command, "PIDTREM", OracleType.Number, obj.ID_TREM, ParameterDirection.Input);
                    AddParameter(command, "PIDCOMPOSICAO", OracleType.Number, obj.COMPOSICAO, ParameterDirection.Input);
                    AddParameter(command, "PDATASAIDAREALIZADA", OracleType.DateTime, obj.DATA_EMISSAO_VIRTUAL, ParameterDirection.Input);
                    AddParameter(command, "PIDOS", OracleType.Number, obj.ID_OS, ParameterDirection.Input);
                    AddParameter(command, "PPREFIXO", OracleType.VarChar, obj.PREFIXO_TREM, ParameterDirection.Input);
                    AddParameter(command, "PORIGEM", OracleType.VarChar, "TELA", ParameterDirection.Input);

                    session.Transaction.Enlist(command);

                    command.Prepare();
                    command.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}