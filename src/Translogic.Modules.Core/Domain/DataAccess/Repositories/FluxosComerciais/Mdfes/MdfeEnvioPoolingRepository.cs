﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;

	/// <summary>
	/// Classe de repositório do pooling de envio do MDF-e
	/// </summary>
	public class MdfeEnvioPoolingRepository : NHRepository<MdfeEnvioPooling, int?>, IMdfeEnvioPoolingRepository 
	{
		/// <summary>
		/// Limpa a sessão
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Clear();
				session.Flush();
			}
		}

		/// <summary>
		/// Remove os MDF-es da lista
		/// </summary>
		/// <param name="listaMdfe">Lista com os MDF-es a serem removidos</param>
		public void RemoverPorLista(IList<Mdfe> listaMdfe)
		{
			string hql = @"DELETE FROM MdfeEnvioPooling WHERE Id = :idMdfe";
			using (ISession session = OpenSession())
			{
				foreach (Mdfe mdfe in listaMdfe)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idMdfe", mdfe.Id.Value);
					query.ExecuteUpdate();
				}
			}
		}
	}
}