﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// Repositório da classe de versão do MDF-e
	/// </summary>
	public class MdfeVersaoRepository : NHRepository<MdfeVersao, int?>, IMdfeVersaoRepository 
	{
	}
}