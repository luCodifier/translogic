namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// Reposit�rio da classe MdfeSerieEmpresaUf
	/// </summary>
	public class MdfeSerieEmpresaUfRepository : NHRepository<MdfeSerieEmpresaUf, int>, IMdfeSerieEmpresaUfRepository
	{
		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="empresa">Objeto da empresa</param>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="MdfeSerieEmpresaUf"/></returns>
		public MdfeSerieEmpresaUf ObterPorEmpresaUf(IEmpresa empresa, string uf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Empresa", empresa) && Restrictions.Eq("Uf", uf));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="MdfeSerieEmpresaUf"/></returns>
		public MdfeSerieEmpresaUf ObterPorUf(string uf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Uf", uf));
			return ObterPrimeiro(criteria);
		}
	}
}