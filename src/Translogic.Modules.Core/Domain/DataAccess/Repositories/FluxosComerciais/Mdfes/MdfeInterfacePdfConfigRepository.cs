namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using NHibernate;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
	
	/// <summary>
	/// Reposit�rio da classe MdfeInterfacePdfConfigRepository.
	/// </summary>
	public class MdfeInterfacePdfConfigRepository : NHRepository<MdfeInterfacePdfConfig, int?>, IMdfeInterfacePdfConfigRepository
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		public IList<MdfeInterfacePdfConfig> ObterListaInterfacePorHost(string hostName)
		{
			DateTime dataMinima = DateTime.Now;
			dataMinima = dataMinima.AddDays(-5);

			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM MdfeInterfacePdfConfig mic  
											WHERE mic.HostName = :hostName 
											AND mic.DataHora >= :DataMinima
											ORDER BY mic.DataUltimaLeitura
";
					IQuery query = session.CreateQuery(hql);
					query.SetString("hostName", hostName);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetMaxResults(50);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<MdfeInterfacePdfConfig>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}