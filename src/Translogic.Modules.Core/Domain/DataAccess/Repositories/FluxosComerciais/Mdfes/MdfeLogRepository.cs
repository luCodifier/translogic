﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Net;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Mdfe;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;
	using Util;

	/// <summary>
	/// Repositório da classe do MDF-e log
	/// </summary>
	public class MdfeLogRepository : NHRepository<MdfeLog, int?>, IMdfeLogRepository 
	{
		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		public void InserirLogInfo(Mdfe mdfe, string mensagem)
		{
			InserirLogInfo(mdfe, string.Empty, mensagem);
		}

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="builder">String builder do log</param>
		public void InserirLogInfo(Mdfe mdfe, StringBuilder builder)
		{
			InserirLogInfo(mdfe, builder.ToString());
		}

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="nomeServico">Nome do serviço ou método que está sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		public void InserirLogInfo(Mdfe mdfe, string nomeServico, string mensagem)
		{
			using (ISession session = OpenSession())
			{
				MdfeLog mdfeLog = new MdfeLog();
				mdfeLog.Mdfe = mdfe;
				mdfeLog.NomeServico = nomeServico;
				mdfeLog.Descricao = Tools.TruncateString(mensagem, 250);
				mdfeLog.TipoLog = TipoLogMdfeEnum.Info;
				mdfeLog.DataHora = DateTime.Now;
				mdfeLog.HostName = Dns.GetHostName();
				Inserir(mdfeLog);
			}
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(Mdfe mdfe, string mensagem)
		{
			InserirLogErro(mdfe, string.Empty, mensagem);
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="builder">String builderdo log</param>
		public void InserirLogErro(Mdfe mdfe, StringBuilder builder)
		{
			InserirLogErro(mdfe, builder.ToString());
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(Mdfe mdfe, string nomeServico, string mensagem)
		{
			using (ISession session = OpenSession())
			{
				MdfeLog mdfeLog = new MdfeLog();
				mdfeLog.Mdfe = mdfe;
				mdfeLog.Descricao = Tools.TruncateString(mensagem, 250);
				mdfeLog.NomeServico = nomeServico;
				mdfeLog.TipoLog = TipoLogMdfeEnum.Erro;
				mdfeLog.DataHora = DateTime.Now;
				mdfeLog.HostName = Dns.GetHostName();
				Inserir(mdfeLog);
			}
		}
	}
}