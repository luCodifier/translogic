﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Repositório da classe MDF-e
	/// </summary>
	public class MdfeArvoreRepository : NHRepository<MdfeArvore, int?>, IMdfeArvoreRepository 
	{
		/// <summary>
		/// Obtém a arvore de MDfe pelo MDfe filho
		/// </summary>
		/// <param name="mdfeFilho">MDfe filho que está contido na arvore</param>
		/// <returns>Retorna a lista com os elementos da arvore</returns>
		public IList<MdfeArvore> ObterArvorePorMdfeFilho(Mdfe mdfeFilho)
		{
			IList<MdfeArvore> result;
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
						@"select car
							FROM MdfeArvore car,
							     MdfeArvore caf
							WHERE car.MdfeRaiz.Id = caf.MdfeRaiz.Id
							AND   caf.MdfeFilho = :IdMdfeFilho
							ORDER BY car.Sequencia");

			parametros.Add(q => q.SetInt32("IdMdfeFilho", (int)mdfeFilho.Id));

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(sb.ToString());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				foreach (Action<IQuery> action in parametros)
				{
					action.Invoke(query);
				}

				result = query.List<MdfeArvore>();
			}

			return result;
		}
	}
}