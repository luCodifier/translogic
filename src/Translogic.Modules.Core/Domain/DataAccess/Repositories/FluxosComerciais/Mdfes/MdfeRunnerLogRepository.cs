﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Cte;
	using Model.Diversos.Mdfe;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;
	using Util;

	/// <summary>
	/// Repositório da classe MdfeRunnerLog
	/// </summary>
	public class MdfeRunnerLogRepository : NHRepository<MdfeRunnerLog, int?>, IMdfeRunnerLogRepository 
	{
		/// <summary>
		/// Gets or sets SessionFactory.
		/// </summary>
		public ISessionFactory SessionFactory { get; set; }

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		public void InserirLogInfo(string hostName, string nomeServico, string mensagem)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				MdfeRunnerLog runnerLog = new MdfeRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(mensagem, 250);
				runnerLog.TipoLog = TipoLogMdfeEnum.Info;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="builder">String builder do log</param>
		public void InserirLogInfo(string hostName, string nomeServico, StringBuilder builder)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				MdfeRunnerLog runnerLog = new MdfeRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(builder.ToString(), 250);
				runnerLog.TipoLog = TipoLogMdfeEnum.Info;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(string hostName, string nomeServico, string mensagem)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				MdfeRunnerLog runnerLog = new MdfeRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(mensagem, 250);
				runnerLog.TipoLog = TipoLogMdfeEnum.Erro;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="builder">String builderdo log</param>
		public void InserirLogErro(string hostName, string nomeServico, StringBuilder builder)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				MdfeRunnerLog runnerLog = new MdfeRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(builder.ToString(), 250);
				runnerLog.TipoLog = TipoLogMdfeEnum.Erro;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}		 
	}
}