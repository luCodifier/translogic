﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Repositório da interface de recebimento da Config o MDF-e
	/// </summary>
	public class MdfeInterfaceRecebimentoConfigRepository : NHRepository<MdfeInterfaceRecebimentoConfig, int?>, IMdfeInterfaceRecebimentoConfigRepository 
	{
		/// <summary>
		/// Obtem os itens da interface que nao foram processados (que não estão no pooling de recebimento)
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-producao / 2-homologação</param>
		/// <returns>Retorna uma lista dos itens que não estão no pooling (interface de recebimento)</returns>
		public IList<MdfeInterfaceRecebimentoConfig> ObterNaoProcessados(int indAmbienteSefaz)
		{
			try
			{
				DateTime dataMinima = DateTime.Now;
				dataMinima = dataMinima.AddDays(-5);
				using (ISession session = OpenSession())
				{
					string hql = @"FROM MdfeInterfaceRecebimentoConfig mir
						WHERE NOT EXISTS(SELECT mir.Id FROM MdfeRecebimentoPooling mrp WHERE mrp.Id = mir.Id)
						AND DataHora >= :DataMinima
					  AND mir.Mdfe.AmbienteSefaz = :ambienteSefaz
						ORDER BY mir.DataUltimaLeitura";
					
					IQuery query = session.CreateQuery(hql);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetInt32("ambienteSefaz", indAmbienteSefaz);

					query.SetMaxResults(50);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<MdfeInterfaceRecebimentoConfig>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Remove os cte da interface que ja foi processados
		/// </summary>
		/// <param name="mdfe">MDF-es para serem removidos da lista</param>
		public void RemoverPorMdfe(Mdfe mdfe)
		{
			string hql = @"DELETE FROM MdfeInterfaceRecebimentoConfig mir WHERE mir.Mdfe.Id = :idMdfe";
			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);
				query.SetInt32("idMdfe", mdfe.Id.Value);
				query.ExecuteUpdate();
			}
		}
	}
}