﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Mdfes;
	using Model.FluxosComerciais.Mdfes.Repositories;

	/// <summary>
	/// Classe de repositório da estação de geração do MDF-e
	/// </summary>
	public class MdfeEstacaoGeracaoRepository : NHRepository<MdfeEstacaoGeracao, int?>, IMdfeEstacaoGeracaoRepository 
	{
	}
}