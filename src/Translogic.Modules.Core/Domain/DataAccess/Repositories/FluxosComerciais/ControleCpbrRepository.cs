namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Repositories;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
	/// Repositorio para <see cref="AssociaFluxo"/>
	/// </summary>
    public class ControleCpbrRepository : NHRepository<ControleCpbr, int?>, IControleCpbrRepository
	{
       
	}
}