namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Reposit�io da classe de contrato hist�rico
	/// </summary>
	public class ContratoHistoricoRepository : NHRepository<ContratoHistorico, int?>, IContratoHistoricoRepository
	{
		/// <summary>
		/// Obt�m a lista de hist�ricos de contrato pelo contrato
		/// </summary>
		/// <param name="contrato">Contrato a ser pesquisado no hist�rico</param>
		/// <returns>Retorna a lista historica dos contratos</returns>
		public IList<ContratoHistorico> ObterPorContrato(Contrato contrato)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Contrato", contrato));
			criteria.AddOrder(new Order("DataHoraAtualizacao", false));
			return ObterTodos(criteria);
		}
	}
}