﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
    using System.Linq;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
    using NHibernate.Linq;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
    public class HistoricoNfeRepository : NHRepository<HistoricoNfe, int>, IHistoricoNfeRepository
	{
        /// <summary>
        /// Obtem uma lista de endereços de e-mail que devem receber notificação de NFe alterada.
        /// </summary>
        /// <returns>Lista de string com os endereços de e-mail</returns>
        public List<string> ObterListaDeEmailsQuandoHaAlteracao()
        {
            var sql = "select nea.email_destinatario from nfe_email_alteracao nea";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                var resultObjects = query.List();
                var result = resultObjects.Cast<string>().ToList();
                
                return result;
            }
        }

        /// <summary>
        /// informa se a NFe é oriunda da do simconsultas ou do EDI
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>nome da tabela</returns>
        public string ObterTabelaOrigemNfe(string chaveNfe)
        {
            using (var session = OpenSession())
            {
                var existeSimConsultas = session
                    .Query<NfeSimconsultas>()
                    .Where(e => e.ChaveNfe == chaveNfe)
                    .Count() > 0;

                if (existeSimConsultas)
                {
                    return "nfe_simconsultas";
                }
            }

            return "outros";
        }

        /// <summary>
        /// Lista todos os historicos não conferidos
        /// </summary>
        /// <returns>lista de historicos de alterações de nfe não conferidos</returns>
        public List<HistoricoNfe> ListarHistoricoNfe()
        {
            using (var session = OpenSession())
            {
                var query = session
                    .Query<HistoricoNfe>()
                    .Where(e => !e.AlteracaoConferida)
                    .OrderBy(e => e.DataAlteracao);

                var result = query.ToList();

                return result;
            }
        }
	}
}