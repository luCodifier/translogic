﻿using NHibernate.Exceptions;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
    using ALL.Core.Dominio;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using Translogic.Modules.Core.Domain.DataAccess.CustomType;
    using Translogic.Modules.EDI.Domain.Models.Dto;

    /// <summary>
    /// Classe de repositório do NFE Simconsultas
    /// </summary>
    public class NfeReadonlyRepository : NHRepository<NfeReadonly, int?>, INfeReadonlyRepository
    {
        /// <summary>
        /// Obtém os dados da NFe pela chave
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <returns> Objeto NfeReadonly </returns>
        public NfeReadonly ObterPorChaveNfe(string chaveNfe)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM NfeReadonly nfe WHERE nfe.ChaveNfe = :chavesNfe ";
                IQuery query = session.CreateQuery(hql);
                query.SetString("chavesNfe", chaveNfe);
                query.SetResultTransformer(Transformers.DistinctRootEntity);
                query.SetTimeout(900000);

                var lista = query.List<NfeReadonly>();

                if (lista != null && lista.Count > 0)
                {
                    return lista[0];
                }
                else
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Obtém os dados das NFes pelas chaves sem estado de sessão
        /// </summary>
        /// <param name="chavesNfe"> As chaves de nfes separadas por ','. </param>
        /// <returns> Lista de NfeReadonly </returns>
        public IList<NfeReadonly> ObterPorChavesNfeSemEstado(List<string> chavesNfe)
        {
            
            var chaves = string.Join(", ", chavesNfe.Select(i => string.Format("'{0}'", i)));
            StringBuilder hql = new StringBuilder();

            hql.Append(@"SELECT NFE.NFE_ID_NFE AS ""Id"",
                               NFE.TIPO AS ""Tipo"",
                               NFE.NFE_CHAVE AS ""ChaveNfe"",
                               NFE.NFE_CNPJ_ORI AS ""CnpjEmitente"",
                               NFE.NFE_CNPJ_DST AS ""CnpjDestinatario"",
                               NFE.NFE_DT_EMISSAO AS ""DataEmissao"",
                               NFE.CLI_ID_CLI AS ""IdCliente"",
                               NFE.NFE_IE_ORI AS ""InscricaoEstadualEmitente"",
                               NFE.NFE_RAZAO_SOCIAL_ORI AS ""RazaoSocialEmitente"",
                               NFE.NFE_FANTASIA_ORI AS ""NomeFantasiaEmitente"",
                               NFE.NFE_FONE_ORI AS ""TelefoneEmitente"",
                               NFE.NFE_LOGRADOURO_ORI AS ""LogradouroEmitente"",
                               NFE.NFE_COMPLEMENTO_ORI AS ""ComplementoEmitente"",
                               NFE.NFE_NUMERO_ORI AS ""NumeroEmitente"",
                               NFE.NFE_BAIRRO_ORI AS ""BairroEmitente"",
                               NFE.NFE_COD_MUNICIPIO_ORI AS ""CodigoMunicipioEmitente"",
                               NFE.NFE_MUNICIPIO_ORI AS ""MunicipioEmitente"",
                               NFE.NFE_CEP_ORI AS ""CepEmitente"",
                               NFE.NFE_UF_ORI AS ""UfEmitente"",
                               NFE.NFE_COD_PAIS_ORI AS ""CodigoPaisEmitente"",
                               NFE.NFE_PAIS_ORI AS ""PaisEmitente"",
                               NFE.NFE_IE_DST AS ""InscricaoEstadualDestinatario"",
                               NFE.NFE_RAZAO_SOCIAL_DST AS ""RazaoSocialDestinatario"",
                               NFE.NFE_FANTASIA_DST AS ""NomeFantasiaDestinatario"",
                               NFE.NFE_FONE_DST AS ""TelefoneDestinatario"",
                               NFE.NFE_LOGRADOURO_DST AS ""LogradouroDestinatario"",
                               NFE.NFE_NUMERO_DST AS ""NumeroDestinatario"",
                               NFE.NFE_COMPLEMENTO_DST AS ""ComplementoDestinatario"",
                               NFE.NFE_BAIRRO_DST AS ""BairroDestinatario"",
                               NFE.NFE_COD_MUNICIPIO_DST AS ""CodigoMunicipioDestinatario"",
                               NFE.NFE_MUNICIPIO_DST AS ""MunicipioDestinatario"",
                               NFE.NFE_UF_DST AS ""UfDestinatario"",
                               NFE.NFE_CEP_DST AS ""CepDestinatario"",
                               NFE.NFE_COD_PAIS_DST AS ""CodigoPaisDestinatario"",
                               NFE.NFE_PAIS_DST AS ""PaisDestinatario"",
                               NFE.NFE_NUM_NF AS ""NumeroNotaFiscal"",
                               NFE.NFE_SERIE AS ""SerieNotaFiscal"",
                               NFE.NFE_PLACA AS ""Placa"",
                               NFE.NFE_PESO AS ""Peso"",
                               NFE.NFE_VALOR_FRETE AS ""ValorTotalFrete"",
                               NFE.NFE_VALOR AS ""Valor"",
                               NFE.NFE_CUF AS ""CodigoUfIbge"",
                               NFE.NFE_CNF AS ""CodigoChaveAcesso"",
                               NFE.NFE_NATOP AS ""NaturezaOperacao"",
                               NFE.NFE_ID_PAG AS ""FormaPagamento"",
                               NFE.NFE_MOD AS ""ModeloNota"",
                               NFE.NFE_DT_SAIDA AS ""DataSaida"",
                               NFE.NFE_TPNF AS ""TipoNotaFiscal"",
                               NFE.NFE_CMUNFG AS ""CodigoIbge"",
                               NFE.NFE_TPIMP AS ""FormatoImpressao"",
                               NFE.NFE_TPEMIS AS ""TipoEmissao"",
                               NFE.NFE_CDV AS ""DigitoVerificadorChave"",
                               NFE.NFE_TPAMB AS ""TipoAmbiente"",
                               NFE.NFE_FINNFE AS ""FinalidadeEmissao"",
                               NFE.NFE_PROCEMI AS ""ProcessoEmissao"",
                               NFE.NFE_VERPROC AS ""VersaoProcesso"",
                               NFE.VBC AS ""ValorBaseCalculoIcms"",
                               NFE.VICMS AS ""ValorIcms"",
                               NFE.VBCST AS ""ValorBaseCalculoSubTributaria"",
                               NFE.VST AS ""ValorSubTributaria"",
                               NFE.VPROD AS ""ValorProduto"",
                               NFE.VFRETE AS ""ValorFrete"",
                               NFE.VSEG AS ""ValorSeguro"",
                               NFE.VDESC AS ""ValorDesconto"",
                               NFE.VII AS ""ValorImpostoImportacao"",
                               NFE.VIPI AS ""ValorIpi"",
                               NFE.VPIS AS ""ValorPis"",
                               NFE.VCOFINS AS ""ValorCofins"",
                               NFE.VOUTRO AS ""ValorOutro"",
                               NFE.CNPJ_TRANSP AS ""CnpjTransportadora"",
                               NFE.NOME_TRANSP AS ""NomeTransportadora"",
                               NFE.MODFRETE AS ""ModeloFrete"",
                               NFE.ENDER_TRANSP AS ""EnderecoTransportadora"",
                               NFE.UF_TRANSP AS ""UfTransportadora"",
                               NFE.PLACA_REB AS ""PlacaReboque"",
                               NFE.PESOB AS ""PesoBruto"",
                               NFE.MUN_TRASP AS ""MunicipioTransportadora"",
                               NFE.NFE_CANC_EMITENTE AS ""CanceladoEmitente"",
                               NFE.NFE_TIMESTAMP AS ""DataHoraGravacao"",
                               NFE.NFE_VOLUME AS ""Volume""
                        FROM VW_NFE NFE
                        WHERE NFE.NFE_CHAVE IN ({0})");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString(), chaves));

                query.AddScalar("Id" , NHibernateUtil.Int32);
                query.AddScalar("Tipo" , NHibernateUtil.AnsiString);
                query.AddScalar("ChaveNfe" , NHibernateUtil.AnsiString);
                query.AddScalar("CnpjEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("CnpjDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("DataEmissao" , NHibernateUtil.DateTime);
                query.AddScalar("IdCliente" , NHibernateUtil.Int32);
                query.AddScalar("InscricaoEstadualEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("RazaoSocialEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("NomeFantasiaEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("TelefoneEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("LogradouroEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("ComplementoEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("NumeroEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("BairroEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("CodigoMunicipioEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("MunicipioEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("CepEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("UfEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("CodigoPaisEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("PaisEmitente" , NHibernateUtil.AnsiString);
                query.AddScalar("InscricaoEstadualDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("RazaoSocialDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("NomeFantasiaDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("TelefoneDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("LogradouroDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("NumeroDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("ComplementoDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("BairroDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("CodigoMunicipioDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("MunicipioDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("UfDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("CepDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("CodigoPaisDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("PaisDestinatario" , NHibernateUtil.AnsiString);
                query.AddScalar("NumeroNotaFiscal" , NHibernateUtil.Int32);
                query.AddScalar("SerieNotaFiscal" , NHibernateUtil.AnsiString);
                query.AddScalar("Placa" , NHibernateUtil.AnsiString);
                query.AddScalar("Peso" , NHibernateUtil.Double);
                query.AddScalar("ValorTotalFrete" , NHibernateUtil.Double);
                query.AddScalar("Valor" , NHibernateUtil.Double);
                query.AddScalar("CodigoUfIbge" , NHibernateUtil.Int32);
                query.AddScalar("CodigoChaveAcesso" , NHibernateUtil.Int32);
                query.AddScalar("NaturezaOperacao" , NHibernateUtil.AnsiString);
                //query.AddScalar("FormaPagamento" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.FormaPagamentoEnumCustom type, Translogic.Modules.Core");
                query.AddScalar("ModeloNota" , NHibernateUtil.AnsiString);
                query.AddScalar("DataSaida" , NHibernateUtil.DateTime);
                //query.AddScalar("TipoNotaFiscal" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.TipoNotaFiscalEnumCustom type, Translogic.Modules.Core");
                query.AddScalar("CodigoIbge" , NHibernateUtil.Int32);
                //query.AddScalar("FormatoImpressao" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.FormatoImpressaoEnumCustom type, Translogic.Modules.Core");
                //query.AddScalar("TipoEmissao" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.TipoEmissaoEnumCustom type, Translogic.Modules.Core");
                query.AddScalar("DigitoVerificadorChave" , NHibernateUtil.Int32);
                //query.AddScalar("TipoAmbiente" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.TipoAmbienteEnumCustom type, Translogic.Modules.Core");
                //query.AddScalar("FinalidadeEmissao" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.FinalidadeEmissaoEnumCustom type, Translogic.Modules.Core");
                //query.AddScalar("ProcessoEmissao" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.ProcessoEmissaoEnumCustom type, Translogic.Modules.Core");
                query.AddScalar("VersaoProcesso" , NHibernateUtil.AnsiString);
                query.AddScalar("ValorBaseCalculoIcms" , NHibernateUtil.Double);
                query.AddScalar("ValorIcms" , NHibernateUtil.Double);
                query.AddScalar("ValorBaseCalculoSubTributaria" , NHibernateUtil.Double);
                query.AddScalar("ValorSubTributaria" , NHibernateUtil.Double);
                query.AddScalar("ValorProduto" , NHibernateUtil.Double);
                query.AddScalar("ValorFrete" , NHibernateUtil.Double);
                query.AddScalar("ValorSeguro" , NHibernateUtil.Double);
                query.AddScalar("ValorDesconto" , NHibernateUtil.Double);
                query.AddScalar("ValorImpostoImportacao" , NHibernateUtil.Double);
                query.AddScalar("ValorIpi" , NHibernateUtil.Double);
                query.AddScalar("ValorPis" , NHibernateUtil.Double);
                query.AddScalar("ValorCofins" , NHibernateUtil.Double);
                query.AddScalar("ValorOutro" , NHibernateUtil.Double);
                query.AddScalar("CnpjTransportadora" , NHibernateUtil.AnsiString);
                query.AddScalar("NomeTransportadora" , NHibernateUtil.AnsiString);
                //query.AddScalar("ModeloFrete" type="Translogic.Modules.Core.Domain.DataAccess.Custom type.ModeloFreteEnumCustom type, Translogic.Modules.Core");
                query.AddScalar("EnderecoTransportadora" , NHibernateUtil.AnsiString);
                query.AddScalar("UfTransportadora" , NHibernateUtil.AnsiString);
                query.AddScalar("PlacaReboque" , NHibernateUtil.AnsiString);
                query.AddScalar("PesoBruto" , NHibernateUtil.Double);
                query.AddScalar("MunicipioTransportadora" , NHibernateUtil.AnsiString);
                query.AddScalar("CanceladoEmitente", new BooleanCharType());
                query.AddScalar("DataHoraGravacao" , NHibernateUtil.DateTime);
                query.AddScalar("Volume" , NHibernateUtil.Double);


                //SessionFactory.Evict(typeof(NfeReadonly));
                query.SetResultTransformer(Transformers.AliasToBean(typeof(NfeReadonly)));
                var retorno = query.List<NfeReadonly>();

                foreach (var item in retorno)
                {
                    session.Evict(item);
                }

                return retorno;
            }
        }


        /// <summary>
        /// Obtem o registro dos PDFs EDI através da chave da NFe
        /// </summary>
        /// <param name="chavesNfe">Lista de chaves da NFe</param>
        /// <returns>Retorna os registros dos PDFs</returns>
        public IList<NfePdfEdiDto> ObterPdfEdiPorChavesNfe(List<string> chavesNfe)
        {
            var chaves = string.Join(", ", chavesNfe.Select(i => string.Format("'{0}'", i)));
            StringBuilder hql = new StringBuilder();

            using (var session = OpenSession())
            {
                hql.AppendFormat(@"
                    SELECT RESULTADOS.Id_Nfe            AS ""Id_Nfe""
                         , RESULTADOS.IdNfeSimConsultas AS ""IdNfeSimConsultas""
                         , RESULTADOS.Pdf               AS ""Pdf""
                         , RESULTADOS.Data              AS ""Data""
                         , RESULTADOS.ChaveNfe          AS ""ChaveNfe""
                      FROM (
                            SELECT PDF_EDI.NFE_ID_NFE    AS Id_Nfe
                                 , NULL                  AS IdNfeSimConsultas
                                 , PDF_EDI.NFE_PDF       AS Pdf
                                 , PDF_EDI.NFE_TIMESTAMP AS Data
                                 , NFE.NFE_CHAVE         AS ChaveNfe
                              FROM EDI.EDI2_NFE       NFE
                              JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                             WHERE NFE.NFE_CHAVE IN ({0})
                             UNION ALL
                            SELECT 0                     AS Id_Nfe
                                 , PDF_SIM.NFE_ID_PDF    AS IdNfeSimConsultas
                                 , PDF_SIM.NFE_PDF       AS Pdf
                                 , PDF_SIM.NFE_TIMESTAMP AS Data
                                 , NFE.NFE_CHAVE         AS ChaveNfe
                              FROM NFE_SIMCONSULTAS   NFE
                              JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = NFE.NFE_ID_NFE
                             WHERE NFE.NFE_CHAVE IN ({0})
                        ) RESULTADOS", chaves);

                var query = session.CreateSQLQuery(hql.ToString());

                query.AddScalar("Id_Nfe", NHibernateUtil.Decimal);
                query.AddScalar("IdNfeSimConsultas", NHibernateUtil.Decimal);
                query.AddScalar("Pdf", NHibernateUtil.BinaryBlob);
                query.AddScalar("Data", NHibernateUtil.DateTime);
                query.AddScalar("ChaveNfe", NHibernateUtil.AnsiString);
                
                query.SetResultTransformer(Transformers.AliasToBean<NfePdfEdiDto>());
                var resultados = query.List<NfePdfEdiDto>();

                return resultados;
            }
        }

        /// <summary>
        /// Obtém os dados das NFes pelas chaves
        /// </summary>
        /// <param name="chavesNfe"> As chaves de nfes separadas por ','. </param>
        /// <returns> Lista de NfeReadonly </returns>
        public IList<NfeReadonly> ObterPorChavesNfe(List<string> chavesNfe)
        {
            using (ISession session = OpenSession())
            {
                var parametros = new List<Action<IQuery>>();
                string hql = @"FROM NfeReadonly nfe WHERE nfe.ChaveNfe IN (:chavesNfe) ";

                var query = session.CreateQuery(hql);

                parametros.Add(q => q.SetParameterList("chavesNfe", chavesNfe));
                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);
                var lista = query.List<NfeReadonly>();
                return lista;
            }
        }

        /// <summary>
        /// Atualiza os pesos da Nfe no EDI ou no Simconsultas
        /// </summary>
        /// <param name="nfe">Objeto da NF-e</param>
        /// <remarks> Este código não está utilizando hql nem os objetos, pois as entidades do EDI não estão mapeadas</remarks>
        public void AtualizarPesosNfe(NfeReadonly nfe)
        {
            string sql = "UPDATE {0} SET NFE_PESO = :pesoLiquido, PESOB = :pesoBruto WHERE NFE_ID_NFE= :idNfe";
            string tabela = "NFE_SIMCONSULTAS";
            if (nfe.Tipo.Equals("EDI"))
            {
                tabela = "EDI.EDI2_NFE";
            }

            sql = string.Format(sql, tabela);
            using (ISession session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sql);
                query.SetInt32("idNfe", nfe.Id.Value);
                query.SetDouble("pesoLiquido", nfe.Peso);
                query.SetDouble("pesoBruto", nfe.PesoBruto);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Retorna as Nfe fora do Edi
        /// </summary>
        /// <returns>Lista de Nfe</returns>
        public IList<string> ObterNfeForaEdi()
        {
            using (ISession session = OpenSession())
            {
                string hql = @" SELECT NF.NFE_CHAVE_NFE
								FROM BO_BOLAR_NF      NF,
									BO_BOLAR_BO      B,
									BO_BOLAR_VAGAO   BV,
									FLUXO_COMERCIAL  FC,
									AREA_OPERACIONAL ORI,
									MUNICIPIO        MN,
									ESTADO           E
							WHERE NF.ID_VAGAO_ID = BV.ID_VAGAO
								AND BV.ID_BO_ID = B.ID_BO
								AND B.IDT_SIT IN ('N', 'E')
								AND BV.DATA_CARREG BETWEEN TRUNC(SYSDATE - 1) AND SYSDATE
								AND NF.NFE_CHAVE_NFE IS NOT NULL
								AND SUBSTR(FC.FX_COD_FLX, 3) = TO_CHAR(LPAD(B.COD_FLX, 5, '0'))
								AND FC.AO_ID_EST_OR = ORI.AO_ID_AO
								AND ORI.MN_ID_MNC = MN.MN_ID_MNC
								AND MN.ES_ID_EST = E.ES_ID_EST
								AND E.ES_SGL_EST IN (SELECT S.UF FROM CTE_SERIE_EMP_UF S WHERE SYSDATE >= S.DATA_INI_VIG)
								AND NOT EXISTS
							(SELECT 1 FROM VW_NFE NFE WHERE NFE.NFE_CHAVE = NF.NFE_CHAVE_NFE)";
                ISQLQuery query = session.CreateSQLQuery(hql);
                // query.SetInt32("idCte", cte.Id.Value);

                // query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<string>();
            }
        }


        /// <summary>
        /// Obtém os dados da NFe pela chave
        /// </summary>
        /// <param name="chavesNfe"> The chave nfe. </param>
        /// <returns> Objeto NfeReadonly </returns>
        public IList<NotaTicketDto> ObterPorChaveNfe(IList<string> chavesNfe)
        {
            
            var chaves = string.Join(", ", chavesNfe.Select(i => string.Format("'{0}'", i)));
            StringBuilder hql = new StringBuilder();

            hql.AppendFormat(@"SELECT DISTINCT 
                            VW.NFE_SERIE AS ""Serie"", 
                            VW.NFE_NUM_NF AS ""Numero"", 
                            VW.NFE_CHAVE AS ""ChaveNfe"", 
                            (SELECT ((VWP.CPROD||' - ')||VWP.XPROD) 
                                FROM VW_NFE_PROD VWP 
                                WHERE VWP.NFE_ID_NFE=VW.NFE_ID_NFE AND rownum=1) AS ""Produto"", 
                            ST.PESO-ST.PESO_UTILIZADO AS ""PesoDisponivel"" 
                        FROM 
                            VW_NFE VW, 
                            STATUS_NFE ST 
                        WHERE 
                            ST.NFE_CHAVE=VW.NFE_CHAVE 
                            AND (VW.NFE_CHAVE IN ({0}))", chaves);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());

                query.AddScalar("Serie", NHibernateUtil.AnsiString);
                query.AddScalar("Numero", NHibernateUtil.Int32);
                query.AddScalar("ChaveNfe", NHibernateUtil.AnsiString);
                query.AddScalar("Produto", NHibernateUtil.AnsiString);
                query.AddScalar("PesoDisponivel", NHibernateUtil.Double);
                
                query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaTicketDto)));
                return query.List<NotaTicketDto>();
            }
        }

//        /// <summary>
//        /// Obtém os dados da NFe pela chave
//        /// </summary>
//        /// <param name="chavesNfe"> The chave nfe. </param>
//        /// <returns> Objeto NfeReadonly </returns>
//        public IList<NotaTicketDto> ObterPorChaveNfe(IList<string> chavesNfe)
//        {

//            try
//            {
//                List<Action<IQuery>> parametros = new List<Action<IQuery>>();
//                StringBuilder hql = new StringBuilder();

//                hql.Append(@"SELECT distinct
//                            nfe.SerieNotaFiscal as Serie,
//                            nfe.NumeroNotaFiscal as Numero,
//                            nfe.ChaveNfe as ChaveNfe,
//                            (select CONCAT(TO_NUMBER(prod.CodigoProduto) ||' - ', prod.DescricaoProduto)  from NfeProdutoReadonly prod where prod.IdNfe = nfe.Id and rownum = 1) as Produto,
//                            (snfe.Peso - snfe.PesoUtilizado) as PesoDisponivel
//                          FROM NfeReadonly nfe, StatusNfe snfe                               
//                         WHERE snfe.ChaveNfe = nfe.ChaveNfe
//                          AND nfe.ChaveNfe IN (:chavesNfe)
//			    ");

//                using (var session = OpenSession())
//                {
//                    parametros.Add(q => q.SetParameterList("chavesNfe", chavesNfe));

//                    var query = session.CreateQuery(string.Format(hql.ToString()));

//                    foreach (var p in parametros)
//                    {
//                        p.Invoke(query);
//                    }

//                    query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaTicketDto)));
                    
//                    return query.List<NotaTicketDto>();
//                }
//            }
//            catch (Exception)
//            {

//                List<Action<IQuery>> parametros = new List<Action<IQuery>>();
//                StringBuilder hql = new StringBuilder();

//                hql.Append(@"SELECT distinct
//                            nfe.SerieNotaFiscal as Serie,
//                            nfe.NumeroNotaFiscal as Numero,
//                            nfe.ChaveNfe as ChaveNfe,
//                            (select CONCAT(prod.CodigoProduto ||' - ', prod.DescricaoProduto)  from NfeProdutoReadonly prod where prod.IdNfe = nfe.Id and rownum = 1) as Produto,
//                            (snfe.Peso - snfe.PesoUtilizado) as PesoDisponivel
//                          FROM NfeReadonly nfe, StatusNfe snfe                               
//                         WHERE snfe.ChaveNfe = nfe.ChaveNfe
//                          AND nfe.ChaveNfe IN (:chavesNfe)
//			    ");

//                using (var session = OpenSession())
//                {
//                    parametros.Add(q => q.SetParameterList("chavesNfe", chavesNfe));

//                    var query = session.CreateQuery(string.Format(hql.ToString()));

//                    foreach (var p in parametros)
//                    {
//                        p.Invoke(query);
//                    }

//                    query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaTicketDto)));

//                    return query.List<NotaTicketDto>();
//                }
//            }
            
//        }

        /// <summary>
        /// Obtém os dados da Nota Fiscal pela chave da Nfe informada
        /// </summary>
        /// <param name="idDespacho">Id do despacho da composição</param>
        /// <returns>Nota Fiscal</returns>
        public IList<NotaFiscalDto> ObterNotaFiscalPorDespachoId(int idDespacho)
        {
            var parametros = new List<Action<IQuery>>();
            parametros.Add(q => q.SetParameter("idDespacho", idDespacho));

            var sqlQuery = @"SELECT DISTINCT
        N.NF_IDT_NFL AS ""Id"",
       N.NF_TPO_MOD AS ""TipoDocumento"",
       N.NF_SER_NF AS ""SerieNotaFiscal"",
       N.NF_NRO_NF AS ""NumeroNotaFiscal"",
       N.NF_PESO_TOT_NF AS ""PesoBruto"",
       (SELECT SUM(NN.NF_PESO_NF)
          FROM NOTA_FISCAL NN 
         WHERE NN.NF_NRO_NF = N.NF_NRO_NF
           AND NN.NF_SER_NF = N.NF_SER_NF
           AND NN.DP_ID_DP = N.DP_ID_DP) As ""PesoUtilizado"",
       N.NFE_CHAVE_NFE AS ""ChaveNfe"",
       N.NF_PESO_NF AS ""PesoRateio"",
       N.Nf_Val_Nfl as ""ValorNotaFiscal"",
       N.Nf_Valor_Total as ""ValorTotalNotaFiscal"",
       N.NF_PESO_TOT_NF as ""PesoTotal"",
       N.NF_DAT_NF AS ""DataEmissao"",
      N.EP_CGC_REM as ""CnpjEmitente"",
      N.EP_IES_REM AS ""InscEstadualEmitente"",
      N.EP_UF_REM AS ""UfEmitente"",
      EE.EP_RAZ_SCL as ""RazaoSocialEmitente"",
      N.EP_CGC_DES as ""CnpjDestinatario"",
      N.EP_IES_DES AS ""InscEstadualDestinatario"",
      N.EP_UF_DES AS ""UfDestinatario"",
      ED.EP_RAZ_SCL as ""RazaoSocialDestinatario"",
      N.NF_CONTEINER AS ""Conteiner"" 
  FROM NOTA_FISCAL N, EMPRESA EE, EMPRESA ED
 WHERE EE.EP_CGC_EMP(+) = N.EP_CGC_REM
   AND ED.EP_CGC_EMP(+) = N.EP_CGC_DES
   AND N.DP_ID_DP = :idDespacho";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaFiscalDto)));

                return query.List<NotaFiscalDto>();
            }
        }

        /// <summary>
        /// Obtém uma Nota Fiscal dado um ID
        /// </summary>
        /// <param name="idNotaFiscal">Id da Nota para se obter os dados</param>
        /// <returns>Nota Fiscal</returns>
        public NotaFiscalDto ObterNotaFiscalPorIdNota(int idNotaFiscal)
        {
            var parametros = new List<Action<IQuery>>();
            parametros.Add(q => q.SetParameter("idNotaFiscal", idNotaFiscal));

            var sqlQuery = @"SELECT DISTINCT
       N.NF_IDT_NFL AS ""Id"",
       N.NF_TPO_MOD AS ""TipoDocumento"",
       N.NF_SER_NF AS ""SerieNotaFiscal"",
       N.NF_NRO_NF AS ""NumeroNotaFiscal"",
       N.NF_PESO_TOT_NF AS ""PesoBruto"",
       (SELECT SUM(NN.NF_PESO_NF)
          FROM NOTA_FISCAL NN 
         WHERE NN.NF_NRO_NF = N.NF_NRO_NF
           AND NN.NF_SER_NF = N.NF_SER_NF
           AND NN.DP_ID_DP = N.DP_ID_DP) As ""PesoUtilizado"",
       N.NFE_CHAVE_NFE AS ""ChaveNfe"",
       N.NF_PESO_NF AS ""PesoRateio"",
       N.Nf_Val_Nfl as ""ValorNotaFiscal"",
       N.Nf_Valor_Total as ""ValorTotalNotaFiscal"",
       N.NF_PESO_TOT_NF as ""PesoTotal"",
       N.NF_DAT_NF AS ""DataEmissao"",
      N.EP_CGC_REM as ""CnpjEmitente"",
      N.EP_IES_REM AS ""InscEstadualEmitente"",
      N.EP_UF_REM AS ""UfEmitente"",
      EE.EP_RAZ_SCL as ""RazaoSocialEmitente"",
      N.EP_CGC_DES as ""CnpjDestinatario"",
      N.EP_IES_DES AS ""InscEstadualDestinatario"",
      N.EP_UF_DES AS ""UfDestinatario"",
      ED.EP_RAZ_SCL as ""RazaoSocialDestinatario"",
      N.NF_CONTEINER AS ""Conteiner""  
  FROM NOTA_FISCAL N, EMPRESA EE, EMPRESA ED
 WHERE EE.EP_CGC_EMP(+) = N.EP_CGC_REM
   AND ED.EP_CGC_EMP(+) = N.EP_CGC_DES
   AND N.NF_IDT_NFL = :idNotaFiscal";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaFiscalDto)));

                return query.UniqueResult<NotaFiscalDto>();
            }
        }

        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        public IList<AcompanhamentoFatDto> ObterAcompanhamentos()
        {
            var sqlQuery = @"SELECT T2.SIGLA_UF_FERROVIA AS ""Estado"",
                               T2.CG_TIMESTAMP AS ""TimeStamp"",
                               T2.EP_DSC_RSM AS ""Empresa"",
                               T2.DSC_CORRENT AS ""Correntista"",
                               T2.ORIGEM AS ""Origem"", 
                               T2.DESTINO AS ""Destino"",
                               T2.MERCADORIA AS ""Mercadoria"",
                               T2.FATURAMENTO AS ""Faturamento"",  
                               T2.SISTEMA as ""Sistema"",    
                               COUNT(DISTINCT NFE) AS ""TotalNfe"" ,
                               (COUNT(DISTINCT SEFAZ)+COUNT(DISTINCT EDI)) AS ""ComXml"",
                               COUNT(DISTINCT SCO) AS ""SemXml""
                          FROM (SELECT T1.NFE_CHAVE_NFE NFE,
                                       T1.EP_DSC_RSM,
                                       T1.DSC_CORRENT,
                                       T1.SIGLA_UF_FERROVIA,
                                       T1.CG_TIMESTAMP,
                                      case
                                         when t1.vb_cod_tra = 'BOI' OR t1.vb_cod_tra = 'OP00BaDe' THEN
                                          'MANUAL'
                                          ELSE (NVL((SELECT CASE
                                                            WHEN bo.NOM_ARQ = 'EDI' THEN
                                                             'EDI NOVO'
                                                            WHEN bo.NOM_ARQ = 'CAALL' THEN
                                                             'EDI CAALL'
                                                         END
                                                 FROM  BO_BOLAR_BO BO, 
                                                       BO_BOLAR_VAGAO BV
                                                WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                                  AND BV.ID_BO_ID = BO.ID_BO
                                                  AND BO.IDT_SIT = 'D'
                                                  AND BO.NOM_ARQ IN ( 'CAALL' , 'EDI' )
                                                  AND ROWNUM = 1), 'EDI ANTIGO'))
                                           END FATURAMENTO,
                                       ORIGEM,
                                       DESTINO,
                                       MERCADORIA,
                                       (SELECT BO.AO_COD_AOP
                                             FROM  BO_BOLAR_BO BO, BO_BOLAR_VAGAO BV
                                            WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                              AND BV.ID_BO_ID = BO.ID_BO
                                              AND 1=1
                                              AND BO.IDT_SIT = 'D'
                                              ) SISTEMA,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM NFE_DISTRIBUICAO EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG) SEFAZ,
                                       (SELECT NFE_CHAVE
                                          FROM NFE_SIMCONSULTAS NS
                                         WHERE NS.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND NS.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)
                                          AND NOT EXISTS
                                         (SELECT 1
                                                  FROM EDI.EDI2_NFE X
                                                 WHERE X.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND X.NFE_TIMESTAMP < T1.CG_DTR_CRG) ) SCO,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM EDI.EDI2_NFE EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = EDI.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)) EDI
                                  FROM (SELECT REM_FISCAL.EP_DSC_RSM,
                                               REM_FISCAL.EP_ID_EMP,
                                               CORRENT.EP_DSC_RSM AS DSC_CORRENT,
                                               CORRENT.EP_ID_EMP AS ID_CORRENT,
                                               cg.CG_DTR_CRG,
                                               NF.NFE_CHAVE_NFE,
                                               vp.vb_cod_tra,
                                               C.SIGLA_UF_FERROVIA,
                                               C.DP_ID_DP,                       
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_or
                                                                    ) ORIGEM,         
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_ds
                                                                    ) DESTINO,
                                               FC.FX_COD_SEGMENTO MERCADORIA,
                                               TRUNC(CG.CG_TIMESTAMP) AS CG_TIMESTAMP
                                          FROM VAGAO_PEDIDO         VP,
                                               FLUXO_COMERCIAL      FC,
                                               CONTRATO             CT,
                                               EMPRESA              REM_FISCAL,
                                               EMPRESA              CORRENT,
                                               CARREGAMENTO         CG,
                                               DETALHE_CARREGAMENTO DC,
                                               ITEM_DESPACHO        ITD,
                                               NOTA_FISCAL          NF,
                                               CTE                  C
                                         WHERE VP.FX_ID_FLX = FC.FX_ID_FLX
                                           AND FC.CZ_ID_CTT = CT.CZ_ID_CTT
                                           AND CT.CODREMFISCAL = REM_FISCAL.EP_SIG_EMP
                                           AND CT.EP_ID_EMP_COR = CORRENT.EP_ID_EMP
                                           AND CG.CG_ID_CAR = VP.CG_ID_CAR
                                           AND CG.CG_ID_CAR = DC.CG_ID_CAR
                                           AND DC.DC_ID_CRG = ITD.DC_ID_CRG
                                           AND NF.DP_ID_DP = ITD.DP_ID_DP
                                           AND ITD.DP_ID_DP = C.DP_ID_DP
                                           AND ITD.VG_ID_VG = C.VG_ID_VG
                                           AND C.ID_CTE =
                                               (SELECT MAX(C1.ID_CTE)
                                                  FROM CTE C1
                                                 WHERE C1.DP_ID_DP = C.DP_ID_DP)
                                           AND NF.NFE_CHAVE_NFE IS NOT NULL
                                           AND CG.CG_DTR_CRG between 
                                                    trunc(sysdate-30) 
                                                and trunc(sysdate)
                                         GROUP BY REM_FISCAL.EP_DSC_RSM,
                                                  REM_FISCAL.EP_ID_EMP,
                                                  CORRENT.EP_DSC_RSM,
                                                  CORRENT.EP_ID_EMP,
                                                  cg.CG_DTR_CRG,
                                                  NF.NFE_CHAVE_NFE,
                                                  vp.vb_cod_tra,
                                                  C.SIGLA_UF_FERROVIA,
                                                  CG.CG_TIMESTAMP,
                                                  C.DP_ID_DP,
                                                  fc.ao_id_est_or,
                                                  fc.ao_id_est_ds,
                                                  FC.FX_COD_SEGMENTO) T1) T2
                          WHERE T2.SISTEMA NOT IN ('FCA','MRS') OR T2.SISTEMA IS NULL
                          GROUP BY T2.SIGLA_UF_FERROVIA,
                                  T2.CG_TIMESTAMP,
                                  T2.EP_DSC_RSM,
                                  T2.DSC_CORRENT,
                                  T2.FATURAMENTO,
                                         T2.SISTEMA,
                                         T2.ORIGEM,
                                         T2.DESTINO,
                                         T2.MERCADORIA
                         order by 2 desc";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AcompanhamentoFatDto)));

                return query.List<AcompanhamentoFatDto>();
            }
        }

        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        public IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupados()
        {
            var sqlQuery = @"SELECT T2.SIGLA_UF_FERROVIA AS ""Estado"",
                               T2.CG_TIMESTAMP AS ""TimeStamp"",
                                CASE T2.EP_DSC_RSM
                                WHEN 'RAÍZEN COM'	 THEN 'RAÍZEN'	
                                WHEN 'RAÍZEN ENE'	 THEN 'RAÍZEN'
                                WHEN 'BF'	 THEN	'BF'	
                                WHEN 'BF CEREAIS'	 THEN 'BF'  
                                WHEN 'C. VALE'   THEN 'C. VALE' 
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C VALE'   THEN 'C. VALE'  
                                WHEN 'C. VALE'   THEN 'C. VALE'  
                                WHEN 'CVALE'   THEN 'C. VALE'  
                                WHEN 'COOPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'COPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'IPP'   THEN 'IPIRANGA'  
                                WHEN 'IPIRANGA'   THEN 'IPIRANGA'  
                                WHEN 'LOUIS DREY'   THEN 'LOUIS DREY'  
                                WHEN 'LDC BIOENE'   THEN 'LOUIS DREY'  
                                WHEN 'SEARA ALIM'   THEN 'SEARA'  
                                WHEN 'SEARA COME'   THEN 'SEARA'  
                                WHEN 'USINA MOEM'   THEN 'USINA'  
                                WHEN 'USINA'   THEN 'USINA'	
                                WHEN 'VALE'	 THEN 'VALE'	
                                WHEN 'VALE DO IV'	 THEN 'VALE'	
                                WHEN 'SEMEGRAO'	 THEN 'SEMEGRÃO'	
                                WHEN 'SEMEGRÃO'	 THEN 'SEMEGRÃO'	
                                WHEN 'WHITE MART'	 THEN 'WHITE MART'	
                                WHEN 'BELAGRICOL'	 THEN 'AMAGGI'	
                               ELSE  T2.EP_DSC_RSM END AS ""Empresa"",
                               T2.DSC_CORRENT AS ""Correntista"",
                               T2.ORIGEM AS ""Origem"", 
                               T2.DESTINO AS ""Destino"",
                               T2.MERCADORIA AS ""Mercadoria"",
                               T2.FATURAMENTO AS ""Faturamento"",  
                               T2.SISTEMA as ""Sistema"",    
                               COUNT(DISTINCT NFE) AS ""TotalNfe"" ,
                               (COUNT(DISTINCT SEFAZ)+COUNT(DISTINCT EDI)) AS ""ComXml"",
                               COUNT(DISTINCT SCO) AS ""SemXml""
                          FROM (SELECT T1.NFE_CHAVE_NFE NFE,
                                       T1.EP_DSC_RSM,
                                       T1.DSC_CORRENT,
                                       T1.SIGLA_UF_FERROVIA,
                                       T1.CG_TIMESTAMP,
                                        case
                                         when t1.vb_cod_tra = 'BOI' OR t1.vb_cod_tra = 'OP00BaDe' THEN
                                          'MANUAL'
                                          ELSE (NVL((SELECT CASE
                                                            WHEN bo.NOM_ARQ = 'EDI' THEN
                                                             'EDI NOVO'
                                                            WHEN bo.NOM_ARQ = 'CAALL' THEN
                                                             'EDI CAALL'
                                                         END
                                                 FROM  BO_BOLAR_BO BO, 
                                                       BO_BOLAR_VAGAO BV
                                                WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                                  AND BV.ID_BO_ID = BO.ID_BO
                                                  AND BO.IDT_SIT = 'D'
                                                  AND BO.NOM_ARQ IN ( 'CAALL' , 'EDI' )
                                                  AND ROWNUM = 1), 'EDI ANTIGO'))
                                           END FATURAMENTO,
                                       ORIGEM,
                                       DESTINO,
                                       MERCADORIA,
                                       (SELECT BO.AO_COD_AOP
                                             FROM  BO_BOLAR_BO BO, BO_BOLAR_VAGAO BV
                                            WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                              AND BV.ID_BO_ID = BO.ID_BO
                                              AND 1=1
                                              AND BO.IDT_SIT = 'D'
                                              ) SISTEMA,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM NFE_DISTRIBUICAO EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG) SEFAZ,
                                       (SELECT NFE_CHAVE
                                          FROM NFE_SIMCONSULTAS NS
                                         WHERE NS.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND NS.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)
                                          AND NOT EXISTS
                                         (SELECT 1
                                                  FROM EDI.EDI2_NFE X
                                                 WHERE X.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND X.NFE_TIMESTAMP < T1.CG_DTR_CRG) ) SCO,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM EDI.EDI2_NFE EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = EDI.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)) EDI
                                  FROM (SELECT REM_FISCAL.EP_DSC_RSM,
                                               REM_FISCAL.EP_ID_EMP,
                                               CORRENT.EP_DSC_RSM AS DSC_CORRENT,
                                               CORRENT.EP_ID_EMP AS ID_CORRENT,
                                               cg.CG_DTR_CRG,
                                               NF.NFE_CHAVE_NFE,
                                               vp.vb_cod_tra,
                                               C.SIGLA_UF_FERROVIA,
                                               C.DP_ID_DP,                       
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_or
                                                                    ) ORIGEM,         
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_ds
                                                                    ) DESTINO,
                                               FC.FX_COD_SEGMENTO MERCADORIA,
                                               TRUNC(CG.CG_TIMESTAMP) AS CG_TIMESTAMP
                                          FROM VAGAO_PEDIDO         VP,
                                               FLUXO_COMERCIAL      FC,
                                               CONTRATO             CT,
                                               EMPRESA              REM_FISCAL,
                                               EMPRESA              CORRENT,
                                               CARREGAMENTO         CG,
                                               DETALHE_CARREGAMENTO DC,
                                               ITEM_DESPACHO        ITD,
                                               NOTA_FISCAL          NF,
                                               CTE                  C
                                         WHERE VP.FX_ID_FLX = FC.FX_ID_FLX
                                           AND FC.CZ_ID_CTT = CT.CZ_ID_CTT
                                           AND CT.CODREMFISCAL = REM_FISCAL.EP_SIG_EMP
                                           AND CT.EP_ID_EMP_COR = CORRENT.EP_ID_EMP
                                           AND CG.CG_ID_CAR = VP.CG_ID_CAR
                                           AND CG.CG_ID_CAR = DC.CG_ID_CAR
                                           AND DC.DC_ID_CRG = ITD.DC_ID_CRG
                                           AND NF.DP_ID_DP = ITD.DP_ID_DP
                                           AND ITD.DP_ID_DP = C.DP_ID_DP
                                           AND ITD.VG_ID_VG = C.VG_ID_VG
                                           AND C.ID_CTE =
                                               (SELECT MAX(C1.ID_CTE)
                                                  FROM CTE C1
                                                 WHERE C1.DP_ID_DP = C.DP_ID_DP)
                                           AND NF.NFE_CHAVE_NFE IS NOT NULL
                                           AND CG.CG_DTR_CRG between 
                                                    trunc(sysdate-30) 
                                                and trunc(sysdate)
                                         GROUP BY REM_FISCAL.EP_DSC_RSM,
                                                  REM_FISCAL.EP_ID_EMP,
                                                  CORRENT.EP_DSC_RSM,
                                                  CORRENT.EP_ID_EMP,
                                                  cg.CG_DTR_CRG,
                                                  NF.NFE_CHAVE_NFE,
                                                  vp.vb_cod_tra,
                                                  C.SIGLA_UF_FERROVIA,
                                                  CG.CG_TIMESTAMP,
                                                  C.DP_ID_DP,
                                                  fc.ao_id_est_or,
                                                  fc.ao_id_est_ds,
                                                  FC.FX_COD_SEGMENTO) T1) T2
                          WHERE T2.SISTEMA NOT IN ('FCA','MRS') OR T2.SISTEMA IS NULL
                          GROUP BY T2.SIGLA_UF_FERROVIA,
                                  T2.CG_TIMESTAMP,
                                  T2.EP_DSC_RSM,
                                  T2.DSC_CORRENT,
                                  T2.FATURAMENTO,
                                         T2.SISTEMA,
                                         T2.ORIGEM,
                                         T2.DESTINO,
                                         T2.MERCADORIA
                         order by 2 desc";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AcompanhamentoFatDto)));

                return query.List<AcompanhamentoFatDto>();
            }
        }


        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        public IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupadosMalhaNorte()
        {
            var sqlQuery = @"SELECT T2.SIGLA_UF_FERROVIA AS ""Estado"",
                               T2.CG_TIMESTAMP AS ""TimeStamp"",
                                CASE T2.EP_DSC_RSM
                                WHEN 'RAÍZEN COM'	 THEN 'RAÍZEN'	
                                WHEN 'RAÍZEN ENE'	 THEN 'RAÍZEN'
                                WHEN 'BF'	 THEN	'BF'	
                                WHEN 'BF CEREAIS'	 THEN 'BF'  
                                WHEN 'C. VALE'   THEN 'C. VALE' 
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C VALE'   THEN 'C. VALE'  
                                WHEN 'C. VALE'   THEN 'C. VALE'  
                                WHEN 'CVALE'   THEN 'C. VALE'  
                                WHEN 'COOPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'COPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'IPP'   THEN 'IPIRANGA'  
                                WHEN 'IPIRANGA'   THEN 'IPIRANGA'  
                                WHEN 'LOUIS DREY'   THEN 'LOUIS DREY'  
                                WHEN 'LDC BIOENE'   THEN 'LOUIS DREY'  
                                WHEN 'SEARA ALIM'   THEN 'SEARA'  
                                WHEN 'SEARA COME'   THEN 'SEARA'  
                                WHEN 'USINA MOEM'   THEN 'USINA'  
                                WHEN 'USINA'   THEN 'USINA'	
                                WHEN 'VALE'	 THEN 'VALE'	
                                WHEN 'VALE DO IV'	 THEN 'VALE'	
                                WHEN 'SEMEGRAO'	 THEN 'SEMEGRÃO'	
                                WHEN 'SEMEGRÃO'	 THEN 'SEMEGRÃO'	
                                WHEN 'WHITE MART'	 THEN 'WHITE MART'	
                                WHEN 'BELAGRICOL'	 THEN 'AMAGGI'	
                               ELSE  T2.EP_DSC_RSM END AS ""Empresa"",
                               T2.DSC_CORRENT AS ""Correntista"",
                               T2.ORIGEM AS ""Origem"", 
                               T2.DESTINO AS ""Destino"",
                               T2.MERCADORIA AS ""Mercadoria"",
                               T2.FATURAMENTO AS ""Faturamento"",  
                               T2.SISTEMA as ""Sistema"",    
                               COUNT(DISTINCT NFE) AS ""TotalNfe"" ,
                               (COUNT(DISTINCT SEFAZ)+COUNT(DISTINCT EDI)) AS ""ComXml"",
                               COUNT(DISTINCT SCO) AS ""SemXml""
                          FROM (SELECT T1.NFE_CHAVE_NFE NFE,
                                       T1.EP_DSC_RSM,
                                       T1.DSC_CORRENT,
                                       T1.SIGLA_UF_FERROVIA,
                                       T1.CG_TIMESTAMP,
                                        case
                                         when t1.vb_cod_tra = 'BOI' OR t1.vb_cod_tra = 'OP00BaDe' THEN
                                          'MANUAL'
                                          ELSE (NVL((SELECT CASE
                                                            WHEN bo.NOM_ARQ = 'EDI' THEN
                                                             'EDI NOVO'
                                                            WHEN bo.NOM_ARQ = 'CAALL' THEN
                                                             'EDI CAALL'
                                                         END
                                                 FROM  BO_BOLAR_BO BO, 
                                                       BO_BOLAR_VAGAO BV
                                                WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                                  AND BV.ID_BO_ID = BO.ID_BO
                                                  AND BO.IDT_SIT = 'D'
                                                  AND BO.NOM_ARQ IN ( 'CAALL' , 'EDI' )
                                                  AND ROWNUM = 1), 'EDI ANTIGO'))
                                           END FATURAMENTO,
                                       ORIGEM,
                                       DESTINO,
                                       MERCADORIA,
                                       (SELECT BO.AO_COD_AOP
                                             FROM  BO_BOLAR_BO BO, BO_BOLAR_VAGAO BV
                                            WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                              AND BV.ID_BO_ID = BO.ID_BO
                                              AND 1=1
                                              AND BO.IDT_SIT = 'D'
                                              ) SISTEMA,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM NFE_DISTRIBUICAO EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG) SEFAZ,
                                       (SELECT NFE_CHAVE
                                          FROM NFE_SIMCONSULTAS NS
                                         WHERE NS.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND NS.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)
                                          AND NOT EXISTS
                                         (SELECT 1
                                                  FROM EDI.EDI2_NFE X
                                                 WHERE X.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND X.NFE_TIMESTAMP < T1.CG_DTR_CRG) ) SCO,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM EDI.EDI2_NFE EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = EDI.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)) EDI
                                  FROM (SELECT REM_FISCAL.EP_DSC_RSM,
                                               REM_FISCAL.EP_ID_EMP,
                                               CORRENT.EP_DSC_RSM AS DSC_CORRENT,
                                               CORRENT.EP_ID_EMP AS ID_CORRENT,
                                               cg.CG_DTR_CRG,
                                               NF.NFE_CHAVE_NFE,
                                               vp.vb_cod_tra,
                                               C.SIGLA_UF_FERROVIA,
                                               C.DP_ID_DP,                       
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_or
                                                                    ) ORIGEM,         
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_ds
                                                                    ) DESTINO,
                                               FC.FX_COD_SEGMENTO MERCADORIA,
                                               TRUNC(CG.CG_TIMESTAMP) AS CG_TIMESTAMP
                                          FROM VAGAO_PEDIDO         VP,
                                               FLUXO_COMERCIAL      FC,
                                               CONTRATO             CT,
                                               EMPRESA              REM_FISCAL,
                                               EMPRESA              CORRENT,
                                               CARREGAMENTO         CG,
                                               DETALHE_CARREGAMENTO DC,
                                               ITEM_DESPACHO        ITD,
                                               NOTA_FISCAL          NF,
                                               CTE                  C
                                         WHERE 
                                                (C.SIGLA_UF_FERROVIA IN ('MT','MS')
                                                OR (C.SIGLA_UF_FERROVIA = 'SP' AND REM_FISCAL.EP_DSC_RSM NOT IN ('INTERSEMEN','INTERCEMEN')))                         
                         
                                         AND VP.FX_ID_FLX = FC.FX_ID_FLX
                                           AND FC.CZ_ID_CTT = CT.CZ_ID_CTT
                                           AND CT.CODREMFISCAL = REM_FISCAL.EP_SIG_EMP
                                           AND CT.EP_ID_EMP_COR = CORRENT.EP_ID_EMP
                                           AND CG.CG_ID_CAR = VP.CG_ID_CAR
                                           AND CG.CG_ID_CAR = DC.CG_ID_CAR
                                           AND DC.DC_ID_CRG = ITD.DC_ID_CRG
                                           AND NF.DP_ID_DP = ITD.DP_ID_DP
                                           AND ITD.DP_ID_DP = C.DP_ID_DP
                                           AND ITD.VG_ID_VG = C.VG_ID_VG
                                           AND C.ID_CTE =
                                               (SELECT MAX(C1.ID_CTE)
                                                  FROM CTE C1
                                                 WHERE C1.DP_ID_DP = C.DP_ID_DP)
                                           AND NF.NFE_CHAVE_NFE IS NOT NULL
                                           AND CG.CG_DTR_CRG between 
                                                    trunc(sysdate-30) 
                                                and trunc(sysdate)
                                         GROUP BY REM_FISCAL.EP_DSC_RSM,
                                                  REM_FISCAL.EP_ID_EMP,
                                                  CORRENT.EP_DSC_RSM,
                                                  CORRENT.EP_ID_EMP,
                                                  cg.CG_DTR_CRG,
                                                  NF.NFE_CHAVE_NFE,
                                                  vp.vb_cod_tra,
                                                  C.SIGLA_UF_FERROVIA,
                                                  CG.CG_TIMESTAMP,
                                                  C.DP_ID_DP,
                                                  fc.ao_id_est_or,
                                                  fc.ao_id_est_ds,
                                                  FC.FX_COD_SEGMENTO) T1) T2
                          WHERE T2.SISTEMA NOT IN ('FCA','MRS') OR T2.SISTEMA IS NULL
                          GROUP BY T2.SIGLA_UF_FERROVIA,
                                  T2.CG_TIMESTAMP,
                                  T2.EP_DSC_RSM,
                                  T2.DSC_CORRENT,
                                  T2.FATURAMENTO,
                                         T2.SISTEMA,
                                         T2.ORIGEM,
                                         T2.DESTINO,
                                         T2.MERCADORIA
                         order by 2 desc";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AcompanhamentoFatDto)));

                return query.List<AcompanhamentoFatDto>();
            }
        }

        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        public IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupadosMalhaSul()
        {
            var sqlQuery = @"SELECT T2.SIGLA_UF_FERROVIA AS ""Estado"",
                               T2.CG_TIMESTAMP AS ""TimeStamp"",
                                CASE T2.EP_DSC_RSM
                                WHEN 'RAÍZEN COM'	 THEN 'RAÍZEN'	
                                WHEN 'RAÍZEN ENE'	 THEN 'RAÍZEN'
                                WHEN 'BF'	 THEN	'BF'	
                                WHEN 'BF CEREAIS'	 THEN 'BF'  
                                WHEN 'C. VALE'   THEN 'C. VALE' 
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C.VALE'   THEN 'C. VALE'  
                                WHEN 'C VALE'   THEN 'C. VALE'  
                                WHEN 'C. VALE'   THEN 'C. VALE'  
                                WHEN 'CVALE'   THEN 'C. VALE'  
                                WHEN 'COOPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'COPAGRIL'   THEN 'COOPAGRIL'  
                                WHEN 'IPP'   THEN 'IPIRANGA'  
                                WHEN 'IPIRANGA'   THEN 'IPIRANGA'  
                                WHEN 'LOUIS DREY'   THEN 'LOUIS DREY'  
                                WHEN 'LDC BIOENE'   THEN 'LOUIS DREY'  
                                WHEN 'SEARA ALIM'   THEN 'SEARA'  
                                WHEN 'SEARA COME'   THEN 'SEARA'  
                                WHEN 'USINA MOEM'   THEN 'USINA'  
                                WHEN 'USINA'   THEN 'USINA'	
                                WHEN 'VALE'	 THEN 'VALE'	
                                WHEN 'VALE DO IV'	 THEN 'VALE'	
                                WHEN 'SEMEGRAO'	 THEN 'SEMEGRÃO'	
                                WHEN 'SEMEGRÃO'	 THEN 'SEMEGRÃO'	
                                WHEN 'WHITE MART'	 THEN 'WHITE MART'	
                                WHEN 'BELAGRICOL'	 THEN 'AMAGGI'	
                               ELSE  T2.EP_DSC_RSM END AS ""Empresa"",
                               T2.DSC_CORRENT AS ""Correntista"",
                               T2.ORIGEM AS ""Origem"", 
                               T2.DESTINO AS ""Destino"",
                               T2.MERCADORIA AS ""Mercadoria"",
                               T2.FATURAMENTO AS ""Faturamento"",  
                               T2.SISTEMA as ""Sistema"",    
                               COUNT(DISTINCT NFE) AS ""TotalNfe"" ,
                               (COUNT(DISTINCT SEFAZ)+COUNT(DISTINCT EDI)) AS ""ComXml"",
                               COUNT(DISTINCT SCO) AS ""SemXml""
                          FROM (SELECT T1.NFE_CHAVE_NFE NFE,
                                       T1.EP_DSC_RSM,
                                       T1.DSC_CORRENT,
                                       T1.SIGLA_UF_FERROVIA,
                                       T1.CG_TIMESTAMP,
                                      case
                                         when t1.vb_cod_tra = 'BOI' OR t1.vb_cod_tra = 'OP00BaDe' THEN
                                          'MANUAL'
                                          ELSE (NVL((SELECT CASE
                                                            WHEN bo.NOM_ARQ = 'EDI' THEN
                                                             'EDI NOVO'
                                                            WHEN bo.NOM_ARQ = 'CAALL' THEN
                                                             'EDI CAALL'
                                                         END
                                                 FROM  BO_BOLAR_BO BO, 
                                                       BO_BOLAR_VAGAO BV
                                                WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                                  AND BV.ID_BO_ID = BO.ID_BO
                                                  AND BO.IDT_SIT = 'D'
                                                  AND BO.NOM_ARQ IN ( 'CAALL' , 'EDI' )
                                                  AND ROWNUM = 1), 'EDI ANTIGO'))
                                           END FATURAMENTO,
                                       ORIGEM,
                                       DESTINO,
                                       MERCADORIA,
                                       (SELECT BO.AO_COD_AOP
                                             FROM  BO_BOLAR_BO BO, BO_BOLAR_VAGAO BV
                                            WHERE BV.DP_ID_DP = T1.DP_ID_DP
                                              AND BV.ID_BO_ID = BO.ID_BO
                                              AND 1=1
                                              AND BO.IDT_SIT = 'D'
                                              ) SISTEMA,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM NFE_DISTRIBUICAO EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG) SEFAZ,
                                       (SELECT NFE_CHAVE
                                          FROM NFE_SIMCONSULTAS NS
                                         WHERE NS.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND NS.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)
                                          AND NOT EXISTS
                                         (SELECT 1
                                                  FROM EDI.EDI2_NFE X
                                                 WHERE X.NFE_CHAVE = NS.NFE_CHAVE
                                                   AND X.NFE_TIMESTAMP < T1.CG_DTR_CRG) ) SCO,
                                       (SELECT EDI.NFE_CHAVE
                                          FROM EDI.EDI2_NFE EDI
                                         WHERE EDI.NFE_CHAVE = T1.NFE_CHAVE_NFE
                                           AND EDI.NFE_TIMESTAMP < T1.CG_DTR_CRG
                                           AND NOT EXISTS
                                         (SELECT 1
                                                  FROM NFE_DISTRIBUICAO N
                                                 WHERE N.NFE_CHAVE = EDI.NFE_CHAVE
                                                   AND N.NFE_TIMESTAMP < T1.CG_DTR_CRG)) EDI
                                  FROM (SELECT REM_FISCAL.EP_DSC_RSM,
                                               REM_FISCAL.EP_ID_EMP,
                                               CORRENT.EP_DSC_RSM AS DSC_CORRENT,
                                               CORRENT.EP_ID_EMP AS ID_CORRENT,
                                               cg.CG_DTR_CRG,
                                               NF.NFE_CHAVE_NFE,
                                               vp.vb_cod_tra,
                                               C.SIGLA_UF_FERROVIA,
                                               C.DP_ID_DP,                       
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_or
                                                                    ) ORIGEM,         
                                             (SELECT ao.AO_COD_AOP
                                             FROM  area_operacional ao
                                            WHERE ao.ao_id_ao = fc.ao_id_est_ds
                                                                    ) DESTINO,
                                               FC.FX_COD_SEGMENTO MERCADORIA,
                                               TRUNC(CG.CG_TIMESTAMP) AS CG_TIMESTAMP
                                          FROM VAGAO_PEDIDO         VP,
                                               FLUXO_COMERCIAL      FC,
                                               CONTRATO             CT,
                                               EMPRESA              REM_FISCAL,
                                               EMPRESA              CORRENT,
                                               CARREGAMENTO         CG,
                                               DETALHE_CARREGAMENTO DC,
                                               ITEM_DESPACHO        ITD,
                                               NOTA_FISCAL          NF,
                                               CTE                  C
                                         WHERE 
                                                (C.SIGLA_UF_FERROVIA IN ('PR','SC','RS')
                                                OR (C.SIGLA_UF_FERROVIA = 'SP' AND REM_FISCAL.EP_DSC_RSM IN ('INTERSEMEN','INTERCEMEN')))                         
                         
                                         AND VP.FX_ID_FLX = FC.FX_ID_FLX
                                           AND FC.CZ_ID_CTT = CT.CZ_ID_CTT
                                           AND CT.CODREMFISCAL = REM_FISCAL.EP_SIG_EMP
                                           AND CT.EP_ID_EMP_COR = CORRENT.EP_ID_EMP
                                           AND CG.CG_ID_CAR = VP.CG_ID_CAR
                                           AND CG.CG_ID_CAR = DC.CG_ID_CAR
                                           AND DC.DC_ID_CRG = ITD.DC_ID_CRG
                                           AND NF.DP_ID_DP = ITD.DP_ID_DP
                                           AND ITD.DP_ID_DP = C.DP_ID_DP
                                           AND ITD.VG_ID_VG = C.VG_ID_VG
                                           AND C.ID_CTE =
                                               (SELECT MAX(C1.ID_CTE)
                                                  FROM CTE C1
                                                 WHERE C1.DP_ID_DP = C.DP_ID_DP)
                                           AND NF.NFE_CHAVE_NFE IS NOT NULL
                                           AND CG.CG_DTR_CRG between 
                                                    trunc(sysdate-30) 
                                                and trunc(sysdate)
                                         GROUP BY REM_FISCAL.EP_DSC_RSM,
                                                  REM_FISCAL.EP_ID_EMP,
                                                  CORRENT.EP_DSC_RSM,
                                                  CORRENT.EP_ID_EMP,
                                                  cg.CG_DTR_CRG,
                                                  NF.NFE_CHAVE_NFE,
                                                  vp.vb_cod_tra,
                                                  C.SIGLA_UF_FERROVIA,
                                                  CG.CG_TIMESTAMP,
                                                  C.DP_ID_DP,
                                                  fc.ao_id_est_or,
                                                  fc.ao_id_est_ds,
                                                  FC.FX_COD_SEGMENTO) T1) T2
                          WHERE T2.SISTEMA NOT IN ('FCA','MRS') OR T2.SISTEMA IS NULL
                          GROUP BY T2.SIGLA_UF_FERROVIA,
                                  T2.CG_TIMESTAMP,
                                  T2.EP_DSC_RSM,
                                  T2.DSC_CORRENT,
                                  T2.FATURAMENTO,
                                         T2.SISTEMA,
                                         T2.ORIGEM,
                                         T2.DESTINO,
                                         T2.MERCADORIA
                         order by 2 desc";
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(AcompanhamentoFatDto)));

                return query.List<AcompanhamentoFatDto>();
            }
        }


        /// <summary>
        /// Obtém os fluxos associados aos dados da nota fiscal (remetente e destinatario fiscal)
        /// </summary>
        /// <param name="chaveNfe"> chave nfe </param>
        /// <returns> Retorna lista de Objetos do tipo NotaFiscalFluxoAssociacaoDto </returns>
        public IList<NotaFiscalFluxoAssociacaoDto> ObterFluxosAssociadosChaveNfe(DetalhesPaginacao detalhesPaginacao,
                                                                                 List<string> chavesNfe,
                                                                                 bool somenteVigentes)
        {

            var sqlQuery = @"  SELECT DISTINCT 
                                      NFE.NFE_CHAVE                          AS ""ChaveNfe""
                                    , FC.FX_COD_FLX                          AS ""Fluxo""
                                    , ORIGEM.AO_COD_AOP                      AS ""Origem""
                                    , DESTINO.AO_COD_AOP                     AS ""Destino""
                                    , REMETENTE_FISCAL.Ep_Cgc_Emp            AS ""RemetenteFiscalCnpj""
                                    , REMETENTE_FISCAL.EP_DSC_DTL            AS ""RemetenteFiscal""
                                    , DESTINATARIO_FISCAL.Ep_Cgc_Emp         AS ""DestinatarioFiscalCnpj""
                                    , DESTINATARIO_FISCAL.EP_DSC_DTL         AS ""DestinatarioFiscal""
                                    , TOMADOR.EP_CGC_EMP                     AS ""TomadorCnpj""
                                    , TOMADOR.EP_DSC_DTL                     AS ""Tomador""
                                    , EXPEDIDOR.EP_DSC_DTL                   AS ""Expedidor""
                                    , RECEBEDOR.EP_DSC_DTL                   AS ""Recebedor""
                                    , MERCADORIA.MC_DDT_PT                   AS ""Mercadoria""
                                    , FC.FX_DAT_VIG                          AS ""FluxoVigenciaFinal""
                              FROM VW_NFE             NFE
                              JOIN FLUXO_COMERCIAL          FC                    
                                                          ON FC.fx_cnpj_rem_fiscal = NFE.NFE_CNPJ_ORI 
                                                          AND FC.fx_cnpj_des_fiscal = NFE.NFE_CNPJ_DST
                                                          AND FC.FX_MODELO_NF = '55'
                                                          AND EXISTS (
                                                           SELECT 1
                                                           FROM ASSOCIA_FLUXO_VIG AFV
                                                           WHERE AFV.FX_ID_FLX =FC.FX_ID_FLX
                                                          )
                                                          {FiltroSomenteVigentes}
                              JOIN AREA_OPERACIONAL ORIGEM  ON ORIGEM.AO_ID_AO   = FC.AO_ID_EST_OR
                              JOIN AREA_OPERACIONAL DESTINO ON DESTINO.AO_ID_AO = FC.AO_ID_EST_DS
                              JOIN EMPRESA EXPEDIDOR ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
                              JOIN EMPRESA RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
                              JOIN EMPRESA TOMADOR   ON TOMADOR.EP_ID_EMP = FC.EP_ID_EMP_COR
                              JOIN EMPRESA REMETENTE_FISCAL ON REMETENTE_FISCAL.EP_CGC_EMP = NFE.NFE_CNPJ_ORI
                                AND REMETENTE_FISCAL.EP_ID_EMP = 
                                        Nvl( ( select EPR.EP_ID_EMP 
                                               from EMPRESA EPR 
                                               WHERE EPR.EP_CGC_EMP = REMETENTE_FISCAL.EP_CGC_EMP
                                               and EPR.EP_ID_EMP = EXPEDIDOR.EP_ID_EMP
                                               and rownum = 1), 
                                               ( select EPR.EP_ID_EMP 
                                               from EMPRESA EPR 
                                               WHERE EPR.EP_CGC_EMP = REMETENTE_FISCAL.EP_CGC_EMP 
                                               and rownum = 1))

                              JOIN EMPRESA DESTINATARIO_FISCAL ON DESTINATARIO_FISCAL.EP_CGC_EMP = NFE.NFE_CNPJ_DST
                               and DESTINATARIO_FISCAL.EP_ID_EMP = 
                                        Nvl( ( select EPD.EP_ID_EMP 
                                                from EMPRESA EPD 
                                                WHERE EPD.EP_CGC_EMP = DESTINATARIO_FISCAL.EP_CGC_EMP 
                                                and EPD.EP_ID_EMP = RECEBEDOR.EP_ID_EMP
                                                and rownum = 1),
                                                ( select EPD.EP_ID_EMP 
                                                from EMPRESA EPD 
                                                WHERE EPD.EP_CGC_EMP = DESTINATARIO_FISCAL.EP_CGC_EMP 
                                                and rownum = 1))

                              JOIN MERCADORIA MERCADORIA ON MERCADORIA.MC_ID_MRC = FC.MC_ID_MRC
                              WHERE NFE.NFE_CHAVE IN (:chavesNfe) 
                              {ORDENACAO} ";



            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            string colOrdenacaoIndex = "2";
            string ordenacaoDirecao = "ASC";

            if (colunasOrdenacao.Count > 0)
                colOrdenacaoIndex = colunasOrdenacao[0];
            if (direcoesOrdenacao.Count > 0)
                ordenacaoDirecao = direcoesOrdenacao[0];

            else if (colOrdenacaoIndex.ToLower() == "origem")
                colOrdenacaoIndex = "3";
            else if (colOrdenacaoIndex.ToLower() == "destino")
                colOrdenacaoIndex = "4";
            else if (colOrdenacaoIndex.ToLower() == "remetentefiscal")
                colOrdenacaoIndex = "6";
            else if (colOrdenacaoIndex.ToLower() == "destinatariofiscal")
                colOrdenacaoIndex = "8";
            else if (colOrdenacaoIndex.ToLower() == "expedidor")
                colOrdenacaoIndex = "9";
            else if (colOrdenacaoIndex.ToLower() == "recebedor")
                colOrdenacaoIndex = "10";
            else if (colOrdenacaoIndex.ToLower() == "mercadoria")
                colOrdenacaoIndex = "11";
            else
            {
                colOrdenacaoIndex = "2";
                ordenacaoDirecao = "ASC";
            }

            if (somenteVigentes)
                sqlQuery = sqlQuery.Replace("{FiltroSomenteVigentes}", "AND FC.FX_DAT_VIG >= TRUNC(SYSDATE-1)");
            else
                sqlQuery = sqlQuery.Replace("{FiltroSomenteVigentes}", "");

            sqlQuery = sqlQuery.Replace("{ORDENACAO}", " ORDER BY " + colOrdenacaoIndex + " " + ordenacaoDirecao);

            #endregion


            var parametros = new List<Action<IQuery>>();
            parametros.Add(q => q.SetParameterList("chavesNfe", chavesNfe));

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaFiscalFluxoAssociacaoDto)));

                return query.List<NotaFiscalFluxoAssociacaoDto>();
            }
        }

    }
}