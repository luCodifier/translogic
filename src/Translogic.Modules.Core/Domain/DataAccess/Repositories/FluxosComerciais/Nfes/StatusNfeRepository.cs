﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System;
    using System.Collections.Generic;
    using System.Data.OracleClient;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class StatusNfeRepository : NHRepository<StatusNfe, int>, IStatusNfeRepository
	{
		/// <summary>
		/// Obtém o status pela chave
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Objeto StatusNfe</returns>
		public StatusNfe ObterPorChaveNfe(string chaveNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ChaveNfe", chaveNfe));
			return ObterPrimeiro(criteria);
		}

        /// <summary>
        /// Obtém os despachos da Nfe por Chave 
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns>Lista DespachosNfe</returns>
        public IList<DespachosNfeDto> ObterDespachosPorChaveNfe(string chaveNfe)
        {
            ////using (var session = OpenSession())
            ////{
            ////    IQuery query = session.GetNamedQuery("ObterDespachosPorChaveNfe");
            ////    query.SetString("chaveNfe", chaveNfe);
            ////    query.SetResultTransformer(Transformers.AliasToBean(typeof(DespachosNfeDto)));
            ////    return query.List<DespachosNfeDto>();
            ////}

            using (ISession session = OpenSession())
            {
                string sqlQuery = @"
                        						SELECT 
                        							   VG.VG_COD_VAG AS  ""CodigoVagao"",
                        							   FC.FX_COD_FLX AS ""FluxoComercial"",
                        							   DP.DP_NUM_DSP AS ""NumeroDespacho"",
                        							   SD.SK_NUM_SRD AS ""SerieDespacho"",
                        							   DP.DP_DT AS ""DataDespacho"",
                        							   NF.NF_CONTEINER AS ""Conteiner"",
                        							   C.CHAVE_CTE AS ""ChaveCte"",
                        							   AOO.AO_COD_AOP AS ""Origem"",
                        							   AOD.AO_COD_AOP AS ""Destino"",
                                                       C.SITUACAO_ATUAL AS ""Status"",
                                                       IDES.Id_Num_Pdt AS ""PesoUtilizado""
                        						FROM STATUS_NFE SN,
                        							 NOTA_FISCAL NF,
                        							 CTE C,
                        							 VAGAO VG,
                        							 FLUXO_COMERCIAL FC,
                        							 DESPACHO DP,
                        							 SERIE_DESPACHO SD,
                        							 AREA_OPERACIONAL AOO,
                        							 AREA_OPERACIONAL AOD,
                                                     ITEM_DESPACHO IDES     
                        						WHERE NF.NFE_CHAVE_NFE = SN.NFE_CHAVE
                        						  AND C.DP_ID_DP = NF.DP_ID_DP
                        						  AND VG.VG_ID_VG = C.VG_ID_VG
                        						  AND FC.FX_ID_FLX = C.FX_ID_FLX
                        						  AND FC.AO_ID_EST_OR = AOO.AO_ID_AO
                        						  AND FC.AO_ID_EST_DS = AOD.AO_ID_AO
                        						  AND DP.DP_ID_DP = C.DP_ID_DP
                        						  AND SD.SK_IDT_SRD = DP.SK_IDT_SRD
                                                  AND IDES.DP_ID_DP = DP.DP_ID_DP 
                                                  AND IDES.VG_ID_VG = VG.VG_ID_VG 
                        						  AND SN.NFE_CHAVE = :chaveNfe";

                List<DespachosNfeDto> listaDespachos = new List<DespachosNfeDto>();
                DespachosNfeDto despacho = new DespachosNfeDto();

                OracleCommand cmd = new OracleCommand(sqlQuery);
                cmd.Connection = (OracleConnection)session.Connection;
                cmd.Parameters.Add(new OracleParameter("chaveNfe", chaveNfe));

                session.Transaction.Enlist(cmd);
                var rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    despacho = new DespachosNfeDto();

                    if (rdr["CodigoVagao"] != null)
                    {
                        despacho.CodigoVagao = rdr["CodigoVagao"].ToString();
                    }

                    if (rdr["FluxoComercial"] != null)
                    {
                        despacho.FluxoComercial = rdr["FluxoComercial"].ToString();
                    }

                    if (rdr["NumeroDespacho"] != null)
                    {
                        despacho.NumeroDespacho = (decimal)rdr["NumeroDespacho"];
                    }

                    if (rdr["DataDespacho"] != null)
                    {
                        despacho.DataDespacho = (DateTime)rdr["DataDespacho"];
                    }

                    if (rdr["Conteiner"] != null)
                    {
                        despacho.Conteiner = rdr["Conteiner"].ToString();
                    }

                    if (rdr["ChaveCte"] != null)
                    {
                        despacho.ChaveCte = rdr["ChaveCte"].ToString();
                    }

                    if (rdr["Origem"] != null)
                    {
                        despacho.Origem = rdr["Origem"].ToString();
                    }

                    if (rdr["Destino"] != null)
                    {
                        despacho.Destino = rdr["Destino"].ToString();
                    }

                    if (rdr["Status"] != null)
                    {
                        despacho.Status = rdr["Status"].ToString();
                    }

                    if (rdr["PesoUtilizado"] != null)
                    {
                        despacho.PesoUtilizado = (decimal)rdr["PesoUtilizado"];
                    }

                    listaDespachos.Add(despacho);
                }

                return listaDespachos;
            }
        }

        /// <summary>
        /// Verificar Quantidade Faturamento de uma NFe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>dto dos faturamentos da nfe</returns>
        public FaturamentosNfeDto VerificarQuantidadeFaturamento(string chaveNfe)
        {
            var querySql = @"
                select 
                    (select count(cd.id_cte_det) from cte_det cd where cd.nfe_chave_nfe = :chaveNfe) ""FaturamentosCte"",
                    (select count(nf.nf_idt_nfl) from nota_fiscal nf where nf.nfe_chave_nfe = :chaveNfe) ""FaturamentosFiscais""
                from 
                    dual
            ";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(querySql);
                query.SetString("chaveNfe", chaveNfe);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(FaturamentosNfeDto)));

                return query.List<FaturamentosNfeDto>().First();
            }
        }

	    /// <summary>
        /// Apagar Registros NFe
        /// </summary>
        /// <param name="chave">chave da nfe</param>
        public void ApagarRegistrosNFe(string chave)
        {
            using (var session = OpenSession())
            {
	            var comandos = new string[]
                    {
                        "delete from nfe_prod_simconsultas npsc where npsc.nfe_id_nfe = (select nsc.nfe_id_nfe from nfe_simconsultas nsc where nsc.nfe_chave = :chave)",
                        "delete from nfe_simconsultas nsc where nsc.nfe_chave = :chave",
                        "delete from edi.edi2_nfe_prod enfp where enfp.nfe_id_nfe = (select enf.nfe_chave from edi.edi2_nfe enf where enf.nfe_chave = :chave)",
                        "delete from edi.edi2_nfe enf where enf.nfe_chave = :chave",
                        "delete from status_nfe sn where sn.nfe_chave = :chave",
                    };

                foreach (var comando in comandos)
                {
                    var query = session.CreateSQLQuery(comando);
                    query.SetString("chave", chave);
                    query.ExecuteUpdate();
                }
            }
        }
	}
}