﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;

    /// <summary>
    ///  Classe de repositório do Log Dfe
    /// </summary>
    public class LogDfeSefazRepository : NHRepository<LogDfeSefaz, int>, ILogDfeSefazRepository
    {
    }
}