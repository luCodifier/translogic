﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
    /// Classe de repositório StatusNfeLogRepository
	/// </summary>
	public class StatusNfeLogRepository : NHRepository<StatusNfeLog, int>, IStatusNfeLogRepository
	{
	}
}