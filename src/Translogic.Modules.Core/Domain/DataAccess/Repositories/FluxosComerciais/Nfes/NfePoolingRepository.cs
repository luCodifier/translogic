namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

	/// <summary>
	/// Repositorio para <see cref="NfePoolingRepository"/>
	/// </summary>
	public class NfePoolingRepository : NHRepository<NfePooling, int>, INfePoolingRepository
	{
		/// <summary>
		/// Obtem todas as notas no pooling que permitam reenvio
		/// </summary>
		/// <param name="numTentativas">Numero de tentarivas</param>
		/// <param name="codigoUfIbge">Codigo da UF Ibge</param>
		/// <returns>Retorna as notas do pooling</returns>
		public IList<NfePooling> ObterNotasPooling(int numTentativas, string codigoUfIbge)
		{
			DetachedCriteria criteria = CriarCriteria();	
			criteria.Add(
				(Restrictions.Eq("PermiteReenvio", true) || Restrictions.IsNull("PermiteReenvio"))
				&& (Restrictions.Le("DataProxProcessamento", DateTime.Now) || Restrictions.IsNull("DataProxProcessamento"))
                && Restrictions.Le("NumTentativas", numTentativas) && Restrictions.Le("IndEmProcessamento", false)
				);
			if (codigoUfIbge == "OE")
			{
                criteria.Add(
                    Restrictions.Not(Restrictions.Like("ChaveNfe", "32%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "35%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "41%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "42%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "43%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "50%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "51%"))
                    && Restrictions.Not(Restrictions.Like("ChaveNfe", "52%"))
                    );
			}
			else
			{
				criteria.Add(Restrictions.Like("ChaveNfe", codigoUfIbge + "%"));
			}
			
			criteria.SetMaxResults(100);
			criteria.AddOrder(new Order("DataProxProcessamento", true));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Remove as Notas Antigas que n�o permite reenvio
		/// </summary>
		/// <param name="date"> Data de filtro</param>
		/// <param name="numMaxTentativas">Numero m�ximo de tentativas</param>
		/// <param name="codigoUfIbge">Codigo da UF Ibge</param>
		/// <remarks>Quando ficar muito antiga ou exceder o m�ximo de tentativas deve ser removido</remarks>
		public void RemoverNotasAntigasPooling(DateTime date, int numMaxTentativas, string codigoUfIbge)
		{
			using (ISession session = OpenSession())
			{
				string hql =
							  @"DELETE FROM NfePooling np
                                WHERE (DataNfe <= :dataFiltro and PermiteReenvio = :reenvio) or (NumTentativas >= :maxTentativas)
								AND np.ChaveNfe LIKE :codigoUfIbge
							    AND np.IndEmProcessamento = :indEmProcessamento";
				IQuery query = session.CreateQuery(hql);
				query.SetParameter("reenvio", false, new BooleanCharType());
				query.SetParameter("indEmProcessamento", false, new BooleanCharType());
				
				query.SetDateTime("dataFiltro", date);
				query.SetInt32("maxTentativas", numMaxTentativas);
				query.SetString("codigoUfIbge", codigoUfIbge + "%");
				query.ExecuteUpdate();
			}
		}

		/// <summary>
		/// Remove as Notas j� processadas que est�o no pooling
		/// </summary>
		/// <param name="codigoUfIbge">Codigo da UF IBGE</param>
		public void RemoverNotasExistentesPooling(string codigoUfIbge)
		{
			using (ISession session = OpenSession())
			{
				string hql = @"DELETE FROM NfePooling np
								WHERE EXISTS(SELECT 1 FROM NfeReadonly nfe  WHERE nfe.ChaveNfe = np.ChaveNfe)
								AND np.ChaveNfe LIKE :codigoUfIbge
								AND np.IndEmProcessamento = :indEmProcessamento";
				IQuery query = session.CreateQuery(hql);
				query.SetString("codigoUfIbge", codigoUfIbge + "%");
				query.SetParameter("indEmProcessamento", false, new BooleanCharType());
				query.ExecuteUpdate();
			}
		}

        /// <summary>
        /// Busca quantidade de NOtas em Processamento
        /// </summary>
        /// <remarks>Limita a quantidade de processamento de NFe em 300 para n�o gargalar o servidor</remarks>
        public bool AtingiuLimiteProcessamento(string codigoUfIbge)
        {
            bool retorno = false;

            if (codigoUfIbge == "OE")
            {
                using (ISession session = OpenSession())
                {
                    string hql =
                        @"SELECT CASE WHEN COUNT(np.Id) < cp.ContProcesso THEN 'true' ELSE 'false' END 
                                FROM NfePooling np, ConfNfePooling cp
                                WHERE cp.CodigoUfIbge  = :codigoUfIbge
                                AND np.ChaveNfe NOT LIKE '35%'
                                AND np.ChaveNfe NOT LIKE '41%'
                                AND np.ChaveNfe NOT LIKE '42%'
                                AND np.ChaveNfe NOT LIKE '43%'
                                AND np.ChaveNfe NOT LIKE '50%'
                                AND np.ChaveNfe NOT LIKE '51%'
                                AND np.ChaveNfe NOT LIKE '52%'
							    AND np.IndEmProcessamento = 'S' 
                                GROUP BY cp.ContProcesso ";
                    IQuery query = session.CreateQuery(hql);
                    query.SetParameter("codigoUfIbge", codigoUfIbge);
                    var ret = query.UniqueResult();
                    retorno = (ret == null ? true: Convert.ToBoolean(ret));
                }
            }
            else
            {
                using (ISession session = OpenSession())
                {
                    string hql =
                        @"SELECT CASE WHEN COUNT(np.Id) < cp.ContProcesso THEN 'true' ELSE 'false' END 
                                FROM NfePooling np, ConfNfePooling cp
                                WHERE substr(np.ChaveNfe,0,2) = cp.CodigoUfIbge 
                                AND substr(np.ChaveNfe,0,2) = :codigoUfIbge
							    AND np.IndEmProcessamento = 'S'
                                GROUP BY cp.ContProcesso ";
                    IQuery query = session.CreateQuery(hql);
                    query.SetString("codigoUfIbge", codigoUfIbge);

                    var ret = query.UniqueResult();
                    retorno = (ret == null ? true : Convert.ToBoolean(ret));
                }
            }

            return retorno;
        }
	}
}