﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.NfePetrobras;
    using Model.FluxosComerciais.Nfes.Repositories;

    /// <summary>
    /// Interface dos parametros da petrobras
    /// </summary>
    public class ParametrosPetrobrasRepository : NHRepository<ParametrosPetrobras, int>, IParametrosPetrobrasRepository
    {
    }
}