namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

	/// <summary>
    /// Repositorio para <see cref="ConfNfePoolingRepository"/>
	/// </summary>
    public class ConfNfePoolingRepository : NHRepository<ConfNfePooling, int>, IConfNfePoolingRepository
	{
		
	}
}