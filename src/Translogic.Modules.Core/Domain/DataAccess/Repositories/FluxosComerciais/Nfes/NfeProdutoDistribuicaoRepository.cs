namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate;

    /// <summary>
	/// Repositório da classe NFE produto Simconsultas
	/// </summary>
    public class NfeProdutoDistribuicaoRepository : NHRepository<NfeProdutoDistribuicao, int?>, INfeProdutoDistribuicaoRepository
	{
        /// <summary>
        /// Obter a lista de produtos de uma NfeSimConsultas
        /// </summary>
        /// <param name="nfeDistribuicaoConsultasId">Id da NfeSimConsultas que se deseja obter a lista de produtos</param>
        /// <returns>Lista de produtos da Nfe informada</returns>
        public IList<NfeProdutoDistribuicao> ObterPorNfeId(int nfeDistribuicaoConsultasId)
	    {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"FROM NfeProdutoDistribuicao NPS
                         WHERE NPS.NfeDistribuicao.Id = :nfeConsultasId
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetInt32("nfeConsultasId", nfeDistribuicaoConsultasId));

                var query = session.CreateQuery(hql.ToString());

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                return query.List<NfeProdutoDistribuicao>();
            }
	    }
	}
}