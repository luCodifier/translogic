﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositório de associação de nota fiscal com o status de uma NF-e
	/// </summary>
	public class AssociaNotaFiscalStatusNfeRepository : NHRepository<AssociaNotaFiscalStatusNfe, int>, IAssociaNotaFiscalStatusNfeRepository
	{
		/// <summary>
		/// Obtém a associação pelo id do status da nfe
		/// </summary>
		/// <param name="idStatusNfe">Id do status da nfe</param>
		/// <returns>Objeto AssociaNotaFiscalStatusNfe</returns>
		public AssociaNotaFiscalStatusNfe ObterPorIdStatusNfe(int idStatusNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdStatusNfe", idStatusNfe));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obtém a associação pelo id da Nota fiscal
		/// </summary>
		/// <param name="idNotaFiscal">Id da NotaFiscal</param>
		/// <returns>Objeto AssociaNotaFiscalStatusNfe</returns>
		public AssociaNotaFiscalStatusNfe ObterPorIdNotaFiscal(int idNotaFiscal)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdNotaFiscal", idNotaFiscal));
			return ObterPrimeiro(criteria);
		}
	}
}