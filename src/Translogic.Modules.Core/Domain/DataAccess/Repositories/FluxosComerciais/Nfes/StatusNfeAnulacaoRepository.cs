﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class StatusNfeAnulacaoRepository : NHRepository<StatusNfeAnulacao, int>, IStatusNfeAnulacaoRepository
	{	
		/// <summary>
		/// Obtém o status pela chave
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Objeto StatusNfe</returns>
		public StatusNfeAnulacao ObterPorChaveNfe(string chaveNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ChaveNfe", chaveNfe));
			return ObterPrimeiro(criteria);
		}
	}
}