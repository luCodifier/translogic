﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class LogStatusNfeRepository : NHRepository<LogStatusNfe, int>, ILogStatusNfeRepository
	{
		/// <summary>
		/// Obtém todos os status pela chave
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Lista de objetos LogStatusNfe</returns>
		public IList<LogStatusNfe> ObterPorChaveNfe(string chaveNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ChaveNfe", chaveNfe));
			return ObterTodos(criteria);
		}
	}
}