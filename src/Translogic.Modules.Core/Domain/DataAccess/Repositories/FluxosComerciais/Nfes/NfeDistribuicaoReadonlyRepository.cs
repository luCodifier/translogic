﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Classe de repositório do NFE Distribuição
    /// </summary>
    public class NfeDistribuicaoReadonlyRepository : NHRepository<NfeDistribuicaoReadonly, int?>, INfeDistribuicaoReadonlyRepository
    {
        /// <summary>
        /// Obtém os dados da NFe pela chave
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <returns> Objeto NfeReadonly </returns>
        public NfeDistribuicaoReadonly ObterPorChaveNfe(string chaveNfe)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("ChaveNfe", chaveNfe));
            return ObterPrimeiro(criteria);
        }
    }
}