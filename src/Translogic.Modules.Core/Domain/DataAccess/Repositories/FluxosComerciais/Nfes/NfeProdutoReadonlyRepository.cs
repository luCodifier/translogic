﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Classe de repositório do NFE Simconsultas
    /// </summary>
    public class NfeProdutoReadonlyRepository : NHRepository<NfeProdutoReadonly, int?>, INfeProdutoReadonlyRepository
    {
        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        public IList<NfeProdutoReadonly> ObterPorTipoId(int idNfe, string tipo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdNfe", idNfe) && Restrictions.Eq("Tipo", tipo));
            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas (Dettached)
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        public IList<NfeProdutoReadonly> ObterPorTipoIdSemEstado(int idNfe, string tipo)
        {
            var retorno =  ObterPorTipoId(idNfe, tipo);
            
            using (var session = OpenSession())
            {
                foreach (var item in retorno)
                {
                    session.Evict(item);
                }
            }
            return retorno;
        }

        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        public IList<NfeProdutoReadonlyDto> ObterPorTipoIdHql(int idNfe, string tipo)
        {
            using (var session = OpenSession())
            {
                var queryString = string.Empty;
                if (tipo == "SCO")
                {
                    queryString = string.Format(@"
                        select 'SCO'                    AS ""Tipo""
                             , NPS.NFE_ID_NFE           AS ""IdNfe""
                             , NPS.CPROD                AS ""CodigoProduto""
                             , NPS.XPROD                AS ""DescricaoProduto""
                             , NPS.NCM                  AS ""CodigoNcm""
                             , NPS.GENERO               AS ""Genero""
                             , NPS.CFOP                 AS ""Cfop""
                             , NPS.UCOM                 AS ""UnidadeComercial""
                             , NPS.QCOM                 AS ""QuantidadeComercial""
                             , NPS.VUNCOM               AS ""ValorUnitarioComercializacao""
                             , NPS.VPROD                AS ""ValorProduto""
                             , NPS.UTRIB                AS ""UnidadeTributavel""
                             , NPS.QTRIB                AS ""QuantidadeTributavel""
                             , NPS.VUNTRIB              AS ""ValorUnitarioTributacao""
                             , NPS.ICMS_ORIG            AS ""IcmsEmitente""
                             , NPS.ICMS_CST             AS ""IcmsCst""
                             , NPS.PIS_CST              AS ""PisCst""
                             , NPS.CONFINS_CST          AS ""CofinsCst""
                             , NPS.TIMESTAMP            AS ""DataHoraGravacao""
                             , NPS.ICMS_VBC             AS ""ValorBaseCalculoIcms""
                             , NPS.ICMS_PICMS           AS ""AliquotaIcms""
                             , NPS.ICMS_VICMS           AS ""ValorIcms""
                             , NPS.IPI_VBC              AS ""ValorBaseCalculoIpi""
                             , NPS.IPI_PIPI             AS ""PercentualIpi""
                             , NPS.IPI_VIPI             AS ""ValorIpi""
                             , NPS.II_VDESPADU          AS ""ValorDespesaAduaneira""
                             , NPS.II_VIOF              AS ""ValorIof""
                             , NPS.PIS_QBCPROD          AS ""QuantidadeVendida""
                             , NPS.PIS_VALIQPROD        AS ""AliquotaPis""
                             , NPS.PIS_VPIS             AS ""ValorPis""
                             , NPS.COFINS_QBCPROD       AS ""QuantidadeVendidaCofins""
                             , NPS.COFINS_VALIQPROD     AS ""AliquotaCofins""
                             , NPS.COFINS_VCOFINS       AS ""ValorCofins""
                             , NPS.IPI_CST              AS ""IpiCst""
                             , NPS.II_VII               AS ""ValorImpostoImportacao""
                        from NFE_PROD_SIMCONSULTAS NPS
                        WHERE NPS.NFE_ID_NFE = {0}", idNfe);
                }
                else
                {
                    queryString = string.Format(@"
/*
                        select 'EDI'                    AS ""Tipo""
                             , NPS.NFE_ID_NFE           AS ""IdNfe""
                             , NPS.CPROD                AS ""CodigoProduto""
                             , NPS.XPROD                AS ""DescricaoProduto""
                             , NPS.NCM                  AS ""CodigoNcm""
                             , NPS.GENERO               AS ""Genero""
                             , NPS.CFOP                 AS ""Cfop""
                             , NPS.UCOM                 AS ""UnidadeComercial""
                             , NPS.QCOM                 AS ""QuantidadeComercial""
                             , NPS.VUNCOM               AS ""ValorUnitarioComercializacao""
                             , NPS.VPROD                AS ""ValorProduto""
                             , NPS.UTRIB                AS ""UnidadeTributavel""
                             , NPS.QTRIB                AS ""QuantidadeTributavel""
                             , NPS.VUNTRIB              AS ""ValorUnitarioTributacao""
                             , NPS.ICMS_ORIG            AS ""IcmsEmitente""
                             , NPS.ICMS_CST             AS ""IcmsCst""
                             , NPS.PIS_CST              AS ""PisCst""
                             , NPS.CONFINS_CST          AS ""CofinsCst""
                             , NPS.TIMESTAMP            AS ""DataHoraGravacao""
                             , NPS.ICMS_VBC             AS ""ValorBaseCalculoIcms""
                             , NPS.ICMS_PICMS           AS ""AliquotaIcms""
                             , NPS.ICMS_VICMS           AS ""ValorIcms""
                             , NPS.IPI_VBC              AS ""ValorBaseCalculoIpi""
                             , NPS.IPI_PIPI             AS ""PercentualIpi""
                             , NPS.IPI_VIPI             AS ""ValorIpi""
                             , NPS.II_VDESPADU          AS ""ValorDespesaAduaneira""
                             , NPS.II_VIOF              AS ""ValorIof""
                             , NPS.PIS_QBCPROD          AS ""QuantidadeVendida""
                             , NPS.PIS_VALIQPROD        AS ""AliquotaPis""
                             , NPS.PIS_VPIS             AS ""ValorPis""
                             , NPS.COFINS_QBCPROD       AS ""QuantidadeVendidaCofins""
                             , NPS.COFINS_VALIQPROD     AS ""AliquotaCofins""
                             , NPS.COFINS_VCOFINS       AS ""ValorCofins""
                             , NPS.IPI_CST              AS ""IpiCst""
                             , NPS.II_VII               AS ""ValorImpostoImportacao""
                        from NFE_PROD_DISTRIBUICAO NPS
                        WHERE NPS.NFE_ID_NFE = {0}
                        UNION ALL
*/
                        select 'EDI'                    AS ""Tipo""
                             , ENP.NFE_ID_NFE           AS ""IdNfe""
                             , ENP.CPROD                AS ""CodigoProduto""
                             , ENP.XPROD                AS ""DescricaoProduto""
                             , ENP.NCM                  AS ""CodigoNcm""
                             , ENP.GENERO               AS ""Genero""
                             , ENP.CFOP                 AS ""Cfop""
                             , ENP.UCOM                 AS ""UnidadeComercial""
                             , ENP.QCOM                 AS ""QuantidadeComercial""
                             , ENP.VUNCOM               AS ""ValorUnitarioComercializacao""
                             , ENP.VPROD                AS ""ValorProduto""
                             , ENP.UTRIB                AS ""UnidadeTributavel""
                             , ENP.QTRIB                AS ""QuantidadeTributavel""
                             , ENP.VUNTRIB              AS ""ValorUnitarioTributacao""
                             , ENP.ICMS_ORIG            AS ""IcmsEmitente""
                             , ENP.ICMS_CST             AS ""IcmsCst""
                             , ENP.PIS_CST              AS ""PisCst""
                             , ENP.CONFINS_CST          AS ""CofinsCst""
                             , ENP.TIMESTAMP            AS ""DataHoraGravacao""
                             , ENP.ICMS_VBC             AS ""ValorBaseCalculoIcms""
                             , ENP.ICMS_PICMS           AS ""AliquotaIcms""
                             , ENP.ICMS_VICMS           AS ""ValorIcms""
                             , ENP.IPI_VBC              AS ""ValorBaseCalculoIpi""
                             , ENP.IPI_PIPI             AS ""PercentualIpi""
                             , ENP.IPI_VIPI             AS ""ValorIpi""
                             , ENP.II_VDESPADU          AS ""ValorDespesaAduaneira""
                             , ENP.II_VIOF              AS ""ValorIof""
                             , ENP.PIS_QBCPROD          AS ""QuantidadeVendida""
                             , ENP.PIS_VALIQPROD        AS ""AliquotaPis""
                             , ENP.PIS_VPIS             AS ""ValorPis""
                             , ENP.COFINS_QBCPROD       AS ""QuantidadeVendidaCofins""
                             , ENP.COFINS_VALIQPROD     AS ""AliquotaCofins""
                             , ENP.COFINS_VCOFINS       AS ""ValorCofins""
                             , ENP.IPI_CST              AS ""IpiCst""
                             , ENP.II_VII               AS ""ValorImpostoImportacao""
                        from EDI.EDI2_NFE_PROD ENP
                        WHERE ENP.NFE_ID_NFE = {0}
                        ", idNfe);
                }

                var query = session.CreateSQLQuery(queryString);
                query.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean(typeof(NfeProdutoReadonlyDto)));
                var resultados = query.List<NfeProdutoReadonlyDto>();

                return resultados;
            }
        }
    }
}