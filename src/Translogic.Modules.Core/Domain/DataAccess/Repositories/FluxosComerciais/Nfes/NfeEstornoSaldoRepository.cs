﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System;
    using System.Collections.Generic;
    using System.Data.OracleClient;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Nfes;
    using Model.FluxosComerciais.Nfes.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Classe de repositório do estorno de saldo de nfe
    /// </summary>
    public class NfeEstornoSaldoRepository : NHRepository<NfeEstornoSaldo, int>, INfeEstornoSaldoRepository
    {
    }
}