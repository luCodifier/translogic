﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate.Criterion;

    /// <summary>
	/// Classe de repositório do controle das consultas ao DF-e
	/// </summary>
    public class DFeConsultaRepository : NHRepository<DFeConsulta, int?>, IDFeConsultaRepository
	{
        /// <summary>
        /// Obtem data e registro da ultima consulta
        /// </summary>
        /// <returns>Retorna data e registro da ultima consulta, registro é utilzado para realizar a próxima consulta</returns>
        public DFeConsulta ObterUltimoRegistro(string cnpj, string uf)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cnpj", cnpj));
            criteria.Add(Restrictions.Eq("Uf", uf));
            criteria.AddOrder(Order.Desc("UltimoRegistro"));
            var retorno = ObterPrimeiro(criteria);
            return retorno;
        }
	}
}