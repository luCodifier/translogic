namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class LogNfeSaldoRepository : NHRepository<LogNfeSaldo, int>, ILogNfeSaldoRepository
	{
	}
}