﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositório do NfeConfiguracaoEmpresaUnidadeMedida
	/// </summary>
	public class NfeConfiguracaoEmpresaUnidadeMedidaRepository : NHRepository<NfeConfiguracaoEmpresaUnidadeMedida, int>, INfeConfiguracaoEmpresaUnidadeMedidaRepository
	{
		/// <summary>
		/// Obtém a configuração pelo cnpj
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa</param>
		/// <returns>Objeto NfeConfiguracaoEmpresaUnidadeMedida</returns>
		public NfeConfiguracaoEmpresaUnidadeMedida ObterPorCnpj(string cnpj)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cnpj", cnpj));
			return ObterPrimeiro(criteria);
		}
	}
}