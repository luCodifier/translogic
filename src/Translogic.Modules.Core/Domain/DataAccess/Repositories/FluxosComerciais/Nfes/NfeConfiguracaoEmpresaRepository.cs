﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using NHibernate.Criterion;
	using NHibernate.Linq;

    /// <summary>
	/// Classe de repositório do NfeConfiguracaoEmpresaUnidadeMedida
	/// </summary>
    public class NfeConfiguracaoEmpresaRepository : NHRepository<NfeConfiguracaoEmpresa, int>, INfeConfiguracaoEmpresaRepository
	{
		/// <summary>
		/// Obtém a configuração pelo cnpj
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa</param>
        /// <returns>Objeto NfeConfiguracaoEmpresa</returns>
        public NfeConfiguracaoEmpresa ObterPorCnpj(string cnpj)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cnpj", cnpj));
			return ObterPrimeiro(criteria);
		}

        /// <summary>
        /// ListarPorTrechoDe Cnpj
        /// </summary>
        /// <param name="pagination">detalhes da paginacao</param>
        /// <param name="value">valor procurado no cnpj</param>
        /// <returns>Resultado Paginado</returns>
        public ResultadoPaginado<NfeConfiguracaoEmpresa> ListarPorTrechoDeCnpj(DetalhesPaginacao pagination, string value)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.AddOrder(new Order("Cnpj", true));

            if (!string.IsNullOrEmpty(value))
            {
                criteria.Add(Restrictions.InsensitiveLike("Cnpj", value, MatchMode.Anywhere));
            }

            var result = ObterPaginado(criteria, pagination);

            return result;
        }

        /// <summary>
        /// verifica se já existe o cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da entidade</param>
        ///  <param name="id">id da entidade</param>
        /// <returns>resultado da verificacao</returns>
        public bool CnpjJaExiste(string cnpj, int id)
        {
            using (var session = OpenSession())
            {
                var query = session
                    .Query<NfeConfiguracaoEmpresa>()
                    .Where(e => e.Cnpj == cnpj && e.Id != id);

                var result = query.Count();

                return result > 0;
            }
        }
	}
}