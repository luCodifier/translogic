﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Nfes;
	using Model.FluxosComerciais.Nfes.Repositories;
	using Model.FluxosComerciais.Simconsultas;
	using NHibernate;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class StatusRetornoNfeRepository : NHRepository<StatusRetornoNfe, int>, IStatusRetornoNfeRepository
	{
		/// <summary>
		/// Obtém o status pelo código
		/// </summary>
		/// <param name="codigo">Código do status de retorno</param>
		/// <returns>Objeto StatusRetornoNfe</returns>
		public StatusRetornoNfe ObterPorCodigo(string codigo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Codigo", codigo));
			return ObterPrimeiro(criteria);
		}
	}
}