namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using NHibernate;
    using NHibernate.Criterion;

    /// <summary>
    /// Implementa��o de Reposit�rio de DclErroGravacao
    /// </summary>
    public class DclErroGravacaoRepository : NHRepository<DclErroGravacao, DateTime>, IDclErroGravacaoRepository
    {
        /// <summary>
        /// Obt�m os erros de grava��o pelo id de controle
        /// </summary>
        /// <param name="idControle"> The id controle. </param>
        /// <returns> Lista de erros </returns>
        public IList<DclErroGravacao> ObterPorIdControle(decimal idControle)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdControle", idControle));
            return ObterTodos(criteria);
        }
    }
}