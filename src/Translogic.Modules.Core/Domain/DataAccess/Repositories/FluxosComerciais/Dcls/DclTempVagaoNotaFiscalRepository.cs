namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using NHibernate;

    /// <summary>
    /// Implementação de Repositório de DclTempVagaoNotaFiscal
    /// </summary>
    public class DclTempVagaoNotaFiscalRepository : NHRepository<DclTempVagaoNotaFiscal, int>, IDclTempVagaoNotaFiscalRepository
    {
        /// <summary>
        /// Insere uma lista de DclTempVagaoNotaFiscal
        /// </summary>
        /// <param name="listaDclTempVagaoNotaFiscal"> Lista de DclTempVagaoNotaFiscal. </param>
        public void Inserir(IList<DclTempVagaoNotaFiscal> listaDclTempVagaoNotaFiscal)
        {
            foreach (DclTempVagaoNotaFiscal dclTempVagaoNotaFiscal in listaDclTempVagaoNotaFiscal)
            {
                Inserir(dclTempVagaoNotaFiscal);
            }
        }
    }
}