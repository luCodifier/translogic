namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using NHibernate;

    /// <summary>
    /// Implementa��o de Reposit�rio de DclTemp
    /// </summary>
    public class DclTempRepository : NHRepository<DclTemp, int?>, IDclTempRepository
    {
        /// <summary>
        /// Chama a package de gerar despachos
        /// </summary>
        /// <param name="idControle">Id de controle</param>
        /// <param name="codigoTela">C�digo da Tela</param>
        public void GerarDespachos(string idControle, string codigoTela)
        {
            using (ISession session = OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_DESPACHO.SP_GERAR_DESPACHOS",
                    Connection = (OracleConnection)session.Connection 
                };
                AddParameter(command, "pIDControle", OracleType.VarChar, idControle, ParameterDirection.Input);
                AddParameter(command, "pTelaOrigem", OracleType.VarChar, codigoTela, ParameterDirection.Input);

                session.Transaction.Enlist(command);

                command.Prepare();
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Obt�m o Id de controle da DclTemp
        /// </summary>
        /// <returns> Id de controle da dcltemp </returns>
        public decimal ObterIdControle()
        {
            string sql = "SELECT DCL_ERROS_GRV_SEQ_ID.NEXTVAL FROM DUAL";
            using (ISession session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sql);
                query.SetReadOnly(true);
                return query.UniqueResult<decimal>();
            }
        }

        /// <summary>
        /// Apaga os dados das tabelas tempor�rias de carregamento
        /// </summary>
        /// <param name="dclTemp">Dcl temp a ser apagada</param>
        /// <param name="dclTempVagaoConteiner">Dcl temp vagao conteiner</param>
        /// <param name="dclTempVagao">Dcl temp vagao</param>
        /// <param name="dclTempVagaoNotaFiscal">dcl temp vagao nota fiscal</param>
        /// <param name="idControleDclTemp">id controle dcl temp</param>
        public void LimparDados(DclTemp dclTemp, IList<DclTempVagaoConteiner> dclTempVagaoConteiner, IList<DclTempVagao> dclTempVagao, IList<DclTempVagaoNotaFiscal> dclTempVagaoNotaFiscal, decimal idControleDclTemp)
        {
            string hqlTpl = "DELETE FROM {0} WHERE Id IN ({1})";
            using (ISession session = OpenSession())
            {
                string hql = string.Format(hqlTpl, "DclTemp", dclTemp.Id);
                IQuery query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();

                hql = string.Format(hqlTpl, "DclTempVagao", String.Join(",", dclTempVagao.Select(c => c.Id)));
                query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();

                hql = string.Format(hqlTpl, "DclTempVagaoConteiner", String.Join(",", dclTempVagaoConteiner.Select(c => c.Id)));
                query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();

                hql = string.Format(hqlTpl, "DclTempVagaoNotaFiscal", String.Join(",", dclTempVagaoNotaFiscal.Select(c => c.Id)));
                query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();

                hql = "DELETE FROM DclErroGravacao WHERE IdControle = " + idControleDclTemp;
                query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();
            } 
        }

        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> ser� adicionado</param>
        /// <param name="nome">Nome do par�metro</param>
        /// <param name="tipo">Tipo do par�metro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Dire��o <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;

            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }
    }
}