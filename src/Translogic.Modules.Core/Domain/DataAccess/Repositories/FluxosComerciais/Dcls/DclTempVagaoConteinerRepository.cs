namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using NHibernate;

    /// <summary>
    /// Implementação de Repositório de DclTempVagaoConteiner
    /// </summary>
    public class DclTempVagaoConteinerRepository : NHRepository<DclTempVagaoConteiner, string>, IDclTempVagaoConteinerRepository
    {
        /// <summary>
        /// Insere uma lista de DclTempVagaoConteiner
        /// </summary>
        /// <param name="listDclTempVagaoConteiner"> Lista de DclTempVagaoConteiner. </param>
        public void Inserir(IList<DclTempVagaoConteiner> listDclTempVagaoConteiner)
        {
            foreach (DclTempVagaoConteiner dclTempVagaoConteiner in listDclTempVagaoConteiner)
            {
                Inserir(dclTempVagaoConteiner);
            }
        }
    }
}