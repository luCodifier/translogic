namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Dcls
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.FluxosComerciais.Dcls;
    using Model.FluxosComerciais.Dcls.Repositories;
    using NHibernate;

    /// <summary>
    /// Implementação de Repositório de DclTempVagao
    /// </summary>
    public class DclTempVagaoRepository : NHRepository<DclTempVagao, int?>, IDclTempVagaoRepository
    {
        /// <summary>
        /// Insere uma lista de DclTempVagao
        /// </summary>
        /// <param name="listaDclTempVagao"> The lista dcl temp vagao. </param>
        public void Inserir(IList<DclTempVagao> listaDclTempVagao)
        {
            foreach (DclTempVagao dclTempVagao in listaDclTempVagao)
            {
                Inserir(dclTempVagao);
            }
        }
    }
}