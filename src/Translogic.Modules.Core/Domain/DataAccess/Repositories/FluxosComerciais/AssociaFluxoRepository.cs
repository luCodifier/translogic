namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System;
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;

	/// <summary>
	/// Repositorio para <see cref="AssociaFluxo"/>
	/// </summary>
	public class AssociaFluxoRepository : NHRepository<AssociaFluxo, int?>, IAssociaFluxoRepository
	{
		/// <summary>
		/// Obt�m a associa��o do fluxo comercial com o fluxo internacional ativo
		/// </summary>
		/// <param name="fluxo"> Objeto FluxoComercial. </param>
		/// <returns> Objeto AssociaFluxo </returns>
		public AssociaFluxo ObterPorFluxoAtivo(FluxoComercial fluxo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("FluxoComercial", fluxo) && Restrictions.Eq("IndAtivo", true));
			return ObterPrimeiro(criteria);
		}
	}
}