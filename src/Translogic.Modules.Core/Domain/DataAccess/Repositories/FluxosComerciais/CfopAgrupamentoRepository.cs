﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;
	using NHibernate;

	/// <summary>
	/// Classe de repositório do CfopAgrupamento
	/// </summary>
	public class CfopAgrupamentoRepository : NHRepository<CfopAgrupamento, int>, ICfopAgrupamentoRepository
	{
		/// <summary>
		/// Obtém o CfopAgrupamento pelo exemplo de CFOP
		/// </summary>
		/// <param name="cfopProduto">CFOP do produto de exemplo</param>
		/// <returns>Objeto CfopAgrupamento</returns>
		public CfopAgrupamento ObterPorCfopExemplo(string cfopProduto)
		{
			using (ISession session = OpenSession())
			{
				string hql = @" FROM CfopAgrupamento ca
							WHERE :cfopProduto LIKE ca.PrefixoCfop || '%' 
							ORDER BY ca.PrefixoCfop DESC";
				IQuery query = session.CreateQuery(hql);
				query.SetString("cfopProduto", cfopProduto);

				// query.SetResultTransformer(Transformers.DistinctRootEntity);

				IList<CfopAgrupamento> lista = query.List<CfopAgrupamento>();
				if (lista.Count > 0)
				{
					return lista[0];
				}

				return null;
			}
		}
	}
}