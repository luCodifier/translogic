namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositorio para <see cref="DensidadeMercadoria"/>
    /// </summary>
    public class DensidadeMercadoriaRepository : NHRepository<DensidadeMercadoria, string>, IDensidadeMercadoriaRepository
    {
    }
}