namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Repositorio para <see cref="GrupoFluxoComercial"/>
    /// </summary>
    public class GrupoFluxoComercialRepository : NHRepository<GrupoFluxoComercial, int>, IGrupoFluxoComercialRepository
    {
    }
}