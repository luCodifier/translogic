namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;

    /// <summary>
	/// Repositorio para <see cref="AssociaFluxo"/>
	/// </summary>
	public class AssociaFluxoInternacionalRepository : NHRepository<AssociaFluxoInternacional, int?>, IAssociaFluxoInternacionalRepository
	{
        /// <summary>
        /// Obt�m a associa��o do fluxo comercial com o fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Objeto AssociaFluxoInternacional </returns>
        public AssociaFluxoInternacional ObterPorFluxoComercial(FluxoComercial fluxo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("FluxoComercial", fluxo));
            return ObterPrimeiro(criteria);
        }
	}
}