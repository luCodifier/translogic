namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;

    /// <summary>
	/// Repositorio para <see cref="FluxoInternacional"/>
	/// </summary>
	public class FluxoInternacionalRepository : NHRepository<FluxoInternacional, int?>, IFluxoInternacionalRepository
	{
        /// <summary>
        /// Obt�m o fluxo internacional pelo c�digo do fluxo informado
        /// </summary>
        /// <param name="codigoFluxo"> The codigo fluxo. </param>
        /// <returns> Objeto FluxoInternacional </returns>
        public FluxoInternacional ObterPorCodigo(string codigoFluxo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CodigoFluxoInternacional", codigoFluxo));
            return ObterPrimeiro(criteria);
        }
	}
}