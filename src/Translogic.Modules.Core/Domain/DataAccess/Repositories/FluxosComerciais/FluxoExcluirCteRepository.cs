﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;

	/// <summary>
	/// Repositorio para <see cref="FluxoExcluirCte"/>
	/// </summary>
	public class FluxoExcluirCteRepository : NHRepository<FluxoExcluirCte, string>, IFluxoExcluirCteRepository 
	{
	}
}