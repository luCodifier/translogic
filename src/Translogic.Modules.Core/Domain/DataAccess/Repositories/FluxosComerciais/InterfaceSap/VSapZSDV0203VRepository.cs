namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap
{
	using System;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.InterfaceSap;
	using Model.FluxosComerciais.InterfaceSap.Repositories;
	using Model.FluxosComerciais.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositorio para <see cref="VSapZSDV0203V"/>
	/// </summary>
	public class VSapZSDV0203VRepository : NHRepository<VSapZSDV0203V, int>, IVSapZSDV0203VRepository
	{
		/// <summary>
		/// Obt�m VSapZSDV0203V pela Serie, Despacho de 5 digitos e o Tipo
		/// </summary>
		/// <param name="serie"> Numero da Serie </param>
		/// <param name="despacho"> N�mero do Despacho de 5 digitos </param>
		/// <param name="tipo"> tipo da opera��o </param>
		/// <returns> Objeto VSapZSDV0203V </returns>
		public VSapZSDV0203V ObterPorSerieDespachoTipo(int serie, int despacho, string tipo)
		{
			string serieStr = serie != 0 ? Convert.ToString(serie) : string.Empty;
			string despachoStr = despacho != 0 ? Convert.ToString(despacho) : string.Empty;
			despachoStr = despachoStr.PadLeft(5, '0');

			string str = serieStr + "-" + despachoStr + "-2";

			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Like("Bstkd", str, MatchMode.Start));
			criteria.Add(Restrictions.Like("Tipo", tipo, MatchMode.Exact));

			return ObterPrimeiro(criteria);
		}
	}
}