namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.InterfaceSap;
    using Model.FluxosComerciais.InterfaceSap.Repositories;
    using Model.FluxosComerciais.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositorio para <see cref="SapDocsoutrasferCte"/>
    /// </summary>
    public class SapDocsoutrasferCteRepository : NHRepository<SapDocsoutrasferCte, int>, ISapDocsoutrasferCteRepository
    {
    }
}