
using System.Linq;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.InterfaceSap;
    using Model.FluxosComerciais.InterfaceSap.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Repositorio para <see cref="SapChaveCte"/>
    /// </summary>
    public class SapChaveCteRepository : NHRepository<SapChaveCte, int>, ISapChaveCteRepository
    {
        /// <summary>
        /// Obt�m pela chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave de Cte para ser pesquisada</param>
        /// <returns>Retorna a inst�ncia do objeto</returns>
        public SapChaveCte ObterPorChaveCte(string chaveCte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM SapChaveCte sap WHERE sap.ChaveCte = :chaveCte ORDER BY sap.Id";

                IQuery query = session.CreateQuery(hql);
                query.SetString("chaveCte", chaveCte);

                try
                {
                    // return query.UniqueResult<SapChaveCte>();

                    // alterado pra retornar apenas 1 registro caso contenha mais de 1
                    return query.List<SapChaveCte>().FirstOrDefault();
                }
                catch (Exception ex)
                {
                    Sis.LogException(ex, string.Format("Erro em SapChaveCte.ObterPorChaveCte: select * from SAP_CHAVE_CTE where chavecte = '{0}'", chaveCte));
                    throw;
                }
            }
        }

        /// <summary>
        /// Obt�m pela chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave de Cte para ser pesquisada</param>
        /// <returns>Retorna a inst�ncia do objeto</returns>
        public IList<SapChaveCte> ObterPorChavesCte(string chaveCte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM SapChaveCte sap WHERE sap.ChaveCte = :chaveCte ORDER BY sap.Id";

                IQuery query = session.CreateQuery(hql);
                query.SetString("chaveCte", chaveCte);

                return query.List<SapChaveCte>();
            }
        }
    }
}