namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;

	/// <summary>
	/// Repositorio para <see cref="SapCteAnulado"/>
	/// </summary>
	public class SapCteAnuladoRepository : NHRepository<SapCteAnulado, int>, ISapCteAnuladoRepository
	{
	}
}