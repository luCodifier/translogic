namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap
{
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais;
    using Model.FluxosComerciais.InterfaceSap;
    using Model.FluxosComerciais.InterfaceSap.Repositories;
    using Model.FluxosComerciais.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositorio para <see cref="SapDespCte"/>
    /// </summary>
    public class SapDespCteRepository : NHRepository<SapDespCte, int>, ISapDespCteRepository
    {
        /// <summary>
        ///    Insere na SapDespCte  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade da SapDespCte</param>
        /// <returns>Retorna a entidade inserida </returns>
        public SapDespCte InserirSemSessao(SapDespCte entity)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.Insert(entity) as SapDespCte;
            }
        }
    }
}