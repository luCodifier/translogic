namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using ANTT.Domain;
	using CustomType;
	using Model.Diversos;
	using Model.Dto;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;
	using NHibernate;
	using NHibernate.Linq;
	using NHibernate.Transform;

	/// <summary>
	/// Repositorio para <see cref="FluxoComercial"/>
	/// </summary>
	public class FluxoComercialRepository : NHRepository<FluxoComercial, int>, IFluxoComercialRepository
	{
		/// <summary>
		/// Obter fluxo comercial por c�digo
		/// </summary>
		/// <param name="codigofluxo">C�digo do fluxo comercial</param>
		/// <returns>Fluxo Comercial</returns>
		public FluxoComercial ObterPorCodigo(string codigofluxo)
		{
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
					@"FROM FluxoComercial fl 
						WHERE fl.Codigo LIKE :fluxo
						ORDER BY fl.Id desc");

			parametros.Add(q => q.SetString("fluxo", codigofluxo));

			ISession session = OpenSession();

			IQuery query = session.CreateQuery(sb.ToString());
			query.SetResultTransformer(Transformers.DistinctRootEntity);

			foreach (Action<IQuery> action in parametros)
			{
				action.Invoke(query);
			}

			IList<FluxoComercial> result = query.List<FluxoComercial>();
			if (result != null && result.Count > 0)
			{
				return result[0];
			}

			return null;
		}

        /// <summary>
        /// Obter fluxo comercial por c�digo
        /// </summary>
        /// <param name="codigofluxo">C�digo do fluxo comercial</param>
        /// <returns>Fluxo Comercial</returns>
        public FluxoComercial ObterPorCodigoSimplificado(string codigofluxo)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();

            StringBuilder sb = new StringBuilder();
            sb.Append(
                    @"FROM FluxoComercial fl 
                        WHERE SUBSTR(fl.Codigo, 3) = :fluxo
						ORDER BY fl.Id desc");

            parametros.Add(q => q.SetString("fluxo", codigofluxo));

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());
            query.SetResultTransformer(Transformers.DistinctRootEntity);

            foreach (Action<IQuery> action in parametros)
            {
                action.Invoke(query);
            }

            IList<FluxoComercial> result = query.List<FluxoComercial>();
            if (result != null && result.Count > 0)
            {
                return result[0];
            }

            return null;
        }

		/// <summary>
		/// Obt�m os fluxos para o Rateio
		/// </summary>
		/// <param name="empresaRemetente"> C�digo da empresa remetente. </param>
		/// <param name="terminalDestino"> C�digo da empresa do terminal de destino. </param>
		/// <param name="terminalOrigem"> C�digo da empresa do terminal de origem. </param>
		/// <param name="ncm"> C�digo NCM da mercadoria. </param>
		/// <returns> Retorna a lista de fluxos </returns>
		public IList<FluxoComercial> ObterFluxoParaRateio(int empresaRemetente, string terminalDestino, string terminalOrigem, string ncm)
		{
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
					@"FROM FluxoComercial fx
                        INNER JOIN FETCH fx.EmpresaRemetente er
                        INNER JOIN FETCH fx.EmpresaDestinataria ed
                        INNER JOIN FETCH fx.Origem origem
						WHERE er.Id = :empresaRemetente
                        AND ed.Id = :terminalDestino
                        AND origem.Id = :terminalOrigem
                        AND fx.DataVigencia > sysdate
                        AND fx.MercadoriaNcm = :ncm
                        AND ((fx.ModeloNotaFiscal = '55' AND fx.CnpjRemetenteFiscal = er.Cgc) OR fx.ModeloNotaFiscal <> '55' OR fx.ModeloNotaFiscal IS NULL)");

			parametros.Add(q => q.SetParameter("empresaRemetente", empresaRemetente));
			parametros.Add(q => q.SetParameter("terminalDestino", terminalDestino));
			parametros.Add(q => q.SetParameter("terminalOrigem", terminalOrigem));
			parametros.Add(q => q.SetParameter("ncm", ncm));

			ISession session = OpenSession();

			IQuery query = session.CreateQuery(sb.ToString());
			query.SetResultTransformer(Transformers.DistinctRootEntity);

			foreach (Action<IQuery> action in parametros)
			{
				action.Invoke(query);
			}

			return query.List<FluxoComercial>();
		}

		/// <summary>
		/// Obt�m os fluxos pelo id do Despacho Translogic
		/// </summary>
		/// <param name="despacho"> id do Despacho. </param>
		/// <param name="codfluxo"> C�digo do Fluxo Comercial. </param>
		/// <returns> Retorna Fluxo Comercial </returns>
		public FluxoComercial ObterFluxoPorDespachoFluxo(int despacho, string codfluxo)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(
					@"select FC FROM
                    FluxoComercial      FC,
                    ItemDespacho        IDS,
                    DetalheCarregamento DC,
                    Carregamento        CG
                WHERE  IDS.DetalheCarregamento.Id = DC.Id 
                 AND  DC.Carregamento.Id = CG.Id 
                 AND  CG.FluxoComercialCarregamento.id = FC.id 
                 AND  IDS.DespachoTranslogic.Id = :despacho ");

			if (codfluxo != string.Empty)
			{
				sb.Append("AND FC.Codigo = :codfluxo");
			}

			ISession session = OpenSession();
			IQuery query = session.CreateQuery(sb.ToString());

			query.SetInt32("despacho", despacho);

			if (codfluxo != string.Empty)
			{
				query.SetString("codfluxo", codfluxo);
			}

			query.SetResultTransformer(Transformers.DistinctRootEntity);

			return (FluxoComercial)query.UniqueResult();
		}

		/// <summary>
		/// Obter fluxo comercial ferroviario por c�digo
		/// </summary>
		/// <param name="codigoFluxo">C�digo do fluxo comercial</param>
		/// <returns>Fluxo Comercial</returns>
		public FluxoComercial ObterFerroviarioPorCodigo(string codigoFluxo)
		{
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
					@"FROM FluxoComercial fl 					                            
						WHERE SUBSTR(fl.Codigo, 3) = :fluxo
                        AND (fl.Tipo = :tipoFluxoFerroviario
                        OR (fl.Tipo = :tipoFluxoRodoviario AND NVL(fl.FluxoTravado, :indFluxoTravado) = :indFluxoTravado ))
                        ORDER BY fl.Id desc");

			parametros.Add(q => q.SetString("fluxo", codigoFluxo));
			parametros.Add(q => q.SetParameter("tipoFluxoFerroviario", TipoFluxoEnum.Ferroviario, new TipoFluxoEnumCustomType()));
			parametros.Add(q => q.SetParameter("tipoFluxoRodoviario", TipoFluxoEnum.Intermodal, new TipoFluxoEnumCustomType()));
			parametros.Add(q => q.SetParameter("indFluxoTravado", false, new BooleanCharType()));

			ISession session = OpenSession();

			IQuery query = session.CreateQuery(sb.ToString());
			query.SetResultTransformer(Transformers.DistinctRootEntity);

			foreach (Action<IQuery> action in parametros)
			{
				action.Invoke(query);
			}

			IList<FluxoComercial> result = query.List<FluxoComercial>();
			if (result != null && result.Count > 0)
			{
				return result[0];
			}

			return null;
		}

        /// <summary>
        /// Obter fluxos comercial pelo cliente
        /// </summary>
        /// <param name="clienteDescResumida">Descri��o resumida do cliente</param>
        /// <returns>Resumos dos fluxos</returns>
        public IEnumerable<ResumoFluxosDto> ObterFluxosResumidosPorDescResumida(string clienteDescResumida)
        {
            using (var session = OpenSession())
            {
                var query = session.Query<FluxoComercial>()
                    .Where(fc => fc.EmpresaPagadora.DescricaoResumida == clienteDescResumida)
                    .Select(fc => new ResumoFluxosDto
                    {
                        Cliente = fc.EmpresaPagadora.DescricaoResumida,
                        Origem = fc.Origem.Codigo,
                        Destino = fc.Destino.Codigo
                    });

                return query.ToList()
                    .Distinct()
                    .ToList();
            }
        }

        public IList<string> ObterSegmentos()
        {
            using (var session = OpenSession())
            {
                string sql = @" 
                  SELECT DISTINCT FC.FX_COD_SEGMENTO
                    FROM Fluxo_Comercial FC
                   WHERE FC.FX_COD_SEGMENTO IS NOT NULL
                ORDER BY FC.FX_COD_SEGMENTO ASC";

                var query = session.CreateSQLQuery(sql);

                return query.List<string>();
            }
        }


        public decimal ObterDensidadeLiquido(string codigoFluxo)
        {
            using (var session = OpenSession())
            {
                ISQLQuery sql = session.CreateSQLQuery(@"
                    SELECT DD.DS_DENSIDADE ""Densidade"" 
                      FROM 	 FLUXO_COMERCIAL FC
                      JOIN MERCADORIA M ON M.MC_ID_MRC = FC.MC_ID_MRC
                      JOIN DENSIDADE_MERCADORIA DD ON DD.MC_COD_MRC = M.MC_COD_MRC
                     WHERE FC.FX_COD_FLX LIKE '%" + codigoFluxo + "'");

                var densidade = (decimal)sql.UniqueResult();

                if (densidade == 0)
                    densidade = 1;

                return densidade;
            }
        }

        /// <summary>
        /// Obter fluxo comercial por empresa correntista
        /// </summary>
        /// <param name="cnpjCorrentista">C�digo da empresa correntista dos Fluxos Comerciais</param>
        /// <param name="cnpjOrigem">C�digo da empresa de origem dos Fluxos Comerciais (opcional ainda)</param>
        /// <param name="cnpjDestino">C�digo da empresa de destino dos Fluxos Comerciais (opcional ainda)</param>
        /// <param name="somenteVigentes">Filtro de Fluxos Comerciais Vigentes apenas</param>
        /// <returns>Fluxo Comercial</returns>
        public IList<FluxoComercial> ObterFluxoPorEmpresaCorrentista(string cnpjCorrentista, string cnpjOrigem, string cnpjDestino, bool somenteVigentes)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();

            StringBuilder sb = new StringBuilder();
            sb.Append(
                    @"FROM FluxoComercial fx
                        WHERE fx.EmpresaPagadora.Cgc = :CnpjCorrentista
                        AND fx.Contrato.FerroviaFaturamento != :ferroviaFaturamentoMRS
                        AND fx.Contrato.FerroviaFaturamento != :ferroviaFaturamentoFCA
                        {FiltroCnpjOrigem}
                        {FiltroCnpjDestino}
                        {FiltroSomenteVigentes}");

            parametros.Add(q => q.SetString("CnpjCorrentista", cnpjCorrentista));
            parametros.Add(q => q.SetString("ferroviaFaturamentoMRS", ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(FerroviaFaturamentoEnum.MRS)));
            parametros.Add(q => q.SetString("ferroviaFaturamentoFCA", ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(FerroviaFaturamentoEnum.FCA)));

            if (!string.IsNullOrEmpty(cnpjOrigem))
            {
                sb.Replace("{FiltroCnpjOrigem}", string.Format("AND fx.EmpresaRemetente.Cgc = '{0}'", cnpjOrigem));
            }
            else
            {
                sb.Replace("{FiltroCnpjOrigem}", string.Empty);
            }

            if (!string.IsNullOrEmpty(cnpjDestino))
            {
                sb.Replace("{FiltroCnpjDestino}", string.Format("AND fx.EmpresaDestinataria.Cgc = '{0}'", cnpjDestino));
            }
            else
            {
                sb.Replace("{FiltroCnpjDestino}", string.Empty);
            }

            if (somenteVigentes)
            {
                sb.Replace("{FiltroSomenteVigentes}", "AND fx.DataVigencia >= TRUNC(SYSDATE-1)");
            }
            else
            {
                sb.Replace("{FiltroSomenteVigentes}", string.Empty);
            }

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());

            query.SetResultTransformer(Transformers.DistinctRootEntity);

            foreach (Action<IQuery> action in parametros)
            {
                action.Invoke(query);
            }

            IList<FluxoComercial> result = query.List<FluxoComercial>();
            if (result != null && result.Count > 0)
            {
                return result;
            }

            return null;
        }
    }
}