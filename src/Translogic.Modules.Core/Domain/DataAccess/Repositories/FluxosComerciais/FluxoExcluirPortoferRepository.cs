namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;

	/// <summary>
	/// Repositorio para <see cref="FluxoExcluirPortofer"/>
	/// </summary>
	public class FluxoExcluirPortoferRepository : NHRepository<FluxoExcluirPortofer, string>, IFluxoExcluirPortoferRepository
	{
	}
}