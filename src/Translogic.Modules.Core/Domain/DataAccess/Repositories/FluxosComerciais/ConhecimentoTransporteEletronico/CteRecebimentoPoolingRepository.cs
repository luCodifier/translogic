namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe de pooling de recebimento
	/// </summary>
	public class CteRecebimentoPoolingRepository : NHRepository<CteRecebimentoPooling, int?>, ICteRecebimentoPoolingRepository
	{
		/// <summary>
		/// Obt�m os itens do pooling para serem processado o retorno
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que est�o no pooling</returns>
		public IList<CteRecebimentoPooling> ObterListaRecebimentoPorHost(string hostName)
		{
			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM CteRecebimentoPooling crp
                     WHERE crp.HostName = :host
										 ORDER BY crp.DataHora";
					IQuery query = session.CreateQuery(hql);
					query.SetString("host", hostName);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<CteRecebimentoPooling>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		public void RemoverPorLista(IList<CteRecebimentoPooling> lista)
		{
			string hql = @"DELETE FROM CteRecebimentoPooling WHERE Id = :idRecebimento";
			using (ISession session = OpenSession())
			{
				foreach (CteRecebimentoPooling recebimento in lista)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idRecebimento", recebimento.Id.Value);
					query.ExecuteUpdate();
				}
			}
		}

		/// <summary>
		/// Limpa a sessao
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Clear();
				session.Flush();
			}
		}
	}
}