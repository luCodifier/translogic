﻿using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    public class CteConteinerRepository : NHRepository<CteConteiner, int>, ICteConteinerRepository
    {
        /// <summary>
        /// Obtém a lista com os detalhes do Cte
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <param name="idConteiner">Cte a ser pesquisado os detalhes</param>
        /// <returns>Object CteConteiner</returns>
        public CteConteiner ObterPorCteConteiner(int idCte, int idConteiner)
        {
            return ObterPrimeiro(CriarCriteria().Add(Restrictions.Eq("Cte.Id", idCte)).Add(Restrictions.Eq("Conteiner.Id", idConteiner)));
        }

        /// <summary>
        /// Obtém a lista com os detalhes do Cte em stateless
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <param name="idConteiner">Cte a ser pesquisado os detalhes</param>
        /// <returns>Object CteConteiner</returns>
        public CteConteiner ObterPorCteConteinerSemEstado(int idCte, int idConteiner)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                var criteria = session.CreateCriteria<CteConteiner>().Add(Restrictions.Eq("Cte.Id", idCte)).Add(Restrictions.Eq("Conteiner.Id", idConteiner));
                return criteria.List<CteConteiner>().FirstOrDefault();
            }
        }

        /// <summary>
        /// Obtém a lista com os agrupamentos de conteiner para este CTE
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        public IList<CteConteiner> ObterPorCte(int idCte)
        {
            var cteConteiner = ObterPrimeiro(CriarCriteria().Add(Restrictions.Eq("Cte.Id", idCte)));
            return ObterTodos(CriarCriteria().Add(Restrictions.Eq("Agrupamento", cteConteiner == null ? 0 : cteConteiner.Agrupamento)));
        }

        /// <summary>
        /// Obtém a lista com os agrupamentos de carga para este CTE
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        public IList<CteConteiner> ObterPorCteAgrupCarga(int idCte)
        {
            var cteConteiner = ObterPrimeiro(CriarCriteria().Add(Restrictions.Eq("Cte.Id", idCte)));
            return ObterTodos(CriarCriteria().Add(Restrictions.Eq("AgrupamentoCarga", cteConteiner == null ? 0 : cteConteiner.AgrupamentoCarga)));
        }

        /// <summary>
        /// Obtém a lista com os agrupamentos
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        public IList<CteConteiner> ObterPorCteAgrupamento(int idCte)
        {
            var cteConteiner = ObterPrimeiro(CriarCriteria().Add(Restrictions.Eq("Cte.Id", idCte)));
            return ObterTodos(CriarCriteria().Add(Restrictions.Eq("Agrupamento", cteConteiner == null ? 0 : cteConteiner.Agrupamento)));
        }

        /// <summary>
        /// Obtém a lista com os detalhes do Cte
        /// </summary>
        /// <param name="chaveCte">Chave do Cte a ter o indicativo de carga cancelada atualizado na tabela CTE_CONTEINER</param>
        public void CancelarCarregPorChaveCte(string chaveCte)
        {
            using (var session = OpenSession())
            {
                var ql = @" UPDATE CTE_CONTEINER SET IND_CARR_CANC = 'S' where ID_AGRUP_CARGA IN 
                            (SELECT CC.ID_AGRUP_CARGA from CTE_CONTEINER CC inner join CTE C on (C.ID_CTE = CC.ID_CTE)
                            where C.CHAVE_CTE = '"+chaveCte+"') ";

                session.CreateSQLQuery(ql).ExecuteUpdate();
                session.Flush();
            }            
        }
    }
}