﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Repositório para <see cref="CteResponsavelProcedimento"/>
    /// </summary>
    public class CteResponsavelProcedimentoRepository : NHRepository<CteResponsavelProcedimento, int?>, ICteResponsavelProcedimentoRepository
    {
        /// <summary>
        /// Obtém os procedimentos e responsáveis
        /// </summary>
        /// <param name="cteStatusRetornoId">Id CteStatusRetorno para se obter os procedimentos</param>
        /// <returns>Lista de procedimentos com responsáveis para a Cte</returns>
        public IList<CteResponsavelProcedimento> ObterPorCteStatusRetornoId(int cteStatusRetornoId)
        {
            var hqlQuery =
                @"SELECT P FROM CteResponsavelProcedimento P WHERE P.CteStatusRetorno.Id = :cteStatusRetornoId";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);

                query.SetInt32("cteStatusRetornoId", cteStatusRetornoId);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteResponsavelProcedimento>();
            }
        }
    }
}