namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe de arquivo do FTP
	/// </summary>
	public class CteArquivoFtpRepository : NHRepository<CteArquivoFtp, string>, ICteArquivoFtpRepository
	{
		/// <summary>
		/// Limpa a sess�o quando houver um erro
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Flush();
				session.Clear();
			}
		}

		/// <summary>
		/// Atualiza o registro com o Id do cte quando existir a chave
		/// </summary>
		public void AtualizarQuandoAcharCte()
		{
			string hql = @"UPDATE CteArquivoFtp caf SET caf.Cte = (SELECT c FROM Cte c WHERE c.Chave = caf.ChaveCte) 
                          WHERE caf.Cte IS NULL";
			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);
				query.ExecuteUpdate();
			}
		}

		/// <summary>
		/// Limpa os arquivos do FTP antigos e que n�o foram encontrados no translogic
		/// </summary>
		/// <param name="diasRetroativos">Quantidade de dias retroativos</param>
		public void LimparAntigos(int diasRetroativos)
		{
			string hql = @"DELETE FROM  CteArquivoFtp WHERE Cte IS NULL AND DataCadastro < :data";
			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);
				diasRetroativos = diasRetroativos * (-1);
				query.SetDateTime("data", DateTime.Now.AddDays(diasRetroativos));
				query.ExecuteUpdate();
			}
		}

		/// <summary>
		/// Obt�m a lista de ctes que est�o no pool de recebimento
		/// </summary>
		/// <param name="hostName">Host do Processamento</param>
		/// <returns>Lista de CT-e</returns>
		public IList<CteArquivoFtp> ObterListaRecebimentoPorHost(string hostName)
		{
			using (ISession session = OpenSession())
			{
				string hql = @"FROM CteArquivoFtp caf
                              WHERE 
                              EXISTS(SELECT cep.Id FROM CteRecebimentoPooling cep WHERE caf.Cte.Id = cep.Id AND cep.HostName = :host)";
				IQuery query = session.CreateQuery(hql);
				query.SetString("host", hostName);
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<CteArquivoFtp>();
			}
		}

		/// <summary>
		/// Remove os arquivos da tabela de FTP
		/// </summary>
		/// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
		public void RemoverPorLista(IList<CteArquivoFtp> listaProcessamento)
		{
			string hql = @"DELETE FROM CteArquivoFtp WHERE Id = :idCte";
			using (ISession session = OpenSession())
			{
				foreach (CteArquivoFtp cteArquivoFtp in listaProcessamento)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetString("idCte", cteArquivoFtp.Id);
					query.ExecuteUpdate();
				}
			}
		}
	}
}