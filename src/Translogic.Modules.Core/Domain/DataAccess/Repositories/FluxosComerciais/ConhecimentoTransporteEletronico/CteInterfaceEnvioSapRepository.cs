namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    /// <summary>
    /// Reposit�rio da Classe de interface de envio para Sap
    /// </summary>
    public class CteInterfaceEnvioSapRepository : NHRepository<CteInterfaceEnvioSap, int?>, ICteInterfaceEnvioSapRepository
    {
        /// <summary>
        /// Obtem os itens da interface que nao foram processados
        /// </summary>
        /// <param name="hostName">Nome do Servidor</param>
        /// <returns>Retorna uma lista dos itens que nao foram processados</returns>
        public IList<CteInterfaceEnvioSap> ObterNaoProcessadosPorHost(string hostName)
        {
            DateTime dataMinima = DateTime.Now;
            dataMinima = dataMinima.AddDays(-5);

            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteInterfaceEnvioSap cis 
										WHERE cis.DataEnvioSap IS NULL 
										AND cis.HostName = :hostName
										AND EXISTS(SELECT ca.Id FROM CteAgrupamento ca WHERE ca.CteRaiz.Id = cis.Cte.Id)
										AND DataGravacaoRetorno >= :dataMinima
										ORDER BY cis.DataGravacaoRetorno ASC";
                IQuery query = session.CreateQuery(hql);

                query.SetString("hostName", hostName);
                query.SetDateTime("dataMinima", dataMinima);
                query.SetMaxResults(100);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteInterfaceEnvioSap>();
            }
        }

        /// <summary>
        /// Atualiza os dados de envio do SAP
        /// </summary>
        /// <param name="cte">Cte a ser atualizado para envio</param>
        public void AtualizarEnvioSap(Cte cte)
        {
            // Remove da tabela de interface
            string hql = @"UPDATE CteInterfaceEnvioSap cis SET cis.DataEnvioSap = SYSDATE WHERE cis.Cte.Id = :idCte";
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Remove os dados de envio do SAP da interface
        /// </summary>
        /// <param name="cte">Cte a ser removido para envio</param>
        public void RemoverCteInterfaceEnvioSap(Cte cte)
        {
            try
            {
                // Remove da tabela de interface
                string hql = @"DELETE FROM CteInterfaceEnvioSap cis WHERE cis.Cte.Id = :idCte";
                using (ISession session = OpenSession())
                {
                    IQuery query = session.CreateQuery(hql);
                    query.SetInt32("idCte", cte.Id.Value);
                    query.ExecuteUpdate();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obt�m os CT-e's dado um agrupamento
        /// </summary>
        /// <param name="listaAgrupamento">Lista de agrupamentos</param>
        /// <returns>Lista de CteInterfaceEnvioSap</returns>
        public IList<CteInterfaceEnvioSap> ObterFilhosPorAgrupamento(IList<CteAgrupamento> listaAgrupamento)
        {
            DateTime dataMinima = DateTime.Now;
            dataMinima = dataMinima.AddDays(-5);

            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteInterfaceEnvioSap cis 
									WHERE cis.DataEnvioSap IS NULL 
									AND DataGravacaoRetorno >= :dataMinima
									AND Cte IN (:ctes)";
                IQuery query = session.CreateQuery(hql);

                query.SetDateTime("dataMinima", dataMinima);
                Cte[] listaCtesAgrupamento = listaAgrupamento.Select(c => c.CteFilho).ToArray();
                query.SetParameterList("ctes", listaCtesAgrupamento);
                query.SetMaxResults(50);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteInterfaceEnvioSap>();
            }
        }

        /// <summary>
        /// Grava o nome do host na table de envio para o SAP
        /// </summary>
        /// <param name="hostName">Nome do host</param>
        /// <param name="cteInterfaceEnvioSap">Cte interface envio SAP</param>
        public void GravarHostNaoProcessados(string hostName, CteInterfaceEnvioSap cteInterfaceEnvioSap)
        {
            string hql = @"UPDATE CteInterfaceEnvioSap cis2 SET cis2.HostName = :hostName WHERE cis2.Id = :idCteInterfaceEnvioSap and cis2.HostName IS NULL";
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetString("hostName", hostName);
                query.SetInt32("idCteInterfaceEnvioSap", cteInterfaceEnvioSap.Id.Value);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Obt�m os itens n�o processados que est�o sem host
        /// </summary>
        /// <returns>Lista de CteInterfaceEnvioSap</returns>
        public IList<CteInterfaceEnvioSap> ObterNaoProcessadosSemHost()
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteInterfaceEnvioSap cis 
								WHERE cis.DataEnvioSap IS NULL 
								AND cis.HostName IS NULL
								AND EXISTS(SELECT ca.Id FROM CteAgrupamento ca WHERE ca.CteRaiz.Id = cis.Cte.Id)";
                IQuery query = session.CreateQuery(hql);
                query.SetMaxResults(50);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteInterfaceEnvioSap>();
            }
        }

        /// <summary>
        /// Obt�m um registro com os dados para interface de envio Sap - Cte
        /// </summary>
        /// <param name="cteId">id do Cte a ser obtido</param>
        /// <returns>Retorna um registro de interface Sap - Cte</returns>
        public CteInterfaceEnvioSap ObterPorCte(int cteId)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteInterfaceEnvioSap cis 
								WHERE cis.Cte.Id = :cteId
								AND cis.DataEnvioSap IS NULL ";

                IQuery query = session.CreateQuery(hql);
                query.SetInt32("cteId", cteId);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<CteInterfaceEnvioSap> lista = query.List<CteInterfaceEnvioSap>();
                if (lista.Count > 0)
                {
                    return lista[0];
                }

                return null;
            }
        }

        /// <summary>
        /// Obt�m os ctes que n�o foram processados o envio para o SAP
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ</param>
        /// <returns>Retorna uma lista com os ctes a serem enviados para o SAP</returns>
        public IList<CteInterfaceEnvioSap> ObterNaoProcessados(int indAmbienteSefaz)
        {
            DateTime dataMinima = DateTime.Now;
#if DEBUG2
            dataMinima = dataMinima.AddDays(-100);
#else
			dataMinima = dataMinima.AddDays(-5);
#endif

            try
            {
                using (ISession session = OpenSession())
                {
#if (DEBUG)
                    string hql =
                @" FROM 
                              CteInterfaceEnvioSap cies
						   WHERE 
                              cies.Cte.Id NOT IN (SELECT csp.Id FROM CteSapPooling csp WHERE csp.Id = cies.Cte.Id)
                              AND DataGravacaoRetorno >= :DataMinima
							  AND cies.Cte.AmbienteSefaz = :ambienteSefaz
						   ORDER BY 
                              cies.DataUltimaLeitura";

#else
 					string hql =
                        @" FROM 
                              CteInterfaceEnvioSap cies
						   WHERE 
                              cies.Cte.Id NOT IN (SELECT csp.Id FROM CteSapPooling csp WHERE csp.Id = cies.Cte.Id)
							  AND DataGravacaoRetorno >= :DataMinima
							  AND cies.Cte.AmbienteSefaz = :ambienteSefaz
						   ORDER BY 
                              cies.DataUltimaLeitura";                   
                    
#endif



                    IQuery query = session.CreateQuery(hql);
                    query.SetDateTime("DataMinima", dataMinima);
                    query.SetInt32("ambienteSefaz", indAmbienteSefaz);
                    query.SetMaxResults(50);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<CteInterfaceEnvioSap>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}