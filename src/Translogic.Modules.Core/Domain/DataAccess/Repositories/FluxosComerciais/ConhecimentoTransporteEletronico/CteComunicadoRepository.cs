namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositório da classe CteComunicado
	/// </summary>
	public class CteComunicadoRepository : NHRepository<CteComunicado, int?>, ICteComunicadoRepository
	{
		/// <summary>
		/// Obtem o Comunicado ativo para o Estado
		/// </summary>
		/// <param name="estado">Estado a ser pesquisado</param>
		/// <returns>Lista de CteComunicado</returns>
		public CteComunicado ObterPorSiglaUf(string estado)
		{
			DetachedCriteria criteria = CriarCriteria();

			criteria.Add(Restrictions.Eq("SiglaUf", estado) && Restrictions.Eq("Ativo", true));

			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obtem os Comunicado ativos
		/// </summary>
		/// <returns>Lista de CteComunicado</returns>
		public IList<CteComunicado> ObterTodosAtivos()
		{
			DetachedCriteria criteria = CriarCriteria();

			criteria.Add(Restrictions.Eq("Ativo", true));

			return ObterTodos(criteria);
		}
	}
}
