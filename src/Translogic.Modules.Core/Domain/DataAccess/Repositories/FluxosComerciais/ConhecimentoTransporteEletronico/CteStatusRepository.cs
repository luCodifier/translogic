namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using Model.Acesso;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.TriangulusConfig;
    using NHibernate.Criterion;
    using NHibernate.Linq;

	/// <summary>
	/// Classe de reposit�rio do CteStatus
	/// </summary>
	public class CteStatusRepository : NHRepository<CteStatus, int?>, ICteStatusRepository
	{
		/// <summary>
		/// Insere o status do Cte
		/// </summary>
		/// <param name="cte">Cte de referencia</param>
		/// <param name="usuario">usuario de inser��o</param>
		/// <param name="cteStatusRetorno">Status de retorno do cte</param>
		public void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno)
		{
			InserirCteStatus(cte, usuario, cteStatusRetorno, string.Empty);
		}

		/// <summary>
		/// Insere o status do Cte
		/// </summary>
		/// <param name="cte">Cte de referencia</param>
		/// <param name="usuario">usuario de inser��o</param>
		/// <param name="cteStatusRetorno">Status de retorno do cte</param>
		/// <param name="xmlRetorno">Xml de Retorno</param>
		public void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno, string xmlRetorno)
		{
			CteStatus cteStatus = new CteStatus();
			cteStatus.Cte = cte;
			cteStatus.Usuario = usuario;
			cteStatus.CteStatusRetorno = cteStatusRetorno;
			cteStatus.DataHora = DateTime.Now;
			if (! string.IsNullOrEmpty(xmlRetorno))
			{
				cteStatus.XmlRetorno = xmlRetorno;
			}
			
			Inserir(cteStatus);
		}

        /// <summary>
        /// Insere o status do Cte
        /// </summary>
        /// <param name="cte">Cte de referencia</param>
        /// <param name="usuario">usuario de inser��o</param>
        /// <param name="cteStatusRetorno">Status de retorno do cte</param>
        /// <param name="consultaCte">Objeto de consulta do Cte</param>
        /// <param name="xmlRetorno">Xml de Retorno</param>
        public void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno, Vw209ConsultaCte consultaCte, string xmlRetorno)
        {
            CteStatus cteStatus = new CteStatus();
            cteStatus.Cte = cte;
            cteStatus.Usuario = usuario;
            cteStatus.CteStatusRetorno = cteStatusRetorno;
            cteStatus.DataHora = DateTime.Now;

            if (consultaCte != null)
            {
                cteStatus.Protocolo = consultaCte.Protocolo.ToString();
                cteStatus.DhProtocolo = consultaCte.DhProtocolo;
            }

            if (!string.IsNullOrEmpty(xmlRetorno))
            {
                cteStatus.XmlRetorno = xmlRetorno;
            }

            Inserir(cteStatus);
        }

		/// <summary>
		/// Obt�m os status do Cte pelo Cte
		/// </summary>
		/// <param name="cte">Cte para obter o status</param>
		/// <returns>Retorna uma lista com os status do CTE</returns>
		public IList<CteStatus> ObterPorCte(Cte cte)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cte", cte));
			criteria.AddOrder(new Order("DataHora", true));
			return ObterTodos(criteria);
		}

        /// <summary>
        /// Obt�m o CteStatus pela situacao do cte
        /// </summary>
        /// <param name="cte">Cte para obter o status</param>
        /// <param name="situacaoCte">Situa��o do Cte</param>
        /// <returns>Retorna a verifica��o</returns>
        public CteStatus ObterCteStatusPorSituacaoCte(Cte cte, int situacaoCte)
        {
            using (var session = OpenSession())
            {
                var query = session
                    .Query<CteStatus>()
                    .Where(e => e.Cte.Id == cte.Id && e.CteStatusRetorno.Id == situacaoCte);

                var result = query.FirstOrDefault();

                return result;
            }
        }

        /// <summary>
        /// Verifica se h� Status do CTE igual a "autorizado"
        /// </summary>
        /// <param name="cte">Cte para obter o status</param>
        /// <returns>Retorna a verifica��o</returns>
        public bool TemStatusAutorizado(Cte cte)
        {
            using (var session = OpenSession())
            {
                var query = session
                    .Query<CteStatus>()
                    .Where(e => e.Cte.Id == cte.Id && e.CteStatusRetorno.Id == 100);

                var result = query.Count();

                return result > 0;
            }
        }
    }
}