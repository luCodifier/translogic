namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Repositório da classe CteTempDetalhe
	/// </summary>
	public class CteTempDetalheRepository : NHRepository<CteTempDetalhe, string>, ICteTempDetalheRepository
	{
	}
}