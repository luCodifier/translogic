namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;

    /// <summary>
    /// Classe de reposit�rio do Cte Complementado 
    /// </summary>
    public class CteComplementadoRepository : NHRepository<CteComplementado, int?>, ICteComplementadoRepository
    {
        /// <summary>
        /// Obt�m a lista de cte complementado por cte complementar
        /// </summary>
        /// <param name="cteComplementar">Cte complementar</param>
        /// <returns>Retorna uma lista com os ctes complementados</returns>
        public IList<CteComplementado> ObterCteComplementadoPorCteComplementar(Cte cteComplementar)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CteComplementar", cteComplementar));

            criteria.CreateAlias("CteComplementar", "cc", JoinType.InnerJoin);
            criteria.CreateAlias("Cte", "c", JoinType.InnerJoin);

            criteria.SetFetchMode("c", FetchMode.Lazy);
            criteria.SetFetchMode("cc", FetchMode.Lazy);

            return ObterTodos(criteria);
        }
    }
}