﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Implementacao do Repositorio para consultas ao WS de conclusao e exportacao de cte
    /// </summary>
    public class CteConclusaoExportacaoRepository : NHRepository, ICteConclusaoExportacaoRepository
    {
        /// <summary>
        /// Metodo que retorna objeto a partir da consulta feita pelo request
        /// </summary>
        /// <param name="request">Objeto com os dados de entrada para a busca</param>
        /// <returns>Retorna Objeto com os dados dos ctes </returns>
        public List<ElectronicBillofLadingExportDtoData> InformationCteConclusaoExportacao(EletronicBillofLadingExportCompletedRailRequest request)
        {
            var maior30Dias = (request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.EndDate - request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();
            sb.AppendFormat(
                @"
                    WITH cteFiltro
                    AS 
                    (
                         SELECT MIN(ID_CTE) AS MINID, MAX(ID_CTE) AS MAXID
                         FROM CTE
                         WHERE CTE.DATA_CTE BETWEEN  to_date('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') 
                                                 AND to_date('{1:dd/MM/yyyy HH:mm:ss}','dd/mm/yyyy hh24:mi:ss')
                    )                        
                    SELECT 
                        CTE.ID_CTE AS ""CteId"", 
                        CTE.CHAVE_CTE AS ""CteKey"",     
                        CASE WHEN DD.DD_QTD_PAD > 0 THEN DD.DD_TU_AUT
                             WHEN DD.DD_NUM_VOL > 0 THEN DD.DD_NUM_VOL
                             ELSE DC.DC_QTD_INF     END AS ""UnloadedWeight"",    
                        VP.VP_DAT_CHG AS ""DateTimeArrival"",
                        AO.AO_COD_AOP AS ""BondedEnclosure"",
                        C.CG_ID_CAR AS ""Carregamento"",
                        NF.NF_IDT_NFL AS ""NotaId"",
                        NF.NFE_CHAVE_NFE AS ""NotaChave""
                    FROM 
                       cteFiltro          CF,
                        CTE                CTE,
                        DESPACHO           DESP,
                        ITEM_DESPACHO      IDESP,
                        DETALHE_CARREGAMENTO DC,
                        DETALHE_DESCARREGAMENTO DD,
                        CARREGAMENTO         C,
                        FLUXO_COMERCIAL      FC,
                        VAGAO_PATIO          VP,
                        AREA_OPERACIONAL     AO,
                        NOTA_FISCAL          NF
                    WHERE CTE.ID_CTE BETWEEN CF.MINID AND MAXID
                        AND CTE.SITUACAO_ATUAL = 'AUT'                       
                        AND CTE.DP_ID_DP = DESP.DP_ID_DP
                        AND IDESP.DP_ID_DP = DESP.DP_ID_DP AND CTE.VG_ID_VG = IDESP.VG_ID_VG    
                        AND DC.DC_ID_CRG = IDESP.DC_ID_CRG
                        AND DD.DC_ID_CRG = DC.DC_ID_CRG
                        AND C.CG_ID_CAR = DC.CG_ID_CAR
                        AND FC.FX_ID_FLX = C.FX_ID_FLX_CRG
                        AND C.PT_ID_ORT = VP.PT_ID_ORT
                        AND CTE.VG_ID_VG = VP.VG_ID_VG
                        AND VP.AO_ID_AO = AO.AO_ID_AO                       
                        AND (FC.AO_ID_EST_DS = AO.AO_ID_AO_INF OR FC.AO_ID_EST_DS = AO.AO_ID_AO)
                        AND NF.DP_ID_DP = CTE.DP_ID_DP AND NF.VG_ID_VG = CTE.VG_ID_VG",
                        request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate,
                        maior30Dias ? request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.StartDate.AddDays(31) : request.ElectronicBillofLadingExportCompletedRail.ElectronicBillofLadingExportCompletedRailType.ElectronicBillofLadingExportCompletedRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingExportDtoData>());

            var result = query.List<ElectronicBillofLadingExportDtoData>();

            return result.ToList();
        }
    }
}