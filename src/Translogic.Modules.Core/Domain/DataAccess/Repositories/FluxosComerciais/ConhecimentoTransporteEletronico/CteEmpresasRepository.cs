namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Classe de Cte Empresa
	/// </summary>
	public class CteEmpresasRepository : NHRepository<CteEmpresas, int?>, ICteEmpresasRepository
	{
        public CteEmpresas InserirSemSessao(CteEmpresas entity)
	    {
	        using (var session = SessionManager.OpenStatelessSession())
	        {
	            return session.Insert(entity) as CteEmpresas;
	        }
	    }

	    /// <summary>
		/// Obt�m lista de empresas utilizadas para o cte.
		/// </summary>
		/// <param name="cte">Cte a ser pesquisado</param>
		/// <returns>Lista dos elementos da arvore</returns>
		public CteEmpresas ObterPorCte(Cte cte)
		{
			DetachedCriteria criteria = CriarCriteria();                                                                                                                                                                                                                                                                                                                                                                                                                                                   
			criteria.Add(Restrictions.Eq("Cte", cte));
			return ObterPrimeiro(criteria);
		}

        /// <summary>
        /// Obt�m lista de empresas utilizadas para o cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista dos elementos da arvore</returns>
        public IList<CteEmpresas> ObterPorListaCte(decimal[] cte)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Cte.Id", cte));
            return ObterTodos(criteria);
        }

		/// <summary>
		/// apaga a relacao de empresas utilizadas para o cte.
		/// </summary>
		/// <param name="cte">Cte a ser pesquisado</param>
		public void RemoverPorCte(Cte cte)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cte", cte));
			this.Remover(criteria);
		}

        /// <summary>
        /// apaga a relacao de empresas pelo Id
        /// </summary>
        /// <param name="id">Id a ser pesquisado</param>
        public void RemoverPorId(int? id)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Id", id));
            this.Remover(criteria);
        }
    }
}