namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;
	using NHibernate.Transform;

	/// <summary>
	/// Classe de reposit�rio do CteAgrupamento
	/// </summary>
	public class CteAgrupamentoRepository : NHRepository<CteAgrupamento, int?>, ICteAgrupamentoRepository
	{
		/// <summary>
		/// Obt�m CTE Raiz pelo Filho
		/// </summary>
		/// <param name="cteFilho">Cte filho do agrupamento</param>
		/// <returns>Cte Agrupamento Raiz</returns>
		public CteAgrupamento ObterAgrupamentoRaizPorCteFilho(Cte cteFilho)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("CteFilho", cteFilho));
			criteria.AddOrder(new Order("Sequencia", true));
			criteria.CreateAlias("CteRaiz", "cr", JoinType.InnerJoin);
			criteria.SetFetchMode("cr", FetchMode.Eager);
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m CTE Raiz pelo Filho
		/// </summary>
		/// <param name="cteFilho">Cte filho do agrupamento</param>
		/// <returns>Cte raiz no Agrupamento</returns>
		public Cte ObterCteRaizPorCteFilho(Cte cteFilho)
		{
			CteAgrupamento cteAgrupamento = ObterAgrupamentoRaizPorCteFilho(cteFilho);

			Cte cteRaiz = null;

			if (cteAgrupamento != null)
			{
				cteRaiz = cteAgrupamento.CteRaiz;
			}

			return cteRaiz;
		}

		/// <summary>
		/// Obt�m lista do agrupamento que o CTE filho faz parte
		/// </summary>
		/// <param name="cteFilho">Cte filho do agrupamento</param>
		/// <returns>Retorna a lista de agrupamento que o filho faz parte</returns>
		public IList<CteAgrupamento> ObterAgrupamentoPorCteFilho(Cte cteFilho)
		{
			IList<CteAgrupamento> result;
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
				@"select car
									FROM CteAgrupamento car,
											 CteAgrupamento caf
									WHERE car.CteRaiz.Id = caf.CteRaiz.Id
									AND   caf.CteFilho = :IdCteFilho
									ORDER BY car.Sequencia");

			parametros.Add(q => q.SetInt32("IdCteFilho", (int)cteFilho.Id));

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(sb.ToString());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				foreach (Action<IQuery> action in parametros)
				{
					action.Invoke(query);
				}

				result = query.List<CteAgrupamento>();
			}

			return result;
		}
	}
}