namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Repositorio da classe de motivo de cancelamento do CTE
	/// </summary>
	public class CteMotivoCancelamentoRepository : NHRepository<CteMotivoCancelamento, int>, ICteMotivoCancelamentoRepository
	{
    	/// <summary>
    	/// Obt�m os motivos ativos
    	/// </summary>
    	/// <returns>Lista de motivos de cancelamentos ativos</returns>
    	public IList<CteMotivoCancelamento> ObterAtivos()
    	{
    		DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Ativo", true));
    		criteria.AddOrder(new Order("Descricao", true));
    		return ObterTodos(criteria);
    	}
	}
}