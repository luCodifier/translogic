﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// classe Cte envio seguradora histórico 
    /// </summary>
    public class CteEnvioSeguradoraHistRepository : NHRepository<CteEnvioSeguradoraHist, int>, ICteEnvioSeguradoraHistRepository
    {
    }
}