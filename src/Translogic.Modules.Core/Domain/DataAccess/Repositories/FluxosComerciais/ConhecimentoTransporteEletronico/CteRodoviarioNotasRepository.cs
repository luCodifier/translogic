﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Services.Ctes.Interface;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs;
    
    /// <summary>
    /// Repositorio para consultas das notas dos Ctes rodoviarios na base do translogic
    /// </summary>
    public class CteRodoviarioNotasRepository : NHRepository, ICteRodoviarioNotasRepository
    {
        /// <summary>
        /// Retorna Notas dos Ctes Rodoviarios cadastrados na base do translogic
        /// </summary>
        /// <param name="cte">Id do Cte Rodoviario</param>
        /// <returns>Retorna Lista de Notas vinculadas ao Cte</returns>
        public List<WaybillInformation> ObterNotasRodoPorCteId(int cte)
        {
            var sb = new StringBuilder();
            sb.AppendFormat(@"  SELECT 
                                        N.CRN_CHAVE_NFE_RODO AS ""WaybillInvoiceKey"",
                                        N.CRN_PESO_DECLARADO AS ""DeclaredWeight"",
                                        N.CRN_PESO_AFERIDO   AS ""MeasuredWeight"",
                                        N.CRN_DIFERENCA_PESO AS ""WeightDifference""
                                FROM  
                                        CTE_RODOVIARIO_NOTAS N
                                WHERE 
                                        N.CR_ID_CR = :cteId");   

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());
            query.SetParameter("cteId", cte);

            query.SetResultTransformer(Transformers.AliasToBean<WaybillInformation>());
            var result = query.List<WaybillInformation>();
            return result.ToList();
        } 
    }
}