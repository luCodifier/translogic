namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da interface de importa��o dos aquivos pdf
	/// </summary>
	public class CteInterfaceEnvioEmailRepository : NHRepository<CteInterfaceEnvioEmail, int?>, ICteInterfaceEnvioEmailRepository
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		public IList<CteInterfaceEnvioEmail> ObterListaInterfacePorHost(string hostName)
		{
			DateTime dataMinima = DateTime.Now;
			dataMinima = dataMinima.AddDays(-5);

			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM CteInterfaceEnvioEmail ciee  
											WHERE ciee.HostName = :hostName 
											AND ciee.DataHora >= :DataMinima
											ORDER BY ciee.DataUltimaLeitura";

					IQuery query = session.CreateQuery(hql);
					query.SetString("hostName", hostName);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetMaxResults(50);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<CteInterfaceEnvioEmail>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Obt�m o item da interface pelo Cte
		/// </summary>
		/// <param name="idCte">Id do Cte a ser retornado</param>
		/// <returns>Retorna o item da interface pelo Cte</returns>
		public CteInterfaceEnvioEmail ObterInterfacePorCte(int idCte)
		{
			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM CteInterfaceEnvioEmail ciee  
											WHERE ciee.Cte.Id = :idCte 
											ORDER BY ciee.DataUltimaLeitura";

					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idCte", idCte);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					IList<CteInterfaceEnvioEmail> lista = query.List<CteInterfaceEnvioEmail>();

					if (lista.Count > 0)
					{
						return lista[0];
					}

					return null;
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}