namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate.Criterion;

    /// <summary>
	/// Repositorio da classe de vers�o do CTE
	/// </summary>
	public class CteVersaoRepository : NHRepository<CteVersao, int>, ICteVersaoRepository
	{
	    /// <summary>
	    /// Vers�o vigente do CTe
	    /// </summary>
	    /// <returns>Objeto CteVersao </returns>
	    public CteVersao ObterVigente()
	    {
	        DetachedCriteria criteria = CriarCriteria();
	        criteria.Add(Restrictions.Eq("Ativo", true));
	        return ObterPrimeiro(criteria);
	    }
	}
}