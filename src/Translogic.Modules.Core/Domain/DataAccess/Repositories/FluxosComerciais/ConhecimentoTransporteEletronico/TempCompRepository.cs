namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositorio de temp comp
	/// </summary>
	public class TempCompRepository : NHRepository<TempComp, int>, ITempCompRepository
	{
		/// <summary>
		/// Obter por primaria
		/// </summary>
		/// <param name="idPrimaria">Identificador do primaria</param>
		/// <returns>Retorna uma de elementos do primaria</returns>
		public IList<TempComp> ObterPorPrimaria(int idPrimaria)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IdPrincipal", idPrimaria));
			return ObterTodos(criteria);
		}
	}
}