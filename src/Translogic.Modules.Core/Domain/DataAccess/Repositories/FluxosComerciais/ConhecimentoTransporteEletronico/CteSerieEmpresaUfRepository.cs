namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Reposit�rio da s�rie empresa UF
	/// </summary>
	public class CteSerieEmpresaUfRepository : NHRepository<CteSerieEmpresaUf, int>, ICteSerieEmpresaUfRepository
	{
	}
}