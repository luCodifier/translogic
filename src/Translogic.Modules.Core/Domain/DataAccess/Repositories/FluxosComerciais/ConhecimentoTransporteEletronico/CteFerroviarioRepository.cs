﻿using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario;
using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    /// <summary>
    /// Serviço de informações do cte.
    /// </summary>
    public class CteFerroviarioRepository : NHRepository, ICteFerroviarioRepository
    {
        /// <summary>
        /// Retorna informações dos ctes
        /// </summary>
        /// <param name="request">Dados do request periodo de filtro</param>
        /// <returns> Objeto Retorna dados do cte referente ao periodo</returns>
        public InformationElectronicNotificationResponse InformationElectronicCtes(InformationElectronicNotificationRequest request)
        {
            var maior30Dias = (request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate - request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();
            sb.AppendFormat(
                @"
                    WITH cteFiltro
                    AS 
                    (
                         SELECT MIN(ID_CTE) AS MINID, MAX(ID_CTE) AS MAXID
                         FROM CTE
                         WHERE CTE.DATA_CTE BETWEEN  to_date('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') 
                                                 AND to_date('{1:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss')
                    )            

                     SELECT MIN(MT.MT_DTS_RAL) as ""DateTimeOutputTrainComposition"",
                           CASE
                             WHEN C.SITUACAO_ATUAL = 'AUT' THEN
                              'AUTORIZADO'
                             WHEN C.SITUACAO_ATUAL = 'CAN' THEN
                              'CANCELADO'
                           END as ""EletronicManifestStatus"",
                           C.CHAVE_CTE as ""EletronicManifestKey"",
                           C.ID_CTE as ""EletronicManifestId"",
                           MDFE.CHAVE_MDFE  as ""MdfeKey""
                      FROM 
                           cteFiltro         CF,
                           COMPVAGAO         CV,
                           COMPOSICAO        CP,
                           VAGAO_PEDIDO      VP,
                           VAGAO             VG,
                           ITEM_DESPACHO     ITD,
                           DESPACHO          DP,
                           CTE               C,
                           MOVIMENTACAO_TREM MT,
                           MDFE_COMPOSICAO   MDFEC,
                           MDFE              MDFE
                     WHERE 
                       C.ID_CTE BETWEEN CF.MINID AND MAXID
                       AND CV.CP_ID_CPS = CP.CP_ID_CPS
                       AND CV.PT_ID_ORT = VP.PT_ID_ORT
                       AND CV.VG_ID_VG = VP.VG_ID_VG
                       AND CV.VG_ID_VG = VG.VG_ID_VG
                       AND ITD.VG_ID_VG = VP.VG_ID_VG
                       AND ITD.DP_ID_DP = DP.DP_ID_DP
                       AND DP.PT_ID_ORT = VP.PT_ID_ORT
                       AND C.DP_ID_DP = DP.DP_ID_DP
                       AND CP.TR_ID_TRM = MT.TR_ID_TRM  
                       AND MDFEC.ID_CTE = C.ID_CTE
                       AND MDFE.ID_MDFE = MDFEC.ID_MDFE                     
                       AND C.SITUACAO_ATUAL IN ('AUT', 'CAN')
                       AND C.SIGLA_UF_FERROVIA = 'MT' -- IN ('MT', 'MS', 'SP') -- somente malha norte
                       AND C.DATA_CTE >= TO_DATE('01/01/2019 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
                       GROUP BY C.SITUACAO_ATUAL, C.CHAVE_CTE, C.ID_CTE, MDFE.CHAVE_MDFE",
                        request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate,
                        maior30Dias ? request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate.AddDays(31) : request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());
           
            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingType>());
            
            var dados = query.List<ElectronicBillofLadingType>().ToArray();

            var response = new InformationElectronicNotificationResponse
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    VersionIdentifier = "1.0",
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        ElectronicBillofLadingData = dados
                    }
                }
            };

            return response;
        }

        public InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMdfes(InformationElectronicNotificationRequest request)
        {
            var maior30Dias = (request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate - request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();

            sb.AppendFormat(
                @"
                    WITH cteFiltro
                    AS 
                    (
                         SELECT MIN(ID_CTE) AS MINID, MAX(ID_CTE) AS MAXID
                         FROM CTE
                         WHERE CTE.DATA_CTE BETWEEN  to_date('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') 
                                                 AND to_date('{1:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss')
                    )            

                      SELECT MIN(MT.MT_DTS_RAL) as ""DateTimeOutputTrainComposition"",
                           CASE
                             WHEN C.SITUACAO_ATUAL = 'AUT' THEN
                              'AUTORIZADO'
                             WHEN C.SITUACAO_ATUAL = 'CAN' THEN
                              'CANCELADO'
                           END as ""EletronicManifestStatus"",
                           C.CHAVE_CTE as ""EletronicManifestKey"",
                           C.ID_CTE as ""EletronicManifestId"",
                           MDFE.CHAVE_MDFE  as ""MdfeKey"",
                           (SELECT MAX(ET.EM_DAT_OCR)
                              FROM EVENTO E, EVENTO_TREM ET
                             WHERE E.EN_ID_EVT = ET.EN_ID_EVT
                               AND ET.TR_ID_TRM = MT.TR_ID_TRM
                               AND ET.EN_ID_EVT = 15) ""DataEncerramentoOrdemServico""
                      FROM 
                           cteFiltro         CF,
                           COMPVAGAO         CV,
                           COMPOSICAO        CP,
                           VAGAO_PEDIDO      VP,
                           VAGAO             VG,
                           ITEM_DESPACHO     ITD,
                           DESPACHO          DP,
                           CTE               C,
                           MOVIMENTACAO_TREM MT,
                           MDFE_COMPOSICAO   MDFEC,
                           MDFE              MDFE,
                           FLUXO_COMERCIAL   FC,
                           MERCADORIA        M
                     WHERE 
                       C.ID_CTE BETWEEN CF.MINID AND MAXID
                       AND CV.CP_ID_CPS = CP.CP_ID_CPS
                       AND CV.PT_ID_ORT = VP.PT_ID_ORT
                       AND CV.VG_ID_VG = VP.VG_ID_VG
                       AND CV.VG_ID_VG = VG.VG_ID_VG
                       AND ITD.VG_ID_VG = VP.VG_ID_VG
                       AND ITD.DP_ID_DP = DP.DP_ID_DP
                       AND DP.PT_ID_ORT = VP.PT_ID_ORT
                       AND C.DP_ID_DP = DP.DP_ID_DP
                       AND CP.TR_ID_TRM = MT.TR_ID_TRM  
                       AND MDFEC.ID_CTE = C.ID_CTE
                       AND MDFE.ID_MDFE = MDFEC.ID_MDFE
                       AND FC.FX_ID_FLX = C.FX_ID_FLX
                       AND M.MC_ID_MRC = FC.MC_ID_MRC
                       AND M.MC_NIVEL2 LIKE 'GR_OS'  -- Somente Produtos que estao como Graos
                   
                       AND C.SITUACAO_ATUAL IN ('AUT', 'CAN')
                       AND C.SIGLA_UF_FERROVIA = 'MT' -- IN ('MT', 'MS', 'SP') -- somente malha norte
                       AND C.DATA_CTE >= TO_DATE('01/01/2019 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
                       GROUP BY C.SITUACAO_ATUAL, C.CHAVE_CTE, C.ID_CTE, MDFE.CHAVE_MDFE, MT.TR_ID_TRM",

                        request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate,
                        maior30Dias ? request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate.AddDays(31) : request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingType>());

            var dados = query.List<ElectronicBillofLadingType>().ToArray();

            var response = new InformationElectronicNotificationResponse
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    VersionIdentifier = "1.0",
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        ElectronicBillofLadingData = dados
                    }
                }
            };
            return response;
        }

        public InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMdfesNew(InformationElectronicNotificationRequest request)
        {
            var maior30Dias = (request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate - request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();

            sb.AppendFormat(
                @"  SELECT VWS.DT_OUTPUT_TRAIN_COMPOSITION AS ""DateTimeOutputTrainComposition"",
                           VWS.ELETRONIC_MANIFEST_STATUS   AS ""EletronicManifestStatus"",
                           VWS.ELETRONIC_MANIFEST_KEY      AS ""EletronicManifestKey"",
                           VWS.ELETRONIC_MANIFEST_ID       AS ""EletronicManifestId"",
                           VWS.MDFE_KEY                    AS ""MdfeKey"",
                           VWS.DT_ENCERRAMENTO_OS          AS ""DataEncerramentoOrdemServico""
                      FROM VW_SEFAZ_CTE_MDFE_NORTE VWS
                     WHERE VWS.DATA_CTE BETWEEN
                           TO_DATE('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') AND
                           TO_DATE('{1:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') ",
                        request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate,
                        maior30Dias ? request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate.AddDays(31) : request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());


            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingType>());

            var dados = query.List<ElectronicBillofLadingType>().ToArray();

            var response = new InformationElectronicNotificationResponse
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    VersionIdentifier = "1.0",
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        ElectronicBillofLadingData = dados
                    }
                }
            };
            return response;
        }



        public InformationElectronicNotificationResponse InformationElectronicCtesMalhaSulComMdfes(InformationElectronicNotificationRequest request)
        {
            var maior30Dias = (request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate - request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();

            sb.AppendFormat(
                @"  SELECT VWS.DT_OUTPUT_TRAIN_COMPOSITION AS ""DateTimeOutputTrainComposition"",
                           VWS.ELETRONIC_MANIFEST_STATUS   AS ""EletronicManifestStatus"",
                           VWS.ELETRONIC_MANIFEST_KEY      AS ""EletronicManifestKey"",
                           VWS.ELETRONIC_MANIFEST_ID       AS ""EletronicManifestId"",
                           VWS.MDFE_KEY                    AS ""MdfeKey"",
                           VWS.DT_ENCERRAMENTO_OS          AS ""DataEncerramentoOrdemServico""
                      FROM VW_SEFAZ_CTE_MDFE_SUL VWS
                     WHERE VWS.DATA_CTE BETWEEN
                           TO_DATE('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') AND
                           TO_DATE('{1:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') ",
                        request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate,
                        maior30Dias ? request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.StartDate.AddDays(31) : request.InformationElectronicBillofLadingRailType.ElectronicBillofLadingRailDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingType>());

            var dados = query.List<ElectronicBillofLadingType>().ToArray();

            var response = new InformationElectronicNotificationResponse
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    VersionIdentifier = "1.0",
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        ElectronicBillofLadingData = dados
                    }
                }
            };
            return response;
        }
    }
}