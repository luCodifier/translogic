namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.AcessoDados;
    using Model.Estrutura;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;

    /// <summary>
	/// Repositorio da classe de vers�o do CTE
	/// </summary>
	public class CteSerieNumeroEmpresaUfRepository : NHRepository<CteSerieNumeroEmpresaUf, int>, ICteSerieNumeroEmpresaUfRepository
	{
        /// <summary>
        /// Obt�m por empresa e estado
        /// </summary>
        /// <param name="empresa">Objeto da empresa</param>
        /// <param name="uf">Estado da empresa</param>
        /// <returns>Objeto <see cref="CteSerieNumeroEmpresaUf"/></returns>
        public CteSerieNumeroEmpresaUf ObterPorEmpresaUf(IEmpresa empresa, string uf)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("Versao", "v", JoinType.InnerJoin);

            criteria.Add(Restrictions.Eq("Empresa", empresa) && Restrictions.Eq("Uf", uf));

            criteria.SetFetchMode("v", FetchMode.Eager);

            return ObterPrimeiro(criteria);
        }

		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="CteSerieNumeroEmpresaUf"/></returns>
		public CteSerieNumeroEmpresaUf ObterPorUf(string uf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Uf", uf));
			return ObterPrimeiro(criteria);
		}
	}
}