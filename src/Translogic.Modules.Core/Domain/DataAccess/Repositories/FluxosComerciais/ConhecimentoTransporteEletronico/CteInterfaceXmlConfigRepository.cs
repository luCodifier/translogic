namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da interface de importa��o dos aquivos xml
	/// </summary>
	public class CteInterfaceXmlConfigRepository : NHRepository<CteInterfaceXmlConfig, int?>, ICteInterfaceXmlConfigRepository
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		public IList<CteInterfaceXmlConfig> ObterListaInterfacePorHost(string hostName)
		{
			DateTime dataMinima = DateTime.Now;
			dataMinima = dataMinima.AddDays(-5);

#if DEBUG
			if (Environment.MachineName == "PULSAR")
			{
				dataMinima = dataMinima.AddDays(-15);
			}

#endif

			try
			{
				using (ISession session = OpenSession())
				{
					string hql = @"FROM CteInterfaceXmlConfig cic  
											WHERE cic.HostName = :hostName 
											AND cic.DataHora >= :DataMinima
											ORDER BY cic.DataUltimaLeitura
";
					IQuery query = session.CreateQuery(hql);
					query.SetString("hostName", hostName);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetMaxResults(50);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<CteInterfaceXmlConfig>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}