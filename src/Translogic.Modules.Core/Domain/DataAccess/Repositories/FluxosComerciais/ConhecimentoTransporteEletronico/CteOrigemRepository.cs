﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    public class CteOrigemRepository : NHRepository<CteOrigem, int?>, ICteOrigemRepository
    {
        /// <summary>
        /// Obtém as informações dos CTES de origem a partir do CTE informado.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista de CTES de origem</returns>
        public IList<CteOrigem> ObterPorCte(Cte cte)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cte.Id", cte.Id));
            return ObterTodos(criteria);
        }
    }
}