namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ALL.Core.AcessoDados;

    using NHibernate;
    using NHibernate.Criterion;

    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    ///     Classe de Cte Cancelado Refaturamento Config
    /// </summary>
    public class CteCanceladoRefaturamentoConfigRepository : NHRepository<CteCanceladoRefaturamentoConfig, int?>,
                                                       ICteCanceladoRefaturamentoConfigRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obtem os ctes cancelados para refaturamento da base consolidada config
        /// </summary>
        /// <param name="data">Data do Cte</param>
        /// <returns>Lista de Ctes Cancelados</returns>
        public IList<CteCanceladoRefaturamentoConfig> ObterCtesCanceladosRefaturamentoBaseConsolidadaConfigPorData(DateTime data)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Data", data));
            criteria.SetFetchMode("ListaNotasConfig", FetchMode.Join);

            return ObterTodos(criteria);
        }

        /// <summary>
        ///     Obtem o �ltimo cte cancelado para refaturamento da base consolidada config
        /// </summary>
        /// <returns>�ltimo Cte Cancelado</returns>
        public CteCanceladoRefaturamentoConfig ObterUltimoCteCanceladoRefaturamentoBaseConsolidadaConfig()
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.AddOrder(Order.Desc("Data"));
            criteria.SetFirstResult(1);
            criteria.SetMaxResults(1);

            return ObterTodos(criteria).FirstOrDefault();
        }

        #endregion
    }
}