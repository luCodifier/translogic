namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.AcessoDados;

    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    ///     Classe de Cte Cancelado Notas
    /// </summary>
    public class CteCanceladoNotasRepository : NHRepository<CteCanceladoNotas, int?>,
                                                       ICteCanceladoNotasRepository
    {
    }
}