﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using ALL.Core.AcessoDados;

    using NHibernate;
    using NHibernate.Transform;

    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviarioCancelado.Outputs;

    /// <summary>
    ///     Repositorio para retorno das ctes canceladas
    /// </summary>
    public class CteFerroviarioCanceladoRepository : NHRepository, ICteFerroviarioCanceladoRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     ObterCtesCanceladosSemDespachoCancelado retorna ctes cancelados e os novos correspondentes
        /// </summary>
        /// <param name="request">Objeto EletronicBillofLadingCancelledRailRequest com as datas para busca</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Objeto EletronicBillofLadingCancelledRailResponse</returns>
        public IList<EletronicBillofLadingCancelledData> ObterCtesCanceladosSemDespachoCancelado(
            EletronicBillofLadingCancelledRailRequest request,
            out string message)
        {
            string msg;
            var response = new EletronicBillofLadingCancelledRailResponse();
            var detalhe = new EletronicBillofLadingCancelledRailType();

            if (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                > request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate)
            {
                message = "ERRO-003: Data Inicial deve ser menor que a data Final";
            }
            else
            {
                bool maior30Dias =
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30;
                var sb = new StringBuilder();
                sb.AppendFormat(
                    @"            
                    WITH FILTRO
                    AS 
                    (
                         SELECT ID_CTE
                         FROM CTE
                         WHERE CTE.DATA_CTE BETWEEN  to_date('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') 
                                                 AND to_date('{1:dd/MM/yyyy HH:mm:ss}','dd/mm/yyyy hh24:mi:ss')
                               and exists (SELECT
                                             1
                                              FROM CTE_STATUS CS
                                              WHERE CS.ID_CTE = CTE.ID_CTE
                                              AND CS.CODIGO_STATUS_RETORNO = 220)
                    )      
                    
                    SELECT 
                            CTE_OLD.ID_CTE as ""CteCancelledId"", 
                            CTE_NEW.ID_CTE as ""CteLadingId"", 
                            CTE_OLD.CHAVE_CTE as ""CteCancelledKey"",
                            CTE_NEW.CHAVE_CTE as ""CteLadingKey"",
                            NF_OLD.NF_IDT_NFL as ""WaybillInvoiceCancelledId"",
                            NF_OLD. NFE_CHAVE_NFE as ""WaybillInvoiceCancelledKey"",
                            NF_NEW.NF_IDT_NFL as ""WaybillInvoiceId"",
                            NF_NEW.NFE_CHAVE_NFE as ""WaybillInvoiceKey""
                    FROM 
                          FILTRO F,
                          CTE_ARVORE ARVORE_OLD,
                          DESPACHO DESP_OLD,
                          DESPACHO DESP_NEW,
                          CTE CTE_OLD,
                          CTE CTE_NEW,
                          NOTA_FISCAL NF_OLD,
                          NOTA_FISCAL NF_NEW
                    WHERE 
                          ARVORE_OLD.ID_CTE = F.ID_CTE
                          AND ARVORE_OLD.ID_CTE = CTE_OLD.ID_CTE
                          AND CTE_NEW.ID_CTE = (SELECT MAX(A.ID_CTE) FROM CTE_ARVORE A WHERE A.ID_CTE_RAIZ = CTE_OLD.ID_CTE)
                          AND CTE_NEW.SITUACAO_ATUAL = 'AUT'
                          AND DESP_OLD.DP_ID_DP = CTE_OLD.DP_ID_DP
                          AND DESP_NEW.DP_ID_DP = CTE_NEW.DP_ID_DP  
                          AND NF_OLD.DP_ID_DP(+) = DESP_OLD.DP_ID_DP
                          AND NF_NEW.DP_ID_DP(+) = DESP_NEW.DP_ID_DP",
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate,
                    maior30Dias
                        ? request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                              .StartDate.AddDays(31)
                        : request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                              .EndDate);

                IStatelessSession session = this.SessionManager.OpenStatelessSession();
                IQuery query = session.CreateSQLQuery(sb.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<EletronicBillofLadingCancelledData>());
                string dataFim;

                if (
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30)
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .StartDate.AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .EndDate.ToShortDateString();
                }

                string dataIni =
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                        .ToShortDateString();

                query.SetResultTransformer(Transformers.AliasToBean<EletronicBillofLadingCancelledDataDto>());

                IList<EletronicBillofLadingCancelledDataDto> result =
                    query.List<EletronicBillofLadingCancelledDataDto>();

                IEnumerable<EletronicBillofLadingCancelledData> dados =
                    result.GroupBy(a => a.CteCancelledId)
                        .Select(
                            a =>
                            new EletronicBillofLadingCancelledData
                                {
                                    CteCancelledId = a.First().CteCancelledId,
                                    CteLadingId = a.First().CteLadingId,
                                    CteCancelledKey = a.First().CteCancelledKey,
                                    CteLadingKey = a.First().CteLadingKey,
                                    WayBillsInfoCancelled =
                                        a.Select(
                                            b =>
                                            new WayBillsInfo
                                                {
                                                    WaybillId = b.WaybillInvoiceCancelledId,
                                                    WaybillKey = b.WaybillInvoiceCancelledKey,
                                                }).ToList(),
                                    WayBillsInfoLading =
                                        a.Select(
                                            b =>
                                            new WayBillsInfo
                                                {
                                                    WaybillId = b.WaybillInvoiceId,
                                                    WaybillKey = b.WaybillInvoiceKey
                                                }).ToList()
                                });

                message = "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni + " E " + dataFim;
                return dados.ToList();
            }

            return new List<EletronicBillofLadingCancelledData>();
        }

        /// <summary>
        ///     Obtem os despachos cancelados e os próximos despachos gerados
        /// </summary>
        /// <param name="request">Objeto EletronicBillofLadingCancelledRailRequest com as datas para busca</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Lista de despachos cancelados</returns>
        public IList<DespachoCanceladoDto> ObterDespachosCancelados(
            EletronicBillofLadingCancelledRailRequest request,
            out string message)
        {
            if (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                > request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate)
            {
                message = "ERRO-003: Data Inicial deve ser menor que a data Final";
            }
            else
            {
                bool maior30Dias =
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30;
                var sb = new StringBuilder();
                sb.AppendFormat(
                    @"
                    SELECT canc.dp_id_dp AS ""IdDespachoCancelado"",
                           JOIN(CURSOR(SELECT dp.dp_id_dp
                                       FROM   despacho      dp,
                                              item_despacho idp,
                                              t2_pedido     ped1
                                       WHERE  dp.dp_id_dp         = idp.dp_id_dp
                                        AND   ped1.w1_id_ped      = nvl(dp.px_idt_pdr, dp.pt_id_ort)
                                        AND   idp.vg_id_vg        = canc.vg_id_vg
                                        AND   dp.dp_dt            BETWEEN canc.dcc_timestamp AND canc.dcc_timestamp+1
                                        AND   ped1.w1_idt_est_ori = ped.w1_idt_est_ori
                                        AND   dp.dp_dat_cld       IS NULL), '-') AS ""IdProximoDespacho""
                    FROM   (
                            SELECT nvl(dp.px_idt_pdr, dp.pt_id_ort) id_pedido,
                                   dcc.*
                            FROM   detalhe_carregamento_cancelado dcc,
                                   despacho                        dp,
                                   item_despacho                  idp
                            WHERE  dcc.dp_id_dp       = dp.dp_id_dp
                             AND   dp.dp_id_dp        = idp.dp_id_dp
                             AND   dcc.vg_id_vg       = idp.vg_id_vg
                             AND   dcc.dcc_timestamp BETWEEN  to_date('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') 
                                                     AND to_date('{1:dd/MM/yyyy HH:mm:ss}','dd/mm/yyyy hh24:mi:ss')
                           )          canc,
                           t2_pedido  ped
                    WHERE
                    canc.id_pedido = ped.w1_id_ped
                     AND   EXISTS         (SELECT 1
                                           FROM   area_operacional ao,
                                                  municipio        mn
                                           WHERE  ao.mn_id_mnc = mn.mn_id_mnc
                                            AND   mn.es_id_est = 62
                                            AND   ao.ao_id_ao  = ped.w1_idt_est_ori)",
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate,
                    maior30Dias
                        ? request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                              .StartDate.AddDays(31)
                        : request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                              .EndDate);

                string dataFim;

                if (
                    (request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.EndDate
                     - request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                           .StartDate).TotalDays > 30)
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .StartDate.AddDays(30).ToShortDateString();
                }
                else
                {
                    dataFim =
                        request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail
                            .EndDate.ToShortDateString();
                }

                string dataIni =
                    request.ElectronicBillofLadingCancelledRailType.ElectronicBillofLadingCancelledRailDetail.StartDate
                        .ToShortDateString();

                IStatelessSession session = this.SessionManager.OpenStatelessSession();
                IQuery query = session.CreateSQLQuery(sb.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<DespachoCanceladoDto>());

                message = "PESQUISA REALIZADA ENTRE OS DIAS " + dataIni + " E " + dataFim;

                return query.List<DespachoCanceladoDto>();
            }

            return new List<DespachoCanceladoDto>();
        }

        #endregion
    }
}