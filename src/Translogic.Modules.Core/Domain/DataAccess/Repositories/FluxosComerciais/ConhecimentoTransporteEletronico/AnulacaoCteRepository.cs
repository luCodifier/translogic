﻿using System.Threading;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    public class CteAnuladoRepository : NHRepository<CteAnulado, int?>, ICteAnuladoRepository
    {
        public CteAnulado ObterPorIdCteAnulacao(int idCteAnulado)
        {
            var criteria = CriarCriteria().Add(Restrictions.Eq("CteAnulacao.Id", idCteAnulado));
            return ObterPrimeiro(criteria);
        }
    }
}