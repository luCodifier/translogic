namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Reposit�rio da classe de detalhe do cte
    /// </summary>
    public class CteDetalheRepository : NHRepository<CteDetalhe, int?>, ICteDetalheRepository
    {
        /// <summary>
        /// Obt�m a lista com os detalhes do Cte
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado os detalhes</param>
        /// <returns>Lista com os detalhes</returns>
        public IList<CteDetalhe> ObterPorCte(Cte cte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteDetalhe cd  
									INNER JOIN FETCH cd.Cte c
									INNER JOIN FETCH c.FluxoComercial fc
									WHERE cd.Cte.Id = :idCte ";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteDetalhe>();
            }
        }

        public MalhaEmitenteDto GetMalhaByEstacao(string estacao)
        {
            var sql = @"SELECT COD_MALHA AS ""CodigoMalha""
                              ,DOMINIO AS ""DominioEmitente""
                              ,NOME AS ""NomeEmitente""
                              ,CNPJ AS ""CnpjEmitente""
                          FROM (SELECT ME.COD_MALHA, ME.DOMINIO, ME.NOME, ME.CNPJ
                                  FROM MALHA_EMITENTE ME
                                 WHERE ME.COD_MALHA = (SELECT UP.UP_IND_MALHA
                                                         FROM AREA_OPERACIONAL AO
                                                         JOIN UNID_PRODUCAO UP
                                                           ON AO.UP_ID_UNP = UP.UP_ID_UNP
                                                        WHERE AO.AO_COD_AOP = :P_Estacao))
                        WHERE ROWNUM = 1";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                query.SetParameter("P_Estacao", estacao);
                query.SetResultTransformer(Transformers.AliasToBean<MalhaEmitenteDto>());
                return (MalhaEmitenteDto)query.UniqueResult();
            }
        }
    }
}