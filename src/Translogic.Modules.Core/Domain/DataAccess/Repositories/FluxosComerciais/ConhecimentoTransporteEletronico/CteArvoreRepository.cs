namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe de Cte Arvore
	/// </summary>
	public class CteArvoreRepository : NHRepository<CteArvore, int?>, ICteArvoreRepository
	{
		/// <summary>
		/// Obt�m lista da arvore pelo cte raiz
		/// </summary>
		/// <param name="cteRaiz">Cte raiz da arvore</param>
		/// <returns>Lista dos elementos da arvore</returns>
		public IList<CteArvore> ObterPorCteRaiz(Cte cteRaiz)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("CteRaiz", cteRaiz));
			criteria.AddOrder(new Order("Sequencia", true));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m a arvore de Cte pelo cte filho
		/// </summary>
		/// <param name="cteFilho">Cte filho que est� contido na arvore</param>
		/// <returns>Retorna a lista com os elementos da arvore</returns>
		public IList<CteArvore> ObterArvorePorCteFilho(Cte cteFilho)
		{
			IList<CteArvore> result;
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();

			StringBuilder sb = new StringBuilder();
			sb.Append(
				@"select car
									FROM CteArvore car,
											 CteArvore caf
									WHERE car.CteRaiz.Id = caf.CteRaiz.Id
									AND   caf.CteFilho = :IdCteFilho
									ORDER BY car.Sequencia");

			parametros.Add(q => q.SetInt32("IdCteFilho", (int)cteFilho.Id));

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(sb.ToString());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				foreach (Action<IQuery> action in parametros)
				{
					action.Invoke(query);
				}

				result = query.List<CteArvore>();
			}

			return result;
		}

		/// <summary>
		/// Obt�m o cte Folha da arvore
		/// </summary>
		/// <param name="cteFilho">Cte filho que est� contido na arvore</param>
		/// <returns>Retorna o Cte Folha da arvore</returns>
		public CteArvore ObterCteFolhaDaArvore(Cte cteFilho)
		{
			IList<CteArvore> lista = ObterArvorePorCteFilho(cteFilho);

			CteArvore cteRetorno = lista.OrderByDescending(c => c.Id).FirstOrDefault();

			return cteRetorno;
		}

		/// <summary>
		/// Obt�m o cte arvore pelo cte filho (obt�m o n� da arvore)
		/// </summary>
		/// <param name="cteFilho">Cte filho do n� da arvore</param>
		/// <returns>Retorna o n� do cte arvore</returns>
		public CteArvore ObterCteArvorePorCteFilho(Cte cteFilho)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("CteFilho", cteFilho));
			criteria.AddOrder(new Order("Sequencia", true));
			criteria.CreateAlias("CteRaiz", "cr", JoinType.InnerJoin);
            criteria.CreateAlias("CtePai", "cp", JoinType.InnerJoin);
            criteria.SetFetchMode("cp", FetchMode.Select);
            criteria.SetFetchMode("cr", FetchMode.Select);
			return ObterPrimeiro(criteria);
		}
	}
}