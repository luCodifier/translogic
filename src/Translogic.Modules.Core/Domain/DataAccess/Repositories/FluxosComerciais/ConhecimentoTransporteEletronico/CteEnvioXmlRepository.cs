namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Dto;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;

	/// <summary>
	/// Classe do reposit�rio do envio de xml
	/// </summary>
	public class CteEnvioXmlRepository : NHRepository<CteEnvioXml, int?>, ICteEnvioXmlRepository
	{
		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		public IList<CteEnvioXml> ObterPorCte(Cte cte)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cte", cte));
			criteria.Add(Restrictions.Eq("Ativo", true));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		public IList<CteEnvioXml> ObterTodosPorCte(Cte cte)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cte", cte));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="idCte">Id Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		public IList<ReenvioEmailDto> ObterPorCte(int idCte)
		{
			string hql = @"  SELECT  
                                    c.Id                as IdXmlEnvio,  
                                    c.Cte.Id            as IdCte,  
                                    c.EnvioPara         as EnvioPara,  
                                    c.TipoEnvioCte      as TipoEnvioCte,  
                                    c.TipoArquivoEnvio  as TipoArquivoEnvio,  
                                    case when (select COUNT(*) from CteEnvioEmailErro er where er.Cte.Id = c.Cte.Id AND er.EmailPara LIKE '%' || c.EnvioPara || '%') = 0  Then
											'N' else 'S' END as Erro
                               FROM CteEnvioXml c 
                              WHERE c.Cte.Id = ( :IdCte ) ";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetInt32("IdCte", idCte);

				query.ReturnTypes[5] = new BooleanCharType();

				query.SetResultTransformer(Transformers.AliasToBean(typeof(ReenvioEmailDto)));

				return query.List<ReenvioEmailDto>();
			}
		}

		/// <summary>
		/// Retorna os CteEnvioXml
		/// </summary>
		/// <param name="idCtes">id dos Cte</param>
		/// <returns> Retorna a lista de Email</returns>
		public IList<CteEnvioXml> ObterTodosPorCte(int[] idCtes)
		{
			string hql = @"  SELECT c FROM CteEnvioXml c WHERE c.Cte.Id in ( :IdCtes ) ";

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);

				query.SetParameterList("IdCtes", idCtes);

				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<CteEnvioXml>();
			}
		}

		/// <summary>
		/// Remove a lista de envio por Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		public void RemoverPorCte(Cte cte)
		{
			try
			{
				// Remove da tabela de interface
				string hql = @"DELETE FROM CteEnvioXml cex WHERE cex.Cte.Id = :idCte";
				using (ISession session = OpenSession())
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idCte", cte.Id.Value);
					query.ExecuteUpdate();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Obt�m por cte e por email
		/// </summary>
		/// <param name="cte">Cte de refer�ncia</param>
		/// <param name="email">Email para ser procurado</param>
		/// <returns>Retorna a instancia do objeto</returns>
		public CteEnvioXml ObterPorCte(Cte cte, string email)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Cte", cte));
			criteria.Add(Restrictions.Eq("EnvioPara", email));
			return ObterPrimeiro(criteria);
		}
	}
}