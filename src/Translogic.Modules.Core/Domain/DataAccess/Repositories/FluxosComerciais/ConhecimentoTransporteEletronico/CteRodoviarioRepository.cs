﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.CteRodoviario;
    using Model.CteRodoviario.Outputs;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Repositorio das consultas de ctes rodoviarios
    /// </summary>
    public class CteRodoviarioRepository : NHRepository, ICteRodoviarioRepository
    {
        /// <summary>
        /// Metodo que retorna os dados dos ctes rodoviarios para o ws
        /// </summary>
        /// <param name="request">Objeto de input do servico</param>
        /// <returns>Retorna output do servico</returns>
        public List<ElectronicBillofLadingDataDto> InformationCtesRodoviarios(InformationElectronicBillofLadingHighwayRequest request)
        {
            var maior30Dias = (request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.EndDate - request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate).TotalDays > 30;
            var sb = new StringBuilder();
            sb.AppendFormat(
                @"  SELECT C.CR_ID_CR               AS ""ElectronicBillofLadingId"",
                           C.CR_BASE_ADUANEIRA      AS ""CustomsBase"",
                           C.CR_BASE_ADUANEIRA_DESC AS ""DescriptionCustomsBase"",
                           C.CR_DATA_DESCARGA       AS ""DateDischarge"",
                           C.CR_CHAVE_CTE_RODO      AS ""ElectronicBillofLadingKey"",
                           N.CRN_CHAVE_NFE_RODO     AS ""DtoWaybillInvoiceKey"",
                           N.CRN_PESO_DECLARADO     AS ""DtoDeclaredWeight"",
                           N.CRN_PESO_AFERIDO       AS ""DtoMeasuredWeight"",
                           N.CRN_DIFERENCA_PESO     AS ""DtoWeightDifference""
                      FROM CTE_RODOVIARIO C, CTE_RODOVIARIO_NOTAS N

                     WHERE N.CR_ID_CR = C.CR_ID_CR      
                       AND C.CR_DATA_DESCARGA BETWEEN
                           TO_DATE('{0:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss') AND
                           TO_DATE('{1:dd/MM/yyyy HH:mm:ss}', 'dd/mm/yyyy hh24:mi:ss')  ",
                        request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate,
                        maior30Dias ? request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.StartDate.AddDays(31) : request.InformatioElectronicBillofLadingHighway.InformationElectronicBillofLadingHighwayType.ElectronicBillofLadingHighwayDetail.EndDate
                        );

            var session = SessionManager.OpenStatelessSession();
            IQuery query = session.CreateSQLQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<ElectronicBillofLadingDataDto>());

            var result = query.List<ElectronicBillofLadingDataDto>();

            return result.ToList();
        }
    }
}