﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate.Criterion;

    /// <summary>
    /// Repositório da classe CorrecaoConteinerCte
    /// </summary>
    public class CorrecaoConteinerCteRepository : NHRepository<CorrecaoConteinerCte, int?>, ICorrecaoConteinerCteRepository
    {
        /// <summary>
        /// Obtém as informações de correção para o Cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Informações da Carta de Correção</returns>
        public IList<CorrecaoConteinerCte> ObterPorCte(Cte cte)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Cte", cte));
            return ObterTodos(criteria);
        }
    }
}