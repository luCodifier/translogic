namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;

    /// <summary>
	/// Reposit�rio da classe CteTemp
	/// </summary>
	public class CteTempRepository : NHRepository<CteTemp, int>, ICteTempRepository
	{
	    /// <summary>
	    /// Gera o cte de manuten��o pelas tabelas tempor�rias
	    /// </summary>
	    /// <param name="usuario">Usu�rio que est� realizando a opera��o</param>
	    public void GerarCteManutencaoPorTemporarias(Usuario usuario)
	    {
            using (ISession session = OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_CTE.GERAR_POR_TEMPORARIAS",
                    Connection = (OracleConnection)session.Connection
                };
                AddParameter(command, "PIDUSUARIO", OracleType.Number, usuario.Id, ParameterDirection.Input);

                session.Transaction.Enlist(command);

                command.Prepare();
                command.ExecuteNonQuery();
            }
	    }

        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> ser� adicionado</param>
        /// <param name="nome">Nome do par�metro</param>
        /// <param name="tipo">Tipo do par�metro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Dire��o <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;

            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }

	    /// <summary>
        /// Adicionar um novo <see cref="cte"/> ao <see cref="cte"/>
	    /// </summary>
        /// <param name="cte"><see cref="cte"/> onde o </param>
        public void SalvarCteTemp(CteTemp cte)
	    {
	        using (var session = OpenSession())
	        {
	            using (var trans = session.BeginTransaction())
	            {
                    session.SaveOrUpdate(cte);
                    session.Flush();

	                trans.Commit();
	            }
	        }
	    }

	}
}