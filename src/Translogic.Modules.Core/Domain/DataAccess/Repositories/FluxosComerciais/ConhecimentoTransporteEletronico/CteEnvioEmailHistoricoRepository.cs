using System;
using System.Linq;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Dto;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Repositorio da classe de envio de email historico
	/// </summary>
	public class CteEnvioEmailHistoricoRepository : NHRepository<CteEnvioEmailHistorico, int?>, ICteEnvioEmailHistoricoRepository
	{
        public IList<HistoricoEnvioEmailDto> ObterPor(IList<int> idCtes)
        {
            var sql = @"SELECT 
                      v.vg_cod_vag 					as ""CodigoVagao""
                      ,c.nro_cte					as ""NumeroCte""
                      ,c.chave_cte					as ""ChaveCte""
                      ,c.data_cte					as ""DataHoraCte""
                      ,ceh.tipo_arquivo_envio		as ""TipoArquivo""
                      ,ceh.data_hora				as ""DataHoraEnvio""
                      ,'E-mail enviado com sucesso' as ""Status""
                      ,ceh.email_para				as ""EnviadoPara""
                      ,ceh.email_cc					as ""EnviadoCc""
                      ,ceh.email_co					as ""EnviadoCo""
                    FROM cte c 
                           JOIN VAGAO v ON c.vg_id_vg = v.vg_id_vg
                           JOIN CTE_ENVIO_EMAIL_HIST ceh on ceh.id_cte = c.id_cte
                    where c.ID_CTE in( :idCtes )     

                    UNION 

                    SELECT 
                      v.vg_cod_vag 					as ""CodigoVagao""
                      ,c.nro_cte					as ""NumeroCte""
                      ,c.chave_cte					as ""ChaveCte""
                      ,c.data_cte					as ""DataHoraCte""
                      ,cee.tipo_arquivo_envio		as ""TipoArquivo""
                      ,cee.data_hora				as ""DataHoraEnvio""
                      ,cee.descricao_erro 			as ""Status""
                      ,cee.email_para				as ""EnviadoPara""
                      ,cee.email_cc					as ""EnviadoCc""
                      ,cee.email_co					as ""EnviadoCo""
                    FROM cte c 
                           JOIN VAGAO v ON c.vg_id_vg = v.vg_id_vg
                           JOIN CTE_ENVIO_EMAIL_ERRO cee on cee.id_cte = c.id_cte
                    where c.ID_CTE in( :idCtes )";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);
                    query.SetParameterList("idCtes", idCtes);

                    query.SetResultTransformer(Transformers.AliasToBean(typeof(HistoricoEnvioEmailDto)));
                    IList<HistoricoEnvioEmailDto> items = query.List<HistoricoEnvioEmailDto>();

                    return items.ToList();
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}