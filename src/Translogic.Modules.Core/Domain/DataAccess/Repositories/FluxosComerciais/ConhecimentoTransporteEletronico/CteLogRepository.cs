namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Net;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Diversos.Cte;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using Util;

    /// <summary>
    /// Reposit�rio da classe CteLog
    /// </summary>
    public class CteLogRepository : NHRepository<CteLog, int?>, ICteLogRepository
    {
        /// <summary>
        /// Insere o log de informa��o
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="mensagem">Mensagem de log</param>
        public void InserirLogInfo(Cte cte, string mensagem)
        {
            InserirLogInfo(cte, string.Empty, mensagem);
        }

        /// <summary>
        /// Insere o log de informa��o
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="builder">String builder do log</param>
        public void InserirLogInfo(Cte cte, StringBuilder builder)
        {
            InserirLogInfo(cte, builder.ToString());
        }

        /// <summary>
        /// Insere o log de informa��o
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="nomeServico">Nome do servi�o ou m�todo que est� sendo logado</param>
        /// <param name="mensagem">Mensagem de log</param>
        public void InserirLogInfo(Cte cte, string nomeServico, string mensagem)
        {
            using (ISession session = OpenSession())
            {
                CteLog cteLog = new CteLog();
                cteLog.Cte = cte;
                cteLog.NomeServico = nomeServico;
                cteLog.Descricao = Tools.TruncateString(mensagem, 250);
                cteLog.TipoLog = TipoLogCteEnum.Info;
                cteLog.DataHora = DateTime.Now;
                cteLog.HostName = Dns.GetHostName();
                Inserir(cteLog);
            }
        }

        /// <summary>
        /// Insere o log de erro
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        /// <param name="ex">Exception do erro</param>
        public void InserirLogErro(Cte cte, string mensagem, Exception ex = null)
        {
            InserirLogErro(cte, string.Empty, mensagem, ex);
        }

        /// <summary>
        /// Insere o log de erro
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="builder">String builderdo log</param>
        public void InserirLogErro(Cte cte, StringBuilder builder)
        {
            InserirLogErro(cte, builder.ToString());
        }

        /// <summary>
        /// Insere o log de erro
        /// </summary>
        /// <param name="cte">Cte a ser logado</param>
        /// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        public void InserirLogErro(Cte cte, string nomeServico, string mensagem, Exception ex = null)
        {
            if (ex != null)
            {
                string msg;
                if (cte == null)
                {
                    msg = string.Format("{0} - {1}", nomeServico, mensagem);
                    Sis.LogException2(new Exception(msg));
                }
                else
                {
                    msg = string.Format("ERRO DE CTE: Id={0} - Chave={1} - Servico={2} - Mensagem={3}", cte.Id, cte.Chave, nomeServico, mensagem);
                    Sis.LogException(ex, msg);
                }
            }

            using (ISession session = OpenSession())
            {
                CteLog cteLog = new CteLog();
                cteLog.Cte = cte;
                cteLog.Descricao = Tools.TruncateString(mensagem, 3000);
                cteLog.NomeServico = nomeServico;
                cteLog.TipoLog = TipoLogCteEnum.Erro;
                cteLog.DataHora = DateTime.Now;
                cteLog.HostName = Dns.GetHostName();
                Inserir(cteLog);
            }
        }
    }
}