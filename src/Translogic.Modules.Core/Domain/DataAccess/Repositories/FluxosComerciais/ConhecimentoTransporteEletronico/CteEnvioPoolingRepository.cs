namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe de pooling de envio 
	/// </summary>
	public class CteEnvioPoolingRepository : NHRepository<CteEnvioPooling, int?>, ICteEnvioPoolingRepository
	{
		/// <summary>
		/// Remove os CT-e's do pool
		/// </summary>
		/// <param name="lista"> Lista de CTes a serem removidos. </param>
		public void RemoverPorLista(IList<Cte> lista)
		{
			string hql = @"DELETE FROM CteEnvioPooling WHERE Id = :idCte";
			using (ISession session = OpenSession())
			{
				foreach (Cte cte in lista)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idCte", cte.Id.Value);
					query.ExecuteUpdate();
				}
			}
		}

		/// <summary>
		/// Limpa a sess�o
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Clear();
				session.Flush();
			}
		}

		/// <summary>
		/// Limpa o objeto da sess�o
		/// </summary>
		/// <param name="pooling">Cte pooling sendo utilizado na sess�o</param>
		public void ClearSession(CteEnvioPooling pooling)
		{
			using (ISession session = OpenSession())
			{
				session.Evict(pooling);
			}
		}
	}
}
