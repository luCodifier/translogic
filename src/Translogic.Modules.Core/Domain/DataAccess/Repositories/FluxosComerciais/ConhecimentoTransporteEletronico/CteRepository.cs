namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using ALL.Core.AcessoDados;
    using ALL.Core.AcessoDados.TiposCustomizados;
    using ALL.Core.Dominio;
    using CustomType;
    using Model.Acesso;
    using Model.Diversos.Cte;
    using Model.Dto;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Interfaces.DadosFiscais;

    /// <summary>
    /// Reposit�rio da classe Cte 
    /// </summary>
    public class CteRepository : NHRepository<Cte, int?>, ICteRepository
    {
        /// <summary>
        /// Fabrica de sess�es injetadas
        /// </summary>
        public ISessionFactory SessionFactory { get; set; }

        /// <summary>
        ///    Insere o CTE  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade do CTE</param>
        /// <returns>Retorna a entidade inserida </returns>
        public Cte InserirSemSessao(Cte entity)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.Insert(entity) as Cte;
            }
        }

        /// <summary>
        ///    Atualizar o CTE  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade do CTE</param>
        /// <returns>Retorna a entidade atualizada </returns>
        public Cte AtualizarSemSessao(Cte entity)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                session.Update(entity);
            }
            return entity;
        }

        /// <summary>
        /// Obtem os ctes pela nfe de anula��o.
        /// </summary>
        /// <param name="nfeAnulacao">Nota de anula��o</param>
        /// <returns>retorna a lista de Ct-e </returns>
        public IList<Cte> ObterPorChaveAnulacao(NotaFiscalAnulacao nfeAnulacao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem os ctes pelo despacho.
        /// </summary>
        /// <param name="despachoId">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e </returns>
        public IList<Cte> ObterCtesPorDespacho(int despachoId)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Despacho.Id", despachoId));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem os ctes pelos despachos.
        /// </summary>
        /// <param name="despachosId">Lista de id dos despachos</param>
        /// <returns>retorna a lista de Ct-es </returns>
        public IList<Cte> ObterCtesPorDespachos(List<int> despachosId)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Despacho.Id", despachosId));

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Obtem os ctes por uma lista de IDs.
        /// </summary>
        /// <param name="ids">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e</returns>
        public IList<Cte> ObterPorIds(List<int> ids)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("FluxoComercial", "fc", JoinType.InnerJoin);
            criteria.CreateAlias("fc.Origem", "ori", JoinType.InnerJoin);
            criteria.CreateAlias("fc.Destino", "dst", JoinType.InnerJoin);
            criteria.CreateAlias("fc.Mercadoria", "mer", JoinType.InnerJoin);
            criteria.CreateAlias("Despacho", "dp", JoinType.InnerJoin);
            criteria.CreateAlias("Vagao", "vag", JoinType.InnerJoin);
            criteria.CreateAlias("vag.Serie", "ser", JoinType.InnerJoin);
            criteria.CreateAlias("ListaDetalhes", "ld", JoinType.InnerJoin);

            criteria.SetFetchMode("fc", FetchMode.Eager);
            criteria.SetFetchMode("ori", FetchMode.Eager);
            criteria.SetFetchMode("dst", FetchMode.Eager);
            criteria.SetFetchMode("mer", FetchMode.Eager);
            criteria.SetFetchMode("dp", FetchMode.Eager);
            criteria.SetFetchMode("vag", FetchMode.Eager);
            criteria.SetFetchMode("ser", FetchMode.Eager);
            criteria.SetFetchMode("ld", FetchMode.Eager);

            criteria.Add(Restrictions.In("Id", ids));

            var resultados = ObterTodos(criteria);
            return resultados;
        }

        /// <summary>
        /// Obt�m cte anterior referente ao vag�o
        /// </summary>
        /// <param name="cte">Cte corrente, ser� retornado cte anterior ao mesmo</param>
        /// <returns>Retorna o CTE</returns>
        public Cte ObterCteAnteriorVagao(Cte cte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									INNER JOIN FETCH c.Despacho d
									INNER JOIN FETCH c.FluxoComercial f
									INNER JOIN FETCH c.Vagao vg
                  WHERE vg.Id = :idVagao 
                        AND c.Id != :idCte
                  ORDER BY c.Id DESC
                ";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idVagao", cte.Vagao.Id.Value);
                query.SetInt32("idCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);
                query.SetMaxResults(10);

                var list = query.List<Cte>();
                Cte cteRetorno = null;
                foreach (var cteAnt in list)
                {
                    var cancelado = cteAnt.Despacho.DataCancelamento.HasValue;
                    var descarregado = session.Query<ItemDespacho>().Any(a => a.DespachoTranslogic.Id == cteAnt.Despacho.Id && a.DetalheCarregamento.Carregamento.Descarregamento != null);
                    if (!cancelado && !descarregado)
                    {
                        continue;
                    }
                    else
                    {
                        cteRetorno = cteAnt;
                        break;
                    }
                }

                return cteRetorno;
            }
        }

        /// <summary>
        /// Retorna todos os Cte	
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int despacho)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.Add(Restrictions.Between("DataHora", dataInicial, dataFinal));

            criteria.CreateAlias("Despacho", "desp", JoinType.InnerJoin);
            criteria.CreateAlias("desp.SerieDespacho", "serdesp", JoinType.LeftOuterJoin);
            criteria.CreateAlias("desp.SerieDespachoUf", "serdespuf", JoinType.LeftOuterJoin);

            if (serie != 0 && despacho != 0)
            {
                criteria.Add(Restrictions.Eq("serdesp.SerieDespachoNum", Convert.ToString(serie)));
                criteria.Add(Restrictions.Eq("desp.NumeroDespacho", despacho));
            }

            criteria.SetFetchMode("desp", FetchMode.Eager);
            criteria.SetFetchMode("serdesp", FetchMode.Eager);
            criteria.SetFetchMode("serdespuf", FetchMode.Eager);

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="codVagao">C�digo do Vag�o</param>
        /// <param name="chave">Chave do CTE</param>
        /// <param name="serieCte">S�rie do CTE</param>
        /// <param name="nroCte">N�mero do CTE</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string codVagao, string chave, int serieCte, int nroCte)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.Add(Restrictions.Between("DataHora", dataInicial, dataFinal));

            criteria.CreateAlias("Despacho", "desp", JoinType.InnerJoin);
            criteria.CreateAlias("desp.SerieDespacho", "serdesp", JoinType.LeftOuterJoin);
            criteria.CreateAlias("desp.SerieDespachoUf", "serdespuf", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Vagao", "vag", JoinType.LeftOuterJoin);

            if (chave != string.Empty)
            {
                criteria.Add(Restrictions.Eq("Chave", chave.PadLeft(44, '0')));
            }

            if (serieCte != 0)
            {
                criteria.Add(Restrictions.Eq("Serie", Convert.ToString(serieCte).PadLeft(3, '0')));
            }

            if (nroCte != 0)
            {
                criteria.Add(Restrictions.Eq("Numero", Convert.ToString(nroCte).PadLeft(9, '0')));
            }

            // criteria.Add(Restrictions.Eq("SituacaoAtual", situacao));

            if (serie != 0 && numDespacho != 0)
            {
                criteria.Add(Restrictions.Eq("serdesp.SerieDespachoNum", Convert.ToString(serie)));
                criteria.Add(Restrictions.Eq("desp.NumeroDespacho", numDespacho));
            }

            if (codVagao != string.Empty)
            {
                criteria.Add(Restrictions.Eq("vag.Codigo", codVagao));
            }

            criteria.SetFetchMode("desp", FetchMode.Eager);
            criteria.SetFetchMode("serdesp", FetchMode.Eager);
            criteria.SetFetchMode("serdespuf", FetchMode.Eager);
            criteria.SetFetchMode("vag", FetchMode.Eager);

            return ObterTodos(criteria);
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>b
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteManutencaoPendente(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							c.Id = 
							( 
							    SELECT CA2.CteFilho.Id 
							      FROM CteArvore CA2 
							     WHERE CA2.Id = 
								    ( 
								    SELECT MAX(CAR.Id) FROM CteArvore CAR, 
								    CteArvore CAF
								    WHERE 
								    CAR.CteRaiz.Id = CAF.CteRaiz.Id 
								    AND CAF.CteFilho.Id = c.Id
								    ) 
							) 
						AND c.DataHora BETWEEN :dataInicial AND :dataFinal  
						AND c.SituacaoAtual <> 'AUT' 
						AND c.SituacaoAtual <> 'CAN'
						AND (c.Manutencao = 'S')
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia )");

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetDateTime("dataInicial", dataInicial);
            queryCount.SetDateTime("dataInicial", dataInicial);

            query.SetDateTime("dataFinal", dataFinal);
            queryCount.SetDateTime("dataFinal", dataFinal);

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination != null)
            {
                if (pagination.Inicio.HasValue)
                {
                    query.SetFirstResult(pagination.Inicio.Value);
                }

                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="impresso"> Indicador se cte impresso</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="incluirNfe">ir� fazer join com nfe</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, bool? impresso, string codigoUfDcl, bool incluirNfe)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE
                            ( 					
							     c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (impresso.HasValue)
            {
                sb.Append(" AND c.Impresso = :impresso ");
            }

            if (!String.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,

                                                    (select 
                                                         canucte.SituacaoAtual
                                                     from  
                                                         CteAnulado canu,
                                                         Cte canucte 
                                                     where 
                                                         canu.CteAnulacao.Id = canucte.Id and
                                                         canu.Cte.Id = c.Id 
                                                     ) as SituacaoCteAnulacao,
                                                    
                                                    case when desti is null then deest.Sigla else diest.Sigla end as UfDestino,
                                                    (
                                                       select 
                                                            count(cs1.Id)
                                                        from 
                                                            CteStatus cs1,
                                                            CteStatus cs2
                                                        where 
                                                            cs1.CteStatusRetorno.Id = 19 
                                                            and cs1.Cte.Id = c.Id
                                                            and c.SituacaoAtual = 'EAC' 
                                                            and cs2.Id = (select max(csi.Id) from CteStatus csi where csi.Cte.Id = c.Id)
                                                            and cs2.CteStatusRetorno.Id not in (27, 28)  
                                                    ) as ReemitirCancelamento,
                                                    (
                                                       select 
                                                            count(csa.Id)
                                                        from 
                                                            CteStatus csa
                                                        where 
                                                            csa.CteStatusRetorno.Id = 100 
                                                            and csa.Cte.Id = c.Id
                                                    ) as QtdeAutorizacoes
                                                    ");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            if (incluirNfe)
            {
                stringHqlCount = stringHqlCount.Replace("FROM Cte c ", " FROM CteDetalhe detalhe \n left join detalhe.Cte c ");
                stringHqlResult = stringHqlResult.Replace("FROM Cte c ", " FROM CteDetalhe detalhe \n left join detalhe.Cte c ");
                stringHqlResult = stringHqlResult.Replace("c.Id as CteId,", " c.Id as CteId, \n detalhe.ChaveNfe as Nfe, [pesos] ");
                stringHqlResult = stringHqlResult.Replace("[pesos]", " (select sn.Peso from StatusNfe sn where sn.ChaveNfe = detalhe.ChaveNfe) as PesoNfe , (select sn.PesoUtilizado from StatusNfe sn where sn.ChaveNfe = detalhe.ChaveNfe) as PesoUtilizadoNfe, \n ");
            }

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!String.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (impresso.HasValue)
            {
                query.SetParameter("impresso", impresso.Value, new BooleanCharType());
                queryCount.SetParameter("impresso", impresso.Value, new BooleanCharType());
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination != null)
            {
                if (pagination.Inicio.HasValue)
                {
                    query.SetFirstResult(pagination.Inicio.Value);
                }

                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero(s) do Vag�o(�es)</param>
        /// <param name="impresso"> Indicador se cte impresso</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="incluirNfe">ir� fazer join com nfe</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string[] numVagao, bool? impresso, string codigoUfDcl, bool incluirNfe)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE
                            ( 					
							     c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (impresso.HasValue)
            {
                sb.Append(" AND c.Impresso = :impresso ");
            }

            if (numVagao != null && numVagao.Length > 0)
            {
                sb.Append("AND v.Codigo IN (:vagao) ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino,
                                                    (
                                                       select 
                                                            count(cs1.Id)
                                                        from 
                                                            CteStatus cs1,
                                                            CteStatus cs2
                                                        where 
                                                            cs1.CteStatusRetorno.Id = 19 
                                                            and cs1.Cte.Id = c.Id
                                                            and c.SituacaoAtual = 'EAC' 
                                                            and cs2.Id = (select max(csi.Id) from CteStatus csi where csi.Cte.Id = c.Id)
                                                            and cs2.CteStatusRetorno.Id not in (27, 28)  
                                                    ) as ReemitirCancelamento,
                                                    (
                                                       select 
                                                            count(csa.Id)
                                                        from 
                                                            CteStatus csa
                                                        where 
                                                            csa.CteStatusRetorno.Id = 100 
                                                            and csa.Cte.Id = c.Id
                                                    ) as QtdeAutorizacoes
                                                    ");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            if (incluirNfe)
            {
                stringHqlCount = stringHqlCount.Replace("FROM Cte c ", " FROM CteDetalhe detalhe \n left join detalhe.Cte c ");
                stringHqlResult = stringHqlResult.Replace("FROM Cte c ", " FROM CteDetalhe detalhe \n left join detalhe.Cte c ");
                stringHqlResult = stringHqlResult.Replace("c.Id as CteId,", " c.Id as CteId, \n detalhe.ChaveNfe as Nfe, [pesos] ");
                stringHqlResult = stringHqlResult.Replace("[pesos]", " (select sn.Peso from StatusNfe sn where sn.ChaveNfe = detalhe.ChaveNfe) as PesoNfe , (select sn.PesoUtilizado from StatusNfe sn where sn.ChaveNfe = detalhe.ChaveNfe) as PesoUtilizadoNfe, \n ");
            }

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (numVagao != null && numVagao.Length > 0)
            {
                query.SetParameterList("vagao", numVagao);
                queryCount.SetParameterList("vagao", numVagao);
            }

            if (impresso.HasValue)
            {
                query.SetParameter("impresso", impresso.Value, new BooleanCharType());
                queryCount.SetParameter("impresso", impresso.Value, new BooleanCharType());
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination != null)
            {
                if (pagination.Inicio.HasValue)
                {
                    query.SetFirstResult(pagination.Inicio.Value);
                }

                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para anula��o
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>b
        /// <param name="horasCancelamento"> Horas cancelamento</param>b
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteAnulacao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string origem, string destino, string numVagao, int horasCancelamento)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
                            LEFT JOIN c.CteMotivoCancelamento cmc
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN fc.Contrato ct
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							( 					
							     c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
						AND c.SituacaoAtual in ('AGA')
						AND c.NfeAnulacao is null
                        AND c.TipoCte NOT IN ('SBT')  
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (chaveCte == null)
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (despacho != 0)
            {
                sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != null)
            {
                sb.Append(" AND c.Chave IN (:chaveCte) ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                sb.ToString(),
                                                @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Serie as SerieCte,
                                                    fc.IndRateioCte as RateioCte,
													c.Chave as Chave,	
													c.ValorCte as ValorCte,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
                                                    ct.IndIeToma as IndIeToma, 
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino,
                                                    CASE WHEN (SELECT MAX((SYSDATE - (S1.DataHora + :HorasCancelamento /24)) * 24)
														   FROM CteStatus S1
														INNER JOIN S1.CteStatusRetorno statusRet1
														INNER JOIN S1.Cte ctes1
															WHERE ctes1.Id = c.Id
															AND statusRet1.Id = 100) > :HorasCancelamento Then 
														'S'
													Else 
														'N' 
													End as Acima168horas,
                                                    (SELECT CASE WHEN count(VPV2.Id) > 0 then 'N'
													Else 
														'S' end
							                              FROM ItemDespacho ITD2
							                              JOIN ITD2.DetalheCarregamento DC2
							                              JOIN DC2.Carregamento CG2,
							                              VagaoPedidoVigente VPV2
							                             WHERE CG2.IdPedido = VPV2.Pedido.Id 
							                               AND VPV2.Vagao = c.Vagao 
							                               AND ITD2.DespachoTranslogic = c.Despacho							
						                                ) as Descarregados,
                                                        cmc.EnviaSefaz as PermiteCancRejeitado
                                                     ");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (chaveCte == null)
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (chaveCte != null)
            {
                query.SetParameterList("chaveCte", chaveCte);
                queryCount.SetParameterList("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            query.SetInt32("HorasCancelamento", horasCancelamento);

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination != null)
            {
                if (chaveCte == null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);
                    if (limit > 0)
                    {
                        query.SetMaxResults(limit);
                    }
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteRelatorioErro(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string origem, string destino)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							c.Id = 
							( 
							    SELECT CA2.CteFilho.Id 
							      FROM CteArvore CA2 
							     WHERE CA2.Id = 
								    ( 
								    SELECT MAX(CAR.Id) FROM CteArvore CAR, 
								    CteArvore CAF
								    WHERE 
								    CAR.CteRaiz.Id = CAF.CteRaiz.Id 
								    AND CAF.CteFilho.Id = c.Id
								    ) 
							) 
						AND c.DataHora BETWEEN :dataInicial AND :dataFinal  
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia )");

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            sb.Append(" AND (c.SituacaoAtual = 'EAR' OR c.SituacaoAtual = 'ERR') ");

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetDateTime("dataInicial", dataInicial);
            queryCount.SetDateTime("dataInicial", dataInicial);

            query.SetDateTime("dataFinal", dataFinal);
            queryCount.SetDateTime("dataFinal", dataFinal);

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination != null)
            {
                if (pagination.Inicio.HasValue)
                {
                    query.SetFirstResult(pagination.Inicio.Value);
                }

                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteGerarArquivoLote(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string origem, string destino, string[] numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							c.Id = 
							( 
							    SELECT CA2.CteFilho.Id 
							      FROM CteArvore CA2 
							     WHERE CA2.Id = 
								    ( 
								    SELECT MAX(CAR.Id) FROM CteArvore CAR, 
								    CteArvore CAF
								    WHERE 
								    CAR.CteRaiz.Id = CAF.CteRaiz.Id 
								    AND CAF.CteFilho.Id = c.Id
								    ) 
							) 
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia )
						AND c.SituacaoAtual = 'AUT' 
						");

            if (chaveCte == null)
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append(" AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (numVagao != null && numVagao.Length > 0)
            {
                sb.Append(" AND v.Codigo IN (:vagao) ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append(" AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != null)
            {
                sb.Append(" AND c.Chave IN (:chaveCte) ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append(" AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append(" AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
												    c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (chaveCte == null)
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (numVagao != null && numVagao.Length > 0)
            {
                query.SetParameterList("vagao", numVagao);
                queryCount.SetParameterList("vagao", numVagao);
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (chaveCte != null)
            {
                query.SetParameterList("chaveCte", chaveCte);
                queryCount.SetParameterList("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            if (chaveCte == null)
            {
                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos osdo agrupamento do conteiner.
        /// </summary>
        /// <param name="chaveCte">Chave do cte</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> ObterCtesAgrupamentoConteiner(string chaveCte)
        {
            using (var session = OpenSession())
            {
                string sql = @" SELECT c.* from 
                                CTE c
                                inner join
                                CTE_CONTEINER ccon on (c.ID_CTE = ccon.ID_CTE) where 
                                ID_AGRUPAMENTO in (
                                   SELECT ccon.Id_Agrupamento from 
                                   CTE c
                                   inner join
                                   CTE_CONTEINER ccon on (c.ID_CTE = ccon.ID_CTE) where c.CHAVE_CTE = :chaveCte
                                ); ";

                IQuery query = session.CreateSQLQuery(sql);
                query.SetString("chaveCte", chaveCte);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<Cte>();
            }
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data Inicial</param>
        /// <param name="dataFinal">Data Final</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="email">Chave do Cte informada</param>
        /// <param name="erro"> Emails com Erro</param>
        /// <param name="serie">Serie do despacho</param>
        /// <param name="numDespacho">Numero despacho</param>
        /// <param name="destino">Destino do despacho</param>
        /// <param name="origem">origem do despacho</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo Uf do despacho</param>
        /// <param name="numeroCte"> Numero do cte</param>
        /// <param name="chaveCte"> Array de chaves do cte</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteReenvioEmail(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string email, bool erro, string serie, int numDespacho, string destino, string origem, string numVagao, string codigoUfDcl, string numeroCte, string[] chaveCte)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
                                 FROM Cte c 
							LEFT  JOIN  c.Despacho d
							LEFT JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest                            
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 		
                        c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (chaveCte == null)
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(email))
            {
                sb.Append(@" AND EXISTS (SELECT 1 FROM CteEnvioXml xml WHERE c.Id = xml.Cte.Id AND UPPER(xml.EnvioPara) LIKE :email ) ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (chaveCte != null)
            {
                sb.Append(" AND c.Chave IN (:chaveCte) ");
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (numDespacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (!string.IsNullOrEmpty(codFluxo))
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (!string.IsNullOrEmpty(numeroCte))
            {
                sb.Append("AND c.Numero = :nroCte ");
            }

            if (erro)
            {
                sb.Append("  AND EXISTS (SELECT 1 FROM CteEnvioEmailErro erro WHERE  c.Id = erro.Cte.Id)  ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                    sb.ToString(),
                                                    @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
                                                    sdu.CodigoControle as CodigoControle,
									 				NVL(d.NumeroDespachoUf, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
													c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (chaveCte == null)
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (chaveCte != null)
            {
                query.SetParameterList("chaveCte", chaveCte);
                queryCount.SetParameterList("chaveCte", chaveCte);
            }

            if (numDespacho != 0)
            {
                query.SetInt32("despacho", numDespacho);
                queryCount.SetInt32("despacho", numDespacho);
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            if (!string.IsNullOrEmpty(codFluxo))
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (!string.IsNullOrEmpty(numeroCte))
            {
                query.SetString("nroCte", numeroCte);
                queryCount.SetString("nroCte", numeroCte);
            }

            if (!string.IsNullOrEmpty(email))
            {
                query.SetString("email", email);
                queryCount.SetString("email", email);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Salvar todos os CteEnvioXml
        /// </summary>
        /// <param name="emailAntigo"> e-mail a ser alterado</param>
        /// <param name="novoEmail"> Novo e-mail a ser alterado</param>
        /// <param name="fluxo"> Fluxo informado</param>
        public void SalvarEmailCteReenvioArquivo(string emailAntigo, string novoEmail, string fluxo)
        {
            StringBuilder hqlQuery = new StringBuilder();

            hqlQuery.AppendLine(@"UPDATE  CteEnvioXml X SET X.EnvioPara = :novoEmail
                                   WHERE  X.EnvioPara = :emailAntigo 
                                     AND  EXISTS (
                                              SELECT xml FROM CteEnvioXml xml WHERE  xml.EnvioPara = :emailAntigo  
                                         AND  EXISTS (SELECT C.Id FROM Cte C WHERE C.Id = xml.Cte.Id AND C.FluxoComercial.Codigo like :fluxo) 
                                         AND  xml.Id = X.Id)");

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hqlQuery.ToString());
                query.SetString("novoEmail", novoEmail);
                query.SetString("emailAntigo", emailAntigo);
                query.SetString("fluxo", '%' + fluxo);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteImpressao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, int serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							INNER JOIN  c.Despacho d
							INNER JOIN  c.Vagao v
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							c.Id = 
							( 
							SELECT CA2.CteFilho.Id 
							  FROM CteArvore CA2 
							 WHERE CA2.Id = 
								( 
								SELECT MAX(CAR.Id) FROM CteArvore CAR, 
								CteArvore CAF
								WHERE 
								CAR.CteRaiz.Id = CAF.CteRaiz.Id 
								AND CAF.CteFilho.Id = c.Id
								) 
							) 
						AND c.DataHora BETWEEN :dataInicial AND :dataFinal 
						AND c.SituacaoAtual = 'AUT' ");

            if (serie != 0)
            {
                sb.Append("AND sd.SerieDespachoNum = :serie ");
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (despacho != 0)
            {
                sb.Append("AND d.NumeroDespacho = :despacho ");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo = :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													NVL(sdu.NumeroSerieDespacho, 0) as SerieDesp6,
													NVL(d.NumeroDespacho, 0) as NumDesp6,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
													c.SituacaoAtual as SituacaoCte,							
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetDateTime("dataInicial", dataInicial);
            queryCount.SetDateTime("dataInicial", dataInicial);

            query.SetDateTime("dataFinal", dataFinal);
            queryCount.SetDateTime("dataFinal", dataFinal);

            if (serie != 0)
            {
                query.SetString("serie", Convert.ToString(serie));
                queryCount.SetString("serie", Convert.ToString(serie));
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", codFluxo);
                queryCount.SetString("codFluxo", codFluxo);
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();
            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna os ctes por IdCte
        /// </summary>
        /// <param name="ids">id dos cte</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> ObterTodosPorId(int[] ids)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT c FROM Cte c WHERE c.Id in ( :IdCtes ) order by c.ArquivoPdfGerado DESC ");

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());

            query.SetParameterList("IdCtes", ids);

            query.SetResultTransformer(Transformers.DistinctRootEntity);

            return query.List<Cte>();
        }

        /// <summary>
        /// Obtem os ctes por uma lista de IDs.
        /// </summary>
        /// <param name="ids">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e por hql</returns>
        public IList<Cte> ObterPorIdsHql(List<int> ids)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" SELECT cte FROM ");
            sb.AppendLine("      Cte  cte,  ");
            ////sb.AppendLine("      CteDetalhe  det, ");
            sb.AppendLine("      DespachoTranslogic  do,  ");
            sb.AppendLine("      VagaoPedidoVigente  v,  ");
            sb.AppendLine("      FluxoComercial  fl,  ");
            sb.AppendLine("      ItemDespacho  ide  ");
            sb.AppendLine(" WHERE ");
            sb.AppendLine("     do.PedidoDerivado.Id = v.PedidoDerivado.Id ");
            sb.AppendLine("     and do.Id = cte.Despacho.Id  ");
            sb.AppendLine("     and fl.Id = cte.FluxoComercial.Id ");
            ////sb.AppendLine("     and cte.Id = det.Cte.Id  ");
            sb.AppendLine("     and do.Id = ide.DespachoTranslogic.Id ");
            sb.AppendLine("     and cte.Id in( :IdCtes )");

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());

            query.SetParameterList("IdCtes", ids);

            query.SetResultTransformer(Transformers.DistinctRootEntity);

            return query.List<Cte>();
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<ManutencaoCteDto> ObterCteManutencao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            var sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
						  FROM Cte c 
							LEFT JOIN c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							LEFT JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
							LEFT JOIN c.CteMotivoCancelamento motivo
						WHERE 	
				
	                    c.Id = 
							( 
								SELECT CA2.CteFilho.Id 
								  FROM CteArvore CA2 
								 WHERE CA2.Id =     
									( 
										SELECT MAX(CAR.Id) FROM CteArvore CAR, 
										CteArvore CAF
										WHERE 
										CAR.CteRaiz.Id = CAF.CteRaiz.Id 
										AND CAF.CteFilho.Id = c.Id 
									) 
							)
						AND 
						EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
						AND (motivo.ExibirTelaManutencao IS NULL OR motivo.ExibirTelaManutencao = 'S')				
						AND (c.SituacaoAtual = :situacaoCancelado 
						 OR  c.SituacaoAtual = :situacaoAnulado
						 OR  c.SituacaoAtual = :situacaoInvalidado
                         OR  c.SituacaoAtual = :situacaoAnuladoConfig)
                        AND  c.AmbienteSefaz IN ( SELECT 
                                                     CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                  WHERE CSUF.Uf = c.SiglaUfFerrovia )
						
                        AND c.TipoCte NOT IN ('VTL','SBT')
            ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo like :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = '" + chaveCte + "'");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC ");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                    sb.ToString(),
                                                    @" c.Id as CteId,
                                                    fc.TipoServicoCte as TipoServicoCte,
													d.Id as IdDespacho,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,								
                                                    sdu.CodigoControle as CodigoControle,					
													v.Id as VagaoId,
													v.Codigo as CodVagao,													
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as CodigoOrigem,
													COALESCE(desti.Codigo, dest.Codigo) as CodigoDestino,
													m.Apelido as CodigoMercadoria,
													c.Numero as NroCte,
													c.Serie as SerieCte,
													c.Chave as ChaveCte,							
													c.DataHora as DataEmissao,
													c.PesoVagao as ToneladaUtil,
													c.VolumeVagao as Volume,   
                                                    c.SituacaoAtual as Situacao,                       
                                                    motivo.Descricao as MotivoDescricao,
                                                    (
                                                       select  
                                                          case when (csr.Id = 38) then 'Anulado Config' else 
                                                          case when (csr.Id = 36) then 'Anulado Nota Fiscal' else
                                                          csr.Descricao end end as Descricao                                                           
                                                       from 
                                                          CteStatus cs  
                                                       inner join 
                                                          cs.CteStatusRetorno csr 
                                                       where 
                                                          cs.Id  in ( select max(cstmp.Id) from CteStatus cstmp where cstmp.Cte.id = c.Id  and  cstmp.CteStatusRetorno.Id in (13, 38, 101, 128) )
                                                        
                                                    ) as StatusRetornoDescricao,

                                                    (select 
                                                         canucte.SituacaoAtual
                                                     from  
                                                         CteAnulado canu,
                                                         Cte canucte 
                                                     where 
                                                         canu.CteAnulacao.Id = canucte.Id and
                                                         canu.Cte.Id = c.Id 
                                                     ) as SituacaoCteAnulacao

                                                    ");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetParameter("situacaoCancelado", SituacaoCteEnum.Cancelado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoCancelado", SituacaoCteEnum.Cancelado, new SituacaoCteEnumCustomType());

            query.SetParameter("situacaoInvalidado", SituacaoCteEnum.Inutilizado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoInvalidado", SituacaoCteEnum.Inutilizado, new SituacaoCteEnumCustomType());

            query.SetParameter("situacaoAnulado", SituacaoCteEnum.Anulado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoAnulado", SituacaoCteEnum.Anulado, new SituacaoCteEnumCustomType());

            query.SetParameter("situacaoAnuladoConfig", SituacaoCteEnum.AnulacaoConfig, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoAnuladoConfig", SituacaoCteEnum.AnulacaoConfig, new SituacaoCteEnumCustomType());

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                // query.SetString("chaveCte", chaveCte);
                // queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(ManutencaoCteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<ManutencaoCteDto> items = query.List<ManutencaoCteDto>().Where(p => (p.Situacao == SituacaoCteEnum.AnulacaoConfig && p.SituacaoCteAnulacao == SituacaoCteEnum.Autorizado) || (p.Situacao != SituacaoCteEnum.AnulacaoConfig)).ToList();
            return new ResultadoPaginado<ManutencaoCteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna uma lista de Cte's para corre��o
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CorrecaoCteDto> ObterListaCteParaCorrecao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							LEFT JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
							LEFT JOIN c.CteMotivoCancelamento motivo
						WHERE 					
							c.Id = 
							( 
								SELECT CA2.CteFilho.Id 
								  FROM CteArvore CA2 
								 WHERE CA2.Id =     
									( 
										SELECT MAX(CAR.Id) FROM CteArvore CAR, 
										CteArvore CAF
										WHERE 
										CAR.CteRaiz.Id = CAF.CteRaiz.Id 
										AND CAF.CteFilho.Id = c.Id 
									) 
							)
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
                        AND NOT EXISTS (SELECT 1 FROM CorrecaoCte CO WHERE c.Id = CO.Cte.Id)
                        AND NOT EXISTS (SELECT 1 FROM CorrecaoConteinerCte CO WHERE c.Id = CO.Cte.Id)
						AND (c.SituacaoAtual  = :situacaoAutorizado)
                        AND c.TipoCte NOT IN ('VTL','SBT')
            ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo like :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = '" + chaveCte + "'");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC ");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													d.Id as IdDespacho,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,								
                                                    sdu.CodigoControle as CodigoControle,					
													v.Codigo as CodigoVagao,													
													fc.Codigo as CodFluxo,
													c.Numero as NroCte,
													c.Serie as SerieCte,
													c.Chave as ChaveCte,							
													c.DataHora as DataEmissao,
													c.PesoVagao as ToneladaUtil,
													c.VolumeVagao as Volume	");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetParameter("situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CorrecaoCteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CorrecaoCteDto> items = query.List<CorrecaoCteDto>();
            return new ResultadoPaginado<CorrecaoCteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<ManutencaoCteDto> ObterCteVirtual(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							INNER JOIN  c.Despacho d
							LEFT  JOIN d.SerieDespachoUf sdu
							LEFT  JOIN d.SerieDespacho sd
							INNER JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
							LEFT JOIN c.CteMotivoCancelamento motivo
						WHERE 					
							c.Id = 
							( 
								SELECT CA2.CteFilho.Id 
								  FROM CteArvore CA2 
								 WHERE CA2.Id = 
									( 
										SELECT MAX(CAR.Id) FROM CteArvore CAR, 
										CteArvore CAF
										WHERE 
										CAR.CteRaiz.Id = CAF.CteRaiz.Id 
										AND CAF.CteFilho.Id = c.Id
                                        AND NOT EXISTS (SELECT 1 FROM Cte x WHERE x.Id = CAR.CteFilho.Id and x.TipoCte = 'VTL')
									) 
							)
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
						AND (c.SituacaoAtual  = :situacaoAutorizado
                        OR c.SituacaoAtual  = :situacaoCancelado)
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia )");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo like :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = '" + chaveCte + "'");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC ");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													d.Id as IdDespacho,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,								
                                                    sdu.CodigoControle as CodigoControle,					
													v.Id as VagaoId,
													v.Codigo as CodVagao,													
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as CodigoOrigem,
													COALESCE(desti.Codigo, dest.Codigo) as CodigoDestino,
													m.Apelido as CodigoMercadoria,
													c.Numero as NroCte,
													c.Serie as SerieCte,
													c.Chave as ChaveCte,							
													c.DataHora as DataEmissao,
													c.PesoVagao as ToneladaUtil,
													d.DataCancelamento as DataCancDespacho,
													c.VolumeVagao as Volume	");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            query.SetParameter("situacaoCancelado", SituacaoCteEnum.Cancelado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoCancelado", SituacaoCteEnum.Cancelado, new SituacaoCteEnumCustomType());

            query.SetParameter("situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            queryCount.SetParameter("situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                // query.SetString("chaveCte", chaveCte);
                // queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(ManutencaoCteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<ManutencaoCteDto> items = query.List<ManutencaoCteDto>();
            return new ResultadoPaginado<ManutencaoCteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="codigoEstacaoOrigem">C�digo da Esta��o de Origem</param>
        /// <param name="numeroCte">N�mero do Cte</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="impresso">Status de impress�o Cte</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int despacho, string chaveCte, string codigoEstacaoOrigem, int numeroCte, string codFluxo, bool impresso)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(
                                            @"SELECT c FROM 
                    Cte c, 
                    ItemDespacho idesp,
                    DetalheCarregamento detcarr, 
                    Carregamento carr, 
                    FluxoComercial flux
                  WHERE
                      c.Despacho.id = idesp.DespachoTranslogic.id
                  AND idesp.DetalheCarregamento.id = detcarr.id
                  AND detcarr.Carregamento.id = carr.id
                  AND carr.FluxoComercialCarregamento.id = flux.id 
                  AND c.DataHora BETWEEN :dataInicial AND :dataFinal 
                  AND c.Impresso = :impresso ");

            if (serie != 0)
            {
                sb.Append("AND c.Despacho.SerieDespacho.SerieDespachoNum = :serie ");
            }

            if (despacho != 0)
            {
                sb.Append("AND c.Despacho.NumeroDespacho = :despacho ");
            }

            if (chaveCte != string.Empty)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (codigoEstacaoOrigem != string.Empty)
            {
                sb.Append("AND flux.Origem.Codigo = :codigoEstacaoOrigem ");
            }

            if (numeroCte != 0)
            {
                sb.Append("AND c.Numero = :numeroCte ");
            }

            if (codFluxo != string.Empty)
            {
                sb.Append("AND flux.Codigo = :codFluxo ");
            }

            ISession session = OpenSession();
            IQuery query = session.CreateQuery(sb.ToString());

            query.SetDateTime("dataInicial", dataInicial);

            query.SetDateTime("dataFinal", dataFinal);

            query.SetString("impresso", impresso ? "S" : "N");

            if (serie != 0)
            {
                query.SetString("serie", Convert.ToString(serie));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
            }

            if (chaveCte != string.Empty)
            {
                query.SetString("chaveCte", chaveCte);
            }

            if (codigoEstacaoOrigem != string.Empty)
            {
                query.SetString("codigoEstacaoOrigem", codigoEstacaoOrigem);
            }

            if (numeroCte != 0)
            {
                query.SetInt32("numeroCte", Convert.ToInt32(numeroCte));
            }

            if (codFluxo != string.Empty)
            {
                query.SetString("codFluxo", codFluxo);
            }

            query.SetResultTransformer(Transformers.DistinctRootEntity);

            return query.List<Cte>();
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="diasPrazoCancelamento">Dias para cancelamento</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="horasCancelamento">Horas prazo para cancelamento</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteCancelamento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string erro, string origem, string destino, string[] numVagao, int diasPrazoCancelamento, string codigoUfDcl, int horasCancelamento)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							LEFT JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti	
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							( 					
							    c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
                        AND c.SituacaoAtual = 'AUT'
                        AND c.TipoCte NOT IN ('SBT','ANL')  
                        AND c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (chaveCte == null)
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append(" AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (numVagao != null)
            {
                sb.Append(" AND v.Codigo IN (:vagao)");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append(" AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != null)
            {
                sb.Append(" AND c.Chave IN (:chaveCte) ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append(" AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append(" AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append(" AND c.SituacaoAtual = :erro ");
            }

            #region Order By

            var orderBy = String.Empty;
            if (pagination != null)
            {
                // Order by (Recupera o indice e o direcionamento (ASC/DESC))
                var colunasOrdenacao = pagination.ColunasOrdenacao;
                var direcoesOrdenacao = pagination.DirecoesOrdenacao;
                string colOrdenacaoIndex = "c.DataHora";
                string ordenacaoDirecao = "DESC";

                if (colunasOrdenacao.Count > 0)
                    colOrdenacaoIndex = colunasOrdenacao[0];
                if (direcoesOrdenacao.Count > 0)
                    ordenacaoDirecao = direcoesOrdenacao[0];

                if (colOrdenacaoIndex.ToLower() == "codigovagao")
                {
                    colOrdenacaoIndex = "v.Codigo";
                }
                else if (colOrdenacaoIndex.ToLower() == "fluxo")
                {
                    colOrdenacaoIndex = "fc.Codigo";
                }
                else if (colOrdenacaoIndex.ToLower() == "origem")
                {
                    colOrdenacaoIndex = "COALESCE(orii.Codigo, ori.Codigo)";
                }
                else if (colOrdenacaoIndex.ToLower() == "destino")
                {
                    colOrdenacaoIndex = "COALESCE(desti.Codigo, dest.Codigo)";
                }
                else if (colOrdenacaoIndex.ToLower() == "mercadoria")
                {
                    colOrdenacaoIndex = "m.Apelido";
                }
                else if (colOrdenacaoIndex.ToLower() == "cte")
                {
                    colOrdenacaoIndex = "c.Numero";
                }
                else if (colOrdenacaoIndex.ToLower() == "dateemissao")
                {
                    colOrdenacaoIndex = "c.DataHora";
                }
                else
                {
                    colOrdenacaoIndex = "c.DataHora";
                    ordenacaoDirecao = "DESC";
                }

                orderBy = String.Format(" ORDER BY {0} {1}", colOrdenacaoIndex, ordenacaoDirecao);
            }
            else
            {
                orderBy = " ORDER BY c.DataHora DESC";
            }

            #endregion  Order By

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                sb.ToString(),
                                                    @" c.Id as CteId,
													fc.Codigo as Fluxo,
                                                    fc.IndRateioCte as RateioCte,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,
                                                    sdu.CodigoControle as CodigoControle,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,
													(SELECT 'N' 
													   FROM CteAgrupamento ca, CteAgrupamento cap, Cte c1 
														WHERE ca.CteRaiz.Id = cap.CteFilho.Id
														  AND c1.Id = cap.CteFilho.Id
													      AND c1.SituacaoAtual = 'AUT'
														  AND ca.CteFilho.Id = c.Id) as CteAgrupamentoNaoAutorizado,	
													CASE WHEN (SELECT MAX(SYSDATE - S.DataHora)
														   FROM CteStatus S
														INNER JOIN S.CteStatusRetorno statusRet
														INNER JOIN S.Cte ctes
															WHERE ctes.Id = c.Id
															AND statusRet.Id = 100) > :QtdeDias Then 
														'S'
													Else 
														'N' 
													End as ForaDoTempoCancelado,	
													c.SituacaoAtual as SituacaoCte,																			
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino,
                                                    CASE WHEN (SELECT MAX((SYSDATE - (S1.DataHora + :HorasCancelamento /24)) * 24)
														   FROM CteStatus S1
														INNER JOIN S1.CteStatusRetorno statusRet1
														INNER JOIN S1.Cte ctes1
															WHERE ctes1.Id = c.Id
															AND statusRet1.Id = 100) > :HorasCancelamento Then 
														'S'
													Else 
														'N' 
													End as Acima168horas");

            stringHqlResult += orderBy;

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            // query.SetParameter("'situacaoAutorizado'", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            // queryCount.SetParameter("'situacaoAutorizado'", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            // query.ReturnTypes[0].

            if (chaveCte == null)
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            query.SetInt32("QtdeDias", diasPrazoCancelamento);

            query.SetInt32("HorasCancelamento", horasCancelamento);

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (numVagao != null)
            {
                query.SetParameterList("vagao", numVagao);
                queryCount.SetParameterList("vagao", numVagao);
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != null)
            {
                query.SetParameterList("chaveCte", chaveCte);
                queryCount.SetParameterList("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            if (chaveCte == null)
            {
                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }
            }

            IList<CteDto> items = query.List<CteDto>();
            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para Inutilizacao
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="diasPrazoInutilizacao">Dias para cancelamento</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteInutilizacao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, int diasPrazoInutilizacao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							LEFT JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti	
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
                        NOT EXISTS (SELECT 1 FROM CteStatus cs WHERE cs.Cte.Id = c.Id and cs.CteStatusRetorno.Id = 100)
                        AND c.TipoCte NOT IN ('SBT')  
                        AND c.DataHora <= :dataLimite
                        AND c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append(" AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append(" AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append(" AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append(" AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append(" AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append(" AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append(" AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append(" AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                sb.ToString(),
                                                    @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,
                                                    sdu.CodigoControle as CodigoControle,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,													
													c.SituacaoAtual as SituacaoCte,																			
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino ");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            query.SetString("dataLimite", DateTime.Now.AddDays(-diasPrazoInutilizacao).ToShortDateString());
            queryCount.SetString("dataLimite", DateTime.Now.AddDays(-diasPrazoInutilizacao).ToShortDateString());

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();
            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte Pagos
        /// </summary>
        /// <param name="listaCte">Lista de Cte pago</param>
        /// <returns>lista de Ctes pago</returns>
        public IList<CteDto> ObterCtesPago(List<int> listaCte)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								c.Id as CteId,
								fc.Codigo as Fluxo,
								COALESCE(orii.Codigo, ori.Codigo) as Origem,
								COALESCE(desti.Codigo, dest.Codigo) as Destino,
								m.Apelido as Mercadoria,
								v.Codigo as CodigoVagao,
								c.Numero as Cte,
								c.Chave as Chave,	
								COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
								COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
								sdu.NumeroSerieDespacho as SerieDesp6,
								d.NumeroDespachoUf as NumDesp6,
                                sdu.CodigoControle as CodigoControle,
								c.DataHora as DataEmissao,	
								case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
								case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
								c.Impresso as Impresso,
								c.SituacaoAtual as SituacaoCte,																			
								c.ArquivoPdfGerado as ArquivoPdfGerado ,
								( SELECT 'S' 
									FROM VSapZSDV0203V sap
									WHERE sap.ChaveCte = c.Chave  
									AND Tipo = 'DES' 
									AND ((sap.FaturaCompensada IS NOT NULL AND  sap.FaturaCompensada <> ' ' )
									OR (sap.Compensacao IS NOT NULL AND  sap.Compensacao <> ' ' ))) as Pago
							FROM Cte c 
							LEFT JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							LEFT JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim 
							LEFT JOIN dim.Estado diest
						WHERE c.Id IN (:listaCte) ");

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());

            query.SetParameterList("listaCte", listaCte);

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            return query.List<CteDto>();
        }

        /// <summary>
        /// Retorna a lista de Ctes que n�o constam na tabela Cte_Empresas
        /// </summary> 
        /// <returns>Retorna a lista dos ctes com a flag de Pago preenchida</returns>
        public IList<CteDto> RetornaCtesNaoConstamCteEmpresas()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"Select c.Id as CteId from Cte c
                        where not exists (select ct.Id from CteEmpresas ct
                        where c.Id = ct.Cte) and ambiente_sefaz = '1' and data_cte <= sysdate -7");

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            return query.List<CteDto>();
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo UF de Despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteComplemento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							INNER JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
                            LEFT JOIN c.ListaComplemento compl
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							( 					
							     c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) 
                        AND c.SituacaoAtual = 'AUT' 
                        AND c.TipoCte <> 'VTL' ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,
                                                    sdu.CodigoControle as CodigoControle,
                                                    c.TipoCte as TipoCte,
                                                    c.ValorCte as ValorCte,
                                                    c.PesoVagao as PesoVagao,
                                                    compl.ValorDiferenca as ValorDiferencaComplemento,
                                                    compl.PesoDiferenca as PesoDiferencaComplemento,
                                                    compl.DataHora as DataComplemento,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,													
													c.SituacaoAtual as SituacaoCte,																			
													c.ArquivoPdfGerado as ArquivoPdfGerado,

                                                    (
                                                       select 
                                                            user.Codigo
                                                        from 
                                                            CteStatus csu
                                                            join csu.Usuario user 
                                                        where 
                                                            csu.Cte.Id = c.Id
                                                            and csu.Id = (select min(csui.Id) from CteStatus csui where csui.Cte.Id = csu.Cte.Id)
                                                    ) as CodigoUsuario,

													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            // Codigo cometado - VSapZSDV0203V n�o funcionando em producao
            string x =
                    @"(SELECT 'S' 
													   FROM VSapZSDV0203V sap
													  WHERE sap.Bstkd = CONCAT( CONCAT(sd.SerieDespachoNum, '-'), CONCAT(lpad(d.NumeroDespacho, 5, '0'), '-2') ) 
														AND Tipo = 'DES' 
														AND sap.Compensacao <> '') as Pago,";

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            // query.SetParameter("'situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            // queryCount.SetParameter("'situacaoAutorizado'", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());

            // query.ReturnTypes[0].

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo UF de Despacho</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteComplementoIcms(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							INNER JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
                            LEFT JOIN c.ListaComplemento compl                    
                            LEFT JOIN compl.Usuario usr                    
							INNER JOIN fc.Origem ori
							INNER JOIN fc.Contrato ct
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							( 					
							     c.Id IN
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id IN 
								    ( 
                                       select
                                            aInterna02.Id
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte = 'VTL'  								                                 
								    ) 
							    )
                                OR
                                c.Id = 
							    ( 
							        SELECT CA2.CteFilho.Id 
							        FROM CteArvore CA2 
							        WHERE CA2.Id = 
								    ( 
                                        select
                                            max(aInterna02.Id)
                                        from 
                                            CteArvore aInterna01, 
                                            Cte cInterno01,
                                            CteArvore aInterna02,
                                            Cte cInterno02
                                        where 
                                            cInterno01.Id = c.Id

                                            and cInterno01.Id = aInterna01.CteFilho.Id
                                            and aInterna02.CteRaiz.Id = aInterna01.CteRaiz.Id
                                            and aInterna02.CteFilho.Id = cInterno02.Id
                                            and cInterno02.TipoCte <> 'VTL'  
								    ) 
							    )
                            )
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
                        AND  c.AmbienteSefaz IN (SELECT 
                                                    CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
                                                  FROM 
                                                     CteSerieNumeroEmpresaUf CSUF 
                                                 WHERE CSUF.Uf = c.SiglaUfFerrovia ) 
                        AND c.SituacaoAtual = 'AUT'                         
                        AND c.TipoCte <> 'VTL' 
                        AND ct.PercentualAliquotaIcms > c.PercentualAliquotaIcms
                        ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                    @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.Apelido as Mercadoria,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,
                                                    sdu.CodigoControle as CodigoControle,
                                                    c.TipoCte as TipoCte,
                                                    c.ValorCte as ValorCte,
                                                    c.PesoVagao as PesoVagao,
                                                    round(((c.ValorCte - c.ValorIcms) * round(100/(100-ct.PercentualAliquotaIcms),6)),2) - c.ValorCte as ValorDiferencaComplemento,
                                                    compl.DataHora as DataComplemento,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,													
													c.SituacaoAtual as SituacaoCte,																			
													c.ArquivoPdfGerado as ArquivoPdfGerado,
                                                    usr.Nome as CodigoUsuario,
                                                    c.PercentualAliquotaIcms as AliquotaIcmsCte,
                                                    ct.PercentualAliquotaIcms as AliquotaIcmsContratoAtual,
                                                    ct.IndIeToma as IndIeToma, 
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            ////long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = items.Count(), Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">C�digo DCL</param>
        /// <returns>lista de Cte</returns>
        public ResultadoPaginado<CteDto> ObterCteTakeOrPay(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(@"  SELECT 
								{0}
							FROM Cte c 
							INNER JOIN  c.Despacho d
							LEFT JOIN d.SerieDespachoUf sdu
							LEFT JOIN d.SerieDespacho sd
							INNER JOIN c.Vagao v
							INNER JOIN c.FluxoComercial fc
							INNER JOIN fc.Mercadoria m
							LEFT JOIN c.Complemento compl
							INNER JOIN fc.Origem ori
							INNER JOIN ori.EmpresaConcessionaria oe
							INNER JOIN ori.Municipio om
							INNER JOIN om.Estado oest
							LEFT JOIN fc.OrigemIntercambio orii
							LEFT JOIN orii.EmpresaConcessionaria oie
							LEFT JOIN orii.Municipio oim
							LEFT JOIN oim.Estado oiest
							INNER JOIN fc.Destino dest
							INNER JOIN dest.EmpresaConcessionaria de
							INNER JOIN fc.EmpresaPagadora empPag
							INNER JOIN dest.Municipio dm
							INNER JOIN dm.Estado deest
							LEFT JOIN fc.DestinoIntercambio desti
							LEFT JOIN desti.EmpresaConcessionaria die
							LEFT JOIN desti.Municipio dim
							LEFT JOIN dim.Estado diest
						WHERE 					
							c.Id = 
							( 
								SELECT CA2.CteFilho.Id 
								  FROM CteArvore CA2 
								 WHERE CA2.Id = 
									( 
										SELECT MAX(CAR.Id) FROM CteArvore CAR, 
										CteArvore CAF
										WHERE 
										CAR.CteRaiz.Id = CAF.CteRaiz.Id 
										AND CAF.CteFilho.Id = c.Id
									) 
							)
						AND EXISTS (SELECT 1 FROM CteAgrupamento CA WHERE  c.Id = CA.CteFilho.Id AND CA.Sequencia=1) 
						AND  c.AmbienteSefaz IN (SELECT 
						CASE WHEN CSUF.ProducaoSefaz = 'S' THEN 1 ELSE 2 END
						FROM CteSerieNumeroEmpresaUf CSUF 
						WHERE CSUF.Uf = c.SiglaUfFerrovia ) 
						AND c.SituacaoAtual = 'AUT' ");

            if (string.IsNullOrEmpty(chaveCte))
            {
                sb.Append(" AND c.DataHora BETWEEN :dataInicial AND :dataFinal  ");
            }

            if (!string.IsNullOrEmpty(serie))
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append("AND COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) = :serie ");
                }
                else
                {
                    sb.Append("AND TO_CHAR(sdu.NumeroSerieDespacho) = :serie ");
                }
            }

            // if (!string.IsNullOrEmpty(serie))
            // {
            //    sb.Append("AND (case when d.NumeroDespachoUf is null  then COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) else TO_CHAR(sdu.NumeroSerieDespacho) end) = :serie ");
            // }

            if (!string.IsNullOrEmpty(numVagao))
            {
                sb.Append("AND v.Codigo = :vagao ");
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                sb.Append("AND sdu.CodigoControle = :codigoControle ");
            }

            if (despacho != 0)
            {
                if (string.IsNullOrEmpty(codigoUfDcl))
                {
                    sb.Append(" AND COALESCE(d.NumDespIntercambio, d.NumeroDespacho) = :despacho ");
                }
                else
                {
                    sb.Append(" AND  d.NumeroDespachoUf = :despacho ");
                }
            }

            // if (despacho != 0)
            // {
            //    sb.Append(" AND (case when d.NumeroDespachoUf is null  then COALESCE(d.NumDespIntercambio, d.NumeroDespacho) else d.NumeroDespachoUf end) = :despacho ");
            // }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sb.Append("AND fc.Codigo LIKE :codFluxo ");
            }

            if (!string.IsNullOrEmpty(chaveCte))
            {
                sb.Append("AND c.Chave = :chaveCte ");
            }

            if (origem != string.Empty & origem != null)
            {
                sb.Append("AND (case when orii is null then ori.Codigo else orii.Codigo end) = :codigoOrigem ");
            }

            if (destino != string.Empty & destino != null)
            {
                sb.Append("AND (case when desti is null then dest.Codigo else desti.Codigo end) = :codigoDestino ");
            }

            if (erro != string.Empty & erro != null)
            {
                sb.Append("AND c.SituacaoAtual = :erro ");
            }

            sb.Append(" ORDER BY c.DataHora DESC");

            ISession session = OpenSession();

            string stringHqlResult = string.Format(
                                                                                            sb.ToString(),
                                                                                            @" c.Id as CteId,
													fc.Codigo as Fluxo,
													COALESCE(orii.Codigo, ori.Codigo) as Origem,
													COALESCE(desti.Codigo, dest.Codigo) as Destino,
													m.DescricaoResumida as Mercadoria,
                                                    empPag.NomeFantasia as ClienteFatura,
													v.Codigo as CodigoVagao,
													c.Numero as Cte,
													c.Chave as Chave,	
													COALESCE(d.SerieDespachoSdi, sd.SerieDespachoNum) as SerieDesp5,																	
													COALESCE(d.NumDespIntercambio, d.NumeroDespacho) as NumDesp5,
													sdu.NumeroSerieDespacho as SerieDesp6,
													d.NumeroDespachoUf as NumDesp6,
                                                    sdu.CodigoControle as CodigoControle,
                                                    c.ValorCte as ValorCte,
                                                    c.TipoCte as TipoCte,
                                                    c.PesoVagao as PesoVagao,
                                                    compl.ValorDiferenca as ValorDiferencaComplemento,
                                                    compl.DataHora as DataComplemento,
													c.DataHora as DataEmissao,	
													case when orii is null then oe.DescricaoResumida else oie.DescricaoResumida end as FerroviaOrigem,
													case when desti is null then de.DescricaoResumida else die.DescricaoResumida end as FerroviaDestino,
													c.Impresso as Impresso,													
													c.SituacaoAtual as SituacaoCte,																			
													c.ArquivoPdfGerado as ArquivoPdfGerado,
													case when orii is null then oest.Sigla else oiest.Sigla end as UfOrigem,
													case when desti is null then deest.Sigla else diest.Sigla end as UfDestino");

            // Codigo cometado - VSapZSDV0203V n�o funcionando em producao
            string x =
                    @"(SELECT 'S' 
													   FROM VSapZSDV0203V sap
													  WHERE sap.Bstkd = CONCAT( CONCAT(sd.SerieDespachoNum, '-'), CONCAT(lpad(d.NumeroDespacho, 5, '0'), '-2') ) 
														AND Tipo = 'DES' 
														AND sap.Compensacao <> '') as Pago,";

            string stringHqlCount = string.Format(sb.ToString(), " COUNT(c.Id) ");

            IQuery query = session.CreateQuery(stringHqlResult);
            IQuery queryCount = session.CreateQuery(stringHqlCount);

            // query.SetParameter("'situacaoAutorizado", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());
            // queryCount.SetParameter("'situacaoAutorizado'", SituacaoCteEnum.Autorizado, new SituacaoCteEnumCustomType());

            // query.ReturnTypes[0].

            if (string.IsNullOrEmpty(chaveCte))
            {
                query.SetDateTime("dataInicial", dataInicial);
                queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (!string.IsNullOrEmpty(serie))
            {
                query.SetString("serie", serie);
                queryCount.SetString("serie", serie);
            }

            if (!string.IsNullOrEmpty(numVagao))
            {
                query.SetString("vagao", Convert.ToString(numVagao));
                queryCount.SetString("vagao", Convert.ToString(numVagao));
            }

            if (!string.IsNullOrEmpty(codigoUfDcl))
            {
                query.SetString("codigoControle", Convert.ToString(codigoUfDcl));
                queryCount.SetString("codigoControle", Convert.ToString(codigoUfDcl));
            }

            if (despacho != 0)
            {
                query.SetInt32("despacho", despacho);
                queryCount.SetInt32("despacho", despacho);
            }

            if (erro != string.Empty & erro != null)
            {
                query.SetString("erro", erro);
                queryCount.SetString("erro", erro);
            }

            if (chaveCte != string.Empty & chaveCte != null)
            {
                query.SetString("chaveCte", chaveCte);
                queryCount.SetString("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }

            if (origem != string.Empty & origem != null)
            {
                query.SetString("codigoOrigem", origem);
                queryCount.SetString("codigoOrigem", origem);
            }

            if (destino != string.Empty & destino != null)
            {
                query.SetString("codigoDestino", destino);
                queryCount.SetString("codigoDestino", destino);
            }

            long total = queryCount.UniqueResult<long>();

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0)
            {
                query.SetMaxResults(limit);
            }

            IList<CteDto> items = query.List<CteDto>();

            return new ResultadoPaginado<CteDto> { Total = total, Items = items };
        }

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="situacao">Situacao do Cte</param>
        /// <param name="codVagao">C�digo do Vag�o</param>
        /// <param name="chave">Chave do CTE</param>
        /// <param name="serieCte">S�rie do CTE</param>
        /// <param name="nroCte">N�mero do CTE</param>
        /// <returns>lista de Cte</returns>
        public IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, SituacaoCteEnum situacao, string codVagao, string chave, int serieCte, int nroCte)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retorna Cte pelo Id
        /// </summary>
        /// <param name="idCte">Id do Cte selecionado</param>
        /// <returns>Cte Selecionado</returns>
        public Cte ObterPorIdCte(int idCte)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("Despacho", "desp", JoinType.InnerJoin);
            criteria.CreateAlias("desp.SerieDespacho", "serdesp", JoinType.LeftOuterJoin);
            criteria.CreateAlias("desp.SerieDespachoUf", "serdespuf", JoinType.LeftOuterJoin);
            criteria.CreateAlias("Vagao", "vag", JoinType.LeftOuterJoin);

            if (idCte != 0)
            {
                criteria.Add(Restrictions.Eq("Id", idCte));
            }

            criteria.SetFetchMode("desp", FetchMode.Eager);
            criteria.SetFetchMode("serdesp", FetchMode.Eager);
            criteria.SetFetchMode("serdespuf", FetchMode.Eager);
            criteria.SetFetchMode("vag", FetchMode.Eager);

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Retorna Cte pelo Id com fetch para o EDI acessar despacho, fluxo, origem e destino
        /// </summary>
        /// <param name="idCte">Id do Cte selecionado</param>
        /// <returns>Cte Selecionado</returns>
        public Cte ObterPorIdEdi(int idCte)
        {
            DetachedCriteria criteria = CriarCriteria();

            criteria.CreateAlias("Despacho", "desp", JoinType.InnerJoin);
            criteria.CreateAlias("Vagao", "vag", JoinType.InnerJoin);
            criteria.CreateAlias("FluxoComercial", "fx", JoinType.InnerJoin);
            criteria.CreateAlias("fx.Destino", "dest", JoinType.InnerJoin);
            criteria.CreateAlias("fx.Origem", "orig", JoinType.InnerJoin);

            criteria.Add(Restrictions.Eq("Id", idCte));

            criteria.SetFetchMode("desp", FetchMode.Eager);
            criteria.SetFetchMode("vag", FetchMode.Eager);
            criteria.SetFetchMode("fx", FetchMode.Eager);
            criteria.SetFetchMode("dest", FetchMode.Eager);
            criteria.SetFetchMode("orig", FetchMode.Eager);

            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m os CTe's que est�o com status de processamento
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual o ambiente Sefaz</param>
        /// <returns>Lista de CTe's</returns>
        public IList<Cte> ObterStatusProcessamentoEnvio(int indAmbienteSefaz)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = string.Format(
                                   @"FROM 
                                      Cte c
								   WHERE 
                                      c.Id NOT IN (SELECT cep.Id FROM CteEnvioPooling cep WHERE c.Id = cep.Id)  
								   AND 
                                      c.SituacaoAtual IN ('PAE', 'AGC', 'PCE', 'PAN', 'PAR', 'AGI')  
                                   AND 
                                      c.AmbienteSefaz = {0}
								   ORDER BY 
                                      c.Id", indAmbienteSefaz);

                    IQuery query = session.CreateQuery(hql);
                    // query.SetDateTime("dataInicial", );

                    query.SetMaxResults(50);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<Cte>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obt�m a lista de Ctes que est�o no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <returns>Lista de CT-e</returns>
        public IList<Cte> ObterListaEnvioPorHost(string hostName)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									INNER JOIN FETCH c.Despacho d
									INNER JOIN FETCH c.FluxoComercial f
									INNER JOIN FETCH c.Vagao v
									INNER JOIN FETCH v.EmpresaProprietaria
									INNER JOIN FETCH v.Serie s
									LEFT OUTER JOIN FETCH c.ListaDetalhes ld
									WHERE 
									EXISTS(SELECT cep.Id FROM CteEnvioPooling cep WHERE c.Id = cep.Id AND cep.HostName = :host)
                                    AND	ROWNUM < 51
									AND c.SituacaoAtual IN ('EFA', 'EFC', 'EFI', 'EFN', 'EFR', 'EFT', 'ECC')
									";

                    IQuery query = session.CreateQuery(hql);
                    query.SetString("host", hostName);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<Cte>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obt�m a lista de Ctes complementado que est�o no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <returns>Lista de CT-e</returns>
        public IList<Cte> ObterListaEnvioComplementarPorHost(string hostName)
        {
            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									INNER JOIN FETCH c.FluxoComercial f
									WHERE 
									EXISTS(SELECT cep.Id FROM CteEnvioPooling cep WHERE c.Id = cep.Id AND cep.HostName = :host)
									AND c.SituacaoAtual IN (:situacaoComplemento, :situacaoCancelamento, :situacaoInutilizacao, :situacaoPendenteCorrecao)";

                    IQuery query = session.CreateQuery(hql);
                    query.SetString("host", hostName);
                    // query.SetParameter("situacaoEnviado", SituacaoCteEnum.EnviadoFilaArquivoEnvio, new SituacaoCteEnumCustomType());
                    query.SetParameter("situacaoCancelamento", SituacaoCteEnum.EnviadoFilaArquivoCancelamento, new SituacaoCteEnumCustomType());
                    // query.SetParameter("situacaoInutilizacao", SituacaoCteEnum.EnviadoFilaArquivoInutilizacao, new SituacaoCteEnumCustomType());
                    query.SetParameter("situacaoComplemento", SituacaoCteEnum.EnviadoFilaArquivoComplemento, new SituacaoCteEnumCustomType());
                    query.SetParameter("situacaoInutilizacao", SituacaoCteEnum.EnviadoFilaArquivoInutilizacao, new SituacaoCteEnumCustomType());
                    query.SetParameter("situacaoPendenteCorrecao", SituacaoCteEnum.EnviadoFilaArquivoCorrecao, new SituacaoCteEnumCustomType());

                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<Cte>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Obt�m os CTe's que est�o com o status de processamento para recebimento 
        /// </summary>
        /// <returns>Lista de Ctes</returns>
        public IList<Cte> ObterStatusProcessamentoRecebimento()
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM Cte c
                                WHERE c.SituacaoAtual IN (:situacaoEnviado, :situacaoCancelamento, :situacaoInutilizacao, :situacaoComplemento)
                                AND NOT EXISTS(SELECT crp.Id FROM CteRecebimentoPooling crp WHERE c.Id = crp.Id)
                                AND EXISTS(SELECT caf.Id FROM CteArquivoFtp caf  WHERE c.Id = caf.Cte.Id)
                               ORDER BY c.Id";
                IQuery query = session.CreateQuery(hql);
                query.SetParameter("situacaoEnviado", SituacaoCteEnum.EnviadoArquivoEnvio, new SituacaoCteEnumCustomType());
                query.SetParameter("situacaoCancelamento", SituacaoCteEnum.EnviadoArquivoCancelamento, new SituacaoCteEnumCustomType());
                query.SetParameter("situacaoInutilizacao", SituacaoCteEnum.EnviadoArquivoInutiliza��o, new SituacaoCteEnumCustomType());
                query.SetParameter("situacaoComplemento", SituacaoCteEnum.EnviadoArquivoComplemento, new SituacaoCteEnumCustomType());

                query.SetMaxResults(10);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<Cte>();
            }
        }

        /// <summary>
        /// Chama a package para Gerar o Cte Complementar
        /// </summary>
        /// <param name="usuario"> Usu�rio Logado </param>
        /// <param name="idCteComplementar"> Id do Cte Complementar retornado pela Package </param>
        public void GerarCteComplementar(Usuario usuario, out int idCteComplementar)
        {
            using (ISession session = OpenSession())
            {
                idCteComplementar = 0;

                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_CTE.GERAR_CTE_COMPLEMENTAR",
                    Connection = (OracleConnection)session.Connection
                };
                AddParameter(command, "PIDUSUARIO", OracleType.Number, usuario.Id, ParameterDirection.Input);
                AddParameter(command, "PIDCTECOMPLEMENTAR", OracleType.Number, idCteComplementar, ParameterDirection.Output);

                session.Transaction.Enlist(command);

                command.Prepare();
                command.ExecuteNonQuery();

                idCteComplementar = int.Parse(command.Parameters["PIDCTECOMPLEMENTAR"].Value.ToString());
            }
        }

        /// <summary>
        /// Chama a package para Gerar o Cte Complementar
        /// </summary>
        /// <param name="usuario"> Usu�rio Logado </param>
        /// <param name="idCteAnulacao"> Id do Cte Complementar retornado pela Package </param>
        public void GerarCteAnulacao(Usuario usuario, out int idCteAnulacao)
        {
            using (ISession session = OpenSession())
            {
                idCteAnulacao = 0;

                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_CTE.GERAR_CTE_ANULACAO",
                    Connection = (OracleConnection)session.Connection
                };
                AddParameter(command, "PIDUSUARIO", OracleType.Number, usuario.Id, ParameterDirection.Input);
                AddParameter(command, "PIDCTEANULACAO", OracleType.Number, idCteAnulacao, ParameterDirection.Output);

                session.Transaction.Enlist(command);
                command.Prepare();
                command.ExecuteNonQuery();

                idCteAnulacao = int.Parse(command.Parameters["PIDCTEANULACAO"].Value.ToString());
            }

        }

        /// <summary>
        /// Chama a package para verificar a gera��o de cte.
        /// </summary>
        /// <param name="fluxo"> C�digo do fluxo</param>
        /// <returns>Retorna se deve gerar cte.</returns>
        public bool VerificarFLuxoGerarCte(string fluxo)
        {
            using (ISession session = OpenSession())
            {
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "PKG_DESPACHO.VERIFICAR_FLUXO_GERAR_CTE",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "PCODFLUXO", OracleType.Number, fluxo, ParameterDirection.Input);

                session.Transaction.Enlist(command);

                command.Prepare();
                string retorno = command.ExecuteScalar() as string;

                if (string.IsNullOrEmpty(retorno))
                {
                    throw new Exception("Erro ao verificar gera��o de CT-e para o fluxo");
                }

                return retorno.Equals("S");
            }
        }

        /// <summary>
        /// Atualiza o valor do CTE
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        /// <param name="valorCte">Valor do cte para ser atualizado</param>
        public void AtualizarValorCte(Cte cte, double valorCte)
        {
            string hql = @"UPDATE Cte SET ValorCte = :valorCte WHERE Id = :idCte";
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);
                query.SetDouble("valorCte", valorCte);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Atualiza os Flags de arquivo gerado do CTE de acordo com o tipo de arquivo
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        /// <param name="tipoArquivoEnvioEnum">Tipo de arquivo que ser� atualizado o flag (PDF ou XML)</param>
        public void AtualizarArquivoGeradoCte(Cte cte, TipoArquivoEnvioEnum tipoArquivoEnvioEnum)
        {
            string hql = string.Empty;
            if (tipoArquivoEnvioEnum == TipoArquivoEnvioEnum.Pdf)
            {
                hql = @"UPDATE Cte SET ArquivoPdfGerado = 'S' WHERE Id = :idCte";
            }
            else
            {
                hql = @"UPDATE Cte SET ArquivoXmlGerado = 'S' WHERE Id = :idCte";
            }

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <param name="idCte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        public Cte ObterCtePorIdHql(int idCte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									 LEFT JOIN FETCH c.Despacho d
									INNER JOIN FETCH c.FluxoComercial f
									LEFT JOIN FETCH c.Vagao v
									LEFT JOIN FETCH v.EmpresaProprietaria ep
									LEFT JOIN FETCH v.Serie
                  WHERE c.Id = :idCte 
									";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", idCte);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<Cte> lista = query.List<Cte>();
                return lista.FirstOrDefault();
            }
        }

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <param name="cte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        public Cte ObterCtePorHql(Cte cte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									 LEFT JOIN FETCH c.Despacho d
									INNER JOIN FETCH c.FluxoComercial f
									LEFT JOIN FETCH c.Vagao v
									LEFT JOIN FETCH v.EmpresaProprietaria ep
									LEFT JOIN FETCH v.Serie
                  WHERE c.Id = :idCte 
									";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<Cte> lista = query.List<Cte>();
                return lista.FirstOrDefault();
            }
        }

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <param name="cte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        public Cte ObterCteComplementarPorHql(Cte cte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM Cte c
									INNER JOIN FETCH c.Versao v
									INNER JOIN FETCH c.FluxoComercial f
                  WHERE c.Id = :idCte 
									";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                IList<Cte> lista = query.List<Cte>();
                return lista[0];
            }
        }

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <returns>Retorna o CTE</returns>
        public IList<ImportacaoEmailDto> ObterCteEnvioXml()
        {
            using (ISession session = OpenSession())
            {
                string hql = @"SELECT C as Cte, FC.EmpresaPagadora.EmailXml as EmailXml, FC.EmpresaPagadora.EmailNfe as EmailNfe
                                 FROM Cte C 
                           INNER JOIN C.FluxoComercial FC
                           WHERE TRUNC(C.DataHora) = '16/04/2012'
                             AND C.AmbienteSefaz = 1
                             AND C.Serie = 2
                             AND NOT (EXISTS(SELECT 1 FROM CteEnvioEmailHistorico hist WHERE hist.Cte.Id = C.Id))";

                string sql = @" select cte0_.id_cte, ep.email_nfe, ep.email_xml
                                  from CTE cte0_
                                  JOIN FLUXO_COMERCIAL FC ON cte0_.fx_id_flx = fc.fx_id_flx
                                  JOIN EMPRESA EP ON EP.EP_ID_EMP = FC.EP_ID_EMP_COR
                                 where trunc(cte0_.DATA_CTE) = '16/04/2012'
                                   and cte0_.AMBIENTE_SEFAZ = 1
                                   and not (exists (select 1
                                               from cte_envio_email_hist cteenvioxm1_
                                              where cteenvioxm1_.ID_CTE = cte0_.ID_CTE))
                                 order by cte0_.data_cte";

                /*     string sql = @"SELECT to_number(c.Id_cte)
                                         FROM CTE C  WHERE C.AMBIENTE_SEFAZ = 1  
                                         AND TRUNC(C.DATA_CTE) = '15/04/2012'   
                                         AND NOT EXISTS(SELECT 1 FROM CTE_ENVIO_EMAIL_HIST X WHERE X.ID_CTE = C.ID_CTE)";
                     */
                IQuery query = session.CreateQuery(hql);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(ImportacaoEmailDto)));

                return query.List<ImportacaoEmailDto>();
            }
        }

        /// <summary>
        /// Obt�m o Cte pela chave
        /// </summary>
        /// <param name="chave">Chave do Cte</param>
        /// <returns>Objeto cte</returns>
        public Cte ObterPorChave(string chave)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Chave", chave));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Atualiza a situacao do Cte por Stateless
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        public void AtualizarSituacao(Cte cte)
        {
            string hql = string.Empty;
            hql = @"UPDATE Cte SET SituacaoAtual = :situacaoAtual WHERE Id = :idCte";

            using (IStatelessSession session = SessionFactory.OpenStatelessSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id.Value);
                query.SetParameter("situacaoAtual", cte.SituacaoAtual, new SituacaoCteEnumCustomType());
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Atualiza a situacao do Cte por Stateless
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        public void AtualizarMotivoCancelamento(Cte cte)
        {
            string hql = string.Empty;
            hql = @"UPDATE Cte SET CteMotivoCancelamento = :cteMotivoCancelamento WHERE Id = :idCte";

            using (IStatelessSession session = SessionFactory.OpenStatelessSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idCte", cte.Id ?? 0);
                query.SetParameter("cteMotivoCancelamento", cte.CteMotivoCancelamento.Id);
                query.ExecuteUpdate();
            }
        }

        /// <summary>
        /// Atualiza a situacao do Cte por Stateless
        /// </summary>
        /// <param name="idCte">Cte para ser atualizado</param>
        /// <param name="situacao">Situa��o Cte</param>
        /// <param name="dataAnulacao">Data anula��o do Cte</param>
        public void AtualizarSituacaoAnulacao(string idCte, string situacao, DateTime dataAnulacao)
        {
            var hql = @"UPDATE Cte SET SituacaoAtual = '" + situacao + "',  WHERE Id = " + idCte;
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql);
                query.ExecuteUpdate();
                session.Flush();
            }
        }

        /// <summary>
        /// Obter o CT-e pelo despacho do Translogic
        /// </summary>
        /// <param name="despachoTranslogic">Despacho do translogic</param>
        /// <param name="idVagao : VG_ID_VG para buscar o CTE correto para aquele vagao">Despacho do translogic</param>
        /// <param name="idCte : esse campo somente tem valor qdo ws_despacho.id_cte � n�o nulo. isto vale para ctes virtuais">Id do Cte</param>
        /// <returns>Retorna a inst�ncia do CT-e</returns>
        public Cte ObterPorDespacho(DespachoTranslogic despachoTranslogic, int? idVagao = null, int? idCte = null)
        {
            if (idCte != null)
            {
                return ObterPorId(idCte);
            }

            IList<Cte> lista;
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder sb = new StringBuilder();

            sb.Append(
                @"FROM Cte c
					INNER JOIN FETCH c.Despacho d
					WHERE d.Id = :idDespachoTranslogic
			");

            if (idVagao.GetValueOrDefault() > 0)
            {
                sb.Append(" AND c.Vagao.Id = " + idVagao);
            }

            parametros.Add(q => q.SetInt32("idDespachoTranslogic", (int)despachoTranslogic.Id));

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(sb.ToString());
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                foreach (Action<IQuery> action in parametros)
                {
                    action.Invoke(query);
                }

                query.SetMaxResults(1);
                return query.List<Cte>().FirstOrDefault();
            }
        }

        /// <summary>
        /// Verifica se existe o status de retorno informado para o cte
        /// </summary>
        /// <param name="cteId">Identificador do Cte</param>
        /// <param name="codigoStatusRetorno">C�digo de Status do Retorno</param>
        /// <returns>Retorna a lista de Ctes</returns>
        public bool ExisteStatusRetornoCte(int cteId, int codigoStatusRetorno)
        {
            string sql = @"SELECT
                            COUNT(CS.CODIGO_STATUS_RETORNO)
                            FROM CTE C,
                            CTE_STATUS CS,
                            CTE_STATUS_RETORNO CSR
                            WHERE C.ID_CTE = CS.ID_CTE
                            AND CS.CODIGO_STATUS_RETORNO = CSR.CODIGO_STATUS_RETORNO
                            AND CSR.CODIGO_STATUS_RETORNO = :codigoStatusRetorno
                            AND C.ID_CTE = :cteId";
            using (ISession session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sql);
                query.SetInt32("codigoStatusRetorno", codigoStatusRetorno);
                query.SetInt32("cteId", cteId);
                query.SetReadOnly(true);
                var resultado = query.UniqueResult<decimal>();
                return resultado > 0;
            }
        }

        /// <summary>
        /// Obter os Refaturamentos
        /// </summary>
        /// <param name="ordemServico"> Ordem de Servi�o</param>
        /// <param name="prefixoTrem"> Prefixo do Trem</param>
        /// <returns>Retorna a inst�ncia do RefaturamentoDto</returns>
        public IList<RefaturamentoDto> ObterParaRefaturamento(string ordemServico, string prefixoTrem)
        {
            StringBuilder builder = new StringBuilder();
            using (var session = OpenSession())
            {
                builder.AppendLine(@" SELECT distinct 
										cv.CV_SEQ_CMP AS ""SequenciaVagao""										
										,T.TR_ID_TRM AS ""IdTrem""
										,C.CP_ID_CPS AS ""IdComposicao""
										,V.VG_ID_VG AS ""IdVagao""
										,sv.SV_COD_SER AS ""SerieVagao""
										,SUBSTR(VG_COD_VAG,1,6)||'-'||SUBSTR (VG_COD_VAG,7,1) AS ""Veiculo""
										,prp.EP_SIG_EMP AS ""EmpresaProprietaria""
										,(SELECT fv.FV_COD_FRT FROM VAGAO_FROTA vf, FROTA_VAGAO fv WHERE v.VG_ID_VG = vf.VG_ID_VG AND vf.FV_ID_FT_AVG = fv.FV_ID_FT AND fv.FV_IND_RST = 'S' AND ROWNUM = 1) AS ""Frota""
										,org.AO_COD_AOP AS ""Origem""
										,dst.AO_COD_AOP AS ""Destino""
										,(SELECT m.MC_COD_MRC FROM MERCADORIA m WHERE m.MC_ID_MRC=NVL(pt.MC_ID_MRC,px.MC_ID_MRC)) AS ""Mercadoria""
										,(SELECT rmt.EP_SIG_EMP FROM EMPRESA rmt WHERE rmt.EP_ID_EMP=NVL(pt.EP_ID_EMP_RMT,px.EP_ID_EMP_RMT)) AS ""EmpresaRemetente"" 
										,(SELECT csg.EP_SIG_EMP FROM EMPRESA csg WHERE csg.EP_ID_EMP=NVL(pt.EP_ID_EMP_CSG,px.EP_ID_EMP_CSG)) AS ""EmpresaCsg"" 
										,pt.PT_COD_PDT AS ""Pedido"" 
										,(SELECT ROUND(SUM(NVL(DC_QTD_CAR,DC_QTD_INF)),2) FROM DETALHE_CARREGAMENTO dc WHERE dc.VG_ID_VG = cv.VG_ID_VG AND dc.CG_ID_CAR=vb.CG_ID_CAR) AS ""Tu""
										,NVL((SELECT CARGA_BRUTA FROM V_PESO_VAGAO vp WHERE vp.VG_ID_VG = cv.VG_ID_VG),NVL(v.VG_NUM_TRA,fe.FC_TAR)) AS ""CargaBruta"" 
										,cd.CD_COD_CDU AS ""CondUso""
										,v.VG_IND_FRO AS ""IndFreio""
										,(SELECT m.MC_DRS_PT FROM MERCADORIA m WHERE m.MC_ID_MRC = NVL(pt.MC_ID_MRC, px.MC_ID_MRC)) AS ""MercDescricao""
										,(SELECT csg.EP_DSC_RSM FROM EMPRESA csg WHERE csg.EP_ID_EMP = NVL(pt.EP_ID_EMP_CSG, px.EP_ID_EMP_CSG)) AS ""EmpresaCsgDesc""
										,FE.FC_CMP AS ""Comprimento""
										,null AS ""Nota""
										,v.TB_COD_BITOLA AS ""Bitola""
										,clasv.cs_dsc_pt as ""DescClasse""
										,(
											SELECT DP.DP_ID_DP
											FROM CARREGAMENTO CG,DETALHE_CARREGAMENTO DC, ITEM_DESPACHO IDS,DESPACHO DP
											WHERE CG.CG_ID_CAR = DC.CG_ID_CAR
											AND   IDS.DC_ID_CRG = DC.DC_ID_CRG
											AND   IDS.VG_ID_VG = VB.VG_ID_VG
											AND   CG.PT_ID_ORT = VB.PT_ID_ORT
											AND   IDS.DP_ID_DP = DP.DP_ID_DP
										) AS ""IdDespacho"",
										FC.FX_COD_FLX AS ""CodFluxoComercial"",
										FC.FX_ID_FLX AS ""IdFluxoComercial""
										FROM 
											FOLHA_ESPECIF_VAGAO fe 
										,SERIE_VAGAO sv 
										,EMPRESA prp 
										,CONDICAO_USO cd 
										,CONDUSO_VAGAO_VIG ci 
										,AREA_OPERACIONAL org 
										,AREA_OPERACIONAL dst 
										,VAGAO v 
										,PEDIDO_DERIVADO px 
										,PEDIDO_TRANSPORTE pt 
										,VAGAO_PEDIDO_VIG vb 
										,COMPVAGAO_VIG cv 
										,COMPOSICAO c 
										,TIPO_VAGAO tv 
										,CLASSE_VAGAO clasv 
										,FLUXO_COMERCIAL FC
 										,TREM T 
										,T2_OS OS
										WHERE 
												sv.SV_ID_SV=v.SV_ID_SV 
											AND fe.FC_ID_FC=v.FC_ID_FC 
											AND cd.CD_IDT_CUS=ci.CD_IDT_CUS 
											AND ci.VG_ID_VG=v.VG_ID_VG 
											AND prp.EP_ID_EMP=v.EP_ID_EMP_PRP 
											AND v.VG_ID_VG=cv.VG_ID_VG 
											AND dst.AO_ID_AO=pt.AO_ID_AO_DST 
											AND org.AO_ID_AO=pt.AO_ID_AO_ORG 
											AND px.PX_IDT_PDR(+)=vb.PX_IDT_PDR 
											AND pt.PT_ID_ORT=vb.PT_ID_ORT 
											AND vb.VG_ID_VG=cv.VG_ID_VG 
											AND cv.CP_ID_CPS=c.CP_ID_CPS 
											and sv.TV_IDT_TVG = tv.TV_IDT_TVG 
											and tv.CS_IDT_CVG = clasv.CS_IDT_CVG																													 
											AND FC.FX_ID_FLX = VB.FX_ID_FLX 
											AND c.TR_ID_TRM = T.TR_ID_TRM
											AND T.OF_ID_OSV = OS.X1_ID_OS
                                            AND NOT EXISTS (SELECT 1 FROM VAGOES_REFAT_TREM VRT WHERE VRT.CP_ID_CPS = C.CP_ID_CPS AND VRT.VG_ID_VG = CV.VG_ID_VG) 
											AND T.TR_STT_TRM IN ('C','R') ");

                if (!string.IsNullOrEmpty(ordemServico))
                {
                    builder.AppendLine(" AND OS.X1_NRO_OS = :ordemServico ");
                }

                if (!string.IsNullOrEmpty(prefixoTrem))
                {
                    builder.AppendLine(" AND T.TR_PFX_TRM = :prefixoTrem ");
                }

                builder.AppendLine(" ORDER BY 1 ");

                ISQLQuery query = session.CreateSQLQuery(builder.ToString());

                if (!string.IsNullOrEmpty(ordemServico))
                {
                    query.SetString("ordemServico", ordemServico);
                }

                if (!string.IsNullOrEmpty(prefixoTrem))
                {
                    query.SetString("prefixoTrem", prefixoTrem);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(RefaturamentoDto)));
                return query.List<RefaturamentoDto>();
            }
        }

        /// <summary>
        /// Salvar os Refaturamentos
        /// </summary>
        /// <param name="listaVagoes"> Lista de Vagoes</param>
        public void SalvarRefaturamento(RefatVagoesDto listaVagoes)
        {
            using (ISession session = OpenSession())
            {
                XmlDocument xmlDocument = new XmlDocument();
                string xmlVagoes = GerarXmlRefaturamento(listaVagoes);
                string xmlRetorno = null;

                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "SP_REFAT_EM_TREM",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xmlVagoes, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlRetorno, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);

                command.Prepare();
                command.ExecuteNonQuery();

                xmlRetorno = command.Parameters["pxmlSaida"].Value.ToString();

                if (!string.IsNullOrEmpty(xmlRetorno))
                {
                    xmlDocument.LoadXml(xmlRetorno);
                    XmlNode erro = xmlDocument.SelectSingleNode(@"XML/ERRO");

                    throw new Exception(erro != null ? erro.InnerText : "Ocorreu um erro inesperado");
                }
            }
        }

        /// <summary>
        /// Recupera o saldo disponivel para o contrato.
        /// </summary>
        /// <param name="contrato">n�mero do contrato</param>
        /// <returns>Saldo dispon�vel.</returns>
        public decimal ObterSaldoContratoDisponivel(string contrato)
        {
            string sql = "SELECT SALDO_RETENCAO FROM sispat.VW_SADO_POR_CONTRATO_TERMINAL@link_tlrodo xx WHERE xx.contrato= :contrato";
            using (ISession session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(sql);
                query.SetString("contrato", contrato);
                query.SetReadOnly(true);
                decimal saldo = query.UniqueResult<decimal>();
                return saldo / 1000;
            }
        }

        /// <summary>
        ///    Obt�m o somat�rio dos pesos das notas dos ctes contidos no vagao vigente do cte informado pela chave
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        public decimal ObterSomaPesoNotasCtesPorVagaoPedidoVigente(string chaveCte)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine("  SELECT ");
            sqlCte.AppendLine("      sum(det.NF_PESO_NF) as peso ");
            sqlCte.AppendLine("  FROM  ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det det,  ");
            sqlCte.AppendLine("      despacho do,    ");
            sqlCte.AppendLine("      vagao_pedido_vig v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl, ");
            sqlCte.AppendLine("      item_despacho    ide ");
            sqlCte.AppendLine("  WHERE  ");
            sqlCte.AppendLine("      do.px_idt_pdr = v.px_idt_pdr  ");
            sqlCte.AppendLine("      and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("      and fl.fx_id_flx = c.fx_id_flx  ");
            sqlCte.AppendLine("      and c.id_cte = det.id_cte   ");
            sqlCte.AppendLine("      and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("      and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte =  '" + chaveCte + "') ");
            sqlCte.AppendLine("      and fl.FX_ID_FLX in (select FX_ID_FLX from cte where chave_cte =  '" + chaveCte + "') ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                return query.UniqueResult<Decimal>();
            }
        }

        /// <summary>
        ///    Obt�m o somat�rio dos pesos das notas dos ctes contidos no vagao vigente e fluxo do cte informado pela chave
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        public decimal ObterSomaPesoNotasCtesPorVagaoPedidoVigenteFluxo(string chaveCte)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine("  SELECT ");
            sqlCte.AppendLine("      sum(det.NF_PESO_NF) as peso ");
            sqlCte.AppendLine("  FROM  ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det det,  ");
            sqlCte.AppendLine("      despacho do,    ");
            sqlCte.AppendLine("      vagao_pedido_vig v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl, ");
            sqlCte.AppendLine("      item_despacho    ide ");
            sqlCte.AppendLine("  WHERE  ");
            sqlCte.AppendLine("      do.px_idt_pdr = v.px_idt_pdr  ");
            sqlCte.AppendLine("      and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("      and fl.fx_id_flx = c.fx_id_flx  ");
            sqlCte.AppendLine("      and c.id_cte = det.id_cte   ");
            sqlCte.AppendLine("      and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("      and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte =  '" + chaveCte + "') ");
            sqlCte.AppendLine("      and fl.FX_ID_FLX in (select FX_ID_FLX from cte where chave_cte =  '" + chaveCte + "') ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                return query.UniqueResult<Decimal>();
            }
        }

        /// <summary>
        /// Obt�m ctes cancelados por periodo sigla ferrovia.
        /// </summary>
        /// <param name="dataInicio"> Data inicio. </param>
        /// <param name="dataTermino"> Data termino. </param>
        /// <param name="siglaEstado"> Sigla do estado. </param>
        /// <returns> Lista de ctes cancelados. </returns>
        public IList<CteFerroviarioCancelado> ObterCtesCanceladosPorPeriodoSiglaFerrovia(DateTime dataInicio, DateTime dataTermino, string siglaEstado)
        {
            StringBuilder builder = new StringBuilder();
            using (var session = OpenSession())
            {
                builder.AppendLine(@"SELECT c.ID_CTE as ""IdCte"",
											C.CHAVE_CTE as ""ChaveCte"",  
										   JOIN(CURSOR(SELECT CD.NFE_CHAVE_NFE FROM CTE_DET CD WHERE CD.ID_CTE=C.ID_CTE), '|') AS ""Nfes""
									FROM CTE C 
									JOIN CTE_STATUS CS ON C.ID_CTE = CS.ID_CTE
									WHERE 
									C.SITUACAO_ATUAL = 'CAN'
									AND CS.DATA_HORA between :dataInicio and :dataTermino
									AND CS.CODIGO_STATUS_RETORNO=13
									and c.sigla_uf_ferrovia=:siglaEstado");

                ISQLQuery query = session.CreateSQLQuery(builder.ToString());
                query.SetString("siglaEstado", siglaEstado);
                query.SetDateTime("dataInicio", dataInicio);
                query.SetDateTime("dataTermino", dataTermino);

                query.AddScalar("IdCte", NHibernateUtil.Int32);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteFerroviarioCancelado)));
                return query.List<CteFerroviarioCancelado>();
            }
        }

        /// <summary>
        /// Obt�m ctes por periodo sigla ferrovia.
        /// </summary>
        /// <param name="dataInicio"> Data inicio. </param>
        /// <param name="dataTermino"> Data termino. </param>
        /// <param name="siglaEstado"> Sigla do Estado. </param>
        /// <returns> Lista de ctes gerados </returns>
        public IList<CteFerroviarioDescarregado> ObterCtesDescarregaosPorPeriodoSiglaFerrovia(DateTime dataInicio, DateTime dataTermino, string siglaEstado)
        {
            StringBuilder builder = new StringBuilder();
            using (var session = OpenSession())
            {
                builder.AppendLine(@"SELECT 
										C.CHAVE_CTE as ""ChaveCte"", 
										C.SITUACAO_ATUAL as ""Status"", 
										E.EP_CGC_EMP as ""RecintoEntregaAlfandegado"", 
										C.PESO_VAGAO as ""PesoEntregue"",
										join (
										cursor (SELECT c1.chave_cte FROM CTE_ARVORE CA1 
										join cte_arvore ca2 on ca1.id_cte_raiz = ca2.id_cte_raiz and ca2.sequencia > ca1.sequencia
										join cte c1 on ca2.id_cte = c1.id_cte
										WHERE CA1.ID_CTE=c.id_cte), '|'
										)  as ""CtesNovos"",
										join (
										cursor (SELECT CD.NFE_CHAVE_NFE FROM CTE_DET CD WHERE CD.ID_CTE=C.ID_CTE and CD.NFE_CHAVE_NFE is not null), '|'
										)  as  ""Nfes"",
										M.MN_DSC_MNC AS ""CidadeEmbarque""
									FROM DESCARREGAMENTO D
									JOIN DETALHE_CARREGAMENTO DC ON D.CG_ID_CAR = DC.CG_ID_CAR
									JOIN ITEM_DESPACHO IDE ON IDE.DC_ID_CRG = DC.DC_ID_CRG
									JOIN CTE C ON C.DP_ID_DP = IDE.DP_ID_DP
									JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = C.FX_ID_FLX 
									JOIN AREA_OPERACIONAL AO ON NVL(FC.AO_ID_INT_OR, FC.AO_ID_EST_OR) = AO.AO_ID_AO
									JOIN MUNICIPIO M ON AO.MN_ID_MNC = M.MN_ID_MNC
									JOIN CONTRATO CT ON FC.CZ_ID_CTT = CT.CZ_ID_CTT
									JOIN EMPRESA E ON CT.EP_ID_EMP_DST = E.EP_ID_EMP
									WHERE D.DS_DAT_DSC between :dataInicio and :dataTermino
									AND C.SIGLA_UF_FERROVIA = :siglaEstado");

                ISQLQuery query = session.CreateSQLQuery(builder.ToString());
                query.SetString("siglaEstado", siglaEstado);
                query.SetDateTime("dataInicio", dataInicio);
                query.SetDateTime("dataTermino", dataTermino);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteFerroviarioDescarregado)));
                return query.List<CteFerroviarioDescarregado>();
            }
        }

        /// <summary>
        /// Obt�m os ctes da arvore gerados apos o cancelamento
        /// </summary>
        /// <param name="idCte">Id do Cte...</param>
        /// <returns>Lista de novos ctes</returns>
        public IList<CteFerroviarioCanceladoNovoCte> ObterCtesGeradosAposCancelamentoPorIdCte(int idCte)
        {
            StringBuilder builder = new StringBuilder();
            using (var session = OpenSession())
            {
                builder.AppendLine(@"SELECT c1.chave_cte as ""ChaveCte"",
											JOIN(CURSOR(SELECT CD.NFE_CHAVE_NFE FROM CTE_DET CD WHERE CD.ID_CTE=C1.ID_CTE), '|') AS ""Nfes""
										FROM CTE_ARVORE CA1 
										join cte_arvore ca2 on ca1.id_cte_raiz = ca2.id_cte_raiz and ca2.sequencia > ca1.sequencia
										join cte c1 on ca2.id_cte = c1.id_cte
										WHERE CA1.ID_CTE=:idCte");

                ISQLQuery query = session.CreateSQLQuery(builder.ToString());
                query.SetInt32("idCte", idCte);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteFerroviarioCanceladoNovoCte)));
                return query.List<CteFerroviarioCanceladoNovoCte>();
            }
        }

        /// <summary>
        /// Retorna a lista de Ctes com erro para o sem�foro
        /// </summary>
        /// <returns>Lista de Ctes com erro</returns>
        public IList<SemaforoCteDto> ObterCtesErro()
        {
            const string SqlResult = @"SELECT SUM(QtdZeroDozeHoras) as ""QtdZeroDozeHoras"", SUM(QtdDozeVinteQuatroHoras) as ""QtdDozeVinteQuatroHoras"", SUM(QtdMaisQueVinteQuatroHoras) AS ""QtdMaisQueVinteQuatroHoras"", Uf AS ""Uf"" FROM (
SELECT COUNT(0) as QtdZeroDozeHoras, 0 as QtdDozeVinteQuatroHoras, 0 AS QtdMaisQueVinteQuatroHoras, C.Sigla_Uf_Ferrovia as Uf
                    FROM CTE C
                   WHERE C.AMBIENTE_SEFAZ = 1
                     AND C.SITUACAO_ATUAL NOT IN ('AUT','ANL','INT','CAN')
                     AND C.Data_CTE < sysdate - 2 / 24 / 60
                     AND ((sysdate - C.Data_CTE) * 24) <= 12
                   GROUP BY C.Sigla_Uf_Ferrovia, 0
UNION ALL                   
SELECT 0 as QtdZeroDozeHoras,  COUNT(0) as QtdDozeVinteQuatroHoras, 0 AS QtdMaisQueVinteQuatroHoras, C.Sigla_Uf_Ferrovia as Uf
                    FROM CTE C
                   WHERE C.AMBIENTE_SEFAZ = 1
                     AND C.SITUACAO_ATUAL NOT IN ('AUT','ANL','INT','CAN')
                     AND C.Data_CTE < sysdate - 2 / 24 / 60
                     AND ((sysdate - C.Data_CTE) * 24) > 12 AND ((sysdate - C.Data_CTE) * 24) <= 24
                   GROUP BY C.Sigla_Uf_Ferrovia, 0
UNION ALL                   
SELECT 0 as QtdZeroDozeHoras, 0 as QtdDozeVinteQuatroHoras,  COUNT(0) AS QtdMaisQueVinteQuatroHoras, C.Sigla_Uf_Ferrovia as Uf
                    FROM CTE C
                   WHERE C.AMBIENTE_SEFAZ = 1
                     AND C.SITUACAO_ATUAL NOT IN ('AUT','ANL','INT','CAN')
                     AND C.Data_CTE < sysdate - 2 / 24 / 60
                     AND ((sysdate - C.Data_CTE) * 24) > 24
                   GROUP BY C.Sigla_Uf_Ferrovia, 0) GROUP BY Uf, 0,1,2 ORDER BY 3";

            IList<SemaforoCteDto> lstRetorno;

            using (var session = OpenSession())
            {
                ISQLQuery query = session.CreateSQLQuery(SqlResult);
                query.SetResultTransformer(Transformers.AliasToBean(typeof(SemaforoCteDto)));
                lstRetorno = query.List<SemaforoCteDto>();
            }

            return lstRetorno;
        }

        /// <summary>
        /// Obter os vag�es de acordo com os filtros informados na tela
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="dataInicial">Data inicial do Cte</param>
        /// <param name="dataFinal">Data final do Cte</param>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <param name="fluxo">O fluxo do cte</param>
        /// <param name="siglaUfFerrovia">a UF da ferrovia do Cte</param>
        /// <param name="origem">A Origem do cte</param>
        /// <param name="destino">O Destino do cte</param>
        /// <param name="serieDesp">O n�mero de s�rie do despacho</param>
        /// <param name="numeroDesp">O n�mero do despacho</param>
        /// <param name="chaveCte">O n�mero da chave do cte</param>
        /// <param name="codVagao">O c�digo do vag�o</param>
        /// <param name="tipoCte">Tipo de Cte para serem filtradas</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorFiltros(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string chaveNfe, string fluxo, string siglaUfFerrovia, string origem, string destino, int serieDesp, int numeroDesp, string[] chaveCte, string codVagao, string tipoCte)
        {
            var whereCte = new StringBuilder();
            var whereCteCp = new StringBuilder();
            var parametros = new List<Action<IQuery>>();
            ResultadoPaginado<CteDto> retorno = null;

            whereCte.AppendLine(" AND CG.CG_DTR_CRG BETWEEN :dataInicial AND :dataFinal");
            whereCteCp.AppendLine(" AND CG.CG_DTR_CRG BETWEEN :dataInicial AND :dataFinal");
            parametros.Add(q => q.SetParameter("dataInicial", dataInicial));
            parametros.Add(q => q.SetParameter("dataFinal", dataFinal));

            if (!string.IsNullOrWhiteSpace(chaveNfe))
            {
                whereCte.AppendLine(" AND N.nfe_chave_nfe = :chaveNfe");
                whereCteCp.AppendLine(" AND N.nfe_chave_nfe = :chaveNfe");
                parametros.Add(q => q.SetParameter("chaveNfe", chaveNfe));
            }

            if (chaveCte != null && chaveCte.Any())
            {
                whereCte.AppendLine(" AND C.chave_cte IN ( ");
                whereCteCp.AppendLine(" AND COR.chave_cte IN ( ");
                if (chaveCte.Count() > 1)
                {
                    for (var i = 0; i < chaveCte.Count() - 1; i++)
                    {
                        whereCte.AppendLine(":chaveCte" + i + ",");
                        whereCteCp.AppendLine(":chaveCte" + i + ",");
                        int i1 = i;
                        parametros.Add(q => q.SetParameter("chaveCte" + i1, chaveCte[i1]));
                    }
                    whereCte.AppendLine(":chaveCte" + (chaveCte.Count() - 1) + ")");
                    whereCteCp.AppendLine(":chaveCte" + (chaveCte.Count() - 1) + ")");
                    int i2 = (chaveCte.Count() - 1);
                    parametros.Add(q => q.SetParameter("chaveCte" + i2, chaveCte[i2]));
                }
                else
                {
                    whereCte.AppendLine(":chaveCte0)");
                    whereCteCp.AppendLine(":chaveCte0)");
                    parametros.Add(q => q.SetParameter("chaveCte0", chaveCte[0]));
                }
            }

            if (!string.IsNullOrWhiteSpace(fluxo))
            {
                whereCte.AppendLine(" and FC.FX_COD_FLX LIKE :fluxo");
                whereCteCp.AppendLine(" and FC.FX_COD_FLX LIKE :fluxo");
                parametros.Add(q => q.SetParameter("fluxo", string.Format("%{0}", fluxo)));
            }

            if (!string.IsNullOrWhiteSpace(siglaUfFerrovia))
            {
                whereCte.AppendLine(" and C.Sigla_Uf_Ferrovia = :siglaUfFerrovia");
                whereCteCp.AppendLine(" and COR.Sigla_Uf_Ferrovia = :siglaUfFerrovia");
                parametros.Add(q => q.SetParameter("siglaUfFerrovia", siglaUfFerrovia));
            }

            if (!string.IsNullOrWhiteSpace(origem))
            {
                whereCte.AppendLine(" and AO.AO_COD_AOP = :origem");
                whereCteCp.AppendLine(" and AO.AO_COD_AOP = :origem");
                parametros.Add(q => q.SetParameter("origem", origem));
            }

            if (!string.IsNullOrWhiteSpace(destino))
            {
                whereCte.AppendLine(" and AOO.AO_COD_AOP = :destino");
                whereCteCp.AppendLine(" and AOO.AO_COD_AOP = :destino");
                parametros.Add(q => q.SetParameter("destino", destino));
            }

            if (serieDesp > 0)
            {
                whereCte.AppendLine(" and sd.sk_num_srd = :serieDesp");
                parametros.Add(q => q.SetParameter("serieDesp", serieDesp));
            }

            if (numeroDesp > 0)
            {
                whereCte.AppendLine(" and D.Dp_Num_Dsp = :numeroDesp");
                parametros.Add(q => q.SetParameter("numeroDesp", numeroDesp));
            }

            if (!string.IsNullOrWhiteSpace(codVagao))
            {
                whereCte.AppendLine(" and V.VG_COD_VAG = :codVagao");
                whereCteCp.AppendLine(" and V.VG_COD_VAG = :codVagao");
                parametros.Add(q => q.SetParameter("codVagao", codVagao));
            }

            if (!string.IsNullOrWhiteSpace(tipoCte))
            {
                if (tipoCte != "CCE")
                {
                    whereCte.AppendLine(" AND C.TIPO_CTE = :tipoCte");
                    whereCteCp.AppendLine(" AND COR.TIPO_CTE = :tipoCte");
                    parametros.Add(q => q.SetParameter("tipoCte", tipoCte));
                }

                if (tipoCte == "CCE")
                {
                    whereCte.AppendLine(" AND EXISTS(SELECT 1 FROM CTE_CORRECAO CR WHERE CR.ID_CTE = C.ID_CTE)");
                    whereCteCp.AppendLine(" AND EXISTS(SELECT 1 FROM CTE_CORRECAO CR WHERE CR.ID_CTE = COR.ID_CTE)");
                }
            }

            var sqlQuery = GerarSqlQueryComposicaoFiltros(whereCte.ToString(), whereCteCp.ToString());

            using (var session = SessionManager.OpenStatelessSession())
            {
                var sqlCount = string.Format(@"SELECT COUNT(0) FROM({0})", sqlQuery);

                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(sqlCount);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);

                    query.SetMaxResults(limit > 0 ? limit : DetalhesPaginacao.LimitePadrao);
                }

                IList<CteDto> items = query.List<CteDto>();

                var totalCount = items.Count < 50 ? (decimal)items.Count : queryCount.UniqueResult<decimal>();

                retorno = new ResultadoPaginado<CteDto> { Total = (long)totalCount, Items = items };
            }

            return retorno;
        }

        /// <summary>
        /// Obt�m os vag�es de acordo como o Trem informado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorTrem(DetalhesPaginacao pagination, int idTrem)
        {
            var whereCte = new StringBuilder();
            var whereCteCp = new StringBuilder();
            var parametros = new List<Action<IQuery>>();
            ResultadoPaginado<CteDto> retorno = null;

            if (idTrem > 0)
            {
                whereCte.AppendLine(" AND T.tr_id_trm = :idTrem");
                whereCteCp.AppendLine(" AND T.tr_id_trm = :idTrem");
                parametros.Add(q => q.SetParameter("idTrem", idTrem));
            }

            var sqlQuery = GerarSqlQueryComposicaoFiltros(whereCte.ToString(), whereCteCp.ToString());

            using (var session = OpenSession())
            {
                var sqlCount = string.Format(@"SELECT COUNT(0) FROM({0})", sqlQuery);

                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(sqlCount);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);
                    if (limit > 0)
                    {
                        query.SetMaxResults(limit);
                    }
                    else
                    {
                        query.SetMaxResults(DetalhesPaginacao.LimitePadrao);
                    }
                }

                IList<CteDto> items = query.List<CteDto>();
                var totalCount = queryCount.UniqueResult<decimal>();

                retorno = new ResultadoPaginado<CteDto> { Total = (long)totalCount, Items = items };
            }

            return retorno;
        }

        /// <summary>
        /// Obt�m os vag�es de acordo com o Mdfe selecionado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idMdfe">Id da Mdfe selecionada</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorMdfe(DetalhesPaginacao pagination, int idMdfe)
        {
            var whereCte = new StringBuilder();
            var whereCteCp = new StringBuilder();
            var parametros = new List<Action<IQuery>>();
            ResultadoPaginado<CteDto> retorno = null;

            if (idMdfe > 0)
            {
                whereCte.AppendLine(" AND M.id_mdfe = :idMdfe");
                whereCteCp.AppendLine(" AND M.id_mdfe = :idMdfe");
                parametros.Add(q => q.SetParameter("idMdfe", idMdfe));
            }

            var sqlQuery = GerarSqlQueryComposicaoFiltros(whereCte.ToString(), whereCteCp.ToString());

            using (var session = OpenSession())
            {
                var sqlCount = string.Format(@"SELECT COUNT(0) FROM({0})", sqlQuery);

                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(sqlCount);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);
                    if (limit > 0)
                    {
                        query.SetMaxResults(limit);
                    }
                    else
                    {
                        query.SetMaxResults(DetalhesPaginacao.LimitePadrao);
                    }
                }

                IList<CteDto> items = query.List<CteDto>();
                var totalCount = queryCount.UniqueResult<decimal>();

                retorno = new ResultadoPaginado<CteDto> { Total = (long)totalCount, Items = items };
            }

            return retorno;
        }

        /// <summary>
        ///    Obt�m os ctes de acordo com o id do vag�o vigente 
        /// </summary>
        /// <param name="idVagao">Detalhes de pagina��o de resultados</param>
        public IList<Cte> ObterCtesPorVagaoPedidoVigente(int? idVagao)
        {
            ////DetachedCriteria criteria = CriarCriteria();


            ////            sqlCte.AppendLine("      Cte  cte,  ");
            ////sqlCte.AppendLine("      CteDetalhe  det, ");
            ////sqlCte.AppendLine("      DespachoTranslogic  do,  ");
            ////sqlCte.AppendLine("      VagaoPedidoVigente  v,  ");
            ////sqlCte.AppendLine("      FluxoComercial  fl,  ");
            ////sqlCte.AppendLine("      ItemDespacho  ide  ");


            ////criteria.CreateAlias("CteDetalhe", "ctedet", JoinType.InnerJoin);
            ////criteria.CreateAlias("FluxoComercial", "flx", JoinType.InnerJoin);
            ////criteria.CreateAlias("DespachoTranslogic", "desp", JoinType.InnerJoin);
            ////criteria.CreateAlias("ItemDespacho", "itdesp", JoinType.InnerJoin);

            ////criteria.CreateAlias("VagaoPedidoVigente", "vgped", JoinType.InnerJoin);


            ////criteria.CreateAlias("desp.SerieDespacho", "serdesp", JoinType.LeftOuterJoin);
            ////criteria.CreateAlias("desp.SerieDespachoUf", "serdespuf", JoinType.LeftOuterJoin);
            ////criteria.CreateAlias("Vagao", "vag", JoinType.LeftOuterJoin);
            ////criteria.CreateAlias("Vagao", "vag", JoinType.LeftOuterJoin);

            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));
            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));
            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));
            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));
            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));
            ////criteria.Add(Restrictions.Eq("NfeAnulacao", nfeAnulacao));

            ////criteria.SetFetchMode("desp", FetchMode.Eager);
            ////criteria.SetFetchMode("serdesp", FetchMode.Eager);
            ////criteria.SetFetchMode("serdespuf", FetchMode.Eager);
            ////criteria.SetFetchMode("vag", FetchMode.Eager);
            ////criteria.SetFetchMode("vag", FetchMode.Eager);


            ////return ObterTodos(criteria);

            ////sdfadfasdf


            ////DetachedCriteria criteria = CriarCriteria();

            ////criteria.CreateAlias("Despacho", "desp", JoinType.InnerJoin);
            ////criteria.CreateAlias("desp.SerieDespacho", "serdesp", JoinType.LeftOuterJoin);
            ////criteria.CreateAlias("desp.SerieDespachoUf", "serdespuf", JoinType.LeftOuterJoin);
            ////criteria.CreateAlias("Vagao", "vag", JoinType.LeftOuterJoin);

            ////if (idCte != 0)
            ////{
            ////    criteria.Add(Restrictions.Eq("Id", idCte));
            ////}


            ////return ObterPrimeiro(criteria);


            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT cte FROM ");
            sqlCte.AppendLine("      Cte  cte,  ");
            sqlCte.AppendLine("      CteDetalhe  det, ");
            sqlCte.AppendLine("      DespachoTranslogic  do,  ");
            sqlCte.AppendLine("      VagaoPedidoVigente  v,  ");
            sqlCte.AppendLine("      FluxoComercial  fl,  ");
            sqlCte.AppendLine("      ItemDespacho  ide  ");
            sqlCte.AppendLine(" WHERE ");
            sqlCte.AppendLine("     do.PedidoDerivado.Id = v.PedidoDerivado.Id ");
            sqlCte.AppendLine("     and do.Id = cte.Despacho.Id  ");
            sqlCte.AppendLine("     and fl.Id = cte.FluxoComercial.Id ");
            sqlCte.AppendLine("     and cte.Id = det.Cte.Id  ");
            sqlCte.AppendLine("     and do.Id = ide.DespachoTranslogic.Id ");
            sqlCte.AppendLine("     and v.Vagao.Id = " + idVagao);

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(sqlCte.ToString());
                return query.List<Cte>();
            }
        }

        /// <summary>
        ///    Obt�m os ctes de acordo com o id do vag�o vigente do CTE informado pelo chave.
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        /// <param name="conteiner">C�digo do conteiner</param>
        public IList<CteDto> ObterCtesPorVagaoPedidoVigenteConteiner(string chaveCte, string conteiner)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT DISTINCT  ");
            sqlCte.AppendLine("     c.chave_cte as \"Chave\", ");
            sqlCte.AppendLine("     c.PESO_VAGAO as \"PesoVagaoDec\", ");
            sqlCte.AppendLine("     c.IND_MULT_DP as \"IndMultiploDespacho\" ");
            sqlCte.AppendLine(" FROM ");
            sqlCte.AppendLine("     cte   c,  ");
            sqlCte.AppendLine("     cte_det   det, ");
            sqlCte.AppendLine("     despacho   do,  ");
            sqlCte.AppendLine("     vagao_pedido_vig v,  ");
            sqlCte.AppendLine("     fluxo_comercial  fl,  ");
            sqlCte.AppendLine("     item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("  and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte =  '" + chaveCte + "') ");
            sqlCte.AppendLine("    and det.nf_conteiner in (select distinct nf_conteiner from cte_det cd inner ");
            sqlCte.AppendLine("    join cte c on (c.Id_Cte = cd.Id_Cte) where ");
            sqlCte.AppendLine("    c.chave_cte = '" + chaveCte + "')");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<CteDto>());
                return query.List<CteDto>();
            }
        }

        /// <summary>
        ///    Obt�m os ctes de acordo com o id do vag�o vigente 
        /// </summary>
        /// <param name="idVagao">Id do vag�o</param>
        /// <param name="conteiner">C�digo do conteiner</param>
        public IList<Cte> ObterCtesRateioPorCte(string chaveCte)
        {
            var sqlCte = new StringBuilder();


            // - pega id do despacho do CTE, para pegar o pedido do despacho
            var cte = ObterPorChave(chaveCte);


            ////var idPedido = cte.Despacho.PedidoDerivado.Id;


            ////asdfadfadsf

            //// - pega o vagao pelo pedido do despacho
            //// - pelo vagao pedido, verifica os ctes gerados no mesmo dia no mesmo conteiner no vagao


            //// lista de conteiners do CTE e do vagao


            sqlCte.AppendLine(" SELECT c.* FROM ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det          det, ");
            sqlCte.AppendLine("      despacho         do,  ");
            sqlCte.AppendLine("      vagao_pedido     v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl,  ");
            sqlCte.AppendLine("      item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            ////sqlCte.AppendLine("  and v.vg_id_vg = " + idVagao);
            ////sqlCte.AppendLine("  and det.nf_conteiner = '" + conteiner + "' ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                query.SetResultTransformer(Transformers.AliasToBean(typeof(Cte)));
                return query.List<Cte>();
            }
        }

        /// <summary>
        ///    Obt�m os ctes de acordo com os id passadps em uma lista de int
        /// </summary>
        /// <param name="ctes">Id do vag�o</param>
        public List<CteDto> ObterCtesPorListaIds(IList<int> ctes)
        {
            var ctesRetorno = new List<CteDto>();
            foreach (var cteId in ctes)
            {
                var cte = ObterPorId(cteId);
                if (ctesRetorno.All(p => p.CteId != cteId))
                {
                    ctesRetorno.Add(new CteDto() { CteId = cte.Id ?? 0, Chave = cte.Chave, RateioCte = cte.FluxoComercial.IndRateioCte ?? false });
                }
            }
            return ctesRetorno;
        }

        /// <summary>
        /// Obt�m os vag�es de acordo com o sem�foro
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no sem�foro</param>
        /// <param name="periodoHorasErroSemaforo">O per�odo de tempo selecionado no sem�foro</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        public ResultadoPaginado<CteDto> ObterComposicaoPorSemaforo(DetalhesPaginacao pagination, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo)
        {
            var whereCte = new StringBuilder();
            var whereCteCp = new StringBuilder();
            var parametros = new List<Action<IQuery>>();
            ResultadoPaginado<CteDto> retorno = null;

            whereCte.AppendLine(" AND C.SITUACAO_ATUAL NOT IN ('AUT','ANL','INT','CAN')");
            whereCte.AppendLine(" AND C.AMBIENTE_SEFAZ = 1");
            whereCte.AppendLine(" AND C.SIGLA_UF_FERROVIA = :siglaUfSemaforo");

            whereCteCp.AppendLine(" AND COR.SITUACAO_ATUAL NOT IN ('AUT','ANL','INT','CAN')");
            whereCteCp.AppendLine(" AND COR.AMBIENTE_SEFAZ = 1");
            whereCteCp.AppendLine(" AND COR.SIGLA_UF_FERROVIA = :siglaUfSemaforo");

            parametros.Add(q => q.SetParameter("siglaUfSemaforo", siglaUfSemaforo));

            switch (periodoHorasErroSemaforo)
            {
                case HorasErroEnum.DeZeroADozeHoras:
                    whereCte.AppendLine(" AND ((sysdate - C.Data_CTE) * 24) <= 12");
                    whereCteCp.AppendLine(" AND ((sysdate - COR.Data_CTE) * 24) <= 12");
                    break;
                case HorasErroEnum.DeDozeAVinteQuatroHoras:
                    whereCte.AppendLine(" AND ((sysdate - C.Data_CTE) * 24) > 12 AND ((sysdate - C.Data_CTE) * 24) <= 24");
                    whereCteCp.AppendLine(" AND ((sysdate - COR.Data_CTE) * 24) > 12 AND ((sysdate - COR.Data_CTE) * 24) <= 24");
                    break;
                case HorasErroEnum.MaisQueVinteQuatroHoras:
                    whereCte.AppendLine(" AND ((sysdate - C.Data_CTE) * 24) > 24");
                    whereCteCp.AppendLine(" AND ((sysdate - COR.Data_CTE) * 24) > 24");
                    break;
            }

            var sqlQuery = GerarSqlQueryComposicaoFiltros(whereCte.ToString(), whereCteCp.ToString());

            using (var session = OpenSession())
            {
                var sqlCount = string.Format(@"SELECT COUNT(0) FROM({0})", sqlQuery);

                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(sqlCount);

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean(typeof(CteDto)));

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);
                    if (limit > 0)
                    {
                        query.SetMaxResults(limit);
                    }
                    else
                    {
                        query.SetMaxResults(DetalhesPaginacao.LimitePadrao);
                    }
                }

                IList<CteDto> items = query.List<CteDto>();
                var totalCount = queryCount.UniqueResult<decimal>();

                retorno = new ResultadoPaginado<CteDto> { Total = (long)totalCount, Items = items };
            }

            return retorno;
        }

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id da Cte que se deseja obter os status</param>
        /// <returns>Lista de Status da Cte</returns>
        public IList<CteStatus> ObterStatusCtePorIdCte(int idCte)
        {
            using (ISession session = OpenSession())
            {
                var hqlQyery = @"FROM CteStatus cs
                                WHERE cs.Cte.Id = :idCte
                                ORDER BY cs.DataHora";

                IQuery query = session.CreateQuery(hqlQyery);
                query.SetInt32("idCte", idCte);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteStatus>();
            }
        }

        /// <summary>
        /// Retorna os tipos de CTE que existe no banco de dados
        /// </summary>
        /// <returns>Lista de Tipos de Cte</returns>
        public IList<string> ObterTiposCte()
        {
            const string SqlQuery = @"SELECT DISTINCT C.TIPO_CTE FROM CTE C WHERE C.TIPO_CTE != 'RFT' GROUP BY C.TIPO_CTE";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(SqlQuery);

                return query.List<string>();
            }
        }

        /// <summary>
        /// Verifica se uma Cte pode ser inutilizada
        /// </summary>
        /// <param name="idCte">Id da Cte para inutilizar</param>
        /// <returns>Verdadeiro se pode inutilizar, falso se n�o pode</returns>
        public bool PodeInutilizar(int idCte)
        {
            var cte = ObterPorId(idCte);
            long autorizacoes = 0;

            var hqlString =
                @"select 
                        count(csa.Id)
                    from 
                        CteStatus csa
                    where 
                        csa.CteStatusRetorno.Id = 100 
                        and csa.Cte.Id = :idCte";

            using (var session = OpenSession())
            {
                IQuery query = session.CreateQuery(hqlString);
                query.SetInt32("idCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                autorizacoes = query.UniqueResult<long>();
            }

            return autorizacoes <= 0 && cte.SituacaoAtual == SituacaoCteEnum.Autorizado;
        }

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="idCte">Id da Cte para verificar</param>
        /// <returns>Verdadeiro se a Cte pode ser cancelada, sen�o falso</returns>
        public bool PodeCancelar(int idCte)
        {
            var cte = ObterPorId(idCte);
            decimal reemiteCancelamento = 0;

            var hqlString =
                @"select 
                        count(cs1.Id)
                    from 
                        CteStatus cs1,
                        CteStatus cs2
                    where 
                        cs1.CteStatusRetorno.Id = 19 
                        and cs1.Cte.Id = c.Id
                        and c.SituacaoAtual = 'EAC' 
                        and cs2.Id = (select max(csi.Id) from CteStatus csi where csi.Cte.Id = :idCte)
                        and cs2.CteStatusRetorno.Id not in (27, 28)";

            using (var session = OpenSession())
            {
                IQuery query = session.CreateQuery(hqlString);
                query.SetInt32("idCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                reemiteCancelamento = query.UniqueResult<decimal>();
            }

            return reemiteCancelamento > 0 && cte.SituacaoAtual == SituacaoCteEnum.ErroAutorizadoReEnvio;
        }

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="idVagao">Id do vagao vigente</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        public bool ExistemCtesRateioVagaoVigente(int idVagao)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT count(1) as cont FROM ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det          det, ");
            sqlCte.AppendLine("      despacho         do,  ");
            sqlCte.AppendLine("      vagao_pedido     v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl,  ");
            sqlCte.AppendLine("      item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("  and v.vg_id_vg = " + idVagao);
            sqlCte.AppendLine("  and fl.fx_ind_rateio_cte = 'S' ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                ////query.SetResultTransformer(Transformers.AliasToBean(typeof(Cte)));
                var result = query.UniqueResult<Decimal>();
                return result > 0;
            }
        }

        /// <summary>
        ///    Verifica se existe cte que fecha multiplo despacho no vagao vigente
        /// </summary>
        /// <param name="idVagao">Id do vagao vigente</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        public bool ExisteCteFinalizaMultiploDespachoVagaoVigente(int idVagao)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT count(1) as cont FROM ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det          det, ");
            sqlCte.AppendLine("      despacho         do,  ");
            sqlCte.AppendLine("      vagao_pedido     v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl,  ");
            sqlCte.AppendLine("      item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("  and v.vg_id_vg = " + idVagao);
            sqlCte.AppendLine("  and c.ind_mult_dp = 'N' ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                ////query.SetResultTransformer(Transformers.AliasToBean(typeof(Cte)));
                var result = query.UniqueResult<Decimal>();
                return result > 0;
            }
        }

        /// <summary>
        ///    Verifica se existe algum cte de rateio no vagao vigente do CTE informado
        /// </summary>
        /// <param name="chaveCte">chave do cte</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        public bool ExistemCtesRateioVagaoVigente(string chaveCte)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT count(1) as cont FROM ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det          det, ");
            sqlCte.AppendLine("      despacho         do,  ");
            sqlCte.AppendLine("      vagao_pedido     v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl,  ");
            sqlCte.AppendLine("      item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("  and v.vg_id_vg in (select VG_ID_VG from cte where chave_cte = '" + chaveCte + "') ");
            sqlCte.AppendLine("  and fl.fx_ind_rateio_cte = 'S' ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                var result = query.UniqueResult<Decimal>();
                return result > 0;
            }
        }

        /// <summary>
        ///    Verifica se existe cte que fecha multiplo despacho no vagao vigente
        /// </summary>
        /// <param name="chaveCte">chave do cte</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        public bool ExisteCteFinalizaMultiploDespachoVagaoVigente(string chaveCte)
        {
            var sqlCte = new StringBuilder();

            sqlCte.AppendLine(" SELECT count(1) as cont FROM ");
            sqlCte.AppendLine("      cte   c,  ");
            sqlCte.AppendLine("      cte_det          det, ");
            sqlCte.AppendLine("      despacho         do,  ");
            sqlCte.AppendLine("      vagao_pedido     v,  ");
            sqlCte.AppendLine("      fluxo_comercial  fl,  ");
            sqlCte.AppendLine("      item_despacho    ide  ");
            sqlCte.AppendLine(" WHERE do.px_idt_pdr = v.px_idt_pdr ");
            sqlCte.AppendLine("  and do.dp_id_dp = c.dp_id_dp  ");
            sqlCte.AppendLine("  and fl.fx_id_flx = c.fx_id_flx ");
            sqlCte.AppendLine("  and c.id_cte = det.id_cte  ");
            sqlCte.AppendLine("  and do.dp_id_dp = ide.dp_id_dp ");
            sqlCte.AppendLine("  and c.chave_cte = '" + chaveCte + "'");
            sqlCte.AppendLine("  and c.ind_mult_dp = 'N' ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlCte.ToString());
                var result = query.UniqueResult<Decimal>();
                return result > 0;
            }
        }

        /// <summary>
        /// Monta o Xml de Refaturamento
        /// </summary>
        /// <param name="listaVagoes">Lista de Vagoes</param>
        /// <returns>Retorna o xml de refaturamento em string.</returns>
        private string GerarXmlRefaturamento(RefatVagoesDto listaVagoes)
        {
            StringBuilder xml = new StringBuilder();

            xml.Append(@"<XML>");

            xml.Append(string.Format(@"<VAGOES ID_TREM=""{0}"" ID_COMPOSICAO=""{1}"" USUARIO=""{2}"">", listaVagoes.IdTrem, listaVagoes.IdComposicao, listaVagoes.Usuario));

            foreach (var vagao in listaVagoes.ListaVagao)
            {
                xml.Append(string.Format(@"<VAGAO ID=""{0}"" ID_DESPACHO=""{1}"" CONTRATO=""{2}"" />", vagao.IdVagao, vagao.IdDespacho, vagao.Contrato));
            }

            xml.Append(@"</VAGOES>");
            xml.Append(@"</XML>");

            return xml.ToString();
        }

        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> ser� adicionado</param>
        /// <param name="nome">Nome do par�metro</param>
        /// <param name="tipo">Tipo do par�metro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Dire��o <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }

        private string GerarSqlQueryComposicaoFiltros(string whereQueryCte, string whereQueryCteComplemetar)
        {
            var strQry = new StringBuilder();
            strQry.AppendLine("SELECT DISTINCT {0} FROM")
                .AppendLine(" VAGAO_PEDIDO VP,")
                .AppendLine(" COMPVAGAO CV,")
                .AppendLine(" COMPOSICAO CP,")
                .AppendLine(" VAGAO V,")
                .AppendLine(" CARREGAMENTO CG,")
                .AppendLine(" DETALHE_CARREGAMENTO DCG,")
                .AppendLine(" ITEM_DESPACHO ITD,")
                .AppendLine(" DESPACHO D,")
                .AppendLine(" NOTA_FISCAL N,")
                .AppendLine(" SERIE_DESPACHO SD,")
                .AppendLine(" CTE C,")
                .AppendLine(" CTE_DET CD,")
                .AppendLine(" {1}")
                .AppendLine(" FLUXO_COMERCIAL FC,")
                .AppendLine(" Area_Operacional AO,")
                .AppendLine(" Area_Operacional AOO,")
                .AppendLine(" TREM T,")
                .AppendLine(" MDFE M,")
                .AppendLine(" T2_OS OS")
                .AppendLine(" WHERE ")
                .AppendLine(" VP.PT_ID_ORT = CV.PT_ID_ORT(+)")
                .AppendLine(" AND VP.VG_ID_VG = CV.VG_ID_VG(+)")
                .AppendLine(" AND V.VG_ID_VG = VP.VG_ID_VG")
                .AppendLine(" AND CG.CG_ID_CAR = VP.CG_ID_CAR")
                .AppendLine(" AND CG.CG_ID_CAR = DCG.CG_ID_CAR")
                .AppendLine(" AND ITD.DC_ID_CRG = DCG.DC_ID_CRG")
                .AppendLine(" AND ITD.VG_ID_VG = DCG.VG_ID_VG")
                .AppendLine(" AND ITD.DP_ID_DP = D.DP_ID_DP")
                .AppendLine(" AND N.DP_ID_DP = D.DP_ID_DP")
                .AppendLine(" {2}")
                .AppendLine(" AND FC.FX_ID_FLX(+) = C.Fx_Id_Flx")
                .AppendLine(" AND SD.SK_IDT_SRD = D.SK_IDT_SRD")
                .AppendLine(" AND AO.AO_ID_AO(+) = FC.AO_ID_EST_OR")
                .AppendLine(" AND AOO.AO_ID_AO(+)= FC.AO_ID_EST_DS")
                .AppendLine(" AND CP.CP_ID_CPS(+) = CV.CP_ID_CPS")
                .AppendLine(" AND T.Tr_Id_Trm(+) = CP.TR_ID_TRM")
                .AppendLine(" AND M.CP_ID_CPS(+) = CP.CP_ID_CPS")
                .AppendLine(" AND OS.X1_ID_OS(+)= T.Of_Id_Osv")
                .AppendLine(" AND CG.CG_DTR_CRG < sysdate - 2 / 24 / 60");

            var selectStCte = new StringBuilder(@"C.ID_CTE as ""IdCte"", ")
                .AppendLine(@"C.SITUACAO_ATUAL as ""SituacaoCteStr"", ")
                .AppendLine(@"V.VG_COD_VAG as ""CodigoVagao"", ")
                .AppendLine(@"FC.FX_COD_FLX as ""Fluxo"", ")
                .AppendLine(@"sd.sk_num_srd as ""SerieDespacho"", ")
                .AppendLine(@"D.Dp_Num_Dsp as ""NumeroDespacho"", ")
                .AppendLine(@"D.DP_ID_DP as ""IdDespacho"", ")
                .AppendLine(@"C.SERIE_CTE as ""SerieCte"", ")
                .AppendLine(@"C.NRO_CTE as ""Numero"", ")
                .AppendLine(@"c.chave_cte as ""Chave"", ")
                .AppendLine(@"NVL(CG.CG_DTR_CRG, TO_DATE('01/01/01 00:00:00', 'dd/MM/yyyy HH24:MI:ss')) as ""DataCarregamento"", ")
                .AppendLine(@"NVL(C.DATA_CTE, TO_DATE('01/01/0001 00:00:00', 'dd/MM/yyyy HH24:MI:ss')) as ""DataEmissao"", ")
                .AppendLine(@"CASE WHEN CG.BD_IDT_BLD > 0 THEN 'S' ELSE 'N' END as ""HouveBaldeio""");

            var selectStCteCp = new StringBuilder(@"COR.ID_CTE as ""IdCte"", ")
                .AppendLine(@"COR.SITUACAO_ATUAL as ""SituacaoCteStr"", ")
                .AppendLine(@"V.VG_COD_VAG as ""CodigoVagao"", ")
                .AppendLine(@"FC.FX_COD_FLX as ""Fluxo"", ")
                .AppendLine(@"NULL as ""SerieDespacho"", ")
                .AppendLine(@"NULL as ""NumeroDespacho"", ")
                .AppendLine(@"COR.DP_ID_DP as ""IdDespacho"", ")
                .AppendLine(@"COR.SERIE_CTE as ""SerieCte"", ")
                .AppendLine(@"COR.NRO_CTE as ""Numero"", ")
                .AppendLine(@"COR.chave_cte as ""Chave"", ")
                .AppendLine(@"NVL(CG.CG_DTR_CRG, TO_DATE('01/01/01 00:00:00', 'dd/MM/yyyy HH24:MI:ss')) as ""DataCarregamento"", ")
                .AppendLine(@"NVL(COR.DATA_CTE, TO_DATE('01/01/0001 00:00:00', 'dd/MM/yyyy HH24:MI:ss')) AS ""DataEmissao"", ")
                .AppendLine(@"CASE WHEN CG.BD_IDT_BLD > 0 THEN 'S' ELSE 'N' END as ""HouveBaldeio""");

            const string JoinStatementCte = " AND C.DP_ID_DP(+) = D.DP_ID_DP AND CD.ID_CTE(+) = C.ID_CTE";

            const string FromStatementCteCp = "CTE_COMPLEMENTADO CC, CTE COR,";
            const string JoinStatementCteCp = "AND C.DP_ID_DP = D.DP_ID_DP AND C.ID_CTE(+) = CC.ID_CTE AND COR.ID_CTE(+) = CC.ID_CTE_COMPLEMENTAR AND CD.ID_CTE(+) = C.ID_CTE";

            var sqlQueryCte = new StringBuilder(string.Format(strQry.ToString(), selectStCte.ToString(), String.Empty, JoinStatementCte));
            var sqlQueryCteCp = new StringBuilder(string.Format(strQry.ToString(), selectStCteCp.ToString(), FromStatementCteCp, JoinStatementCteCp));

            sqlQueryCte.AppendLine(whereQueryCte);
            sqlQueryCteCp.AppendLine(whereQueryCteComplemetar);

            return sqlQueryCte.AppendLine(" UNION ALL ").AppendLine(sqlQueryCteCp.ToString()).ToString();
        }


        public Cte ObterCtePaiIdCteComplementar(int id)
        {
            using (ISession session = OpenSession())
            {
                string sql =
                        @"SELECT CT.ID_CTE AS CteId FROM CTE CT 
                           WHERE CT.ID_CTE = (SELECT CC.ID_CTE
                                                FROM CTE C
                                          INNER JOIN CTE_COMPLEMENTADO CC ON C.ID_CTE = CC.ID_CTE_COMPLEMENTAR
                                               WHERE C.TIPO_CTE = 'CPL' AND C.ID_CTE = :P_ID)";

                IQuery query = session.CreateSQLQuery(sql);
                query.SetParameter("P_ID", id);
                query.SetResultTransformer(Transformers.DistinctRootEntity);
                var idcte = query.List<decimal>();
                if (idcte.Count() > 0)
                {
                    return ObterPorIdCte((int)idcte[0]);
                }
                return null;
            }
        }

        public Cte ObterCteAutorizadoByDespacho(int id)
        {
            using (ISession session = OpenSession())
            {
                string sql = @" SELECT CT.ID_CTE AS CteId
                                      FROM CTE CT
                                     WHERE CT.ID_CTE = (SELECT *
                                                          FROM (SELECT C.ID_CTE as IDCTE
                                                                  FROM CTE_ANULADO C
                                                                 WHERE C.ID_CTE_ANULACAO = :P_ID
                                                                 order by C.DATA_HORA DESC)
                                                         WHERE ROWNUM = 1)";

                IQuery query = session.CreateSQLQuery(sql);
                query.SetParameter("P_ID", id);
                query.SetResultTransformer(Transformers.DistinctRootEntity);
                var idcte = query.List<decimal>();
                if (idcte.Count() > 0)
                {
                    return ObterPorIdCte((int)idcte[0]);
                }
                return null;
            }
        }

        /// <summary>
        /// Pega as informa��es do cte Virtual para a apresenta��o em tela e a exporta��o do excel quando o parametro isExportarExcel == true
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="codFluxo"></param>
        /// <param name="chaveCte"></param>
        /// <param name="numVagao"></param>
        /// <param name="isExportarExcel"></param>
        /// <returns></returns>
        public ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> CteVirtuaisSemVinculoMdfeDto(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string[] chaveCte, string[] numVagao, bool isExportarExcel)
        {
            StringBuilder sb = new StringBuilder();

            #region -- Query --
            sb.Append(@"SELECT C.NRO_CTE   AS ""NUM_CTE_VIRTUAL"",
                               C.SERIE_CTE AS ""SERIE_CTE_VIRTUAL"",
                               C.DATA_CTE  AS ""DATA_EMISSAO_VIRTUAL"",
                               C.DP_ID_DP  AS ""ID_DESPACHO"",
                               FX.FX_COD_FLX AS ""COD_FLUXO"",
                               V.VG_COD_VAG AS ""VAGAO""
                          FROM CTE C
                          INNER JOIN VAGAO V 
                             ON C.VG_ID_VG = V.VG_ID_VG
                          INNER JOIN FLUXO_COMERCIAL FX
                             ON FX.FX_ID_FLX = C.FX_ID_FLX
                         WHERE C.TIPO_CTE = 'VTL'
                           AND C.DATA_CTE BETWEEN :dataInicial AND :dataFinal {0} 
                           AND NOT EXISTS(SELECT 1 FROM MDFE_COMPOSICAO MC WHERE MC.ID_CTE = C.ID_CTE)");
            #endregion

            #region -- Condi��es --
            var sbCondicao = new StringBuilder();
            if (numVagao != null && numVagao.Length > 0)
            {
                sbCondicao.Append(" AND C.VG_ID_VG IN (SELECT V.VG_ID_VG FROM VAGAO V WHERE V.VG_COD_VAG IN (:vagao) )");
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                sbCondicao.Append(" AND FX.FX_COD_FLX like :codFluxo ");
            }

            if (chaveCte != null)
            {
                sbCondicao.Append(" AND C.CHAVE_CTE IN (:chaveCte) ");
            }

            ISession session = OpenSession();
            var auxSb = string.Format(sb.ToString(), sbCondicao.ToString());
            var stringHqlResult = auxSb;
            var stringHqlCount = string.Empty;

            if (!isExportarExcel)
                stringHqlCount = string.Format(auxSb.ToString(), " COUNT(C.ID_CTE) ");

            #endregion

            IQuery query = session.CreateSQLQuery(stringHqlResult);
            IQuery queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", stringHqlCount.ToString()));

            #region -- Valor Variaveis --
            if (dataInicial != null && dataFinal != null)
            {
                query.SetDateTime("dataInicial", dataInicial);

                if (!isExportarExcel)
                    queryCount.SetDateTime("dataInicial", dataInicial);

                query.SetDateTime("dataFinal", dataFinal);
                if (!isExportarExcel)
                    queryCount.SetDateTime("dataFinal", dataFinal);
            }

            if (numVagao != null && numVagao.Length > 0)
            {
                query.SetParameterList("vagao", numVagao);
                if (!isExportarExcel)
                    queryCount.SetParameterList("vagao", numVagao);
            }

            if (chaveCte != null)
            {
                query.SetParameterList("chaveCte", chaveCte);
                if (!isExportarExcel)
                    queryCount.SetParameterList("chaveCte", chaveCte);
            }

            if (codFluxo != string.Empty & codFluxo != null)
            {
                query.SetString("codFluxo", string.Concat("%", codFluxo));
                if (!isExportarExcel)
                    queryCount.SetString("codFluxo", string.Concat("%", codFluxo));
            }
            #endregion

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteVirtuaisSemVinculoMdfeDto)));

            if (pagination.Inicio.HasValue)
            {
                query.SetFirstResult(pagination.Inicio.Value);
            }

            int limit = GetLimit(pagination);
            if (limit > 0 && !isExportarExcel)
            {
                query.SetMaxResults(limit);
            }

            decimal total = 0;
            if (!isExportarExcel)
                total = queryCount.UniqueResult<decimal>();

            IList<CteVirtuaisSemVinculoMdfeDto> items = query.List<CteVirtuaisSemVinculoMdfeDto>();
            var result = new ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> { Total = (long)total, Items = items };

            return result;

        }

        public IList<CteVirtuaisSemVinculoMdfeDto> CteVirtuaisMdfePai(string[] numDespacho)
        {
            StringBuilder sb = new StringBuilder();

            #region -- Query --
            sb.Append(@"SELECT DISTINCT M.MT_ID_MOV    AS ""ID_MOVIMENTACAO"",
                                        M.AO_ID_AO     AS ""ID_AREA_OPERACIONAL"",
                                        M.TR_ID_TRM    AS ""ID_TREM"",
                                        M.CP_ID_CPS    AS ""COMPOSICAO"",
                                        M.PREFIXO_TREM AS ""PREFIXO_TREM"",
                                        MC.DP_ID_DP    AS ""ID_DESPACHO""
                          FROM MDFE M
                          INNER JOIN MDFE_COMPOSICAO MC
                            ON MC.ID_MDFE = M.ID_MDFE
                         WHERE MC.DP_ID_DP IN (:numDespacho)
                           AND M.ID_MDFE IN
                               (SELECT MAX(MD.ID_MDFE)
                                  FROM MDFE MD
                                 INNER JOIN MDFE_COMPOSICAO MCO
                                    ON MCO.ID_MDFE = MD.ID_MDFE
                                 WHERE MCO.ID_CTE IN
                                       (
                                        --FAZ A BUSCA DO CTE COM O TIPO NML
                                        SELECT ID_CTE
                                          FROM CTE
                                         WHERE DP_ID_DP IN (:numDespacho)
                                           AND TIPO_CTE = 'NML')
                                 GROUP BY MCO.ID_CTE)");
            #endregion

            #region -- Condi��es --
            ISession session = OpenSession();
            var stringHqlResult = string.Format(sb.ToString());

            #endregion

            IQuery query = session.CreateSQLQuery(stringHqlResult);

            #region -- Valor Variaveis --

            if (numDespacho != null && numDespacho.Length > 0)
            {
                query.SetParameterList("numDespacho", numDespacho);
            }

            #endregion

            query.SetResultTransformer(Transformers.AliasToBean(typeof(CteVirtuaisSemVinculoMdfeDto)));
            var result = query.List<CteVirtuaisSemVinculoMdfeDto>();

            return result;
        }


        #region -- Monitoramento Planejamento Descarga --

        public IList<MonitoramentoPlanejamentoDescargaDto> MonitoramentoPlanejamentoDescarga(DateTime dataInicial, DateTime dataFinal, string prefixo, string os, string terminal, string vagao)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(@"SELECT DISTINCT regexp_replace(LPAD(EMP.EP_CGC_EMP, 14),
                               '([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{4})',
                               '\1.\2.\3/\4-') AS ""Cnpj"",
                                        EMP.EP_RAZ_SCL AS ""RazaoSocial"",
                                        V.VG_COD_VAG AS ""Vagao"",
                                        SV.SV_COD_SER AS ""Serie"",
                                        MT.MT_DTS_RAL AS ""DataSaidaEstacao"",
                                        CTE.SITUACAO_ATUAL AS ""StatusCte"",
                                        CTE.ID_CTE AS ""NumeroCte"",
                                        CTE.CHAVE_CTE AS ""ChaveCte"",
                                        CASE CTE.PDF_GERADO
                                          WHEN 'S' THEN
                                           'SIM'
                                          ELSE
                                           'N�O'
                                        END AS ""Pdf"",
                                        CASE CTE.XML_GERADO
                                          WHEN 'S' THEN
                                           'SIM'
                                          ELSE
                                           'N�O'
                                        END AS ""Xml"",
                                        AOM.AO_COD_AOP AS ""Estacao"",
                                        OS.X1_NRO_OS AS ""NumOS"",                          
                                        T.TR_PFX_TRM AS ""Prefixo""
                          FROM EDI_DESCARREGAMENTO_ENVIO_WS E,
                               VAGAO_PEDIDO_VIG             VPV,
                               COMPVAGAO                    CVIG,
                               VAGAO                        V,
                               SERIE_VAGAO                  SV,
                               DETALHE_CARREGAMENTO         DC,
                               ITEM_DESPACHO                IDS,
                               CTE                          CTE,
                               CTE_EMPRESAS                 CTEE,
                               MOVIMENTACAO_TREM            MT,
                               EMPRESA                      EMP,
                               AREA_OPERACIONAL             AO,
                               AREA_OPERACIONAL             AOM,
                               COMPOSICAO                   COMP,
                               TREM                         T,
                               T2_OS                        OS,
                               CONF_ENVIO_DOC               CONF
                         WHERE DC.CG_ID_CAR = VPV.CG_ID_CAR
                           AND CONF.ID_EP_ID_EMP_DEST = EMP.EP_ID_EMP
                           AND COMP.CP_ID_CPS = CVIG.CP_ID_CPS
                           AND COMP.TR_ID_TRM = T.TR_ID_TRM
                           AND OS.x1_id_os = T.OF_ID_OSV
                           AND DC.VG_ID_VG = VPV.VG_ID_VG
                           AND IDS.DC_ID_CRG = DC.DC_ID_CRG
                           AND CTE.DP_ID_DP = IDS.DP_ID_DP
                           AND CVIG.VG_ID_VG = VPV.VG_ID_VG
                           AND VPV.VG_ID_VG = V.VG_ID_VG
                           AND V.SV_ID_SV = SV.SV_ID_SV
                           AND VPV.Pt_Id_Ort = CVIG.Pt_Id_Ort
                           AND CTEE.ID_CTE = CTE.ID_CTE
                           AND MT.CP_ID_CPS = CVIG.CP_ID_CPS
                           AND E.EDEW_ID_MOV = MT.MT_ID_MOV
                           AND EMP.EP_ID_EMP = CTEE.EP_ID_EMP_REC
                           AND MT.AO_ID_AO = AO.AO_ID_AO
                           AND AO.AO_ID_AO_INF = AOM.AO_ID_AO
                           AND E.EDEW_TIMESTAMP BETWEEN :dataInicial AND :dataFinal
                           AND AOM.AO_ID_AO = CONF.ID_AO_ID_AO_ENVIO
                           AND CONF.ENVIO_SAIDA='S'   
                           AND NOT EXISTS (SELECT 1
                                  FROM COMPOSICAO       CP1,
                                       COMPVAGAO_VIG    CVV1,
                                       VAGAO_PEDIDO_VIG VPV1,
                                       FLUXO_COMERCIAL  FXO
                                 WHERE CP1.TR_ID_TRM = E.EDEW_ID_TREM
                                   AND CP1.CP_ID_CPS = CVV1.CP_ID_CPS
                                   AND CVV1.VG_ID_VG = VPV1.VG_ID_VG
                                   AND VPV1.FX_ID_FLX = FXO.FX_ID_FLX
                                   AND FXO.FX_IND_OPER = 'S')
                           AND OS.X1_NRO_OS = :os
                            ");

            if (!string.IsNullOrWhiteSpace(terminal))
            {
                sb.AppendLine(@" AND EMP.EP_CGC_EMP = :terminal ");
    }
            if (!string.IsNullOrWhiteSpace(prefixo))
            {
                sb.AppendLine(@" AND T.TR_PFX_TRM = :prefixo ");
}
            if (!string.IsNullOrWhiteSpace(vagao))
            {
                sb.AppendLine(@" AND V.VG_COD_VAG = :vagao ");
            }
            sb.AppendLine(@"GROUP BY V.VG_COD_VAG,
                                  SV.SV_COD_SER,
                                  MT.MT_DTS_RAL,
                                  CTE.SITUACAO_ATUAL,
                                  CTE.ID_CTE,
                                  CTE.CHAVE_CTE,
                                  CTE.PDF_GERADO,
                                  CTE.XML_GERADO,
                                  AOM.AO_COD_AOP,
                                  EMP.EP_CGC_EMP,
                                  EMP.EP_RAZ_SCL,
                                  OS.X1_NRO_OS,
                                  T.TR_PFX_TRM
                         ORDER BY MT.MT_DTS_RAL");

            ISession session = OpenSession();
            var stringHqlResult = sb.ToString();
            IQuery query = session.CreateSQLQuery(stringHqlResult);
            
            if (dataInicial != null && dataFinal != null)
            {
                query.SetDateTime("dataInicial", dataInicial);
                query.SetDateTime("dataFinal", dataFinal);
            }
            if (!string.IsNullOrWhiteSpace(os))
            {
                query.SetString("os", os);
            }
            if (!string.IsNullOrWhiteSpace(terminal))
            {
                query.SetString("terminal", terminal);
            }
            if (!string.IsNullOrWhiteSpace(prefixo))
            {
                query.SetString("prefixo", prefixo);
            }
            if (!string.IsNullOrWhiteSpace(vagao))
            {
                query.SetString("vagao", vagao);
            }
            query.SetResultTransformer(Transformers.AliasToBean(typeof(MonitoramentoPlanejamentoDescargaDto)));
            IList<MonitoramentoPlanejamentoDescargaDto> result = query.List<MonitoramentoPlanejamentoDescargaDto>();
           return result;
        }
        #endregion

    }
}
