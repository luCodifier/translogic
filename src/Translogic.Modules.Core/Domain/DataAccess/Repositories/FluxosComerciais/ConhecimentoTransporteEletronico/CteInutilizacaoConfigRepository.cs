namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Linq;

    using ALL.Core.AcessoDados;

    using NHibernate.Criterion;

    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    ///     Classe de Cte Inutiliza��o Config
    /// </summary>
    public class CteInutilizacaoConfigRepository : NHRepository<CteInutilizacaoConfig, int?>,
                                                   ICteInutilizacaoConfigRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obtem o cte inutilizado pelo id da lista config
        /// </summary>
        /// <returns>Cte Inutilizado</returns>
        public CteInutilizacaoConfig ObterCteinutilizadosBaseConsolidadaConfigPorIdLista(int idListaConfig)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdListaConfig", idListaConfig));

            return ObterTodos(criteria).FirstOrDefault();
        }

        /// <summary>
        ///     Obtem a lista de ctes inutilizados processados ou n�o processados
        /// </summary>
        /// <returns>Lista de Ctes Inutilizados</returns>
        public IList<CteInutilizacaoConfig> ObterTodosProcessados(bool processado)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Processado", processado));

            return ObterTodos(criteria);
        }

        #endregion
    }
}