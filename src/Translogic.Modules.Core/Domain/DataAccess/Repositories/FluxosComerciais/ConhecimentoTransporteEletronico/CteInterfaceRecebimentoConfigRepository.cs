namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe de recebimento da interface de recebimento da config
	/// </summary>
	public class CteInterfaceRecebimentoConfigRepository : NHRepository<CteInterfaceRecebimentoConfig, int?>, ICteInterfaceRecebimentoConfigRepository
	{
		/// <summary>
		/// Obtem os itens da interface que nao foram processados (que n�o est�o no pooling de recebimento)
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-producao / 2-homologa��o</param>
		/// <returns>Retorna uma lista dos itens que n�o est�o no pooling</returns>
		public IList<CteInterfaceRecebimentoConfig> ObterNaoProcessados(int indAmbienteSefaz)
		{
			try
			{
				DateTime dataMinima = DateTime.Now;
				dataMinima = dataMinima.AddDays(-5);

				using (ISession session = OpenSession())
				{
					string hql = @"FROM CteInterfaceRecebimentoConfig cir
                    WHERE NOT EXISTS(SELECT cir.Id FROM CteRecebimentoPooling crp WHERE crp.Id = cir.Cte.Id)
										AND DataHora >= :DataMinima
					  AND cir.Cte.AmbienteSefaz = :ambienteSefaz
                    ORDER BY cir.DataUltimaLeitura";
					IQuery query = session.CreateQuery(hql);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetDateTime("DataMinima", dataMinima);
					query.SetInt32("ambienteSefaz", indAmbienteSefaz);

					query.SetMaxResults(50);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<CteInterfaceRecebimentoConfig>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Remove os cte da interface que ja foi processados
		/// </summary>
		/// <param name="cte">cte para ser removido da lista</param>
		public void RemoverPorCte(Cte cte)
		{
			string hql = @"DELETE FROM CteInterfaceRecebimentoConfig cir WHERE cir.Cte.Id = :idCte";
			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(hql);
				query.SetInt32("idCte", cte.Id.Value);
				query.ExecuteUpdate();
			}
		}
	}
}