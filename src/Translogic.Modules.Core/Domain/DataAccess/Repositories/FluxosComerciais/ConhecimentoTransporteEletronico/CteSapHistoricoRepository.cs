namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Classe de repositório do Sap histórico
	/// </summary>
	public class CteSapHistoricoRepository : NHRepository<CteSapHistorico, int?>, ICteSapHistoricoRepository
	{
	}
}