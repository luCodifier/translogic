namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Linq;

	using ALL.Core.AcessoDados;

	using Castle.Windsor.Configuration.Interpreters.XmlProcessor.ElementProcessors;

	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	using NHibernate.Transform;

	using Util;
    using System.Collections.Generic;
    using System.Text;
    using NHibernate;

	/// <summary>
	/// Reposit�rio da classe de CteArquivo
	/// </summary>
	public class CteArquivoRepository : NHRepository<CteArquivo, int?>, ICteArquivoRepository
	{
        public string ObterArqPorId(int? id)
	    {
	        using (var session = SessionManager.OpenStatelessSession())
	        {
	            var query = session.CreateSQLQuery("SELECT ARQUIVO_XML FROM CTE_ARQUIVO WHERE ID_CTE = :ID");

	            query.SetParameter("ID", id);

	            return query.List<string>().FirstOrDefault();
	        }
	    }

        public byte[] ObterArqPorId(string chaveCte)
        {
            try
            {
                using (var session = SessionManager.OpenStatelessSession())
                {
                    var query =
                        session.CreateSQLQuery(
                            "SELECT A.ARQUIVO_PDF FROM CTE_ARQUIVO A, CTE C WHERE C.ID_CTE = A.ID_CTE AND C.CHAVE_CTE = :chaveCte");

                    query.SetString("chaveCte", chaveCte);

                    return query.List<byte[]>().FirstOrDefault();
                }
            }
            catch(Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

	    /// <summary>
		/// Insere um Cte arquivo
		/// </summary>
		/// <param name="cte">Cte de refer�ncia</param>
		/// <param name="nomeArquivoPdf">Nome do arquivo pdf</param>
		/// <param name="nomeArquivoXml">Nome do arquivo xml</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		public CteArquivo Inserir(Cte cte, string nomeArquivoPdf, string nomeArquivoXml)
		{
			try
			{
				CteArquivo cteArquivo = new CteArquivo();

				byte[] arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivoPdf);
				cteArquivo.Id = cte.Id;
				cteArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
				arquivoBinario.CopyTo(cteArquivo.ArquivoPdf, 0);
				cteArquivo.TamanhoPdf = arquivoBinario.Length;
				cteArquivo.DataHora = DateTime.Now;

				cteArquivo.ArquivoXml = Tools.ConvertFileToString(nomeArquivoXml);
				cteArquivo.TamanhoXml = cteArquivo.ArquivoXml.Length;
				cteArquivo.DataHoraXml = DateTime.Now;

				return Inserir(cteArquivo);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Insere um Cte arquivo (PDF)
		/// </summary>
		/// <param name="cte">Cte de refer�ncia</param>
		/// <param name="nomeArquivo">Nome do arquivo pdf</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		public CteArquivo InserirOuAtualizarPdf(Cte cte, string nomeArquivo)
		{
			try
			{
				CteArquivo cteArquivo = ObterPorId(cte.Id);
				byte[] arquivoBinario;

				// Caso n�o exista, insere
				if (cteArquivo == null)
				{
					cteArquivo = new CteArquivo();

					arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
					cteArquivo.Id = cte.Id;
					cteArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
					arquivoBinario.CopyTo(cteArquivo.ArquivoPdf, 0);
					cteArquivo.TamanhoPdf = arquivoBinario.Length;
					cteArquivo.DataHora = DateTime.Now;

					return Inserir(cteArquivo);
				}
				
				// Atualiza o Cte
				arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
				cteArquivo.ArquivoPdf = new byte[arquivoBinario.Length];
				arquivoBinario.CopyTo(cteArquivo.ArquivoPdf, 0);
				cteArquivo.TamanhoPdf = arquivoBinario.Length;
				cteArquivo.DataHora = DateTime.Now;
				return Atualizar(cteArquivo);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

        /// <summary>
        /// Atualiza o arquivo da carta de corre��o
        /// </summary>
        /// <param name="cte">Cte de refer�ncia</param>
        /// <param name="nomeArquivo">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto CteArquivo</returns>
        public CteArquivo AtualizarPdfCartaDeCorrecao(Cte cte, string nomeArquivo)
        {
            try
            {
                CteArquivo cteArquivo = ObterPorId(cte.Id);
                byte[] arquivoBinario;

                // Caso n�o exista, insere
                if (cteArquivo == null)
                {
                    cteArquivo = new CteArquivo();

                    arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
                    cteArquivo.Id = cte.Id;
                    cteArquivo.ArquivoPdfCartaDeCorrecao = new byte[arquivoBinario.Length];
                    arquivoBinario.CopyTo(cteArquivo.ArquivoPdfCartaDeCorrecao, 0);
                    cteArquivo.TamanhoPdf = arquivoBinario.Length;
                    cteArquivo.DataHora = DateTime.Now;

                    return Inserir(cteArquivo);
                }
				
                // Atualiza o Cte
                arquivoBinario = Tools.ConvertBinaryFileToByteArray(nomeArquivo);
                cteArquivo.ArquivoPdfCartaDeCorrecao = new byte[arquivoBinario.Length];
                arquivoBinario.CopyTo(cteArquivo.ArquivoPdfCartaDeCorrecao, 0);
                cteArquivo.DataHora = DateTime.Now;
                return Atualizar(cteArquivo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

		/// <summary>
		/// Insere um Cte arquivo (XML)
		/// </summary>
		/// <param name="cte">Cte de refer�ncia</param>
		/// <param name="nomeArquivo">Nome do arquivo Xml</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		public CteArquivo InserirOuAtualizarXml(Cte cte, string nomeArquivo)
		{
			try
			{
				CteArquivo cteArquivo = ObterPorId(cte.Id);
				string conteudoXml = string.Empty;

				// Caso n�o exista, insere
				if (cteArquivo == null)
				{
					cteArquivo = new CteArquivo();

					conteudoXml = Tools.ConvertFileToString(nomeArquivo);
					cteArquivo.Id = cte.Id;
					cteArquivo.ArquivoXml = conteudoXml;
					cteArquivo.TamanhoXml = conteudoXml.Length;
					cteArquivo.DataHoraXml = DateTime.Now;

					return Inserir(cteArquivo);
				}

				// Atualiza o Cte
				conteudoXml = Tools.ConvertFileToString(nomeArquivo);
				cteArquivo.ArquivoXml = conteudoXml;
				cteArquivo.TamanhoXml = conteudoXml.Length;
				cteArquivo.DataHoraXml = DateTime.Now;
				return Atualizar(cteArquivo);
			}
			catch (Exception ex)
			{
				throw;
			}
		}

        /// <summary>
        /// Atualiza o arquivo da carta de corre��o
        /// </summary>
        /// <param name="cte">Cte de refer�ncia</param>
        /// <param name="nomeArquivo">Nome do arquivo Xml</param>
        /// <returns>Retorna o objeto CteArquivo</returns>
        public CteArquivo AtualizarXmlCartaDeCorrecao(Cte cte, string nomeArquivo)
        {
            try
            {
                CteArquivo cteArquivo = ObterPorId(cte.Id);
                string conteudoXml = string.Empty;

                // Caso n�o exista, insere
                if (cteArquivo == null)
                {
                    cteArquivo = new CteArquivo();

                    conteudoXml = Tools.ConvertFileToString(nomeArquivo);
                    cteArquivo.Id = cte.Id;
                    cteArquivo.ArquivoXmlCartaDeCorrecao = conteudoXml;
                    cteArquivo.DataHoraXml = DateTime.Now;

                    return Inserir(cteArquivo);
                }

                // Atualiza o Cte
                conteudoXml = Tools.ConvertFileToString(nomeArquivo);
                cteArquivo.ArquivoXmlCartaDeCorrecao = conteudoXml;
                cteArquivo.DataHoraXml = DateTime.Now;
                return Atualizar(cteArquivo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public IList<CteArquivo> ObterPorIds(List<int> idsCte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"SELECT {ARQ} FROM CteArquivo ARQ
                                 WHERE ARQ.Id IN (:idsCte) ";
                IQuery query = session.CreateQuery(hql);
                query.SetParameterList("idsCte", idsCte);
                IList<CteArquivo> cteArquivos = query.List<CteArquivo>();
                if (cteArquivos.Count > 0)
                {
                    return cteArquivos.ToList();
                }

                return new List<CteArquivo>();
            }
        }


        /// <summary>
        /// Obtem uma lista de arquivos cte de acordo com o ID de Despacho informado
        /// </summary>
        /// <param name="idDespacho">Id do Despacho dos Ctes a serem localizados</param>
        /// <returns>Retorna os arquivos cte</returns>
        public IList<CteArquivo> ObterPorIdDespacho(int idDespacho)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"SELECT {ARQ} FROM CteArquivo ARQ, Cte c
                                 WHERE 
                                    ARQ.Id = c.Id
                                    AND c.Despacho.Id = :idDespacho";
                IQuery query = session.CreateQuery(hql);
                
                query.SetParameter("idDespacho", idDespacho);

                IList<CteArquivo> cteArquivos = query.List<CteArquivo>();
                if (cteArquivos.Count > 0)
                {
                    return cteArquivos.ToList();
                }

                return new List<CteArquivo>();
            }
        }
	}
}