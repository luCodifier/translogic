namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.Diversos.Cte;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using Util;

	/// <summary>
	/// Reposit�rio da classe de CteRunner
	/// </summary>
	public class CteRunnerLogRepository : NHRepository<CteRunnerLog, int?>, ICteRunnerLogRepository 
	{
		/// <summary>
		/// Gets or sets SessionFactory.
		/// </summary>
		public ISessionFactory SessionFactory { get; set; }

		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		public void InserirLogInfo(string hostName, string nomeServico, string mensagem)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				CteRunnerLog runnerLog = new CteRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(mensagem, 250);
				runnerLog.TipoLog = TipoLogCteEnum.Info;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}

		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="builder">String builder do log</param>
		public void InserirLogInfo(string hostName, string nomeServico, StringBuilder builder)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				CteRunnerLog runnerLog = new CteRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(builder.ToString(), 250);
				runnerLog.TipoLog = TipoLogCteEnum.Info;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				 session.Insert(runnerLog);
			}
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		public void InserirLogErro(string hostName, string nomeServico, string mensagem, Exception ex = null)
		{
			try
			{
				using (IStatelessSession session = SessionFactory.OpenStatelessSession())
				{
					CteRunnerLog runnerLog = new CteRunnerLog();
					runnerLog.Descricao = Tools.TruncateString(mensagem, 250);
					runnerLog.TipoLog = TipoLogCteEnum.Erro;
					runnerLog.HostName = hostName;
					runnerLog.NomeServico = nomeServico;
					runnerLog.DataHora = DateTime.Now;
					session.Insert(runnerLog);
				}
			}
			catch { }

			if (ex != null)
			{
				Sis.LogException(ex, string.Format("{0} - {1}", nomeServico, mensagem));
			}
		}

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="builder">String builderdo log</param>
		public void InserirLogErro(string hostName, string nomeServico, StringBuilder builder)
		{
			using (IStatelessSession session = SessionFactory.OpenStatelessSession())
			{
				CteRunnerLog runnerLog = new CteRunnerLog();
				runnerLog.Descricao = Tools.TruncateString(builder.ToString(), 250);
				runnerLog.TipoLog = TipoLogCteEnum.Erro;
				runnerLog.HostName = hostName;
				runnerLog.NomeServico = nomeServico;
				runnerLog.DataHora = DateTime.Now;
				session.Insert(runnerLog);
			}
		}
	}
}