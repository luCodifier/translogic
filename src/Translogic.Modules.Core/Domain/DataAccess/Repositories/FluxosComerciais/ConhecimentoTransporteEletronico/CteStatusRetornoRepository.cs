namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da classe CteStatusRetorno
	/// </summary>
	public class CteStatusRetornoRepository : NHRepository<CteStatusRetorno, int?>, ICteStatusRetornoRepository
	{
        /// <summary>
        /// Obt�m uma lista de Erros da Status Retorno
        /// </summary>
        /// <param name="statusRetornoId">Lista de Ids da CteStatusRetorno para se obter os status detalhados</param>
        /// <returns>Lista CteStatusRetorno</returns>
        public IList<CteStatusRetorno> ObterCteStatusRetornoPorId(string statusRetornoId)
        {
            var hqlQuery =
                @"SELECT P FROM CteStatusRetorno P WHERE P.Id in("+ statusRetornoId +")";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<CteStatusRetorno>();
            }
        }
	}
}