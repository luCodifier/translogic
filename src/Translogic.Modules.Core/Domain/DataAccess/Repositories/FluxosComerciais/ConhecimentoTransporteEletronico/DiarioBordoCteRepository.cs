﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Repositório para <see cref="DiarioBordoCte"/>
    /// </summary>
    public class DiarioBordoCteRepository : NHRepository<DiarioBordoCte, int?>, IDiarioBordoCteRepository
    {
        /// <summary>
        /// Obtém o diário de bordo da Cte por id de Cte
        /// </summary>
        /// <param name="pagination">Detalhes da paginação</param>
        /// <param name="idCte">Id da Cte para se obter o diário de bordo</param>
        /// <returns>Lista de diário de bordo para a Cte</returns>
        public ResultadoPaginado<DiarioBordoCte> ObterDiarioBordoCte(DetalhesPaginacao pagination, int idCte)
        {
            var hqlQuery = @"SELECT {0} FROM DiarioBordoCte D WHERE D.Cte.Id = :idCte";

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(string.Format(hqlQuery, "D"));
                var queryCount = session.CreateQuery(string.Format(hqlQuery, "COUNT(D.Id)"));

                query.SetInt32("idCte", idCte);
                queryCount.SetInt32("idCte", idCte);

                if (pagination != null)
                {
                    if (pagination.Inicio.HasValue)
                    {
                        query.SetFirstResult(pagination.Inicio.Value);
                    }

                    int limit = GetLimit(pagination);

                    query.SetMaxResults(limit > 0 ? limit : DetalhesPaginacao.LimitePadrao);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                var items = query.List<DiarioBordoCte>();
                var total = queryCount.UniqueResult<long>();

                return new ResultadoPaginado<DiarioBordoCte>()
                {
                    Items = items,
                    Total = total
                };
            }
        }

        /// <summary>
        /// Grava um novo registro no diário de bordo para a Cte selecionada
        /// </summary>
        /// <param name="acao">Ação ou mensagem informada</param>
        /// <param name="cte">Cte selecionada</param>
        /// <param name="usuario">Usuario que está inserindo o registro no diário</param>
        /// <returns>Resultado com o sucesso ou não da operação</returns>
        public bool GravarDiarioBordoCte(string acao, Cte cte, Usuario usuario)
        {
            var diarioNovo = new DiarioBordoCte()
                                 {
                                     Acao = acao,
                                     Cte = cte,
                                     DataHora = DateTime.Now,
                                     UsuarioCadastro = usuario
                                 };

            return Inserir(diarioNovo) != null && diarioNovo.Id.HasValue;
        }
    }
}