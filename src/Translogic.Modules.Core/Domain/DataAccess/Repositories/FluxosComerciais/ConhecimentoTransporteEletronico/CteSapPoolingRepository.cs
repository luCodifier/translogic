namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NHibernate;
	using NHibernate.Transform;

	/// <summary>
	/// Classe do reposit�rio do pooling de envio para o SAP
	/// </summary>
	public class CteSapPoolingRepository : NHRepository<CteSapPooling, int?>, ICteSapPoolingRepository
	{
		/// <summary>
		/// Obt�m os itens do pooling para serem enviados para o SAP
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que est�o no pooling</returns>
		public IList<CteSapPooling> ObterListaRecebimentoPorHost(string hostName)
		{
			try
			{
				using (ISession session = OpenSession())
				{
				    string hql = "";

#if !(DEBUG)
                    hql = @"FROM CteSapPooling crp WHERE crp.HostName = :host ORDER BY crp.DataHora";
                    
#else

                    hql = @"FROM CteSapPooling crp WHERE crp.HostName = :host ORDER BY crp.DataHora";
#endif

					IQuery query = session.CreateQuery(hql);
					query.SetString("host", hostName);
					query.SetResultTransformer(Transformers.DistinctRootEntity);

					return query.List<CteSapPooling>();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Limpa a sess�o
		/// </summary>
		public void ClearSession()
		{
			using (ISession session = OpenSession())
			{
				session.Clear();
				session.Flush();
			}
		}

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		public void RemoverPorLista(IList<CteSapPooling> lista)
		{
			string hql = @"DELETE FROM CteSapPooling WHERE Id = :idItem";
			using (ISession session = OpenSession())
			{
				foreach (CteSapPooling item in lista)
				{
					IQuery query = session.CreateQuery(hql);
					query.SetInt32("idItem", item.Id.Value);
					query.ExecuteUpdate();
				}
			}
		}
	}
}