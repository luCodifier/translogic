namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Reposit�rio da interface de importa��o dos aquivos pdf
    /// </summary>
    public class CteInterfacePdfConfigRepository : NHRepository<CteInterfacePdfConfig, int?>, ICteInterfacePdfConfigRepository
    {
        /// <summary>
        /// Obt�m lista dos itens da interface pelo Host
        /// </summary>
        /// <param name="hostName">Nome do Servidor</param>
        /// <returns>Retorna a lista dos itens da interface</returns>
        public IList<CteInterfacePdfConfig> ObterListaInterfacePorHost(string hostName)
        {
            DateTime dataMinima = DateTime.Now;
            dataMinima = dataMinima.AddDays(-5);

            try
            {
                using (ISession session = OpenSession())
                {
                    string hql = @"FROM CteInterfacePdfConfig cic  
											WHERE cic.HostName = :hostName 
											AND cic.DataHora >= :DataMinima
											ORDER BY cic.DataUltimaLeitura
";

#if DEBUG
                    if (Environment.MachineName.ToUpper() == "PULSAR")
                    {
                        hostName = "CRW-VWPS01";
                    }
#endif

                    IQuery query = session.CreateQuery(hql);
                    query.SetString("hostName", hostName);
                    query.SetDateTime("DataMinima", dataMinima);
                    query.SetMaxResults(50);
                    query.SetResultTransformer(Transformers.DistinctRootEntity);

                    return query.List<CteInterfacePdfConfig>();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}