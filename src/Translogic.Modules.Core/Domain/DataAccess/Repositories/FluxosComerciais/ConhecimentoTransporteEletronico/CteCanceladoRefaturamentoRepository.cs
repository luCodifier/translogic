namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ALL.Core.AcessoDados;

    using NHibernate;
    using NHibernate.Criterion;

    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    ///     Classe de Cte Cancelado Refaturamento
    /// </summary>
    public class CteCanceladoRefaturamentoRepository : NHRepository<CteCanceladoRefaturamento, int?>,
                                                       ICteCanceladoRefaturamentoRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obtem os ctes cancelados para refaturamento da base consolidada
        /// </summary>
        /// <param name="data">Data do Cte</param>
        /// <returns>Lista de Ctes Cancelados</returns>
        public IList<CteCanceladoRefaturamento> ObterCtesCanceladosRefaturamentoBaseConsolidadaPorData(DateTime data)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Data", data));
            criteria.SetFetchMode("ListaNotas", FetchMode.Join);

            return ObterTodos(criteria);
        }

        /// <summary>
        ///     Obtem o �ltimo cte cancelado para refaturamento da base consolidada
        /// </summary>
        /// <returns>�ltimo Cte Cancelado</returns>
        public CteCanceladoRefaturamento ObterUltimoCteCanceladoRefaturamentoBaseConsolidada()
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.AddOrder(Order.Desc("Data"));
            criteria.SetFirstResult(1);
            criteria.SetMaxResults(1);

            return ObterTodos(criteria).FirstOrDefault();
        }

        #endregion
    }
}