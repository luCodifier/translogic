namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Repositorio da classe de envio de email com erro
    /// </summary>
    public class CteEnvioEmailErroRepository : NHRepository<CteEnvioEmailErro, int?>, ICteEnvioEmailErroRepository
    {
        /// <summary>
        /// Remove os CteEnvioEmail com Erro
        /// </summary>
        /// <param name="idCtes">id dos Cte</param>
        public void RemoverTodosPorCte(int[] idCtes)
        {
            string hql = @"  DELETE FROM CteEnvioEmailErro c WHERE c.Cte.Id in ( :IdCtes ) ";
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetParameterList("IdCtes", idCtes);
                query.ExecuteUpdate();
            }
        }
    }
}