﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Simconsultas;
    using Model.FluxosComerciais.Simconsultas.Repositories;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

    /// <summary>
    /// Classe de repositório do NFE Simconsultas
    /// </summary>
    public class NfeSimconsultasRepository : NHRepository<NfeSimconsultas, int?>, INfeSimconsultasRepository
    {
        /// <summary>
        /// Obtem pela chave nfe
        /// </summary>
        /// <param name="chaveNfe">campo Chave nfe</param>
        /// <returns>Retorna a nfe do simConsultas</returns>
        public NfeSimconsultas ObterPorChaveNfe(string chaveNfe)
        {
            ////HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
            using (ISession session = OpenSession())
            {
                string hql = @"FROM NfeSimconsultas nfe WHERE nfe.ChaveNfe = :chavesNfe ";
                IQuery query = session.CreateQuery(hql);
                query.SetString("chavesNfe", chaveNfe);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                var lista = query.List<NfeSimconsultas>();

                if (lista != null && lista.Count > 0)
                {
                    return lista[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Verifica se existe chave nfe
        /// </summary>
        /// <param name="chaveNfe">campo Chave nfe</param>
        /// <returns>True/False</returns>
        public bool ExisteChaveNfe(string chaveNfe)
        {
            var sql = "nfe_simconsultas ns where ns.nfe_chave = :chaveNfe";

            using (var session = OpenSession())
            {
                var queryExiste = session.CreateSQLQuery(string.Format(@"SELECT 1 FROM {0}", sql));
                queryExiste.SetParameter("chaveNfe", chaveNfe);

                decimal? existe = queryExiste.UniqueResult<decimal>();

                return (existe == 1 ? true : false);
            }
        }

        /// <summary>
        /// recupera o tipo de operacao (entrada ou saída) da nfe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>tipo de operacao (entrada ou saída)</returns>
        public object ObterTipoNotaFiscal(string chaveNfe)
        {
            var sql = "select nfe_tpnf from nfe_simconsultas ns where ns.nfe_chave = :chaveNfe";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetParameter("chaveNfe", chaveNfe);

                var result = query.UniqueResult();

                return result;
            }
        }

        /// <summary>
        /// Obtem pela lista de chave nfe
        /// </summary>
        /// <param name="chavesNfe">lista com as chaves da nfe</param>
        /// <returns>Lista de NfeSimConsulta</returns>
        public IList<NfeSimconsultas> ObterPorChavesNfe(IList<string> chavesNfe)
        {
            using (ISession session = OpenSession())
            {
                var parametros = new List<Action<IQuery>>();
                var hql = @"FROM NfeSimconsultas nfe WHERE nfe.ChaveNfe IN (:chavesNfe) ";

                var query = session.CreateQuery(hql);

                parametros.Add(q => q.SetParameterList("chavesNfe", chavesNfe));
                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);
                var lista = query.List<NfeSimconsultas>();
                return lista;
            }
        }

        public string ObterTagXml(string chaveNfe, string tag)
        {
            var sql = string.Format(@"
                
                        SELECT NFE.NFE_XML AS NFE_XML
                          FROM EDI.EDI2_NFE NFE
                         WHERE NFE.NFE_CHAVE = '{0}'
                
            ", chaveNfe);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                var valor = query.UniqueResult();
                return valor != null ? valor.ToString() : string.Empty;
            }
        }
    }
}