﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Simconsultas;
	using Model.FluxosComerciais.Simconsultas.Repositories;

	/// <summary>
	/// Classe de repositório do LogSimconsultas
	/// </summary>
	public class LogSimconsultasRepository : NHRepository<LogSimconsultas, int>, ILogSimconsultasRepository
	{
	}
}