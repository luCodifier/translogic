﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;

	/// <summary>
	/// Classe de repositório do CteDocumentoRemetenteSimconsultas
	/// </summary>
	public class CteDocumentoRemetenteSimconsultasRepository : NHRepository<CteDocumentoRemetenteSimconsultas, int?>, ICteDocumentoRemetenteSimconsultasRepository
	{
	}
}