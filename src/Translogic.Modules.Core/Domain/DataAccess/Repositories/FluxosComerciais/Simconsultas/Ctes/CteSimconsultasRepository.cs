﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

	using NHibernate;
	using NHibernate.Criterion;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;

	/// <summary>
	/// Classe de repositório do CTe Simconsultas
	/// </summary>
	public class CteSimconsultasRepository : NHRepository<CteSimconsultas, int?>, ICteSimconsultasRepository
	{
		/// <summary>
		/// Obtém o cte pela chave
		/// </summary>
		/// <param name="chaveCte">Chave do Cte</param>
		/// <returns>Objeto CteSimconsultas</returns>
		public CteSimconsultas ObterPorChaveCte(string chaveCte)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ChaveCte", chaveCte));
			return this.ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obtém os que não foram baixados
		/// </summary>
		/// <returns>Lista de chaves que não foram baixados</returns>
		public IList<string> ObterNaoBaixados()
		{
			using (ISession session = this.OpenSession())
			{
				string sql = "select DISTINCT * from cte_simconsultas_processar c1 where not exists(select 1 from cte_simconsultas cs where cs.chave_cte = c1.chave_cte)";
				var query = session.CreateSQLQuery(sql);
				// query.SetMaxResults(500);
				return query.List<string>();
			}
		}

		/// <summary>
		/// Obtém os que não foram processados
		/// </summary>
		/// <returns>Lista de chaves que não foram processados</returns>
		public IList<CteSimconsultas> ObterNaoProcessados()
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("IndProcessado", false)); // && Restrictions.Like("CteXml", "<CTe%"));
			// criteria.SetMaxResults(1000);
			return this.ObterTodos(criteria);
		}
	}
}