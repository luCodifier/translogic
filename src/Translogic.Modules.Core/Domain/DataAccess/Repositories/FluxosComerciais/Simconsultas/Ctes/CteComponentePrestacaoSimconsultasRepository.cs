﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;

	/// <summary>
	/// Classe de repositório do CteComponentePrestacaoSimconsultas
	/// </summary>
	public class CteComponentePrestacaoSimconsultasRepository : NHRepository<CteComponentePrestacaoSimconsultas, int?>, ICteComponentePrestacaoSimconsultasRepository
	{
	}
}