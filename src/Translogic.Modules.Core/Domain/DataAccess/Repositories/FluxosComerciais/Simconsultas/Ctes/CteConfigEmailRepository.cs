﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes
{
    using System.Linq;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories;

    /// <summary>
    /// Implementação do repositório da CteConfigEmail
    /// </summary>
    public class CteConfigEmailRepository : NHRepository<CteConfigEmail, int?>, ICteConfigEmailRepository
    {
        /// <summary>
        /// Obtém uma CteConfigEmail pela Cte informada
        /// </summary>
        /// <param name="cte">Cte que possui empresa tomadora</param>
        /// <returns>Configuração que indica se a empresa recebe corpo no email ou somente o anexo</returns>
        public CteConfigEmail ObterPorCte(Cte cte)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"FROM CteEmpresas ce
									INNER JOIN FETCH ce.Cte c, CteConfigEmail cc
                  WHERE ce.EmpresaTomadora.Id = cc.Id 
                        AND c = :cte
                  ORDER BY c.Id DESC
                ";

                IQuery query = session.CreateQuery(hql);
                query.SetParameter("cte", cte);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                var result = query.List<CteConfigEmail>();

                return result.FirstOrDefault();
            }
        }
    }
}