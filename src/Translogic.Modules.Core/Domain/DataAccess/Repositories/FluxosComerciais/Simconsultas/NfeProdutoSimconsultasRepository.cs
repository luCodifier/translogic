namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Simconsultas;
    using Model.FluxosComerciais.Simconsultas.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
	/// Repositório da classe NFE produto Simconsultas
	/// </summary>
	public class NfeProdutoSimconsultasRepository : NHRepository<NfeProdutoSimconsultas, int?>, INfeProdutoSimconsultasRepository
	{
        /// <summary>
        /// Obter a lista de produtos de uma NfeSimConsultas
        /// </summary>
        /// <param name="nfeSimConsultasId">Id da NfeSimConsultas que se deseja obter a lista de produtos</param>
        /// <returns>Lista de produtos da Nfe informada</returns>
	    public IList<NfeProdutoSimconsultas> ObterPorNfeId(int nfeSimConsultasId)
	    {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"FROM NfeProdutoSimconsultas NPS
                         WHERE NPS.NfeSimconsultas.Id = :nfeSimConsultasId
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetInt32("nfeSimConsultasId", nfeSimConsultasId));

                var query = session.CreateQuery(hql.ToString());

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                return query.List<NfeProdutoSimconsultas>();
            }
	    }
	}
}