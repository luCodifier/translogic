﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Simconsultas;
	using Model.FluxosComerciais.Simconsultas.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Classe de repositório do NFE Simconsultas
	/// </summary>
	public class NfeAnulacaoSimconsultasRepository : NHRepository<NotaFiscalAnulacao, int?>, INfeAnulacaoSimconsultasRepository
	{
		/// <summary>
		/// Obtem pela chave nfe
		/// </summary>
		/// <param name="chaveNfe">campo Chave nfe</param>
		/// <returns>Retorna a nfe do simConsultas</returns>
		public NotaFiscalAnulacao ObterPorChaveNfe(string chaveNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("ChaveNfe", chaveNfe));
			return ObterPrimeiro(criteria);
		}
	}
}