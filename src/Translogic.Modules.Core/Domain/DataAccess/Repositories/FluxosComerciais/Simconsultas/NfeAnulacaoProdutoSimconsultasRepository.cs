namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas
{
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.Simconsultas;
	using Model.FluxosComerciais.Simconsultas.Repositories;

	/// <summary>
	/// Repositório da classe NFE produto Simconsultas
	/// </summary>
	public class NfeAnulacaoProdutoSimconsultasRepository : NHRepository<NotaFiscalAnulacaoProduto, int?>, INfeAnulacaoProdutoSimconsultasRepository
	{
	}
}