namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais;
	using Model.FluxosComerciais.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Core.Infrastructure;

	/// <summary>
	/// Classe do reposit�rio de contrato
	/// </summary>
	public class ContratoRepository : NHRepository<Contrato, int?>, IContratoRepository
	{
		/// <summary>
		/// Obtem o contrato pelo fluxo e pela data
		/// </summary>
		/// <param name="codigoFluxoComercial">C�digo do fluxo comercial</param>
		/// <param name="dataVigencia">Data de vig�ncia</param>
		/// <returns>Objeto contrato</returns>
		public Contrato ObterPorFluxoData(string codigoFluxoComercial, DateTime dataVigencia)
		{
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();
			DateTime dataTmp = new DateTime(dataVigencia.Year, dataVigencia.Month, dataVigencia.Day);
			StringBuilder sb = new StringBuilder();
			sb.Append(
				@"FROM Contrato c
						WHERE c.CodigoFluxoComercial = :codigoFluxo
						AND :dataVigencia BETWEEN c.InicioVigencia AND c.TerminoVigencia");

			parametros.Add(q => q.SetString("codigoFluxo", codigoFluxoComercial));
			parametros.Add(q => q.SetDateTime("dataVigencia", dataTmp));
			
			ISession session = OpenSession();

			IQuery query = session.CreateQuery(sb.ToString());
			query.SetResultTransformer(Transformers.DistinctRootEntity);

			foreach (Action<IQuery> action in parametros)
			{
				action.Invoke(query);
			}

			IList<Contrato> result = query.List<Contrato>();
			if (result != null && result.Count == 1)
			{
				return result[0];
			}

			if (result != null && result.Count > 1)
			{
				throw new TranslogicException("O fluxo comercial possui dois contratos vigentes para a data({0})", dataVigencia.ToString("dd/MM/yyyy"));
			}

			return null;
		}
	}
}