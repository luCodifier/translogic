﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="WsTicketLogRecebimento"/>
    /// </summary>
    public class WsTicketLogRecebimentoRepository : NHRepository<WsTicketLogRecebimento, int>, IWsTicketLogRecebimentoRepository
    {
    }
}