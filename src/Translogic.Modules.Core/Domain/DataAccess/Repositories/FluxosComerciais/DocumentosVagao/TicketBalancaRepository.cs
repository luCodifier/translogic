﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

    /// <summary>
    /// Repositorio para <see cref="TicketBalanca"/>
    /// </summary>
    public class TicketBalancaRepository : NHRepository<TicketBalanca, int?>, ITicketBalancaRepository
    {
        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="idComposicao">id da composição</param>
        /// <param name="vagoes">vagoes a serem filtrados </param>
        /// <returns>Lista de tickets</returns>
        public IList<decimal> ObterNotasTicketsPorVagao(int idComposicao, IList<int> vagoes)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"
                        WITH TAB_DS_TEMP
                        AS 
                        (
                             SELECT MAX(EV0.EA_DTO_EVG) AS DT_EVENTO,
                                   CP0.CP_DAT_INC AS DT_INI_COMP,
                                   CP0.CP_DAT_FIM,
                                   CV0.VG_ID_VG AS ID_VAGAO
                              FROM COMPVAGAO CV0, COMPOSICAO CP0, EVENTO_VAGAO EV0
                             WHERE CV0.CP_ID_CPS = :idComposicao
                               AND CV0.CP_ID_CPS = CP0.CP_ID_CPS
                               AND EV0.VG_ID_VG = CV0.VG_ID_VG
                               AND EV0.EA_DTO_EVG < CP0.CP_DAT_INC
                               AND EV0.EN_ID_EVT = 13
                             GROUP BY CP0.CP_DAT_INC, CV0.VG_ID_VG,CP0.CP_DAT_FIM
                        )SELECT NTV.ID
                          FROM VAGAO V,
                               COMPVAGAO CV,
                               DESPACHO DS,
                               ITEM_DESPACHO IDS,       
                               CARREGAMENTO CG,
                               DETALHE_CARREGAMENTO DC,
                               TAB_DS_TEMP,
                               nota_ticket_vagao ntv,
                               vagao_ticket vt,
                               ticket_balanca tb,
                               nota_fiscal nf,
                               ConfiguracaoTranslogic ct
                         WHERE V.VG_ID_VG = CV.VG_ID_VG
                           AND DS.DP_ID_DP = IDS.DP_ID_DP
                           AND DS.DP_DT BETWEEN TAB_DS_TEMP.DT_EVENTO AND TAB_DS_TEMP.DT_INI_COMP
                           AND IDS.VG_ID_VG = TAB_DS_TEMP.ID_VAGAO
                           AND IDS.VG_ID_VG = CV.VG_ID_VG
                           AND DS.DP_ID_DP = IDS.DP_ID_DP 
                           AND DC.DC_ID_CRG = IDS.DC_ID_CRG
                           AND CG.CG_ID_CAR = DC.CG_ID_CAR
                           AND ct.Id = 'INTERVALO_DIAS_TICKET_TSP'
                           AND ntv.vagao_ticket_id = vt.id
                           and vt.ticket_balanca_id = tb.id
                           and nf.nfe_chave_nfe = ntv.chave_nfe
                           AND DS.DP_ID_DP = NF.DP_ID_DP
                           and v.vg_cod_vag = lpad(vt.vg_cod_vag, 7, '0')
                           AND CV.CP_ID_CPS = :idComposicao
                           AND V.VG_ID_VG  IN (:vagoes)
                           AND ((vt.DataCadastro BETWEEN TRUNC(DS.DataDespacho - 3) AND TRUNC(DS.DataDespacho + 3))
                                OR 
                                (vt.DataCadastro BETWEEN TRUNC(DS.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(DS.DataDespacho + NVL(ct.Valor,3))))                           
			");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameter("idComposicao", idComposicao));
                parametros.Add(q => q.SetParameterList("vagoes", vagoes));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<decimal>();
            }
        }

        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="itensDespacho">itens de despacho a serem filtrados </param>
        /// <returns>Lista de tickets</returns>
        public IList<NotaTicketVagao> ObterNotasTicketItensDespacho(IList<int> itensDespacho)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"SELECT distinct
                            ntv
                          FROM  Vagao v,                                                 
                                VagaoTicket vt,                                                 
                                NotaTicketVagao ntv,                                 
                                NotaFiscalTranslogic n,
                                ItemDespacho idc,
                                DespachoTranslogic dp,
                                VagaoPedidoVigente vpv,
                                DetalheCarregamento dc, 
                                ConfiguracaoTranslogic ct       
                         WHERE LPAD(vt.CodigoVagao,7,'0') = v.Codigo
                           AND vt.Id = ntv.Vagao.Id 
                           AND ntv.ChaveNfe = n.ChaveNotaFiscalEletronica
                           AND idc.DespachoTranslogic.Id = n.DespachoTranslogic.Id
                           AND (idc.Vagao.Id = v.Id)
                           AND idc.DespachoTranslogic.Id = dp.Id
                           AND dc.Id = idc.DetalheCarregamento.Id
                           AND vpv.Vagao.Id = idc.Vagao.Id
                           AND vpv.Carregamento.Id = dc.Carregamento.Id
                           AND ct.Id = 'INTERVALO_DIAS_TICKET_TSP'
                           AND idc.Id IN (:itensDespacho)                           
                           AND ((vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - 3) AND TRUNC(dp.DataDespacho + 3) AND vpv.VagaoPedidoOperacional IS NULL)
                                OR 
                                (vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(dp.DataDespacho + NVL(ct.Valor,3)) AND vpv.VagaoPedidoOperacional IS NOT NULL))
            ");



            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespacho", itensDespacho));

                var query = session.CreateQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<NotaTicketVagao>();
            }
        }


        public IList<NotaTicketVagaoClienteDto> ObterNotasTicketVagaoClienteItensDespacho(IList<int> itensDespacho, bool historico)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@"
                SELECT DISTINCT
                       NTV.ID           AS ""NotaTicketVagaoId""
                     , EMP.EP_DSC_DTL   AS ""Cliente""
                     , IDC.ID_ID_ITD    AS ""IdItemDespacho""
                  FROM VAGAO                  V
                  JOIN VAGAO_TICKET           VT      ON LPAD(VT.VG_COD_VAG, 7, '0') = V.VG_COD_VAG
                  JOIN NOTA_TICKET_VAGAO      NTV     ON NTV.VAGAO_TICKET_ID = VT.ID
                  JOIN NOTA_FISCAL            N       ON N.NFE_CHAVE_NFE = NTV.CHAVE_NFE AND N.VG_ID_VG = V.VG_ID_VG
                  JOIN ITEM_DESPACHO          IDC     ON IDC.DP_ID_DP = N.DP_ID_DP AND IDC.VG_ID_VG = V.VG_ID_VG
                  JOIN DESPACHO               DP      ON DP.DP_ID_DP = IDC.DP_ID_DP
                  JOIN DETALHE_CARREGAMENTO   DC      ON DC.DC_ID_CRG = IDC.DC_ID_CRG
                  JOIN CARREGAMENTO           CG      ON DC.CG_ID_CAR = CG.CG_ID_CAR
                  JOIN FLUXO_COMERCIAL        FC      ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                  JOIN EMPRESA                EMP     ON EMP.EP_ID_EMP = FC.EP_ID_EMP_REM                  
                  {0}
                  LEFT JOIN CONF_GERAL        CG      ON CG.CHAVE = 'INTERVALO_DIAS_TICKET_TSP'
                 WHERE IDC.ID_ID_ITD IN {ItensDespachoIds}                   
                   {1}
                 ORDER BY IDC.ID_ID_ITD ASC
                        , NTV.ID        ASC
            ");

            if (!historico)
            {
                hql = hql.Replace("{0}",
                                  @"JOIN VAGAO_PEDIDO_VIG       VPV     ON VPV.VG_ID_VG = IDC.VG_ID_VG
                                                      AND VPV.CG_ID_CAR = CG.CG_ID_CAR");
                hql = hql.Replace("{1}",
                                  @"AND ((VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3) AND VPV.VO_IDT_VPT_OPER IS NULL)
                        OR 
                        (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CG.VALOR,3)) AND TRUNC(DP.DP_DT + NVL(CG.VALOR,3)) AND VPV.VO_IDT_VPT_OPER IS NOT NULL))");
            }
            else
            {
                hql = hql.Replace("{0}", string.Empty);
                hql = hql.Replace("{1}",
                                  @"AND ((VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - 3) AND TRUNC(DP.DP_DT + 3))
                        OR 
                        (VT.DATA_CADASTRO BETWEEN TRUNC(DP.DP_DT - NVL(CG.VALOR,3)) AND TRUNC(DP.DP_DT + NVL(CG.VALOR,3))))");
            }

            if (itensDespacho.Count > 0)
            {

                string idsList = "";
                foreach (int i in itensDespacho)
                {
                    if (String.IsNullOrEmpty(idsList))
                    {
                        idsList = "" + i.ToString() + "";
                    }
                    else
                    {
                        idsList = idsList + ", " + i.ToString() + "";
                    }
                }

                if (!(String.IsNullOrEmpty(idsList)))
                {
                    hql.Replace("{ItensDespachoIds}", "( " + idsList + ")");
                }
                else
                {
                    hql.Replace("{ItensDespachoIds}", "IN (0)");
                }
            }
            else
            {
                hql.Replace("{ItensDespachoIds}", "IN (0)");
            }


            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<NotaTicketVagaoClienteDto>());
                var itens = query.List<NotaTicketVagaoClienteDto>();
                var itensAgrupados = itens.GroupBy(r => new
                {
                    r.IdItemDespacho,
                    r.Cliente
                })
                                          .Select(r => r.First())
                                          .ToList();

                return itensAgrupados;
            }
        }

        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="itensDespacho">itens de despacho a serem filtrados </param>
        /// <param name="historico">informa se a pesquisa esta sendo feita pela tela de histórico devido o Fat2.0</param>
        /// <returns>Lista de tickets</returns>
        public IList<ItemDespachoNotaTicketVagaoDto> ObterNotasTicketItensDespachoDto(IList<int> itensDespacho, bool historico)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            ////HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            hql.Append(@"SELECT distinct
                            idc.Id as IdItemDespacho,
                            v.Serie.Codigo AS SerieVagao,
                            ntv as NotaTicketVagao
                          FROM  Vagao v,
                                VagaoTicket vt,
                                NotaTicketVagao ntv,
                                NotaFiscalTranslogic n,
                                ItemDespacho idc,
                                DespachoTranslogic dp,
                                {0}  
                                ConfiguracaoTranslogic ct
                         WHERE LPAD(vt.CodigoVagao, 7, '0') = v.Codigo
                           AND ntv.Vagao.Id = vt.Id
                           AND n.ChaveNotaFiscalEletronica = ntv.ChaveNfe AND n.Vagao.Id = v.Id
                           AND idc.DespachoTranslogic.Id = n.DespachoTranslogic.Id AND idc.Vagao.Id = v.Id
                           AND dp.Id = idc.DespachoTranslogic.Id
                           AND idc.Id IN (:itensDespacho)
                           {1}
                           AND ct.Id = 'INTERVALO_DIAS_TICKET_TSP'
                           {2}
                         ORDER BY idc.Id DESC
                                , ntv.Id DESC
            ");

            if (!historico)
            {
                hql = hql.Replace("{0}", @"VagaoPedidoVigente vpv, 
                                DetalheCarregamento dc,");
                hql = hql.Replace("{1}", @"AND dc.Id = idc.DetalheCarregamento.Id
                           AND vpv.Vagao.Id = idc.Vagao.Id
                           AND vpv.Carregamento.Id = dc.Carregamento.Id");
                hql = hql.Replace("{2}",
                                  @"AND ((vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - 3) AND TRUNC(dp.DataDespacho + 3) AND vpv.VagaoPedidoOperacional IS NULL)
                                OR 
                                (vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(dp.DataDespacho + NVL(ct.Valor,3)) AND vpv.VagaoPedidoOperacional IS NOT NULL AND vt.PesoLiquido=(idc.PesoCalculo*100)))");
                // (vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(dp.DataDespacho + NVL(ct.Valor,3)) AND vpv.VagaoPedidoOperacional IS NOT NULL))");
            }
            else
            {
                hql = hql.Replace("{0}", string.Empty);
                hql = hql.Replace("{1}", string.Empty);
                hql = hql.Replace("{2}",
                                  @"AND ( (vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - 3) AND TRUNC(dp.DataDespacho + 3))
                                OR 
                                (vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(dp.DataDespacho + NVL(ct.Valor,3))) AND vt.PesoLiquido=(idc.PesoCalculo*100) )");
                //(vt.DataCadastro BETWEEN TRUNC(dp.DataDespacho - NVL(ct.Valor,3)) AND TRUNC(dp.DataDespacho + NVL(ct.Valor,3))))");
            }

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespacho", itensDespacho));

                IQuery query = session.CreateQuery(string.Format(hql.ToString()))
                    .SetResultTransformer(Transformers.AliasToBean<ItemDespachoNotaTicketVagaoDto>());

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                var resultados = query.List<ItemDespachoNotaTicketVagaoDto>();
                var resultadosAgrupados = resultados.GroupBy(r => new
                {
                    r.IdItemDespacho,
                    r.NotaTicketVagao.ChaveNfe
                })
                                                    .Select(r => r.First())
                                                    .ToList();

                return resultadosAgrupados;
            }
        }
    }
}