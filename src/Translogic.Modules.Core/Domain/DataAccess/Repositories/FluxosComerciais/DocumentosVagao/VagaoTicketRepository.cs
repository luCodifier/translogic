﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoTicket"/>
    /// </summary>
    public class VagaoTicketRepository : NHRepository<VagaoTicket, int>, IVagaoTicketRepository
    {
        public VagaoTicketDto GetTaraByIdVagao(string codigoVagao)
        {
            using (ISession session = OpenSession())
            {
                var sql = string.Format(@"SELECT CAST(VG_COD_VAG AS VARCHAR(20))       AS ""CodigoVagao"",
                                                 CAST(TB AS VARCHAR(20))               AS ""PesoBruto"",   
                                                 CAST(TARA AS VARCHAR(20))             AS ""PesoTara"",
                                                 CAST(TU AS VARCHAR(20))               AS ""PesoLiquido"" 
                                           FROM (SELECT VT.VG_COD_VAG,
                                                        VT.TB,
                                                        CASE 
                                                            WHEN VT.AJUSTE_TRG = 'S' THEN 
                                                            (VT.TARA / 100) 
                                                               ELSE
                                                                  (VT.TARA / 1000) 
                                                             END
                                                             AS TARA,
                                                         VT.TU
                                                   FROM VAGAO_TICKET VT,
                                                        VAGAO V,
                                                        DESPACHO DS, 
                                                        ITEM_DESPACHO IDS, 
                                                        CONF_GERAL CF
                                                  WHERE V.VG_ID_VG = '{0}'
                                                    AND V.VG_COD_VAG = lpad(VT.VG_COD_VAG, 7, '0')
                                                    AND DS.DP_ID_DP = IDS.DP_ID_DP
                                                    AND IDS.VG_ID_VG = V.VG_ID_VG
                                                    AND CF.chave = 'INTERVALO_DIAS_TICKET_TSP'
                                                    AND ((VT.Data_Cadastro BETWEEN TRUNC(DS.DP_DT - 3) AND TRUNC(DS.DP_DT + 3))
                                                    OR (VT.DATA_CADASTRO BETWEEN TRUNC(DS.DP_DT - NVL(CF.VALOR,3)) AND TRUNC(DS.DP_DT + NVL(CF.VALOR,3)) AND (VT.TU=IDS.ID_NUM_PCL*100))) 
                                              ORDER BY VT.DATA_CADASTRO DESC)
                                            WHERE ROWNUM = 1", codigoVagao);
                IQuery query = session.CreateSQLQuery(sql);
                query.SetResultTransformer(Transformers.AliasToBean<VagaoTicketDto>());
                IList<VagaoTicketDto> lista = query.List<VagaoTicketDto>();
                if (lista.Count > 0)
                {
                    return lista[0];
                }
                return null;
            }
        }
    }
}