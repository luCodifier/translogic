﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="VagaoTicket"/>
    /// </summary>
    public class NotaTicketVagaoRepository : NHRepository<NotaTicketVagao, int>, INotaTicketVagaoRepository
    {
        /// <summary>
        /// Retorna as notas do ticket vagao
        /// </summary>
        /// <param name="listaIds">Lista de Ids para filtro</param>
        /// <returns>Objeto NotaTicketVagao</returns>
        public List<NotaTicketVagao> ObterTodos(IList<decimal> listaIds)
        {
            List<Action<IQuery>> parametros = new List<Action<IQuery>>();
            StringBuilder hql = new StringBuilder();

            hql.Append(@"
                        From  NotaTicketVagao ntv where ntv.Id in (:ids)
                       ");

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("ids", listaIds));

                var query = session.CreateQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<NotaTicketVagao>().ToList();
            }
        }
    }
}