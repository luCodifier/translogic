﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Domain.Model.Dto;
using System.Text;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    public class DespachoLocalBloqueioRepository : NHRepository<DespachoLocalBloqueio, int>,  IDespachoLocalBloqueioRepository
    {

        /// <summary>
        /// Lista de Bloqueios
        /// </summary>
        /// <returns>Lista de Bloqueios</returns>
        public IList<DespachoLocalBloqueioDto> ObterTodosBloqueios(DetalhesPaginacao detalhesPaginacao)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@" SELECT distinct
                                              blo.db_id_db as ""Id"",
                                              ao.Ao_Id_Ao as ""IdEstacao"",
                                              ao.Ao_Cod_Aop as ""CodigoEstacao"",
                                              ao.Ao_Dsc_Aop as ""DescricaoEstacao"",
                                              blo.Db_Cod_Segmento as ""Segmento"",
                                              blo.Db_Cnpj_Raiz as ""CnpjRaiz"",
                                              (Select emp.Ep_Dsc_Rsm 
                                              from Empresa emp  
                                               where emp.ep_cgc_emp like CONCAT(CONCAT('%', blo.db_cnpj_raiz ),'%')
                                               and blo.db_cnpj_raiz is not null
                                               and rownum = 1) as ""Cliente"",
                                               blo.data_cadastro as ""DataBloqueio"",
                                               usu.us_nom_usu as ""UsuarioBloqueio""
                                            FROM  Despacho_Local_Bloqueio blo
                                                  INNER JOIN Area_Operacional ao on ao.Ao_Id_Ao = blo.db_est_id
                                                  LEFT JOIN Usuario usu on usu.us_idt_usu = blo.usu_id_cadastro
                                            {Ordenacao}

                                            ");


            if (detalhesPaginacao != null)
            {
                // Order by (Recupera o indice e o direcionamento (ASC/DESC))
                var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
                var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
                string colOrdenacaoIndex = "3";
                string ordenacaoDirecao = "ASC";

                if (colunasOrdenacao.Count > 0)
                    colOrdenacaoIndex = colunasOrdenacao[0];
                if (direcoesOrdenacao.Count > 0)
                    ordenacaoDirecao = direcoesOrdenacao[0];

                if (colOrdenacaoIndex.ToLower() == "estacaocodigo")
                    colOrdenacaoIndex = "3";
                else if (colOrdenacaoIndex.ToLower() == "estacaodescricao")
                    colOrdenacaoIndex = "4";
                else if (colOrdenacaoIndex.ToLower() == "segmento")
                    colOrdenacaoIndex = "5";
                else if (colOrdenacaoIndex.ToLower() == "cnpjraiz")
                    colOrdenacaoIndex = "6";
                else if (colOrdenacaoIndex.ToLower() == "cliente")
                    colOrdenacaoIndex = "7";
                else
                {
                    colOrdenacaoIndex = "3";
                    ordenacaoDirecao = "ASC";
                }

                hql.Replace("{Ordenacao}", " ORDER BY " + colOrdenacaoIndex + " " + ordenacaoDirecao);
            }
            else
            {
                hql.Replace("{Ordenacao}", String.Empty);
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<DespachoLocalBloqueioDto>());
                var itens = query.List<DespachoLocalBloqueioDto>();


                return itens;
            }

        }
    }
}