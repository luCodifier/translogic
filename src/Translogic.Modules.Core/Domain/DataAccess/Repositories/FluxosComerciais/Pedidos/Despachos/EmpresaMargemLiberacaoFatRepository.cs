namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
	using ALL.Core.AcessoDados;
	using NHibernate.Criterion;
	using Translogic.Modules.Core.Domain.Model.Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

	/// <summary>
	/// Repositorio para <see cref="EmpresaMargemLiberacaoFat"/>
	/// </summary>
	public class EmpresaMargemLiberacaoFatRepository : NHRepository<EmpresaMargemLiberacaoFat, int?>, IEmpresaMargemLiberacaoFatRepository
	{
		/// <summary>
		/// Pesquisa pela empresa
		/// </summary>
		/// <param name="empresaCliente">Filtro Empresa Cliente</param>
		/// <returns>Retorna a empresa, ou null caso n�o encontre</returns>
		public EmpresaMargemLiberacaoFat ObterPorEmpresa(EmpresaCliente empresaCliente)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Empresa", empresaCliente));

			return ObterPrimeiro(criteria);
		}
	}
}