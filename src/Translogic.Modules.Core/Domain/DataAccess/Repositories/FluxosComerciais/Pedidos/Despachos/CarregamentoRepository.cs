namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="Carregamento"/>
	/// </summary>
	public class CarregamentoRepository : NHRepository<Carregamento, int?>, ICarregamentoRepository
	{
        /// <summary>
        /// Retorna id da estacao mae 
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>retorna id da estacao mae</returns>
        public string ObterEstacaoMaePorDespacho(int despacho, string serie)
        {
            var sql = @"select 
                             c.AO_ID_AO 
                        from 
                             despacho d,
                             serie_despacho s,
                             item_despacho i,
                             detalhe_carregamento dc,
                             carregamento c 
                        where 
                             d.dp_num_dsp = :despacho
                             and d.sk_idt_srd = s.sk_idt_srd
                             and s.sk_num_srd = :serie
                             and d.dp_id_dp = i.dp_id_dp 
                             and dc.dc_id_crg = i.dc_id_crg 
                             and c.cg_id_car = dc.cg_id_car";

            try
            {
                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql);
                    query.SetParameter("despacho", despacho);
                    query.SetString("serie", serie);
                    var result = query.UniqueResult().ToString();
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }  
        }
	}
}