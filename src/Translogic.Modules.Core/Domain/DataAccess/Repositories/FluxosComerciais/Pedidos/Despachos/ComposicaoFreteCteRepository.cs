namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using NHibernate;
    using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da composi��o frete cte
	/// </summary>
	public class ComposicaoFreteCteRepository : NHRepository<ComposicaoFreteCte, int?>, IComposicaoFreteCteRepository
	{
        /// <summary>
        ///    Insere a ComposicaoFreteCte  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade da ComposicaoFreteCte</param>
        /// <returns>Retorna a entidade inserida </returns>
        public ComposicaoFreteCte InserirSemSessao(ComposicaoFreteCte entity)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.Insert(entity) as ComposicaoFreteCte;
            }
        }

        /// <summary>
        ///    Atualizar ComposicaoFreteCte  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade ComposicaoFreteCte</param>
        /// <returns>Retorna a entidade atualizada </returns>
        public ComposicaoFreteCte AtualizarSemSessao(ComposicaoFreteCte entity)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                session.Update(entity);
            }
            return entity;
        }

		/// <summary>
		/// Obt�m a Composi��o Frete Cte pelo Id do CTE
		/// </summary>
		/// <param name="cte">Cte da composi��o frete</param>
		/// <returns>Retorna a lista com as composi��es Frete CTE</returns>
		public IList<ComposicaoFreteCte> ObterComposicaoFreteCtePorCte(Cte cte)
		{
            using (ISession session = OpenSession())
            {
                string hql = @"FROM ComposicaoFreteCte cfc
                  WHERE cfc.Cte.Id = :IdCte 
                  ORDER BY cfc.Id
                ";
                
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("IdCte", cte.Id.Value);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<ComposicaoFreteCte>();
            }
		}
	}
}