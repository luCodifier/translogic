namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="DetalheCarregamento"/>
	/// </summary>
	public class DetalheCarregamentoRepository : NHRepository<DetalheCarregamento, int?>, IDetalheCarregamentoRepository
	{
	}
}