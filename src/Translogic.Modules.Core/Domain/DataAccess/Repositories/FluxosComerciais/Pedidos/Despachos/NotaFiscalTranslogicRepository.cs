namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Pedidos.Despachos;
    using Model.FluxosComerciais.Pedidos.Despachos.Dtos;
    using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

	/// <summary>
	/// Repositorio para <see cref="NotaFiscalTranslogic"/>
	/// </summary>
	public class NotaFiscalTranslogicRepository : NHRepository<NotaFiscalTranslogic, int?>, INotaFiscalTranslogicRepository
	{
		/// <summary>
		/// Obt�m as notas fiscais que devem ser atualizadas as informa��es
		/// </summary>
		/// <returns> Lista de notas fiscais de Dom Pedro </returns>
		public IList<NotaFiscalLdpDto> ObterNotasCorrecaoDomPedro()
		{
			using (ISession session = OpenSession())
			{
				ISQLQuery query = session.CreateSQLQuery(ObterSqlDomPedro());
				query.SetResultTransformer(Transformers.AliasToBean(typeof(NotaFiscalLdpDto)));
				// query.SetMaxResults(1);
				return query.List<NotaFiscalLdpDto>();
			}
		}

		/// <summary>
		/// Atualiza a placa e o peso de uma nota fiscal
		/// </summary>
		/// <param name="notaFiscalLdpDto">Id da nota fiscal</param>
		public void AtualizarPlacaPeso(NotaFiscalLdpDto notaFiscalLdpDto)
		{
			string sql = "UPDATE NOTA_FISCAL SET NF_PLACA_CV = :placaCavalo, NF_PESO_TOT_NF = :pesoTotal WHERE NF_IDT_NFL = :idNota";
			string sqlInsereLog = "INSERT INTO PROC_NF_LDP(NF_IDT_NFL, DATA_PROCESSAMENTO, MENSAGEM) VALUES(:idNota, :dataProcessamento, :mensagem)";
			using (ISession session = OpenSession())
			{
				try
				{
					ISQLQuery query;
					if (string.IsNullOrEmpty(notaFiscalLdpDto.MensagemErro))
					{
						query = session.CreateSQLQuery(sql);
						query.SetString("placaCavalo", notaFiscalLdpDto.PlacaCavalo);
						query.SetDouble("pesoTotal", notaFiscalLdpDto.PesoTotal.Value);
						query.SetDecimal("idNota", notaFiscalLdpDto.IdNota);
						query.ExecuteUpdate();
					}

					query = session.CreateSQLQuery(sqlInsereLog);
					query.SetDecimal("idNota", notaFiscalLdpDto.IdNota);
					query.SetDateTime("dataProcessamento", DateTime.Now);
					query.SetString("mensagem", notaFiscalLdpDto.MensagemErro);
					query.ExecuteUpdate();
				}
				catch (Exception)
				{
					session.Clear();
				}

				session.Flush();
			}
		}

		/// <summary>
		/// Atualiza a lista de notas em lote
		/// </summary>
		/// <param name="listaNotas"> The lista notas. </param>
		public void AtualizarEmLotePlacaPeso(List<NotaFiscalLdpDto> listaNotas)
		{
			using (ISession session = OpenSession())
			{
				foreach (NotaFiscalLdpDto notaFiscalLdpDto in listaNotas)
				{
					try
					{
						AtualizarPlacaPeso(notaFiscalLdpDto);
					}
					catch (Exception)
					{
						session.Flush();
						session.Clear();
					}
				}
			}
		}

		/// <summary>
		/// Obt�m a nota fiscal do translogic pelo despacho
		/// </summary>
		/// <param name="despachoTranslogic">Despacho do translogic</param>
		/// <returns>Retorna a nota fiscal</returns>
		public IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespacho(DespachoTranslogic despachoTranslogic)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("DespachoTranslogic", despachoTranslogic));
			criteria.AddOrder(new Order("DtCadastro", true));
			return ObterTodos(criteria);
		}

        /// <summary>
        /// Obt�m as notas fiscais do translogic pelos despachos
        /// </summary>
        /// <param name="despachoIds">Lista de ids de despachos</param>
        /// <returns>Retorna as notas fiscais</returns>
        public IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespacho(List<int> despachoIds)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.In("DespachoTranslogic.Id", despachoIds));

            return ObterTodos(criteria);
        }

		/// <summary>
		/// Obt�m a nota fiscal do translogic pelo carregamento
		/// </summary>
		/// <param name="carregamento">Carregamento do translogic</param>
		/// <returns>Retorna a nota fiscal</returns>
		public IList<NotaFiscalTranslogic> ObterNotaFiscalPorCarregamento(Carregamento carregamento)
		{
			IList<NotaFiscalTranslogic> result;
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();
			StringBuilder sb = new StringBuilder();

			sb.Append(
				@" SELECT nft
					FROM NotaFiscalTranslogic nft,
					ItemDespacho ide 
					INNER JOIN ide.DetalheCarregamento dc
					INNER JOIN dc.Carregamento c
					WHERE 
						ide.DespachoTranslogic = nft.DespachoTranslogic and ide.Vagao = nft.Vagao
						and c.Id = :idCarregamento
			");
			parametros.Add(q => q.SetInt32("idCarregamento", (int)carregamento.Id));

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(sb.ToString());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				foreach (Action<IQuery> action in parametros)
				{
					action.Invoke(query);
				}

				result = query.List<NotaFiscalTranslogic>();
			}

			return result;
		}

		/// <summary>
		/// Obt�m as notas fiscais do translogic que utilizaram a chave nfe
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Lista de NotaFiscalTranslogic</returns>
		public IList<NotaFiscalTranslogic> ObterPorChaveNfeSemDespachosCancelados(string chaveNfe)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.CreateAlias("DespachoTranslogic", "d");
			criteria.Add(
							Restrictions.Eq("ChaveNotaFiscalEletronica", chaveNfe)
							&& Restrictions.IsNull("d.DataCancelamento")
						);
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m as notas fiscais pelo despacho e pelo vag�o
		/// </summary>
		/// <param name="despachoTranslogic">Despacho do Translogic</param>
		/// <param name="vagao">Vag�o com as notas fiscais</param>
		/// <returns>Retorna uma lista com as notas fiscais do Translogic</returns>
		public IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespachoVagao(DespachoTranslogic despachoTranslogic, Vagao vagao)
		{
			IList<NotaFiscalTranslogic> result;
			List<Action<IQuery>> parametros = new List<Action<IQuery>>();
			StringBuilder sb = new StringBuilder();

			sb.Append(
				@" SELECT nft
					FROM NotaFiscalTranslogic nft
					INNER JOIN nft.DespachoTranslogic dt
					INNER JOIN nft.Vagao vg
					WHERE dt.Id = :idDespachoTranslogic
					AND		vg.Id = :idVagao
			");
			parametros.Add(q => q.SetInt32("idDespachoTranslogic", (int)despachoTranslogic.Id));
			parametros.Add(q => q.SetInt32("idVagao", (int)vagao.Id));

			using (ISession session = OpenSession())
			{
				IQuery query = session.CreateQuery(sb.ToString());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				foreach (Action<IQuery> action in parametros)
				{
					action.Invoke(query);
				}

				result = query.List<NotaFiscalTranslogic>();
			}

			return result;
		}

        /// <summary>
        /// Obt�m as notas fiscais pelo despacho e pelo vag�o dos CT-es passados como par�metro
        /// </summary>
        /// <param name="ctes">Lista de CT-es</param>
        /// <returns>Retorna uma lista com as notas fiscais do Translogic</returns>
        public IList<NotaFiscalTranslogic> ObterNotaFiscalPorCtesDespachoVagao(IList<Cte> ctes)
        {
            IList<NotaFiscalTranslogic> result;
            var parametros = new List<Action<IQuery>>();
            var sb = new StringBuilder();

            sb.Append(@" 
                SELECT nft
				  FROM NotaFiscalTranslogic nft
				  JOIN nft.DespachoTranslogic dt
				  JOIN nft.Vagao vg
				 WHERE dt.Id IN ( :idsDespachoTranslogic )
				   AND vg.Id IN ( :idsVagao )
			");

            var idsDespachos = ctes.Select(c => c.Despacho.Id).ToList();
            var idsVagoes = ctes.Select(c => c.Vagao.Id).ToList();

            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(sb.ToString());

                query.SetParameterList("idsDespachoTranslogic", idsDespachos);
                query.SetParameterList("idsVagao", idsVagoes);

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                foreach (Action<IQuery> action in parametros)
                {
                    action.Invoke(query);
                }

                result = query.List<NotaFiscalTranslogic>();
            }

            return result;
        }

		private static string ObterSqlDomPedro()
		{
			return @"
                SELECT NF1.NF_IDT_NFL AS ""IdNota"", NF1.NFE_CHAVE_NFE AS ""ChaveNfe""
                FROM (SELECT D.DP_ID_DP, DC.VG_ID_VG FROM
                VAGAO_PEDIDO_VIG VPV
                 JOIN CARREGAMENTO C
                   ON VPV.CG_ID_CAR = C.CG_ID_CAR
                 JOIN DETALHE_CARREGAMENTO DC
                   ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND VPV.VG_ID_VG = DC.VG_ID_VG
                 JOIN ITEM_DESPACHO IT
                   ON IT.DC_ID_CRG = DC.DC_ID_CRG AND IT.VG_ID_VG = DC.VG_ID_VG
                 JOIN DESPACHO D
                   ON D.DP_ID_DP = IT.DP_ID_DP
                 JOIN FLUXO_COMERCIAL FC
                   ON FC.FX_ID_FLX = VPV.FX_ID_FLX
                  JOIN NOTA_FISCAL NF
                   ON NF.DP_ID_DP = D.DP_ID_DP AND NF.VG_ID_VG = DC.VG_ID_VG
                WHERE 
                  FC.AO_ID_EST_DS = (SELECT AO.AO_ID_AO FROM AREA_OPERACIONAL AO WHERE AO.AO_COD_AOP='LDP')
                GROUP BY D.DP_ID_DP, DC.VG_ID_VG
                HAVING COUNT(*) > 1
                ) T1
                 JOIN NOTA_FISCAL NF1
                   ON NF1.DP_ID_DP = T1.DP_ID_DP AND NF1.VG_ID_VG = T1.VG_ID_VG
                WHERE NF1.NFE_CHAVE_NFE IS NOT NULL   
                AND NOT EXISTS( SELECT 1 FROM PROC_NF_LDP PNL WHERE PNL.NF_IDT_NFL = NF1.NF_IDT_NFL)
            ";
		}
	}
}
