namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Model.FluxosComerciais.Pedidos.Despachos;
	using Model.FluxosComerciais.Pedidos.Despachos.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;

	/// <summary>
	/// Reposit�rio da composi��o frete contrato
	/// </summary>
	public class ComposicaoFreteContratoRepository : NHRepository<ComposicaoFreteContrato, int?>, IComposicaoFreteContratoRepository 
	{
		/// <summary>
		/// Obt�m pelo n�mero do contrato e pela condi��o tarifa
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da tarifa do contrato</param>
		/// <returns>Retorna a composi��o frete contrato</returns>
		public ComposicaoFreteContrato ObterPorNumeroContratoCondicaoTarifa(string numeroContrato, string condicaoTarifa)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroCodigoContrato", numeroContrato));
			criteria.Add(Restrictions.Eq("CondicaoTarifa", condicaoTarifa));

			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		public IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, string letraFerrovia, bool? utilizadoParaCalculo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroCodigoContrato", numeroContrato));
			if (! String.IsNullOrEmpty(letraFerrovia))
			{
				criteria.Add(Restrictions.Eq("Ferrovia", letraFerrovia));
			}

			if (utilizadoParaCalculo != null)
			{
				criteria.Add(Restrictions.Eq("UtilizadoParaCalculo", utilizadoParaCalculo));
			}
			
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		public IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, DateTime dataReferencia, string letraFerrovia, bool? utilizadoParaCalculo)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroCodigoContrato", numeroContrato));
			if (!String.IsNullOrEmpty(letraFerrovia))
			{
				criteria.Add(Restrictions.Eq("Ferrovia", letraFerrovia));
			}

			if (utilizadoParaCalculo != null)
			{
				criteria.Add(Restrictions.Eq("UtilizadoParaCalculo", utilizadoParaCalculo));
			}

			criteria.Add(Restrictions.Le("DataInicioVigencia", dataReferencia));
			criteria.Add(
				Restrictions.Disjunction().Add(Restrictions.Ge("DataFimVigencia", dataReferencia)).Add(
					Restrictions.IsNull("DataFimVigencia")));

			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <param name="condicaoTarifa">Condi��o de Tarifa</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		public IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, DateTime dataReferencia, string letraFerrovia, bool? utilizadoParaCalculo, string condicaoTarifa)
		{
			condicaoTarifa = condicaoTarifa + "%";

			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroCodigoContrato", numeroContrato));
			if (!String.IsNullOrEmpty(letraFerrovia))
			{
				criteria.Add(Restrictions.Eq("Ferrovia", letraFerrovia));
			}

			if (utilizadoParaCalculo != null)
			{
				criteria.Add(Restrictions.Eq("UtilizadoParaCalculo", utilizadoParaCalculo));
			}

			criteria.Add(Restrictions.Like("CondicaoTarifa", condicaoTarifa));
			criteria.Add(Restrictions.Le("DataInicioVigencia", dataReferencia));
			criteria.Add(
				Restrictions.Disjunction().Add(Restrictions.Ge("DataFimVigencia", dataReferencia)).Add(
					Restrictions.IsNull("DataFimVigencia")));

			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m a composi��o pelo n�mero de contrato
		/// </summary>
		/// <param name="numeroContrato">N�mero do contrato</param>
		/// <returns>Retorna a lista com as composi��o</returns>
		public IList<ComposicaoFreteContrato> ObterComposicaoPorNumeroContrato(string numeroContrato)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroCodigoContrato", numeroContrato));
			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m a composi��o frete contrato das ferrovias do grupo pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		public IList<ComposicaoFreteContrato> ObterComposicaoFerroviasGrupoPorNumeroContrato(string numeroContrato)
		{
			using (ISession session = OpenSession())
			{
				string hql = @"FROM ComposicaoFreteContrato cfc
                     WHERE cfc.Ferrovia IN (SELECT ef.Sigla FROM EmpresaFerrovia ef WHERE ef.IndEmpresaALL='S')
                     AND cfc.DataFimVigencia IS NULL
                     AND cfc.NumeroCodigoContrato = :numeroContrato
                     AND cfc.UtilizadoParaCalculo = 'S'
                     ORDER BY cfc.DataInicioVigencia";

				IQuery query = session.CreateQuery(hql);
				query.SetString("numeroContrato", numeroContrato);
				
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<ComposicaoFreteContrato>();
			}
		}

		/// <summary>
		/// Obt�m a composi��o frete contrato das ferrovias do grupo pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da Tarifa</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		public IList<ComposicaoFreteContrato> ObterComposicaoFerroviasGrupoPorNumeroContrato(string numeroContrato, string condicaoTarifa)
		{
			using (ISession session = OpenSession())
			{
				condicaoTarifa = condicaoTarifa + "%";
				
				string hql =
					@"FROM ComposicaoFreteContrato cfc
                     WHERE cfc.Ferrovia IN (SELECT ef.Sigla FROM EmpresaFerrovia ef WHERE ef.IndEmpresaALL='S')
                     AND cfc.CondicaoTarifa LIKE :condicaoTarifa
                     AND cfc.DataFimVigencia IS NULL
                     AND cfc.NumeroCodigoContrato = :numeroContrato
                     AND cfc.UtilizadoParaCalculo = 'S'
                     ORDER BY cfc.DataInicioVigencia";

				IQuery query = session.CreateQuery(hql);
				query.SetString("numeroContrato", numeroContrato);
				query.SetString("condicaoTarifa", condicaoTarifa);

				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<ComposicaoFreteContrato>();
			}
		}

		/// <summary>
		/// Obtem a composicao frete contrato pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		public IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato)
		{
			using (ISession session = OpenSession())
			{
				string hql =
					@"FROM ComposicaoFreteContrato cfc
                     WHERE cfc.DataFimVigencia IS NULL
                     AND cfc.NumeroCodigoContrato = :numeroContrato
                     AND cfc.UtilizadoParaCalculo = 'S'
                     ORDER BY cfc.DataInicioVigencia";

				IQuery query = session.CreateQuery(hql);
				query.SetString("numeroContrato", numeroContrato);

				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<ComposicaoFreteContrato>();
			}
		}

		/// <summary>
		/// Obtem a composicao frete contrato pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da tarifa</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		public IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, string condicaoTarifa)
		{
			using (ISession session = OpenSession())
			{
				condicaoTarifa = condicaoTarifa + "%";

				string hql =
					@"FROM ComposicaoFreteContrato cfc
                     WHERE cfc.CondicaoTarifa LIKE :condicaoTarifa
                     AND cfc.DataFimVigencia IS NULL
                     AND cfc.NumeroCodigoContrato = :numeroContrato
                     AND cfc.UtilizadoParaCalculo = 'S'
                     ORDER BY cfc.DataInicioVigencia";

				IQuery query = session.CreateQuery(hql);
				query.SetString("numeroContrato", numeroContrato);
				query.SetString("condicaoTarifa", condicaoTarifa);

				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<ComposicaoFreteContrato>();
			}
		}
	}
}