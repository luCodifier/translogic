namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.OracleClient;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using ALL.Core.AcessoDados;
    using Interfaces.Trem.OrdemServico;
    using Model.Acesso;
    using Model.Codificador;
    using Model.Diversos;
    using Model.Dto;
    using Model.Estrutura;
    using Model.FluxosComerciais.Pedidos;
    using Model.QuadroEstados.Dimensoes;
    using Model.Trem.OrdemServico;
    using Model.Trem.Veiculo.Conteiner;
    using Model.Trem.Veiculo.Vagao;
    using Model.Trem.Veiculo.Vagao.QuadroEstado;
    using Model.Via;
    using NHibernate;
    using NHibernate.Linq;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

    /// <summary>
    /// Repositorio para <see cref="Descarregamento"/>
    /// </summary>
    public class DescarregamentoRepository : NHRepository<Descarregamento, int?>, IDescarregamentoRepository
    {
        #region CONSTANTES

        /*,VG_TIMESTAMP
  ,LO_IDT_LCL
  ,LV_IDT_LCV
  ,LV_TIMESTAMP
  ,RV_IDT_RPV
  ,RV_TIMESTAMP
  ,SC_IDT_STV
  ,SC_TIMESTAMP
  ,SD_IDT_STI
  ,SD_TIMESTAMP
  ,CI_IDT_CDV
  ,CI_TIMESTAMP
  ,pt
  ,pt_ts
  ,px
  ,px_ts
  ,vb
  ,vb_ts
  ,VP_IDT_PTV
  ,VP_TIMESTAMP
  ,ind_dst
  ,FLAG_PRECARGA*/
        // TODO: Mudar uso da query para Linq.
        private const string __SQL_VAGOESDESCARREGAMENTO =
            @"SELECT
  lin AS ""Linha""
  ,idlin AS ""IdLinha""
  ,seq AS ""Sequencia""
  ,ser AS ""Serie""
  ,vag AS ""Vagao""
  ,ped AS ""CodPedido""
  ,pt AS ""IdPedido""
  ,ped AS ""CodPedidoTransporte""
  ,pt AS ""IdPedidoTransporte""
  ,pt_ts AS ""DataPedidoTransporte""
  ,px AS ""IdPedidoDerivado""
  ,px_ts AS ""DataPedidoDerivado""
  ,mer AS ""Mercadoria""
  ,AO_ID_AO AS ""IdAreaOperacional""
  ,vg AS ""IdVagao""
  ,VP_IDT_PTV AS ""IdVagaoPatio""
  ,vb AS ""IdVagaoPedido""
  ,CI_IDT_CDV AS ""IdCondicaoUso""
  ,CI_TIMESTAMP AS ""DataCondicaoUso""
  ,SG_IDT_STC AS ""IdLocacao""
  ,SG_TIMESTAMP AS ""DataLocacao""
  ,LO_IDT_LCL AS ""IdLocalizacao""
  ,NVL(MAX(DT_TU1), MAX(DT_TU2)) AS ""DataPesagem""
FROM (
  SELECT
     via.EV_COD_ELV lin
    ,via.EV_ID_ELV idlin
    ,VP_NUM_SEQ seq
    ,SV_COD_SER ser
    ,v.VG_COD_VAG vag
    ,ped
    ,(SELECT MC_COD_MRC FROM MERCADORIA m WHERE m.MC_ID_MRC = mer) mer
    ,sede.AO_ID_AO
    ,v.VG_ID_VG vg
    ,VG_TIMESTAMP
    ,LO_IDT_LCL
    ,LV_IDT_LCV
    ,LV_TIMESTAMP
    ,RV_IDT_RPV
    ,RV_TIMESTAMP
    ,SC_IDT_STV
    ,SC_TIMESTAMP
    ,SG_IDT_STC
    ,SG_TIMESTAMP
    ,SD_IDT_STI
    ,SD_TIMESTAMP
    ,CI_IDT_CDV
    ,CI_TIMESTAMP
    ,pt
    ,pt_ts
    ,px
    ,px_ts
    ,vb
    ,vb_ts
    ,VP_IDT_PTV
    ,VP_TIMESTAMP  
    ,(  SELECT
            DCAL.DCL_DATA_INFO
        FROM DESCARGA_CAALL DCAL
        WHERE DCAL.VB_IDT_VPT = vb) AS DT_TU1
    ,(  SELECT
            MAX(PEF.PF_DT_PESAGEM) AS DT_TU
        FROM PESAGEM_ESTATICA_FERRO PEF, AREA_OP_BALANCA_FERRO BF
        WHERE PEF.PF_COD_BALANCA = BF.COD_BALANCA
            AND PEF.PF_COD_VAGAO = v.VG_COD_VAG
            AND PEF.PF_DT_PESAGEM > data_carregamento
            AND (BF.AO_ID_AO = filha.AO_ID_AO OR BF.AO_ID_AO = sede.AO_ID_AO)
    ) AS DT_TU2
    ,DECODE(sede.AO_ID_AO, dst, 1, 0) ind_dst
    ,( SELECT ID2.IND_PRE_FAT
        FROM   ITEM_DESPACHO ID2
        WHERE  ID2.DC_ID_CRG = pedido.id_detalhe_carga
        AND    ROWNUM=1  ) AS FLAG_PRECARGA
  FROM (
    SELECT
         vpe.VG_ID_VG vg
        ,pt.PT_ID_ORT pt
        ,pt.PT_COD_PDT ped
        ,NVL(pd.MC_ID_MRC,pt.MC_ID_MRC) mer
        ,PT_TIMESTAMP pt_ts
        ,pd.PX_IDT_PDR px
        ,PX_TIMESTAMP px_ts
        ,VB_IDT_VPT vb
        ,VB_TIMESTAMP vb_ts
        ,NVL(pd.AO_ID_AO_DST,pt.AO_ID_AO_DST) dst
        ,dca.DC_ID_CRG id_detalhe_carga
        ,C.CG_DTR_CRG data_carregamento
     FROM
         VAGAO_PEDIDO_VIG vpe
        ,PEDIDO_DERIVADO pd
        ,FLUXO_COMERCIAL fc
        ,PEDIDO_TRANSPORTE pt
        ,DETALHE_CARREGAMENTO dca
        ,CARREGAMENTO C
        ,EMPRESA CORR
     WHERE
             vpe.PT_ID_ORT=pt.PT_ID_ORT
         AND fc.FX_ID_FLX = vpe.FX_ID_FLX
         AND dca.CG_ID_CAR=vpe.CG_ID_CAR
         AND dca.VG_ID_VG=vpe.VG_ID_VG
         AND C.CG_ID_CAR = dca.CG_ID_CAR
         AND vpe.PX_IDT_PDR=pd.PX_IDT_PDR(+)
         AND CORR.EP_ID_EMP = fc.EP_ID_EMP_COR
         AND (:cliente IS NULL OR CORR.EP_DSC_RSM = :cliente)
         AND C.CG_DTR_CRG BETWEEN :dataInicial AND :dataFinal
         AND (:fluxo IS NULL OR NVL(pd.PX_NUM_FLX,pt.PT_NUM_FLX) = :fluxo)
         AND (:pedido IS NULL OR pt.PT_COD_PDT= :pedido)) pedido
  ,EVENTO ev
  ,SERIE_VAGAO sv
  ,MUDANCA_QEVG me
  ,VAGAO_PATIO_VIG vpa
  ,AREA_OPERACIONAL filha
  ,AREA_OPERACIONAL sede
  ,ELEMENTO_VIA via
  ,ESTADO_VAGAO st
  ,VAGAO v
WHERE
  sv.SV_ID_SV = v.SV_ID_SV
  AND v.VG_ID_VG = vg
  AND st.VG_ID_VG = vg
  AND st.QS_IDT_QDS=me.QS_IDT_QDS_ATU
  AND me.EN_ID_EVT=ev.EN_ID_EVT
  AND ev.EN_COD_EVT='DSC'
  AND via.EV_ID_ELV=vpa.EV_ID_ELV
  AND vg=vpa.VG_ID_VG
  AND vpa.AO_ID_AO=filha.AO_ID_AO
  AND filha.AO_ID_AO_INF=sede.AO_ID_AO
  {0}
  AND (:linha IS NULL OR via.EV_COD_ELV=:linha)
  AND (:ao IS NULL OR sede.AO_COD_AOP=:ao)
  AND (:merc IS NULL OR (SELECT MC_COD_MRC FROM MERCADORIA m WHERE m.MC_ID_MRC = mer)=:merc))
WHERE DT_TU1 IS NOT NULL OR DT_TU2 IS NOT NULL
GROUP BY
   lin
  ,idlin
  ,seq
  ,ser
  ,vag
  ,ped
  ,mer
  ,AO_ID_AO
  ,vg
  ,VG_TIMESTAMP
  ,LO_IDT_LCL
  ,LV_IDT_LCV
  ,LV_TIMESTAMP
  ,RV_IDT_RPV
  ,RV_TIMESTAMP
  ,SC_IDT_STV
  ,SC_TIMESTAMP
  ,SG_IDT_STC
  ,SG_TIMESTAMP
  ,SD_IDT_STI
  ,SD_TIMESTAMP
  ,CI_IDT_CDV
  ,CI_TIMESTAMP
  ,pt
  ,pt_ts
  ,px
  ,px_ts
  ,vb
  ,vb_ts
  ,VP_IDT_PTV
  ,VP_TIMESTAMP
  ,ind_dst
  ,FLAG_PRECARGA
ORDER BY 1,2,3,4,5,6";

        #endregion

        #region ObterVagoesParaDescarregamento

        /// <summary>
        /// Recupera a lista de vag�es a serem descarregados
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do Cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesParaDescarregamento(string codAO, string codLinha, string codVagao, string codMerc, string codFluxo, string codPedido, string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(String.Format(__SQL_VAGOESDESCARREGAMENTO, String.IsNullOrWhiteSpace(codVagao) ? String.Empty : "AND v.VG_COD_VAG IN(:vagoes)").Replace('\n', ' '));

                query.SetResultTransformer(Transformers.AliasToBean(typeof(VagaoDescarregamentoDto)));
                query.SetParameter("fluxo", String.IsNullOrWhiteSpace(codFluxo) ? null : codFluxo);
                query.SetParameter("pedido", String.IsNullOrWhiteSpace(codPedido) ? null : codPedido);
                query.SetParameter("linha", String.IsNullOrWhiteSpace(codLinha) ? null : codLinha);
                query.SetParameter("ao", String.IsNullOrWhiteSpace(codAO) ? null : codAO);
                query.SetParameter("merc", String.IsNullOrWhiteSpace(codMerc) ? null : codMerc);
                query.SetParameter("cliente", String.IsNullOrWhiteSpace(identCliente) ? null : identCliente);
                query.SetParameter("dataInicial", dataInicio);
                query.SetParameter("dataFinal", dataFim);

                if (!String.IsNullOrWhiteSpace(codVagao))
                {
                    query.SetParameterList("vagoes", codVagao.Split(new[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries));
                }

                return query.List<VagaoDescarregamentoDto>();
            }
        }

        #endregion

        #region ObterVagoesComErro

        /// <summary>
        /// Recupera a lista de vag�es n�o descarregados por erros
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesComErro(string codAO, string codLinha, string codVagao, string codMerc, string codFluxo, string codPedido, string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            using (var session = OpenSession())
            {
                codFluxo = String.IsNullOrWhiteSpace(codFluxo) ? null : codFluxo;
                codPedido = String.IsNullOrWhiteSpace(codPedido) ? null : codPedido;
                codAO = String.IsNullOrWhiteSpace(codAO) ? null : codAO;
                codMerc = String.IsNullOrWhiteSpace(codMerc) ? null : codMerc;
                identCliente = String.IsNullOrWhiteSpace(identCliente) ? null : identCliente;

                var query = from eda in session.Query<ErroDescargaAutomatica>()
                            where session.Query<VagaoPedidoVigente>().Any(a => a.Id == eda.VagaoPedido.Id)
                                && (codFluxo == null || eda.VagaoPedido.FluxoComercial.Codigo == codFluxo)
                                && (codPedido == null || eda.VagaoPedido.Pedido.Codigo == codPedido)
                                && (codAO == null || eda.VagaoPedido.FluxoComercial.Destino.Codigo == codAO)
                                && (codMerc == null || eda.VagaoPedido.FluxoComercial.Mercadoria.Codigo == codMerc)
                                && (identCliente == null || eda.VagaoPedido.FluxoComercial.EmpresaPagadora.DescricaoResumida == identCliente)
                                && eda.Data >= dataInicio
                                && eda.Data <= dataFim
                            select eda;

                return query.Select(eda => new VagaoDescarregamentoDto
                {
                    Vagao = eda.VagaoPedido.Vagao.Codigo,
                    IdVagao = Convert.ToDecimal(eda.VagaoPedido.Vagao.Id.Value),
                    Serie = eda.VagaoPedido.Vagao.Serie.Codigo,
                    IdVagaoPedido = Convert.ToDecimal(eda.VagaoPedido.Id.Value),
                    CodPedido = eda.VagaoPedido.Pedido.Codigo,
                    IdPedido = Convert.ToDecimal(eda.VagaoPedido.Pedido.Id.Value),
                    Erro = eda.Descricao,
                    XMLErro = eda.XMLRetorno
                }).ToList();
            }
        }

        #endregion

        #region ObterVagoesDescarregados

        /// <summary>
        /// Recupera a lista de vag�es a serem descarregados
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        public IList<VagaoDescarregamentoDto> ObterVagoesDescarregados(string codAO, string codLinha, string codVagao, string codMerc, string codFluxo, string codPedido, string identCliente, DateTime dataInicio, DateTime dataFim)
        {
            using (var session = OpenSession())
            {
                codFluxo = String.IsNullOrWhiteSpace(codFluxo) ? null : codFluxo;
                codPedido = String.IsNullOrWhiteSpace(codPedido) ? null : codPedido;
                codAO = String.IsNullOrWhiteSpace(codAO) ? null : codAO;
                codMerc = String.IsNullOrWhiteSpace(codMerc) ? null : codMerc;
                identCliente = String.IsNullOrWhiteSpace(identCliente) ? null : identCliente;

                var query = from vpe in session.Query<VagaoPedido>()
                            where vpe.CodTransDescarga == "DSC_AUT"
                                && (codFluxo == null || vpe.FluxoComercial.Codigo == codFluxo)
                                && (codPedido == null || vpe.Pedido.Codigo == codPedido)
                                && (codAO == null || vpe.Pedido.TerminalDestino.Codigo == codAO)
                                && (codMerc == null || vpe.FluxoComercial.Mercadoria.Codigo == codMerc)
                                && (identCliente == null || vpe.FluxoComercial.EmpresaPagadora.DescricaoResumida == identCliente)
                            select vpe;

                return query.Select(vpe => new VagaoDescarregamentoDto
                {
                    Vagao = vpe.Vagao.Codigo,
                    IdVagao = Convert.ToDecimal(vpe.Vagao.Id.Value),
                    Serie = vpe.Vagao.Serie.Codigo,
                    IdVagaoPedido = Convert.ToDecimal(vpe.Id.Value),
                    CodPedido = vpe.Pedido.Codigo,
                    IdPedido = Convert.ToDecimal(vpe.Pedido.Id.Value),
                }).ToList();
            }
        }

        #endregion

        #region ObterClientesComDescarregamentos

        /// <summary>
        /// Obter clientes com vag�es em condi��o de descarregamento
        /// </summary>
        /// <param name="idAO">Identificador a Area Operacional</param>
        /// <returns>Lista de empresas com vag�es para descarregamento</returns>
        public IList<string> ObterClientesComDescarregamentos(int idAO)
        {
            using (var session = OpenSession())
            {
                var query = from vpv in session.Query<VagaoPedidoVigente>()
                            join vpa in session.Query<VagaoPatioVigente>() on vpv.Vagao.Id equals vpa.Vagao.Id
                            where (vpa.Patio.Id == idAO || vpa.Patio.EstacaoMae.Id == idAO)
                                && vpv.FluxoComercial.EmpresaPagadora != null
                            select vpv.FluxoComercial.EmpresaPagadora.DescricaoResumida;

                return query.Distinct().ToList();
            }
        }

        #endregion

        #region DescarregarVagoes

        /// <summary>
        /// Efetuar descarregamento de vag�es com base nas TUs da Tabela de PESAGEM_ESTATICA_FERRO
        /// </summary>
        /// <param name="usuAcao">Usu�rio que efetuou a a��o</param>
        /// <param name="identAO">Identificador da �rea Operacional</param>
        /// <param name="vagaoEmCarreg">Indicador se � para mudar o vag�o para "EM CARREGAMENTO"</param>
        /// <param name="vagoes">Lista de vag�es</param>
        public void DescarregarVagoes(Usuario usuAcao, int identAO, bool vagaoEmCarreg, List<VagaoDescarregamentoDto> vagoes)
        {
            using (var session = OpenSession())
            {
                var dataBaseCmd = session.Connection.CreateCommand();
                dataBaseCmd.CommandText = "PKG_CCP.SP_DESCARGA_CCP";
                dataBaseCmd.CommandType = CommandType.StoredProcedure;

                var databParamEntrada = __addParameter(dataBaseCmd, "pxmlEntrada", DbType.String, DBNull.Value, ParameterDirection.Input, 4000);
                var databParamSaida = __addParameter(dataBaseCmd, "pxmlSaida", DbType.String, DBNull.Value, ParameterDirection.Output, 4000);
                foreach (var vagao in vagoes)
                {
                    try
                    {
                        var xmlEntrada = __gerarXMLSPDescarga(session, usuAcao, vagao.DataPesagem, vagao.DataPesagem.AddSeconds(1), identAO, vagaoEmCarreg, new[] { vagao });

                        databParamEntrada.Value = xmlEntrada;
                        databParamSaida.Value = DBNull.Value;

                        session.Transaction.Enlist(dataBaseCmd);

                        dataBaseCmd.Prepare();
                        dataBaseCmd.ExecuteNonQuery();

                        var xmlResultado = databParamSaida.Value as string;

                        if (xmlResultado != "<XML></XML>")
                        {
                            var objErro = new ErroDescargaAutomatica();
                            objErro.XMLRetorno = xmlResultado;
                            objErro.Usuario = usuAcao;
                            objErro.Descricao = "Durante o descarregamento, foram encontrados erros. Verifique com o administrador do sistema.";
                            objErro.Data = DateTime.Now;
                            objErro.VagaoPedido = session.Get<VagaoPedido>(Convert.ToInt32(vagao.IdVagaoPedido));
                            session.SaveOrUpdate(objErro);
                        }
                    }
                    catch (Exception ex)
                    {
                        var objErro = new ErroDescargaAutomatica();
                        objErro.XMLRetorno = "EXCEPTION:" + ex.StackTrace;
                        objErro.Usuario = usuAcao;
                        objErro.Descricao = "Ocorreu um erro ao descarregar o vag�o: " + ex.Message + ".";
                        objErro.Data = DateTime.Now;
                        objErro.VagaoPedido = session.Get<VagaoPedido>(Convert.ToInt32(vagao.IdVagaoPedido));
                        session.SaveOrUpdate(objErro);
                    }
                }
            }
        }

        private static string __gerarXMLSPDescarga(ISession session, Usuario usuAcao, DateTime dataInicio, DateTime dataFim, int identAO, bool vagaoEmCarreg, IEnumerable<VagaoDescarregamentoDto> vapeds)
        {
            StringBuilder strbuilderXML = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(strbuilderXML, new XmlWriterSettings { OmitXmlDeclaration = true });

            xmlWriter.WriteStartElement("XML");

            xmlWriter.WriteStartElement("DESCARGA");

            xmlWriter.WriteAttributeString("COD_LOCAL", session.Get<EstacaoMae>(identAO).Codigo);
            xmlWriter.WriteAttributeString("ID_LOCAL", identAO.ToString());
            xmlWriter.WriteAttributeString("DT_INICIO", dataInicio.ToString("dd/MM/yyyy HH:mm:ss"));
            xmlWriter.WriteAttributeString("DT_FIM", dataFim.ToString("dd/MM/yyyy HH:mm:ss"));
            xmlWriter.WriteAttributeString("IND_NOVA_CARGA", vagaoEmCarreg ? "S" : "N");
            xmlWriter.WriteAttributeString("USUARIO", usuAcao.Codigo);
            xmlWriter.WriteAttributeString("SISTEMA", "DSC_AUT");

            foreach (var vagao in vapeds)
            {
                xmlWriter.WriteStartElement("VAGAO");

                xmlWriter.WriteAttributeString("ID", vagao.IdVagao.ToString());
                xmlWriter.WriteAttributeString("COD", vagao.Vagao);
                xmlWriter.WriteAttributeString("ID_LINHA", Convert.ToInt32(vagao.IdLinha).ToString());
                xmlWriter.WriteAttributeString("COD_LINHA", vagao.Linha);
                xmlWriter.WriteAttributeString("COD_PEDIDO", vagao.CodPedido);
                xmlWriter.WriteAttributeString("ID_PEDIDO", vagao.IdPedido.ToString());

                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
            xmlWriter.Flush();

            return strbuilderXML.ToString();
        }

        #endregion

        #region __addParameter

        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="DbCommand"/>
        /// </summary>
        /// <param name="command"><see cref="DbCommand"/> onde o <see cref="OracleParameter"/> ser� adicionado</param>
        /// <param name="nome">Nome do par�metro</param>
        /// <param name="tipo">Tipo do par�metro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Dire��o <see cref="ParameterDirection"/></param>
        /// <param name="tamanho">Tamanho do campo</param>
        /// <returns>DbParameter criado</returns>
        private static IDbDataParameter __addParameter(IDbCommand command, string nome, DbType tipo, object valor, ParameterDirection direcao, int? tamanho)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = nome;
            parameter.DbType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (tamanho.HasValue)
            {
                parameter.Size = tamanho.Value;
            }

            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
            return parameter;
        }

        #endregion
    }
}
