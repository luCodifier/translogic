namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Trem.Veiculo.Vagao;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;

    /// <summary>
	/// Repositorio para <see cref="ItemDespacho"/>
	/// </summary>
	public class ItemDespachoRepository : NHRepository<ItemDespacho, int?>, IItemDespachoRepository
    {
        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="itensDespachosIds">ids dos itens despachos</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        public IList<ItemDespacho> ObterPorIds(List<int> itensDespachosIds)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Id", itensDespachosIds));

            return ObterTodos(criteria);
        }

        /// <summary>
        ///   Obter ItemDespacho pelo ID do Item Despacho
        /// </summary>
        /// <returns>Item Despacho</returns>
        public ItemDespacho ObterPorIdSemEstado(int idDespacho)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.Get<ItemDespacho>(idDespacho);
            }
        }

        /// <summary>
        ///   Obter ItemDespacho pelo ID do Despacho sem estado
        /// </summary>
        /// <returns>ItemDespacho</returns>
        public ItemDespacho ObterItemDespachoPorIdDespachoSemEstado(int idDespacho)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.QueryOver<ItemDespacho>().Where(p => p.DespachoTranslogic.Id == idDespacho && (p.IndBaldeio == null || p.IndBaldeio == "R")).SingleOrDefault();
            }
        }

        /// <summary>
    	/// Obt�m o item de despacho pelo despacho
    	/// </summary>
    	/// <param name="despacho">Objeto de Despacho do Translogic</param>
    	/// <returns>Retorna a insta�ncia da serie despacho UF</returns>
    	public ItemDespacho ObterItemDespachoPorDespacho(DespachoTranslogic despacho)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("DespachoTranslogic", despacho));
            return ObterPrimeiro(criteria);
        }

        /// <summary>
        /// Obt�m os Itens de Despacho pelo vag�o que possua pedido Vigente
        /// </summary>
        /// <param name="vagao"> Objeto vagao. </param>
        /// <returns>Lista de itens de despacho </returns>
        public IList<ItemDespacho> ObterPorVagaoComPedidoVigente(Vagao vagao)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"SELECT {idu} FROM 
                                ItemDespacho idu
                                  INNER JOIN idu.DetalheCarregamento dc
                                  INNER JOIN dc.Carregamento c
                                WHERE 
                                  EXISTS (SELECT vpv.Id FROM VagaoPedidoVigente vpv WHERE vpv.Vagao.Id = :idVagao AND vpv.Carregamento = c)
                                  AND idu.Vagao.Id = :idVagao";
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idVagao", vagao.Id.Value);
                return query.List<ItemDespacho>();
            }
        }

        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="idsItemDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        public IList<DespachoTranslogic> ObterDespachosPorItemDespacho(int[] idsItemDespacho)
        {
            using (ISession session = OpenSession())
            {
                var listItemDespacho = session
                .QueryOver<ItemDespacho>()
                .WhereRestrictionOn(i => i.Id.Value).IsIn(idsItemDespacho)
                .Fetch(x => x.DespachoTranslogic).Eager
                .List();

                if (!listItemDespacho.Any())
                    return null;

                return listItemDespacho.Select(x => x.DespachoTranslogic).ToList();
            }
        }

        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="idsItemDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        public IList<ItemDespachoDespachoDto> ObterDespachosPorItemDespachoDto(int[] idsItemDespacho)
        {
            using (ISession session = OpenSession())
            {
                var listItemDespacho = session
                .QueryOver<ItemDespacho>()
                .WhereRestrictionOn(i => i.Id.Value).IsIn(idsItemDespacho)
                .Fetch(x => x.DespachoTranslogic).Eager
                .List();

                if (!listItemDespacho.Any())
                    return null;

                return listItemDespacho.Select(x => (ItemDespachoDespachoDto)x).ToList();
            }
        }

        /// <summary>
        /// Obt�m o vag�o e o cte dos itens de despacho
        /// </summary>
        /// <param name="idsItemDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos dados do ItemDespacho</returns>
        public IList<ImpressaoDocumentosVagaoItemDespacho> ObterVagoesCtesItensDespacho(IList<int> itensDespacho)
        {
            var parameters = new List<Action<IQuery>>();

            var hql = new StringBuilder(@" SELECT IDS.ID_ID_ITD AS ""IdItemDespacho""
                                                , V.VG_COD_VAG  AS ""CodigoVagao""
                                                , DP.DP_DT      AS ""DataCarga""
                                                , MAX(C.ID_CTE) AS ""IdCte""
                                            FROM ITEM_DESPACHO IDS
	                                  INNER JOIN DESPACHO       DP ON DP.DP_ID_DP   = IDS.DP_ID_DP
	                                  INNER JOIN VAGAO           V ON V.VG_ID_VG    = IDS.VG_ID_VG
	                                   LEFT JOIN CTE             C ON C.DP_ID_DP    = DP.DP_ID_DP
                                       LEFT JOIN COMPVAGAO_VIG CVV ON CVV.VG_ID_VG  = V.VG_ID_VG
                                           WHERE IDS.ID_ID_ITD IN (:IdsItemDespacho)
                                        GROUP BY IDS.ID_ID_ITD
                                               , V.VG_COD_VAG
                                               , DP.DP_DT
                                               , CVV.CV_SEQ_CMP
                                        ORDER BY CVV.CV_SEQ_CMP ASC
                                        ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));
                query.SetParameterList("IdsItemDespacho", itensDespacho);

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<ImpressaoDocumentosVagaoItemDespacho>());
                var itens = query.List<ImpressaoDocumentosVagaoItemDespacho>();

                return itens;
            }
        }

        /// <summary>
        /// Obt�m o vag�o e o seu item despacho para obter suas documenta��es
        /// </summary>
        /// <param name="composicaoId">Id da composi��o</param>
        /// <returns>Retorna a inst�ncia dos dados do ItemDespacho</returns>
        public IList<ImpressaoDocumentosVagaoItemDespacho> ObterVagoesItensDespachoPorComposicao(decimal composicaoId)
        {
            var parameters = new List<Action<IQuery>>();

            // Para este m�todo n�o � necess�rio a impress�o de CTe e, por este motivo, setamos-o para zero.
            // Zero pois reutilizamos a classe ImpressaoDocumentosVagaoItemDespacho e deix�-lo como Nullable 
            // ter�amos que fazer refactory nos callers deste m�todo.
            var hql = new StringBuilder(@"
                                          SELECT ITD.ID_ID_ITD  AS ""IdItemDespacho""
                                               , VG.VG_COD_VAG  AS ""CodigoVagao""
                                               , DP.DP_DT       AS ""DataCarga""
                                               , 0              AS ""IdCte""
                                               , CP.CP_ID_CPS   AS ""IdComposicao""
                                               , CPV.CV_SEQ_CMP AS ""Sequencia""
                                            FROM COMPOSICAO           CP
                                            JOIN COMPVAGAO            CPV  ON CPV.CP_ID_CPS = CP.CP_ID_CPS
                                            JOIN VAGAO                VG   ON VG.VG_ID_VG   = CPV.VG_ID_VG
                                            JOIN VAGAO_PEDIDO         VPED ON VPED.VG_ID_VG = CPV.VG_ID_VG AND VPED.PT_ID_ORT = CPV.PT_ID_ORT
                                            JOIN CARREGAMENTO         CG   ON CG.CG_ID_CAR  = VPED.CG_ID_CAR
                                            JOIN DETALHE_CARREGAMENTO DC   ON DC.CG_ID_CAR  = CG.CG_ID_CAR
                                            JOIN ITEM_DESPACHO        ITD  ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                                            JOIN DESPACHO             DP   ON DP.DP_ID_DP = ITD.DP_ID_DP
                                           WHERE CP.CP_ID_CPS = :IdComposicao
                                        ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(string.Format(hql.ToString()));
                query.SetDecimal("IdComposicao", composicaoId);

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<ImpressaoDocumentosVagaoItemDespacho>());
                var itens = query.List<ImpressaoDocumentosVagaoItemDespacho>();

                return itens;
            }
        }


        /// <summary>
        /// Obt�m o item de despacho pelo N�mero do Despacho e N�mero do Vag�o
        /// </summary>
        /// <param name="numeroDespacho">N�mero do Despacho</param>
        /// <param name="numeroVagao">N�mero do Vag�o</param>
        /// <returns>Retorna a inst�ncia do item de despacho pelo N�mero do Despacho e N�mero do Vag�o</returns>
        public ItemDespacho ObterItemDespachoPorNumeroDespachoNumeroVagao(int numeroDespacho, string numeroVagao)
        {
            using (ISession session = OpenSession())
            {
                string hql = @"SELECT idu.Id as Id, 
                                    idu.DespachoTranslogic as DespachoTranslogic,  
                                    idu.Vagao as Vagao
                                FROM 
                                ItemDespacho idu
                                  INNER JOIN idu.DespachoTranslogic dp
                                  INNER JOIN idu.Vagao v
                                WHERE 
                                    dp.NumeroDespacho = :numeroDespacho                                  
                                  AND v.Codigo = :numeroVagao";
                IQuery query = session.CreateQuery(hql);
                query.SetString("numeroDespacho", numeroDespacho.ToString());
                query.SetString("numeroVagao", numeroVagao);

                query.SetResultTransformer(Transformers.AliasToBean<ItemDespacho>());
                var itemDespacho = query.UniqueResult<ItemDespacho>();

                return itemDespacho;
            }
        }

        /// <summary>
        /// Obt�m o ID do item de despacho pelo ID do Despacho
        /// </summary>
        /// <param name="idDespacho">ID do Despacho</param>
        /// <returns>Retorna o ID do item de despacho pelo ID do Despacho</returns>
        public int? ObterIDItemDespachoPorIdDespacho(int idDespacho)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT ID.ID_ID_ITD AS ""Id"" FROM ITEM_DESPACHO ID
                            INNER JOIN DESPACHO DP ON (ID.DP_ID_DP = DP.DP_ID_DP)                            
                            WHERE DP.DP_ID_DP = {0}", idDespacho);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.AddScalar("Id", NHibernateUtil.Int32);

                var result = query.UniqueResult<int?>();

                return result;
            }

            #endregion
        }
    }
}