namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using ALL.Core.AcessoDados;
	using Model.Diversos;
	using Model.Estrutura;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

	/// <summary>
	/// Repositorio para <see cref="SerieDespachoUf"/>
	/// </summary>
	public class SerieDespachoUfRepository : NHRepository<SerieDespachoUf, int?>, ISerieDespachoUfRepository
	{
		/// <summary>
		/// Obt�m a empresa da serie despacho Uf pelo id da empresa
		/// </summary>
		/// <param name="empresa">Objeto de Despacho do Translogic</param>
		/// <returns>Retorna a inst�ncia da serie despacho UF</returns>
		public SerieDespachoUf ObterPorEmpresa(int empresa)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("EmpresaFerroviaDespacho.Id", empresa));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m a serie despacho uf pela empresa e pela sigla
		/// </summary>
		/// <param name="empresaConcessionaria"> The empresa concessionaria. </param>
		/// <param name="siglaUf"> The sigla do estado. </param>
		/// <returns> Objeto SerieDespachoUf </returns>
		public SerieDespachoUf ObterPorEmpresaUf(IEmpresa empresaConcessionaria, string siglaUf)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("EmpresaFerroviaDespacho", empresaConcessionaria) && Restrictions.Eq("CodUnidadeFederativaFerrovia", siglaUf));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obt�m a empresa de ferrovia pelo CNPJ definido pelo grupo
		/// </summary>
		/// <param name="cnpj">CNPJ a ser consultado</param>
		/// <returns>Objeto EmpresaFerrovia</returns>
		public IEmpresa ObterEmpresaPorCnpjDoGrupo(string cnpj)
		{
			using (ISession session = OpenSession())
			{
				string hql = @"FROM SerieDespachoUf sdu
                              WHERE 
                              sdu.CnpjFerrovia = :cnpjFerrovia)";
				IQuery query = session.CreateQuery(hql);
				query.SetString("cnpjFerrovia", cnpj);
				IList<SerieDespachoUf> lista = query.List<SerieDespachoUf>();
				if (lista.Count > 0)
				{
					IEmpresa empresa = lista[0].EmpresaFerroviaDespacho;
					empresa.Estado = new Estado { Sigla = lista[0].CodUnidadeFederativaFerrovia };
					return empresa;
				}

				return null;
			}
		}

		/// <summary>
		/// Obt�m a serie do despacho uf pelo cnpj e pelo uf
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa emitente</param>
		/// <param name="uf">Unidade federativa da empresa</param>
		/// <returns>Retorna a ins�ncia do objeto</returns>
		public SerieDespachoUf ObterPorCnpjUf(string cnpj, string uf)
		{
			using (ISession session = OpenSession())
			{
				string hql =
					@"FROM SerieDespachoUf sdu
												INNER JOIN FETCH sdu.EmpresaFerroviaDespacho efd
												WHERE sdu.CnpjFerrovia = :cnpjFerrovia
												AND sdu.CodUnidadeFederativaFerrovia = :uf";
				IQuery query = session.CreateQuery(hql);
				query.SetString("cnpjFerrovia", cnpj);
				query.SetString("uf", uf);
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				IList<SerieDespachoUf> resultado = query.List<SerieDespachoUf>();
				return resultado.FirstOrDefault();
			}
		}
	}
}