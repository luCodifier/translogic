using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto.Despacho;
using Translogic.Modules.Core.Spd;
using Speed.Common;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    /// <summary>
    ///   Repositorio para <see cref="DespachoTranslogic" />
    /// </summary>
    public class DespachoTranslogicRepository : NHRepository<DespachoTranslogic, int?>, IDespachoTranslogicRepository
    {
        /// <summary>
        ///   Obter Dados Despacho
        /// </summary>
        /// <returns>Lista de dto Manutencao Despacho</returns>
        public IList<ManutencaoDespachoDto> ObterDadosDespacho()
        {
            var parametros = new List<Action<IQuery>>();
            var sb = new StringBuilder();

            sb.Append(
                @"FROM EquipeDiesel                                  ed
							INNER JOIN FETCH ed.ListaMaquinistaRallyDiesel mrd
							INNER JOIN FETCH mrd.ListaViagens              lv
							INNER JOIN FETCH mrd.Maquinista                maq
							INNER JOIN FETCH mrd.EquipeDiesel              ed
							LEFT JOIN FETCH lv.ListaLocomotivas            lloc
							LEFT JOIN FETCH ed.ListaPenalidadeTimeUp       lptu
						  WHERE lv.ViagemValidaCopa   = :viagemValidaCopa
							AND   lv.ViagemManobra      = :viagemManobra
							AND   ed.ChaveCorredor       = :chaveCorredor
							AND   lv.ViagemExcluidaCopa = :viagemExcluidaCopa
							AND   lv.DataPartida BETWEEN :dataInicio AND :dataTermino");

            sb.Append(
                @"FROM DespachoTranslogic                                  dt
							INNER JOIN FETCH dt.PedidoDerivado			   pd	
							INNER JOIN FETCH mrd.ListaViagens              lv
							INNER JOIN FETCH mrd.Maquinista                maq
							INNER JOIN FETCH mrd.EquipeDiesel              ed
							LEFT JOIN FETCH lv.ListaLocomotivas            lloc
							LEFT JOIN FETCH ed.ListaPenalidadeTimeUp       lptu
						  WHERE lv.ViagemValidaCopa   = :viagemValidaCopa
							AND   lv.ViagemManobra      = :viagemManobra
							AND   ed.ChaveCorredor       = :chaveCorredor
							AND   lv.ViagemExcluidaCopa = :viagemExcluidaCopa
							AND   lv.DataPartida BETWEEN :dataInicio AND :dataTermino");

            ISession session = OpenSession();

            IQuery query = session.CreateQuery(sb.ToString());
            query.SetResultTransformer(Transformers.DistinctRootEntity);

            foreach (var action in parametros)
            {
                action.Invoke(query);
            }

            return query.List<ManutencaoDespachoDto>();
        }

        /// <summary>
        ///   Obter Despacho pelo N�mero do Despacho e S�rie
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public IList<DespachoTranslogic> ObterDespachoPeloNumeroDespachoSerie(int numDespacho, string serie)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("SerieDespacho", "serieDespacho");
            criteria.Add(Restrictions.Eq("serieDespacho.SerieDespachoNum", serie));
            criteria.Add(Restrictions.Eq("NumeroDespacho", numDespacho));

            return ObterTodos(criteria);
        }

        /// <summary>
        ///   Obter Despacho pelo N�mero do Despacho e S�rie
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public IList<DespachoTranslogic> ObterDespachoPeloNumeroDespachoSerieIntercambio(int numDespacho, string serie)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("SerieDespachoSdi", serie));
            criteria.Add(Restrictions.Eq("NumDespIntercambio", numDespacho));

            return ObterTodos(criteria);
        }

        /// <summary>
        ///   Obter Despacho pelo ID do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public DespachoTranslogic ObterPorIdSemEstado(int idDespacho)
        {
            using (var session = SessionManager.OpenStatelessSession())
            {
                return session.Get<DespachoTranslogic>(idDespacho);
            }
        }

        /// <summary>
        ///   Obter data do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public DateTime? ObterDataDespachoPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select DP_DT from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<DateTime?>();
            }
        }

        /// <summary>
        ///   Obter NumeroDespacho do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public decimal ObterNumeroDespachoPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                var dp = session.CreateSQLQuery(string.Format("select DP_NUM_DSP from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<decimal>();
                return dp;
            }
        }

        /// <summary>
        ///   Obter NumDespIntercambio do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterNumDespIntercambioPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select DP_NUM_DPI from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        ///   Obter NumeroSerieDespacho do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterNumeroSerieDespachoPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select DP_NUM_SER from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        ///   Obter SerieDespacho do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterSerieDespachoPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select SK_IDT_SRD from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        ///   Obter SerieDespachoSdi do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public string ObterSerieDespachoSdiPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select DP_NUM_SDI from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<string>();
            }
        }

        /// <summary>
        ///   Obter NumeroDespachoUf do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterNumeroDespachoUfPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select DP_NUM_DSP_UF from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        ///   Obter SerieDespachoUf do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterSerieDespachoUfPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select SD_IDT_SUF from DESPACHO where DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        ///   Obter SerieDespachoNum do despacho pelo N�mero do Despacho
        /// </summary>
        /// <returns>Lista de Despacho</returns>
        public int? ObterSerieDespachoNumPorId(int numDespacho)
        {
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(string.Format("select S.SK_NUM_SRD from DESPACHO D INNER JOIN SERIE_DESPACHO S ON (D.SK_IDT_SRD = S.SK_IDT_SRD) where D.DP_ID_DP = {0}", numDespacho)).UniqueResult<int?>();
            }
        }

        /// <summary>
        /// Obt�m Dto contendo informa��es b�sicas do Despacho para manipula��o e consulta na gera��o do Processo de Seguro
        /// </summary>
        /// <param name="numeroDespacho">N�mero do Despacho</param>
        /// <param name="serieDespacho">S�rie do Despacho</param>
        /// <param name="numeroVagao">N�mero do Vag�o</param>
        /// <returns>Retorna Dto contendo informa��es b�sicas do Despacho para manipula��o e consulta na gera��o do Processo de Seguro</returns>
        public DespachoProcessoSeguroDto ObterDespachoProcessoSeguroDtoPorNumeroDespachoNumeroVagao(int numeroDespacho, string serieDespacho, string numeroVagao)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT   
                                DP.DP_ID_DP AS ""Id"", 
                                DP.DP_NUM_DSP AS ""NumeroDespacho"",
                                SD.SK_NUM_SRD AS ""Serie"" 
                            FROM 
                                ITEM_DESPACHO ID            
                                INNER JOIN DESPACHO DP ON (ID.DP_ID_DP = DP.DP_ID_DP)
                                INNER JOIN SERIE_DESPACHO SD ON (SD.SK_IDT_SRD = DP.SK_IDT_SRD)
                                INNER JOIN VAGAO VG ON (ID.VG_ID_VG = VG.VG_ID_VG)
                            WHERE 
                                DP.DP_NUM_DSP = {0}
                                AND SD.SK_NUM_SRD = '{1}'
                                AND VG.VG_COD_VAG = LPAD('{2}', 7, '0')
                            ORDER BY DP.DP_ID_DP DESC", numeroDespacho, serieDespacho, numeroVagao);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetMaxResults(1);
                query.AddScalar("Id", NHibernateUtil.Int32);
                query.AddScalar("NumeroDespacho", NHibernateUtil.Int32);
                query.AddScalar("Serie", NHibernateUtil.String);

                query.SetResultTransformer(Transformers.AliasToBean<DespachoProcessoSeguroDto>());

                return query.UniqueResult<DespachoProcessoSeguroDto>();

            }

            #endregion
        }


        /// <summary>
        ///   Obter Peso do Carregamento vinculado ao despacho e s�rie
        /// </summary>
        /// <returns>Peso do Carregamento vinculado ao despacho e s�rie</returns>
        public Double ObterPesoCarregamento(int numDespacho, string serie)
        {
            #region SQL Query
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT
                                COALESCE(SUM(DC.DC_QTD_INF), 0) AS ""PesoCarregamento""
                            FROM
                                DESPACHO DP,
                                SERIE_DESPACHO SD,
                                DETALHE_CARREGAMENTO DC,
                                ITEM_DESPACHO IDS
                            WHERE
                                DP.DP_NUM_DSP = {0}
                                AND SD.SK_NUM_SRD = '{1}'
                                AND DP.SK_IDT_SRD = SD.SK_IDT_SRD    
                                AND DP.DP_ID_DP = IDS.DP_ID_DP
                                AND IDS.DC_ID_CRG = DC.DC_ID_CRG", numDespacho, serie);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.AddScalar("PesoCarregamento", NHibernateUtil.Double);

                return query.UniqueResult<Double>();

            }

            #endregion
        }

        public DateTime? ObterDataDescargaNfe(int? idCte)
        {
            var sql = string.Format("select FN_GET_DATA_DESPACHO_CTE({0}) from dual", idCte);
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(sql).UniqueResult<DateTime?>();
            }
        }
 
        public DateTime? ObterDataDescargaNfe(int? numDespacho, string serieDespacho)
        {
            var sql = string.Format("select FN_GET_DATA_DESPACHO({0}, '{1}') from dual", numDespacho, serieDespacho);
            using (ISession session = OpenSession())
            {
                return session.CreateSQLQuery(sql).UniqueResult<DateTime?>();
            }
        }

        public bool VerificaPrecificacaoOrigem(int? idContratoHistorico)
        {
            using (var db = Dbs.NewDb(Db.Translogic))
            {
                var sql = string.Format("select PKG_CTE.VERIFICA_PRECIFICACAO_ORIGEM({0}) from dual", idContratoHistorico);
                return db.ExecuteInt32(sql).ToBoolean();
            }
        }

        public string ObterMensagemDataDespacho(int? idCte)
        {
            var data = ObterDataDescargaNfe(idCte);
            if (data == null)
                return null;
            else
                return "DT DESC: " + data.Value.ToString("dd/MM/yyyy") + " ??";
        }

        public string ObterMensagemDataDespacho(int? numDespacho, string serieDespacho)
        {
            var data = ObterDataDescargaNfe(numDespacho, serieDespacho);
            if (data == null)
                return null;
            else
                return "DT DESC: " + data.Value.ToString("dd/MM/yyyy") + " ??";
        }

    }
}