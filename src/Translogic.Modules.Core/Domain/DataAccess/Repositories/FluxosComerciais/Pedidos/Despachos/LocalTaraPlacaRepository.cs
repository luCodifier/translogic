namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using ALL.Core.AcessoDados;
    using Model.Via;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;

    /// <summary>
    /// Repositorio para <see cref="LocalTaraPlaca"/>
    /// </summary>
    public class LocalTaraPlacaRepository : NHRepository<LocalTaraPlaca, int?>, ILocalTaraPlacaRepository
    {
        /// <summary>
        /// Verifica se o local passado est� cadastrado
        /// </summary>
        /// <param name="local">Area operacional</param>
        /// <returns>Valor booleano</returns>
        public bool VerificarLocalExiste(IAreaOperacional local)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Local", local));
            LocalTaraPlaca retorno = ObterPrimeiro(criteria);
            return retorno != null;
        }
    }
}