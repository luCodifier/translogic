namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Pedidos;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="VagaoPedidoVigente"/>
	/// </summary>
	public class VagaoPedidoVigenteRepository : NHRepository<VagaoPedidoVigente, int?>, IVagaoPedidoVigenteRepository
	{
        /// <summary>
		/// Obt�m Vagao Pedido por id do Vafao
		/// </summary>
		/// <param name="idVagao">Id vagao do translogic</param>
		/// <returns>Retorna Vagao Pedido</returns>
		public IList<VagaoPedidoVigente> ObterVagaoPedidoPorVagao(int? idVagao)
		{
			if (idVagao == 0 || idVagao == null)
			{
				return null;
			}

			try
			{
				DetachedCriteria criteria = CriarCriteria();
				criteria.Add(Restrictions.Eq("Vagao.Id", idVagao));
				return this.ObterTodos(criteria);
			}
			catch (System.Exception)
			{
				return null;
			}
		}
	}
}