namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;
    using Model.FluxosComerciais.Pedidos;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Repositorio para <see cref="VagaoPedido"/>
	/// </summary>
	public class VagaoPedidoRepository : NHRepository<VagaoPedido, int?>, IVagaoPedidoRepository
	{
        /// <summary>
        /// Obt�m Vagao Pedido por id do Pedido
        /// </summary>
        /// <param name="pedidoId">Id do Pedido</param>
        /// <returns>Retorna Vagao Pedido</returns>
        public VagaoPedido ObterVagaoPedidoPorPedido(int pedidoId)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Pedido.Id", pedidoId));
            return ObterPrimeiro(criteria);
        }

		/// <summary>
		/// Obt�m Vagao Pedido por id do Vafao
		/// </summary>
		/// <param name="idVagao">Id vagao do translogic</param>
		/// <returns>Retorna Vagao Pedido</returns>
		public VagaoPedido ObterVagaoPedidoPorVagao(int? idVagao)
		{
			if (idVagao == 0 || idVagao == null)
			{
				return null;
			}

			try
			{
				DetachedCriteria criteria = CriarCriteria();
				criteria.Add(Restrictions.Eq("Vagao.Id", idVagao));
				return ObterPrimeiro(criteria);
			}
			catch (System.Exception)
			{
				return null;
			}
		}

		/// <summary>
		/// Obt�m lista de Vagao Pedido por ComposicaoVagao
		/// </summary>
		/// <param name="composicaoVagao">Objeto composicaoVagao</param>
		/// <returns>Retorna Vagao Pedido</returns>
		public IList<VagaoPedido> ObterVagoesPedidosPorComposicaoVagao(ComposicaoVagao composicaoVagao)
	    {
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Pedido.Id", composicaoVagao.Pedido.Id) && Restrictions.Eq("Vagao.Id", composicaoVagao.Vagao.Id));
			return ObterTodos(criteria);
	    }

		/// <summary>
        /// Obt�m Vagao Pedido por id do Pedido Derivado
        /// </summary>
        /// <param name="pedidoDerivadoId">Id do Pedido Derivado</param>
        /// <returns>Retorna Vagao Pedido</returns>
        public VagaoPedido ObterVagaoPedidoPorPedidoDerivado(int pedidoDerivadoId)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("PedidoDerivado.Id", pedidoDerivadoId));
            return ObterPrimeiro(criteria);
        }
	}
}