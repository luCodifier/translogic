namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="Pedido"/>
	/// </summary>
	public class PedidoRepository : NHRepository<Pedido, int?>, IPedidoRepository
	{
	}
}