namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="PedidoTransporte"/>
	/// </summary>
	public class PedidoTransporteRepository : NHRepository<PedidoTransporte, int?>, IPedidoTransporteRepository
	{
	}
}