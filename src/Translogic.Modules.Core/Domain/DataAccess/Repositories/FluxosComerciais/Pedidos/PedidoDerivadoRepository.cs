namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;

    /// <summary>
	/// Repositorio para <see cref="PedidoDerivado"/>
	/// </summary>
	public class PedidoDerivadoRepository : NHRepository<PedidoDerivado, int?>, IPedidoDerivadoRepository
	{
        /// <summary>
        /// Obtem os pedidos por ids
        /// </summary>
        /// <param name="ids">Ids dos pedidos derivados</param>
        /// <returns>Retorna a lista de pedidos derivados</returns>
        public IList<PedidoDerivado> ObterPorIds(List<int> ids)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.In("Id", ids));

            return ObterTodos(criteria);
        }
	}
}