namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories;
    using Translogic.Core.Infrastructure.Web;
    using System.Text;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
    using ALL.Core.Dominio;
    using System;
    using System.Collections.Generic;
    /// <summary>
    /// Repositorio para <see cref="Baldeio"/>
    /// </summary>
    public class BaldeioRepository : NHRepository<Baldeio, int?>, IBaldeioRepository
    {
        public ResultadoPaginado<BaldeioDto> ObterConsultaBaldeio(  DetalhesPaginacaoWeb detalhesPaginacaoWeb,
                                                                    string dataInicial,
                                                                    string dataFinal,
                                                                    string local,
                                                                    string vagaoCedente,
                                                                    string vagaoRecebedor)
        {
            #region Query
            var sql = new StringBuilder(@" SELECT DISTINCT   NVL(BD.BD_IDT_BLD,0)        AS ""IdBaldeio""
                                                   ,NVL(ARQ.ID_ANEXO_BALDEIO,0) AS ""IdArquivoBaldeio""
                                                   ,AOP.AO_COD_AOP              AS ""Local"" 
                                                   ,BD.BD_DAT_OCR               AS ""DataBaldeio""
                                                   ,VG2.VG_COD_VAG              AS ""VagaoCedente""
                                                   ,VG.VG_COD_VAG               AS ""VagaoRecebedor""
                                                   ,BD.BD_QTD_BLD               AS ""QuantidadeBaldeada"" 
                                                   ,DED.DD_QTD_PAD              AS ""QuantidadeVagaoCedente"" 
                                                   ,DESP.DP_NUM_DSP             AS ""Despacho"" 
                                                   ,DESP.DP_DT                  AS ""DataDespacho""                                                    
                                            FROM   CARREGAMENTO DC2, 
                                                   BALDEIO BD, 
                                                   VAGAO VG, 
                                                   VAGAO VG2, 
                                                   DETALHE_CARREGAMENTO DCC, 
                                                   DESCARREGAMENTO DE, 
                                                   DETALHE_DESCARREGAMENTO DED, 
                                                   AREA_OPERACIONAL AOP, 
                                                   DESPACHO DESP, 
                                                   ITEM_DESPACHO IDESP,
                                                   ARQUIVO_BALDEIO ARQ
                                            WHERE  DC2.BD_IDT_BLD = BD.BD_IDT_BLD 
                                                   AND VG.VG_ID_VG = DCC.VG_ID_VG 
                                                   AND DC2.CG_ID_CAR = DCC.CG_ID_CAR 
                                                   AND DE.BD_IDT_BLD = BD.BD_IDT_BLD 
                                                   AND DE.DS_ID_DSC = DED.DS_ID_DSC 
                                                   AND VG2.VG_ID_VG = DED.VG_ID_VG 
                                                   AND AOP.AO_ID_AO = DC2.AO_ID_AO 
                                                   AND DESP.PT_ID_ORT = DC2.PT_ID_ORT 
                                                   AND IDESP.DP_ID_DP = DESP.DP_ID_DP 
                                                   AND IDESP.VG_ID_VG = DCC.VG_ID_VG 
                                                   AND BD.BD_IDT_BLD = ARQ.BD_ID_BLD (+)");
            #endregion

            #region WHERE
            if (!string.IsNullOrWhiteSpace(dataInicial) && !string.IsNullOrWhiteSpace(dataFinal))
            {
                sql.AppendFormat(@" AND BD.BD_DAT_OCR BETWEEN TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                    AND TO_DATE('{1} 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ", dataInicial.Substring(0, 10), dataFinal.Substring(0, 10));
            }

            if (!string.IsNullOrWhiteSpace(local))
            {
                sql.AppendFormat("AND AOP.AO_COD_AOP = '{0}'", local.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(vagaoCedente))
            {
                sql.AppendFormat("AND VG2.VG_COD_VAG = '{0}' ", vagaoCedente);
            }

            if (!string.IsNullOrWhiteSpace(vagaoRecebedor))
            {
                sql.AppendFormat("AND VG.VG_COD_VAG = '{0}'", vagaoRecebedor);
            }

            #endregion


            #region OrderBy

            // Order by (direcionamento (ASC/DESC))
            sql.Append(" ORDER BY BD.BD_DAT_OCR, NVL(BD.BD_IDT_BLD,0)  ");

            #endregion

            #region Acesso a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                query.SetResultTransformer(Transformers.AliasToBean<BaldeioDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<BaldeioDto>();

                var result = new ResultadoPaginado<BaldeioDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
            #endregion
        }

        public IEnumerable<BaldeioDto> ObterConsultaBaldeioExportar(string dataInicial, 
                                                                    string dataFinal, 
                                                                    string local, 
                                                                    string vagaoCedente, 
                                                                    string vagaoRecebedor)
        {

            #region Query
            var sql = new StringBuilder(@" SELECT DISTINCT  NVL(BD.BD_IDT_BLD,0) AS ""IdBaldeio""
                                                   ,AOP.AO_COD_AOP              AS ""Local"" 
                                                   ,BD.BD_DAT_OCR               AS ""DataBaldeio""
                                                   ,VG2.VG_COD_VAG              AS ""VagaoCedente""
                                                   ,VG.VG_COD_VAG               AS ""VagaoRecebedor""
                                                   ,BD.BD_QTD_BLD               AS ""QuantidadeBaldeada"" 
                                                   ,DED.DD_QTD_PAD              AS ""QuantidadeVagaoCedente"" 
                                                   ,DESP.DP_NUM_DSP             AS ""Despacho"" 
                                                   ,DESP.DP_DT                  AS ""DataDespacho""   
                                                   ,CASE WHEN ARQ.ID_ANEXO_BALDEIO IS NULL THEN 'N'
                                                   ELSE 'S' END AS ""TemAnexo""                                                    
                                                   FROM   CARREGAMENTO DC2, 
                                                           BALDEIO BD, 
                                                           VAGAO VG, 
                                                           VAGAO VG2, 
                                                           DETALHE_CARREGAMENTO DCC, 
                                                           DESCARREGAMENTO DE, 
                                                           DETALHE_DESCARREGAMENTO DED, 
                                                           AREA_OPERACIONAL AOP, 
                                                           DESPACHO DESP, 
                                                           ITEM_DESPACHO IDESP,
                                                           ARQUIVO_BALDEIO ARQ
                                                   WHERE  DC2.BD_IDT_BLD = BD.BD_IDT_BLD 
                                                           AND VG.VG_ID_VG = DCC.VG_ID_VG 
                                                           AND DC2.CG_ID_CAR = DCC.CG_ID_CAR 
                                                           AND DE.BD_IDT_BLD = BD.BD_IDT_BLD 
                                                           AND DE.DS_ID_DSC = DED.DS_ID_DSC 
                                                           AND VG2.VG_ID_VG = DED.VG_ID_VG 
                                                           AND AOP.AO_ID_AO = DC2.AO_ID_AO 
                                                           AND DESP.PT_ID_ORT = DC2.PT_ID_ORT 
                                                           AND IDESP.DP_ID_DP = DESP.DP_ID_DP 
                                                           AND IDESP.VG_ID_VG = DCC.VG_ID_VG 
                                                           AND BD.BD_IDT_BLD = ARQ.BD_ID_BLD (+) ");
            #endregion

            #region WHERE
            if (!string.IsNullOrWhiteSpace(dataInicial) && !string.IsNullOrWhiteSpace(dataFinal))
            {
                sql.AppendFormat(@" AND BD.BD_DAT_OCR BETWEEN TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                    AND TO_DATE('{1} 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ", dataInicial.Substring(0, 10), dataFinal.Substring(0, 10));
            }

            if (!string.IsNullOrWhiteSpace(local))
            {
                sql.AppendFormat("AND AOP.AO_COD_AOP = '{0}'", local.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(vagaoCedente))
            {
                sql.AppendFormat("AND VG2.VG_COD_VAG = '{0}' ", vagaoCedente);
            }

            if (!string.IsNullOrWhiteSpace(vagaoRecebedor))
            {
                sql.AppendFormat("AND VG.VG_COD_VAG = '{0}'", vagaoRecebedor);
            }

            #endregion

            #region OrderBy

            // Order by (direcionamento (ASC/DESC))
            sql.Append(" ORDER BY BD.BD_DAT_OCR, NVL(BD.BD_IDT_BLD,0)  ");

            #endregion

            #region Acesso a base

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<BaldeioDto>());

                return query.List<BaldeioDto>();

            }
            #endregion
        }
    }
}