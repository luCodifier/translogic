﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Atrasos
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.FluxosComerciais.Atrasos;
    using Model.FluxosComerciais.Atrasos.Repositories;
    using NHibernate;
    using NHibernate.Linq;

    public class AvisoAtrasoRepository : NHRepository<AvisoAtraso, int>, IAvisoAtrasoRepository
    {
        public ResultadoPaginado<AvisoAtraso> ObterAtrasos(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, TipoAtraso tipo, ResponsavelAtraso responsavel)
        {
            using (var session = OpenSession())
            {
                var query = session.QueryOver<AvisoAtraso>()
                    .Fetch(a => a.Cliente)
                    .Eager
                    .Where(a => a.Data >= dataInicial && a.Data < dataFinal);

                if (tipo != null)
                {
                    query = query.And(a => a.Tipo.Id == tipo.Id);
                }

                if (responsavel != null)
                {
                    query = query.And(a => a.Responsavel.Id == responsavel.Id);
                }

                var count = query.RowCount();
                if (pagination.Inicio.HasValue)
                {
                    query = (IQueryOver<AvisoAtraso, AvisoAtraso>) query.Skip(pagination.Inicio.Value);
                }
                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query = (IQueryOver<AvisoAtraso, AvisoAtraso>) query.Take(limit);
                }

                return new ResultadoPaginado<AvisoAtraso> { Total = count, Items = query.List() };
            }

        }

        public AvisoAtraso Salvar(AvisoAtraso avisoAtraso)
        {
            return base.InserirOuAtualizar(avisoAtraso);
        }

        public IEnumerable<MotivoAtraso> ObterMotivos()
        {
            using (var session = OpenSession())
            {
                return session.Query<MotivoAtraso>()
                    .ToList();
            }
        }

        public IEnumerable<TipoAtraso> ObterTipos()
        {
            using (var session = OpenSession())
            {
                return session.Query<TipoAtraso>()
                    .ToList();
            }
        }

        public IEnumerable<ResponsavelAtraso> ObterResponsaveis()
        {
            using (var session = OpenSession())
            {
                return session.Query<ResponsavelAtraso>()
                    .ToList();
            }
        }
    }
}