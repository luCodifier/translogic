﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories;
using System.Text;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.ControlePerdas
{
    /// <summary>
    /// Implementação de repositório de Recomendação de Vagão - (Gestão de regras de recomendação)
    /// </summary>
    public class RecomendacaoVagaoRepository : NHRepository<RecomendacaoVagao, int>, IRecomendacaoVagaoRepository
    {
        /// <summary>
        /// Retorna o registro de RecomendacaoVagao, de acordo com o ID da Causa de Dano 
        /// </summary>
        /// <param name="idCausaSeguro">ID da Causa de Dano</param>
        /// <returns>Retorna o registro de RecomendacaoVagao, de acordo com o ID da Causa de Dano</returns>
        public RecomendacaoVagao ObterPorIdCausaSeguro(int idCausaSeguro)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.CreateAlias("CausaSeguro", "cs");
            criteria.Add(Restrictions.Eq("cs.Id", idCausaSeguro));

            return ObterPrimeiro(criteria);
        }
    }
}