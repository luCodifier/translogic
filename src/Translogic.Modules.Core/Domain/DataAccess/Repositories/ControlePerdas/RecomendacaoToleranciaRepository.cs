﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;
using Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories;
using System.Text;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.ControlePerdas
{
    /// <summary>
    /// Implementação de repositório de Recomendação de Tolerância - (Gestão de regras de recomendação)
    /// </summary>
    public class RecomendacaoToleranciaRepository : NHRepository<RecomendacaoTolerancia, int>, IRecomendacaoToleranciaRepository
    {
        /// <summary>
        /// Retorna o registro de RecomendacaoTolerancia, de acordo com o código de Malha
        /// </summary>
        /// <param name="codMalha">Código de Malha (LG, MS, MN)</param>
        /// <returns>Retorna o registro de RecomendacaoTolerancia, de acordo com o código de Malha</returns>
        public RecomendacaoTolerancia ObterPodCodMalha(string codMalha)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CodMalha", codMalha));

            return ObterPrimeiro(criteria);
        }
    }
}