﻿

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.ConsultaTrem
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Dto.ConsultaTrem;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem;
    using Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories;
    using System.Text;
    using System;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Extensions;
    using System.Collections.Generic;
    using NHibernate.Criterion;

    public class PerguntaChecklistRepository : NHRepository<PerguntaChecklist, int>, IPerguntaChecklistRepository
    {
        public ResultadoPaginado<PerguntaCheckListDto> ObterConsultaPerguntasChecklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? idAgrupador, string tipoPergunta)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT PCK.ID_PRGCK       AS ""IdPergunta""
                                                ,PCK.AGRUPADOR      AS ""IdAgrupador""
                                                ,PCK.DESCRICAO      AS ""Pergunta""
                                                ,PCK.ORDEM          AS ""OrdemPergunta"" 
                                                ,PCK.TIPO           AS ""TipoPerguntaCheckList""
                                            FROM PERGUNTAS_CHECKLIST PCK
                                          ");

            #endregion

            #region Filtros
            sql.AppendFormat(" WHERE PCK.TIPO = '{0}' ", tipoPergunta);

            if (idAgrupador.HasValue)
            {
                sql.AppendFormat(" AND PCK.AGRUPADOR = {0} ", idAgrupador);
            }

            #endregion

            #region Order by
            sql.AppendFormat(" ORDER BY PCK.ORDEM");

            #endregion

            #region Exection

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                query.SetResultTransformer(Transformers.DistinctRootEntity);
                query.SetResultTransformer(Transformers.AliasToBean<PerguntaCheckListDto>());


                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<PerguntaCheckListDto>();

                var result = new ResultadoPaginado<PerguntaCheckListDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
            #endregion
        }


        public IList<PerguntaCheckListDto> ObterPerguntasChecklistPorTipo(string tipo)
        {
            #region Qry
            var sql = new StringBuilder(@"SELECT PCK.ID_PRGCK       AS ""IdPergunta""
                                                ,PCK.AGRUPADOR      AS ""IdAgrupador""
                                                ,PCK.DESCRICAO      AS ""Pergunta""
                                                ,PCK.ORDEM          AS ""OrdemPergunta""
                                                ,PCK.TIPO           AS ""TipoPerguntaCheckList""
                                            FROM PERGUNTAS_CHECKLIST PCK");
            #endregion

            #region Filtro
            sql.AppendFormat(" WHERE PCK.TIPO = '{0}'  ORDER BY PCK.ORDEM", tipo);
            #endregion

            #region Execution

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<PerguntaCheckListDto>());
                return query.List<PerguntaCheckListDto>();
            }
            #endregion

        }
    }
}