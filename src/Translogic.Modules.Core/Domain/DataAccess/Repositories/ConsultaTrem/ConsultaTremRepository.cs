﻿using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
using Translogic.Core.Infrastructure.Web;
using System.Text;
using ALL.Core.AcessoDados;
using System;
using NHibernate.Transform;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;
using Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories;
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.ConsultaTrem
{
    public class ConsultaTremRepository : NHRepository, IConsultaTremRepository
    {
        public ResultadoPaginado<ConsultaTremDto> ObterConsultaTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {
            var sqlWihtoutOrderBy = new StringBuilder();
            var sql = new StringBuilder(@"SELECT 
                                           IdTrem                                                                                                           AS ""IdTrem""                                        
                                        ,  OS                                                                                                               AS ""NumOs""
                                        , TREM                                                                                                              AS ""Trem""
                                        , ORG                                                                                                               AS ""Origem""
                                        , DST                                                                                                               AS ""Destino""
                                        , PKG_UTIL.SF_HORASMINUTOS( nvl (round((sysdate-desde),2),round((sysdate-DataHora),2)))                             AS ""HorasAPT1""
                                        ,nvl (round((sysdate-desde)*24,2),round((sysdate-DataHora)*24,2))                                                   AS ""HorasAPT""
                                        , partida_prog                                                                                                      AS ""PartidaPrevisao""
                                        , partida_real                                                                                                      AS ""PartidaReal""
                                        , (
                                            case when q.tr_stt_trm = 'C' and POS_VIRTUAL is null and partida_real is not null 
                                            then 'Circulando'
                                            else 
                                                case when q.tr_stt_trm = 'P'
                                                then 'Em Formacao'
                                                else 
                                                    case when q.tr_stt_trm = 'R' or (q.tr_stt_trm = 'C' and POS_VIRTUAL is not null)
                                                    then 'Parado'
                                                    else 
                                                        case when q. mt_id_mov is null and  q.tr_stt_trm = 'C'
          
                                                                then
                                                        'Liberado'
                                                        end
                                                    end
                                                end
                                            end
                                        )                                                                                                                   AS ""Situacao""
                                        , LOCAL_ATUAL                                                                                                       AS ""LocalAtual""
                                        , DataHora                                                                                                          AS ""DataHora""
                                        , POS_VIRTUAL                                                                                                       AS ""PosicaoVirtual""
                                        , Desde                                                                                                             AS ""Desde""
                                        , T_LOCOS                                                                                                           AS ""TLocos""
                                        , LT                                                                                                                AS ""LT""
                                        , LR                                                                                                                AS ""LR""
                                        , T_VAGAO                                                                                                           AS ""TVagoes""
                                        , VC                                                                                                                AS ""VC""
                                        , VV                                                                                                                AS ""VZ""
                                        , COMPRIMENTO                                                                                                       AS ""Comprimento""
                                        , PESO                                                                                                              AS ""Peso""
                                        from
                                    (
                                        select T.TR_ID_TRM as IdTrem,T.OS,T.MT_ID_MOV, T.TREM, T.COMPRIMENTO, T.TR_ID_TRM, T.PESO, T.T_LOCOS, T.LT, T.LR, T.T_VAGAO, T.VC
                                        , T.VV
                                        , T.POS_VIRTUAL
                                        , T.ORG, T.DST
                                        , min(mt.mt_dts_prg) as partida_prog
                                        , min(mt.mt_dts_ral) as partida_real
                                        , max(t.tr_stt_trm) as tr_stt_trm
                                        , MAX(ap.ao_cod_aop) as LOCAL_ATUAL
                                        , max(umt.mt_dtc_ral) as DataHora
                                        , max(t.tr_dat_lbr) as data_liberacao
                                        , max(t.ao_id_ao_pvt) as ao_id_ao_pvt
                                        , max(t.tr_id_trm) as tr_id_trm
                                        , max(CP_DAT_INC) as CP_DAT_INC
                                        , max(Desde) as Desde
                                            , max(Desde2) as Desde2
                                        from (
    
                                        select 
                                            os.x1_nro_os os                                            
                                            , t.tr_pfx_trm trem
                                            , max(t.tr_stt_trm) as tr_stt_trm
                                            , max(t.ao_id_ao_pvt) as ao_id_ao_pvt
                                            , c.cp_cmp_cps comprimento
                                            , max(t.tr_id_trm) as tr_id_trm
                                            , MAX(c.cp_ton_brt) peso
                                            , COUNT(distinct cl.PL_IDT_PTL) T_LOCOS
                                            , count(distinct (case when st.sa_ind_tpo = 'T' then st.sa_idt_stt end)) as LT
                                            , count(distinct (case when st.sa_ind_tpo = 'R' then st.sa_idt_stt end)) as LR
                                            , COUNT(distinct  cv.CV_IDT_CVG) T_VAGAO
                                            , count(distinct (case when cv.cv_ind_car = 'S' then CV_IDT_CVG end)) as VC
                                            , count(distinct (case when cv.cv_ind_car = 'N' then CV_IDT_CVG end)) as VV
                                            , MAX(a.ao_cod_aop) Pos_virtual
                                            , MAX(orig.ao_cod_aop) org
                                            , MAX(dest.ao_cod_aop) dst
                                            , max(mt_id_mov) mt_id_mov
                                            , max(tr_dat_lbr) tr_dat_lbr
                                            , max(c.CP_DAT_INC) CP_DAT_INC
                                            , max( nvl(nvl(nvl(
                                                (
                                                    select mt.mt_dtc_ral 
                                                    from movimentacao_trem mt, area_operacional  f
                                                    where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao_inf
                                                ),
                                                (
                                                    select mt.mt_dtc_ral
                                                    from movimentacao_trem mt, area_operacional  f
                                                    where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao)
                                                ),
                                                (
                                                    select t.tr_dat_lbr 
                                                    from movimentacao_trem mt
                                                    where mt.tr_id_trm = t.tr_id_trm and t.tr_stt_trm = 'C' having min(mt.mt_dts_ral) is null)
                                                ),
                                                (
                                                    select c.cp_dat_inc 
                                                    from trem tr
                                                    where tr.tr_id_trm = t.tr_id_trm
                                                    and t.tr_stt_trm = 'P')
                                                )
                
                                                )    Desde
        
                                                , max( COALESCE(
                                                        (
                                                            select mt.mt_dtc_ral 
                                                            from movimentacao_trem mt, area_operacional  f
                                                            where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao_inf
                                                        ),
                                                        (
                                                            select mt.mt_dtc_ral
                                                            from movimentacao_trem mt, area_operacional  f
                                                            where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao
                                                        )
                                                        ,
                                                        (
                                                            select t.tr_dat_lbr 
                                                            from movimentacao_trem mt
                                                            where mt.tr_id_trm = t.tr_id_trm and t.tr_stt_trm = 'C' having min(mt.mt_dts_ral) is null
                                                        )
                                                        ,
                                                        (
                                                            select c.cp_dat_inc 
                                                            from trem tr
                                                            where tr.tr_id_trm = t.tr_id_trm
                                                            and t.tr_stt_trm = 'P'
                                                        )
            
                                                    ) 
                                                )   Desde2
                                            from t2_os os
                                            inner join trem t on (os.x1_id_os = t.of_id_osv  and t.tr_stt_trm <> 'E' )
                                            inner join composicao c on (c.cp_stt_cmp = 'A'  and c.tr_id_trm = t.tr_id_trm)
                                            inner join area_operacional orig on (orig.ao_id_ao = t.ao_id_ao_org)
                                            inner join area_operacional dest on (dest.ao_id_ao = t.ao_id_ao_dst)
                                            left join  area_operacional a on (t.ao_id_ao_pvt = a.ao_id_ao)
                                            left join compvagao_vig cv on (cv.cp_id_cps = c.cp_id_cps)
                                            left join comp_locomot_vig cl on (cl.cp_id_cps = c.cp_id_cps)
                                            LEFT JOIN situacao_tracao_vig st ON (st.pl_idt_ptl = cl.pl_idt_ptl)                                           
        
                                            -- adicionar filtros aqui
                                            --where t.mt_id_mov is null and t.tr_stt_trm = 'C' 

                                            WHERE 1 = 1");

            //OS
            if (!(string.IsNullOrEmpty(os)))
            {
                sql.AppendLine(" AND OS.X1_NRO_OS = '" + os + "'");
            }

            //ORIGEM
            if (!(string.IsNullOrEmpty(origem)))
            {
                sql.AppendLine(" AND orig.AO_COD_AOP = '" + origem + "'");
            }

            //DESTINO
            if (!(string.IsNullOrEmpty(destino)))
            {
                sql.AppendLine(" AND dest.AO_COD_AOP = '" + destino + "'");
            }

            //PREFIXO
            if (!(string.IsNullOrEmpty(prefixo)))
            {
                sql.AppendLine(" AND t.TR_PFX_TRM = '" + prefixo + "'");
            }

            //PREFIXOS EXCLUIDOS
            if (!string.IsNullOrWhiteSpace(prefixosExcluir))
            {
                var ArPrefixos = prefixosExcluir.Split(',');
                var prefixosConcats = string.Empty;

                for (int i = 0; i < ArPrefixos.Length; i++)
                {
                    if (i == 0)
                    {
                        prefixosConcats = "'" + ArPrefixos[i].ToString() + "'";
                    }
                    else
                    {
                        prefixosConcats = prefixosConcats + ",'" + ArPrefixos[i].ToString() + "'";
                    }
                }

                sql.AppendLine(" AND SUBSTR(t.TR_PFX_TRM, 1, 1) NOT IN (" + prefixosConcats + ")");

            }

            //PREFIXO
            if (!(string.IsNullOrEmpty(prefixo)))
            {
                sql.AppendLine(" AND t.TR_PFX_TRM = '" + prefixo + "'");
            }

            sql.AppendLine(@" GROUP BY os.x1_nro_os,t.tr_pfx_trm, c.cp_cmp_cps
                            ) t

                            left join movimentacao_trem mt on (mt.tr_id_trm = t.tr_id_trm) -- movimentações do trem
                            left join movimentacao_trem umt on (umt.tr_id_trm = t.tr_id_trm and umt.mt_id_mov = t.mt_id_mov) -- ultima movimentacao trem
                            LEFT JOIN area_operacional ap ON (ap.ao_id_ao = umt.ao_id_ao)

                            group by T.MT_ID_MOV, T.OS, T.TREM, T.COMPRIMENTO, T.TR_ID_TRM, T.PESO, T.T_LOCOS, T.LT, T.LR, T.T_VAGAO, T.VC, T.VV, T.POS_VIRTUAL, T.ORG, T.DST
                            ) q WHERE 1 = 1");

            //Situação
            if (!(string.IsNullOrEmpty(situacao)))
            {
                switch (situacao)
                {
                    case "C":
                        sql.AppendLine(" AND q.tr_stt_trm = 'C' and POS_VIRTUAL is null and partida_real is not null");
                        break;
                    case "P":
                        sql.AppendLine(" AND q.tr_stt_trm = 'P'");
                        break;
                    case "R":
                        sql.AppendLine(" AND q.tr_stt_trm = 'R' or (q.tr_stt_trm = 'C' and POS_VIRTUAL is not null)");
                        break;
                    case "L":
                        sql.AppendLine(" AND q. mt_id_mov is null and  q.tr_stt_trm = 'C'");
                        break;
                    default:
                        break;
                }

            }
            //R - Parado
            //P - Em Formação
            //C - Circulando
            //L - Liberado

            //TEMPOAPT
            if (!(string.IsNullOrEmpty(tempoAPT)))
            {
                sql.AppendLine(" AND nvl (round((sysdate-desde)*24,2),round((sysdate-DataHora)*24,2)) >= " + tempoAPT);
            }

            sqlWihtoutOrderBy.AppendLine(sql.ToString());

            sql.AppendLine(" ORDER BY 10 ASC, 7 ASC");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlWihtoutOrderBy.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<ConsultaTremDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<ConsultaTremDto>();

                var result = new ResultadoPaginado<ConsultaTremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IEnumerable<ConsultaTremDto> ObterConsultaTremExportar(string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir)
        {

            #region Query
            var sql = new StringBuilder(@"SELECT 
                                           IdTrem                                                                                                           AS ""IdTrem""
                                        ,  OS                                                                                                               AS ""NumOs""
                                        , TREM                                                                                                              AS ""Trem""
                                        , ORG                                                                                                               AS ""Origem""
                                        , DST                                                                                                               AS ""Destino""
                                        , PKG_UTIL.SF_HORASMINUTOS( nvl (round((sysdate-desde),2),round((sysdate-DataHora),2)))                             AS ""HorasAPT1""
                                        ,nvl (round((sysdate-desde)*24,2),round((sysdate-DataHora)*24,2))                                                   AS ""HorasAPT""
                                        , partida_prog                                                                                                      AS ""PartidaPrevisao""
                                        , partida_real                                                                                                      AS ""PartidaReal""
                                        , (
                                            case when q.tr_stt_trm = 'C' and POS_VIRTUAL is null and partida_real is not null 
                                            then 'Circulando'
                                            else 
                                                case when q.tr_stt_trm = 'P'
                                                then 'Em Formacao'
                                                else 
                                                    case when q.tr_stt_trm = 'R' or (q.tr_stt_trm = 'C' and POS_VIRTUAL is not null)
                                                    then 'Parado'
                                                    else 
                                                        case when q. mt_id_mov is null and  q.tr_stt_trm = 'C'
          
                                                                then
                                                        'Liberado'
                                                        end
                                                    end
                                                end
                                            end
                                        )                                                                                                                   AS ""Situacao""
                                        , LOCAL_ATUAL                                                                                                       AS ""LocalAtual""
                                        , DataHora                                                                                                          AS ""DataHora""
                                        , POS_VIRTUAL                                                                                                       AS ""PosicaoVirtual""
                                        , Desde                                                                                                             AS ""Desde""
                                        , T_LOCOS                                                                                                           AS ""TLocos""
                                        , LT                                                                                                                AS ""LT""
                                        , LR                                                                                                                AS ""LR""
                                        , T_VAGAO                                                                                                           AS ""TVagoes""
                                        , VC                                                                                                                AS ""VC""
                                        , VV                                                                                                                AS ""VZ""
                                        , COMPRIMENTO                                                                                                       AS ""Comprimento""
                                        , PESO                                                                                                              AS ""Peso""
                                        from
                                    (
                                        select T.TR_ID_TRM as IdTrem,T.OS,T.MT_ID_MOV, T.TREM, T.COMPRIMENTO, T.TR_ID_TRM, T.PESO, T.T_LOCOS, T.LT, T.LR, T.T_VAGAO, T.VC
                                        , T.VV
                                        , T.POS_VIRTUAL
                                        , T.ORG, T.DST
                                        , min(mt.mt_dts_prg) as partida_prog
                                        , min(mt.mt_dts_ral) as partida_real
                                        , max(t.tr_stt_trm) as tr_stt_trm
                                        , MAX(ap.ao_cod_aop) as LOCAL_ATUAL
                                        , max(umt.mt_dtc_ral) as DataHora
                                        , max(t.tr_dat_lbr) as data_liberacao
                                        , max(t.ao_id_ao_pvt) as ao_id_ao_pvt
                                        , max(t.tr_id_trm) as tr_id_trm
                                        , max(CP_DAT_INC) as CP_DAT_INC
                                        , max(Desde) as Desde
                                            , max(Desde2) as Desde2
                                        from (
    
                                        select 
                                            os.x1_nro_os os
                                            , t.tr_pfx_trm trem
                                            , max(t.tr_stt_trm) as tr_stt_trm
                                            , max(t.ao_id_ao_pvt) as ao_id_ao_pvt
                                            , c.cp_cmp_cps comprimento
                                            , max(t.tr_id_trm) as tr_id_trm
                                            , MAX(c.cp_ton_brt) peso
                                            , COUNT(distinct cl.PL_IDT_PTL) T_LOCOS
                                            , count(distinct (case when st.sa_ind_tpo = 'T' then st.sa_idt_stt end)) as LT
                                            , count(distinct (case when st.sa_ind_tpo = 'R' then st.sa_idt_stt end)) as LR
                                            , COUNT(distinct  cv.CV_IDT_CVG) T_VAGAO
                                            , count(distinct (case when cv.cv_ind_car = 'S' then CV_IDT_CVG end)) as VC
                                            , count(distinct (case when cv.cv_ind_car = 'N' then CV_IDT_CVG end)) as VV
                                            , MAX(a.ao_cod_aop) Pos_virtual
                                            , MAX(orig.ao_cod_aop) org
                                            , MAX(dest.ao_cod_aop) dst
                                            , max(mt_id_mov) mt_id_mov
                                            , max(tr_dat_lbr) tr_dat_lbr
                                            , max(c.CP_DAT_INC) CP_DAT_INC
                                            , max( nvl(nvl(nvl(
                                                (
                                                    select mt.mt_dtc_ral 
                                                    from movimentacao_trem mt, area_operacional  f
                                                    where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao_inf
                                                ),
                                                (
                                                    select mt.mt_dtc_ral
                                                    from movimentacao_trem mt, area_operacional  f
                                                    where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao)
                                                ),
                                                (
                                                    select t.tr_dat_lbr 
                                                    from movimentacao_trem mt
                                                    where mt.tr_id_trm = t.tr_id_trm and t.tr_stt_trm = 'C' having min(mt.mt_dts_ral) is null)
                                                ),
                                                (
                                                    select c.cp_dat_inc 
                                                    from trem tr
                                                    where tr.tr_id_trm = t.tr_id_trm
                                                    and t.tr_stt_trm = 'P')
                                                )
                
                                                )    Desde
        
                                                , max( COALESCE(
                                                        (
                                                            select mt.mt_dtc_ral 
                                                            from movimentacao_trem mt, area_operacional  f
                                                            where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao_inf
                                                        ),
                                                        (
                                                            select mt.mt_dtc_ral
                                                            from movimentacao_trem mt, area_operacional  f
                                                            where mt.tr_id_trm = t.tr_id_trm and f.ao_id_ao = mt.ao_id_ao and t.ao_id_ao_pvt = f.ao_id_ao
                                                        )
                                                        ,
                                                        (
                                                            select t.tr_dat_lbr 
                                                            from movimentacao_trem mt
                                                            where mt.tr_id_trm = t.tr_id_trm and t.tr_stt_trm = 'C' having min(mt.mt_dts_ral) is null
                                                        )
                                                        ,
                                                        (
                                                            select c.cp_dat_inc 
                                                            from trem tr
                                                            where tr.tr_id_trm = t.tr_id_trm
                                                            and t.tr_stt_trm = 'P'
                                                        )
            
                                                    ) 
                                                )   Desde2
                                            from t2_os os
                                            inner join trem t on (os.x1_id_os = t.of_id_osv  and t.tr_stt_trm <> 'E' )
                                            inner join composicao c on (c.cp_stt_cmp = 'A'  and c.tr_id_trm = t.tr_id_trm)
                                            inner join area_operacional orig on (orig.ao_id_ao = t.ao_id_ao_org)
                                            inner join area_operacional dest on (dest.ao_id_ao = t.ao_id_ao_dst)
                                            left join  area_operacional a on (t.ao_id_ao_pvt = a.ao_id_ao)
                                            left join compvagao_vig cv on (cv.cp_id_cps = c.cp_id_cps)
                                            left join comp_locomot_vig cl on (cl.cp_id_cps = c.cp_id_cps)
                                            LEFT JOIN situacao_tracao_vig st ON (st.pl_idt_ptl = cl.pl_idt_ptl)
        
                                            -- adicionar filtros aqui
                                            --where t.mt_id_mov is null and t.tr_stt_trm = 'C' 

                                            WHERE 1 = 1");

            //OS
            if (!(string.IsNullOrEmpty(os)))
            {
                sql.AppendLine(" AND OS.X1_NRO_OS = '" + os + "'");
            }

            //ORIGEM
            if (!(string.IsNullOrEmpty(origem)))
            {
                sql.AppendLine(" AND orig.AO_COD_AOP = '" + origem + "'");
            }

            //DESTINO
            if (!(string.IsNullOrEmpty(destino)))
            {
                sql.AppendLine(" AND dest.AO_COD_AOP = '" + destino + "'");
            }

            //PREFIXO
            if (!(string.IsNullOrEmpty(prefixo)))
            {
                sql.AppendLine(" AND t.TR_PFX_TRM = '" + prefixo + "'");
            }

            //PREFIXOS EXCLUIDOS
            if (!string.IsNullOrWhiteSpace(prefixosExcluir))
            {
                var ArPrefixos = prefixosExcluir.Split(',');
                var prefixosConcats = string.Empty;

                for (int i = 0; i < ArPrefixos.Length; i++)
                {
                    if (i == 0)
                    {
                        prefixosConcats = "'" + ArPrefixos[i].ToString() + "'";
                    }
                    else
                    {
                        prefixosConcats = prefixosConcats + ",'" + ArPrefixos[i].ToString() + "'";
                    }
                }

                sql.AppendLine(" AND SUBSTR(t.TR_PFX_TRM, 1, 1) NOT IN (" + prefixosConcats + ")");

            }

            //PREFIXO
            if (!(string.IsNullOrEmpty(prefixo)))
            {
                sql.AppendLine(" AND t.TR_PFX_TRM = '" + prefixo + "'");
            }

            sql.AppendLine(@" GROUP BY os.x1_nro_os,t.tr_pfx_trm, c.cp_cmp_cps   
                            ) t

                            left join movimentacao_trem mt on (mt.tr_id_trm = t.tr_id_trm) -- movimentações do trem
                            left join movimentacao_trem umt on (umt.tr_id_trm = t.tr_id_trm and umt.mt_id_mov = t.mt_id_mov) -- ultima movimentacao trem
                            LEFT JOIN area_operacional ap ON (ap.ao_id_ao = umt.ao_id_ao)

                            group by T.MT_ID_MOV, T.OS, T.TREM, T.COMPRIMENTO, T.TR_ID_TRM, T.PESO, T.T_LOCOS, T.LT, T.LR, T.T_VAGAO, T.VC, T.VV, T.POS_VIRTUAL, T.ORG, T.DST
                            ) q WHERE 1 = 1");

            //Situação
            if (!(string.IsNullOrEmpty(situacao)))
            {
                switch (situacao)
                {
                    case "C":
                        sql.AppendLine(" AND q.tr_stt_trm = 'C' and POS_VIRTUAL is null and partida_real is not null");
                        break;
                    case "P":
                        sql.AppendLine(" AND q.tr_stt_trm = 'P'");
                        break;
                    case "R":
                        sql.AppendLine(" AND q.tr_stt_trm = 'R' or (q.tr_stt_trm = 'C' and POS_VIRTUAL is not null)");
                        break;
                    case "L":
                        sql.AppendLine(" AND q. mt_id_mov is null and  q.tr_stt_trm = 'C'");
                        break;
                    default:
                        break;
                }

            }
            //R - Parado
            //P - Em Formação
            //C - Circulando
            //L - Liberado

            //TEMPOAPT
            if (!(string.IsNullOrEmpty(tempoAPT)))
            {
                sql.AppendLine(" AND nvl (round((sysdate-desde)*24,2),round((sysdate-DataHora)*24,2)) >= " + tempoAPT);
            }

            //sql.AppendLine(sql.ToString());

            #endregion

            #region OrderBy

            // Order by (direcionamento (ASC/DESC))
            sql.Append(" ORDER BY 10 ASC, 7 ASC");

            #endregion

            #region Acesso a base

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<ConsultaTremDto>());

                return query.List<ConsultaTremDto>();

            }
            #endregion
        }


        public IEnumerable<ConsultaTremDto> ObterTotalMercadarioOnu(string os)
        {
            #region query
            var sql = new StringBuilder(@" SELECT COUNT(MRC.MC_NUM_ONU)  AS ""TotalOnu"", OS.X1_NRO_OS AS ""NumOs""
                                            FROM COMPVAGAO_VIG CVV
                                            JOIN MERCADORIA MRC ON CVV.MC_ID_MRC = MRC.MC_ID_MRC
                                            JOIN COMPOSICAO CPS ON CVV.CP_ID_CPS = CPS.CP_ID_CPS
                                            JOIN TREM T ON CPS.TR_ID_TRM = T.TR_ID_TRM
                                            JOIN T2_OS OS ON T.OF_ID_OSV = OS.X1_ID_OS ");
            sql.AppendFormat(" WHERE OS.X1_NRO_OS IN({0}) ", os);
            sql.AppendLine(" GROUP BY OS.X1_NRO_OS ");

            #endregion

            #region Acesso a base

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<ConsultaTremDto>());
                return query.List<ConsultaTremDto>();
            }

            #endregion
        }
    }
}