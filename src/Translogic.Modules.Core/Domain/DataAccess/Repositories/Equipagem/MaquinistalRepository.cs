﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Equipagem
{
    using ALL.Core.AcessoDados;
    using Model.Equipagem;
    using Model.Equipagem.Repositories;

    /// <summary>
    /// Implementação de repositório de Maquinista
    /// </summary>
    public class MaquinistaRepository : NHRepository<Maquinista, int?>, IMaquinistaRepository
    {
    }
}