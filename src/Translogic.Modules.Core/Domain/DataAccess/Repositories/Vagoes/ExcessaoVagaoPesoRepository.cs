﻿using System;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using NHibernate.Criterion;
using Translogic.Modules.Core.Domain.Model.Vagoes;
using Translogic.Modules.Core.Domain.Model.Vagoes.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Vagoes
{
    public class ExcessaoVagaoPesoRepository : NHRepository<ExcessaoVagaoPeso, int>, IExcessaoVagaoPesoRepository
    {
        public ResultadoPaginado<ExcessaoVagaoPeso> Obter(DetalhesPaginacao paginacao, DateTime dtInicial, DateTime dtFim, string codigoVagao, decimal? peso)
        {
            var criteria = CriarCriteria();
            if (!string.IsNullOrEmpty(codigoVagao))
            {
                criteria.Add(Restrictions.Eq("CodigoVagao", codigoVagao));
            }

            if (peso.HasValue)
            {
                criteria.Add(Restrictions.Eq("Peso", peso));
            }

            criteria.Add(Restrictions.Between("Data", dtInicial, dtFim.AddDays(1).AddSeconds(-1)));

            return ObterPaginado(criteria, paginacao);
        }


        public ExcessaoVagaoPeso ObterPorCodigoVagao(string codigoVagao)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("CodigoVagao", codigoVagao));
            return ObterUnico(criteria);
        }
    }
}