﻿using System;
using System.Text;
using NHibernate;
using ALL.Core.Dominio;
using ALL.Core.AcessoDados;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao;
using Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao.Repositories;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.LiberacaoFormacaoTrem
{
    public class RelatorioFichaRecomendacaoRepository : NHRepository<RelatorioFichaRecomendacao, int>, IRelatorioFichaRecomendacaoRepository
    {
        public ResultadoPaginado<RelatorioFichaRecomendacaoDto> ObterTrensRecomendados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string trem, decimal? numOS, string avaria)
        {
            #region SQL
            var parameters = new List<Action<IQuery>>();
            var hql = new StringBuilder(@"SELECT T.""NumOs""
                                  ,T.""Prefixo""
                                  ,T.""Origem""
                                  ,T.""Destino""
                                  ,T.""Local""
                                  ,T.""DtChegadaPrevista""
                                  ,T.QtdAvaria AS ""QtdAvaria""
                                  ,T.QtdVagao  AS ""QtdVagao""
                            FROM(      
                                  SELECT  DISTINCT  
                                        OrdemInformacao.OF_COD_OSV AS ""NumOs""
                                        ,trm.tr_pfx_trm AS ""Prefixo""
                                        ,(SELECT org.AO_COD_AOP FROM AREA_OPERACIONAL org WHERE org.AO_ID_AO = NVL(px.AO_ID_AO_ORG, pt.AO_ID_AO_ORG)) AS  ""Origem""
                                        ,(SELECT dst.AO_COD_AOP FROM AREA_OPERACIONAL dst WHERE dst.AO_ID_AO=NVL(px.AO_ID_AO_DST,pt.AO_ID_AO_DST)) AS ""Destino""
                                        ,(SELECT LO.AO_COD_AOP FROM AREA_OPERACIONAL lo JOIN MOVIMENTACAO_TREM MT ON lo.AO_ID_AO =  mt.AO_ID_AO
                                          WHERE TRM.MT_ID_MOV = MT.MT_ID_MOV AND TRM.OF_ID_OSV  = OrdemInformacao.OF_ID_OSV 
                                          AND (:trem is null or TRM.TR_PFX_TRM = :trem)
                                          AND (:numOS is null or OrdemInformacao.OF_COD_OSV = :numOS)) AS ""Local""
                                         ,TRM.TR_DTP_CHG AS ""DtChegadaPrevista""
                                        ,(SELECT COUNT(EVOV.EVM_ID_MOT)  
                                          FROM  COMPVAGAO_VIG CV 
                                          JOIN VAGAO vg  ON vg.VG_ID_VG = cv.VG_ID_VG 
                                          JOIN CONDUSO_VAGAO_VIG CG ON CV.VG_ID_VG = CG.VG_ID_VG 
                                          JOIN COMPOSICAO CP  ON CV.CP_ID_CPS = CP.CP_ID_CPS 
                                          JOIN TREM TR ON CP.TR_ID_TRM = TR.TR_ID_TRM 
                                          JOIN evento_vg_obs_vig EVOV ON  EVOV.VG_ID_VG = VG.VG_ID_VG   
                                          WHERE TRM.OF_ID_OSV = OrdemInformacao.OF_ID_OSV 
                                          AND (:trem is null or TR.TR_PFX_TRM = :trem)
                                          AND CG.CD_IDT_CUS = 10 
                                          AND (:numOS is null or OrdemInformacao.OF_COD_OSV = :numOS)) AS QtdAvaria,
                                        (SELECT DISTINCT COUNT (CG.CD_IDT_CUS)  
                                          FROM COMPVAGAO_VIG CV 
                                          JOIN CONDUSO_VAGAO_VIG CG ON CV.VG_ID_VG = CG.VG_ID_VG 
                                          JOIN COMPOSICAO CP ON CV.CP_ID_CPS = CP.CP_ID_CPS
                                          JOIN TREM TR ON CP.TR_ID_TRM = TR.TR_ID_TRM 
                                          WHERE TRM.OF_ID_OSV = OrdemInformacao.OF_ID_OSV 
                                          AND (:trem is null or TR.TR_PFX_TRM = :trem) 
                                          AND CG.CD_IDT_CUS = 10
                                          AND (:numOS is null or OrdemInformacao.OF_COD_OSV = :numOS)) AS QtdVagao
                                        FROM 
                                            FROTA_VAGAO fv 
                                            ,VAGAO_FROTA vf 
                                            ,EMPRESA ep 
                                            ,CONDICAO_USO cd 
                                            ,CONDUSO_VAGAO_VIG ci 
                                            ,SERIE_VAGAO sv 
                                            ,PEDIDO_TRANSPORTE pt 
                                            ,PEDIDO_DERIVADO px 
                                            ,VAGAO_PEDIDO_VIG vb 
                                            ,VAGAO vg 
                                            ,COMPVAGAO_VIG cv 
                                            ,COMPOSICAO cp 
                                            ,evento_vg_obs_vig EVOV
                                            ,evento_vg_motivo EVM
                                            ,trem TRM
                                            ,ORDEMFORMACAO OrdemInformacao        
                                         WHERE  fv.FV_IND_RST = 'S' 
                                             AND fv.FV_ID_FT = vf.FV_ID_FT_AVG 
                                             AND vf.VG_ID_VG = cv.VG_ID_VG 
                                             AND ep.EP_ID_EMP = vg.EP_ID_EMP_PRP 
                                             AND cd.CD_IDT_CUS = ci.CD_IDT_CUS 
                                             AND ci.VG_ID_VG = cv.VG_ID_VG 
                                             AND sv.SV_ID_SV = vg.SV_ID_SV 
                                             AND px.PX_IDT_PDR(+) = vb.PX_IDT_PDR 
                                             AND pt.PT_ID_ORT = vb.PT_ID_ORT 
                                             AND vb.VG_ID_VG = cv.VG_ID_VG 
                                             AND vg.VG_ID_VG = cv.VG_ID_VG 
                                             AND cv.CP_ID_CPS = cp.CP_ID_CPS        
                                             AND EVOV.VG_ID_VG=VG.VG_ID_VG
                                             AND EVOV.EVM_ID_MOT = EVM.EVM_ID_MOT
                                             AND cp.tr_id_trm   = trm.tr_id_trm
                                             AND TRM.OF_ID_OSV = OrdemInformacao.OF_ID_OSV
                                             AND (:trem is null or TRM.TR_PFX_TRM = :trem)          
                                             AND ci.CD_IDT_CUS = 10
                                             AND (:numOS is null or OrdemInformacao.OF_COD_OSV = :numOS)               
                                          )T");
            //Filtro de AVARIA
            if(avaria != "Todos" && avaria != string.Empty)
            {
                if(avaria == "Sim")
                {
                    hql.AppendLine(" WHERE QtdAvaria > 0 AND QtdVagao > 0");
                }else if(avaria == "Não"){
                    hql.AppendLine(" WHERE QtdAvaria < 0 AND QtdVagao < 0");
                }
            }

            #endregion

            #region Acessa a base

            using (var session = OpenSession())
            {
                //Parametro Filtro TREM
                if (trem != string.Empty)
                {
                    parameters.Add(q => q.SetString("trem", trem.ToUpper()));
                }
                else {
                    parameters.Add(q => q.SetString("trem", null));
                }
                //Parametro Filtro Num OS
                if (numOS != null)
                {
                    parameters.Add(q => q.SetDecimal("numOS", numOS.Value));
                }
                else
                {
                    parameters.Add(q => q.SetString("numOS", null));
                }

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", query.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<RelatorioFichaRecomendacaoDto>());

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<RelatorioFichaRecomendacaoDto>();

                var result = new ResultadoPaginado<RelatorioFichaRecomendacaoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;

            #endregion

            }
        }

        public IList<RelatorioFichaRecomendacaoExportDto> ObterListaFichaRecomendacaoExportar(decimal numOS)
        {

            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT  DISTINCT
                                            cv.CV_SEQ_CMP                                                                                                 AS ""SEQ"" 
                                            ,CP.TR_ID_TRM                                                                                                 AS ""IdTrem""
                                            ,trm.tr_pfx_trm                                                                                               AS ""Trem""
                                            ,OrdemInformacao.OF_COD_OSV                                                                                   AS ""OS""
                                            ,SUBSTR(vg.VG_COD_VAG,1,6)||'-'||SUBSTR(vg.VG_COD_VAG,7,1)                                                    AS ""Vagao"" 
                                            ,cd.CD_DSC_PT                                                                                                 AS ""Recomendacao"" 
                                            ,DECODE(cd.CD_DSC_PT, 'Rec Oficina', EVM.EVM_DSC_MOT, 'Sem Recomend',' ')                                     AS ""AvariaFreio""
                                            ,(SELECT org.AO_COD_AOP FROM AREA_OPERACIONAL org WHERE org.AO_ID_AO = NVL(px.AO_ID_AO_ORG, pt.AO_ID_AO_ORG)) AS ""Origem"" 
                                            ,(SELECT dst.AO_COD_AOP FROM AREA_OPERACIONAL dst WHERE dst.AO_ID_AO=NVL(px.AO_ID_AO_DST,pt.AO_ID_AO_DST))    AS ""Destino"" 
                                            ,EVM.EVM_DSC_MOT                                                                                              AS ""MotivoRecomendacao""
                                            ,DECODE((SELECT EVM.EVM_DSC_MOT FROM evento_vg_motivo evemo 
                                                        WHERE evemo.EVM_ID_MOT = EVM.EVM_ID_MOT 
                                                        AND EVM_CLS_MOT = 'F'),'F','Ruim',null, 'Bom')                                                    AS ""FreioSituacao""
                                                FROM 
                                                        FROTA_VAGAO fv 
                                                    ,VAGAO_FROTA vf 
                                                    ,EMPRESA ep 
                                                    ,CONDICAO_USO cd 
                                                    ,CONDUSO_VAGAO_VIG ci 
                                                    ,SERIE_VAGAO sv 
                                                    ,PEDIDO_TRANSPORTE pt 
                                                    ,PEDIDO_DERIVADO px 
                                                    ,VAGAO_PEDIDO_VIG vb 
                                                    ,VAGAO vg 
                                                    ,COMPVAGAO_VIG cv 
                                                    ,COMPOSICAO cp 
                                                    ,evento_vg_obs_vig EVOV
                                                    ,evento_vg_motivo EVM
                                                    ,trem TRM
                                                    ,ORDEMFORMACAO OrdemInformacao ");

            #endregion

            #region WHERE
            // Filtros
            var sqlWhere = new StringBuilder();

            sqlWhere.Append(@" 
                                    fv.FV_IND_RST = 'S' 
                                     AND fv.FV_ID_FT = vf.FV_ID_FT_AVG 
                                     AND vf.VG_ID_VG = cv.VG_ID_VG 
                                     AND ep.EP_ID_EMP = vg.EP_ID_EMP_PRP 
                                     AND cd.CD_IDT_CUS = ci.CD_IDT_CUS 
                                     AND ci.VG_ID_VG = cv.VG_ID_VG 
                                     AND sv.SV_ID_SV = vg.SV_ID_SV 
                                     AND px.PX_IDT_PDR(+) = vb.PX_IDT_PDR 
                                     AND pt.PT_ID_ORT = vb.PT_ID_ORT 
                                     AND vb.VG_ID_VG = cv.VG_ID_VG 
                                     AND vg.VG_ID_VG = cv.VG_ID_VG 
                                     AND cv.CP_ID_CPS = cp.CP_ID_CPS
                                     AND TRM.OF_ID_OSV = OrdemInformacao.OF_ID_OSV
                                     AND EVOV.VG_ID_VG = VG.VG_ID_VG
                                     AND EVOV.EVM_ID_MOT = EVM.EVM_ID_MOT ");
            if (numOS != null)
            {
                sqlWhere.AppendFormat(" AND cp.tr_id_trm = trm.tr_id_trm  AND OrdemInformacao.OF_COD_OSV = '{0}'", numOS);
            }
            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            sql.Append(" ORDER BY 1, seq");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<RelatorioFichaRecomendacaoExportDto>());

                return query.List<RelatorioFichaRecomendacaoExportDto>();
            }
        }
    }
}

   