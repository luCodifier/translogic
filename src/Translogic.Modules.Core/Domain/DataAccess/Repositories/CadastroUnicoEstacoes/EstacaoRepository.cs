namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.CadastroUnicoEstacoes
{
	using ALL.Core.AcessoDados;
	using Model.CadastroUnicoEstacoes;
	using Model.CadastroUnicoEstacoes.Repositories;

	/// <summary>
	/// Implementa��o de reposit�rio de esta��o do cadastro �nico com NH
	/// </summary>
	public class EstacaoRepository : NHRepository<Estacao, int>, IEstacaoRepository
	{
	}
}