﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Tolerancia
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Tolerancia;
    using Translogic.Modules.Core.Domain.Model.Tolerancia.Repositories;

    public class ToleranciaMercadoriaRepository : NHRepository<ToleranciaMercadoria, int>, IToleranciaMercadoriaRepository
    {
        public ResultadoPaginado<ToleranciaMercadoriaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal, string mercadoria)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT
                             TOL_MERC_ID AS ""Id""
                            ,TOL_MERC_MIN AS ""Tolerancia""
                            ,TOL_MERC_TIMESTAMP   AS  ""VersionDate""
                            ,MERCADORIA.MC_ID_MRC AS ""IdMercadoria""
	                        ,MERCADORIA.MC_COD_MRC||' - '|| MERCADORIA.MC_DRS_PT  AS ""Mercadoria""
                            ,USUARIO.US_NOM_USU AS ""Usuario""
                        FROM 
                            TOLERANCIA_MERCADORIA
                        INNER JOIN
                            MERCADORIA
                        ON
                            MERCADORIA.MC_ID_MRC    = TOLERANCIA_MERCADORIA.MC_ID_MRC
                        INNER JOIN
                            USUARIO
                        ON
                            USUARIO.US_USR_IDU      = TOLERANCIA_MERCADORIA.US_USR_IDU");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();

            if (dataInicial.HasValue && dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND TOL_MERC_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial.Value.ToString("dd/MM/yyyy"), dataFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataInicial.HasValue)
            {
                sqlWhere.AppendFormat(" AND TOL_MERC_TIMESTAMP >= TO_DATE('{0} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')", dataInicial.Value.ToString("dd/MM/yyyy"));
            }
            else if (dataFinal.HasValue)
            {
                sqlWhere.AppendFormat(" AND TOL_MERC_TIMESTAMP <= TO_DATE('{0} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')", dataFinal.Value.ToString("dd/MM/yyyy"));
            }

            if (!string.IsNullOrEmpty(mercadoria) && mercadoria != "0")
            {
                sqlWhere.AppendFormat(" AND MERCADORIA.MC_ID_MRC = {0}", mercadoria);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy
            sql.Append(" ORDER BY MERCADORIA.MC_DRS_PT");
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<ToleranciaMercadoriaDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<ToleranciaMercadoriaDto>();

                var result = new ResultadoPaginado<ToleranciaMercadoriaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }
        public ToleranciaMercadoria Obter(int id)
        {
            return this.Obter(id);
        }

        public void Salvar(ToleranciaMercadoria toleranciaMercadoria)
        {
            if (toleranciaMercadoria.Id == 0)
            {
                if (ExisteMercadoria(toleranciaMercadoria.Mercadoria.Id.ToString()))
                    throw new Exception("Já existe tolerancia para esta mercadoria");

                this.InserirOuAtualizar(toleranciaMercadoria);
            }
            else
                this.Atualizar(toleranciaMercadoria);
        }

        public void Excluir(ToleranciaMercadoria toleranciaMercadoria)
        {
            if (ExisteMercadoriaConfigurada(toleranciaMercadoria.Mercadoria.Id.ToString()))
            {
                throw new Exception("Já existe configuração para esta mercadoria");
            }
            this.Remover(toleranciaMercadoria);
        }

        private bool ExisteMercadoriaConfigurada(string idMercadoria)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT
                    GRP_RTA_FRT_MC_PESO_ID
                FROM
                    GRP_RTA_FRT_MC_PESO ");

            #endregion

            #region Where

            var sqlWhere = new StringBuilder();
            if (!string.IsNullOrEmpty(idMercadoria))
            {
                sqlWhere.AppendFormat(" MC_ID_MRC = {0}", idMercadoria);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString();
                sql.Append(aux);
            }

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var result = query.UniqueResult<decimal>();
                return result > 0;
            }
        }

        public bool ExisteMercadoria(string idMercadoria)
        {
            var result = this.ObterConsulta(new DetalhesPaginacaoWeb(), Convert.ToDateTime("01/01/2000"), DateTime.Now, idMercadoria);

            return result.Items.Count > 0;
        }
    }
}