﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.AnexacaoDesanexacao
{
    public class LogAnxHistTremRepository : NHRepository<LogAnxHistTrem, int>, ILogAnxHistTremRepository
    {
        public LogAnxHistTrem ObterPorOS(decimal os)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OS", os));

            return ObterPrimeiro(criteria);
        }
    }
}