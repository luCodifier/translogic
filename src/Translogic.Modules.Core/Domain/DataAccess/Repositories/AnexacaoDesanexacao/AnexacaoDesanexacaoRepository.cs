﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;
using NHibernate.Transform;
using System.Text;
using System;
using NHibernate;
using System.Data.OracleClient;
using System.Data;
using NHibernate.Criterion;
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.AnexacaoDesanexacao
{
    public class AnexacaoDesanexacaoRepository : NHRepository<AnexacaoDesanexacaoAuto, int>, IAnexacaoDesanexacaoRepository
    {
        public ResultadoPaginado<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto)
        {
            var sqlWihtoutOrderBy = new StringBuilder();
            var sql = new StringBuilder(@"SELECT    l.acao                                                  AS ""Operacao"",
                                                    l.os                                                    AS ""OS"", 
                                                    tr.tr_pfx_trm                                           AS ""Trem"", 
                                                    substr(ori.ao_cod_aop, 1, 3)                            AS ""Origem"", 
                                                    substr(dest.ao_cod_aop, 1, 3)                           AS ""Destino"", 
                                                    l.estacao                                               AS ""LocalParada"", 
                                                    case when 
                                                        l.processado = 'S' then 
                                                          'Verde'
                                                          else 
                                                          'Vermelho' 
                                                        end                                                 AS ""StatusProcessamento"", 
                                                    case when 
                                                        l.status_parada = 'ABERTA' then 
                                                          'Vermelho'
                                                        else 
                                                          'Verde' 
                                                        end                                                 AS ""StatusParada"", 
                                                    (select NVL(max(l1.tentativas), 1)
                                                    from log_erro_anx_auto l1
                                                    where l1.os = l.os
                                                    and l1.estacao = l.estacao
                                                    and l1.acao = l.acao)                                   AS ""Tentativas"",
                                                    (select aj.NEXT_DATE
                                                     from all_jobs aj
                                                     where aj.JOB = 18723)                                  AS ""dtProximaExecucao"", 
                                                    case when l.descricao <> 'PROCESSADO' then l.descricao 
                                                    else null end                                           AS ""Obs"" 
                                                    FROM log_erro_anx_auto l, 
                                                    t2_os t, 
                                                    trem tr, 
                                                    area_operacional ori, 
                                                    area_operacional dest 
                                                    where l.os = t.x1_nro_os(+) 
                                                    and t.x1_id_os = tr.of_id_osv 
                                                    and tr.ao_id_ao_org = ori.ao_id_ao 
                                                    and dest.ao_id_ao = tr.ao_id_ao_dst
                                                    and l.processado in ('N', 'S')");
            //Data Inicial & Data Final
            if (anexacaoDesanexacaoRequestDto.dtInicial.HasValue && anexacaoDesanexacaoRequestDto.dtFinal.HasValue)
            {
                sql.AppendFormat(" AND l.TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", anexacaoDesanexacaoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), anexacaoDesanexacaoRequestDto.dtFinal.Value.ToString("dd/MM/yyyy"));
            }

            //Status Processado
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.StatusProcessamento)))
            {
                if (anexacaoDesanexacaoRequestDto.StatusProcessamento == "Processados")
                {
                    sql.AppendLine(" AND l.processado = 'S'");
                }else{
                    sql.AppendLine(" AND l.processado = 'N'");
                }          
            }

            //OS
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.OS)))
            {
                sql.AppendLine(" AND l.os = '" + anexacaoDesanexacaoRequestDto.OS + "'");
            }

            //Trem Prefixo
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.TremPrefixo)))
            {
                sql.AppendLine(" AND tr.tr_pfx_trm = '" + anexacaoDesanexacaoRequestDto.TremPrefixo.ToUpper() + "'");
            }

            //ORIGEM
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Origem)))
            {
                sql.AppendLine(" AND substr(ori.ao_cod_aop, 1, 3) = '" + anexacaoDesanexacaoRequestDto.Origem.ToUpper() + "'");
            }

            //DESTINO
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Destino)))
            {
                sql.AppendLine(" AND substr(dest.ao_cod_aop, 1, 3) = '" + anexacaoDesanexacaoRequestDto.Destino.ToUpper() + "'");
            }

            //LOCAL/PARADA
            if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Local)))
            {
                sql.AppendLine(" AND l.estacao = '" + anexacaoDesanexacaoRequestDto.Local.ToUpper() + "'");
            }

            sql.AppendLine(@" GROUP BY  l.acao,
                                        l.os, 
                                        tr.tr_pfx_trm, 
                                        substr(ori.ao_cod_aop, 1, 3), 
                                        substr(dest.ao_cod_aop, 1, 3), 
                                        l.estacao, 
                                        l.processado, 
                                        l.status_parada,
                                        l.descricao");

            sqlWihtoutOrderBy.AppendLine(sql.ToString());

            //sql.AppendLine(" ORDER BY 10 ASC, 7 ASC");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlWihtoutOrderBy.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<AnexacaoDesanexacaoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<AnexacaoDesanexacaoDto>();

                var result = new ResultadoPaginado<AnexacaoDesanexacaoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<VeiculoTremDto> ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os, string estacao, string acao)
        {
            //var sqlWihtoutOrderBy = new StringBuilder();
            var sql = @"SELECT  LPAD(LG.veiculo,7,'0')                                      AS ""VagaoLocomotiva"", 
                                       LG.PROCESSADO                                        AS ""StatusProcessamento"",
                                       LG.acao                                              AS ""Acao"",
                                       CASE
                                         (SELECT 1
                                         FROM vagao v
                                         WHERE v.vg_cod_vag = lg.veiculo)
                                         WHEN 1 THEN 'S'
                                           ELSE 'N'
                                       END                                                  AS ""Cad"", 
                                       'S'                                                  AS ""Pedido"",
                                       EV.LO_DSC_PT                                         AS ""Localizacao"",
                                       EV.SI_DSC_PT                                         AS ""Situacao"",
                                       EV.SH_DSC_PT                                         AS ""Lotacao"",
                                       EV.CD_DSC_PT                                         AS ""Recomendacao"", 
                                       nvl((SELECT lv.acao||' '||lv.numero_veiculo 
                                            FROM log_anx_hist_veiculo lv
                                            WHERE lv.os = lg.os
                                            AND lv.local = lg.estacao
                                            AND lv.numero_veiculo_old = lg.veiculo
                                            AND ROWNUM = 1), lg.descricao)                  AS ""DescricaoErro""
                                  FROM ESTADO_VAGAO_NOVA EV,
                                            (select le.os,
                                                    le.processado,
                                                    le.descricao,
                                                    le.vagao_loco veiculo,
                                                    le.acao,
                                                    le.estacao
                                               FROM log_erro_anx_auto le
                                               WHERE  le.vagao_loco IS NOT NULL
                                               AND   le.processado in ('N', 'S')) lg
                                WHERE EV.VG_COD_VAG (+) = LG.VEICULO
                                AND lg.os = :os
                                AND lg.acao = :acao 
                                AND lg.estacao = :estacao
 
                                UNION 
 
                                SELECT lg.veiculo Loco,
                                       lg.processado,
                                       LG.acao,
                                         CASE
                                         (SELECT 1
                                         FROM locomotiva v
                                         WHERE l.lo_cod_lcl = lg.veiculo)
                                         WHEN 1 THEN 'S'
                                           ELSE 'N'
                                       END cad,
                                       'S' Pedido,
                                       l.lo_dsc_pt localizacao,
                                       sit.si_dsc_pt situacao,
                                       NULL lotacao,
                                       cu.cd_dsc_pt recomendação,
                                        nvl((SELECT lv.acao||' '||lv.numero_veiculo 
                                            FROM log_anx_hist_veiculo lv
                                            WHERE lv.os = lg.os
                                            AND lv.local = lg.estacao
                                            AND lv.numero_veiculo_old = lg.veiculo
                                            AND ROWNUM = 1), lg.descricao)descricao
                                  FROM loco_patio_vig a,
                                       locomotiva c,
                                       area_operacional b,
                                       situacao_loco_vig slc,
                                       situacao sit,
                                       serie_locos sl,
                                       local_loco_vig llv,
                                       localizacao l,
                                       conduso_loco_vig cul,
                                       condicao_uso cu,
                                       (SELECT le.os,
                                               le.processado,
                                               le.descricao,
                                               le.vagao_loco veiculo,
                                               le.acao,
                                               le.estacao
                                          FROM log_erro_anx_auto le
                                          WHERE le.vagao_loco IS NOT NULL
                                          AND   le.processado IN ('N', 'S')) lg
 
                                WHERE a.lp_dat_frm is null
                                   AND a.ao_id_ao = b.ao_id_ao
                                   AND a.lc_id_lc = c.lc_id_lc
                                   AND slc.lc_id_lc = c.lc_id_lc
                                   AND slc.st_dat_trm is null
                                   AND slc.si_idt_stc = sit.si_idt_stc
                                   AND sl.sl_id_sl = c.sl_id_sl
                                   AND llv.lc_id_lc = c.lc_id_lc
                                   AND l.lo_idt_lcl = llv.lo_idt_lcl
                                   AND cul.lc_id_lc = c.lc_id_lc
                                   AND cu.cd_idt_cus = cul.cd_idt_cus
                                   AND lg.veiculo = c.lc_cod_loc (+)
                                AND lg.os = :os
                                AND lg.acao = :acao 
                                AND lg.estacao = :estacao";
         
            //sqlWihtoutOrderBy.AppendLine(sql.ToString());

            //sql.AppendLine(" ORDER BY 10 ASC, 7 ASC");

            using (var session = OpenSession())
            {
                IQuery query = session.CreateSQLQuery(sql);
                query.SetDecimal("os", os);
                query.SetString("acao", acao);
                query.SetString("estacao", estacao);

                query.SetResultTransformer(Transformers.AliasToBean<VeiculoTremDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var itens = query.List<VeiculoTremDto>();
                var total = itens.Count;
                

                var result = new ResultadoPaginado<VeiculoTremDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IEnumerable<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacaoExportar(AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto)
        {

            #region Query
                //            var sqlWihtoutOrderBy = new StringBuilder();
                var sql = new StringBuilder(@"SELECT l.acao                                                     AS ""Operacao"",
                                                        l.os                                                    AS ""OS"",
                                                        tr.tr_pfx_trm                                           AS ""Trem"",
                                                        substr(ori.ao_cod_aop, 1, 3)                            AS ""Origem"",
                                                        substr(dest.ao_cod_aop, 1, 3)                           AS ""Destino"",
                                                        l.estacao                                               AS ""LocalParada"",
                                                        case when 
                                                        l.processado = 'S' then 
                                                        'Sim'
                                                        else 
                                                        'Não' 
                                                        end                                                     AS ""StatusProcessamento"",
                                                        case when 
                                                        l.status_parada = 'ABERTA' then 
                                                        'Aberta'
                                                        else 
                                                        'Fechada' 
                                                        end                                                     AS ""StatusParada"",
                                                        case when 
                                                        l.tentativas = 0 then 1
                                                        when l.tentativas is null then 1
                                                        else l.tentativas end                                   AS ""Tentativas"",
                                                        sysdate                                                 AS ""dtProximaExecucao"",
                                                        case when l.descricao <> 'PROCESSADO' 
                                                        then l.descricao
                                                        else null end                                           AS ""Obs""
                                                        FROM log_erro_anx_auto l,
                                                        t2_os             t,
                                                        trem              tr,
                                                        area_operacional  ori,
                                                        area_operacional  dest
                                                        where l.os = t.x1_nro_os
                                                        and t.x1_id_os = tr.of_id_osv
                                                        and tr.ao_id_ao_org = ori.ao_id_ao
                                                        and dest.ao_id_ao = tr.ao_id_ao_dst");
                //Data Inicial & Data Final
                if (anexacaoDesanexacaoRequestDto.dtInicial.HasValue && anexacaoDesanexacaoRequestDto.dtFinal.HasValue)
                {
                    sql.AppendFormat(" AND l.TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", anexacaoDesanexacaoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), anexacaoDesanexacaoRequestDto.dtFinal.Value.ToString("dd/MM/yyyy"));
                }

                //Status Processado
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.StatusProcessamento)))
                {
                if (anexacaoDesanexacaoRequestDto.StatusProcessamento == "Processados")
                {
                    sql.AppendLine(" AND l.processado = 'S'");
                }else{
                    sql.AppendLine(" AND l.processado = 'N'");
                }          
                }

                //OS
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.OS)))
                {
                    sql.AppendLine(" AND l.os = '" + anexacaoDesanexacaoRequestDto.OS + "'");
                }

                //Trem Prefixo
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.TremPrefixo)))
                {
                    sql.AppendLine(" AND tr.tr_pfx_trm = '" + anexacaoDesanexacaoRequestDto.TremPrefixo.ToUpper() + "'");
                }

                //ORIGEM
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Origem)))
                {
                    sql.AppendLine(" AND substr(ori.ao_cod_aop, 1, 3) = '" + anexacaoDesanexacaoRequestDto.Origem.ToUpper() + "'");
                }

                //DESTINO
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Destino)))
                {
                    sql.AppendLine(" AND substr(dest.ao_cod_aop, 1, 3) = '" + anexacaoDesanexacaoRequestDto.Destino.ToUpper() + "'");
                }

                //LOCAL/PARADA
                if (!(string.IsNullOrEmpty(anexacaoDesanexacaoRequestDto.Local)))
                {
                    sql.AppendLine(" AND l.estacao = '" + anexacaoDesanexacaoRequestDto.Local.ToUpper() + "'");
                }
            
                sql.AppendLine(@" GROUP BY l.os,
                                            tr.tr_pfx_trm,
                                            substr(ori.ao_cod_aop, 1, 3),
                                            substr(dest.ao_cod_aop, 1, 3),
                                            l.estacao,
                                            l.processado,
                                            l.status_parada,
                                            l.tentativas,
                                            l.descricao,
                                            l.acao");

        #endregion

            #region OrderBy

            // Order by (direcionamento (ASC/DESC))

        #endregion

            #region Acesso a base

            using (var session = OpenSession())
            {
            var query = session.CreateSQLQuery(sql.ToString());

            query.SetResultTransformer(Transformers.AliasToBean<AnexacaoDesanexacaoDto>());

            return query.List<AnexacaoDesanexacaoDto>();

            }
        #endregion
    }

        public AnexacaoDesanexacaoDto ObterPorOS(decimal os, string numeroVeiculo)
        {
            try
            {
                #region SQL Query

                var sql = new StringBuilder(@"SELECT l.acao                                             AS ""Operacao"",
                                                    l.os                                                AS ""OS"",
                                                    tr.tr_pfx_trm                                       AS ""Trem"",
                                                    substr(ori.ao_cod_aop, 1, 3)                        AS ""Origem"",
                                                    substr(dest.ao_cod_aop, 1, 3)                       AS ""Destino"",
                                                    l.estacao                                           AS ""LocalParada"",
                                                    l.processado                                        AS ""StatusProcessamento"",
                                                    case when 
                                                        l.status_parada = 'ABERTA' then 
                                                          'Vermelho'
                                                        else 
                                                          'Verde' 
                                                        end                                             AS ""StatusParada"",
                                                    case when 
                                                        l.tentativas = 0 then 1
                                                        when l.tentativas is null then 1
                                                    else l.tentativas end                               AS ""Tentativas"",
                                                    sysdate                                             AS ""dtProximaExecucao"",
                                                    case when l.descricao <> 'PROCESSADO' 
                                                        then l.descricao
                                                        else null end                                   AS ""Obs""
                                                FROM log_erro_anx_auto l,
                                                    t2_os             t,
                                                    trem              tr,
                                                    area_operacional  ori,
                                                    area_operacional  dest
                                            where l.os = t.x1_nro_os
                                                and t.x1_id_os = tr.of_id_osv
                                                and tr.ao_id_ao_org = ori.ao_id_ao
                                                and dest.ao_id_ao = tr.ao_id_ao_dst
                                                AND l.os = " + os + " AND ROWNUM = 1");
            
                #endregion

                using (var session = OpenSession())
                {

                    var query = session.CreateSQLQuery(sql.ToString());

                    query.SetResultTransformer(Transformers.AliasToBean<AnexacaoDesanexacaoDto>());

                    var result = query.UniqueResult<AnexacaoDesanexacaoDto>();

                    return result;
                }
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public AnexacaoDesanexacaoAuto ObterPorOperacaoOsLocalParada(string operacao, decimal os, string localParada)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Acao", operacao));
            criteria.Add(Restrictions.Eq("OS", os));
            criteria.Add(Restrictions.Eq("Estacao", localParada));

            var entity = ObterPrimeiro(criteria);
            if (entity == null)
            {
                return new AnexacaoDesanexacaoAuto();
            }
            else
            {
                return entity;
            }

        }

        public decimal ObterIdTrem(decimal os)
        {
            try
            {
                #region SQL Query

                var sql = new StringBuilder(@"SELECT tr.tr_id_trm FROM trem tr, t2_os t WHERE tr.of_id_osv = t.x1_id_os and t.x1_nro_os = " + os);

                #endregion

                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql.ToString());

                    var result = query.UniqueResult();

                    if (Convert.ToDouble(result) != null)
                    {
                        return Convert.ToInt32(result);
                    }
                    else
                    {
                        return Convert.ToInt32(0);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public decimal ObterIdLocal(string local)
        {
            try
            {
                #region SQL Query

                var sql = new StringBuilder(@" select ao.ao_id_ao from area_operacional ao where ao.ao_cod_aop = '" + local + "'");

                #endregion

                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql.ToString());

                    var result = query.UniqueResult();

                    if (Convert.ToDouble(result) != null)
                    {
                        return Convert.ToInt32(result);
                    }
                    else
                    {
                        return Convert.ToInt32(0);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public decimal ObterIdParada(decimal os, decimal idLocal)
        {
            try
            {
                #region SQL Query

                var sql = new StringBuilder(@"SELECT pr.pr_id_prt from paradas_realizadas_trem pr 
                                                WHERE pr.tr_id_trm = (select tr.tr_id_trm AS idTrem FROM trem tr, t2_os t WHERE tr.of_id_osv = t.x1_id_os and t.x1_nro_os = " + os + 
                                                ") and pr.ao_id_ao = " + idLocal);

                #endregion

                using (var session = OpenSession())
                {
                    var query = session.CreateSQLQuery(sql.ToString());

                    var result = query.UniqueResult();

                    if (Convert.ToDouble(result) != null)
                    {
                        return Convert.ToInt32(result);
                    }
                    else
                    {
                        return Convert.ToInt32(0);
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Atualiza Status Parada para status FECHADA
        /// </summary>
        /// <param name="chaveCte">Atualiza status parada para FECHADA, batendo Operacao , OS, LocalParada</param>
        public void UpdateSatusParada(string Operacao, decimal OS, string LocalParada)
        {
            using (var session = OpenSession())
            {
                var ql = @"UPDATE LOG_ERRO_ANX_AUTO SET STATUS_PARADA = 'FECHADA'" +
                            " WHERE ACAO = '" + Operacao + "'" +
                            " AND OS = " + OS +
                            " AND ESTACAO = '" + LocalParada + "'";

                session.CreateSQLQuery(ql).ExecuteUpdate();
                session.Flush();
            }
        }

        /// <summary>
        /// Zera tentativas
        /// </summary>
        /// <param name="chaveCte">Zera tentativas</param>
        public void ZeraTentativas(string Operacao, decimal OS)
        {
            using (var session = OpenSession())
            {
                var ql = @"UPDATE LOG_ERRO_ANX_AUTO SET TENTATIVAS = 0" +
                            " WHERE ACAO = '" + Operacao + "'" +
                            " AND OS = " + OS;

                session.CreateSQLQuery(ql).ExecuteUpdate();
                session.Flush();
            }
        }

        /// <summary>
        /// Chama a package para Encerrar Parada.
        /// </summary>
        /// <param name="xml"> Código do fluxo</param>
        /// <returns>Retorna retorno de execução de PKG.</returns>
        public string EncerrarParadaPKG(string xml)
        {
            string xmlSaida = string.Empty;

            #region Executa PKG
            using (ISession session = OpenSession())
            {
                session.BeginTransaction();
                var command = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    //CommandText = "PKG_VOO_ATIVOS.SP_EXECUTAR_VOO_EQUIPAMENTO",
                    CommandText = "PKG_CCP_II.SP_ENCERRAR_PARADA",
                    Connection = (OracleConnection)session.Connection
                };

                AddParameter(command, "pxmlEntrada", OracleType.VarChar, xml, ParameterDirection.Input);
                AddParameter(command, "pxmlSaida", OracleType.VarChar, xmlSaida, ParameterDirection.Output);
                command.Parameters["pxmlSaida"].Size = 4000;
                session.Transaction.Enlist(command);
                try
                {
                    command.Prepare();
                    command.ExecuteNonQuery();
                    session.Transaction.Commit();

                    xmlSaida = command.Parameters["pxmlSaida"].Value.ToString();
                }
                catch (Exception ex)
                {
                    session.Transaction.Rollback();

                    xmlSaida = ex.Message;

                    xmlSaida = FormataXMLSaida(xmlSaida);

                }

                return xmlSaida;
            }
            #endregion
        }

        #region Métodos Comuns
        /// <summary>
        /// Adicionar um novo <see cref="OracleParameter"/> ao <see cref="OracleCommand"/>
        /// </summary>
        /// <param name="command"><see cref="OracleCommand"/> onde o <see cref="OracleParameter"/> será adicionado</param>
        /// <param name="nome">Nome do parâmetro</param>
        /// <param name="tipo">Tipo do parâmetro <see cref="OracleType"/></param>
        /// <param name="valor">Valor de entrada</param>
        /// <param name="direcao">Direção <see cref="ParameterDirection"/></param>
        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }

        /// <summary>
        /// Formatar a string de retorno de erros de PGKs
        /// </summary>
        /// <param name="xmlSaida"></param>
        /// <returns></returns>
        private string FormataXMLSaida(string xmlSaida)
        {
            xmlSaida = xmlSaida.Replace("PKG_EVENTO.EV_VAGAO:", "");


            if (xmlSaida.IndexOf("#") > 0)
                xmlSaida = xmlSaida.Replace(xmlSaida.Substring(0, xmlSaida.IndexOf("#") + 1), "");


            if (xmlSaida.IndexOf("</ERRO>") > 0)
                xmlSaida = xmlSaida.Replace(xmlSaida.Substring(xmlSaida.IndexOf("</ERRO>"), (xmlSaida.Length - xmlSaida.IndexOf("</ERRO>"))), "");

            return xmlSaida;
        }
        #endregion

    }
}