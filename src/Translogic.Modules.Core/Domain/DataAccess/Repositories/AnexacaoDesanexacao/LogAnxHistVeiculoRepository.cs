﻿using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories;
using Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.AnexacaoDesanexacao
{
    public class LogAnxHistVeiculoRepository : NHRepository<LogAnxHistVeiculo, int>, ILogAnxHistVeiculoRepository
    {
    }
}