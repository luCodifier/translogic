﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.ArquivoBaldeio
{
    using Translogic.Modules.Core.Domain.Model.ArquivoBaldeio.Repositorio;
    using Translogic.Modules.Core.Domain.Model.ArquivoBaldeio;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
    using System.Text;
    using NHibernate.Transform;
    using NHibernate.Criterion;

    public class CartaBaldeioRepository : NHRepository<CartaBaldeio, int>, ICartaBaldeioRepository
    {

        public CartaBaldeioDto ObterArquivoBaldeio(int idArquivoBaldeio)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT ARQ.NOME               AS ""NomeArquivo""
                                               ,ARQ.ARQUIVO_IMPORTADO   AS ""ArquivoPdf""
                                               ,ARQ.BD_ID_BLD           AS ""IdBaldeio""
                                               ,ARQ.ID_ANEXO_BALDEIO    AS ""IdArquivoBaldeio""
                                               ,ARQ.DT_ATUALIZACAO      AS ""DataAtualizacao""
                                               ,ARQ.USUARIO             AS ""Usuario""
                                        FROM ARQUIVO_BALDEIO ARQ
                                        JOIN BALDEIO BLD ON ARQ.BD_ID_BLD = BLD.BD_IDT_BLD");

            sql.AppendFormat(@" WHERE ARQ.ID_ANEXO_BALDEIO ='{0}'", idArquivoBaldeio.ToString());    
                                    
            #endregion

            #region Execution

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<CartaBaldeioDto>());
                return query.UniqueResult<CartaBaldeioDto>();
            }

            #endregion

        }

        public CartaBaldeio ObterPorBaldeio(int idBaldeio)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Baldeio.Id", idBaldeio));
            return ObterPrimeiro(criteria);
        }
    }
}