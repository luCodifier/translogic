﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.AvisoFat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Model.Diversos.AvisoFat;
    using Model.Bolar;
    using Model.Diversos.Repositories;
    using NHibernate;
    using NHibernate.Transform;

    /// <summary>
    /// Repositorio para <see cref="BolarBo"/>
    /// </summary>
    public class ContatoAvisoFatRepository : NHRepository<ContatoAvisoFat, int>, IContatoAvisoFatRepository
    {
        public IList<EnvioMensagemNaoFaturadosDto> CarregaObjetosEnvio()
        {
            try
            {
                var sb = new StringBuilder();
                sb.AppendFormat(
                    @"  SELECT DISTINCT 
                        C.ID_CONTATO AS ""ContatoId"",
                        C.CONTATO_NOME AS ""Nome"",
                        C.CONTATO_EMAIL AS ""Email"",
                        C.CONTATO_TELEFONE AS ""Telefone"",
                        A.AO_COD_AOP AS ""AreaOp"",
                        C.ENVIA_EMAIL AS ""EnviaEmail"",
                        C.ENVIA_SMS AS ""EnviaSms"",
                        (
                          SELECT COUNT (*)   
                          FROM   
                                 bo_bolar_bo bbb,
                                 fluxo_comercial fc,
                                 bo_bolar_imp_cfg bo
                          WHERE bbb.bo_timestamp >= TRUNC(SYSDATE)
                          AND   bbb.cod_flx      = SUBSTR(fc.fx_cod_flx, 3, 5)
                          and   bo.cliente=bbb.ao_cod_aop
                          AND   fc.ao_id_est_or  = E.AO_ID_AO
                          AND   bbb.IDT_SIT = 'N'
                        )
                        AS ""CountVagoes""
     
                         FROM 
                          ENVIO_AVISO_FAT E,
                          AREA_OPERACIONAL A,
                          BO_BOLAR_BO B,
                          CONTATOS_AVISO_FATURAMENTO C,
                          FLUXO_COMERCIAL F
                        WHERE       
                          E.AO_ID_AO = A.AO_ID_AO
                          AND A.AO_ID_AO = F.AO_ID_EST_OR      
                          AND F.FX_ID_FLX = B.COD_FLX
                          AND E.ID_CONTATO = C.ID_CONTATO
                          AND B.BO_TIMESTAMP >= TRUNC(SYSDATE)
                          AND (C.ENVIA_EMAIL = 'S' OR C.ENVIA_SMS = 'S')
                          AND 
                          (
                            SELECT COUNT (*)   
                            FROM   
                                   bo_bolar_bo bbb,
                                   fluxo_comercial fc,
                                   bo_bolar_imp_cfg bo
                            WHERE bbb.bo_timestamp >= TRUNC(SYSDATE)
                            AND   bbb.cod_flx      = SUBSTR(fc.fx_cod_flx, 3, 5)
                            and   bo.cliente=bbb.ao_cod_aop
                            AND   fc.ao_id_est_or  = E.AO_ID_AO
                            AND   bbb.IDT_SIT = 'N'
                          ) > 0");

                var session = SessionManager.OpenStatelessSession();
                IQuery query = session.CreateSQLQuery(sb.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<EnvioMensagemNaoFaturadosDto>());

                var result = query.List<EnvioMensagemNaoFaturadosDto>();

                return result.ToList();
            }
            catch(Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
