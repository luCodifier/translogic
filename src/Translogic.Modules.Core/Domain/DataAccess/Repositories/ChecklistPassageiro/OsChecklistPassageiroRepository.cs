﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using System.Text;
    using NHibernate.Transform;
    using System.Collections.Generic;

    public class OsChecklistPassageiroRepository : NHRepository<Os_Checklist_Passageiro, int>, IOsChecklistPassageiroRepository   
    {
        public Os_Checklist_Passageiro ObterOSPassageiroPorIDCheckList(int idOsCheckList)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OsChecklist.Id", idOsCheckList));

            return ObterPrimeiro(criteria);
        }

        public Os_Checklist_Passageiro ObterOSPassageiroCheckList(int idOsCheckListPassageiro)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Id", idOsCheckListPassageiro));

            return ObterPrimeiro(criteria);
        }

        public OsDadosTremDto BuscaDadosTremPorIdOs(int idOs)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT 
                                  TR.TR_ID_TRM                                                        AS ""IdTrem"",
                                  CP.CP_ID_CPS                                                        AS ""IdComposicao"",
                                  OS.X1_DAT_PAR_PRV_OFI                                               AS ""DataPartidaPrevista"",
                                  (SELECT MT.MT_DTS_RAL 
                                   FROM   MOVIMENTACAO_TREM MT,
                                          AREA_OPERACIONAL AO
                                   WHERE  MT.AO_ID_AO = AO.AO_ID_AO
                                   AND    MT.TR_ID_TRM = TR.TR_ID_TRM
                                   AND    AO.AO_ID_AO_INF = TR.AO_ID_AO_ORG)                          AS ""DataPartidaReal"",
                                  (SELECT MT.MT_DTC_RAL 
                                   FROM   MOVIMENTACAO_TREM MT,
                                          AREA_OPERACIONAL AO
                                   WHERE  MT.AO_ID_AO = AO.AO_ID_AO
                                   AND    MT.TR_ID_TRM = TR.TR_ID_TRM
                                   AND    AO.AO_ID_AO_INF = TR.AO_ID_AO_DST)                          AS ""DataChegadaReal"",
                                  COUNT(1)                                                            AS ""QtdeVagoes"",
                                  SUM( NVL(VG.VG_NUM_TRA, FEV.FC_TAR))                                AS ""TB""
                                FROM T2_OS OS
                                INNER JOIN TREM TR ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                INNER JOIN COMPOSICAO CP ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                INNER JOIN COMPVAGAO CVV ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                INNER JOIN VAGAO VG ON (VG.VG_ID_VG = CVV.VG_ID_VG)
                                INNER JOIN FOLHA_ESPECIF_VAGAO FEV ON (FEV.FC_ID_FC = VG.FC_ID_FC)
                                WHERE X1_NRO_OS = :idOs AND TR.AO_ID_AO_ORG = CP.AO_ID_AO_ORG
                                GROUP BY OS.X1_DAT_PAR_PRV_OFI, TR.TR_DAT_LBR, TR.TR_ID_TRM, CP.CP_ID_CPS, TR.AO_ID_AO_ORG, TR.AO_ID_AO_DST   ");
            #endregion

            #region Parameters
            sql = sql.Replace(":idOs", idOs.ToString());
            #endregion

            #region Execution
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OsDadosTremDto>());

                var result = query.UniqueResult<OsDadosTremDto>();

                return result;
            }
            #endregion
        }

        public IList<LocomotivaDto> BuscaLocomotivasTremPorIdOs(int idOs)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT LC.LC_COD_LOC AS ""Locomotiva""
                                        FROM T2_OS OS
                                        INNER JOIN TREM TR ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                        INNER JOIN COMPOSICAO CP ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                        INNER JOIN COMP_LOCOMOT CLV ON (CLV.CP_ID_CPS = CP.CP_ID_CPS)
                                        INNER JOIN LOCOMOTIVA LC ON (LC.LC_ID_LC = CLV.LC_ID_LC)
                                        --INNER JOIN FOLHA_ESPECIF_LOCO FEL ON (FEL.FE_ID_FL = LC.)
                                        WHERE X1_NRO_OS = :idOs
                                        ORDER BY CLV.PL_SEQ_CMP");
            
            #endregion

            #region Parameters
                sql = sql.Replace(":idOs", idOs.ToString());
            #endregion

            #region Execution
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<LocomotivaDto>());

                var result = query.List<LocomotivaDto>();

                return result;
            }
            #endregion
        }
    }
}