﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public class OsChecklistLogRepository : NHRepository<Os_Checklist_Log, int>, IOsChecklistLogRepository   
    {
    }
}
