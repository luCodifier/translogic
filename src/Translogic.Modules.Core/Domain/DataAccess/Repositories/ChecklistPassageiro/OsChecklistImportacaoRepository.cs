﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using ALL.Core.AcessoDados;
    using System.Text;
    using NHibernate.Transform;
    using System.Text;
    using NHibernate;
    using ALL.Core.Dominio;
    using System.Collections.Generic;
    using NHibernate.Criterion;

    public class OsChecklistImportacaoRepository : NHRepository<Os_Checklist_Importacao, int>, IOsChecklistImportacaoRepository
    {

        public OsCheckListImportacaoDto ObterArquivo(int idCheckListAprovado, string quadro)
        {
            #region Query
            var sql = new StringBuilder(@" SELECT ARQUIVO_IMPORTADO AS ""Arquivo""
                                                 ,ID_OSCK_APR AS ""IdOsAprovacao""
                                                 ,ID_OSCK_IMP AS ""IdOsCheckListImportacao""
                                                 ,IDT_TIPO_APR AS ""TipoAprovacao""
                                                 ,NM_ARQUIVO AS ""NomeArquivo""
                                           FROM OS_CHECKLIST_IMPORTACAO i");

            sql.AppendFormat(@" WHERE ID_OSCK_APR  = {0}
                                  AND IDT_TIPO_APR = '{1}' ", idCheckListAprovado, quadro);

            #endregion

            #region Execution

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OsCheckListImportacaoDto>());
                return query.UniqueResult<OsCheckListImportacaoDto>();
            }

            #endregion
        }

        public Os_Checklist_Importacao ObterPorIdEArea(int idCheckListAprovado, string quadro)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OsChecklistAprovacao.Id", idCheckListAprovado));
            criteria.Add(Restrictions.Eq("TipoAprocacao", quadro));

            var entity = ObterPrimeiro(criteria);
            if (entity == null)
            {
                return new Os_Checklist_Importacao();
            }
            else
            {
                return entity;
            }
        }


        public IList<OsCheckListImportacaoDto> ObterImportacaoPorNumOS(int numOs)
        {
            #region Query
            var sql = new StringBuilder(@" SELECT ARQUIVO_IMPORTADO AS ""Arquivo""
                                                 ,OCI.ID_OSCK_APR AS ""IdOsAprovacao""
                                                 ,ID_OSCK_IMP AS ""IdOsCheckListImportacao""
                                                 ,IDT_TIPO_APR AS ""TipoAprovacao""
                                                 ,NM_ARQUIVO AS ""NomeArquivo""
                                           FROM OS_CHECKLIST_IMPORTACAO OCI
                                           JOIN OS_CHECKLIST_APROVACAO OCA
                                             ON OCI.ID_OSCK_APR = OCA.ID_OSCK_APR
                                                AND ((OCA.VIA_IDT_IMPORTACAO = 'I' AND OCI.IDT_TIPO_APR = 'Via') 
                                                OR (OCA.LOCO_IDT_IMPORTACAO = 'I' AND OCI.IDT_TIPO_APR = 'loc')
                                                OR (OCA.TRAC_IDT_IMPORTACAO = 'I' AND OCI.IDT_TIPO_APR = 'tra')
                                                OR (OCA.VAG_IDT_IMPORTACAO = 'I' AND OCI.IDT_TIPO_APR = 'vag'))
                                           JOIN OS_CHECKLIST OCT
                                             ON OCA.ID_OSCK = OCT.ID_OSCK
                                           JOIN T2_OS OS
                                             ON OS.X1_ID_OS = OCT.X1_ID_OS");

            sql.AppendFormat(@" WHERE OS.X1_NRO_OS = {0}", numOs);

            #endregion

            #region Execution

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OsCheckListImportacaoDto>());
                return query.List<OsCheckListImportacaoDto>();
            }

            #endregion
        }
    }
}