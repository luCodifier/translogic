﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using NHibernate.Criterion;

    public class OsChecklistAprovacaoRepository : NHRepository<Os_Checklist_Aprovacao, int>, IOsChecklistAprovacaoRepository    
    {
        public Os_Checklist_Aprovacao ObterPorOsChecklist(int idOsChecklist)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OsChecklist.Id", idOsChecklist));

            return ObterPrimeiro(criteria);
        }

        public bool Concluido(int idOsChecklist)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("OsChecklist.Id", idOsChecklist));

            var entity = ObterPrimeiro(criteria);

            if (entity == null)
            {
                return false;
            }
            else {
                return entity.ViaStatus == 'A' && entity.LocoStatus == 'A' && entity.TracStatus == 'A' && entity.VagStatus == 'A';
            }           
        }
    }
}
