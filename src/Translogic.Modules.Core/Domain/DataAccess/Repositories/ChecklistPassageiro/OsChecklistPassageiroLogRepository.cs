﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public class OsChecklistPassageiroLogRepository : NHRepository<Os_Checklist_Passageiro_Log, int>, IOsChecklistPassageiroLogRepository 
    {
    }
}