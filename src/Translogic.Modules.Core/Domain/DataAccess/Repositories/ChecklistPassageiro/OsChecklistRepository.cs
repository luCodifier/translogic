﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using System;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using NHibernate.Transform;
    using System.Text;
    using NHibernate;
    using NHibernate.Criterion;

    public class OsChecklistRepository : NHRepository<Os_Checklist, int>, IOsChecklistRepository
    {
        public bool OsDePassageiro(int os)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT OS.X1_PFX_TRE FROM T2_OS OS WHERE OS.X1_NRO_OS = :numOs");
            #endregion

            #region Parameters
            sql = sql.Replace(":numOs", os.ToString());
            #endregion

            #region Execution
            using (var session = OpenSession())
            {
                var queryCount = session.CreateSQLQuery(sql.ToString());
                var prefixoTrem = queryCount.UniqueResult<string>();

                if (string.IsNullOrEmpty(prefixoTrem))
                    return true;

                var prefixo = prefixoTrem.Substring(0, 1);

                return prefixo.ToUpper() == "P";
            }

            #endregion
        }

        public ResultadoPaginado<OsChecklistDto> BuscarOsCheklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, int os = 0)
        {
            #region Query
          
            var sql = new StringBuilder(@"SELECT 
                                            OS.X1_ID_OS as ""IdOs"",
                                            OS.X1_NRO_OS AS ""NumOs"", 
                                            OSCK.ID_OSCK AS ""IdOsCheckList"",
                                            OSCK_APR.ID_OSCK_APR AS ""IdOsCheckListAprovacao"",
                                            OS.X1_PFX_TRE AS ""Prefixo"",
                                            ORG.ao_cod_aop AS ""Origem"",
                                            DST.ao_cod_aop AS ""Destino"",
                                            OS.x1_dat_par_prv_ofi AS ""Data"",
                                            CASE WHEN OSCK_APR.ID_OSCK_APR IS NULL THEN 'N/A'
                                                 WHEN OSCK.X1_ID_OS IS NULL THEN 'N/A' 
                                                 WHEN (OSCK_APR.VIA_IDT_STATUS = 'A' AND OSCK_APR.VIA_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OSCK_APR.VIA_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OSCK_APR.VIA_IDT_STATUS = 'R' AND OSCK_APR.VIA_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OSCK_APR.VIA_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""Via"",
                                            CASE WHEN OSCK_APR.ID_OSCK_APR IS NULL THEN 'N/A'
                                                 WHEN OSCK.X1_ID_OS IS NULL THEN 'N/A' 
                                                 WHEN (OSCK_APR.LOCO_IDT_STATUS = 'A' AND OSCK_APR.LOCO_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OSCK_APR.LOCO_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OSCK_APR.LOCO_IDT_STATUS = 'R' AND OSCK_APR.LOCO_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OSCK_APR.LOCO_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""Locomotiva"",
                                            CASE WHEN OSCK_APR.ID_OSCK_APR IS NULL THEN 'N/A'
                                                 WHEN OSCK.X1_ID_OS IS NULL THEN 'N/A' 
                                                 WHEN (OSCK_APR.TRAC_IDT_STATUS = 'A' AND OSCK_APR.TRAC_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OSCK_APR.TRAC_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OSCK_APR.TRAC_IDT_STATUS = 'R' AND OSCK_APR.TRAC_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OSCK_APR.TRAC_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""Tracao"",
                                            CASE WHEN OSCK_APR.ID_OSCK_APR IS NULL THEN 'N/A'
                                                 WHEN OSCK.X1_ID_OS IS NULL THEN 'N/A' 
                                                 WHEN (OSCK_APR.VAG_IDT_STATUS = 'A' AND OSCK_APR.VAG_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OSCK_APR.VAG_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OSCK_APR.VAG_IDT_STATUS = 'R' AND OSCK_APR.VAG_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OSCK_APR.VAG_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""Vagao"",
                                            CASE WHEN OSCK.X1_ID_OS IS NULL THEN 'N/A' 
                                                 WHEN OSCK_PASS.STATUS = 'P' THEN 'Pendente' 
                                                 WHEN OSCK_PASS.STATUS = 'A' THEN 'Em andamento'
                                                 WHEN OSCK_PASS.STATUS = 'C' THEN 'Concluído'
                                                 ELSE 'Pendente' END AS ""Controlador"",
                                            OSCK.IDT_IDA AS ""Ida"",
                                            OSCK.IDT_VOLTA AS ""Volta"",
                                            CASE WHEN OSCK_PASS.STATUS IS NOT NULL AND OSCK_PASS.STATUS = 'C' THEN 1 
                                                 ELSE 0 END AS ""Concluido""
                                            FROM T2_OS OS
                                            INNER JOIN AREA_OPERACIONAL DST ON DST.AO_ID_AO = OS.X1_IDT_EST_DES 
                                            INNER JOIN AREA_OPERACIONAL ORG  ON ORG.AO_ID_AO = OS.X1_IDT_EST_ORI
                                            LEFT JOIN OS_CHECKLIST OSCK ON OSCK.X1_ID_OS = OS.X1_ID_OS
                                            LEFT JOIN OS_CHECKLIST_APROVACAO OSCK_APR ON OSCK_APR.ID_OSCK = OSCK.ID_OSCK
                                            LEFT JOIN OS_CHECKLIST_PASSAGEIRO OSCK_PASS ON OSCK_PASS.ID_OSCK = OSCK.ID_OSCK
                                            WHERE SUBSTR( OS.X1_PFX_TRE, 0 , 1 ) = 'P'  AND X1_IDT_SIT_OS = 14 ");
            #endregion

            #region Parameters

            if (dataInicial != "" && dataFinal != "")
            {
                sql.AppendFormat(" AND OS.X1_DAT_PAR_PRV_OFI BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial, dataFinal);
            }

            if (os > 0)
            {
                sql.AppendFormat("  AND OS.X1_NRO_OS = {0} ", os.ToString());
            }

            #endregion

            sql.Append(" ORDER BY 1 ");

            #region Execution
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                query.SetResultTransformer(Transformers.AliasToBean<OsChecklistDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<OsChecklistDto>();

                var result = new ResultadoPaginado<OsChecklistDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                foreach (var item in result.Items)
                {
                    item.Via = item.Via.Replace("BUG", "-");
                    item.Locomotiva = item.Locomotiva.Replace("BUG", "-");
                    item.Tracao = item.Tracao.Replace("BUG", "-");
                    item.Vagao = item.Vagao.Replace("BUG", "-");
                }

                return result;
            }

            #endregion
        }

        public Os_Checklist ObterOSCheckListPorIdOS(int idOs)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("IdOs", idOs));

            var entity = ObterPrimeiro(criteria);
            if (entity == null)
            {
                return new Os_Checklist();
            }
            else
            {
                return entity;
            }

        }

        public OsChecklistExportacaoDto ObterConsultaExportacaoPDF(string idOs)
        {
            #region Query
            var sql = new StringBuilder(@"SELECT OS.X1_NRO_OS                                                                   AS ""OS""
                                                ,CP.CP_ID_CPS                                                                   AS ""IdComposicao""
                                                ,OS.X1_ID_OS                                                                    AS ""IdOS""
                                                ,OS.X1_PFX_TRE                                                                  AS ""Prefixo""    
                                                ,TR.TR_ID_TRM                                                                   AS ""IdTrem""
                                                ,OCT.ID_OSCK                                                                    AS ""IdOSCheckList""
                                                ,OCA.VIA_USR_NOME                                                               AS ""ViaAprovador""
                                                ,OCA.VIA_USR_MATR                                                               AS ""ViaMatricula""
                                                ,OCA.LOCO_USR_NOME                                                              AS ""LocomotivaAprovador""
                                                ,OCA.TRAC_USR_NOME                                                              AS ""TracaoAprovador""
                                                ,OCA.VAG_USR_NOME                                                               AS ""VagaoAprovador""
                                                ,OCA.VAG_USR_MATR                                                               AS ""VagaoMatricula""
                                                ,OCA.LOCO_USR_MATR                                                              AS ""LocomotivaMatricula""
                                                ,OCA.TRAC_USR_MATR                                                              AS ""TracaoMatricula""
                                                ,OCA.VAG_USR_MATR                                                               AS ""VagaoMatricula""
                                                ,OCA.VIA_DT_APR                                                                 AS ""ViaData""
                                                ,OCA.LOCO_DT_APR                                                                AS ""LocomotivaData""
                                                ,OCA.TRAC_DT_APR                                                                AS ""TracaoData""
                                                ,OCA.VAG_DT_APR                                                                 AS ""VagaoData""
                                            ,CASE
                                                 WHEN (OCA.VIA_IDT_STATUS = 'A' AND OCA.VIA_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OCA.VIA_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OCA.VIA_IDT_STATUS = 'R' AND OCA.VIA_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OCA.VIA_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""ViaStatus""
                                            ,CASE 
                                                 WHEN (OCA.LOCO_IDT_STATUS = 'A' AND OCA.LOCO_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OCA.LOCO_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OCA.LOCO_IDT_STATUS = 'R' AND OCA.LOCO_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OCA.LOCO_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""LocomotivaStatus""
                                            ,CASE 
                                                 WHEN (OCA.TRAC_IDT_STATUS = 'A' AND OCA.TRAC_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OCA.TRAC_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OCA.TRAC_IDT_STATUS = 'R' AND OCA.TRAC_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OCA.TRAC_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""TracaoStatus""
                                            ,CASE  
                                                 WHEN (OCA.VAG_IDT_STATUS = 'A' AND OCA.VAG_IDT_IMPORTACAO = 'I') THEN 'Aprovado BUG Importado'
                                                 WHEN OCA.VAG_IDT_STATUS = 'A' THEN 'Aprovado' 
                                                 WHEN (OCA.VAG_IDT_STATUS = 'R' AND OCA.VAG_IDT_IMPORTACAO = 'I') THEN 'Reprovado BUG Importado'
                                                 WHEN OCA.VAG_IDT_STATUS = 'R' THEN 'Reprovado'
                                                 ELSE 'Pendente' END AS ""VagaoStatus""
                                                ,OCA.VIA_OBS                                                                    AS ""ViaJustificativa""
                                                ,OCA.LOCO_OBS                                                                   AS ""LocomotivaJustificativa""
                                                ,OCA.TRAC_OBS                                                                   AS ""TracaoJustificativa""
                                                ,OCA.VAG_OBS                                                                    AS ""VagaoJustificativa"" 
                                                ,OCP.EQU_MAQ_NOME                                                               AS ""MaquinistaNome""
                                                ,OCP.EQU_MAQ_MATR                                                               AS ""MaquinistaMatricula""
                                                ,OCP.EQU_AUX_NOME                                                               AS ""AuxiliarNome""
                                                ,OCP.EQU_AUX_MATR                                                               AS ""AuxiliarMatricula""
                                                ,OCP.EQU_SUP_NOME                                                               AS ""SupervisorNome""
                                                ,OCP.EQP_SUP_MATR                                                               AS ""SupervisorMatricula""
                                                ,OCP.LOCOMOTIVA_1                                                               AS ""CodigoLoco1""
                                                ,OCP.DIESEL_1                                                                   AS ""DieselLoco1""
                                                ,OCP.DT_VENCIMENTO_1                                                            AS ""DtVencimentoLoco1""
                                                ,CASE WHEN OCP.CATIVAS_1 = 'X' THEN null           
                                                      WHEN OCP.CATIVAS_1 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.CATIVAS_1 = 'N' THEN 'Não'  END                                  AS ""CativasLoco1""
                                                ,OCP.OBSERVACAO_1                                                               AS ""AdicionaisLoco1""
                                                ,OCP.LOCOMOTIVA_2                                                               AS ""CodigoLoco2""
                                                ,OCP.DIESEL_2                                                                   AS ""DieselLoco2""
                                                ,OCP.DT_VENCIMENTO_2                                                            AS ""DtVencimentoLoco2""
                                                ,CASE WHEN OCP.CATIVAS_2 = 'X' THEN null           
                                                      WHEN OCP.CATIVAS_2 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.CATIVAS_2 = 'N' THEN 'Não'  END                                  AS ""CativasLoco2""
                                                ,OCP.OBSERVACAO_2                                                               AS ""AdicionaisLoco2""
                                                ,OCP.LOCOMOTIVA_3                                                               AS ""CodigoLoco3""
                                                ,OCP.DIESEL_3                                                                   AS ""DieselLoco3""
                                                ,OCP.DT_VENCIMENTO_3                                                            AS ""DtVencimentoLoco3""
                                                ,CASE WHEN OCP.CATIVAS_3 = 'X' THEN null           
                                                      WHEN OCP.CATIVAS_3 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.CATIVAS_3 = 'N' THEN 'Não'  END                                  AS ""CativasLoco3""
                                                ,OCP.OBSERVACAO_3                                                               AS ""AdicionaisLoco3""
                                                ,OCP.LOCOMOTIVA_4                                                               AS ""CodigoLoco4""
                                                ,OCP.DIESEL_4                                                                   AS ""DieselLoco4""
                                                ,OCP.DT_VENCIMENTO_4                                                            AS ""DtVencimentoLoco4""
                                                ,CASE WHEN OCP.CATIVAS_4 = 'X' THEN null           
                                                      WHEN OCP.CATIVAS_4 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.CATIVAS_4 = 'N' THEN 'Não'  END                                  AS ""CativasLoco4""
                                                ,OCP.OBSERVACAO_4                                                               AS ""AdicionaisLoco4""
                                                ,CASE WHEN OCP.RESPOSTA1 = 'X' THEN null           
                                                      WHEN OCP.RESPOSTA1 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.RESPOSTA1 = 'N' THEN 'Não'  END                                  AS ""Resposta1""

                                                ,CASE WHEN OCP.RESPOSTA2 = 'X' THEN null           
                                                      WHEN OCP.RESPOSTA2 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.RESPOSTA2 = 'N' THEN 'Não'  END                                  AS ""Resposta2""

                                                ,CASE WHEN OCP.RESPOSTA3 = 'X' THEN null           
                                                      WHEN OCP.RESPOSTA3 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.RESPOSTA3 = 'N' THEN 'Não'  END                                  AS ""Resposta3""

                                                ,CASE WHEN OCP.RESPOSTA4 = 'X' THEN null           
                                                      WHEN OCP.RESPOSTA4 = 'S' THEN 'Sim'                                                            
                                                      WHEN OCP.RESPOSTA4 = 'N' THEN 'Não'  END                                  AS ""Resposta4""

                                                ,OCP.RESPOSTA5_NUM                                                              AS ""Resposta5""
                                                ,OCP.RESPOSTA6                                                                  AS ""Resposta6""
                                                ,OCP.RESPOSTA7                                                                  AS ""Resposta7""
                                                ,OCP.PREV_PREVISTO                                                              AS ""Previsto""
                                                ,OCP.PREV_REAL                                                                  AS ""Real""
                                                ,OCP.PREV_CHEGADA                                                               AS ""ChegadaLocoOrigem""
                                                ,OCP.STATUS                                                                     AS ""Status""
                                                ,CASE WHEN OCT.IDT_IDA = 1 THEN 'Com Aprovação' ELSE 'Sem Aprovação' END        AS ""Tipo""
                                                ,OCP.CCO_SUP_NOME                                                               AS ""Supervisor""
                                                ,OCP.CTR_SUP_NOME                                                               AS ""Controlador""
                                                ,OCP.COMENT_ADICIONAIS                                                          AS ""ComentAdicionais"" 
                                        FROM OS_CHECKLIST OCT
                                        LEFT JOIN OS_CHECKLIST_APROVACAO OCA ON OCA.ID_OSCK = OCT.ID_OSCK
                                        LEFT JOIN OS_CHECKLIST_PASSAGEIRO OCP ON OCT.ID_OSCK = OCP.ID_OSCK
                                        JOIN T2_OS OS ON OCT.X1_ID_OS = OS.X1_ID_OS
                                        LEFT JOIN TREM TR ON TR.OF_ID_OSV = OS.X1_ID_OS
                                        LEFT JOIN COMPOSICAO CP ON CP.TR_ID_TRM = TR.TR_ID_TRM
                                        WHERE OS.X1_NRO_OS IN (:idOs) AND ROWNUM = 1");
            #endregion

            #region Parameters
            sql = sql.Replace(":idOs", idOs.ToString());
            #endregion

            #region Execution
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OsChecklistExportacaoDto>());

                var result = query.UniqueResult<OsChecklistExportacaoDto>();

                result.ViaStatus = result.ViaStatus.Replace("BUG", "-");
                result.LocomotivaStatus = result.LocomotivaStatus.Replace("BUG", "-");
                result.TracaoStatus = result.TracaoStatus.Replace("BUG", "-");
                result.VagaoStatus = result.VagaoStatus.Replace("BUG", "-");

                return result;
            }
            #endregion
        }
    }
}
