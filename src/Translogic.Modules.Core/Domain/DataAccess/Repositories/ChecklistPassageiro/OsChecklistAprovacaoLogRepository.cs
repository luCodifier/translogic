﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public class OsChecklistAprovacaoLogRepository : NHRepository<Os_Checklist_Aprovacao_Log, int>, IOsChecklistAprovacaoLogRepository 
    {
    }
}