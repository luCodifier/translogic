﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    public class GerenciamentoDocumentacaoRefaturamentoRepository : NHRepository<GerenciamentoDocumentacaoRefaturamento, int>, IGerenciamentoDocumentacaoRefaturamentoRepository
    {
        public GerenciamentoDocumentacaoRefaturamento ObterPorOsRecebedor(decimal osId, decimal recebedorId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.Add(Restrictions.Eq("OsId", osId));
                criteria.Add(Restrictions.Eq("RecebedorId", recebedorId));

                var configuracao = ObterPrimeiro(criteria);
                return configuracao;
            }
        }

        /// <summary>
        /// Retorna todos as solicitações de documentações de refaturamento que estão pendentes de criação inicial
        /// </summary>
        /// <param name="recebedorId">ID do terminal de destino</param>
        /// <returns>Todos os registros cuja documentação ainda não foi criada</returns>
        public ICollection<GerenciamentoDocumentacaoRefaturamento> ObterPendentesGeracao(decimal recebedorId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.CreateAlias("Documentacao", "DOC", JoinType.LeftOuterJoin);
                criteria.Add(Restrictions.Eq("RecebedorId", recebedorId));
                criteria.Add(Restrictions.IsNull("DOC.Id"));

                criteria.AddOrder(Order.Desc("Id"));

                var documentacoes = ObterTodos(criteria);
                return documentacoes;
            }
        }

        /// <summary>
        /// Retorna todos as documentações de refaturamento que estão pendentes de atualização
        /// </summary>
        /// <returns>Todos os registros cuja documentação está pendente de atualização</returns>
        public ICollection<GerenciamentoDocumentacaoRefaturamento> ObterPendentesAtualizacao()
        {
            using (var session = OpenSession())
            {
                var numeroTentativasMaximas = new decimal(5);

                var criteria = CriarCriteria();
                criteria.CreateAlias("Documentacao", "DOC", JoinType.InnerJoin);

                // Deve estar criado
                criteria.Add(Restrictions.IsNotNull("DOC.CriadoEm"));

                // Não excedeu o número de tentativas
                criteria.Add(Restrictions.Le("DOC.Tentativas", numeroTentativasMaximas));

                // Ainda não foi excluído
                criteria.Add(Restrictions.IsNull("DOC.ExcluidoEm"));

                // Está pendente de processamento
                criteria.Add(Restrictions.Eq("Processado", "N"));

                var documentacoes = ObterTodos(criteria);
                return documentacoes;
            }
        }
    }
}