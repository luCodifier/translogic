﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    public class DocumentacaoMotivoRefaturamentoRepository : NHRepository<DocumentacaoMotivoRefaturamento, int>, IDocumentacaoMotivoRefaturamentoRepository
    {
        /// <summary>
        /// Obtem todos os registros ativos
        /// </summary>
        /// <returns>Retorna todos os registros ativos</returns>
        public ICollection<DocumentacaoMotivoRefaturamento> ObterAtivos()
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("Ativo", "S"));

                return ObterTodos(criteria);
            }
        }
    }
}