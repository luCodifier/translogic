﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate.Transform;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="HistoricoEnvioDocumentacao"/>
    /// </summary>
    public class HistoricoEnvioDocumentacaoRepository : NHRepository<HistoricoEnvioDocumentacao, int>, IHistoricoEnvioDocumentacaoRepository
    {
        /// <summary>
        /// Obtem todos os registros pendentes a serem enviados
        /// </summary>
        /// <returns></returns>
        public IList<HistoricoEnvioDocumentacao> ObterPendentes()
        {
            using (var session = OpenSession())
            {
                var hql = @"  
                    FROM HistoricoEnvioDocumentacao HED
                   WHERE HED.Enviado = :enviado
                     AND NOT EXISTS(FROM HistoricoEnvioDocumentacao S 
                                    WHERE S.OsId = HED.OsId 
                                      AND S.Recomposicao = 'S' 
                                      AND S.Id > HED.Id
                                      AND S.ComposicaoId != HED.ComposicaoId)";

                var query = session.CreateQuery(hql);
                query.SetString("enviado", "N");
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<HistoricoEnvioDocumentacao>();
            }
        }
    }
}