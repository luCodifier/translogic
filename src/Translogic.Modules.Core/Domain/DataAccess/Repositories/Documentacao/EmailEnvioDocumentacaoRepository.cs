﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="EmailEnvioDocumentacao"/>
    /// </summary>
    public class EmailEnvioDocumentacaoRepository : NHRepository<EmailEnvioDocumentacao, int>, IEmailEnvioDocumentacaoRepository
    {
        /// <summary>
        /// Obtem todos os e-mails pelo id da configuração
        /// </summary>
        /// <param name="idConfiguracao">ID da configuração</param>
        /// <returns>Todos os e-mails da configuração</returns>
        public ICollection<EmailEnvioDocumentacao> ObterEmailsPorConfiguracao(int idConfiguracao)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.CreateAlias("ConfiguracaoEnvio", "CONF", JoinType.InnerJoin);
                criteria.Add(Restrictions.Eq("CONF.Id", idConfiguracao));

                var emails = ObterTodos(criteria);
                return emails;
            }
        }
        /// <summary>
        /// Obtem todos os e-mails pelo id da configuração
        /// </summary>
        /// <param name="paginacao">Grid da tela</param>
        /// <param name="idConfiguracao">ID da configuração</param>
        /// <returns>Todos os e-mails da configuração</returns>
        public ResultadoPaginado<EmailEnvioDocumentacao> ObterEmailsPorConfiguracaoGrid(DetalhesPaginacao paginacao, int idConfiguracao)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.CreateAlias("ConfiguracaoEnvio", "CONF", JoinType.InnerJoin);
                criteria.Add(Restrictions.Eq("CONF.Id", idConfiguracao));

                var emails = ObterPaginado(criteria, paginacao);
                return emails;
            }
        }
    }
}