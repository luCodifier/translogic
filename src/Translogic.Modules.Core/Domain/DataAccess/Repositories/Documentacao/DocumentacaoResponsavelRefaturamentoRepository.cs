﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    public class DocumentacaoResponsavelRefaturamentoRepository : NHRepository<DocumentacaoResponsavelRefaturamento, int>, IDocumentacaoResponsavelRefaturamentoRepository
    {
    }
}