﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public class GerenciamentoDocumentacaoRepository : NHRepository<GerenciamentoDocumentacao, int>, IGerenciamentoDocumentacaoRepository
    {
        /// <summary>
        /// Obtem registros que estão pendentes para serem excluídos
        /// </summary>
        /// <returns>Documentação que deve ser excluída</returns>
        public ICollection<GerenciamentoDocumentacao> ObterNaoVigentes()
        {
            using (var session = OpenSession())
            {
                var hql = @"  
                    FROM GerenciamentoDocumentacao GD
                   WHERE GD.ExcluidoEm IS     NULL
                     AND GD.CriadoEm   IS NOT NULL 
                     AND GD.CriadoEm + GD.DiasValidade < :dataAtual";

                var query = session.CreateQuery(hql);
                query.SetDateTime("dataAtual", DateTime.Today);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<GerenciamentoDocumentacao>();
            }
        }

        /// <summary>
        /// Obtem os regitros que estão pendentes de geração inicial
        /// </summary>
        /// <returns>Documentações pendentes de criação no FileShare</returns>
        public ICollection<GerenciamentoDocumentacao> ObterNaoGeradosInicialmente()
        {
            using (var session = OpenSession())
            {
                var numeroTentativasMaximas = new decimal(5);

                var criteria = CriarCriteria();
                criteria.Add(Restrictions.IsNull("CriadoEm"));
                criteria.Add(Restrictions.Le("Tentativas", numeroTentativasMaximas));

                return ObterTodos(criteria);
            }
        }

        /// <summary>
        /// Obtem o registro cuja chave de acesso é a informada
        /// </summary>
        /// <param name="chaveAcesso">Chave de acesso</param>
        /// <returns>Documentação cuja chave de acesso seja a passada na parametrização</returns>
        public GerenciamentoDocumentacao ObterPorChaveAcesso(string chaveAcesso)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("ChaveAcesso", chaveAcesso));

                return ObterPrimeiro(criteria);
            }
        }

        /// <summary>
        /// Obtem o registro cujo ID da OS é a informada
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        /// <param name="refaturamento">Indicador se é para procurar registros de refaturamento</param>
        /// <returns>Documentação cujo ID da OS seja a passada na parametrização</returns>
        public GerenciamentoDocumentacao ObterPorOs(decimal osId, decimal recebedorId, bool refaturamento)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("OsId", osId));
                criteria.Add(Restrictions.Eq("RecebedorId", recebedorId));

                if (refaturamento)
                {
                    criteria.Add(Restrictions.IsNotNull("GerenciamentoDocumentacaoRefaturamento"));
                }
                else
                {
                    criteria.Add(Restrictions.IsNull("GerenciamentoDocumentacaoRefaturamento"));    
                }
                
                criteria.AddOrder(Order.Desc("Id"));

                return ObterPrimeiro(criteria);
            }
        }

        /// <summary>
        /// Obtém as documentações pelo número da OS e pelo recebedor, se for informado.
        /// </summary>
        /// <param name="numeroOs">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor. Opcional.</param>
        /// <returns>Documentação cujo número da OS seja a passada no parâmetro do método</returns>
        public ICollection<GerenciamentoDocumentacao> ObterPorNumeroOs(string numeroOs, int? recebedorId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("OsNumero", Convert.ToDecimal(numeroOs)));
                if (recebedorId.HasValue)
                {
                    criteria.Add(Restrictions.Eq("RecebedorId", Convert.ToDecimal(recebedorId.Value.ToString())));
                }

                criteria.AddOrder(Order.Desc("Id"));

                return ObterTodos(criteria);
            }
        }

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        public ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumento(decimal osId, decimal recebedorId)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            #region SQL Query

            hql.Append(@"

                SELECT DISTINCT
                       TOS.X1_ID_OS     AS ""IdOs""
                     , CP.CP_ID_CPS     AS ""IdComposicao""
                     , DP.DP_ID_DP      AS ""IdDespacho""
                     , ITD.ID_ID_ITD    AS ""IdItemDespacho""

                     , VG.VG_COD_VAG    AS ""CodigoVagao""
                     , FC.FX_COD_FLX    AS ""CodigoFluxo"" 

                     , FC.FX_COD_SEGMENTO AS ""SegmentoFluxo""

                     , REMETENTE.EP_DSC_DTL as ""Cliente""
                     , DP.DP_DT       AS ""DataCarga""
                     , CVV.CV_SEQ_CMP AS ""Sequencia""

                     --, NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF) AS ""IdNfePdf""
                     , 0 AS ""IdNfePdf""

                     , LAUDO_MERCADORIA.LaudoId                                     AS ""LaId""
                     , LAUDO_MERCADORIA.ClienteNome                                 AS ""LaClienteNome""
                     , LAUDO_MERCADORIA.ClienteCnpj                                 AS ""LaClienteCnpj""
                     , LAUDO_MERCADORIA.DataClassificacao                           AS ""LaDataClassificacao""
                     , LAUDO_MERCADORIA.NumeroLaudo                                 AS ""LaNumeroLaudo""
                     , LAUDO_MERCADORIA.NumeroOsLaudo                               AS ""LaNumeroOsLaudo""
                     , LAUDO_MERCADORIA.PesoLiquido                                 AS ""LaPesoLiquido""
                     , LAUDO_MERCADORIA.Destino                                     AS ""LaDestino""
                     , LAUDO_MERCADORIA.ClassificadorNome                           AS ""LaClassificadorNome""
                     , LAUDO_MERCADORIA.ClassificadorCpf                            AS ""LaClassificadorCpf""
                     , LAUDO_MERCADORIA.AutorizadoPor                               AS ""LaAutorizadoPor""
                     , LAUDO_MERCADORIA.TipoLaudo                                   AS ""LaTipoLaudo""
                     , LAUDO_MERCADORIA.EmpresaNome                                 AS ""LaEmpresaNome""
                     , LAUDO_MERCADORIA.Produto                                     AS ""LaProduto""
                     , LAUDO_MERCADORIA.Classificacao                               AS ""LaClassificacao""
                     , LAUDO_MERCADORIA.ClassificacaoOrdem                          AS ""LaClassificacaoOrdem""
                     , LAUDO_MERCADORIA.Porcentagem                                 AS ""LaPorcentagem""
                     , LAUDO_MERCADORIA.NumeroNfe                                   AS ""LaNumeroNfe""
                     , LAUDO_MERCADORIA.ChaveNfe                                    AS ""LaChaveNfe""
                     , LAUDO_MERCADORIA.DescricaoProduto                            AS ""LaDescricaoProduto""
                     , LAUDO_MERCADORIA.EmpresaGeradoraId                           AS ""LaEmpresaGeradoraId""
                     , LAUDO_MERCADORIA.EmpresaGeradoraNome                         AS ""LaEmpresaGeradoraNome""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCnpj                         AS ""LaEmpresaGeradoraCnpj""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEndereco                     AS ""LaEmpresaGeradoraEndereco""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCep                          AS ""LaEmpresaGeradoraCep""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCidadeDescricao              AS ""LaEmpresaGeradoraCidadeDesc""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEstadoSigla                  AS ""LaEmpresaGeradoraEstadoSigla""
                     , LAUDO_MERCADORIA.EmpresaGeradoraIe                           AS ""LaEmpresaGeradoraIe""
                     , LAUDO_MERCADORIA.EmpresaGeradoraTelefone                     AS ""LaEmpresaGeradoraTelefone""
                     , LAUDO_MERCADORIA.EmpresaGeradoraFax                          AS ""LaEmpresaGeradoraFax""

                     , NVL(NFE.NFE_SERIE , SIM.NFE_SERIE ) AS ""Serie""
                     , NVL(NFE.NFE_NUM_NF, SIM.NFE_NUM_NF) AS ""Numero""
                     , NVL(NFE.NFE_CHAVE , SIM.NFE_CHAVE ) AS ""ChaveNfe""
                     , NVL((SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM EDI.EDI2_NFE_PROD PROD
                                                      WHERE PROD.NFE_ID_NFE = NFE.NFE_ID_NFE
                                                        AND ROWNUM = 1),
                           (SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM NFE_PROD_SIMCONSULTAS PROD
                                                      WHERE PROD.NFE_ID_NFE = SIM.NFE_ID_NFE
                                                        AND ROWNUM = 1)
                        ) AS ""Produto""

                     , NVL(NF.NF_PESO_NF, SNFE.PESO)     AS ""Peso""
                     , (SNFE.PESO - SNFE.PESO_UTILIZADO) AS ""PesoDisponivel""

                  FROM T2_OS              TOS

                  -- Obtendo a ultima composicao da OS
                  JOIN (
                       SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                            , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                         FROM COMPOSICAO         CP_TMP
                         JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                         JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS = T_TMP.OF_ID_OSV
                        WHERE CP_TMP.CP_DAT_INC > SYSDATE - 120
                          AND EXISTS( SELECT 1 
                                       FROM COMPVAGAO CVV_TMP 
                                      WHERE CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS 
                                        AND ROWNUM = 1 )
                        GROUP BY TOS_TMP.X1_ID_OS
                  ) CP_MAX ON CP_MAX.X1_ID_OS = TOS.X1_ID_OS
                  JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CP_MAX.CP_ID_CPS

                  JOIN COMPVAGAO          CVV ON CVV.CP_ID_CPS = CP.CP_ID_CPS
                  JOIN VAGAO_PEDIDO_VIG   VPV ON VPV.VG_ID_VG = CVV.VG_ID_VG
                  JOIN VAGAO VG ON VG.VG_ID_VG = VPV.VG_ID_VG
                  JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = VPV.CG_ID_CAR
                  JOIN DETALHE_CARREGAMENTO DC ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND DC.VG_ID_VG = VPV.VG_ID_VG
                  JOIN ITEM_DESPACHO ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                  JOIN DESPACHO DP ON DP.DP_ID_DP = ITD.DP_ID_DP
                  JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                  JOIN EMPRESA REMETENTE ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                  JOIN EMPRESA RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST

                  JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP      = ITD.DP_ID_DP
                   LEFT JOIN EDI.EDI2_NFE       NFE     ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN NFE_SIMCONSULTAS   SIM     ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                   LEFT JOIN STATUS_NFE         SNFE    ON SNFE.NFE_CHAVE = NF.NFE_CHAVE_NFE
                   LEFT JOIN (

                                   SELECT DISTINCT
                                            L.ID_WS_LAUDO               AS LaudoId
                                          , L.ID_DESPACHO               AS DespachoId
                                          , L.CODIGO_VAGAO              AS CodigoVagao
                                          , L.CLIENTE_NOME              AS ClienteNome
                                          , L.CLIENTE_CNPJ              AS ClienteCnpj
                                          , L.DATA_CLASSIFICACAO        AS DataClassificacao
                                          , L.NUMERO_LAUDO              AS NumeroLaudo
                                          , L.NUMERO_OS_LAUDO           AS NumeroOsLaudo
                                          , L.PESO_LIQUIDO              AS PesoLiquido
                                          , L.DESTINO                   AS Destino
                                          , L.CLASSIFICADOR_NOME        AS ClassificadorNome
                                          , L.CLASSIFICADOR_CPF         AS ClassificadorCpf
                                          , L.AUTORIZADO_POR            AS AutorizadoPor
                                          , LT.DESCRICAO                AS TipoLaudo
                                          , LE.NOME_EMPRESA             AS EmpresaNome
                                          , LPT.DESCRICAO               AS Produto
                                          , LCT.DESCRICAO               AS Classificacao
                                          , LCO.ORDEM                   AS ClassificacaoOrdem
                                          , LP.VALOR                    AS Porcentagem
                                          , L.NUMERO_NFE                AS NumeroNfe
                                          , L.CHAVE_NFE                 AS ChaveNfe
                                          , L.DESCRICAO_PRODUTO         AS DescricaoProduto

                                          , LE.ID_WS_LAUDO_EMPRESA      AS EmpresaGeradoraId
                                          , LE.NOME_EMPRESA             AS EmpresaGeradoraNome
                                          , LE.CNPJ_EMPRESA             AS EmpresaGeradoraCnpj
                                          , LE.ENDERECO_PRINCIPAL       AS EmpresaGeradoraEndereco
                                          , LE.CEP_EMPRESA              AS EmpresaGeradoraCep
                                          , LE.CIDADE_DESCRICAO         AS EmpresaGeradoraCidadeDescricao
                                          , LE.ESTADO_SIGLA             AS EmpresaGeradoraEstadoSigla
                                          , LE.IE_EMPRESA               AS EmpresaGeradoraIe
                                          , LE.TELEFONE_EMPRESA         AS EmpresaGeradoraTelefone
                                          , LE.FAX_EMPRESA              AS EmpresaGeradoraFax

                                       FROM WS_LAUDO_MENSAGEM             MENSAGEM
                                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = MENSAGEM.ID_WS_LAUDO_MENSAGEM
                                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                  LEFT JOIN WS_LAUDO_CLASSIFICACAO_ORDEM  LCO ON LCO.ID_WS_LAUDO_PRODUTO_TIPO = LPT.ID_WS_LAUDO_PRODUTO_TIPO 
                                                                             AND LCO.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                 INNER JOIN DESPACHO                        D ON D.DP_ID_DP = L.ID_DESPACHO
                                 INNER JOIN ITEM_DESPACHO                 ITD ON ITD.DP_ID_DP = D.DP_ID_DP
                                 INNER JOIN (
                                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                                            , TMP_LOG.ID_WS_LAUDO_EMPRESA
                                         FROM WS_LAUDO_MENSAGEM TMP
                                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                                               , TMP.PROTOCOLO_REQUEST
                                  ) TMP_MAX ON TMP_MAX.ID = MENSAGEM.ID_WS_LAUDO_MENSAGEM

                   ) LAUDO_MERCADORIA ON LAUDO_MERCADORIA.CodigoVagao = VG.VG_COD_VAG 
                     AND TO_DATE(TO_CHAR(LAUDO_MERCADORIA.DataClassificacao,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')

                 WHERE TOS.X1_ID_OS = :osId
                   AND RECEBEDOR.EP_ID_EMP = :recebedorId
			");

            #endregion SQL Query

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetDecimal("osId", osId));
                parametros.Add(q => q.SetDecimal("recebedorId", recebedorId));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GerenciamentoDocumentacaoDto>());
                var itens = query.List<GerenciamentoDocumentacaoDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obtem os dados do documento levando em consideração o histórico
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        public ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoHistorico(decimal osId, decimal recebedorId)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            #region SQL Query

            hql.Append(@"

                SELECT DISTINCT
                       TOS.X1_ID_OS     AS ""IdOs""
                     , CP.CP_ID_CPS     AS ""IdComposicao""
                     , DP.DP_ID_DP      AS ""IdDespacho""
                     , ITD.ID_ID_ITD    AS ""IdItemDespacho""

                     , VG.VG_COD_VAG    AS ""CodigoVagao""

                     , FC.FX_COD_FLX      AS ""CodigoFluxo"" 
                     , FC.FX_COD_SEGMENTO AS ""SegmentoFluxo""

                     , REMETENTE.EP_DSC_DTL as ""Cliente""
                     , DP.DP_DT       AS ""DataCarga""
                     , CVV.CV_SEQ_CMP AS ""Sequencia""

                     --, NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF) AS ""IdNfePdf""
                     , 0 AS ""IdNfePdf""

                     , LAUDO_MERCADORIA.LaudoId                                     AS ""LaId""
                     , LAUDO_MERCADORIA.ClienteNome                                 AS ""LaClienteNome""
                     , LAUDO_MERCADORIA.ClienteCnpj                                 AS ""LaClienteCnpj""
                     , LAUDO_MERCADORIA.DataClassificacao                           AS ""LaDataClassificacao""
                     , LAUDO_MERCADORIA.NumeroLaudo                                 AS ""LaNumeroLaudo""
                     , LAUDO_MERCADORIA.NumeroOsLaudo                               AS ""LaNumeroOsLaudo""
                     , LAUDO_MERCADORIA.PesoLiquido                                 AS ""LaPesoLiquido""
                     , LAUDO_MERCADORIA.Destino                                     AS ""LaDestino""
                     , LAUDO_MERCADORIA.ClassificadorNome                           AS ""LaClassificadorNome""
                     , LAUDO_MERCADORIA.ClassificadorCpf                            AS ""LaClassificadorCpf""
                     , LAUDO_MERCADORIA.AutorizadoPor                               AS ""LaAutorizadoPor""
                     , LAUDO_MERCADORIA.TipoLaudo                                   AS ""LaTipoLaudo""
                     , LAUDO_MERCADORIA.EmpresaNome                                 AS ""LaEmpresaNome""
                     , LAUDO_MERCADORIA.Produto                                     AS ""LaProduto""
                     , LAUDO_MERCADORIA.Classificacao                               AS ""LaClassificacao""
                     , LAUDO_MERCADORIA.ClassificacaoOrdem                          AS ""LaClassificacaoOrdem""
                     , LAUDO_MERCADORIA.Porcentagem                                 AS ""LaPorcentagem""
                     , LAUDO_MERCADORIA.NumeroNfe                                   AS ""LaNumeroNfe""
                     , LAUDO_MERCADORIA.ChaveNfe                                    AS ""LaChaveNfe""
                     , LAUDO_MERCADORIA.DescricaoProduto                            AS ""LaDescricaoProduto""
                     , LAUDO_MERCADORIA.EmpresaGeradoraId                           AS ""LaEmpresaGeradoraId""
                     , LAUDO_MERCADORIA.EmpresaGeradoraNome                         AS ""LaEmpresaGeradoraNome""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCnpj                         AS ""LaEmpresaGeradoraCnpj""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEndereco                     AS ""LaEmpresaGeradoraEndereco""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCep                          AS ""LaEmpresaGeradoraCep""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCidadeDescricao              AS ""LaEmpresaGeradoraCidadeDesc""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEstadoSigla                  AS ""LaEmpresaGeradoraEstadoSigla""
                     , LAUDO_MERCADORIA.EmpresaGeradoraIe                           AS ""LaEmpresaGeradoraIe""
                     , LAUDO_MERCADORIA.EmpresaGeradoraTelefone                     AS ""LaEmpresaGeradoraTelefone""
                     , LAUDO_MERCADORIA.EmpresaGeradoraFax                          AS ""LaEmpresaGeradoraFax""

                     , NVL(NFE.NFE_SERIE , SIM.NFE_SERIE ) AS ""Serie""
                     , NVL(NFE.NFE_NUM_NF, SIM.NFE_NUM_NF) AS ""Numero""
                     , NVL(NFE.NFE_CHAVE , SIM.NFE_CHAVE ) AS ""ChaveNfe""
                     , NVL((SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM EDI.EDI2_NFE_PROD PROD
                                                      WHERE PROD.NFE_ID_NFE = NFE.NFE_ID_NFE
                                                        AND ROWNUM = 1),
                           (SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM NFE_PROD_SIMCONSULTAS PROD
                                                      WHERE PROD.NFE_ID_NFE = SIM.NFE_ID_NFE
                                                        AND ROWNUM = 1)
                        ) AS ""Produto""

                     , NVL(NF.NF_PESO_NF, SNFE.PESO)     AS ""Peso""
                     , (SNFE.PESO - SNFE.PESO_UTILIZADO) AS ""PesoDisponivel""

                  FROM T2_OS              TOS

                  -- Obtendo a ultima composicao do vagão contido na OS
                  JOIN (
                       SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                            , MAX(VPV_TMP.VB_IDT_VPT) AS VB_IDT_VPT
                            , MAX(VPV_TMP_DATA.VB_IDT_VPT) AS VB_IDT_VPT_DATA
                            , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                            , CVV_TMP.VG_ID_VG      AS VG_ID_VG
                         FROM COMPOSICAO         CP_TMP
                         JOIN COMPVAGAO          CVV_TMP    ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                    LEFT JOIN VAGAO_PEDIDO       VPV_TMP    ON VPV_TMP.VG_ID_VG = CVV_TMP.VG_ID_VG AND VPV_TMP.PT_ID_ORT = CVV_TMP.PT_ID_ORT
                    LEFT JOIN VAGAO_PEDIDO       VPV_TMP_DATA ON VPV_TMP_DATA.VG_ID_VG = CVV_TMP.VG_ID_VG AND VPV_TMP_DATA.VB_TIMESTAMP BETWEEN CP_TMP.CP_DAT_INC AND CP_DAT_FIM + 2
                         JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM   = CP_TMP.TR_ID_TRM
                         JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS  = T_TMP.OF_ID_OSV
                        WHERE CP_TMP.CP_DAT_INC > SYSDATE - 60
                        GROUP BY TOS_TMP.X1_ID_OS
                               , CVV_TMP.VG_ID_VG
                  ) CP_MAX ON CP_MAX.X1_ID_OS = TOS.X1_ID_OS
                  JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CP_MAX.CP_ID_CPS
                  JOIN COMPVAGAO          CVV ON CVV.CP_ID_CPS = CP.CP_ID_CPS AND CVV.VG_ID_VG = CP_MAX.VG_ID_VG
                  JOIN VAGAO_PEDIDO VPV ON VPV.VG_ID_VG = CVV.VG_ID_VG AND VPV.VB_IDT_VPT = NVL(CP_MAX.VB_IDT_VPT, CP_MAX.VB_IDT_VPT_DATA)
                  JOIN VAGAO VG ON VG.VG_ID_VG = VPV.VG_ID_VG
                  JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = VPV.CG_ID_CAR
                  JOIN DETALHE_CARREGAMENTO DC ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND DC.VG_ID_VG = VPV.VG_ID_VG
                  JOIN ITEM_DESPACHO ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                  JOIN DESPACHO DP ON DP.DP_ID_DP = ITD.DP_ID_DP
                  JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                  JOIN EMPRESA REMETENTE ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                  JOIN EMPRESA RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST

                  JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP      = ITD.DP_ID_DP
                   LEFT JOIN EDI.EDI2_NFE       NFE     ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN NFE_SIMCONSULTAS   SIM     ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                   LEFT JOIN STATUS_NFE         SNFE    ON SNFE.NFE_CHAVE = NF.NFE_CHAVE_NFE
                   LEFT JOIN (

                                   SELECT DISTINCT
                                            L.ID_WS_LAUDO               AS LaudoId
                                          , L.ID_DESPACHO               AS DespachoId
                                          , L.CODIGO_VAGAO              AS CodigoVagao
                                          , L.CLIENTE_NOME              AS ClienteNome
                                          , L.CLIENTE_CNPJ              AS ClienteCnpj
                                          , L.DATA_CLASSIFICACAO        AS DataClassificacao
                                          , L.NUMERO_LAUDO              AS NumeroLaudo
                                          , L.NUMERO_OS_LAUDO           AS NumeroOsLaudo
                                          , L.PESO_LIQUIDO              AS PesoLiquido
                                          , L.DESTINO                   AS Destino
                                          , L.CLASSIFICADOR_NOME        AS ClassificadorNome
                                          , L.CLASSIFICADOR_CPF         AS ClassificadorCpf
                                          , L.AUTORIZADO_POR            AS AutorizadoPor
                                          , LT.DESCRICAO                AS TipoLaudo
                                          , LE.NOME_EMPRESA             AS EmpresaNome
                                          , LPT.DESCRICAO               AS Produto
                                          , LCT.DESCRICAO               AS Classificacao
                                          , LCO.ORDEM                   AS ClassificacaoOrdem
                                          , LP.VALOR                    AS Porcentagem
                                          , L.NUMERO_NFE                AS NumeroNfe
                                          , L.CHAVE_NFE                 AS ChaveNfe
                                          , L.DESCRICAO_PRODUTO         AS DescricaoProduto

                                          , LE.ID_WS_LAUDO_EMPRESA      AS EmpresaGeradoraId
                                          , LE.NOME_EMPRESA             AS EmpresaGeradoraNome
                                          , LE.CNPJ_EMPRESA             AS EmpresaGeradoraCnpj
                                          , LE.ENDERECO_PRINCIPAL       AS EmpresaGeradoraEndereco
                                          , LE.CEP_EMPRESA              AS EmpresaGeradoraCep
                                          , LE.CIDADE_DESCRICAO         AS EmpresaGeradoraCidadeDescricao
                                          , LE.ESTADO_SIGLA             AS EmpresaGeradoraEstadoSigla
                                          , LE.IE_EMPRESA               AS EmpresaGeradoraIe
                                          , LE.TELEFONE_EMPRESA         AS EmpresaGeradoraTelefone
                                          , LE.FAX_EMPRESA              AS EmpresaGeradoraFax

                                       FROM WS_LAUDO_MENSAGEM             MENSAGEM
                                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = MENSAGEM.ID_WS_LAUDO_MENSAGEM
                                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                  LEFT JOIN WS_LAUDO_CLASSIFICACAO_ORDEM  LCO ON LCO.ID_WS_LAUDO_PRODUTO_TIPO = LPT.ID_WS_LAUDO_PRODUTO_TIPO 
                                                                             AND LCO.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                 INNER JOIN DESPACHO                        D ON D.DP_ID_DP = L.ID_DESPACHO
                                 INNER JOIN ITEM_DESPACHO                 ITD ON ITD.DP_ID_DP = D.DP_ID_DP
                                 INNER JOIN (
                                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                                            , TMP_LOG.ID_WS_LAUDO_EMPRESA
                                         FROM WS_LAUDO_MENSAGEM TMP
                                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                                               , TMP.PROTOCOLO_REQUEST
                                  ) TMP_MAX ON TMP_MAX.ID = MENSAGEM.ID_WS_LAUDO_MENSAGEM

                   ) LAUDO_MERCADORIA ON LAUDO_MERCADORIA.CodigoVagao  = VG.VG_COD_VAG --LAUDO_MERCADORIA.DespachoId = DP.DP_ID_DP
                     AND TO_DATE(TO_CHAR(LAUDO_MERCADORIA.DataClassificacao,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')

                 WHERE TOS.X1_ID_OS = :osId
                   AND RECEBEDOR.EP_ID_EMP = :recebedorId
			");

            #endregion SQL Query

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetDecimal("osId", osId));
                parametros.Add(q => q.SetDecimal("recebedorId", recebedorId));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GerenciamentoDocumentacaoDto>());
                var itens = query.List<GerenciamentoDocumentacaoDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        public ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoRefaturamento(decimal osId, decimal recebedorId)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            #region SQL Query

            hql.Append(@"

                SELECT DISTINCT
                       TOS.X1_ID_OS     AS ""IdOs""
                     , CP.CP_ID_CPS     AS ""IdComposicao""
                     , DP.DP_ID_DP      AS ""IdDespacho""
                     , ITD.ID_ID_ITD    AS ""IdItemDespacho""

                     , VG.VG_COD_VAG    AS ""CodigoVagao""

                     , FC.FX_COD_FLX    AS ""CodigoFluxo"" 
                     , FC.FX_COD_SEGMENTO AS ""SegmentoFluxo""

                     , REMETENTE.EP_DSC_DTL as ""Cliente""
                     , DP.DP_DT       AS ""DataCarga""
                     , CVV.CV_SEQ_CMP AS ""Sequencia""

                     --, NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF) AS ""IdNfePdf""
                     , 0 AS ""IdNfePdf""

                     , LAUDO_MERCADORIA.LaudoId                                     AS ""LaId""
                     , LAUDO_MERCADORIA.ClienteNome                                 AS ""LaClienteNome""
                     , LAUDO_MERCADORIA.ClienteCnpj                                 AS ""LaClienteCnpj""
                     , LAUDO_MERCADORIA.DataClassificacao                           AS ""LaDataClassificacao""
                     , LAUDO_MERCADORIA.NumeroLaudo                                 AS ""LaNumeroLaudo""
                     , LAUDO_MERCADORIA.NumeroOsLaudo                               AS ""LaNumeroOsLaudo""
                     , LAUDO_MERCADORIA.PesoLiquido                                 AS ""LaPesoLiquido""
                     , LAUDO_MERCADORIA.Destino                                     AS ""LaDestino""
                     , LAUDO_MERCADORIA.ClassificadorNome                           AS ""LaClassificadorNome""
                     , LAUDO_MERCADORIA.ClassificadorCpf                            AS ""LaClassificadorCpf""
                     , LAUDO_MERCADORIA.AutorizadoPor                               AS ""LaAutorizadoPor""
                     , LAUDO_MERCADORIA.TipoLaudo                                   AS ""LaTipoLaudo""
                     , LAUDO_MERCADORIA.EmpresaNome                                 AS ""LaEmpresaNome""
                     , LAUDO_MERCADORIA.Produto                                     AS ""LaProduto""
                     , LAUDO_MERCADORIA.Classificacao                               AS ""LaClassificacao""
                     , LAUDO_MERCADORIA.ClassificacaoOrdem                          AS ""LaClassificacaoOrdem""
                     , LAUDO_MERCADORIA.Porcentagem                                 AS ""LaPorcentagem""
                     , LAUDO_MERCADORIA.NumeroNfe                                   AS ""LaNumeroNfe""
                     , LAUDO_MERCADORIA.ChaveNfe                                    AS ""LaChaveNfe""
                     , LAUDO_MERCADORIA.DescricaoProduto                            AS ""LaDescricaoProduto""
                     , LAUDO_MERCADORIA.EmpresaGeradoraId                           AS ""LaEmpresaGeradoraId""
                     , LAUDO_MERCADORIA.EmpresaGeradoraNome                         AS ""LaEmpresaGeradoraNome""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCnpj                         AS ""LaEmpresaGeradoraCnpj""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEndereco                     AS ""LaEmpresaGeradoraEndereco""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCep                          AS ""LaEmpresaGeradoraCep""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCidadeDescricao              AS ""LaEmpresaGeradoraCidadeDesc""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEstadoSigla                  AS ""LaEmpresaGeradoraEstadoSigla""
                     , LAUDO_MERCADORIA.EmpresaGeradoraIe                           AS ""LaEmpresaGeradoraIe""
                     , LAUDO_MERCADORIA.EmpresaGeradoraTelefone                     AS ""LaEmpresaGeradoraTelefone""
                     , LAUDO_MERCADORIA.EmpresaGeradoraFax                          AS ""LaEmpresaGeradoraFax""

                     , NVL(NFE.NFE_SERIE , SIM.NFE_SERIE ) AS ""Serie""
                     , NVL(NFE.NFE_NUM_NF, SIM.NFE_NUM_NF) AS ""Numero""
                     , NVL(NFE.NFE_CHAVE , SIM.NFE_CHAVE ) AS ""ChaveNfe""
                     , NVL((SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM EDI.EDI2_NFE_PROD PROD
                                                      WHERE PROD.NFE_ID_NFE = NFE.NFE_ID_NFE
                                                        AND ROWNUM = 1),
                           (SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM NFE_PROD_SIMCONSULTAS PROD
                                                      WHERE PROD.NFE_ID_NFE = SIM.NFE_ID_NFE
                                                        AND ROWNUM = 1)
                        ) AS ""Produto""

                     , NVL(NF.NF_PESO_NF, SNFE.PESO)     AS ""Peso""
                     , (SNFE.PESO - SNFE.PESO_UTILIZADO) AS ""PesoDisponivel""

                  FROM GERENCIAMENTO_DOC_REFAT      GDR
                  JOIN GERENCIAMENTO_DOC_REFAT_VAG  GDRV ON GDRV.ID_GERENCIAMENTO_DOC_REFAT = GDR.ID_GERENCIAMENTO_DOC_REFAT AND GDRV.ATIVO = 'S'
                  JOIN VAGAO                        VG   ON VG.VG_COD_VAG = GDRV.CODIGO_VAGAO

                  JOIN T2_OS                        TOS  ON TOS.X1_ID_OS = GDR.ID_OS

                  -- Obtendo a ultima composicao do vagão contido na OS
                  JOIN (
                       SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                            , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                            , CVV_TMP.VG_ID_VG      AS VG_ID_VG
                         FROM COMPOSICAO         CP_TMP
                         JOIN COMPVAGAO          CVV_TMP    ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                         JOIN TREM               T_TMP      ON T_TMP.TR_ID_TRM   = CP_TMP.TR_ID_TRM
                         JOIN T2_OS              TOS_TMP    ON TOS_TMP.X1_ID_OS  = T_TMP.OF_ID_OSV
                        WHERE CP_TMP.CP_DAT_INC > SYSDATE - 120
                        GROUP BY TOS_TMP.X1_ID_OS
                               , CVV_TMP.VG_ID_VG
                  ) CP_MAX ON CP_MAX.X1_ID_OS = TOS.X1_ID_OS AND CP_MAX.VG_ID_VG = VG.VG_ID_VG
                  JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CP_MAX.CP_ID_CPS

                  JOIN COMPVAGAO          CVV ON CVV.CP_ID_CPS = CP.CP_ID_CPS AND CVV.VG_ID_VG = VG.VG_ID_VG

                  -- Foi uma premissa do projeto que refaturamento esteja com pedido vigente
                  JOIN VAGAO_PEDIDO_VIG   VPV ON VPV.VG_ID_VG = CVV.VG_ID_VG

                  JOIN CARREGAMENTO CG ON CG.CG_ID_CAR = VPV.CG_ID_CAR
                  JOIN DETALHE_CARREGAMENTO DC ON DC.CG_ID_CAR = VPV.CG_ID_CAR AND DC.VG_ID_VG = VPV.VG_ID_VG
                  JOIN ITEM_DESPACHO ITD ON ITD.DC_ID_CRG = DC.DC_ID_CRG
                  JOIN DESPACHO DP ON DP.DP_ID_DP = ITD.DP_ID_DP
                  JOIN FLUXO_COMERCIAL FC ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                  JOIN EMPRESA REMETENTE ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                  JOIN EMPRESA RECEBEDOR ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST AND RECEBEDOR.EP_ID_EMP = GDR.ID_RECEBEDOR
                  JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP = ITD.DP_ID_DP

                   LEFT JOIN EDI.EDI2_NFE       NFE     ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN NFE_SIMCONSULTAS   SIM     ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                   LEFT JOIN STATUS_NFE         SNFE    ON SNFE.NFE_CHAVE = NF.NFE_CHAVE_NFE

                   LEFT JOIN (

                                   SELECT DISTINCT
                                            L.ID_WS_LAUDO               AS LaudoId
                                          , L.ID_DESPACHO               AS DespachoId
                                          , L.CODIGO_VAGAO              AS CodigoVagao
                                          , L.CLIENTE_NOME              AS ClienteNome
                                          , L.CLIENTE_CNPJ              AS ClienteCnpj
                                          , L.DATA_CLASSIFICACAO        AS DataClassificacao
                                          , L.NUMERO_LAUDO              AS NumeroLaudo
                                          , L.NUMERO_OS_LAUDO           AS NumeroOsLaudo
                                          , L.PESO_LIQUIDO              AS PesoLiquido
                                          , L.DESTINO                   AS Destino
                                          , L.CLASSIFICADOR_NOME        AS ClassificadorNome
                                          , L.CLASSIFICADOR_CPF         AS ClassificadorCpf
                                          , L.AUTORIZADO_POR            AS AutorizadoPor
                                          , LT.DESCRICAO                AS TipoLaudo
                                          , LE.NOME_EMPRESA             AS EmpresaNome
                                          , LPT.DESCRICAO               AS Produto
                                          , LCT.DESCRICAO               AS Classificacao
                                          , LCO.ORDEM                   AS ClassificacaoOrdem
                                          , LP.VALOR                    AS Porcentagem
                                          , L.NUMERO_NFE                AS NumeroNfe
                                          , L.CHAVE_NFE                 AS ChaveNfe
                                          , L.DESCRICAO_PRODUTO         AS DescricaoProduto

                                          , LE.ID_WS_LAUDO_EMPRESA      AS EmpresaGeradoraId
                                          , LE.NOME_EMPRESA             AS EmpresaGeradoraNome
                                          , LE.CNPJ_EMPRESA             AS EmpresaGeradoraCnpj
                                          , LE.ENDERECO_PRINCIPAL       AS EmpresaGeradoraEndereco
                                          , LE.CEP_EMPRESA              AS EmpresaGeradoraCep
                                          , LE.CIDADE_DESCRICAO         AS EmpresaGeradoraCidadeDescricao
                                          , LE.ESTADO_SIGLA             AS EmpresaGeradoraEstadoSigla
                                          , LE.IE_EMPRESA               AS EmpresaGeradoraIe
                                          , LE.TELEFONE_EMPRESA         AS EmpresaGeradoraTelefone
                                          , LE.FAX_EMPRESA              AS EmpresaGeradoraFax

                                       FROM WS_LAUDO_MENSAGEM             MENSAGEM
                                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = MENSAGEM.ID_WS_LAUDO_MENSAGEM
                                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                  LEFT JOIN WS_LAUDO_CLASSIFICACAO_ORDEM  LCO ON LCO.ID_WS_LAUDO_PRODUTO_TIPO = LPT.ID_WS_LAUDO_PRODUTO_TIPO 
                                                                             AND LCO.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                 INNER JOIN DESPACHO                        D ON D.DP_ID_DP = L.ID_DESPACHO
                                 INNER JOIN ITEM_DESPACHO                 ITD ON ITD.DP_ID_DP = D.DP_ID_DP
                                 INNER JOIN (
                                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                                            , TMP_LOG.ID_WS_LAUDO_EMPRESA
                                         FROM WS_LAUDO_MENSAGEM TMP
                                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                                               , TMP.PROTOCOLO_REQUEST
                                  ) TMP_MAX ON TMP_MAX.ID = MENSAGEM.ID_WS_LAUDO_MENSAGEM

                   ) LAUDO_MERCADORIA ON LAUDO_MERCADORIA.CodigoVagao  = VG.VG_COD_VAG --LAUDO_MERCADORIA.DespachoId = DP.DP_ID_DP
                     AND TO_DATE(TO_CHAR(LAUDO_MERCADORIA.DataClassificacao,'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE(TO_CHAR(DP.DP_DT - 3,'dd/MM/yyyy'),'dd/MM/yyyy')

                 WHERE TOS.X1_ID_OS = :osId
                   AND RECEBEDOR.EP_ID_EMP = :recebedorId
			");

            #endregion SQL Query

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetDecimal("osId", osId));
                parametros.Add(q => q.SetDecimal("recebedorId", recebedorId));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GerenciamentoDocumentacaoDto>());
                var itens = query.List<GerenciamentoDocumentacaoDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="itensDespachosIds">Lista de IDs dos itens despachos</param>
        public ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoPorItemDespachos(List<decimal> itensDespachosIds)
        {
            var parametros = new List<Action<IQuery>>();
            var hql = new StringBuilder();

            #region SQL Query

            hql.Append(@"

                SELECT DISTINCT
                       CASE WHEN TOS.X1_ID_OS IS NULL THEN 0 ELSE TOS.X1_ID_OS END AS ""IdOs""
                     , CASE WHEN CP.CP_ID_CPS IS NULL THEN 0 ELSE CP.CP_ID_CPS END AS ""IdComposicao""
                     , DP.DP_ID_DP      AS ""IdDespacho""
                     , ITD.ID_ID_ITD    AS ""IdItemDespacho""

                     , VG.VG_COD_VAG    AS ""CodigoVagao""

                     , REMETENTE.EP_DSC_DTL as ""Cliente""
                     , DP.DP_DT       AS ""DataCarga""
                     , CASE WHEN CVV.CV_SEQ_CMP IS NULL THEN 0 ELSE CVV.CV_SEQ_CMP END AS ""Sequencia""

                     , NVL(PDF_EDI.NFE_ID_PDF, PDF_SIM.NFE_ID_PDF) AS ""IdNfePdf""

                     , LAUDO_MERCADORIA.LaudoId                                     AS ""LaId""
                     , LAUDO_MERCADORIA.ClienteNome                                 AS ""LaClienteNome""
                     , LAUDO_MERCADORIA.ClienteCnpj                                 AS ""LaClienteCnpj""
                     , LAUDO_MERCADORIA.DataClassificacao                           AS ""LaDataClassificacao""
                     , LAUDO_MERCADORIA.NumeroLaudo                                 AS ""LaNumeroLaudo""
                     , LAUDO_MERCADORIA.NumeroOsLaudo                               AS ""LaNumeroOsLaudo""
                     , LAUDO_MERCADORIA.PesoLiquido                                 AS ""LaPesoLiquido""
                     , LAUDO_MERCADORIA.Destino                                     AS ""LaDestino""
                     , LAUDO_MERCADORIA.ClassificadorNome                           AS ""LaClassificadorNome""
                     , LAUDO_MERCADORIA.ClassificadorCpf                            AS ""LaClassificadorCpf""
                     , LAUDO_MERCADORIA.AutorizadoPor                               AS ""LaAutorizadoPor""
                     , LAUDO_MERCADORIA.TipoLaudo                                   AS ""LaTipoLaudo""
                     , LAUDO_MERCADORIA.EmpresaNome                                 AS ""LaEmpresaNome""
                     , LAUDO_MERCADORIA.Produto                                     AS ""LaProduto""
                     , LAUDO_MERCADORIA.Classificacao                               AS ""LaClassificacao""
                     , LAUDO_MERCADORIA.ClassificacaoOrdem                          AS ""LaClassificacaoOrdem""
                     , LAUDO_MERCADORIA.Porcentagem                                 AS ""LaPorcentagem""
                     , LAUDO_MERCADORIA.NumeroNfe                                   AS ""LaNumeroNfe""
                     , LAUDO_MERCADORIA.ChaveNfe                                    AS ""LaChaveNfe""
                     , LAUDO_MERCADORIA.DescricaoProduto                            AS ""LaDescricaoProduto""
                     , LAUDO_MERCADORIA.EmpresaGeradoraId                           AS ""LaEmpresaGeradoraId""
                     , LAUDO_MERCADORIA.EmpresaGeradoraNome                         AS ""LaEmpresaGeradoraNome""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCnpj                         AS ""LaEmpresaGeradoraCnpj""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEndereco                     AS ""LaEmpresaGeradoraEndereco""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCep                          AS ""LaEmpresaGeradoraCep""
                     , LAUDO_MERCADORIA.EmpresaGeradoraCidadeDescricao              AS ""LaEmpresaGeradoraCidadeDesc""
                     , LAUDO_MERCADORIA.EmpresaGeradoraEstadoSigla                  AS ""LaEmpresaGeradoraEstadoSigla""
                     , LAUDO_MERCADORIA.EmpresaGeradoraIe                           AS ""LaEmpresaGeradoraIe""
                     , LAUDO_MERCADORIA.EmpresaGeradoraTelefone                     AS ""LaEmpresaGeradoraTelefone""
                     , LAUDO_MERCADORIA.EmpresaGeradoraFax                          AS ""LaEmpresaGeradoraFax""

                     , NVL(NFE.NFE_SERIE , SIM.NFE_SERIE ) AS ""Serie""
                     , NVL(NFE.NFE_NUM_NF, SIM.NFE_NUM_NF) AS ""Numero""
                     , NVL(NFE.NFE_CHAVE , SIM.NFE_CHAVE ) AS ""ChaveNfe""
                     , NVL((SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM EDI.EDI2_NFE_PROD PROD
                                                      WHERE PROD.NFE_ID_NFE = NFE.NFE_ID_NFE
                                                        AND ROWNUM = 1),
                           (SELECT CONCAT(PROD.CPROD ||' - ', PROD.XPROD)
                                                       FROM NFE_PROD_SIMCONSULTAS PROD
                                                      WHERE PROD.NFE_ID_NFE = SIM.NFE_ID_NFE
                                                        AND ROWNUM = 1)
                        ) AS ""Produto""

                     , NVL(NF.NF_PESO_NF, SNFE.PESO)     AS ""Peso""
                     , (SNFE.PESO - SNFE.PESO_UTILIZADO) AS ""PesoDisponivel""

                        FROM DESPACHO                DP
                 INNER JOIN ITEM_DESPACHO           ITD         ON ITD.DP_ID_DP = DP.DP_ID_DP
                 INNER JOIN VAGAO                   VG          ON VG.VG_ID_VG = ITD.VG_ID_VG
                 INNER JOIN DETALHE_CARREGAMENTO    DC          ON DC.DC_ID_CRG = ITD.DC_ID_CRG
                 INNER JOIN CARREGAMENTO            CG          ON CG.CG_ID_CAR = DC.CG_ID_CAR
                 INNER JOIN FLUXO_COMERCIAL         FC          ON FC.FX_ID_FLX = CG.FX_ID_FLX_CRG
                 INNER JOIN EMPRESA                 REMETENTE   ON REMETENTE.EP_ID_EMP = FC.EP_ID_EMP_REM
                 INNER JOIN EMPRESA                 RECEBEDOR   ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST

                  -- Obtendo a ultima composicao do vagao daquele despacho
                  LEFT JOIN (
                       SELECT COMPV.VG_ID_VG  AS VG_ID_VG
                            , COMPV.PT_ID_ORT AS PT_ID_ORT
                            , MAX(COMPV.CV_IDT_CVG) AS CV_IDT_CVG
                         FROM COMPVAGAO COMPV
                        WHERE COMPV.CV_TIMESTAMP BETWEEN SYSDATE - 60
                                                     AND SYSDATE + 1
                        GROUP BY COMPV.VG_ID_VG
                               , COMPV.PT_ID_ORT
                  ) CV_MAX ON CV_MAX.VG_ID_VG = ITD.VG_ID_VG AND CV_MAX.PT_ID_ORT = DP.PT_ID_ORT
                  LEFT JOIN COMPVAGAO CVV ON CVV.CV_IDT_CVG = CV_MAX.CV_IDT_CVG
                  LEFT JOIN COMPOSICAO CP ON CP.CP_ID_CPS = CVV.CP_ID_CPS
                  LEFT JOIN TREM TR ON TR.TR_ID_TRM = CP.TR_ID_TRM
                  LEFT JOIN AREA_OPERACIONAL ORI_TREM ON ORI_TREM.AO_ID_AO = TR.AO_ID_AO_ORG
                  LEFT JOIN T2_OS TOS ON TOS.X1_ID_OS = TR.OF_ID_OSV

                  JOIN NOTA_FISCAL        NF  ON NF.DP_ID_DP      = ITD.DP_ID_DP
                   LEFT JOIN EDI.EDI2_NFE       NFE     ON NFE.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN NFE_SIMCONSULTAS   SIM     ON SIM.NFE_CHAVE    = NF.NFE_CHAVE_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_EDI ON PDF_EDI.NFE_ID_NFE = NFE.NFE_ID_NFE
                   LEFT JOIN EDI.EDI2_NFE_PDF   PDF_SIM ON PDF_SIM.NFE_ID_SIMCONSULTAS = SIM.NFE_ID_NFE
                   LEFT JOIN STATUS_NFE         SNFE    ON SNFE.NFE_CHAVE = NF.NFE_CHAVE_NFE
                   LEFT JOIN (

                                   SELECT DISTINCT
                                            L.ID_WS_LAUDO               AS LaudoId
                                          , L.ID_DESPACHO               AS DespachoId
                                          , L.CODIGO_VAGAO              AS CodigoVagao
                                          , L.CLIENTE_NOME              AS ClienteNome
                                          , L.CLIENTE_CNPJ              AS ClienteCnpj
                                          , L.DATA_CLASSIFICACAO        AS DataClassificacao
                                          , L.NUMERO_LAUDO              AS NumeroLaudo
                                          , L.NUMERO_OS_LAUDO           AS NumeroOsLaudo
                                          , L.PESO_LIQUIDO              AS PesoLiquido
                                          , L.DESTINO                   AS Destino
                                          , L.CLASSIFICADOR_NOME        AS ClassificadorNome
                                          , L.CLASSIFICADOR_CPF         AS ClassificadorCpf
                                          , L.AUTORIZADO_POR            AS AutorizadoPor
                                          , LT.DESCRICAO                AS TipoLaudo
                                          , LE.NOME_EMPRESA             AS EmpresaNome
                                          , LPT.DESCRICAO               AS Produto
                                          , LCT.DESCRICAO               AS Classificacao
                                          , LCO.ORDEM                   AS ClassificacaoOrdem
                                          , LP.VALOR                    AS Porcentagem
                                          , L.NUMERO_NFE                AS NumeroNfe
                                          , L.CHAVE_NFE                 AS ChaveNfe
                                          , L.DESCRICAO_PRODUTO         AS DescricaoProduto

                                          , LE.ID_WS_LAUDO_EMPRESA      AS EmpresaGeradoraId
                                          , LE.NOME_EMPRESA             AS EmpresaGeradoraNome
                                          , LE.CNPJ_EMPRESA             AS EmpresaGeradoraCnpj
                                          , LE.ENDERECO_PRINCIPAL       AS EmpresaGeradoraEndereco
                                          , LE.CEP_EMPRESA              AS EmpresaGeradoraCep
                                          , LE.CIDADE_DESCRICAO         AS EmpresaGeradoraCidadeDescricao
                                          , LE.ESTADO_SIGLA             AS EmpresaGeradoraEstadoSigla
                                          , LE.IE_EMPRESA               AS EmpresaGeradoraIe
                                          , LE.TELEFONE_EMPRESA         AS EmpresaGeradoraTelefone
                                          , LE.FAX_EMPRESA              AS EmpresaGeradoraFax

                                       FROM WS_LAUDO_MENSAGEM             MENSAGEM
                                 INNER JOIN WS_LAUDO_LOG                  LL  ON LL.ID_WS_LAUDO_MENSAGEM = MENSAGEM.ID_WS_LAUDO_MENSAGEM
                                 INNER JOIN WS_LAUDO_EMPRESA              LE  ON LE.ID_WS_LAUDO_EMPRESA = LL.ID_WS_LAUDO_EMPRESA
                                 INNER JOIN WS_LAUDO                      L   ON L.ID_WS_LAUDO_LOG = LL.ID_WS_LAUDO_LOG
                                 INNER JOIN WS_LAUDO_PRODUTO              LP  ON LP.ID_WS_LAUDO = L.ID_WS_LAUDO
                                 INNER JOIN WS_LAUDO_TIPO                 LT  ON LT.ID_WS_LAUDO_TIPO = L.ID_WS_LAUDO_TIPO
                                 INNER JOIN WS_LAUDO_PRODUTO_TIPO         LPT ON LPT.ID_WS_LAUDO_PRODUTO_TIPO = L.ID_WS_TIPO_PRODUTO
                                 INNER JOIN WS_LAUDO_CLASSIFICACAO_TIPO   LCT ON LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LP.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                  LEFT JOIN WS_LAUDO_CLASSIFICACAO_ORDEM  LCO ON LCO.ID_WS_LAUDO_PRODUTO_TIPO = LPT.ID_WS_LAUDO_PRODUTO_TIPO 
                                                                             AND LCO.ID_WS_LAUDO_CLASSIFICACAO_TIPO = LCT.ID_WS_LAUDO_CLASSIFICACAO_TIPO
                                 INNER JOIN DESPACHO                        D ON D.DP_ID_DP = L.ID_DESPACHO
                                 INNER JOIN ITEM_DESPACHO                 ITD ON ITD.DP_ID_DP = D.DP_ID_DP
                                 INNER JOIN (
                                       SELECT MAX(TMP.ID_WS_LAUDO_MENSAGEM) AS ID
                                            , TMP.PROTOCOLO_REQUEST         AS PROTOCOLO_REQUEST
                                            , TMP_LOG.ID_WS_LAUDO_EMPRESA
                                         FROM WS_LAUDO_MENSAGEM TMP
                                         JOIN WS_LAUDO_LOG TMP_LOG ON TMP_LOG.ID_WS_LAUDO_MENSAGEM = TMP.ID_WS_LAUDO_MENSAGEM
                                        GROUP BY TMP_LOG.ID_WS_LAUDO_EMPRESA
                                               , TMP.PROTOCOLO_REQUEST
                                  ) TMP_MAX ON TMP_MAX.ID = MENSAGEM.ID_WS_LAUDO_MENSAGEM

                   ) LAUDO_MERCADORIA ON LAUDO_MERCADORIA.DespachoId = DP.DP_ID_DP

                 WHERE ITD.ID_ID_ITD IN (:itensDespachosIds)
			");

            #endregion SQL Query

            using (var session = OpenSession())
            {
                parametros.Add(q => q.SetParameterList("itensDespachosIds", itensDespachosIds));

                var query = session.CreateSQLQuery(string.Format(hql.ToString()));

                foreach (var p in parametros)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<GerenciamentoDocumentacaoDto>());
                var itens = query.List<GerenciamentoDocumentacaoDto>();

                return itens;
            }
        }

        /// <summary>
        /// Obtem os dados do documento levando em consideração o ID da composição e o recebedor
        /// </summary>
        /// <param name="composicaoId">ID da composição</param>
        /// <param name="recebedorId">ID do recebedor</param>
        public GerenciamentoDocumentacao ObterPorComposicao(decimal composicaoId, decimal recebedorId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("ComposicaoId", composicaoId));
                criteria.Add(Restrictions.Eq("RecebedorId", recebedorId));

                return ObterPrimeiro(criteria);
            }
        }
    }
}