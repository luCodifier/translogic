﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using System.Globalization;

    /// <summary>
    /// Repositorio para <see cref="ConfiguracaoEnvioDocumentacao"/>
    /// </summary>
    public class ConfiguracaoEnvioDocumentacaoRepository : NHRepository<ConfiguracaoEnvioDocumentacao, int>, IConfiguracaoEnvioDocumentacaoRepository
    {
        /// <summary>
        /// Obtem a configuração por parâmetros
        /// </summary>
        /// <param name="areaOperacional">Código da área operacional</param>
        /// <param name="terminalId">Id do terminal</param>
        /// <returns>A configuração encontrada</returns>
        public ConfiguracaoEnvioDocumentacao ObterPorParametros(string areaOperacional, int terminalId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.CreateAlias("AreaOperacionalEnvio", "AOE", JoinType.InnerJoin);
                criteria.CreateAlias("EmpresaDestino", "ED", JoinType.InnerJoin);
                if (!string.IsNullOrEmpty(areaOperacional))
                {
                    criteria.Add(Restrictions.InsensitiveLike("AOE.Codigo", areaOperacional));
                }
                criteria.Add(Restrictions.Eq("ED.Id", terminalId));

                var configuracao = ObterPrimeiro(criteria);
                return configuracao;
            }
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de envio de documentação de refaturamento
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="malha">Número da Ordem de Serviço</param>
        /// <param name="destino">Número da Ordem de Serviço</param>
        /// <param name="recebedorId">ID do terminal recebedor da mercadoria</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="prefixo">Número da Ordem de Serviço</param>
        /// <param name="dataInicio">Número da Ordem de Serviço</param>
        /// <param name="dataFim">Número da Ordem de Serviço</param>
        /// <param name="tremEncerr">Número da Ordem de Serviço</param>
        /// <param name="situacaoEnvioStatus">Número da Ordem de Serviço</param>
        /// <param name="docCompl">Número da Ordem de Serviço</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<MonitoramentoEnvioDocumentacaoDto> ObterMonitoramentoGrid(DetalhesPaginacao pagination,
                                                                                           string malha,
                                                                                           string destino,
                                                                                           decimal? recebedorId,
                                                                                           string numeroOs,
                                                                                           string prefixo,
                                                                                           string dataInicio,
                                                                                           string dataFim,
                                                                                           string tremEncerr,
                                                                                           int situacaoEnvioStatus,
                                                                                           string docCompl)
        {
            var sql = string.Format(@"
                SELECT DISTINCT 
                       GER.ID_GERENCIAMENTO_DOC   AS ""DocumentacaoId""
                     , TOS.X1_NRO_OS              AS ""OsNumero""
                     , TR.TR_PFX_TRM              AS ""TremPrefixo""
                     , CASE WHEN TR.TR_STT_TRM = 'E' 
                            THEN 'S' 
                            ELSE 'N' 
                             END                  AS ""TremEncerrado""
                     , NVL(HED.ENVIADO, 'N')      AS ""Enviado""
                     , AOE.AO_COD_AOP             AS ""EstacaoEnvio""
                     , CASE WHEN CED.ENVIO_CHEGADA = 'S' THEN 'Na Chegada'                           
                            WHEN CED.ENVIO_SAIDA = 'S'   THEN 'Na Saída' 
                            ELSE 'Ñ Configurado'
                       END AS ""Envio""
                     , HED.CRIADO_EM              AS ""CriadoEm""
                     , HEDE.ENVIADO_EM            AS ""EnviadoEm""
                     , NVL(HED.ENVIO_MANUAL, 'N') AS ""EnvioManual"" 
                     , ENVIADOR.US_NOM_USU        AS ""UsuarioNome""
                     , RECEBEDOR.EP_DSC_RSM       AS ""Recebedor""
                     , CASE WHEN GER.EXCLUIDO_EM IS NULL 
                            THEN 'N' 
                            ELSE 'S' 
                             END                  AS ""DocumentacaoExcluida""
                     , GER.CHAVE_ACESSO           AS ""LinkDownload""
                     , CASE WHEN GER.ID_GERENCIAMENTO_DOC_REFAT IS NULL 
                            THEN 'N' 
                            ELSE 'S' 
                             END AS ""Refaturamento""
                     , HED.RECOMPOSICAO AS ""Recomposicao""
                     , MT_ULTIMO.MT_DTC_PRV AS ""DataChegadaTrem""
                     , CASE WHEN (EXISTS(
                                     SELECT 1 
                                       FROM MOVIMENTACAO_TREM MT
                                       JOIN AREA_OPERACIONAL AO_MT ON AO_MT.AO_ID_AO = MT.AO_ID_AO
                                      WHERE MT.TR_ID_TRM = TR.TR_ID_TRM
                                        AND AO_MT.AO_ID_AO_INF = CED.ID_AO_ID_AO_ENVIO
                                        AND MT.MT_DTS_RAL IS NOT NULL
                                        AND ROWNUM = 1)) THEN 'S' ELSE 'N' END AS ""PassouAreaParametrizada""
                     , TOTAL.VAGOES AS ""QuantidadeVagoes""
                     , AO_MT_MAE_PRIMEIRO.AO_COD_AOP AS ""Origem""
                     , AO_MT_MAE_ULTIMO.AO_COD_AOP   AS ""Destino""
                     , AO_MT_MAE_ATUAL.AO_COD_AOP    AS ""Atual""
                     , CASE WHEN NVL(GER.NFE_COMPLETO   , 'N') = 'S'
                             AND NVL(GER.TICKET_COMPLETO, 'N') = 'S'
                             AND (NVL(GER.LAUDO_COMPLETO , 'N') = 'S' OR NVL(GER.GERAR_REL_LAUDO_MERCADORIA , 'N') = 'N')
                            THEN 'S'
                            ELSE 'N'
                             END AS ""DocumentacaoCompleta""
                  FROM GERENCIAMENTO_DOC GER
                  JOIN CONF_ENVIO_DOC CED ON CED.ID_EP_ID_EMP_DEST = GER.ID_RECEBEDOR
                  JOIN AREA_OPERACIONAL AOE ON AOE.AO_ID_AO = CED.ID_AO_ID_AO_ENVIO
                  JOIN EMPRESA RECEBEDOR ON RECEBEDOR.EP_ID_EMP = GER.ID_RECEBEDOR
                  JOIN TREM TR ON TR.OF_ID_OSV = GER.ID_OS

                  JOIN (
                       SELECT MAX(MT_TMP.MT_ID_MOV) AS MT_ID_MOV
                            , TR_TMP.TR_ID_TRM      AS TR_ID_TRM
                         FROM GERENCIAMENTO_DOC GER_TMP
                         JOIN TREM TR_TMP ON TR_TMP.OF_ID_OSV = GER_TMP.ID_OS
                         JOIN MOVIMENTACAO_TREM MT_TMP ON MT_TMP.TR_ID_TRM = TR_TMP.TR_ID_TRM
                        WHERE MT_TMP.MT_DTC_RAL IS NOT NULL
                        GROUP BY TR_TMP.TR_ID_TRM
                       ) MOV_ATUAL ON MOV_ATUAL.TR_ID_TRM = TR.TR_ID_TRM
                  JOIN MOVIMENTACAO_TREM MT_ATUAL ON MT_ATUAL.MT_ID_MOV = MOV_ATUAL.MT_ID_MOV
                  JOIN AREA_OPERACIONAL AO_MT_ATUAL ON AO_MT_ATUAL.AO_ID_AO = MT_ATUAL.AO_ID_AO
                  JOIN AREA_OPERACIONAL AO_MT_MAE_ATUAL ON AO_MT_MAE_ATUAL.AO_ID_AO = AO_MT_ATUAL.AO_ID_AO_INF

                  JOIN (
                       SELECT MIN(MT_TMP.MT_ID_MOV) AS MIN_MT_ID_MOV
                            , MAX(MT_TMP.MT_ID_MOV) AS MAX_MT_ID_MOV
                            , TR_TMP.TR_ID_TRM AS TR_ID_TRM
                           FROM GERENCIAMENTO_DOC GER_TMP
                           JOIN TREM TR_TMP ON TR_TMP.OF_ID_OSV = GER_TMP.ID_OS
                           JOIN MOVIMENTACAO_TREM MT_TMP ON MT_TMP.TR_ID_TRM = TR_TMP.TR_ID_TRM
                         GROUP BY TR_TMP.TR_ID_TRM
                  ) EXTREMOS_MOVIMENTACOES ON EXTREMOS_MOVIMENTACOES.TR_ID_TRM = TR.TR_ID_TRM

                  JOIN MOVIMENTACAO_TREM MT_PRIMEIRO ON MT_PRIMEIRO.MT_ID_MOV = EXTREMOS_MOVIMENTACOES.MIN_MT_ID_MOV
                  JOIN AREA_OPERACIONAL AO_MT_PRIMEIRO ON AO_MT_PRIMEIRO.AO_ID_AO = MT_PRIMEIRO.AO_ID_AO
                  JOIN AREA_OPERACIONAL AO_MT_MAE_PRIMEIRO ON AO_MT_MAE_PRIMEIRO.AO_ID_AO = AO_MT_PRIMEIRO.AO_ID_AO_INF
                  
                  JOIN MOVIMENTACAO_TREM MT_ULTIMO ON MT_ULTIMO.MT_ID_MOV = EXTREMOS_MOVIMENTACOES.MAX_MT_ID_MOV
                  JOIN AREA_OPERACIONAL AO_MT_ULTIMO ON AO_MT_ULTIMO.AO_ID_AO = MT_ULTIMO.AO_ID_AO
                  JOIN AREA_OPERACIONAL AO_MT_MAE_ULTIMO ON AO_MT_MAE_ULTIMO.AO_ID_AO = AO_MT_ULTIMO.AO_ID_AO_INF
                                    
                  JOIN T2_OS TOS ON TOS.X1_ID_OS = GER.ID_OS
                  JOIN (
                       SELECT MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                            , TR_TMP.TR_ID_TRM AS TR_ID_TRM
                         FROM GERENCIAMENTO_DOC GER_TMP
                         JOIN T2_OS TOS_TMP ON TOS_TMP.X1_ID_OS = GER_TMP.ID_OS
                         JOIN TREM TR_TMP   ON TR_TMP.OF_ID_OSV = TOS_TMP.X1_ID_OS
                         JOIN COMPOSICAO CP_TMP ON CP_TMP.TR_ID_TRM = TR_TMP.TR_ID_TRM
                         WHERE NVL(CP_TMP.CP_CMP_CPS,0)>0
                        GROUP BY TR_TMP.TR_ID_TRM
                       ) ULTIMA_COMPOSICAO ON ULTIMA_COMPOSICAO.TR_ID_TRM = TR.TR_ID_TRM

                  JOIN (
                       SELECT COUNT(DISTINCT CVV_TMP.VG_ID_VG) AS VAGOES
                            , CP_TMP.CP_ID_CPS AS CP_ID_CPS
                         FROM GERENCIAMENTO_DOC GER_TMP
                         JOIN T2_OS TOS_TMP ON TOS_TMP.X1_ID_OS = GER_TMP.ID_OS
                         JOIN TREM TR_TMP   ON TR_TMP.OF_ID_OSV = TOS_TMP.X1_ID_OS
                         JOIN COMPOSICAO CP_TMP ON CP_TMP.TR_ID_TRM = TR_TMP.TR_ID_TRM
                         JOIN COMPVAGAO CVV_TMP ON CVV_TMP.CP_ID_CPS = CP_TMP.CP_ID_CPS
                        GROUP BY CP_TMP.CP_ID_CPS
                      ) TOTAL ON TOTAL.CP_ID_CPS = ULTIMA_COMPOSICAO.CP_ID_CPS

                  LEFT JOIN HIST_ENVIO_DOC HED ON HED.ID_GERENCIAMENTO_DOC = GER.ID_GERENCIAMENTO_DOC
                  INNER JOIN HIST_ENVIO_DOC_EMAIL HEDE ON HEDE.ID_HIST_ENVIO_DOC = HED.ID_HIST_ENVIO_DOC
                  LEFT JOIN USUARIO ENVIADOR ON ENVIADOR.US_IDT_USU = HED.ID_USUARIO_REENVIO
                  INNER JOIN UNID_PRODUCAO  UP   ON UP.UP_ID_UNP  = AO_MT_ATUAL.UP_ID_UNP
                  INNER JOIN MALHA          M    ON M.ML_CD_ML = UP.UP_IND_MALHA                
                 WHERE 1=1
                 AND CED.ATIVO = 'S'
                   {0}
                   {1}
                   {2}
                   {3}
                   {4}
                   {5}
                   /* situacaoEnvioStatus */
                   /* tremEncerrados */
                   /* documentacaoCompleta */                  
                 ORDER BY GER.ID_GERENCIAMENTO_DOC DESC
            ", !string.IsNullOrEmpty(numeroOs) ? string.Format("AND TOS.X1_NRO_OS = {0}", numeroOs) : string.Empty,
                recebedorId.HasValue ? string.Format("AND RECEBEDOR.EP_ID_EMP = {0}", recebedorId.Value) : string.Empty,
                !string.IsNullOrEmpty(prefixo) ? string.Format("AND TR.TR_PFX_TRM = '{0}'", prefixo) : string.Empty,
                !string.IsNullOrEmpty(destino) ? string.Format("AND AO_MT_MAE_ULTIMO.AO_COD_AOP = '{0}'", destino) : string.Empty,
                !string.IsNullOrEmpty(dataInicio) & !string.IsNullOrEmpty(dataFim) ? string.Format("AND MT_ULTIMO.MT_DTC_PRV BETWEEN TO_DATE('{0}','dd/MM/yyyy HH24:MI:SS') AND TO_DATE('{1}','dd/MM/yyyy HH24:MI:SS')",
                    DateTime.ParseExact(dataInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(dataFim, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)) : string.Empty,
                    !string.IsNullOrEmpty(malha) ? (malha.Contains("NORTE") ? string.Format("AND M.ML_DSC_ML IN('{0}','LARGA')", malha) : string.Format("AND M.ML_DSC_ML = '{0}'", malha)) : string.Empty);

            switch (situacaoEnvioStatus)
            {
                case 0: // Registros pendentes
                    sql = sql.Replace("/* situacaoEnvioStatus */", "AND NVL(HED.ENVIADO, 'N') = 'N'");
                    break;
                case 1: // Registros enviadas
                    sql = sql.Replace("/* situacaoEnvioStatus */", "AND NVL(HED.ENVIADO, 'N') = 'S'");
                    break;
                default:
                    sql = sql.Replace("/* situacaoEnvioStatus */", String.Empty);
                    break;
            }

            switch (tremEncerr)
            {
                case "S": // Trem Encerrado
                    sql = sql.Replace("/* tremEncerrados */", "AND TR.TR_STT_TRM = 'E'");
                    break;
                case "N": // Trem Não Encerrado
                    sql = sql.Replace("/* tremEncerrados */", "AND TR.TR_STT_TRM != 'E'");
                    break;
                default: //todos
                    sql = sql.Replace("/* tremEncerrados */", String.Empty);
                    break;
            }

            /* documentacaoCompleta */
            switch (docCompl)
            {
                case "S": // Documentação Completa e não excluída
                    sql = sql.Replace("/* documentacaoCompleta */", "AND (NVL(GER.NFE_COMPLETO, 'N') = 'S' AND NVL(GER.TICKET_COMPLETO, 'N') = 'S' " +
                                                                    "  AND NVL(GER.LAUDO_COMPLETO , 'N') = 'S' AND GER.EXCLUIDO_EM IS NULL)");
                    break;
                case "N": // Documentação Completa ou excluída
                    sql = sql.Replace("/* documentacaoCompleta */", "AND (NVL(GER.NFE_COMPLETO, 'N') = 'N' OR NVL(GER.TICKET_COMPLETO, 'N') = 'N' " +
                                                                    "  AND NVL(GER.LAUDO_COMPLETO , 'N') = 'N' AND GER.EXCLUIDO_EM IS NOT NULL)");
                    break;
                default: //todos
                    sql = sql.Replace("/* documentacaoCompleta */", String.Empty);
                    break;
            }


            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);
            var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql));

            query.SetResultTransformer(Transformers.AliasToBean<MonitoramentoEnvioDocumentacaoDto>());
            query.SetFirstResult(pagination.Inicio ?? 0);
            query.SetMaxResults(pagination.Limite ?? DetalhesPaginacao.LimitePadrao);

            var total = queryCount.UniqueResult<decimal>();
            var itens = query.List<MonitoramentoEnvioDocumentacaoDto>();

            var result = new ResultadoPaginado<MonitoramentoEnvioDocumentacaoDto>
            {
                Items = itens,
                Total = (long)total
            };

            return result;
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de envio de documentação de refaturamento
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="recebedorId">ID do terminal recebedor da mercadoria</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<DocumentacaoRefaturamentoDto> ObterRefaturamentosGrid(DetalhesPaginacao pagination,
                                                                                       string numeroOs,
                                                                                       decimal? recebedorId)
        {
            var sql = string.Format(@"
                SELECT DISTINCT
                       TOS.X1_ID_OS                 AS ""OsId""
                     , TOS.X1_NRO_OS                AS ""NumeroOs""
                     , TOS.X1_PFX_TRE               AS ""PrefixoTrem""
                     , ULTIMA_COMPOSICAO.CP_ID_CPS  AS ""ComposicaoId""
                     , VG.VG_COD_VAG                AS ""CodigoVagao""
                     , EMP.EP_ID_EMP                AS ""RecebedorId""
                     , EMP.EP_DSC_RSM               AS ""Recebedor""
                  FROM T2_OS TOS
                  JOIN TREM              TR   ON TR.OF_ID_OSV = TOS.X1_ID_OS

                    -- Obtendo a ultima composicoes dos vagoes da OS
                  JOIN (
                       SELECT CVV_TMP.VG_ID_VG      AS VG_ID_VG
                            , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                            , MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                         FROM COMPVAGAO  CVV_TMP
                         JOIN COMPOSICAO CP_TMP     ON CP_TMP.CP_ID_CPS = CVV_TMP.CP_ID_CPS
                         JOIN TREM       TR_TMP     ON TR_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                         JOIN T2_OS      TOS_TMP    ON TOS_TMP.X1_ID_OS = TR_TMP.OF_ID_OSV
                        WHERE TOS_TMP.X1_NRO_OS = {0}
                        GROUP BY CVV_TMP.VG_ID_VG
                               , TOS_TMP.X1_ID_OS
                  ) ULTIMA_COMPOSICAO ON ULTIMA_COMPOSICAO.X1_ID_OS = TOS.X1_ID_OS

                  JOIN VAGAO_PEDIDO_VIG  VPV  ON VPV.VG_ID_VG  = ULTIMA_COMPOSICAO.VG_ID_VG
                  JOIN VAGAO             VG   ON VG.VG_ID_VG   = VPV.VG_ID_VG
                  JOIN FLUXO_COMERCIAL   FC   ON FC.FX_ID_FLX  = VPV.FX_ID_FLX
                  JOIN EMPRESA           EMP  ON EMP.EP_ID_EMP = FC.EP_ID_EMP_DST

                    -- Deve existir a configuração do terminal cadastrado na T1398
                  JOIN CONF_ENVIO_DOC    CED  ON CED.ID_EP_ID_EMP_DEST = EMP.EP_ID_EMP

                    -- A configuração deve estar ativa
                 WHERE CED.ATIVO = 'S'
                    -- Deve existir, pelo menos, um e-mail ativo na configuração
                   AND EXISTS(
                                SELECT 1 
                                  FROM EMAIL_ENVIO_DOC EED 
                                 WHERE EED.ID_CONF_ENVIO_DOC_EMAIL = ID_CONF_ENVIO_DOC
                                   AND EED.ATIVO = 'S'
                                   AND ROWNUM = 1 )

                   AND TOS.X1_NRO_OS = {0}
                   {1}
            ", numeroOs, recebedorId.HasValue ? string.Format("AND EMP.EP_ID_EMP     = {0}", recebedorId.Value) : string.Empty);

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetResultTransformer(Transformers.AliasToBean<DocumentacaoRefaturamentoDto>());

            var itens = query.List<DocumentacaoRefaturamentoDto>();
            var total = itens.Count;

            var result = new ResultadoPaginado<DocumentacaoRefaturamentoDto>
            {
                Items = itens,
                Total = (long)total
            };
            return result;
        }

        /// <summary>
        /// Obtem os resultados da grid da tela de parametrização
        /// </summary>
        /// <param name="paginacao">Paginação da Grid</param>
        /// <param name="areaOperacional">Código da Área Operacional de envio</param>
        /// <param name="terminal">Terminal para ser enviado</param>
        /// <param name="ativo">Bool indicando se é para filtrar por registros ativos ou não. Se independe, envie null</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        public ResultadoPaginado<ConfiguracaoEnvioDocumentacao> ObterConfiguracoesGrid(DetalhesPaginacao paginacao,
                                                                                       string areaOperacional,
                                                                                       string terminal,
                                                                                       bool? ativo)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.CreateAlias("AreaOperacionalEnvio", "AOE", JoinType.LeftOuterJoin);
                criteria.CreateAlias("EmpresaDestino", "ED", JoinType.LeftOuterJoin);

                if (!String.IsNullOrEmpty(areaOperacional))
                {
                    criteria.Add(Restrictions.InsensitiveLike("AOE.Codigo", areaOperacional));
                }

                if (!String.IsNullOrEmpty(terminal))
                {
                    int terminalIdOut = 0;
                    if (int.TryParse(terminal, out terminalIdOut) && terminalIdOut > 0)
                    {
                        criteria.Add(Restrictions.Eq("ED.Id", terminalIdOut));
                    }
                    else
                    {
                        criteria.Add(Restrictions.InsensitiveLike("ED.DescricaoResumida", terminal));
                    }
                }

                if (ativo.HasValue)
                {
                    criteria.Add(Restrictions.Eq("Ativo", ativo.Value ? "S" : "N"));
                }

                var configuracoes = ObterPaginado(criteria, paginacao);
                return configuracoes;
            }
        }


        public ConfiguracaoEnvioDocumentacao ObterPorCnpjEmpresa(string cnpj)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                criteria.Add(Restrictions.Eq("Ativo", "S"));
                criteria.CreateAlias("EmpresaDestino", "ED", JoinType.InnerJoin);
                criteria.Add(Restrictions.Eq("ED.Cgc", cnpj));

                var configuracao = ObterPrimeiro(criteria);
                return configuracao;
            }
        }

        /// <summary>
        /// Obtem todos as configurações ativas que possuem vagões destinados a ele. Omitindo o número da OS, verifica-se diretamente na tabela de configurações.
        /// </summary>
        /// <param name="numeroOs">Número da OS</param>
        /// <returns>Retorna todas as empresas cuja configuraação esteja ativa</returns>
        public ICollection<ConfiguracaoEnvioDocumentacaoDto> ObterTerminaisPorOs(string numeroOs)
        {
            var sql = string.Empty;

            // Se não for informado nenhum numero de OS devemos observar diretamente na tabela de configuração
            if (string.IsNullOrEmpty(numeroOs))
            {
                sql = string.Format(@"
                    SELECT DISTINCT
                           EMP.EP_ID_EMP     AS ""RecebedorId""
                         , EMP.EP_DSC_RSM    AS ""Recebedor""
                      FROM CONF_ENVIO_DOC    CED
                      JOIN EMPRESA           EMP  ON EMP.EP_ID_EMP = CED.ID_EP_ID_EMP_DEST

                     -- A configuração deve estar ativa
                     WHERE CED.ATIVO = 'S'

                     -- Deve existir, pelo menos, um e-mail ativo na configuração
                       AND EXISTS(
                                SELECT 1 
                                  FROM EMAIL_ENVIO_DOC EED 
                                 WHERE EED.ID_CONF_ENVIO_DOC_EMAIL = ID_CONF_ENVIO_DOC
                                   AND EED.ATIVO = 'S'
                                   AND ROWNUM = 1 )

                     ORDER BY EMP.EP_DSC_RSM ASC
                ");
            }
            // Informando um número de OS, devemos observar todos os vagões da OS que possuem configuração ativa
            else
            {
                sql = string.Format(@"
                    SELECT DISTINCT
                           EMP.EP_ID_EMP     AS ""RecebedorId""
                         , EMP.EP_DSC_RSM    AS ""Recebedor""
                      FROM T2_OS TOS
                      JOIN TREM              TR   ON TR.OF_ID_OSV = TOS.X1_ID_OS

                      -- Obtendo a ultima composicoes dos vagoes da OS
                      JOIN (
                           SELECT CVV_TMP.VG_ID_VG      AS VG_ID_VG
                                , TOS_TMP.X1_ID_OS      AS X1_ID_OS
                                , MAX(CP_TMP.CP_ID_CPS) AS CP_ID_CPS
                             FROM COMPVAGAO  CVV_TMP
                             JOIN COMPOSICAO CP_TMP     ON CP_TMP.CP_ID_CPS = CVV_TMP.CP_ID_CPS
                             JOIN TREM       TR_TMP     ON TR_TMP.TR_ID_TRM = CP_TMP.TR_ID_TRM
                             JOIN T2_OS      TOS_TMP    ON TOS_TMP.X1_ID_OS = TR_TMP.OF_ID_OSV
                            WHERE TOS_TMP.X1_NRO_OS = {0}
                            GROUP BY CVV_TMP.VG_ID_VG
                                   , TOS_TMP.X1_ID_OS
                      ) ULTIMA_COMPOSICAO ON ULTIMA_COMPOSICAO.X1_ID_OS = TOS.X1_ID_OS

                      JOIN VAGAO_PEDIDO_VIG  VPV  ON VPV.VG_ID_VG  = ULTIMA_COMPOSICAO.VG_ID_VG
                      JOIN VAGAO             VG   ON VG.VG_ID_VG   = VPV.VG_ID_VG
                      JOIN FLUXO_COMERCIAL   FC   ON FC.FX_ID_FLX  = VPV.FX_ID_FLX
                      JOIN EMPRESA           EMP  ON EMP.EP_ID_EMP = FC.EP_ID_EMP_DST

                      -- Deve existir a configuração do terminal cadastrado na T1398
                      JOIN CONF_ENVIO_DOC    CED  ON CED.ID_EP_ID_EMP_DEST = EMP.EP_ID_EMP

                     -- A configuração deve estar ativa
                     WHERE CED.ATIVO = 'S'

                     -- Deve existir, pelo menos, um e-mail ativo na configuração
                       AND EXISTS(
                                SELECT 1 
                                  FROM EMAIL_ENVIO_DOC EED 
                                 WHERE EED.ID_CONF_ENVIO_DOC_EMAIL = ID_CONF_ENVIO_DOC
                                   AND EED.ATIVO = 'S'
                                   AND ROWNUM = 1 )

                     ORDER BY EMP.EP_DSC_RSM ASC

                ", numeroOs);
            }   

            var session = this.SessionManager.OpenStatelessSession();
            var query = session.CreateSQLQuery(sql);

            query.SetResultTransformer(Transformers.AliasToBean<ConfiguracaoEnvioDocumentacaoDto>());
            var itens = query.List<ConfiguracaoEnvioDocumentacaoDto>();

            return itens;
        }
    }
}