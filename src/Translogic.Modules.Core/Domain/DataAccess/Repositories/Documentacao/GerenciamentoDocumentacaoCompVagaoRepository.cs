﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Documentacao;
using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    public class GerenciamentoDocumentacaoCompVagaoRepository : NHRepository<GerenciamentoDocumentacaoCompVagao, int>, IGerenciamentoDocumentacaoCompVagaoRepository
    {
        /// <summary>
        /// Obtem registros que condizem com os parâmetros
        /// </summary>
        public ICollection<GerenciamentoDocumentacaoCompVagao> ObterPorComposicao(decimal osId, decimal composicaoId, decimal? recebedorId)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();
                criteria.Add(Restrictions.Eq("OsId", osId));
                criteria.Add(Restrictions.Eq("ComposicaoId", composicaoId));
                if (recebedorId.HasValue)
                {
                    criteria.Add(Restrictions.Eq("RecebedorId", recebedorId.Value));
                }

                return ObterTodos(criteria);
            }
        }

        /// <summary>
        /// Obtem registros que que antecedem a composição passada no parâmetro
        /// </summary>
        public ICollection<GerenciamentoDocumentacaoCompVagao> ObterPorUltimoEnvioComposicao(decimal osId, decimal composicaoId, decimal? recebedorId)
        {
            using (var session = OpenSession())
            {
                var hql = @"  
                    FROM GerenciamentoDocumentacaoCompVagao GDCV
                   WHERE GDCV.OsId = :osId
                     AND GDCV.RecebedorId = :recebedorId
                     AND GDCV.ComposicaoId = (SELECT MAX(GDCV_TMP.ComposicaoId) 
                                                FROM GerenciamentoDocumentacaoCompVagao GDCV_TMP 
                                               WHERE GDCV_TMP.GerenciamentoDocumentacao.Id = GDCV.GerenciamentoDocumentacao.Id
                                                 AND GDCV_TMP.ComposicaoId < GDCV.ComposicaoId)";

                var query = session.CreateQuery(hql);
                query.SetDecimal("osId", osId);
                query.SetDecimal("recebedorId", recebedorId ?? 0);
                query.SetResultTransformer(Transformers.DistinctRootEntity);

                return query.List<GerenciamentoDocumentacaoCompVagao>();
            }
        }
    }
}