﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="HistoricoEmailEnvioDocumentacao"/>
    /// </summary>
    public class HistoricoEmailEnvioDocumentacaoRepository : NHRepository<HistoricoEmailEnvioDocumentacao, int>, IHistoricoEmailEnvioDocumentacaoRepository
    {
    }
}