﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.LocaisFornecedor
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Linq;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
    using Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories;
    using Translogic.Modules.Core.Util;
    using Translogic.Modules.Core.Domain.Model.Via;
    using ALL.Core.Extensions;

    public class LocalFornecedorRepository : NHRepository<LocalFornecedor, int>, ILocalFornecedorRepository
    {
        public ResultadoPaginado<LocalFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor)
        {
            #region SQL Query
            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT  ID_FORNECEDOR as ""IdFornecedorOs"",
                                       TS.ID_TIPO_SERVICO as ""idTipoServico"",
                                       TS.DESC_SERVICO as ""Servico"", 
                                       AO_COD_AOP as ""Local"" 
                                FROM   TB_LOCAIS_FORNECEDOR LF 
                                       JOIN TB_TIPOSERVICO TS 
                                         ON LF.ID_TIPO_SERVICO = TS.ID_TIPO_SERVICO 
                                       JOIN AREA_OPERACIONAL AO 
                                         ON AO.AO_ID_AO = LF.ID_AREA_OPERACIONAL 
                                WHERE  LF.ID_FORNECEDOR = {0} ", idFornecedor);

            #endregion

            #region orderby
            sql.AppendFormat("ORDER BY AO.AO_COD_AOP ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);

                }

                query.SetResultTransformer(Transformers.AliasToBean<LocalFornecedorDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<LocalFornecedorDto>();

                var result = new ResultadoPaginado<LocalFornecedorDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public bool VerificarExisteFornecedorLocalServico(int idFornecedor, int idLocal, int idServico)
        {
            DetachedCriteria criteria = CriarCriteria()
                .CreateAlias("FornecedoresOs", "fornecedoresOs")
                .CreateAlias("TipoServico", "tipoServico")
                .CreateAlias("EstacaoMae", "estacaoMae")
                .Add(Restrictions.Eq("fornecedoresOs.Id", idFornecedor))
                .Add(Restrictions.Eq("tipoServico.Id", idLocal))
                .Add(Restrictions.Eq("estacaoMae.Id", idServico));

            var list = ObterTodos(criteria).Map<LocalFornecedor, LocalFornecedorDto>();

            if (list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        //        public IList<LocalFornedorDto> ObterLocaisFornencedor(string texto)
        //        {
        //            #region Qry
        //            var sql = new StringBuilder(@"SELECT AO.AO_ID_AO as ""IdLocal""
        //                                                ,AO.AO_COD_AOP as ""Local""
        //                                        FROM AREA_OPERACIONAL AO
        //                                        WHERE ROWNUM <= 50
        //                                        AND AO.AO_COD_AOP LIKE " + texto +
        //                                        "ORDER BY AO_COD_AOP");
        //            #endregion

        //            #region Conecta base
        //            using (var session = OpenSession())
        //            {
        //                var query = session.CreateSQLQuery(sql.ToString());
        //                var lista = query.SetResultTransformer(Transformers.AliasToBean<LocalFornedorDto>()).List<LocalFornedorDto>();
        //                return lista;
        //            }
        //            #endregion
        //        }




        public IList<LocalFornecedorDto> ObterLocalFornencedor(string local)
        {
            throw new NotImplementedException();
        }


        public LocalTipoServicoFornecedorDto ObterLocalFornecedorPorNome(string localEstacao)
        {
            #region qry

            var sql = new StringBuilder();

            sql.AppendFormat(@"SELECT AO.AO_ID_AO as ""IdLocal""
                                        ,AO.AO_COD_AOP as ""LocalEstacao""
                                        FROM AREA_OPERACIONAL AO
                                        WHERE AO.AO_COD_AOP ='{0}'
                                        ORDER BY AO_COD_AOP", localEstacao);
            #endregion

            #region Conecta base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var estacaoServicoDto = query.SetResultTransformer(Transformers.AliasToBean<LocalTipoServicoFornecedorDto>()).List<LocalTipoServicoFornecedorDto>().FirstOrDefault();
                return estacaoServicoDto;
            }
            #endregion
        }

        public void Inserir(IList<LocalFornecedor> locaisFornecedores)
        {
           // this.Inserir(locaisFornecedores);
            foreach (var item in locaisFornecedores)
            {
                this.Inserir(item);
            }
        }


        public IList<LocalFornecedor> ObterLocalFornecedorPorIdFornecedorOs(int? idFornecedorOs)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("FornecedoresOs.Id", idFornecedorOs));

            var lista = ObterTodos(criteria).ToList();
            return lista;
        }

        public void Remover(int idFornecedorOs)
        {
            #region qry
            var parameters = new List<Action<IQuery>>();
            string hql = @"DELETE FROM LocalFornecedor LF WHERE LF.FornecedoresOs.Id = :idFornecedorOs}";
            #endregion

            #region conecta base
            using (ISession session = OpenSession())
            {
                IQuery query = session.CreateQuery(hql);
                query.SetInt32("idFornecedorOs", idFornecedorOs);
                query.ExecuteUpdate();
            }
            #endregion

        }


        public LocalFornecedorDto ObterLocalFornecedorPorIds(int idFornecedorOs, int idTipoServico, int idLocal)
        {
            #region qry
            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT ID_FORNECEDOR AS ""IdFornecedorOs"" ,
                                      ID_TIPO_SERVICO AS ""idTipoServico"",
                                      ID_AREA_OPERACIONAL  AS ""IdLocal""
                                FROM TB_LOCAIS_FORNECEDOR LF
                                WHERE LF.ID_AREA_OPERACIONAL = {0}
                                AND LF.ID_FORNECEDOR = {1}
                                AND LF.ID_TIPO_SERVICO = {2}", idLocal, idFornecedorOs, idTipoServico);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<LocalFornecedorDto>());
                return query.UniqueResult<LocalFornecedorDto>();
            }

            #endregion

        }
    }
}