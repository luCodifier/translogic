﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.MotivoSituacaoVagao
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories;
    using Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes;

    public class SituacaoRepository : NHRepository<Situacao, int?>, ISituacaoRepository
    {
    }
}