﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.MotivoSituacaoVagao
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao;
    using Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories;

    public class MotivoSituacaoVagaoRepository : NHRepository<VagaoMotivo, int>, IMotivoSituacaoVagaoRepository
    {
        public IList<MotivoSituacaoVagaoLotacaoDto> ObterLotacoes()
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                             QRY.ID_CARGA  AS ""IdLotacao""
                                            ,QRY.DSC_CARGA AS ""DescricaoLotacao""
                                        FROM (SELECT 
                                            CG.SH_IDT_STC AS ID_CARGA, 
                                            CG.SH_DSC_PT AS DSC_CARGA
                                          FROM CARGA CG
                                        UNION
                                          SELECT
                                               0       AS ID_CARGA,
                                               'Todas' AS DSC_CARGA
                                          FROM DUAL
                                          ORDER BY ID_CARGA) QRY ");

            #endregion

            #region Where

            // 1) Data Inicio
            //sql.Replace("{dataInicio}", dataInicio.ToString("dd/MM/yyyy"));

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))


            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MotivoSituacaoVagaoLotacaoDto>());
                
                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<MotivoSituacaoVagaoLotacaoDto>();

                var result = itens;

                return result;
            }
        }

        public IList<MotivoSituacaoVagaoSituacaoDto> ObterTodasSituacoes()
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT QRY.ID_SITUACAO   AS ""IdSituacao""
                                               , QRY.DESC_SITUACAO AS ""DescricaoSituacao""
                                         FROM (SELECT S.SI_IDT_STC AS ID_SITUACAO
                                                    , S.SI_DSC_PT AS DESC_SITUACAO
                                                FROM SITUACAO S
                                               UNION
                                              SELECT 0       AS ID_SITUACAO
                                                   , 'Todas' AS DESC_SITUACAO
                                                FROM DUAL) QRY");

            #endregion

            #region OrderBy

            sql.AppendLine(" ORDER BY QRY.ID_SITUACAO ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MotivoSituacaoVagaoSituacaoDto>());

                var itens = query.List<MotivoSituacaoVagaoSituacaoDto>();

                var result = itens;

                return result;
            }
        }

        public IList<MotivoSituacaoVagaoSituacaoDto> ObterSituacoes()
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                           QRY.ID_SITUACAO   AS ""IdSituacao"",
                                           QRY.DESC_SITUACAO AS ""DescricaoSituacao""
                                         FROM (SELECT 
                                                   S.SI_IDT_STC AS ID_SITUACAO, 
                                                   S.SI_DSC_PT AS DESC_SITUACAO
                                              FROM SITUACAO S JOIN 
                                                   SITUACAO_MOTIVOS SM
                                                            ON S.SI_IDT_STC = SM.SI_IDT_STC
                                              UNION
                                                  SELECT
                                                       0       AS ID_SITUACAO,
                                                       'Todas' AS DESC_SITUACAO
                                                  FROM DUAL
                                             ORDER BY ID_SITUACAO) QRY");

            #endregion

            #region Where

            // 1) Data Inicio
            //sql.Replace("{dataInicio}", dataInicio.ToString("dd/MM/yyyy"));

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))


            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MotivoSituacaoVagaoSituacaoDto>());

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<MotivoSituacaoVagaoSituacaoDto>();

                var result = itens;

                return result;
            }
        }

        public IList<MotivoSituacaoVagaoMotivoDto> ObterMotivos(string idSituacao)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                           QRY.ID_SITUACAO  AS ""IdSituacao""
                                          ,QRY.ID_MOTIVO  AS ""IdMotivo""
                                          ,QRY.DSC_MOTIVO AS ""DescricaoMotivo""
                                        FROM (SELECT 
                                              SM.SI_IDT_STC AS ID_SITUACAO,                                       
                                              M.MS_ID_MS AS ID_MOTIVO, 
                                              M.MS_DSC_MOTIVO AS DSC_MOTIVO                                              
                                          FROM MOTIVOS M JOIN 
                                               SITUACAO_MOTIVOS SM 
                                                            ON M.MS_ID_MS = SM.MS_ID_MS  
                                         UNION
                                            SELECT
                                                    0       AS ID_SITUACAO,
                                                    0       AS ID_MOTIVOS,
                                                    'Todos' AS DSC_MOTIVO
                                            FROM DUAL) QRY ");

            #endregion

            #region Where

            int iWhere = 0;

            //1) IdSituacao
            if (!String.IsNullOrWhiteSpace(idSituacao) && idSituacao.Trim() != "0")
            {
                sql.Append(iWhere == 0 ? " WHERE " : " AND ");
                sql.Append(" QRY.ID_SITUACAO IN (0 , ");
                sql.Append(idSituacao);
                sql.Append(") ");

                iWhere++;
            }


            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))


            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MotivoSituacaoVagaoMotivoDto>());

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<MotivoSituacaoVagaoMotivoDto>();

                var result = itens;

                return result;
            }
        }

        public ResultadoPaginado<MotivoSituacaoVagaoDto> ObterConsultaDeVagoesPatio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string listaDeVagoes, string idLocal, string idOS, 
                                                                                    string idLotacao, string idSituacao, string idMotivo)
        {
            #region SQL Query

            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder(@"SELECT 
                                                QRY.COD_VAGAO       AS ""NumeroVagao""
                                               ,QRY.ID_VAGAO        AS ""IdVagao""
                                               ,QRY.LOCAL           AS ""Local""
                                               ,QRY.SERIE           AS ""Serie""
                                               ,QRY.LINHA           AS ""Linha""
                                               ,QRY.SEQUENCIA       AS ""Sequencia""
                                               ,QRY.ID_LOTACAO      AS ""IdLotacao""
                                               ,QRY.DSC_LOTACAO     AS ""DescricaoLotacao""
                                               ,QRY.ID_SITUACAO     AS ""IdSituacao""
                                               ,QRY.DSC_SITUACAO    AS ""DescricaoSituacao""
                                               ,QRY.ID_COND_USO     AS ""IdCondicaoDeUso""
                                               ,QRY.DSC_COND_USO    AS ""DescricaoCondicaoDeUso""
                                               ,QRY.ID_LOCALIZACAO  AS ""IdLocalizacao""
                                               ,QRY.DSC_LOCALIZACAO AS ""DescricaoLocalizacao""
                                               ,QRY.ID_VAGAO_MOTIVO AS ""IdVagaoMotivo""
                                               ,QRY.ID_MOTIVO       AS ""IdMotivo""
                                               ,QRY.DSC_MOTIVO      AS ""DescricaoMotivo""
                                               ,QRY.DT_EMISSAO      AS ""DataEmissao""
                                               ,QRY.NOME_USUARIO    AS ""NomeUsuario""
                                          FROM ( SELECT 
                                                         EN.VG_COD_VAG AS COD_VAGAO,
                                                         VG.VG_ID_VG   AS ID_VAGAO,
                                                         AOM.AO_COD_AOP AS LOCAL,
                                                         SV.SV_COD_SER AS SERIE,
                                                         EV.EV_COD_ELV AS LINHA,
                                                         VPV.VP_NUM_SEQ AS SEQUENCIA,
                                                         EN.SH_IDT_STC AS ID_LOTACAO,
                                                         EN.SH_DSC_PT AS DSC_LOTACAO,
                                                         EN.SI_IDT_STC AS ID_SITUACAO,
                                                         EN.SI_DSC_PT AS DSC_SITUACAO,
                                                         EN.CD_IDT_CUS AS ID_COND_USO,
                                                         EN.CD_DSC_PT AS DSC_COND_USO,
                                                         EN.LO_IDT_LCL AS ID_LOCALIZACAO,
                                                         EN.LO_DSC_PT AS DSC_LOCALIZACAO, 
                                                         VMV.VM_ID_VM AS ID_VAGAO_MOTIVO,
                                                         M.MS_ID_MS AS ID_MOTIVO, 
                                                         M.MS_DSC_MOTIVO AS DSC_MOTIVO,

                                                         VMV.vm_dat_inc AS DT_EMISSAO,
                                                         Nvl(USU.US_NOM_USU,'') as NOME_USUARIO

                                                    FROM VAGAO_PATIO_VIG   VPV,
                                                         ELEMENTO_VIA      EV,
                                                         AREA_OPERACIONAL  AOF,
                                                         AREA_OPERACIONAL  AOM,
                                                         ESTADO_VAGAO_NOVA EN,
                                                         VAGAO             VG,
                                                         SERIE_VAGAO       SV,
                                                         VAGAO_MOTIVO_VIG  VMV,
                                                         MOTIVOS           M,
                                                         USUARIO           USU
          
                                                   WHERE VPV.AO_ID_AO = AOF.AO_ID_AO
                                                     AND AOF.AO_ID_AO_INF = AOM.AO_ID_AO                                                     
                                                     AND VPV.VG_ID_VG = EN.VG_ID_VG
                                                     AND VPV.EV_ID_ELV = EV.EV_ID_ELV
                                                     AND VPV.VG_ID_VG = VG.VG_ID_VG
                                                     AND VG.SV_ID_SV = SV.SV_ID_SV 
                                                     AND VMV.VG_ID_VG (+)= EN.VG_ID_VG
                                                     AND VMV.MS_ID_MS = M.MS_ID_MS (+)
                                                     :where1
                                                     AND EXISTS (SELECT 1
                                                            FROM SITUACAO_MOTIVOS SM
                                                           WHERE SM.SI_IDT_STC = EN.SI_IDT_STC)           
                                                     AND USU.US_USR_IDU (+)= VMV.US_INC

                                                  UNION ALL
                                                  SELECT EN.VG_COD_VAG AS COD_VAGAO,
                                                         VG.VG_ID_VG   AS ID_VAGAO,
                                                         SUBSTR(AOM.AO_COD_AOP, 1, 3) || '-' || TR.TR_PFX_TRM || '-' ||
                                                         TOS.X1_NRO_OS AS LOCAL,
                                                         SV.SV_COD_SER AS SERIE,
                                                         NULL AS LINHA,
                                                         CVV.CV_SEQ_CMP AS SEQUENCIA,
                                                         EN.SH_IDT_STC AS ID_LOTACAO,
                                                         EN.SH_DSC_PT AS DSC_LOTACAO,
                                                         EN.SI_IDT_STC AS ID_SITUACAO,
                                                         EN.SI_DSC_PT AS DSC_SITUACAO,
                                                         EN.CD_IDT_CUS AS ID_COND_USO,
                                                         EN.CD_DSC_PT AS DSC_COND_USO,
                                                         EN.LO_IDT_LCL AS ID_LOCALIZACAO,
                                                         EN.LO_DSC_PT AS DSC_LOCALIZACAO,
                                                         VMV.VM_ID_VM AS ID_VAGAO_MOTIVO,
                                                         M.MS_ID_MS AS ID_MOTIVO, 
                                                         M.MS_DSC_MOTIVO AS DSC_MOTIVO,
                                                         VMV.vm_dat_inc AS DT_EMISSAO,
                                                         Nvl(USU.US_NOM_USU,'') as NOME_USUARIO

                                                    FROM COMPVAGAO_VIG     CVV,
                                                         COMPOSICAO        CP,
                                                         TREM              TR,
                                                         AREA_OPERACIONAL  AOM,
                                                         ESTADO_VAGAO_NOVA EN,
                                                         VAGAO             VG,
                                                         SERIE_VAGAO       SV,
                                                         T2_OS             TOS,
                                                         VAGAO_MOTIVO_VIG  VMV,
                 
                                                         MOTIVOS           M,
                                                         USUARIO           USU

                                                   WHERE TR.AO_ID_AO_PVT = AOM.AO_ID_AO                                                     
                                                     AND TR.TR_STT_TRM = 'R'
                                                     AND TR.OF_ID_OSV = TOS.X1_ID_OS
                                                     AND CP.TR_ID_TRM = TR.TR_ID_TRM
                                                     AND CP.CP_ID_CPS = CVV.CP_ID_CPS
                                                     AND CVV.VG_ID_VG = EN.VG_ID_VG
                                                     AND EN.VG_ID_VG = VG.VG_ID_VG
                                                     AND VG.SV_ID_SV = SV.SV_ID_SV
                                                     AND VMV.VG_ID_VG (+)= EN.VG_ID_VG
                                                     AND VMV.MS_ID_MS = M.MS_ID_MS (+)
                                                     AND USU.US_USR_IDU (+)= VMV.US_INC
                                                     :where2
                                                     AND EXISTS (SELECT 1
                                                            FROM SITUACAO_MOTIVOS SM
                                                           WHERE SM.SI_IDT_STC = EN.SI_IDT_STC)) QRY ");

            #endregion

            #region Where
            var sWhere1 = new StringBuilder("");
            var sWhere2 = new StringBuilder("");

            //1) Lista de Vagoes
            if (!String.IsNullOrWhiteSpace(listaDeVagoes))
            {
                String[] result = listaDeVagoes.Split(';');

                string vagoes = string.Empty;
                                          
                foreach (var item in result)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        vagoes += "'" + item.Trim() + "',";
                    }
                }

                sWhere1.Append(" AND EN.VG_COD_VAG IN ( " + vagoes.Substring(0, vagoes.Length - 1) + ")");
                sWhere2.Append(sWhere1);
            }

            //2) Local
            if (!String.IsNullOrWhiteSpace(idLocal))
            {
                sWhere1.Append(" AND AOM.AO_COD_AOP = '" + idLocal.ToUpper() + "'");
                sWhere2.Append(sWhere1);
            }

            //3) OS
            if (!String.IsNullOrWhiteSpace(idOS))
            {
                sWhere2.Append(" AND TOS.X1_NRO_OS = " + idOS);
                /* quando filtrar pela OS, só retorna os dados do select de baixo */
                sWhere1.Append(" AND 1 = 2");
            }

            //4) Lotação
            if (!String.IsNullOrWhiteSpace(idLotacao) && idLotacao.Trim() != "0")
            {
                sWhere1.Append(" AND EN.SH_IDT_STC = " + idLotacao);
                sWhere2.Append(sWhere1);
            }

            //5) Situacao
            if (!String.IsNullOrWhiteSpace(idSituacao) && idSituacao.Trim() != "0")
            {
                sWhere1.Append(" AND EN.SI_IDT_STC = " + idSituacao);
                sWhere2.Append(sWhere1);
            }

            //6) Situacao
            if (!String.IsNullOrWhiteSpace(idMotivo) && idMotivo.Trim() != "0")
            {
                sWhere1.Append(" AND M.MS_ID_MS = " + idMotivo);
                sWhere2.Append(sWhere1);
            }

            sql = sql.Replace(":where1", sWhere1.ToString()).Replace(":where2", sWhere2.ToString());

            #endregion

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))

                //sql.Append(" ORDER BY QRY.COD_VAGAO, ");
                sql.Append(" ORDER BY QRY.LOCAL, ");
                sql.Append("          QRY.LINHA, ");
                sql.Append("          QRY.SEQUENCIA ASC");
                                                            
            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<MotivoSituacaoVagaoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }
                
                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<MotivoSituacaoVagaoDto>();

                var result = new ResultadoPaginado<MotivoSituacaoVagaoDto>
                {
                    Items =   itens,
                    Total = (long) total
                };

                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idVagaoMotivo"></param>
        /// <param name="idVagao"></param>
        /// <param name="idMotivo"></param>
        /// <returns></returns>
        public VagaoMotivo ObterVagaoMotivo(string idVagaoMotivo, string idVagao, string idMotivo)
        {
            VagaoMotivo itemVagaoMotivo;

            using (ISession session = OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<VagaoMotivo>();
                criteria.Add(Restrictions.Eq("IdVagaoMotivo", Convert.ToInt32(idVagaoMotivo)));
                criteria.Add(Restrictions.Eq("IdVagao", Convert.ToInt32(idVagao)));
                criteria.Add(Restrictions.Eq("IdMotivo", Convert.ToInt32(idMotivo)));
                criteria.Add(Restrictions.IsNull("DataDaExclusao"));
                
                //Inserir consulta pelo valor da data de exclusao nula (Shirata)

                itemVagaoMotivo = criteria.UniqueResult<VagaoMotivo>();
            }

            return itemVagaoMotivo;
        }
    }
}