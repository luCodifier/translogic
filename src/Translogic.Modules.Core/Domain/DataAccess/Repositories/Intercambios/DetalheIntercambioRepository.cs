namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Intercambios
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.Intercambios;
    using Translogic.Modules.Core.Domain.Model.Intercambios.Repositories;

    /// <summary>
	/// Repositorio para <see cref="DetalheCarregamento"/>
	/// </summary>
	public class DetalheIntercambioRepository : NHRepository<DetalheIntercambio, int?>, IDetalheIntercambioRepository
	{
	}
}