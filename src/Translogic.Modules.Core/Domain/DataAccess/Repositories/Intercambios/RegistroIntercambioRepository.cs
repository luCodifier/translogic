namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Intercambios
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Intercambios;
    using Translogic.Modules.Core.Domain.Model.Intercambios.Repositories;

    /// <summary>
	/// Repositorio para <see cref="RegistroIntercambio"/>
	/// </summary>
	public class RegistroIntercambioRepository : NHRepository<RegistroIntercambio, int?>, IRegistroIntercambioRepository
	{
	}
}