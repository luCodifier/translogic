﻿using System;
using NHibernate;
using NHibernate.Transform;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Despacho
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Despacho;
    using Model.Despacho.Repositories;
    using Model.Dto.Despacho;
    using Translogic.Core.Infrastructure.Web;
    using System.Text;
    using System.Collections.Generic;

    public class ConfiguracaoDespachoAutomaticoRepository : NHRepository<ConfiguracaoDespachoAutomatico, int>, IConfiguracaoDespachoAutomaticoRepository
    {
        public ResultadoPaginado<ConfiguracaoDespachoAutomaticoDto> Pesquisar(DetalhesPaginacaoWeb pagination, ConfiguracaoDespachoAutomaticoFiltroDto filtro)
        {
            if (filtro == null)
                throw new ArgumentException("Filtro de pesquisa está inválido");

            var sql = ObterConsultaSql(filtro);

            var sqlQuery = string.Format(sql, @"ca_id_ca AS ""Id"", 
                                       cfg.cliente_dsc  AS ""Cliente"",
                                       ao.ao_cod_aop    AS ""AreaOperacional"",
                                       l.lo_dsc_pt      AS ""Localizacao"",
                                       dsc_segmento     AS ""Segmento"",
                                       ind_carga_term   AS ""Terminal"",
                                       ind_carga_patio  AS ""Patio"",
                                       ca_lib_manual    AS ""Manual"",
                                       ca_timestamp     AS ""Data""");

            var sqlCount = string.Format(sql, " COUNT(ca_id_ca) ");

            sqlQuery += " ORDER BY ";

            if (pagination.ColunasOrdenacao.Count > 0)
            {
                var indiceOrdenacao = 1;

                switch (pagination.ColunasOrdenacao[0])
                {
                    case "Id":
                        indiceOrdenacao = 1;
                        break;
                    case "Cliente":
                        indiceOrdenacao = 2;
                        break;
                    case "AreaOperacional":
                        indiceOrdenacao = 3;
                        break;
                    case "Localizacao":
                        indiceOrdenacao = 4;
                        break;
                    case "Segmento":
                        indiceOrdenacao = 5;
                        break;
                    case "Terminal":
                        indiceOrdenacao = 6;
                        break;
                    case "Patio":
                        indiceOrdenacao = 7;
                        break;
                    case "Manual":
                        indiceOrdenacao = 8;
                        break;
                    case "Data":
                        indiceOrdenacao = 9;
                        break;
                }

                sqlQuery += string.Format("{0} {1}", indiceOrdenacao, pagination.DirecoesOrdenacao[0]);

            }
            else
            {
                sqlQuery += "CA_TIMESTAMP ";
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);
                var queryCount = session.CreateSQLQuery(sqlCount);

                ConfigurarFiltro(filtro, query);
                ConfigurarFiltro(filtro, queryCount);

                var total = queryCount.UniqueResult<decimal>(); 

                query.SetResultTransformer(Transformers.AliasToBean(typeof(ConfiguracaoDespachoAutomaticoDto)));

                if (pagination.Inicio.HasValue)
                {
                    query.SetFirstResult(pagination.Inicio.Value);
                }

                int limit = GetLimit(pagination);
                if (limit > 0)
                {
                    query.SetMaxResults(limit);
                }

                var items = query.List<ConfiguracaoDespachoAutomaticoDto>();

                return new ResultadoPaginado<ConfiguracaoDespachoAutomaticoDto> { Total = (long)total, Items = items };
            }
        }

        private static void ConfigurarFiltro(ConfiguracaoDespachoAutomaticoFiltroDto filtro, IQuery query)
        {
            query.SetParameter("dtInicial", DateTime.Parse(filtro.DtInicial));
            query.SetParameter("dtFinal", DateTime.Parse(filtro.DtFim).AddDays(1).AddSeconds(-1));

            if (!string.IsNullOrEmpty(filtro.Id))
            {
                query.SetParameter("id", filtro.Id);
            }

            if (!string.IsNullOrEmpty(filtro.Cliente))
            {
                query.SetParameter("cliente", filtro.Cliente);
            }

            if (!string.IsNullOrEmpty(filtro.LocalizacaoId))
            {
                query.SetParameter("localizacao", filtro.LocalizacaoId);
            }

            if (!string.IsNullOrEmpty(filtro.Segmento))
            {
                query.SetParameter("segmento", filtro.Segmento);
            }

            if (!string.IsNullOrEmpty(filtro.Terminal))
            {
                query.SetParameter("terminal", filtro.Terminal);
            }

            if (!string.IsNullOrEmpty(filtro.Patio))
            {
                query.SetParameter("patio", filtro.Patio);
            }

            if (!string.IsNullOrEmpty(filtro.Manual))
            {
                query.SetParameter("manual", filtro.Manual);
            }

            if (!string.IsNullOrEmpty(filtro.AreaOperacional))
            {
                query.SetParameter("areaOperacional", filtro.AreaOperacional);
            }
        }

        private static string ObterConsultaSql(ConfiguracaoDespachoAutomaticoFiltroDto filtro)
        {
            var sql = new StringBuilder();

            sql.Append(@"SELECT {0} 
                        FROM
                           bo_bolar_conf_carga_aut aut 
                           JOIN
                              bo_bolar_imp_cfg cfg 
                              ON cfg.cliente = aut.ao_cod_aop 
                           JOIN
                              area_operacional ao 
                              ON aut.ao_id_est = ao.ao_id_ao 
                           LEFT JOIN
                              localizacao l 
                              ON l.lo_idt_lcl = aut.lo_idt_lco 
                        WHERE ca_timestamp BETWEEN :dtInicial AND :dtFinal ");


            if (!string.IsNullOrEmpty(filtro.Id))
            {
                sql.Append(" AND aut.ca_id_ca = :id");
            }

            if (!string.IsNullOrEmpty(filtro.Cliente))
            {
                sql.Append(" AND aut.ao_cod_aop = :cliente");
            }

            if (!string.IsNullOrEmpty(filtro.LocalizacaoId))
            {
                sql.Append(" AND aut.lo_idt_lco = :localizacao");
            }

            if (!string.IsNullOrEmpty(filtro.Segmento))
            {
                sql.Append(" AND aut.dsc_segmento = :segmento");
            }

            if (!string.IsNullOrEmpty(filtro.Terminal))
            {
                sql.Append(" AND aut.ind_carga_term = :terminal");
            }

            if (!string.IsNullOrEmpty(filtro.Patio))
            {
                sql.Append(" AND aut.ind_carga_patio = :patio");
            }

            if (!string.IsNullOrEmpty(filtro.Manual))
            {
                sql.Append(" AND aut.ca_lib_manual = :manual");
            }

            if (!string.IsNullOrEmpty(filtro.AreaOperacional))
            {
                sql.Append(" AND aut.ao_id_est = :areaOperacional");
            }

            return sql.ToString();
        }


        public bool JaCadastrado(int id, string cliente, int areaOperacional, string segmento)
        {
            var sql = new StringBuilder();

            sql.Append(@"SELECT
                           COUNT(*) 
                        FROM
                           bo_bolar_conf_carga_aut aut 
                        WHERE
                           aut.ca_id_ca <> :id 
                           AND aut.ao_cod_aop = :cliente 
                           AND aut.ao_id_est = :areaOperacional 
                           AND aut.dsc_segmento = :segmento");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetInt32("id", id);
                query.SetString("cliente", cliente);
                query.SetInt32("areaOperacional", areaOperacional);
                query.SetString("segmento", segmento);

                var valor = query.UniqueResult<decimal>();

                return valor > 0;
            }
        }


        public IList<ConfiguracaoDespachoAutomaticoDto> Exportar(ConfiguracaoDespachoAutomaticoFiltroDto filtro)
        {
            if (filtro == null)
                throw new ArgumentException("Filtro de pesquisa está inválido");

            var sql = ObterConsultaSql(filtro);

            var sqlQuery = string.Format(sql, @"ca_id_ca AS ""Id"", 
                                       cfg.cliente_dsc  AS ""Cliente"",
                                       ao.ao_cod_aop    AS ""AreaOperacional"",
                                       l.lo_dsc_pt      AS ""Localizacao"",
                                       dsc_segmento     AS ""Segmento"",
                                       ind_carga_term   AS ""Terminal"",
                                       ind_carga_patio  AS ""Patio"",
                                       ca_lib_manual    AS ""Manual"",
                                       ca_timestamp     AS ""Data""");


            sqlQuery += " ORDER BY CA_TIMESTAMP";

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sqlQuery);

                ConfigurarFiltro(filtro, query);

                query.SetResultTransformer(Transformers.AliasToBean(typeof(ConfiguracaoDespachoAutomaticoDto)));

                return query.List<ConfiguracaoDespachoAutomaticoDto>();
            }


        }
    }
}