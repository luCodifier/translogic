﻿
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.TipoServico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.TipoServicos;
    using Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories;    
    using NHibernate.Criterion;

    public class TipoServicoRepository : NHRepository<TipoServico, int>, ITipoServicoRepository
    {       

        public IList<TipoServicoDto> ObterTipoServico()
        {
            #region Qry
            var sql = new StringBuilder(@"SELECT TS.ID_TIPO_SERVICO as ""IdTipoServico"", 
                                                 TS.DESC_SERVICO as ""DescricaoServico""
                                            FROM   TB_TIPOSERVICO TS 
                                            UNION
                                                SELECT
                                                  0 AS ID_TIPO_SERVICO,
                                                  'Todos' AS DESC_SERVICO
                                                  FROM DUAL");
            #endregion

            #region Conecta base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var lista = query.SetResultTransformer(Transformers.AliasToBean<TipoServicoDto>()).List<TipoServicoDto>();
                return lista;
            }
            #endregion
        }

        public TipoServico ObterTipoServicoPorDescricaoServico(string descricaoTipoServico)
        {
            return ObterTodos().FirstOrDefault(ts => ts.Servico.Equals(descricaoTipoServico));           
        }
    }
}