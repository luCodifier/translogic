﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Consultas
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Transform;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Consultas;
    using Translogic.Modules.Core.Domain.Model.Consultas.Repositories;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public class VagaoHistoricoTaraRepository : NHRepository<VagaoHistoricoTara, int>, IVagaoHistoricoTaraRepository
    {
        public ResultadoPaginado<VagaoHistoricoTaraDto> ObterHistoricoTaras(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string vagoes, string serie, string dataInicial, string dataFinal)
        {
            var sqlWihtoutOrderBy = new StringBuilder();
            var sql = new StringBuilder(@"
              SELECT SV.SV_COD_SER       AS Serie,
                   VV.VG_COD_VAG         AS CodVagao,
                   VV.VP_DATA_PROCESSADO AS Data,
                   VV.TARA_ANTERIOR      AS TaraAnterior,
                   VV.TARA_MEDIANA       AS TaraMediana,
                   VG.VG_NUM_TRA         AS TaraAtual
              FROM (SELECT MAX(VP.VP_ID) AS ID, VP.VG_COD_VAG AS COD_VAGAO
                      FROM VAGAO_PESAGEM_PLANILHA VP
                     WHERE VP.VP_DATA_PROCESSADO IS NOT NULL
                       AND VP.IND_PROC_COM_ERR = 'N'     
                     GROUP BY VP.VG_COD_VAG) TAB_AUX,
                   VAGAO_PESAGEM_PLANILHA VV,
                   VAGAO VG,
                   SERIE_VAGAO SV
              WHERE VV.VP_ID = TAB_AUX.ID
                  AND VV.VG_COD_VAG = VG.VG_COD_VAG
                  AND VG.SV_ID_SV = SV.SV_ID_SV ");

            if (!(string.IsNullOrEmpty(vagoes)))
            {
                sql.AppendLine(" AND VG.VG_COD_VAG IN (" + vagoes + ") ");
            }

            if (!(string.IsNullOrEmpty(serie)))
            {
                sql.AppendLine(" AND SV.SV_COD_SER IN (" + serie + ") ");
            }

            if (dataInicial != "" && dataFinal != "")
            {
                sql.AppendFormat(" AND VV.VP_DATA_PROCESSADO BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial, dataFinal);
            }

            sqlWihtoutOrderBy.AppendLine(sql.ToString());
            sql.AppendLine("ORDER BY VV.VP_DATA_PROCESSADO ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlWihtoutOrderBy.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<VagaoHistoricoTaraDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<VagaoHistoricoTaraDto>();

                var result = new ResultadoPaginado<VagaoHistoricoTaraDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IEnumerable<VagaoHistoricoTaraDto> ExportarHistoricoTaras(string vagoes, string serie, string dataInicial, string dataFinal)
        {
            var sqlWihtoutOrderBy = new StringBuilder();
            var sql = new StringBuilder(@"
              SELECT SV.SV_COD_SER       AS Serie,
                   VV.VG_COD_VAG         AS CodVagao,
                   VV.VP_DATA_PROCESSADO AS Data,
                   VV.TARA_ANTERIOR      AS TaraAnterior,
                   VV.TARA_MEDIANA       AS TaraMediana,
                   VG.VG_NUM_TRA         AS TaraAtual
              FROM (SELECT MAX(VP.VP_ID) AS ID, VP.VG_COD_VAG AS COD_VAGAO
                      FROM VAGAO_PESAGEM_PLANILHA VP
                     WHERE VP.VP_DATA_PROCESSADO IS NOT NULL
                       AND VP.IND_PROC_COM_ERR = 'N'     
                     GROUP BY VP.VG_COD_VAG) TAB_AUX,
                   VAGAO_PESAGEM_PLANILHA VV,
                   VAGAO VG,
                   SERIE_VAGAO SV
              WHERE VV.VP_ID = TAB_AUX.ID
                  AND VV.VG_COD_VAG = VG.VG_COD_VAG
                  AND VG.SV_ID_SV = SV.SV_ID_SV ");

            if (!(string.IsNullOrEmpty(vagoes)))
            {
                sql.AppendLine(" AND VG.VG_COD_VAG IN (" + vagoes + ") ");
            }

            if (!(string.IsNullOrEmpty(serie)))
            {
                sql.AppendLine(" AND SV.SV_COD_SER IN (" + serie + ") ");
            }

            if (dataInicial != "" && dataFinal != "")
            {
                sql.AppendFormat(" AND VV.VP_DATA_PROCESSADO BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", dataInicial, dataFinal);
            }

            sqlWihtoutOrderBy.AppendLine(sql.ToString());
            sql.AppendLine("ORDER BY VV.VP_DATA_PROCESSADO ");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<VagaoHistoricoTaraDto>());
                return query.List<VagaoHistoricoTaraDto>();

            }
        }
    }
}