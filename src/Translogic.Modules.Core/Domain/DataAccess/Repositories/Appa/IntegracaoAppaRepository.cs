﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Appa
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Appa;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public class IntegracaoAppaRepository : NHRepository, IIntegracaoAppaRepository
    {
        public IntegracaoAppaDto ObterIntegracaoPorId(int id)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = String.Format(@"
                SELECT LEA.ID_WS_LOG_ENVIOS_APPA AS ""Id""
                     , LEA.DATA_CRIACAO          AS ""DataCriacao""
                     , LEA.DATA_ENVIO            AS ""DataEnvio""
                     , LEA.SUCESSO_ENVIO         AS ""SucessoEnvio""
                     , LEA.MENSAGEM_ERRO         AS ""MensagemErro""
                     , LEA.SUCESSO_RETORNO       AS ""SucessoRetorno""
                     , LEA.MENSAGEM_RETORNO      AS ""MensagemRetorno""
                     , LEA.XML_ENVIADO           AS ""XmlEnviado""
                  FROM WS_LOG_ENVIOS_APPA       LEA
                 WHERE LEA.ID_WS_LOG_ENVIOS_APPA = {0}", id);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(queryPrincipal);

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<IntegracaoAppaDto>());
                var item = query.UniqueResult<IntegracaoAppaDto>();
                return item;
            }
        }

        public ResultadoPaginado<IntegracaoAppaDto> ObterIntegracoesConsulta(DetalhesPaginacao detalhesPaginacao
                                                                            , DateTime dataInicial
                                                                            , DateTime dataFinal
                                                                            , string origem
                                                                            , string destino
                                                                            , string[] vagoes
                                                                            , int situacaoEnvioStatus
                                                                            , int situacaoRecebimentoStatus
                                                                            , int? lote)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                SELECT LEA.ID_WS_LOG_ENVIOS_APPA AS ""Id""
                     , LEA.ID_LOTE               AS ""Lote""
                     , LEA.DATA_CRIACAO          AS ""DataCriacao""
                     , LEA.DATA_ENVIO            AS ""DataEnvio""
                     , LEA.SUCESSO_ENVIO         AS ""SucessoEnvio""
                     , LEA.MENSAGEM_ERRO         AS ""MensagemErro""
                     , LEA.SUCESSO_RETORNO       AS ""SucessoRetorno""
                     , LEA.MENSAGEM_RETORNO      AS ""MensagemRetorno""
                     , FC.FX_COD_FLX             AS ""CodigoFluxoComercial""
                     , AO.AO_COD_AOP             AS ""AreaOperacionalEnvio""
                     , ORI.AO_COD_AOP            AS ""Origem""
                     , DST.AO_COD_AOP            AS ""Destino""
                     , CA.PLACA_VAGAO            AS ""PlacaVagao""
                     , CA.DATA_CHEGADA           AS ""DataPrevistaChegada""
                     , CASE WHEN LEA.XML_ENVIADO IS NOT NULL THEN 'S' ELSE 'N' END AS ""PossuiXml""
                  FROM WS_LOG_ENVIOS_APPA       LEA
                  JOIN WS_CONSOLIDADO_APPA       CA ON CA.ID_WS_CONSOLIDADO_APPA = LEA.ID_WS_CONSOLIDADO_APPA
                  JOIN VAGAO                     VG ON VG.VG_ID_VG               = CA.ID_VAGAO 
                  JOIN FLUXO_COMERCIAL           FC ON FC.FX_ID_FLX              = CA.ID_FLUXO_COMERCIAL
                  JOIN AREA_OPERACIONAL          AO ON AO.AO_ID_AO               = CA.ID_AREA_OPERACIONAL
                  JOIN AREA_OPERACIONAL         ORI ON ORI.AO_ID_AO              = FC.AO_ID_EST_OR
                  JOIN AREA_OPERACIONAL         DST ON DST.AO_ID_AO              = NVL(FC.AO_ID_TER_DS, FC.AO_ID_EST_DS)
                 WHERE LEA.DATA_CRIACAO BETWEEN TO_DATE('{dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                            AND TO_DATE('{dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                       {origem}
                       {destino}
                       {vagoes}
                       {statusEnvio}
                       {statusRecebimento}
                       {lote}
              ORDER BY LEA.DATA_CRIACAO DESC");

            // 1) Data Inicial
            queryPrincipal.Replace("{dataInicial}", dataInicial.ToString("dd/MM/yyyy"));

            // 2) Data Final
            queryPrincipal.Replace("{dataFinal}", dataFinal.ToString("dd/MM/yyyy"));

            // 3) Origem
            if (!String.IsNullOrEmpty(origem))
            {
                queryPrincipal.Replace("{origem}", String.Format("AND ORI.AO_COD_AOP = '{0}'", origem.ToUpper()));
            }
            else
            {
                queryPrincipal.Replace("{origem}", String.Empty);
            }
            // 4) Destino
            if (!String.IsNullOrEmpty(destino))
            {
                queryPrincipal.Replace("{destino}", String.Format("AND DST.AO_COD_AOP = '{0}'", destino.ToUpper()));
            }
            else
            {
                queryPrincipal.Replace("{destino}", String.Empty);
            }

            // 5) Vagões
            if (vagoes.Length > 0)
            {
                string vagoesList = String.Empty;
                foreach (var v in vagoes)
                {
                    if (String.IsNullOrEmpty(vagoesList))
                        vagoesList = "'" + v + "'";
                    else
                        vagoesList = vagoesList + ", '" + v + "'";
                }

                if (!String.IsNullOrEmpty(vagoesList))
                    queryPrincipal.Replace("{vagoes}", "AND VG.VG_COD_VAG IN ( " + vagoesList + ")");
                else
                    queryPrincipal.Replace("{vagoes}", "");
            }
            else
            {
                queryPrincipal.Replace("{vagoes}", "");
            }

            // 6) Status da Situação de Envio
            switch(situacaoEnvioStatus)
            {
                case 0: // Registros com erros de envio
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NULL AND LEA.SUCESSO_ENVIO = 'N'");
                    break;
                case 1: // Registros pendentes de envio
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NULL AND LEA.SUCESSO_ENVIO IS NULL");
                    break;
                case 2: // Enviadas com sucesso
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_ENVIO = 'S'");
                    break;
                default:
                    queryPrincipal.Replace("{statusEnvio}", String.Empty);
                    break;
            }

            // 7) Status da Situação de Recebimento
            switch (situacaoRecebimentoStatus)
            {
                case 0: // Registros com erros de recebimento
                    queryPrincipal.Replace("{statusRecebimento}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_RETORNO = 'N'");
                    break;
                case 2: // Recebidas com sucesso
                    queryPrincipal.Replace("{statusRecebimento}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_RETORNO = 'S'");
                    break;
                default:
                    queryPrincipal.Replace("{statusRecebimento}", String.Empty);
                    break;
            }

            // 8) Lote
            queryPrincipal.Replace("{lote}", lote.HasValue ? String.Format("AND LEA.ID_LOTE = {0}", lote.Value.ToString()) : String.Empty);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(queryPrincipal.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", queryPrincipal.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<IntegracaoAppaDto>());
                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<IntegracaoAppaDto>();

                var result = new ResultadoPaginado<IntegracaoAppaDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IList<IntegracaoAppaDto> ObterIntegracoesExcel(DateTime dataInicial
                                                                    , DateTime dataFinal
                                                                    , string origem
                                                                    , string destino
                                                                    , string[] vagoes
                                                                    , int situacaoEnvioStatus
                                                                    , int situacaoRecebimentoStatus
                                                                    , int? lote)
        {
            var parameters = new List<Action<IQuery>>();

            var queryPrincipal = new StringBuilder(@"
                SELECT LEA.ID_WS_LOG_ENVIOS_APPA AS ""Id""
                     , LEA.ID_LOTE               AS ""Lote""
                     , LEA.DATA_CRIACAO          AS ""DataCriacao""
                     , LEA.DATA_ENVIO            AS ""DataEnvio""
                     , LEA.SUCESSO_ENVIO         AS ""SucessoEnvio""
                     , LEA.MENSAGEM_ERRO         AS ""MensagemErro""
                     , LEA.SUCESSO_RETORNO       AS ""SucessoRetorno""
                     , LEA.MENSAGEM_RETORNO      AS ""MensagemRetorno""
                     , FC.FX_COD_FLX             AS ""CodigoFluxoComercial""
                     , AO.AO_COD_AOP             AS ""AreaOperacionalEnvio""
                     , ORI.AO_COD_AOP            AS ""Origem""
                     , DST.AO_COD_AOP            AS ""Destino""
                     , CA.PLACA_VAGAO            AS ""PlacaVagao""
                     , CA.DATA_CHEGADA           AS ""DataPrevistaChegada""
                     , CASE WHEN LEA.XML_ENVIADO IS NOT NULL THEN 'S' ELSE 'N' END AS ""PossuiXml""
                  FROM WS_LOG_ENVIOS_APPA       LEA
                  JOIN WS_CONSOLIDADO_APPA       CA ON CA.ID_WS_CONSOLIDADO_APPA = LEA.ID_WS_CONSOLIDADO_APPA
                  JOIN VAGAO                     VG ON VG.VG_ID_VG               = CA.ID_VAGAO 
                  JOIN FLUXO_COMERCIAL           FC ON FC.FX_ID_FLX              = CA.ID_FLUXO_COMERCIAL
                  JOIN AREA_OPERACIONAL          AO ON AO.AO_ID_AO               = CA.ID_AREA_OPERACIONAL
                  JOIN AREA_OPERACIONAL         ORI ON ORI.AO_ID_AO              = FC.AO_ID_EST_OR
                  JOIN AREA_OPERACIONAL         DST ON DST.AO_ID_AO              = NVL(FC.AO_ID_TER_DS, FC.AO_ID_EST_DS)
                 WHERE LEA.DATA_CRIACAO BETWEEN TO_DATE('{dataInicial} 00:00:00', 'DD/MM/YYYY HH24:MI:SS') 
                                            AND TO_DATE('{dataFinal} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                       {origem}
                       {destino}
                       {vagoes}
                       {statusEnvio}
                       {statusRecebimento}
                       {lote}
              ORDER BY LEA.DATA_CRIACAO DESC");

            // 1) Data Inicial
            queryPrincipal.Replace("{dataInicial}", dataInicial.ToString("dd/MM/yyyy"));

            // 2) Data Final
            queryPrincipal.Replace("{dataFinal}", dataFinal.ToString("dd/MM/yyyy"));

            // 3) Origem
            if (!String.IsNullOrEmpty(origem))
            {
                queryPrincipal.Replace("{origem}", String.Format("AND ORI.AO_COD_AOP = '{0}'", origem.ToUpper()));
            }
            else
            {
                queryPrincipal.Replace("{origem}", String.Empty);
            }
            // 4) Destino
            if (!String.IsNullOrEmpty(destino))
            {
                queryPrincipal.Replace("{destino}", String.Format("AND DST.AO_COD_AOP = '{0}'", destino.ToUpper()));
            }
            else
            {
                queryPrincipal.Replace("{destino}", String.Empty);
            }

            // 5) Vagões
            if (vagoes.Length > 0)
            {
                string vagoesList = String.Empty;
                foreach (var v in vagoes)
                {
                    if (String.IsNullOrEmpty(vagoesList))
                        vagoesList = "'" + v + "'";
                    else
                        vagoesList = vagoesList + ", '" + v + "'";
                }

                if (!String.IsNullOrEmpty(vagoesList))
                    queryPrincipal.Replace("{vagoes}", "AND VG.VG_COD_VAG IN ( " + vagoesList + ")");
                else
                    queryPrincipal.Replace("{vagoes}", "");
            }
            else
            {
                queryPrincipal.Replace("{vagoes}", "");
            }

            // 6) Status da Situação de Envio
            switch (situacaoEnvioStatus)
            {
                case 0: // Registros com erros de envio
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NULL AND LEA.SUCESSO_ENVIO = 'N'");
                    break;
                case 1: // Registros pendentes de envio
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NULL AND LEA.SUCESSO_ENVIO IS NULL");
                    break;
                case 2: // Enviadas com sucesso
                    queryPrincipal.Replace("{statusEnvio}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_ENVIO = 'S'");
                    break;
                default:
                    queryPrincipal.Replace("{statusEnvio}", String.Empty);
                    break;
            }

            // 7) Status da Situação de Recebimento
            switch (situacaoRecebimentoStatus)
            {
                case 0: // Registros com erros de recebimento
                    queryPrincipal.Replace("{statusRecebimento}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_RETORNO = 'N'");
                    break;
                case 2: // Recebidas com sucesso
                    queryPrincipal.Replace("{statusRecebimento}", "AND LEA.DATA_ENVIO IS NOT NULL AND LEA.SUCESSO_RETORNO = 'S'");
                    break;
                default:
                    queryPrincipal.Replace("{statusRecebimento}", String.Empty);
                    break;
            }

            // 8) Lote
            queryPrincipal.Replace("{lote}", lote.HasValue ? String.Format("AND LEA.ID_LOTE = {0}", lote.Value.ToString()) : String.Empty);

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(queryPrincipal.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", queryPrincipal.ToString()));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<IntegracaoAppaDto>());

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<IntegracaoAppaDto>();

                return itens;
            }
        }



        public void ObterXmlEnvio(int id)
        {
            throw new NotImplementedException();
        }

        public void ReprocessarRegistro(int id)
        {
            using (var session = OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    string sqlQueryUpdate = String.Format(@"
                        UPDATE WS_LOG_ENVIOS_APPA
                           SET DATA_ENVIO       = NULL
                             , SUCESSO_ENVIO    = NULL
                             , MENSAGEM_ERRO    = NULL
                             , SUCESSO_RETORNO  = NULL
                             , MENSAGEM_RETORNO = NULL
                         WHERE ID_WS_LOG_ENVIOS_APPA = {0}", id);

                    var query = session.CreateSQLQuery(sqlQueryUpdate);

                    query.ExecuteUpdate();
                    session.Flush();
                    trans.Commit();
                }
            }
        }
    }
}