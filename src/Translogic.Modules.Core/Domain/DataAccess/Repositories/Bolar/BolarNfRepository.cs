﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Bolar.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Transform;
	using Translogic.Modules.Core.Domain.Model.Bolar;

	/// <summary>
	/// Repositorio para <see cref="BolarNf"/>
	/// </summary>
	public class BolarNfRepository : NHRepository<BolarNf, int>, IBolarNfRepository
	{
		/// <summary>
		/// Retorna bolar nf por número da nota fiscal
		/// </summary>
		/// <param name="numeroNotaFiscal">Número nota fiscal</param>
		/// <returns>Bolar Bo Nf</returns>
		public BolarNf ObterPorNumeroNota(string numeroNotaFiscal)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("NumeroNotaFiscal", numeroNotaFiscal));
			return ObterPrimeiro(criteria);
		}

		/// <summary>
		/// Obter Todas as Nfe nao processadas na Vw_Nfe
		/// </summary>
		/// <param name="data">Data limite para pesquisa</param>
		/// <returns>Retorna lista de chave</returns>
		public IList<string> ObterNfeNaoProcessadas(DateTime data)
		{
			using (ISession session = OpenSession())
			{
				string hql =
					@"SELECT BoNfe.ChaveNfe
						FROM BolarNf BoNfe
							INNER JOIN BoNfe.VagaoBolar vag
							INNER JOIN vag.BolarBo bo
						WHERE BoNfe.ChaveNfe IS NOT NULL 
							AND bo.TimeStamp > :data
							AND NOT EXISTS(SELECT 1 FROM NfeReadonly nfe  WHERE nfe.ChaveNfe = BoNfe.ChaveNfe) 
							AND NOT EXISTS(SELECT 1 FROM NfePooling nfePool  WHERE nfePool.ChaveNfe = BoNfe.ChaveNfe)
							AND NOT EXISTS(SELECT 1 
									FROM StatusNfe sn 
									INNER JOIN sn.StatusRetornoNfe srn
									WHERE sn.ChaveNfe = BoNfe.ChaveNfe 
									AND srn.IndPermiteReenvioBo = :indPermReenvio)
						GROUP BY BoNfe.ChaveNfe";

				IQuery query = session.CreateQuery(hql);
				query.SetDateTime("data", data);
				query.SetParameter("indPermReenvio", false, new BooleanCharType());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<string>();
			}
		}

		/// <summary>
		/// Obter Todas as Nfe do SISPAT nao processadas na Vw_Nfe
		/// </summary>
		/// <param name="data">Data limite para pesquisa</param>
		/// <returns>Retorna lista de chave</returns>
		public IList<string> ObterNfeNaoProcessadasSispat(DateTime data)
		{	
			using (ISession session = OpenSession())
			{
				string hql =
						@"SELECT XX.NFE_CHAVE 
							FROM SISPAT.PTR_NOTA_FISCAL@LINK_TLRODO XX
						   WHERE XX.DATA_HORA_EVENTO_INICIAL > :data AND XX.FLAG_NFE = 'F'
							 AND NOT EXISTS (SELECT 1 FROM VW_NFE V WHERE V.NFE_CHAVE = XX.NFE_CHAVE)
							 AND NOT EXISTS (SELECT 1 FROM Nfe_Pooling nfePool  WHERE nfePool.NFE_CHAVE = XX.NFE_CHAVE)
							 AND NOT EXISTS(SELECT 1 
									FROM STATUS_NFE sn 
									INNER JOIN STATUS_RETORNO_NFE srn ON sn.id_status_retorno_nfe = srn.id_status_retorno_nfe
									WHERE sn.nfe_chave= XX.NFE_CHAVE 
									AND srn.IND_PERM_REENVIO_BO = :indPermReenvio)
						GROUP BY XX.NFE_CHAVE ";

				IQuery query = session.CreateSQLQuery(hql);

				query.SetDateTime("data", data);
				query.SetParameter("indPermReenvio", false, new BooleanCharType());
				query.SetResultTransformer(Transformers.DistinctRootEntity);

				return query.List<string>();
			}
		}
	}
}
