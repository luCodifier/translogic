﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;

    public class BolarBoBolarCteRepository : NHRepository<BolarBoBolarCte, int>, IBolarBoBolarCteRepository
    {
        public IList<BolarBoBolarCte> ObterPorIdCte(int id)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("BolarCte.Id", id));
            return ObterTodos(criteria);
        }

        public IList<BolarBoBolarCte> ObterPorIdBolar(int id)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("BolarBo.Id", id));
            return ObterTodos(criteria);
        }
    }
}