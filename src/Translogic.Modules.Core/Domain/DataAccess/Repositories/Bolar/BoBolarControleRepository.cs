﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using NHibernate.Criterion;
using Translogic.Modules.Core.Domain.Model.Bolar;
using Translogic.Modules.Core.Domain.Model.Bolar.Repositories;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    public class BoBolarControleRepository : NHRepository<BoBolarControle, int>, IBoBolarControleRepository
    {
        public IList<BoBolarControle> ObterTodos(int[] boBolarIds)
        {
            var criteria = CriarCriteria();
            criteria.Add(Restrictions.In("BoIdBo", boBolarIds));
            return ObterTodos(criteria);
        }
    }
}