﻿using NHibernate;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Bolar.Repositories;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Repositorio para <see cref="BolarBo"/>
    /// </summary>
    public class BolarBoRepository : NHRepository<BolarBo, int>, IBolarBoRepository
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        public BolarBo ObterPorCodigo(string codigo)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Like("Codigo", "%" + codigo));
            return ObterPrimeiro(criteria);
        }

        public IList<ProcessamentoFaturamentoAutomaticoDto> Exportar(DateTime dtInicial, DateTime dtFinal, string vagao, string processado, string cliente, string local, string fluxo)
        {
            var sql = new StringBuilder();

            if (string.IsNullOrEmpty(processado) || processado == "3" || processado == "4")
            {
                ObterSqlPendentes(sql, vagao, cliente, local, fluxo, processado);
            }

            if (string.IsNullOrEmpty(processado))
            {
                sql.Append(" UNION ");
            }

            if (string.IsNullOrEmpty(processado) || processado == "2")
            {
                ObterSqlProcessados(sql, vagao, cliente, local, fluxo);
            }

            if (string.IsNullOrEmpty(processado))
            {
                sql.Append(" UNION ");
            }

            if (string.IsNullOrEmpty(processado) || processado == "1")
            {
                ObterSqlNaoProcessados(sql, vagao, cliente, local, fluxo);
            }

            sql.Append(@" ORDER BY 10");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                AtribuirParametro(dtInicial, dtFinal, vagao, cliente, local, fluxo, query);

                query.SetResultTransformer(Transformers.AliasToBean<ProcessamentoFaturamentoAutomaticoDto>());

                return query.List<ProcessamentoFaturamentoAutomaticoDto>();
            }
        }

        public ResultadoPaginado<ProcessamentoFaturamentoAutomaticoDto> Pesquisar(DetalhesPaginacao detalhesPaginacao, DateTime dtInicial, DateTime dtFinal, string vagao, string processado, string cliente, string local, string fluxo)
        {
            var sql = new StringBuilder();

            if (string.IsNullOrEmpty(processado) || processado == "3" || processado == "4")
            {
                ObterSqlPendentes(sql, vagao, cliente, local, fluxo, processado);
            }

            if (string.IsNullOrEmpty(processado))
            {
                sql.Append(" UNION ");
            }

            if (string.IsNullOrEmpty(processado) || processado == "2")
            {
                ObterSqlProcessados(sql, vagao, cliente, local, fluxo);
            }

            if (string.IsNullOrEmpty(processado))
            {
                sql.Append(" UNION ");
            }

            if (string.IsNullOrEmpty(processado) || processado == "1")
            {
                ObterSqlNaoProcessados(sql, vagao, cliente, local, fluxo);
            }

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            string colOrdenacaoIndex;
            var ordenacaoDirecao = String.Empty;

            var ordenacaoPadrao = colunasOrdenacao.Count == 0 && direcoesOrdenacao.Count == 0;
            if (!ordenacaoPadrao)
            {
                colOrdenacaoIndex = colunasOrdenacao[0];
                ordenacaoDirecao = direcoesOrdenacao[0];
            }
            else
            {
                colOrdenacaoIndex = "Data";
            }

            switch (colOrdenacaoIndex)
            {
                case "Serie": colOrdenacaoIndex = "2"; break;
                case "Vagao": colOrdenacaoIndex = "3"; break;
                case "Fluxo": colOrdenacaoIndex = "4"; break;
                case "Md": colOrdenacaoIndex = "5"; break;
                case "StatusProcessamento": colOrdenacaoIndex = "6"; break;
                case "NumeroTentativas": colOrdenacaoIndex = "7"; break;
                case "ProximaExecucao": colOrdenacaoIndex = "8"; break;
                case "Observacao": colOrdenacaoIndex = "9"; break;
                case "Data": colOrdenacaoIndex = "10"; break;
                case "Cliente": colOrdenacaoIndex = "11"; break;
                case "Local": colOrdenacaoIndex = "12"; break;
                case "LocalAtual": colOrdenacaoIndex = "13"; break;
                case "Situacao": colOrdenacaoIndex = "14"; break;
                case "Lotacao": colOrdenacaoIndex = "15"; break;
                case "Localizacao": colOrdenacaoIndex = "16"; break;
                case "CondicaoUso": colOrdenacaoIndex = "17"; break;
                case "Volume": colOrdenacaoIndex = "19"; break;
                case "PesoDespacho": colOrdenacaoIndex = "20"; break;
                case "PesoTotalVagao": colOrdenacaoIndex = "21"; break;
                case "PermiteManual": colOrdenacaoIndex = "22"; break;
            }

            var sqlCount = sql.ToString();

            sql.AppendFormat(@" ORDER BY {0} {1}", colOrdenacaoIndex, ordenacaoDirecao);

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlCount));

                AtribuirParametro(dtInicial, dtFinal, vagao, cliente, local, fluxo, query);
                AtribuirParametro(dtInicial, dtFinal, vagao, cliente, local, fluxo, queryCount);

                query.SetResultTransformer(Transformers.AliasToBean<ProcessamentoFaturamentoAutomaticoDto>());

                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var items = query.List<ProcessamentoFaturamentoAutomaticoDto>();

                var total = queryCount.UniqueResult<decimal>();

                var result = new ResultadoPaginado<ProcessamentoFaturamentoAutomaticoDto>
                {
                    Items = items,
                    Total = (long)total
                };

                return result;
            }
        }

        private static void AtribuirParametro(DateTime dtInicial, DateTime dtFinal, string vagao, string cliente, string local,
                                              string fluxo, ISQLQuery query)
        {
            query.SetParameter("dtInicial", dtInicial);
            query.SetParameter("dtFinal", dtFinal.AddDays(1).AddSeconds(-1));

            if (!string.IsNullOrEmpty(vagao))
            {
                query.SetParameter("vagao", vagao);
            }

            if (!string.IsNullOrEmpty(cliente))
            {
                query.SetParameter("cliente", cliente);
            }

            if (!string.IsNullOrEmpty(local))
            {
                query.SetParameter("local", local);
            }

            if (!string.IsNullOrEmpty(fluxo))
            {
                query.SetParameter("fluxo", fluxo);
            }
        }

        private static void ObterSqlPendentes(StringBuilder sql, string vagao, string cliente, string local, string fluxo, string status)
        {
            var consulta = new StringBuilder();
            consulta.Append(@"
                SELECT DISTINCT bo.id_bo                                    AS Id,
                                sv.sv_cod_ser 								AS Serie,
                                bv.vg_cod_vag 								AS Vagao,
                                bo.cod_flx 									AS Fluxo,
                                bv.md 										AS Md,
                                CASE WHEN BO.IDT_SIT    = 'N'
                                      AND EVN.SI_IDT_STC = 18
                                      AND AO.AO_COD_AOP = ( SELECT AOM.AO_COD_AOP
					                                          FROM VAGAO_PATIO_VIG VPV
					                                          JOIN VAGAO VG ON VG.VG_ID_VG = VPV.VG_ID_VG
					                                          JOIN AREA_OPERACIONAL AOF ON AOF.AO_ID_AO = VPV.AO_ID_AO
					                                          JOIN AREA_OPERACIONAL AOM ON AOF.AO_ID_AO_INF = AOM.AO_ID_AO
					                                         WHERE VG.VG_COD_VAG = BO.LST_VAGOES
                                                               AND ROWNUM = 1 )
                                    THEN 4
                                    ELSE 3
                                     END AS StatusProcessamento, 
				                0  											AS NumeroTentativas,
                                NULL                                        AS ProximaExecucao,
				                NULL     									AS Observacao,
				                bo.bo_timestamp 							AS Data,
				                bbic.cliente_dsc 							AS Cliente,
                                CASE WHEN BO.IDT_SIT = 'Q'
                                     THEN 'N'
                                     ELSE 'S'
                                      END                                   AS Aprovado1380,
				                ao.ao_cod_aop 								AS Local,
				                (SELECT AOM.AO_COD_AOP
					                FROM VAGAO_PATIO_VIG VPV,
					                VAGAO VG,
					                AREA_OPERACIONAL AOF,
					                AREA_OPERACIONAL AOM
					                WHERE VPV.VG_ID_VG=VG.VG_ID_VG
					                AND VG.VG_COD_VAG=BO.LST_VAGOES
					                AND VPV.AO_ID_AO=AOF.AO_ID_AO
					                AND AOF.AO_ID_AO_INF = AOM.AO_ID_AO)	AS LocalAtual,
				                evn.si_dsc_pt 								AS Situacao,
				                evn.sh_dsc_pt 								AS Lotacao,
				                evn.lo_dsc_pt 								AS Localizacao,
				                evn.cd_dsc_pt 								AS CondicaoUso,
				                bv.vl_real 									AS Volume,
				                bv.ps_real 									AS PesoDespacho,
				                bv.ps_total 								AS PesoTotalVagao,
                                NULL                                        AS PermiteManual
                      FROM  bo_bolar_bo bo,
                            bo_bolar_vagao bv,
                            bo_bolar_imp_cfg bbic,
                            fluxo_comercial fc,
                            area_operacional ao,
                            estado_vagao_nova evn,
                            vagao vg,
                            serie_vagao sv
                        WHERE bo.id_bo = bv.id_bo_id
                          AND vg.vg_cod_vag=bo.lst_vagoes
                          AND sv.sv_id_sv=vg.sv_id_sv
                          AND EXISTS
                            (SELECT 1
                             FROM bo_bolar_conf_carga_aut car
                             WHERE car.ao_cod_aop=bo.ao_cod_aop
                               AND CAR.CA_LIB_MANUAL = 'N'
                               AND ROWNUM = 1)
                          AND bbic.cliente = bo.ao_cod_aop
                          AND substr(fc.fx_cod_flx, 3) = to_char(bo.cod_flx)
                          AND ao.ao_id_ao = fc.ao_id_est_or
                          AND evn.vg_cod_vag = bv.vg_cod_vag
                          AND NOT EXISTS (SELECT 1 FROM bo_bolar_controle ctr   WHERE ctr.bo_id_bo=bo.id_bo AND ROWNUM = 1)
                          AND     EXISTS (SELECT 1 FROM bo_bolar_bo bo1         WHERE bo1.id_bo = bo.id_bo AND bo1.idt_sit IN ('N', 'Q') AND ROWNUM = 1)
                          AND bo_timestamp BETWEEN :dtInicial AND :dtFinal");

            AtribuirParametros(consulta, vagao, cliente, local, fluxo);

            sql.AppendFormat(@"SELECT DISTINCT TMP.Id                         AS ""Id""
                                             , TMP.Serie                      AS ""Serie""
                                             , TMP.Vagao                      AS ""Vagao""
                                             , TMP.Fluxo                      AS ""Fluxo""
                                             , TMP.Md                         AS ""Md""
                                             , TMP.StatusProcessamento        AS ""StatusProcessamento""
				                             , TMP.NumeroTentativas           AS ""NumeroTentativas""
                                             , TMP.ProximaExecucao            AS ""ProximaExecucao""
				                             , TMP.Observacao                 AS ""Observacao""
				                             , TMP.Data                       AS ""Data""
				                             , TMP.Cliente                    AS ""Cliente""
                                             , TMP.Aprovado1380               AS ""Aprovado1380""
				                             , TMP.Local                      AS ""Local""
				                             , TMP.LocalAtual                 AS ""LocalAtual""
				                             , TMP.Situacao                   AS ""Situacao""
				                             , TMP.Lotacao                    AS ""Lotacao""
				                             , TMP.Localizacao                AS ""Localizacao""
				                             , TMP.CondicaoUso                AS ""CondicaoUso""
				                             , TMP.Volume                     AS ""Volume""
				                             , TMP.PesoDespacho               AS ""PesoDespacho""
				                             , TMP.PesoTotalVagao             AS ""PesoTotalVagao""
                                             , TMP.PermiteManual              AS ""PermiteManual""
                                          FROM (
                                                 {0}
                                               ) TMP
                                          WHERE 1 = 1
                                            {1}
                ", consulta, string.IsNullOrEmpty(status) ? string.Empty : string.Format("AND TMP.StatusProcessamento = {0}", status));
        }

        private static void ObterSqlNaoProcessados(StringBuilder sql, string vagao, string cliente, string local, string fluxo)
        {
            sql.Append(@"SELECT DISTINCT
                               bo.id_bo 							AS ""Id"",
                               sv.sv_cod_ser 						AS ""Serie"",
                               bv.vg_cod_vag 						AS ""Vagao"",
                               bo.cod_flx 							AS ""Fluxo"",
                               bv.md 								AS ""Md"",
                               1 									AS ""StatusProcessamento"",
                               bbc.num_tentativas 					AS ""NumeroTentativas"",
                               CASE bbc.num_tentativas WHEN 4 
	                            THEN NULL 
	                            ELSE bbc.bc_timestamp + INTERVAL '5' MINUTE 
                               END									AS ""ProximaExecucao"", 
                               (SELECT d.dsc_erro 
		                            FROM  bo_bolar_controle_det d 
		                            WHERE d.bd_id_bd = 
		                            (SELECT MAX(bbcd.bd_id_bd) 
                                     FROM	bo_bolar_controle_det bbcd 
                                     JOIN   bo_bolar_controle c 
                                     ON c.bc_id_bc = bbcd.bc_id_bc 
                                     WHERE  c.bo_id_bo = bo.id_bo))	AS ""Observacao"",
                               bo.bo_timestamp 						AS ""Data"",
                               bbic.cliente_dsc 					AS ""Cliente"",
                               CASE WHEN BO.IDT_SIT = 'Q'
                                             THEN 'N'
                                             ELSE 'S'
                                              END                   AS ""Aprovado1380"",
                               ao.ao_cod_aop 						AS ""Local"",
                               (SELECT AOM.AO_COD_AOP
					           FROM VAGAO_PATIO_VIG VPV,
					                VAGAO VG,
					                AREA_OPERACIONAL AOF,
					                AREA_OPERACIONAL AOM
					           WHERE VPV.VG_ID_VG=VG.VG_ID_VG
					           AND VG.VG_COD_VAG=BO.LST_VAGOES
					           AND VPV.AO_ID_AO=AOF.AO_ID_AO
					           AND AOF.AO_ID_AO_INF = AOM.AO_ID_AO)	AS ""LocalAtual"",
                               evn.si_dsc_pt 						AS ""Situacao"",
                               evn.sh_dsc_pt 						AS ""Lotacao"",
                               evn.lo_dsc_pt 						AS ""Localizacao"",
                               evn.cd_dsc_pt 						AS ""CondicaoUso"",
                               bv.vl_real 							AS ""Volume"",
                               bv.ps_real 							AS ""PesoDespacho"",
                               bv.ps_total 							AS ""PesoTotalVagao"",
                               bbc.bc_ind_lib_tela 					AS ""PermiteManual"" 
                            FROM
                               bo_bolar_bo bo,
                               bo_bolar_vagao bv,
                               bo_bolar_controle bbc,
                               bo_bolar_imp_cfg bbic,
                               fluxo_comercial fc,
                               area_operacional ao,
                               estado_vagao_nova evn,
                               vagao vg,
		                       serie_vagao sv 
                            WHERE
                               bo.id_bo = bv.id_bo_id 
                               AND vg.vg_cod_vag=bo.lst_vagoes
                               AND sv.sv_id_sv=vg.sv_id_sv
                               AND bbc.bo_id_bo = bo.id_bo 
                               AND bbic.cliente = bo.ao_cod_aop 
                               AND substr(fc.fx_cod_flx, 3) = to_char(bo.cod_flx) 
                               AND ao.ao_id_ao = fc.ao_id_est_or 
                               AND evn.vg_cod_vag = bv.vg_cod_vag 
                               AND EXISTS 
                               (SELECT 1 
                                FROM bo_bolar_bo bo1 
                                WHERE bo1.id_bo = bo.id_bo 
                                AND bo1.idt_sit IN ('N', 'Q')
                                AND ROWNUM = 1)
                               AND bo_timestamp BETWEEN :dtInicial AND :dtFinal	");

            AtribuirParametros(sql, vagao, cliente, local, fluxo);
        }

        private static void ObterSqlProcessados(StringBuilder sql, string vagao, string cliente, string local, string fluxo)
        {
            sql.Append(@"SELECT
                           bo.ID_BO                 AS ""Id"", 
                           sv.sv_cod_ser 		    AS ""Serie"",
                           bo.lst_vagoes 			AS ""Vagao"",
                           bo.cod_flx 				AS ""Fluxo"",
                           bv.md 					AS ""Md"",
                           2 						AS ""StatusProcessamento"",
                           NULL						AS ""NumeroTentativas"",
                           NULL						AS ""ProximaExecucao"",
                           NULL						AS ""Observacao"",
                           vp.vb_dat_car 			AS ""Data"",
                           cfg.cliente_dsc 			AS ""Cliente"",
                           CASE WHEN BO.IDT_SIT = 'Q'
                                THEN 'N'
                                ELSE 'S'
                                 END                AS ""Aprovado1380"",
                           ao.ao_cod_aop 			AS ""Local"",
                           (SELECT AOM.AO_COD_AOP
					       FROM VAGAO_PATIO_VIG VPV,
					            VAGAO VG,
					            AREA_OPERACIONAL AOF,
					            AREA_OPERACIONAL AOM
					       WHERE VPV.VG_ID_VG=VG.VG_ID_VG
					       AND VG.VG_COD_VAG=BO.LST_VAGOES
					       AND VPV.AO_ID_AO=AOF.AO_ID_AO
					       AND AOF.AO_ID_AO_INF = AOM.AO_ID_AO)	AS ""LocalAtual"",
                           evn.si_dsc_pt 			AS ""Situacao"",
                           evn.sh_dsc_pt 			AS ""Lotacao"",
                           evn.lo_dsc_pt 			AS ""Localizacao"",
                           evn.cd_dsc_pt 			AS ""CondicaoUso"",
                           bv.vl_real	            AS ""Volume"",
                           bv.ps_real               AS ""PesoDespacho"", 
                           bv.ps_total 				AS ""PesoTotalVagao"",
                           NULL                     AS ""PermiteManual""
                        FROM
                           vagao_pedido vp, 
                           detalhe_carregamento dc, 
                           item_despacho ids, 
                           bo_bolar_vagao bv, 
                           bo_bolar_bo bo,
                           bo_bolar_imp_cfg bbic,
                           estado_vagao_nova evn, 
                           bo_bolar_imp_cfg cfg, 
                           fluxo_comercial fc,
                           area_operacional ao,
                           vagao vg,
		                   serie_vagao sv    
                        WHERE   
                           vp.vb_mat_usa = 'INT_TL' 
                           AND dc.cg_id_car = vp.cg_id_car 
                           AND ids.dc_id_crg = dc.dc_id_crg 
                           AND bv.dp_id_dp = ids.dp_id_dp 
                           AND bv.id_bo_id = bo.id_bo
                           AND bbic.cliente = bo.ao_cod_aop 
                           AND vg.vg_cod_vag=bo.lst_vagoes
                           AND sv.sv_id_sv=vg.sv_id_sv  
                           AND evn.vg_id_vg = vp.vg_id_vg 
                           AND cfg.cliente = bo.ao_cod_aop 
                           AND substr(fc.fx_cod_flx, 3) = to_char(bo.cod_flx)
                           AND ao.ao_id_ao = fc.ao_id_est_or 
                           AND vp.vb_dat_car BETWEEN :dtInicial AND :dtFinal");

            AtribuirParametros(sql, vagao, cliente, local, fluxo);
        }

        private static void AtribuirParametros(StringBuilder sql, string vagao, string cliente, string local, string fluxo)
        {
            if (!string.IsNullOrEmpty(vagao))
            {
                sql.AppendLine(" AND bv.vg_cod_vag =:vagao");
            }

            if (!string.IsNullOrEmpty(cliente))
            {
                sql.AppendLine(" AND bbic.cliente =:cliente");
            }

            if (!string.IsNullOrEmpty(local))
            {
                sql.AppendLine(" AND ao.ao_id_ao =:local");
            }

            if (!string.IsNullOrEmpty(fluxo))
            {
                sql.AppendLine(" AND bo.cod_flx =:fluxo");
            }
        }

        public IList<EmpresaBloqueadaFaturamentoManualDto> ObterEmpresasBloqueadasFaturamentoManual()
        {
            var sql = @"SELECT UPPER(cliente_dsc) AS ""Descricao"" 
                        FROM BO_BOLAR_CONF_CARGA_AUT BBCCA 
                        JOIN BO_BOLAR_IMP_CFG BBIC ON BBCCA.AO_COD_AOP = BBIC.CLIENTE
                        where bbcca.ca_lib_manual = 'N'
                        group by cliente_dsc";

            IList<EmpresaBloqueadaFaturamentoManualDto> list;

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.AliasToBean<EmpresaBloqueadaFaturamentoManualDto>());

                list = query.List<EmpresaBloqueadaFaturamentoManualDto>();
            }

            return list;
        }

        public bool EmpresaBloqueadaFaturamentoManual(string fluxo)
        {
            var sql = string.Format(@"SELECT COUNT(*)
                        FROM   BO_BOLAR_BO BO join bo_bolar_conf_carga_aut a
                               on bo.AO_COD_AOP = a.AO_COD_AOP
                        WHERE  BO.BO_TIMESTAMP>=SYSDATE-30 and a.ca_lib_manual = 'N'
                        AND    BO.COD_FLX=TO_NUMBER('{0}')", fluxo);

            decimal valor = 0;

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                //query.SetResultTransformer(Transformers.AliasToBean<EmpresaBloqueadaFaturamentoManualDto>());

                valor = query.UniqueResult<decimal>();
            }

            return (valor > 0);
        }

        public ResultadoPaginado<RecebimentoArquivoEdiDto> PesquisarRecebimentoArquivoEdi(DetalhesPaginacao detalhesPaginacao, DateTime dtInicial, DateTime dtFinal, string fluxoComercial, string origem, string destino, string situacao, string lstVagoes, string cliente363)
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT  DISTINCT 
                         bo.BO_TIMESTAMP          AS ""Data"",                                                     
                         VAG.ID_VAGAO             AS ""VagaoId"",
                         SV.SV_COD_SER            AS ""Serie"", 
                         VAG.VG_COD_VAG           AS ""Vagao"",
                         FC.FX_COD_FLX            AS ""Fluxo"",
                         BO.IDT_SIT               AS ""Situacao"",
                         VAG.PS_REAL              AS ""PesoReal"",
                         VAG.PS_TOTAL             AS ""PesoTotal"", 
                         VAG.VL_REAL              AS ""VolumeTotal"",
                         VAG.MD                   AS ""MultiploDespacho"", 
                         bo.lst_nfs               AS ""NotasFicais"",
                         NVL(ORIF1.AO_COD_AOP, ORIF.AO_COD_AOP) AS ""Origem"",
                         NVL(DEST1.AO_COD_AOP, DEST.AO_COD_AOP) AS ""Destino"",                                                 
                         CFG.CLIENTE_DSC          AS ""Cliente363"", 
                         MRC.MC_DRS_PT            AS ""Mercadoria"",
                         Recebedor.EP_DSC_RSM     AS ""Recebedor"",
                         EXPEDIDOR.EP_DSC_RSM     AS ""Expedidor"",
                         bo.OBSERVACAO_RECUSADO   as ""ObservacaoRecusado"",
                         bo.BO_TIMESTAMP          as ""DataHoraCadastro"",
                         bo.SERIE_DPCH_INTERC     as ""SerieDespacho"",
                         bo.DT_CARR_DPCH_INTERC   as ""DataDespacho""                                                   

                         FROM BO_BOLAR_BO       BO
                         JOIN BO_BOLAR_VAGAO    VAG         ON VAG.ID_BO_ID = BO.ID_BO
                         JOIN BO_BOLAR_NF       NF          ON NF.ID_VAGAO_ID = VAG.ID_VAGAO
                         JOIN BO_BOLAR_IMP_CFG  CFG         ON CFG.CLIENTE = BO.AO_COD_AOP
                         JOIN FLUXO_COMERCIAL   FC          ON REGEXP_REPLACE(FC.FX_COD_FLX, '^(\D*)', '') = TO_CHAR(BO.COD_FLX)
                         JOIN MERCADORIA        MRC         ON MRC.MC_ID_MRC = FC.MC_ID_MRC
                         JOIN AREA_OPERACIONAL      ORIF            ON ORIF.AO_ID_AO = FC.AO_ID_EST_OR                                                 
                         LEFT JOIN AREA_OPERACIONAL ORIF1           ON ORIF1.AO_ID_AO = FC.AO_ID_INT_OR
                         JOIN AREA_OPERACIONAL      DEST            ON DEST.AO_ID_AO = FC.AO_ID_EST_DS
                         LEFT JOIN AREA_OPERACIONAL      DEST1           ON DEST1.AO_ID_AO = FC.AO_ID_INT_DS
                         LEFT JOIN VAGAO VG ON VAG.VG_COD_VAG = VG.VG_COD_VAG
                         LEFT JOIN SERIE_VAGAO SV ON SV.SV_ID_SV = VG.SV_ID_SV 
                         JOIN EMPRESA           RECEBEDOR   ON RECEBEDOR.EP_ID_EMP = FC.EP_ID_EMP_DST
                         JOIN EMPRESA           EXPEDIDOR   ON EXPEDIDOR.EP_ID_EMP = FC.EP_ID_EMP_REM
                         LEFT JOIN VW_NFE VNF ON VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE AND NF.NFE_CHAVE_NFE IS NOT NULL
                         WHERE
                         bo.BO_TIMESTAMP BETWEEN :dtInicial AND :dtFinal");


            if (!string.IsNullOrEmpty(fluxoComercial))
            {
                sql.Append("AND fc.fx_cod_flx = :codigoFluxo");
            }

            if (!string.IsNullOrEmpty(origem))
            {
                sql.Append("AND (ORIF1.AO_COD_AOP = :origem OR ORIF.AO_COD_AOP = :origem)");
            }

            if (!string.IsNullOrEmpty(destino))
            {
                sql.Append("AND (DEST1.AO_COD_AOP = :destino OR DEST.AO_COD_AOP = :destino)");
            }

            if (!string.IsNullOrEmpty(situacao))
            {
                sql.Append("AND bo.idt_sit = :situacao");
            }

            if (!string.IsNullOrEmpty(lstVagoes))
            {
                sql.Append("AND bo.lst_vagoes in(:vagoes)");
            }

            if (!string.IsNullOrEmpty(cliente363))
            {
                sql.Append("AND CFG.CLIENTE = :cliente");
            }

            var sqlCount = sql.ToString();

            #region OrderBy

            // Order by (Recupera o indice e o direcionamento (ASC/DESC))
            var colunasOrdenacao = detalhesPaginacao.ColunasOrdenacao;
            var direcoesOrdenacao = detalhesPaginacao.DirecoesOrdenacao;
            string colOrdenacaoIndex;
            var ordenacaoDirecao = String.Empty;

            var ordenacaoPadrao = colunasOrdenacao.Count == 0 && direcoesOrdenacao.Count == 0;
            if (!ordenacaoPadrao)
            {
                colOrdenacaoIndex = colunasOrdenacao[0];
                ordenacaoDirecao = direcoesOrdenacao[0];
            }
            else
            {
                colOrdenacaoIndex = "BO.ID_BO, SV.SV_COD_SER, VAG.VG_COD_VAG";
            }

            switch (colOrdenacaoIndex)
            {
                case "Data": colOrdenacaoIndex = "1"; break;
                case "VagaoId": colOrdenacaoIndex = "2"; break;
                case "Serie": colOrdenacaoIndex = "3"; break;
                case "Vagao": colOrdenacaoIndex = "4"; break;
                case "Fluxo": colOrdenacaoIndex = "5"; break;
                case "Situacao": colOrdenacaoIndex = "6"; break;
                case "PesoReal": colOrdenacaoIndex = "7"; break;
                case "PesoTotal": colOrdenacaoIndex = "8"; break;
                case "VolumeTotal": colOrdenacaoIndex = "9"; break;
                case "MultiploDespacho": colOrdenacaoIndex = "10"; break;
                case "Origem": colOrdenacaoIndex = "11"; break;
                case "Destino": colOrdenacaoIndex = "12"; break;
                case "Cliente363": colOrdenacaoIndex = "13"; break;
                case "Mercadoria": colOrdenacaoIndex = "14"; break;
                case "Recebedor": colOrdenacaoIndex = "15"; break;
                case "Expedidor": colOrdenacaoIndex = "16"; break;
                case "ObservacaoRecusado": colOrdenacaoIndex = "17"; break;
                case "DataHoraCadastro": colOrdenacaoIndex = "18"; break;
                case "SerieDespacho": colOrdenacaoIndex = "19"; break;
                case "DataDespacho": colOrdenacaoIndex = "20"; break;
            }

            sql.AppendFormat(@" ORDER BY {0} {1}", colOrdenacaoIndex, ordenacaoDirecao);

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlCount));

                AtribuirParametroRecebimentoArquivoEdi(dtInicial, dtFinal, fluxoComercial, origem, destino, situacao, lstVagoes, cliente363, query);
                AtribuirParametroRecebimentoArquivoEdi(dtInicial, dtFinal, fluxoComercial, origem, destino, situacao, lstVagoes, cliente363, queryCount);

                query.SetResultTransformer(Transformers.AliasToBean<RecebimentoArquivoEdiDto>());

                query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                query.SetMaxResults(detalhesPaginacao.Limite ?? DetalhesPaginacao.LimitePadrao);

                var items = query.List<RecebimentoArquivoEdiDto>();

                var total = queryCount.UniqueResult<decimal>();

                var result = new ResultadoPaginado<RecebimentoArquivoEdiDto>
                {
                    Items = items,
                    Total = (long)total
                };

                return result;
            }
        }

        private void AtribuirParametroRecebimentoArquivoEdi(DateTime dtInicial, DateTime dtFinal, string fluxoComercial, string origem, string destino, string situacao, string lstVagoes, string cliente, ISQLQuery query)
        {
            query.SetParameter("dtInicial", dtInicial);
            query.SetParameter("dtFinal", dtFinal);

            if (!string.IsNullOrEmpty(fluxoComercial))
            {
                query.SetParameter("codigoFluxo", fluxoComercial);
            }

            if (!string.IsNullOrEmpty(origem))
            {
                query.SetParameter("origem", origem);
            }

            if (!string.IsNullOrEmpty(destino))
            {
                query.SetParameter("destino", destino);
            }

            if (!string.IsNullOrEmpty(situacao))
            {
                query.SetParameter("situacao", situacao);
            }

            if (!string.IsNullOrEmpty(lstVagoes))
            {
                query.SetParameterList("vagoes", lstVagoes.Split(','));
            }

            if (!string.IsNullOrEmpty(cliente))
            {
                query.SetParameter("cliente", cliente);
            }
        }
    }
}
