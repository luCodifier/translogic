﻿using System.Collections.Generic;
using System.Linq;
using NHibernate.Linq;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    using ALL.Core.AcessoDados;
    using Model.Bolar.Repositories;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Bolar;
    
    /// <summary>
    /// Repositorio para <see cref="BolarBoImpCfg"/>
    /// </summary>
    public class BolarBoImpCfgRepository : NHRepository<BolarBoImpCfg, int>, IBolarBoImpCfgRepository
    {
	    /// <summary>
	    /// Obter por cnpj
	    /// </summary>
	    /// <param name="cnpj">CNPJ da empresa</param>
	    /// <returns>Objeto BolarBoImpCfg</returns>
	    public BolarBoImpCfg ObterPorCnpjAtivo(string cnpj)
	    {
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Like("Cnpj", cnpj) && Restrictions.Eq("IndAtivo", true));
			return ObterPrimeiro(criteria);
	    }

        /// <summary>
        /// Lista de expedidor
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista de clientes</returns>
        public List<EmpresaPesquisaPorNome> ObterListaNomesExpedidoresDescResumida(string query)
        {
            using (var session = OpenSession())
            {
                var tq = query.Trim().ToUpper();
                var qr = from cfg in session.Query<BolarBoImpCfg>()
                         where cfg.DescricaoEmpresa.ToUpper().Contains(tq)
                         select new EmpresaPesquisaPorNome()
                         {
                             Id = (int)cfg.Id,
                             DescricaoResumida = cfg.DescricaoEmpresa
                         };

                var temp = qr.ToList();

                return temp.Distinct().ToList();
            }
        }


        public IList<EmpresaResumidaDto> ObterTodasAtivas()
        {
            var sql = @"SELECT cliente      AS ""CodigoEmpresa"",
                               cliente_dsc  AS ""DescricaoEmpresa""
                        FROM bo_bolar_imp_cfg 
                        WHERE ic_ativo = 'S' 
                        GROUP BY cliente,cliente_dsc 
                        ORDER BY cliente_dsc";

            IList<EmpresaResumidaDto> list;

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.AliasToBean<EmpresaResumidaDto>());

                list = query.List<EmpresaResumidaDto>();
            }

            return list;
        }

        public IList<EmpresaResumidaDto> ObterTodasComFaturamentoAutomatico()
        {
            var sql = @"SELECT   cliente        AS ""CodigoEmpresa"",
                                 cliente_dsc    AS ""DescricaoEmpresa""
                        FROM bo_bolar_imp_cfg cfg join bo_bolar_conf_carga_aut aut
                            ON cfg.cliente = aut.ao_cod_aop
                        GROUP BY cliente,cliente_dsc
                        ORDER BY 2";

            IList<EmpresaResumidaDto> list;

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                query.SetResultTransformer(Transformers.AliasToBean<EmpresaResumidaDto>());

                list = query.List<EmpresaResumidaDto>();
            }

            return list;
        }
    }
}
