﻿using System.Collections.Generic;
using NHibernate.Criterion;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    using ALL.Core.AcessoDados;
    using Model.Bolar.Repositories;
    using Translogic.Modules.Core.Domain.Model.Bolar;

    /// <summary>
    /// Repositorio para <see cref="BolarCte"/>
    /// </summary>
    public class BolarCteRepository : NHRepository<BolarCte, int>, IBolarCteRepository
    {
        /// <summary>
        /// Retorna bolar ctes por Chave do CTE
        /// </summary>
        /// <param name="chave">Chave do CTE</param>
        /// <returns>Bolar Bo Cte</returns>
        public BolarCte ObterPorChave(string chave)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("ChaveCte", chave));
            return ObterPrimeiro(criteria);
        }

    }
}
