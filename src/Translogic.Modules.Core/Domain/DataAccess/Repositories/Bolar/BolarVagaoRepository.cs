﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar
{
    using ALL.Core.AcessoDados;
    using Model.Bolar.Repositories;
    using NHibernate.Criterion;
    using Translogic.Modules.Core.Domain.Model.Bolar;

    /// <summary>
    /// Repositorio para <see cref="BolarVagao"/>
    /// </summary>
    public class BolarVagaoRepository : NHRepository<BolarVagao, int>, IBolarVagaoRepository
    {
         /// <summary>
        /// Retorna bolar vagao pelo código vagão
        /// </summary>
        /// <param name="codigovagao">Código do vagão</param>
        /// <returns>Bolar Vagao objeto</returns>
        public BolarVagao ObterPorCodigoVagao(string codigovagao)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Vagao", codigovagao));
            return ObterPrimeiro(criteria);
        }
    }
}
