﻿using NHibernate.Transform;
using System;
using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;
using ALL.Core.Dominio;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories;
using Translogic.Core.Infrastructure.Web;
namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FormacaoAutomatica
{
    public class LogFormacaoAutoVeiculoRepository : NHRepository<LogFormacaoAutoVeiculo, int>, ILogFormacaoAutoVeiculoRepository
    {
        public ResultadoPaginado<FormacaoAutoVeiculoDto> ObterConsultaFormacaoAutomaticaVeiculos(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal IdMensagem)
        {
            #region Query

            var sql = string.Format(@"SELECT TAB.VagaoLocomotiva AS ""VagaoLocomotiva"",
                                             TAB.StatusProcessamento AS ""StatusProcessamento"",
                                            TAB.Cad AS ""Cad"",
                                            TAB.Pedido AS ""Pedido"",
                                            TAB.Localizacao AS ""Localizacao"",
                                            TAB.Situacao AS ""Situacao"",
                                            TAB.Lotacao AS ""Lotacao"",
                                            TAB.Recomendacao AS ""Recomendacao"",
                                            TAB.DescricaoErro AS ""DescricaoErro""
                                        FROM (SELECT (SELECT TO_NUMBER(FA.POSICAO)
                                                        FROM FORMACAO_AUTO_VAGAO FA
                                                        WHERE FA.COD_VAGAO = L.LFAV_NUM_VEIC
                                                        AND LT.ID_MENSAGEM = FA.ID_RECEBIMENTO) Posicao,
                                                    L.LFAV_NUM_VEIC VagaoLocomotiva,
                                                    CASE
                                                        WHEN LT.LFA_DESC_OS = 'S' AND LT.LFA_DESC_FTREM = 'S' THEN
                                                        'S'
                                                        ELSE
                                                        'N'
                                                    END StatusProcessamento,
                                                    CASE
                                                    (SELECT 1 FROM VAGAO V WHERE V.VG_COD_VAG = L.LFAV_NUM_VEIC)
                                                        WHEN 1 THEN
                                                        'S'
                                                        ELSE
                                                        'N'
                                                    END Cad,
                                                    CASE (SELECT 1
                                                        FROM VAGAO_PEDIDO_VIG VPV, VAGAO V
                                                        WHERE VPV.VG_ID_VG = V.VG_ID_VG
                                                        AND V.VG_COD_VAG = L.LFAV_NUM_VEIC
                                                        AND ROWNUM = 1)
                                                        WHEN 1 THEN
                                                        'S'
                                                        ELSE
                                                        'N'
                                                    END Pedido,
                                                    (SELECT EV.LO_DSC_PT
                                                        FROM ESTADO_VAGAO_NOVA EV, VAGAO V
                                                        WHERE EV.VG_ID_VG = V.VG_ID_VG
                                                        AND V.VG_COD_VAG = L.LFAV_NUM_VEIC) Localizacao,
                                                    (SELECT EV.SI_DSC_PT
                                                        FROM ESTADO_VAGAO_NOVA EV, VAGAO V
                                                        WHERE EV.VG_ID_VG = V.VG_ID_VG
                                                        AND V.VG_COD_VAG = L.LFAV_NUM_VEIC) Situacao,
                                                    (SELECT EV.SH_DSC_PT
                                                        FROM ESTADO_VAGAO_NOVA EV, VAGAO V
                                                        WHERE EV.VG_ID_VG = V.VG_ID_VG
                                                        AND V.VG_COD_VAG = L.LFAV_NUM_VEIC) Lotacao,
                                                    (SELECT EV.CD_DSC_PT
                                                        FROM ESTADO_VAGAO_NOVA EV, VAGAO V
                                                        WHERE EV.VG_ID_VG = V.VG_ID_VG
                                                        AND V.VG_COD_VAG = L.LFAV_NUM_VEIC) Recomendacao,
                                                    L.LFAV_DESC_ANEX DescricaoErro
                                                FROM LOG_FORM_AUTO_VEICULOS L, LOG_FORMACAO_AUTO LT
                                                WHERE LT.LFA_ID = L.LFA_ID
                                                AND L.LFAV_TP_VEICULO = 'VAGAO'
                                                AND LT.ID_MENSAGEM = {0}
        
                                            UNION
        
                                            SELECT (SELECT TO_NUMBER(FAL.POSICAO)
                                                        FROM FORMACAO_AUTO_LOCO FAL
                                                        WHERE FAL.COD_LOCO = L.LFAV_NUM_VEIC
                                                        AND LT.ID_MENSAGEM = FAL.ID_RECEBIMENTO) POSICAO,
                                                    L.LFAV_NUM_VEIC VagaoLocomotiva,
                                                    CASE
                                                        WHEN LT.LFA_DESC_OS = 'S' AND LT.LFA_DESC_FTREM = 'S' THEN
                                                        'S'
                                                        ELSE
                                                        'N'
                                                    END StatusProcessamento,
                                                    CASE (SELECT 1
                                                        FROM LOCOMOTIVA LM
                                                        WHERE LM.LC_COD_LOC = L.LFAV_NUM_VEIC)
                                                        WHEN 1 THEN
                                                        'S'
                                                        ELSE
                                                        'N'
                                                    END Cad,
                                                    'S' Pedido,
                                                    (SELECT LC.LO_DSC_PT
                                                        FROM LOCOMOTIVA LM, LOCALIZACAO LC, LOCAL_LOCO_VIG LLV
                                                        WHERE LM.LC_COD_LOC = L.LFAV_NUM_VEIC
                                                        AND LM.LC_ID_LC = LLV.LC_ID_LC
                                                        AND LLV.LO_IDT_LCL = LC.LO_IDT_LCL) Localizacao,
                                                    (SELECT S.SI_DSC_PT
                                                        FROM LOCOMOTIVA LM, SITUACAO S, SITUACAO_LOCO_VIG SL
                                                        WHERE LM.LC_COD_LOC = L.LFAV_NUM_VEIC
                                                        AND LM.LC_ID_LC = SL.LC_ID_LC
                                                        AND S.SI_IDT_STC = SL.SI_IDT_STC) Situacao,
                                                    NULL Lotacao,
                                                    (SELECT CDU.CD_DSC_PT
                                                        FROM LOCOMOTIVA LM, CONDUSO_LOCO_VIG CL, CONDICAO_USO CDU
                                                        WHERE LM.LC_ID_LC = CL.LC_ID_LC
                                                        AND LM.LC_COD_LOC = L.LFAV_NUM_VEIC
                                                        AND CDU.CD_IDT_CUS = CL.CD_IDT_CUS) Recomendacao,
                                                    L.LFAV_DESC_ANEX DescricaoErro
                                                FROM LOG_FORM_AUTO_VEICULOS L, LOG_FORMACAO_AUTO LT
                                                WHERE LT.LFA_ID = L.LFA_ID
                                                AND L.LFAV_TP_VEICULO = 'LOCOMOTIVA'
                                                AND LT.ID_MENSAGEM = {0})TAB ORDER BY POSICAO", IdMensagem);
            #endregion

            #region Parameters


            #endregion

            //sql.Append(" ORDER BY 1 ");

            #region Execution
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                query.SetResultTransformer(Transformers.AliasToBean<FormacaoAutoVeiculoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    //query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<FormacaoAutoVeiculoDto>();

                var result = new ResultadoPaginado<FormacaoAutoVeiculoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }

            #endregion
        }
    }
}