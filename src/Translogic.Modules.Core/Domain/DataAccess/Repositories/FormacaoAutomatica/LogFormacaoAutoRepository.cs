﻿using NHibernate.Transform;
using System.Text;
using System;
using NHibernate;
using System.Data.OracleClient;
using System.Data;
using NHibernate.Criterion;
using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;
using ALL.Core.Dominio;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using System.Linq;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.FormacaoAutomatica
{
    public class LogFormacaoAutoRepository : NHRepository<LogFormacaoAuto, int>, ILogFormacaoAutoRepository
    {
        public ResultadoPaginado<FormacaoAutoDto> ObterConsultaFormacaoAutomatica(DetalhesPaginacaoWeb detalhesPaginacaoWeb, FormacaoAutoRequestDto formacaoAutoRequestDto)
        {
            var sqlWihtoutOrderBy = new StringBuilder();
            var sql = new StringBuilder(@"SELECT    L.LFA_ID AS ""LfaId"",
                                                    (SELECT CASE WHEN LENGTH (FAT.PREFIXO) > 4 THEN 'MRS' ELSE 'VLI' END
                                                    FROM FORMACAO_AUTO_TREM FAT
                                                    WHERE FAT.ID_RECEBIMENTO = L.ID_MENSAGEM) AS ""Operacao"",
                                                    L.X1_NRO_OS AS ""OS"",
                                                   (SELECT FAT.PREFIXO  FROM FORMACAO_AUTO_TREM FAT
                                                     WHERE FAT.ID_RECEBIMENTO = L.ID_MENSAGEM) || ' - ' || T.X1_PFX_TRE AS ""Trem"",
                                                    NVL(L.LFA_ORI, (SELECT FAT.ORIGEM
                                                                       FROM FORMACAO_AUTO_TREM FAT
                                                                      WHERE FAT.ID_RECEBIMENTO = L.ID_MENSAGEM)) AS ""LfaOrigem"",
                                                    NVL(L.LFA_DST, (SELECT FAT.DESTINO
                                                                      FROM FORMACAO_AUTO_TREM FAT
                                                                     WHERE FAT.ID_RECEBIMENTO = L.ID_MENSAGEM)) AS ""LfaDestino"",
                                                    CASE
                                                     WHEN L.LFA_DESC_OS = 'S' AND L.LFA_DESC_FTREM = 'S' THEN
                                                      'Verde'
                                                     ELSE
                                                      'Vermelho'
                                                   END AS ""StatusProcessamento"",
                                                   CASE
                                                     WHEN L.LFA_DESC_LIB = 'S' THEN
                                                      'Verde'
                                                     ELSE
                                                      'Vermelho'
                                                   END AS ""StatusLiberacao"",
                                                   L.LFA_TENTATIVAS AS ""LfaTentativas"",
                                                   CASE
                                                     WHEN L.LFA_TENTATIVAS = 6 OR L.LFA_DESC_LIB = 'S' THEN
                                                      NULL
                                                     ELSE
                                                      (SELECT AJ.NEXT_DATE 
                                                       FROM ALL_JOBS AJ 
                                                       WHERE AJ.JOB = 18823)
                                                   END AS ""DtProximaExecucao"",
                                                   CASE
                                                     WHEN L.LFA_DESC_OS <> 'S' AND L.LFA_DESC_OS IS NOT NULL THEN
                                                      L.LFA_DESC_OS
                                                     WHEN L.LFA_DESC_FTREM <> 'S' AND L.LFA_DESC_FTREM IS NOT NULL THEN
                                                      L.LFA_DESC_FTREM
                                                     WHEN L.LFA_DESC_LIB <> 'S' AND L.LFA_DESC_LIB IS NOT NULL THEN
                                                      L.LFA_DESC_LIB
                                                   END AS ""Obs"",
                                                   L.ID_MENSAGEM AS ""LIdMensagem""
                                              FROM LOG_FORMACAO_AUTO L, T2_OS T
                                             WHERE L.X1_NRO_OS = T.X1_NRO_OS(+) 
                                            ");


            ////Data Inicial & Data Final
            if (formacaoAutoRequestDto.dtInicial.HasValue && formacaoAutoRequestDto.dtFinal.HasValue)
            {
                sql.AppendFormat(" AND L.LFA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", formacaoAutoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), formacaoAutoRequestDto.dtFinal.Value.ToString("dd/MM/yyyy"));
            }else if (formacaoAutoRequestDto.dtInicial.HasValue) 
            {
                    sql.AppendFormat(" AND L.LFA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", formacaoAutoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"));
            }

            ////Status Processado
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.StatusProcessamento)))
            {
                if (formacaoAutoRequestDto.StatusProcessamento == "Processados")
                {
                    sql.AppendLine(" AND L.LFA_DESC_OS = 'S' AND L.LFA_DESC_FTREM = 'S'");
                }
                else
                {
                    sql.AppendLine(" AND (L.LFA_DESC_OS != 'S' OR L.LFA_DESC_FTREM != 'S' OR L.LFA_DESC_FTREM IS NULL)");
                }
            }

            ////OS
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.OS)))
            {
                sql.AppendLine(" AND L.X1_NRO_OS = '" + formacaoAutoRequestDto.OS + "'");
            }

            //Trem Prefixo
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.TremPrefixo)))
            {
                sql.AppendLine(" AND T.X1_PFX_TRE  = '" + formacaoAutoRequestDto.TremPrefixo.ToUpper() + "'");
            }

            ////ORIGEM
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.Origem)))
            {
                sql.AppendLine(" AND SUBSTR(L.LFA_ORI, 1, 3) = '" + formacaoAutoRequestDto.Origem.ToUpper() + "'");
            }

            ////DESTINO
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.Destino)))
            {
                sql.AppendLine(" AND SUBSTR(L.LFA_DST, 1, 3) = '" + formacaoAutoRequestDto.Destino.ToUpper() + "'");
            }

            sqlWihtoutOrderBy.AppendLine(sql.ToString());

            /*sql.AppendLine(" ORDER BY L.ID_MENSAGEM;");*/

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlWihtoutOrderBy.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<FormacaoAutoDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<FormacaoAutoDto>();

                var result = new ResultadoPaginado<FormacaoAutoDto>
                {
                    Items = itens.OrderBy(m=>m.LIdMensagem).ToList(),
                    Total = (long)total
                };
               
                return result;
            }
        }

        public IEnumerable<FormacaoAutoDto> ObterConsultaFormacaoAutomaticaExportar(FormacaoAutoRequestDto formacaoAutoRequestDto)
        {
            var sql = new StringBuilder(@"SELECT L.LFA_ID                                                       AS ""LfaId"",  
                                            'FORMACÃO'                                                          AS ""Operacao"", 
                                            L.X1_NRO_OS                                                         AS ""OS"", 
                                            T.X1_PFX_TRE                                                        AS ""Trem"", 
                                            SUBSTR(L.LFA_ORI, 1, 3)                                             AS ""LfaOrigem"", 
                                            SUBSTR(L.LFA_DST, 1, 3)                                             AS ""LfaDestino"", 
                                            CASE 
                                            WHEN L.LFA_DESC_OS = 'S' AND L.LFA_DESC_FTREM = 'S' THEN 
                                            'Verde' 
                                            ELSE 'Vermelho' END                                                 AS ""StatusProcessamento"", 
                                            CASE 
                                            WHEN L.LFA_DESC_LIB = 'S' THEN 
                                            'Verde' 
                                            ELSE 'Vermelho' END                                                 AS ""StatusLiberacao"", 
                                            L.LFA_TENTATIVAS                                                    AS ""LfaTentativas"", 
                                            SYSDATE                                                             AS ""DtProximaExecucao"", 
                                            CASE 
                                            WHEN L.LFA_DESC_OS <> 'S' AND L.LFA_DESC_OS <> NULL THEN 
                                            L.LFA_DESC_OS                                                 
                                            WHEN L.LFA_DESC_FTREM <> 'S' AND L.LFA_DESC_FTREM <> NULL THEN 
                                            L.LFA_DESC_FTREM 
                                            WHEN L.LFA_DESC_LIB <> 'S' AND L.LFA_DESC_LIB <> NULL THEN 
                                            L.LFA_DESC_LIB 
                                            END                                                                 AS ""Obs"" 
                                            FROM LOG_FORMACAO_AUTO L, 
                                                T2_OS T 
                                            WHERE L.X1_NRO_OS = T.X1_NRO_OS(+)");

            ////Data Inicial & Data Final
            if (formacaoAutoRequestDto.dtInicial.HasValue && formacaoAutoRequestDto.dtFinal.HasValue)
            {
                sql.AppendFormat(" AND L.LFA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", formacaoAutoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), formacaoAutoRequestDto.dtFinal.Value.ToString("dd/MM/yyyy"));
            }
            else if (formacaoAutoRequestDto.dtInicial.HasValue)
            {
                sql.AppendFormat(" AND L.LFA_TIMESTAMP BETWEEN TO_DATE('{0} 00:00:00','DD/MM/YYYY HH24:MI:SS') AND TO_DATE('{1} 23:59:59','DD/MM/YYYY HH24:MI:SS') ", formacaoAutoRequestDto.dtInicial.Value.ToString("dd/MM/yyyy"), DateTime.Now.ToString("dd/MM/yyyy"));
            }

            ////Status Processado
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.StatusProcessamento)))
            {
                if (formacaoAutoRequestDto.StatusProcessamento == "Processados")
                {
                    sql.AppendLine(" AND L.LFA_DESC_OS = 'S' AND L.LFA_DESC_FTREM = 'S'");
                }
                else
                {
                    sql.AppendLine(" AND (L.LFA_DESC_OS != 'S' OR L.LFA_DESC_FTREM != 'S' OR L.LFA_DESC_FTREM IS NULL)");
                }
            }

            ////OS
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.OS)))
            {
                sql.AppendLine(" AND L.X1_NRO_OS = '" + formacaoAutoRequestDto.OS + "'");
            }

            //Trem Prefixo
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.TremPrefixo)))
            {
                sql.AppendLine(" AND T.X1_PFX_TRE  = '" + formacaoAutoRequestDto.TremPrefixo.ToUpper() + "'");
            }

            ////ORIGEM
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.Origem)))
            {
                sql.AppendLine(" AND SUBSTR(L.LFA_ORI, 1, 3) = '" + formacaoAutoRequestDto.Origem.ToUpper() + "'");
            }

            ////DESTINO
            if (!(string.IsNullOrEmpty(formacaoAutoRequestDto.Destino)))
            {
                sql.AppendLine(" AND SUBSTR(L.LFA_DST, 1, 3) = '" + formacaoAutoRequestDto.Destino.ToUpper() + "'");
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<FormacaoAutoDto>());

                return query.List<FormacaoAutoDto>();
            }
        }

        /// <summary>
        /// Zera tentativas
        /// </summary>
        public void ZeraTentativas(decimal IdMensagem)
        {
            using (var session = OpenSession())
            {
                var ql = @"UPDATE LOG_FORMACAO_AUTO SET LFA_TENTATIVAS = 0" +
                            " WHERE ID_MENSAGEM = " + IdMensagem;

                session.CreateSQLQuery(ql).ExecuteUpdate();
                session.Flush();
            }
        }
    }
}       