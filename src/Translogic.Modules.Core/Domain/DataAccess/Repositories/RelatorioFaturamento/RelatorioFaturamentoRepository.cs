﻿using System;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Text;
using NHibernate.Transform;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.RelatorioFaturamento.Repositories;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;
using NHibernate;

namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.RelatorioFaturamento
{
    public class RelatorioFaturamentoRepository : NHRepository<Translogic.Modules.Core.Domain.Model.RelatorioFaturamento.RelatorioFaturamento, int>, IRelatorioFaturamentoRepository
    {

        public ResultadoPaginado<RelatorioFaturamentoDto> ObterRelatorioFaturamento(
                                                                  DetalhesPaginacaoWeb detalhesPaginacao,
                                                                  DateTime dataInicio,
                                                                  DateTime dataFinal,
                                                                  string fluxo,
                                                                  string origem,
                                                                  string destino,
                                                                  string[] vagoes,
                                                                  string situacao,
                                                                  string cliente,
                                                                  string expeditor,
                                                                  string segmento,
                                                                  string prefixo,
                                                                  string Os,
                                                                  string malha,
                                                                  string uf,
                                                                  string estacao)
        {
            var hql = new StringBuilder(@"SELECT * FROM (SELECT DISTINCT
                                              DP.DP_DT                                                              AS ""Faturamento"",
                                              VG.VG_COD_VAG                                                         AS ""Vagao"",
                                              UFO.ES_SGL_EST                                                        AS ""UFOrigem"",
                                              OPED.AO_COD_AOP                                                       AS ""Origem"",
                                              UFD.ES_SGL_EST                                                        AS ""UFDestino"",
                                              DPED.AO_COD_AOP                                                       AS ""Destino"",
                                              SV.SV_COD_SER                                                         AS ""TipoVagao"",
                                              FC.FX_COD_FLX                                                         AS ""Fluxo"",
                                              FC.FX_COD_SEGMENTO                                                    AS ""Segmento"",
                                              NVL(BOVG.MD, 'NAO')                                                   AS ""MD"",
                                              CFG.CLIENTE_DSC                                                       AS ""Cliente"",
                                              EPEXP.EP_DSC_RSM                                                      AS ""Expedidor"",
                                              EPREC.EP_DSC_RSM                                                      AS ""Recebedor"",
                       (CASE BO.IDT_SIT 
                          WHEN 'D' THEN 'S' 
                          WHEN 'E' THEN (CASE NVL(VPED.VB_COD_TRA, ' ') WHEN 'BOI' THEN 'N' ELSE 'S' END)
                          ELSE 'N' END)                                                                             AS ""Str363"",
                                               (CASE NVL(VPED.VB_COD_TRA, ' ') 
                                                  WHEN 'BOI' THEN 'S' 
                                                  ELSE 'N' END)                                                     AS ""Str1131"",
                                               (SELECT LISTAGG(TR.TR_PFX_TRM )WITHIN GROUP(ORDER BY TR.TR_PFX_TRM)
                                                FROM   COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)                               AS ""PrefixoTrem"",
                                               (SELECT LISTAGG(OS.X1_NRO_OS)WITHIN GROUP(ORDER BY OS.X1_NRO_OS)
                                                FROM   COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)                               AS ""OS""
                                            FROM   DESPACHO DP
                                              INNER JOIN ITEM_DESPACHO          IDP ON (DP.DP_ID_DP = IDP.DP_ID_DP)
                                              INNER JOIN VAGAO                   VG ON (IDP.VG_ID_VG = VG.VG_ID_VG)
                                              INNER JOIN TRANSLOGIC.SERIE_VAGAO  SV ON (SV.SV_ID_SV = VG.SV_ID_SV)
                                              INNER JOIN TRANSLOGIC.TIPO_VAGAO   TV ON (TV.TV_IDT_TVG = SV.TV_IDT_TVG)
                                              INNER JOIN TRANSLOGIC.CLASSE_VAGAO CV ON (CV.CS_IDT_CVG = TV.CS_IDT_CVG)
                                              INNER JOIN FOLHA_ESPECIF_VAGAO     FE ON (FE.FC_ID_FC = VG.FC_ID_FC)
                                              LEFT JOIN NOTA_FISCAL              NF ON (NF.DP_ID_DP = DP.DP_ID_DP AND VG.VG_ID_VG = NF.VG_ID_VG)
                                              LEFT JOIN VAGAO_PEDIDO           VPED ON (VG.VG_ID_VG = VPED.VG_ID_VG AND DP.DP_DT >= VPED.VB_DAT_CAR AND DP.DP_DT  <= NVL(VPED.VB_DAT_DSC, VPED.VB_DAT_CAR + 15))
                                              LEFT JOIN T2_PEDIDO               PED ON (PED.W1_ID_PED = NVL(VPED.PX_IDT_PDR, VPED.PT_ID_ORT))
                                              LEFT JOIN AREA_OPERACIONAL       OPED ON (OPED.AO_ID_AO = PED.W1_IDT_EST_ORI)
                                              LEFT JOIN AREA_OPERACIONAL       DPED ON (DPED.AO_ID_AO = PED.W1_IDT_EST_DES)
                                              LEFT JOIN UNID_PRODUCAO          UNID ON UNID.UP_ID_UNP = OPED.UP_ID_UNP
                                              LEFT JOIN MUNICIPIO               MNO ON (OPED.MN_ID_MNC = MNO.MN_ID_MNC) 
                                              LEFT JOIN ESTADO                  UFO ON (MNO.ES_ID_EST = UFO.ES_ID_EST)
                                              LEFT JOIN MUNICIPIO               MND ON (OPED.MN_ID_MNC = MND.MN_ID_MNC) 
                                              LEFT JOIN ESTADO                  UFD ON (MND.ES_ID_EST = UFD.ES_ID_EST)
                                              LEFT JOIN MERCADORIA               ME ON (ME.MC_ID_MRC = PED.W1_IDT_MRC)
                                              LEFT JOIN FLUXO_COMERCIAL          FC ON (FC.FX_ID_FLX = PED.W1_IDT_FLX)
                                              LEFT JOIN CONTRATO                 CT ON (CT.CZ_ID_CTT = FC.CZ_ID_CTT)
                                              LEFT JOIN EMPRESA               EPCLI ON (EPCLI.EP_ID_EMP = FC.EP_ID_EMP_COR)
                                              LEFT JOIN EMPRESA               EPEXP ON (EPEXP.EP_ID_EMP = FC.EP_ID_EMP_REM)
                                              LEFT JOIN EMPRESA               EPREC ON (EPREC.EP_ID_EMP = FC.EP_ID_EMP_DST)
                                              LEFT JOIN AREA_OPERACIONAL       TDST ON (TDST.AO_ID_AO  = FC.AO_ID_TER_DS)
                                              LEFT JOIN BO_BOLAR_VAGAO         BOVG ON BOVG.DP_ID_DP = DP.DP_ID_DP
                                              LEFT JOIN BO_BOLAR_BO              BO ON BO.ID_BO = BOVG.ID_BO_ID
                                              LEFT JOIN BO_BOLAR_NF            BONF ON BONF.ID_VAGAO_ID = BOVG.ID_VAGAO
                                              LEFT JOIN BO_BOLAR_IMP_CFG        CFG ON BO.AO_COD_AOP = CFG.CLIENTE
                                                WHERE DP.DP_DT >= TO_DATE('{DataInicialFiltro} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
                                                  AND DP.DP_DT  <= TO_DATE('{DataFinalFiltro} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                                                  {Filtros}) WHERE ROWNUM <= 1000");

            hql = hql.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy"));
            hql = hql.Replace("{DataFinalFiltro}", dataFinal.ToString("dd/MM/yyyy"));

            // Filtros
            string filtros = "";
            //uf = uf.ToUpper();
            //Fluxo
            if (!string.IsNullOrEmpty(fluxo))
            {
                filtros += " AND FC.FX_COD_FLX = '" + fluxo.ToUpper() + "'";
            }

            //Origem
            if(!string.IsNullOrEmpty(origem))
            {
                filtros += " AND OPED.AO_COD_AOP = '" + origem.ToUpper() + "'";
            }
            //Destino
            if(!string.IsNullOrEmpty(destino))
            {
                filtros += " AND DPED.AO_COD_AOP  = '" + destino.ToUpper() + "'";
            }
            //Array Vagoes
            if (vagoes.Length > 0 && vagoes[0] != "")
            {
                filtros += " AND VG.VG_COD_VAG IN (";
                int i = 0;
                for (i = 0; i < vagoes.Length; i++) 
                {
                    if(i == 0)
                    {
                        filtros += "'" + vagoes[i] + "'";
                    }else{
                        filtros += ",'" + vagoes[i] + "'";
                    } 
                }
                filtros += ")";
            }
            ////Situacao
            //if(!string.IsNullOrEmpty(situacao))
            //{
            //    filtros += "AND = '" + situacao + "'";
            //}
            //Cliente
            if (!string.IsNullOrEmpty(cliente))
            {
                //filtros += " AND CFG.ID_BO_BOLAR_IMP_CFG = '" + cliente + "'";
                filtros += " AND CFG.CLIENTE_DSC = '" + cliente + "'";
                
            }
            //Expeditor
            if(!string.IsNullOrEmpty(expeditor))
            {
                filtros += " AND EPEXP.EP_DSC_RSM = '" + expeditor.ToUpper() + "'";
            }
            //Segmento
            if (!string.IsNullOrEmpty(segmento) && segmento.Trim() != "TODOS")
            {
                filtros += " AND FC.FX_COD_SEGMENTO = '" + segmento.ToUpper() + "'";
            }
            //Prefixo
            if(!string.IsNullOrEmpty(prefixo))
            {
                filtros += @" AND (SELECT LISTAGG(TR.TR_PFX_TRM )WITHIN GROUP(ORDER BY TR.TR_PFX_TRM)
                                                FROM COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT) = '" + prefixo.ToUpper() + "'";
            }
            //Os
            if(!string.IsNullOrEmpty(Os))
            {
                filtros += @" AND (SELECT LISTAGG(OS.X1_NRO_OS)WITHIN GROUP(ORDER BY OS.X1_NRO_OS)
                                                FROM   COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)  = '" + Os + "'";
            }
            ////Malha
            //if(!string.IsNullOrEmpty(malha))
            //{
            //    filtros += "AND = '" + malha + "'";
            //}
            //Uf
            if (!string.IsNullOrEmpty(uf) && uf.Trim() != "Todas")
            {
                filtros += " AND UFD.ES_SGL_EST = '" + uf.ToUpper() + "'";
            }
            ////estacao
            //if(!string.IsNullOrEmpty(estacao) && estacao.Trim() != "TODOS")
            //{
            //    filtros += "AND = '" + estacao + "'";
            //}

            //ORDER BY
            filtros += "ORDER BY DP.DP_DT ASC";

            //Verifica se existem filtros
            if (!string.IsNullOrEmpty(filtros))
            {
                hql.Replace("{Filtros}", filtros);
            }
            else 
            { 
                hql.Replace("{Filtros}","");
            }

           


            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", hql.ToString()));

                query.SetResultTransformer(Transformers.AliasToBean<RelatorioFaturamentoDto>());

                if (detalhesPaginacao.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacao.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacao.Limite ?? 1000);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<RelatorioFaturamentoDto>();

                var result = new ResultadoPaginado<RelatorioFaturamentoDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public IList<RelatorioFaturamentoExportDto> ObterRelatorioFaturamentoExportar(
                                                                  DateTime dataInicio,
                                                                  DateTime dataFinal,
                                                                  string fluxo,
                                                                  string origem,
                                                                  string destino,
                                                                  string[] vagoes,
                                                                  string situacao,
                                                                  string cliente,
                                                                  string expeditor,
                                                                  string segmento,
                                                                  string prefixo,
                                                                  string Os,
                                                                  string malha,
                                                                  string uf,
                                                                  string estacao)
        {

            var hql = new StringBuilder(@"SELECT DISTINCT
                                        VG.VG_COD_VAG                                                         AS ""Vagao"",      
                                        SV.SV_COD_SER                                                         AS ""Serie"",   
                                        FC.FX_COD_SEGMENTO                                                    AS ""Segmento"",                                     
                                        DP.DP_DT                                                              AS ""Faturamento"",
                                        UFO.ES_SGL_EST                                                        AS ""UFOrigem"",
                                        OPED.AO_COD_AOP                                                       AS ""Origem"",
                                        UFD.ES_SGL_EST                                                        AS ""UFDestino"",
                                        DPED.AO_COD_AOP                                                       AS ""Destino"",
                                        FC.FX_COD_FLX                                                         AS ""Fluxo"",
                                        NVL(BOVG.MD, 'NAO')                                                   AS ""MD"",
                                        NF.NF_NRO_NF                                                          AS ""NotaFiscal"",
                                        NF.NF_VAL_NFL                                                         AS ""ValorNFCliente"",
                                        NF.NFE_CHAVE_NFE                                                      AS ""ChaveNFe"",
                                        NVL(VG.VG_NUM_TRA, FE.FC_TAR)                                         AS ""Tara"",
                                         (SELECT 
                                            CASE 
                                              WHEN MAX(VNF.NFE_PESO) > 1000 THEN MAX(VNF.NFE_PESO) / 1000 
                                              ELSE MAX(VNF.NFE_PESO) END
                                           FROM   VW_NFE VNF 
                                           WHERE  VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE)                           AS ""PesoRateio"",
                                          (SELECT MAX(VNF.NFE_VOLUME)
                                           FROM   VW_NFE VNF 
                                           WHERE  VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE)                           AS ""Volume"",
                                          IDP.ID_NUM_PCL                                                      AS ""PesoTotal"",
                                          NVL(VG.VG_NUM_TRA, FE.FC_TAR) + IDP.ID_NUM_PCL                      AS ""TB"",
                                          BONF.NUM_CONTAINER                                                  AS ""Containers"",
                                          CFG.CLIENTE_DSC                                                     AS ""Cliente"",
                                          EPEXP.EP_DSC_RSM                                                    AS ""Expedidor"",
                                          EPREC.EP_DSC_RSM                                                    AS ""Recebedor"",
                                          NVL((SELECT RF2.EP_DSC_RSM
                                               FROM   VW_NFE VNF 
                                               INNER JOIN EMPRESA RF2 ON (RF2.EP_CGC_EMP = VNF.NFE_CNPJ_ORI)
                                               WHERE  VNF.NFE_CHAVE = NF.NFE_CHAVE_NFE 
                                               AND    ROWNUM = 1), 
                                               (SELECT RF1.EP_DSC_RSM
                                                FROM EMPRESA RF1 
                                                WHERE RF1.EP_CGC_EMP = NF.EP_CGC_REM
                                                AND ROWNUM = 1))                                              AS ""RemetenteFiscal"",
                                           (CASE BO.IDT_SIT 
                                              WHEN 'D' THEN 'S' 
                                              WHEN 'E' THEN (CASE NVL(VPED.VB_COD_TRA, ' ') 
                                            WHEN 'BOI' THEN 'N' ELSE 'S' END)
                                              ELSE 'N' END)                                                   AS ""Str363"",
                                           (CASE NVL(VPED.VB_COD_TRA, ' ') 
                                              WHEN 'BOI' THEN 'S' 
                                              ELSE 'N' END)                                                   AS ""Str1131"",
                                           (SELECT LISTAGG(TR.TR_PFX_TRM )WITHIN GROUP(ORDER BY TR.TR_PFX_TRM)
                                            FROM   COMPVAGAO CVV 
                                            INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                            INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                            WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                            AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)                             AS ""PrefixoTrem"",
                                            (SELECT LISTAGG(OS.X1_NRO_OS)WITHIN GROUP(ORDER BY OS.X1_NRO_OS)
                                            FROM   COMPVAGAO CVV 
                                            INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                            INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                            INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                            WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                            AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)                             AS ""OS""
                                                                                    FROM   DESPACHO DP
                                                                                      INNER JOIN ITEM_DESPACHO          IDP ON (DP.DP_ID_DP = IDP.DP_ID_DP)
                                                                                      INNER JOIN VAGAO                   VG ON (IDP.VG_ID_VG = VG.VG_ID_VG)
                                                                                      INNER JOIN TRANSLOGIC.SERIE_VAGAO  SV ON (SV.SV_ID_SV = VG.SV_ID_SV)
                                                                                      INNER JOIN TRANSLOGIC.TIPO_VAGAO   TV ON (TV.TV_IDT_TVG = SV.TV_IDT_TVG)
                                                                                      INNER JOIN TRANSLOGIC.CLASSE_VAGAO CV ON (CV.CS_IDT_CVG = TV.CS_IDT_CVG)
                                                                                      INNER JOIN FOLHA_ESPECIF_VAGAO     FE ON (FE.FC_ID_FC = VG.FC_ID_FC)
                                                                                      LEFT JOIN NOTA_FISCAL              NF ON (NF.DP_ID_DP = DP.DP_ID_DP AND VG.VG_ID_VG = NF.VG_ID_VG)
                                                                                      LEFT JOIN VAGAO_PEDIDO           VPED ON (VG.VG_ID_VG = VPED.VG_ID_VG AND DP.DP_DT >= VPED.VB_DAT_CAR AND DP.DP_DT  <= NVL(VPED.VB_DAT_DSC, VPED.VB_DAT_CAR + 15))
                                                                                      LEFT JOIN T2_PEDIDO               PED ON (PED.W1_ID_PED = NVL(VPED.PX_IDT_PDR, VPED.PT_ID_ORT))
                                                                                      LEFT JOIN AREA_OPERACIONAL       OPED ON (OPED.AO_ID_AO = PED.W1_IDT_EST_ORI)
                                                                                      LEFT JOIN AREA_OPERACIONAL       DPED ON (DPED.AO_ID_AO = PED.W1_IDT_EST_DES)
                                                                                      LEFT JOIN UNID_PRODUCAO          UNID ON (UNID.UP_ID_UNP = OPED.UP_ID_UNP)
                                                                                      LEFT JOIN MUNICIPIO               MNO ON (OPED.MN_ID_MNC = MNO.MN_ID_MNC) 
                                                                                      LEFT JOIN ESTADO                  UFO ON (MNO.ES_ID_EST = UFO.ES_ID_EST)
                                                                                      LEFT JOIN MALHA              MALHAORI ON (MALHAORI.ML_CD_ML = UNID.UP_IND_MALHA)
                                                                                      LEFT JOIN MUNICIPIO               MND ON (OPED.MN_ID_MNC = MND.MN_ID_MNC) 
                                                                                      LEFT JOIN ESTADO                  UFD ON (MND.ES_ID_EST = UFD.ES_ID_EST)
                                                                                      LEFT JOIN MERCADORIA               ME ON (ME.MC_ID_MRC = PED.W1_IDT_MRC)
                                                                                      LEFT JOIN FLUXO_COMERCIAL          FC ON (FC.FX_ID_FLX = PED.W1_IDT_FLX)
                                                                                      LEFT JOIN CONTRATO                 CT ON (CT.CZ_ID_CTT = FC.CZ_ID_CTT)
                                                                                      LEFT JOIN EMPRESA               EPCLI ON (EPCLI.EP_ID_EMP = FC.EP_ID_EMP_COR)
                                                                                      LEFT JOIN EMPRESA               EPEXP ON (EPEXP.EP_ID_EMP = FC.EP_ID_EMP_REM)
                                                                                      LEFT JOIN EMPRESA               EPREC ON (EPREC.EP_ID_EMP = FC.EP_ID_EMP_DST)
                                                                                      LEFT JOIN AREA_OPERACIONAL       TDST ON (TDST.AO_ID_AO  = FC.AO_ID_TER_DS)
                                                                                      LEFT JOIN BO_BOLAR_VAGAO         BOVG ON (BOVG.DP_ID_DP = DP.DP_ID_DP)
                                                                                      LEFT JOIN BO_BOLAR_BO              BO ON (BO.ID_BO = BOVG.ID_BO_ID)
                                                                                      LEFT JOIN BO_BOLAR_NF            BONF ON (BONF.ID_VAGAO_ID = BOVG.ID_VAGAO)
                                                                                      LEFT JOIN BO_BOLAR_IMP_CFG        CFG ON (BO.AO_COD_AOP = CFG.CLIENTE)
                                                                                        WHERE DP.DP_DT >= TO_DATE('{DataInicialFiltro} 00:00:00', 'DD/MM/YYYY HH24:MI:SS')
                                                                                          AND DP.DP_DT  <= TO_DATE('{DataFinalFiltro} 23:59:59', 'DD/MM/YYYY HH24:MI:SS')
                                                                                          {Filtros}");

            hql = hql.Replace("{DataInicialFiltro}", dataInicio.ToString("dd/MM/yyyy"));
            hql = hql.Replace("{DataFinalFiltro}", dataFinal.ToString("dd/MM/yyyy"));

            // Filtros
            string filtros = "";
            //uf = uf.ToUpper();
            //Fluxo
            if (!string.IsNullOrEmpty(fluxo))
            {
                filtros += " AND FC.FX_COD_FLX = '" + fluxo.ToUpper() + "'";
            }

            //Origem
            if (!string.IsNullOrEmpty(origem))
            {
                filtros += " AND OPED.AO_COD_AOP = '" + origem.ToUpper() + "'";
            }
            //Destino
            if (!string.IsNullOrEmpty(destino))
            {
                filtros += " AND DPED.AO_COD_AOP  = '" + destino.ToUpper() + "'";
            }
            //Array Vagoes
            if (vagoes.Length > 0 && vagoes[0] != "")
            {
                filtros += " AND VG.VG_COD_VAG IN (";
                int i = 0;
                for (i = 0; i < vagoes.Length; i++)
                {
                    if (i == 0)
                    {
                        filtros += "'" + vagoes[i] + "'";
                    }
                    else
                    {
                        filtros += ",'" + vagoes[i] + "'";
                    }
                }
                filtros += ")";
            }
            ////Situacao
            //if(!string.IsNullOrEmpty(situacao))
            //{
            //    filtros += "AND = '" + situacao + "'";
            //}
            //Cliente
            if (!string.IsNullOrEmpty(cliente))
            {
                filtros += " AND CFG.CLIENTE_DSC = '" + cliente + "'";
            }
            //Expeditor
            if (!string.IsNullOrEmpty(expeditor))
            {
                filtros += " AND EPEXP.EP_DSC_RSM = '" + expeditor.ToUpper() + "'";
            }
            //Segmento
            if (!string.IsNullOrEmpty(segmento) && segmento.Trim() != "TODOS")
            {
                filtros += " AND FC.FX_COD_SEGMENTO = '" + segmento.ToUpper() + "'";
            }
            //Prefixo
            if (!string.IsNullOrEmpty(prefixo))
            {
                filtros += @" AND (SELECT LISTAGG(TR.TR_PFX_TRM )WITHIN GROUP(ORDER BY TR.TR_PFX_TRM)
                                                FROM COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT) = '" + prefixo.ToUpper() + "'";
            }
            //Os
            if (!string.IsNullOrEmpty(Os))
            {
                filtros += @" AND (SELECT LISTAGG(OS.X1_NRO_OS)WITHIN GROUP(ORDER BY OS.X1_NRO_OS)
                                                FROM   COMPVAGAO CVV 
                                                INNER JOIN COMPOSICAO CP ON (CVV.CP_ID_CPS = CP.CP_ID_CPS)
                                                INNER JOIN TREM TR ON (CP.TR_ID_TRM = TR.TR_ID_TRM)
                                                INNER JOIN T2_OS OS ON (TR.OF_ID_OSV = OS.X1_ID_OS)
                                                WHERE CVV.VG_ID_VG = VPED.VG_ID_VG
                                                AND   CVV.PT_ID_ORT = VPED.PT_ID_ORT)  = '" + Os + "'";
            }
            //Malha
            if(!string.IsNullOrEmpty(malha))
            {
                filtros += "AND MALHAORI.ML_DSC_ML = '" + malha.ToUpper() + "'";
            }
            //Uf
            if (!string.IsNullOrEmpty(uf) && uf.Trim() != "Todas")
            {
                filtros += " AND UFD.ES_SGL_EST = '" + uf.ToUpper() + "'";
            }
            ////estacao
            //if(!string.IsNullOrEmpty(estacao) && estacao.Trim() != "TODOS")
            //{
            //    filtros += "AND = '" + estacao + "'";
            //}

            //ORDER BY
            filtros += "ORDER BY DP.DP_DT ASC";

            //Verifica se existem filtros
            if (!string.IsNullOrEmpty(filtros))
            {
                hql.Replace("{Filtros}", filtros);
            }
            else
            {
                hql.Replace("{Filtros}", "");
            }

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(hql.ToString());

                query.SetResultTransformer(Transformers.AliasToBean<RelatorioFaturamentoExportDto>());

                return query.List<RelatorioFaturamentoExportDto>();
            }
        }

        /// <summary>
        /// Busca lista de Clientes
        /// </summary>
        /// <returns></returns>
        public IList<ClienteTipoIntegracaoDto> ObterClientes()
        {
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT  
                                            IMP.ID_BO_BOLAR_IMP_CFG as ""IdCliente"",
                                            IMP.CLIENTE_DSC AS ""Cliente""
                                                FROM BO_BOLAR_IMP_CFG IMP
                                                WHERE IMP.IC_ATIVO = 'S'
                                                ORDER BY IMP.CLIENTE");

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<ClienteTipoIntegracaoDto>());
                var result = query.List<ClienteTipoIntegracaoDto>();

                return result;
            }
        }
    }
}