﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Fornecedor
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.Fornecedor;
    using Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Extensions;


    public class FornecedorOsRepository : NHRepository<FornecedorOs, int>, IFornecedorOsRepository
    {
        public IList<FornecedorOsDto> ObterFornecedor(string local, EnumTipoFornecedor tipoFornecedor, bool todos)
        {
            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT F.ID_FORNECEDOR_OS as ""IdFornecedorOs"", 
                                        F.NOME_FORNECEDOR as ""Nome""
                                        FROM TB_FORNECEDOR_OS F
                                        JOIN TB_LOCAIS_FORNECEDOR L ON L.ID_FORNECEDOR = F.ID_FORNECEDOR_OS
                                        JOIN AREA_OPERACIONAL A ON A.AO_ID_AO = L.ID_AREA_OPERACIONAL
                                        WHERE 1=1  ");
            #endregion

            #region Where

            //Servico
            if (Convert.ToString(tipoFornecedor).Equals("LimpezaLavagem"))
            {
                sql.Append(" AND ID_TIPO_SERVICO IN (1,2)");
            }
            else
            {
                sql.Append(" AND ID_TIPO_SERVICO IN (3)");
            }

            if (!todos) 
            {
                if (!string.IsNullOrEmpty(local))
                    sql.Append(" AND A.AO_COD_AOP = '" + local.ToUpper() + "'");
                
                sql.Append(" AND F.ATIVO = 'S'");
            }
          
            sql.Append(" GROUP BY F.ID_FORNECEDOR_OS, F.NOME_FORNECEDOR");

            #endregion

            #region Conecta na base

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<FornecedorOsDto>());

                var result = query.List<FornecedorOsDto>();

                return result;
            }

            #endregion
        }

        /// <summary>
        /// Busca lista de Locais
        /// </summary>
        /// <returns></returns>
        public IList<OSLocalDto> ObterLocaisParaTipoServico()
        {
            #region SQL Query

            var sql = new StringBuilder(@"SELECT 
                          QRY.CODIGO AS ""IdLocal""
                          ,QRY.AO_COD_AOP AS ""Local""
                          FROM (
                          SELECT A.AO_ID_AO AS ""CODIGO"",
                                 A.AO_COD_AOP AS ""AO_COD_AOP""
                          FROM TB_LOCAIS_FORNECEDOR L
                          INNER JOIN TB_FORNECEDOR_OS FO ON FO.ID_FORNECEDOR_OS = L.ID_FORNECEDOR
                          JOIN AREA_OPERACIONAL A ON A.AO_ID_AO = L.ID_AREA_OPERACIONAL
                          WHERE L.ID_TIPO_SERVICO IN (3) 
                          AND FO.ATIVO = 'S'
                          GROUP BY A.AO_ID_AO, A.AO_COD_AOP
                          UNION
                          SELECT 0  AS CODIGO, 'Todos' AS AO_COD_AOP FROM DUAL) QRY ");                              

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                query.SetResultTransformer(Transformers.AliasToBean<OSLocalDto>());

                var result = query.List<OSLocalDto>();

                return result;
            }
        }
      

        /// <summary>
        /// Obtem fornecedor <see cref="FornecedorOsDto"/>
        /// </summary>
        /// <param name="nome">id do Fornecedor</param>
        /// <returns>Lista de <see cref="FornecedorOsDto"/></returns>
        public string ObterNomePorId(int? idFornecedor)
        {
            DetachedCriteria criteria = CriarCriteria()
                .Add(Restrictions.Eq("ID_FORNECEDOR", idFornecedor));

            var list = ObterTodos(criteria).Map<FornecedorOs, FornecedorOsDto>();

            if (list.Count > 0)
            {
                return list.Single().ToString();
            }
            return "";
        }

        /// <summary>
        /// Obtem fornecedor <see cref="FornecedorOsDto"/>
        /// </summary>
        /// <param name="nome">Nome do Fornecedor</param>
        /// <returns>retorna boolean <see cref="FornecedorOsDto"/></returns>
        /// 
        public bool ObterPorNome(string nomeFornecedor)
        {
            DetachedCriteria criteria = CriarCriteria()                
                .Add(Restrictions.Eq("Nome", nomeFornecedor).IgnoreCase());
            var  fornecedorOs = ObterTodos(criteria).FirstOrDefault();

            if (fornecedorOs != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public IList<FornecedorOs> ObterLocaisFornecedor(int idFornecedor)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("FornecedorOs.Id", idFornecedor));

            var lista = ObterTodos(criteria).ToList();

            return lista;
        }

        public ResultadoPaginado<FornecedorOsDto> ObterFornecedorOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idTipoServico, int? ativo)
        {
            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sql = new StringBuilder(@"SELECT   F.ID_FORNECEDOR_OS,
                                                   F.NOME_FORNECEDOR AS ""Nome"", 
                                                   XMLAGG(XMLELEMENT(E, AO_COD_AOP|| '('|| TS.DESC_SERVICO|| 
                                                   ') ')).EXTRACT('//text()') AS ""Local"", 
                                                   F.DT_REGISTRO AS ""DtRegistro"", 
                                                   F.LOGIN,
                                                   F.ATIVO
                                            FROM   TB_FORNECEDOR_OS F 
                                                   JOIN TB_LOCAIS_FORNECEDOR LF 
                                                     ON LF.ID_FORNECEDOR = F.ID_FORNECEDOR_OS 
                                                   JOIN TB_TIPOSERVICO TS 
                                                     ON LF.ID_TIPO_SERVICO = TS.ID_TIPO_SERVICO 
                                                   JOIN AREA_OPERACIONAL AO 
                                                     ON AO.AO_ID_AO = LF.ID_AREA_OPERACIONAL");

            #endregion

            #region Where
            var sqlWhere = new StringBuilder();

            if (!string.IsNullOrEmpty(nomeFornecedor))
            {
                sqlWhere.AppendFormat(" AND F.NOME_FORNECEDOR LIKE '%{0}%'", nomeFornecedor.ToString());
            }

            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND AO.AO_COD_AOP = '%{0}%'", local.ToUpper());
            }

            if (idTipoServico.HasValue)
            {
                sqlWhere.AppendFormat(" AND TS.ID_TIPO_SERVICO = {0}", idTipoServico);
            }

            if (ativo.HasValue)
            {
                sqlWhere.AppendFormat(" AND F.ATIVO ={0}", ativo);
            }

            if (sqlWhere.Length > 0)
            {
                string aux = " WHERE " + sqlWhere.ToString().Substring(5);
                sql.Append(aux);
            }

            #endregion

            #region OrderBy

            sql.Append(@" ORDER BY F.NOME_FORNECEDOR; ");

            #endregion

            #region Acessa a base
            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);
                }

                query.SetResultTransformer(Transformers.AliasToBean<FornecedorOsDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<FornecedorOsDto>();

                var result = new ResultadoPaginado<FornecedorOsDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
            #endregion
        }

        public ResultadoPaginado<LocalTipoServicoFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor)
        {
            #region SQL Query
            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT TS.DESC_SERVICO AS Servico , 
                                       AO_COD_AOP AS LocalEstacao
                                FROM   TB_LOCAIS_FORNECEDOR LF 
                                       JOIN TB_TIPOSERVICO TS 
                                         ON LF.ID_TIPO_SERVICO = TS.ID_TIPO_SERVICO 
                                       JOIN AREA_OPERACIONAL AO 
                                         ON AO.AO_ID_AO = LF.ID_AREA_OPERACIONAL 
                                WHERE  LF.ID_FORNECEDOR = {0}", idFornecedor);

            #endregion

            #region orderby
            sql.AppendFormat("AO.AO_COD_AOP ASC");

            #endregion

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sql.ToString().Replace("{ORDENACAO}", String.Empty)));

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                    p.Invoke(queryCount);

                }

                query.SetResultTransformer(Transformers.AliasToBean<LocalTipoServicoFornecedorDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    query.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    query.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var itens = query.List<LocalTipoServicoFornecedorDto>();

                var result = new ResultadoPaginado<LocalTipoServicoFornecedorDto>
                {
                    Items = itens,
                    Total = (long)total
                };

                return result;
            }
        }

        public ResultadoPaginado<FornecedorOsDto> ObterFornecedoresOsHabilitados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idServico, string Ativo)
        {
            #region SQL
            var parameters = new List<Action<IQuery>>();

            var sqlGeral = new StringBuilder(@"SELECT DISTINCT F.ID_FORNECEDOR_OS as ""IdFornecedorOS"", 
                                        (SELECT AO_COD_AOP FROM AREA_OPERACIONAL WHERE AO_ID_AO = L.ID_AREA_OPERACIONAL) as ""Local"", 
                                        (SELECT DESC_SERVICO FROM TB_TIPOSERVICO WHERE ID_TIPO_SERVICO = L.ID_TIPO_SERVICO) as ""Servico""
                                            FROM   TB_FORNECEDOR_OS F
                                                JOIN TB_LOCAIS_FORNECEDOR L ON F.ID_FORNECEDOR_OS = L.ID_FORNECEDOR");
            #region Where

            var sqlWhere = new StringBuilder();
            //Fornecedor
            if (!string.IsNullOrEmpty(nomeFornecedor))
            {
                sqlWhere.AppendFormat(" AND F.NOME_FORNECEDOR LIKE '%{0}%'", nomeFornecedor);
            }
            //Local
            if (!string.IsNullOrEmpty(local))
            {
                sqlWhere.AppendFormat(" AND (SELECT AO_COD_AOP FROM AREA_OPERACIONAL WHERE AO_ID_AO = L.ID_AREA_OPERACIONAL) LIKE '%{0}%'", local.ToUpper());
            }
            //Servico
            if (idServico != null && idServico > 0)
            {
                sqlWhere.AppendFormat(" AND L.ID_TIPO_SERVICO = {0}", idServico);
            }
            //Ativo
            if (Ativo != null)
            {
                sqlWhere.AppendFormat(" AND F.ATIVO = '{0}'", Ativo);
            }

            #endregion
            var sqlFornecedores = new StringBuilder();

            if (sqlWhere.Length > 0)
            {
                sqlFornecedores = new StringBuilder(@"SELECT F.ID_FORNECEDOR_OS as ""IdFornecedorOs"" , 
                                                             F.NOME_FORNECEDOR as ""Nome"",
                                                             F.DT_REGISTRO as ""DtRegistro"",
                                                                F.LOGIN as ""Login"" 
                                                                FROM   TB_FORNECEDOR_OS F
                                                                    JOIN TB_LOCAIS_FORNECEDOR L ON F.ID_FORNECEDOR_OS = L.ID_FORNECEDOR");
            }

            string aux = "";
            if (sqlWhere.Length > 0)
            {
                aux = " WHERE " + sqlWhere.ToString().Substring(5);

                sqlGeral.Append(aux);
            }

            sqlFornecedores.Append(aux + " GROUP BY F.ID_FORNECEDOR_OS, F.NOME_FORNECEDOR, F.DT_REGISTRO, F.LOGIN");
            
            #endregion
            #region Conecta na base

            using (var session = OpenSession())
            {
                //Busca todos registros TB_LOCAIS_FORNECEDOR
                var queryGeral = session.CreateSQLQuery(sqlGeral.ToString());
                //Busca todos registros(fornecedores(TB_LOCAIS_FORNECEDOR) retirando repetidos
                var queryFornecedores = session.CreateSQLQuery(sqlFornecedores.ToString());
                //Retorna quantidade registros
                var queryCount = session.CreateSQLQuery(String.Format(@"SELECT COUNT(1) FROM ({0})", sqlFornecedores.ToString().Replace("{ORDENACAO}", String.Empty)));


                foreach (var p in parameters)
                {
                    p.Invoke(queryGeral);
                    //p.Invoke(queryCount);
                }

                queryGeral.SetResultTransformer(Transformers.AliasToBean<LocalTipoServicoFornecedorDto>());
                queryFornecedores.SetResultTransformer(Transformers.AliasToBean<FornecedorOsDto>());

                if (detalhesPaginacaoWeb.Limite != 0)
                {
                    queryGeral.SetFirstResult(detalhesPaginacaoWeb.Inicio ?? 0);
                    queryGeral.SetMaxResults(detalhesPaginacaoWeb.Limite ?? DetalhesPaginacao.LimitePadrao);
                }

                var total = queryCount.UniqueResult<decimal>();
                var fornecedores = queryFornecedores.List<FornecedorOsDto>();
                var geral = queryGeral.List<LocalTipoServicoFornecedorDto>();

                string ConcaLocalServico = "";

                List<FornecedorOsDto> listPronta = new List<FornecedorOsDto>();

                for (int i = 0; i < fornecedores.Count; i++)
                {
                    //Armazena nome fornecedor
                    for (int x = 0; x < geral.Count; x++)
                    {
                        if (fornecedores[i].IdFornecedorOs == geral[x].IdFornecedorOS)
                        {
                            if (ConcaLocalServico == "")
                            {
                                ConcaLocalServico += geral[x].Local.ToString() + " (" + geral[x].Servico + ")";
                            }
                            else
                            {
                                ConcaLocalServico += ", " + geral[x].Local.ToString() + " (" + geral[x].Servico + ")";
                            }
                        }
                    }
                    //Adiciona na lista do objeto FornecedorOsDto
                    listPronta.Add(new FornecedorOsDto()
                    {
                        IdFornecedorOs = fornecedores[i].IdFornecedorOs,
                        Nome = fornecedores[i].Nome,
                        ConcaLocalServico = ConcaLocalServico,
                        DtRegistro = fornecedores[i].DtRegistro,
                        Login = fornecedores[i].Login
                    });
                    //Limpa Variavel

                    ConcaLocalServico = "";
                }

                var result = new ResultadoPaginado<FornecedorOsDto>
                {
                    Items = listPronta,
                    Total = (long)total
                };

                return result;
            }

            #endregion
        }        

        public bool ExisteFornecedorOsPorNomeId(string nomeFornecedor, int idFornecedorOs)
        {
            #region SQL Query
            var parameters = new List<Action<IQuery>>();
            var sql = new StringBuilder();
            sql.AppendFormat(@"SELECT NOME_FORNECEDOR as ""Nome""
                               FROM TB_FORNECEDOR_OS F 
                               WHERE UPPER(F.NOME_FORNECEDOR )= UPPER('{0}') 
                                     AND ID_FORNECEDOR_OS <>{1}", nomeFornecedor, idFornecedorOs);
            #endregion

            #region conectabase

            using (var session = OpenSession())
            {
                var query = session.CreateSQLQuery(sql.ToString());

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.AliasToBean<FornecedorOs>());
                var fornecedorOs = query.UniqueResult<FornecedorOs>();

                if (fornecedorOs != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            #endregion
        }
    }
}