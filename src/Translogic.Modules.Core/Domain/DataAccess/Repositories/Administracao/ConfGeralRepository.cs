﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Administracao
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using NHibernate.Criterion;
    using Translogic.Core.Infrastructure.Web;
    using Model.Administracao;
    using Model.Diversos.Repositories;

    public class ConfGeralRepository : NHRepository<ConfGeral, string>, IConfGeralRepository
    {
        public ResultadoPaginado<ConfGeral> ObterTodosPaginado(DetalhesPaginacaoWeb pagination, string chave)
        {
            using (var session = OpenSession())
            {
                var criteria = CriarCriteria();

                if (!string.IsNullOrEmpty(chave))
                {
                    criteria.Add(Restrictions.Like("Id", chave, MatchMode.Anywhere));
                }

                var configuracoes = ObterPaginado(criteria, pagination);
                return configuracoes;
            }
        }
    }
}