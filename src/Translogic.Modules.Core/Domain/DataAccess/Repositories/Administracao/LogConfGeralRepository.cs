﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Administracao
{
    using System;
    using ALL.Core.AcessoDados;
    using Model.Administracao;
    using Model.Diversos.Repositories;

    public class LogConfGeralRepository : NHRepository<LogConfGeral, Int32>, ILogConfGeralRepository
    {
    }
}