namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.OracleClient;
    using ALL.Core.AcessoDados;
    using ALL.Core.Extensions;
    using Interfaces.Acesso;
    using Model.Acesso;
    using Model.Acesso.Repositories;
    using NHibernate;
    using NHibernate.Criterion;
    using Yahoo.Yui.Compressor;

    /// <summary>
    /// Implementa��o de reposit�rio de Usuario com NH
    /// </summary>
    public class UsuarioRepository : NHRepository<Usuario, int?>, IUsuarioRepository
    {
        #region IUsuarioRepository MEMBROS

        /// <summary>
        /// Obt�m usu�rio pelo c�digo
        /// </summary>
        /// <param name="codigo"> Codigo do usu�rio. </param>
        /// <returns>
        /// Objeto <see cref="Usuario"/>
        /// </returns>
        public Usuario ObterPorCodigo(string codigo)
        {
            return ObterPrimeiro(Restrictions.Eq("Codigo", codigo));
        }

        #endregion

        /// <summary>
        /// Obtem todos os <see cref="UsuarioDto"/>
        /// </summary>
        /// <param name="nome">Nome do Usuario</param>
        /// <returns>Lista de <see cref="UsuarioDto"/></returns>
        public IList<UsuarioDto> ObterPorNome(string nome)
        {
            DetachedCriteria criteria = CriarCriteria()
                .Add(Restrictions.Eq("Ativo", true))
                .Add(Restrictions.InsensitiveLike("Nome", nome, MatchMode.Anywhere));

            return ObterTodos(criteria).Map<Usuario, UsuarioDto>();
        }

        /// <summary>
        /// Obt�m a lista de permiss�es da transa��o
        /// </summary>
        /// <param name="transacao"> Transa��o a ser verificada a permiss�o. </param>
        /// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
        /// <returns> Lista de permiss�es </returns>
        public IList ObterPermissoes(string transacao, Usuario usuario)
        {
            ISession session = OpenSession();
            ISQLQuery query = session.CreateSQLQuery(ObterQueryPermissoes(transacao, string.Empty, usuario));
            return query.List();
        }

        /// <summary>
        /// Obt�m a lista de permiss�es da transa��o
        /// </summary>
        /// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
        /// <returns> Lista ultima data de acesso </returns>
        public DateTime? ObterDataUltimoAcessoSoxs(Usuario usuario)
        {
            ISession session = OpenSession();
            string SQL = @"SELECT LU.LU_TIMESTAMP
                           FROM  (
                                   SELECT MAX(LU1.LU_IDT_LU) ID
                                   FROM   LOG_USUARIO LU1
                                   WHERE  LU1.LU_IDT_LU <(SELECT MAX(lu.lu_idt_lu) 
                                                          FROM  LOG_USUARIO LU, 
                                                                EVENTO_USUARIO EU
                                                          WHERE LU.US_IDT_USU={0}
                                                          AND   LU.EU_IDT_EU = EU.EU_IDT_EU
                                                          AND   EU.EU_COD_EU='LES'
                                                          AND   LU.LU_IDT_LU < (
                                                                                SELECT MAX(LU1.LU_IDT_LU) 
                                                                                FROM LOG_USUARIO LU1 
                                                                                WHERE LU1.US_IDT_USU ={0}
                                                                                )
                                                         )
                                   AND   LU1.US_IDT_USU={0}                      
                                  )TAB,
                                  LOG_USUARIO LU
                            WHERE LU.LU_IDT_LU=TAB.ID
                            AND   LU.US_IDT_USU={0}";

            ISQLQuery query = session.CreateSQLQuery(string.Format(SQL, usuario.Id));
            return query.UniqueResult<DateTime?>();
        }

        /// <summary>
        /// Obt�m a quantidade de dias para expirar a senha, se vier negativo j� expirou
        /// </summary>
        /// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
        /// <returns> Lista ultima data de acesso </returns>
        public int ObterDiasAlterar2D(Usuario usuario)
        {
            ISession session = OpenSession();
            string SQL = @"SELECT TRUNC(U.US_DAT_SEN+60)-TRUNC(SYSDATE)
                            FROM  USUARIO U 
                            WHERE U.US_IDT_USU={0}";

            ISQLQuery query = session.CreateSQLQuery(string.Format(SQL, usuario.Id));
            return query.UniqueResult<int>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fluxo"></param>
        /// <returns></returns>
        public bool VerificarUltimas5Senhas(Usuario usuario, String senhaNova)
        {
            int numSenhas;
            String resultadoQuery = "N";
            bool retorno = false;
            using (ISession session = OpenSession())
            {
                string SQL = @"SELECT COUNT(*)  as Num
                           FROM   USUARIO_SENHA_HIST H
                           WHERE  H.US_IDT_USU = {0}";

                ISQLQuery query = session.CreateSQLQuery(string.Format(SQL, usuario.Id));
                var numSenhas1 = query.UniqueResult();
                var lixo = numSenhas1.ToString();
                numSenhas = int.Parse(lixo);
                //numSenhas = 0;
                if (numSenhas >= 5)
                {
                    SQL = @"SELECT DISTINCT 'S'
                             FROM (
                                    SELECT TAB1.*, ROWNUM AS LINHA
                                    FROM (
                                            SELECT H.*
                                            from USUARIO_SENHA_HIST H
                                            WHERE H.US_IDT_USU = :usuario
                                            ORDER BY H.US_HIST_TIMESTAMP
                                    )tab1
                                  )tab
                            WHERE tab.linha >= (:numSenhas - 5)
                            AND TAB.US_SEN_USU = :novaSenha";
                    //ISQLQuery query1 = session.CreateSQLQuery(string.Format(SQL, usuario.Id,numSenhas,senhaNova));
                    ISQLQuery query1 = session.CreateSQLQuery(SQL);
                    query1.SetParameter("usuario", usuario.Id);
                    query1.SetParameter("numSenhas", numSenhas);
                    query1.SetParameter("novaSenha", senhaNova);

                    resultadoQuery = query1.UniqueResult<String>();
                    if (resultadoQuery.IsNullOrEmpty())
                    {
                        retorno = false;
                    }
                    else
                    {
                        retorno = resultadoQuery.Equals("S");
                    }
                }
                else
                {
                    SQL = @" SELECT DISTINCT 'S'
                              FROM (
                                      SELECT TAB1.*,ROWNUM AS LINHA
                                      FROM (

                                            SELECT H.*
                                            from USUARIO_SENHA_HIST H
                                            WHERE H.US_IDT_USU=:usuario
                                            ORDER BY H.US_HIST_TIMESTAMP
                                      )tab1
                                   )tab
                               WHERE tab.linha<=:numSenhas
                               AND TAB.US_SEN_USU =:novaSenha";
                    ISQLQuery query2 = session.CreateSQLQuery(SQL);
                    //ISQLQuery query2 = session.CreateSQLQuery(string.Format(SQL, usuario.Id, numSenhas, senhaNova));

                    query2.SetParameter("usuario", usuario.Id);
                    query2.SetParameter("numSenhas", numSenhas);
                    query2.SetParameter("novaSenha", senhaNova);
                    resultadoQuery = query2.UniqueResult<String>();
                    if (resultadoQuery.IsNullOrEmpty())
                    {
                        retorno = false;
                    }
                    else
                    {
                        retorno = resultadoQuery.Equals("S");
                    }
                }
            }

            return retorno;
        }

        /// <summary>
        /// Obt�m a lista de permiss�es da transa��o
        /// </summary>
        /// <param name="transacao"> Transa��o que a a��o pertence. </param>
        /// <param name="acao"> A��o a ser verificada a permissao. </param>
        /// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
        /// <returns> Lista de permiss�es </returns>
        public IList ObterPermissoes(string transacao, string acao, Usuario usuario)
        {
            ISession session = OpenSession();
            ISQLQuery query = session.CreateSQLQuery(ObterQueryPermissoes(transacao, acao, usuario));
            return query.List();
        }

        private static string ObterQueryPermissoes(string transacao, string acao, Usuario usuario)
        {
            string strSql = @"
							SELECT T.TS_COD_TRS AS TRANSACAO,
							  JOIN(CURSOR (SELECT TA.TT_COD_TT
											 FROM TRANSACAO_ACAO TA
											WHERE TA.TS_IDT_TRS = T.TS_IDT_TRS
											  {2}
											  AND EXISTS
											(SELECT 1
													 FROM USUARIO             U,
														  TRANSACAO_USUR      TU,
														  TRANSACAO_ACAO_USUR TAU
													WHERE U.US_USR_IDU = '{0}'
													  AND TU.US_IDT_USU = U.US_IDT_USU
													  AND TU.TS_IDT_TRS = T.TS_IDT_TRS
													  AND TAU.TT_IDT_TT = TA.TT_IDT_TT
													  AND TAU.US_IDT_USU = U.US_IDT_USU
													  AND TAU.TA_IND_ACESSO = 'S'
													  AND TAU.TA_IND_BLOQ = 'N'
												   UNION
												   SELECT 1
													 FROM USUARIO              U,
														  AGRUPAM_USUARIO      AG,
														  TRANSACAO_ACAO_GRUPO TAG
													WHERE U.US_USR_IDU = '{0}'
													  AND U.US_IDT_USU = AG.US_IDT_USU
													  AND TAG.GR_IDT_GPU = AG.GR_IDT_GPU
													  AND TAG.TS_IDT_TRS = T.TS_IDT_TRS
													  AND TAG.TT_IDT_TT = TA.TT_IDT_TT
													  AND TAG.TC_IND_ACESSO = 'S'
													  AND TAG.TC_IND_BLOQ = 'N'
												   UNION
												   SELECT 1
													 FROM USUARIO U, GRUPO_USUARIO GU, AGRUPAM_USUARIO AG
													WHERE U.US_USR_IDU = '{0}'
													  AND GU.GR_COD_GRP = 'MASTE'
													  AND AG.GR_IDT_GPU = GU.GR_IDT_GPU
													  AND AG.US_IDT_USU = U.US_IDT_USU
												   UNION
												   SELECT 1 FROM DUAL WHERE T.TS_IND_PUBLICA = 'S')
											ORDER BY TA.TT_COD_TT), ',') AS ACOES
							  FROM TRANSACAO T
							 WHERE UPPER(T.TS_COD_TRS) = '{1}'
							   AND T.TS_IND_ACESSO = 'S'
							   AND T.TS_IND_BLOQ = 'N'
							   AND EXISTS
							 (SELECT 1
									  FROM TRANSACAO_USUR TU, USUARIO U
									 WHERE U.US_USR_IDU = '{0}'
									   AND U.US_IDT_USU = TU.US_IDT_USU
									   AND TU.TS_IDT_TRS = T.TS_IDT_TRS
									   AND TU.UG_IND_ACESSO = 'S'
									   AND TU.UG_IND_BLOQ = 'N'
									UNION
									SELECT 1
									  FROM USUARIO U, AGRUPAM_USUARIO AG, TRANSACAO_GRUPO TG
									 WHERE U.US_USR_IDU = '{0}'
									   AND U.US_IDT_USU = AG.US_IDT_USU
									   AND TG.GR_IDT_GPU = AG.GR_IDT_GPU
									   AND TG.TS_IDT_TRS = T.TS_IDT_TRS
									   AND TG.TG_IND_ACESSO = 'S'
									   AND TG.TG_IND_BLOQ = 'N'
									UNION
									SELECT 1
									  FROM USUARIO U, GRUPO_USUARIO GU, AGRUPAM_USUARIO AG
									 WHERE U.US_USR_IDU = '{0}'
									   AND GU.GR_COD_GRP = 'MASTE'
									   AND AG.GR_IDT_GPU = GU.GR_IDT_GPU
									   AND AG.US_IDT_USU = U.US_IDT_USU
									UNION
									SELECT 1 FROM DUAL WHERE T.TS_IND_PUBLICA = 'S')";

            if (string.IsNullOrEmpty(acao))
            {
                return string.Format(strSql, usuario.Codigo, transacao.ToUpper(), string.Empty);
            }

            return string.Format(strSql, usuario.Codigo, transacao.ToUpper(), string.Format(" AND UPPER(TA.TT_COD_TT)='{0}'", acao.ToUpper()));
        }

        private void AddParameter(OracleCommand command, string nome, OracleType tipo, object valor, ParameterDirection direcao)
        {
            OracleParameter parameter = new OracleParameter();
            parameter.ParameterName = nome;
            parameter.OracleType = tipo;
            parameter.Direction = direcao;
            parameter.Value = DBNull.Value;
            if (valor != null)
            {
                parameter.Value = valor;
            }

            command.Parameters.Add(parameter);
        }
    }
}