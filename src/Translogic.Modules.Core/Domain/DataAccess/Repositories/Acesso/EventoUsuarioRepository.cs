namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using System.Collections.Generic;
	using System.Linq;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de reposit�rio de evento de usuario com NH
	/// </summary>
	public class EventoUsuarioRepository : NHRepository<EventoUsuario, int>, IEventoUsuarioRepository
	{
		#region IEventoUsuarioRepository MEMBROS
		
		/// <summary>
		/// Obt�m Evento pelo c�digo
		/// </summary>
		/// <param name="codigo">C�digo do Evento</param>
		/// <returns>Evento do Usuario - <see cref="EventoUsuario"/></returns>
		public EventoUsuario ObterPorCodigo(EventoUsuarioCodigo codigo)
		{
			DetachedCriteria criteria = CriarCriteria()
				.SetCacheable(true);

			IList<EventoUsuario> eventosCache = ObterTodos(criteria);

			return eventosCache.Where(evento => evento.Codigo == codigo).First();
		}

		#endregion
	}
}