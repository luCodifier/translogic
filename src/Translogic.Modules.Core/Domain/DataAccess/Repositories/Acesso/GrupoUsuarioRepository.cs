namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de reposit�rio de Grupo de Usu�rio com NH
	/// </summary>
	public class GrupoUsuarioRepository : NHRepository<GrupoUsuario, int>, IGrupoUsuarioRepository
	{
		#region M�TODOS
		/// <summary>
		/// Obt�m grupos com paginados, com par�metro de c�digo
		/// </summary>
		/// <param name="paginacao">Detalhes da pagina��o - <see cref="DetalhesPaginacao"/></param>
		/// <param name="codigo">C�digo do grupo</param>
		/// <returns>Resultado Paginado - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		public ResultadoPaginado<GrupoUsuario> ObterPaginadoComCodigoIniciando(DetalhesPaginacao paginacao, string codigo)
		{
			if (string.IsNullOrEmpty(codigo))
			{
				return ObterTodosPaginado(paginacao);
			}

			DetachedCriteria criteria = CriarCriteria().
				Add(Restrictions.Like("Codigo", codigo, MatchMode.Start));

			return ObterPaginado(criteria, paginacao);
		}

		/// <summary>
		/// Obt�m grupo com par�metro de c�digo
		/// </summary>
		/// <param name="codigo">C�digo do grupo</param>
		/// <returns>Resultado Paginado - <see cref="GrupoUsuario"/></returns>
		public GrupoUsuario ObterPorCodigo(string codigo)
		{
			DetachedCriteria criteria = CriarCriteria().
				Add(Restrictions.Eq("Codigo", codigo));

			return ObterPrimeiro(criteria);
		}

		/*
		/// <summary>
		/// Obt�m todos os grupos de usu�rios paginados
		/// </summary>
		/// <param name="paginacao">Detalhes da Pagina��o - <see cref="DetalhesPaginacao"/></param>
		/// <param name="filterDetails">Detalhes do Filtro - <see cref="DetalhesFiltro"/></param>
		/// <returns>Resultado Paginado - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		public ResultadoPaginado<GrupoUsuario> ObterTodosPaginado(DetalhesPaginacao paginacao, DetalhesFiltro[] filterDetails)
		{
			if (filterDetails == null)
			{
				return ObterTodosPaginado(paginacao);
			}

			DetachedCriteria criteria = CriarCriteria();

			foreach (var filterDetail in filterDetails)
			{
				switch (filterDetail.FormaPesquisa)
				{
					case FormaPesquisaFiltro.Start:
						criteria.Add(Restrictions.Like(filterDetail.Campo, filterDetail.Valor, MatchMode.Start));
						break;
					case FormaPesquisaFiltro.End:
						criteria.Add(Restrictions.Like(filterDetail.Campo, filterDetail.Valor, MatchMode.End));
						break;
					case FormaPesquisaFiltro.Anywhere:
						criteria.Add(Restrictions.Like(filterDetail.Campo, filterDetail.Valor, MatchMode.Anywhere));
						break;
					case FormaPesquisaFiltro.Equals:
						criteria.Add(Restrictions.Eq(filterDetail.Campo, filterDetail.Valor));
						break;
					case FormaPesquisaFiltro.NotEquals:
						criteria.Add(Restrictions.Not(Restrictions.Eq(filterDetail.Campo, filterDetail.Valor)));
						break;
				}
			}

			return ObterPaginado(criteria, paginacao);
		}*/

		#endregion
	}
}