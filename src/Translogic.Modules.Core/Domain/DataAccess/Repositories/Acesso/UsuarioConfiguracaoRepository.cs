﻿namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

	using NHibernate.Criterion;

	using Translogic.Modules.Core.Domain.Model.Acesso;
	using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;

	/// <summary>
	/// Implementação de repositorio de configuração do usuario
	/// </summary>
	public class UsuarioConfiguracaoRepository : NHRepository<UsuarioConfiguracao, int>, IUsuarioConfiguracaoRepository
	{
		/// <summary>
		/// Obtém configurações do usuario
		/// </summary>
		/// <param name="usuario">Usuario a ser obtida as configurações</param>
		/// <returns>Lista de configurações</returns>
		public IList<UsuarioConfiguracao> ObterPorUsuario(Usuario usuario)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Usuario", usuario));
			return this.ObterTodos(criteria);
		}
	}
}