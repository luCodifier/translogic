namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de reposit�rio de Agrupamentode Usu�rio
	/// </summary>
	public class AgrupamentoUsuarioRepository : NHRepository<AgrupamentoUsuario, int>, IAgrupamentoUsuarioRepository
	{
		#region M�TODOS

        /// <summary>
        /// Retorna os grupos do usu�rio pelo c�digo do usu�rio.
        /// </summary>
        /// <param name="usuario">Usu�rio do grupo</param>
        /// <returns>Lista de Grupos Paginados - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
        public IList<GrupoUsuario> ObterGruposPorUsuario(Usuario usuario)
        {
            DetachedCriteria criteria = CriarCriteria();
            criteria.Add(Restrictions.Eq("Usuario", usuario));

            IList<AgrupamentoUsuario> lista = ObterTodos(criteria);

            return lista.Select(item => new GrupoUsuario
                                            {
                                                Id = item.Grupo.Id,
                                                Codigo = item.Grupo.Codigo,
                                                Empresa = item.Grupo.Empresa,
                                                Descricao = item.Grupo.Descricao,
                                                Permissoes = item.Grupo.Permissoes,
                                                Restrito = item.Grupo.Restrito,
                                                VersionDate = item.Grupo.VersionDate
                                            }).ToList();
        }

		/// <summary>
		/// Retorna os grupos do usu�rio pelo c�digo do usu�rio.
		/// </summary>
		/// <param name="usuario">Usu�rio do grupo</param>
		/// <param name="grupoUsuario">Grupo do usu�rio.</param>
		/// <returns>Lista de Grupos Paginados - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		public bool VerificarUsuarioGrupo(Usuario usuario, GrupoUsuario grupoUsuario)
		{
			DetachedCriteria criteria = CriarCriteria();
			criteria.Add(Restrictions.Eq("Usuario", usuario));
			criteria.Add(Restrictions.Eq("Grupo", grupoUsuario));

			AgrupamentoUsuario agrupamentoUsuario = ObterPrimeiro(criteria);
			return agrupamentoUsuario != null;
		}

		#endregion
	}
}