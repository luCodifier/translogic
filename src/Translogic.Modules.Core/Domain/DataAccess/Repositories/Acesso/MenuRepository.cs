namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using System.Collections.Generic;
	using System.Threading;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.SqlCommand;
	using NHibernate.Transform;

	/// <summary>
	/// Implementa��o de reposit�rio do menu
	/// </summary>
	public class MenuRepository : NHRepository<Menu, int>, IMenuRepository
	{
		#region IMenuRepository MEMBROS
		/// <summary>
		/// Obt�m os itens de menu ativos e seta no cache
		/// </summary>
		/// <returns>Lista de itens de menu</returns>
		public IList<Menu> ObterItensAtivos()
		{
			using (ISession session = OpenSession())
			{
				// session.EnableFilter("Culture").SetParameter("CultureName", Thread.CurrentThread.CurrentUICulture.Name);

				DetachedCriteria criteria = CriarCriteria()
					.CreateAlias("I18N", "i", JoinType.LeftOuterJoin)
					.Add(Restrictions.IsNull("i.Cultura") || Restrictions.Eq("i.Cultura", Thread.CurrentThread.CurrentUICulture.Name))
					.Add(Restrictions.Eq("Ativo", true))
					.SetCacheable(true)
					.SetCacheRegion("MENU_ATIVOS")
					.AddOrder(Order.Asc("Ordem"));
				criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

				IList<Menu> menus = ObterTodos(criteria);

				// session.DisableFilter("Culture");

				return menus;
			}
		}

        /// <summary>
        /// Obt�m os itens de menu ativos e seta no cache
        /// </summary>
        /// <param name="codUsuario">C�digo do usuario</param>
        /// <returns>Lista de itens de menu</returns>
        public IList<Menu> ObterItensAtivos(string codUsuario)
        {
            using (ISession session = OpenSession())
            {
                try
                {
                    session.EnableFilter("Autorizar").SetParameter("CodigoUsuario", codUsuario);

                    DetachedCriteria criteria = CriarCriteria()
                        .CreateAlias("I18N", "i", JoinType.LeftOuterJoin)
                        .Add(Restrictions.IsNull("i.Cultura") || Restrictions.Eq("i.Cultura", Thread.CurrentThread.CurrentUICulture.Name))
                        .Add(Restrictions.Eq("Ativo", true))
                        // .SetCacheable(true)
                        // .SetCacheRegion("MENU_ATIVOS")
                        .AddOrder(Order.Asc("Ordem"));
                    criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

                    IList<Menu> menus = ObterTodos(criteria);   

                    return menus;
                }
                finally
                {
                    session.DisableFilter("Autorizar");
                }
            }
        }

		#endregion
	}
}