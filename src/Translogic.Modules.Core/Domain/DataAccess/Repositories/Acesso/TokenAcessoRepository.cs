namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o do reposit�rio de Token de Acesso com NH
	/// </summary>
	public class TokenAcessoRepository : NHRepository<TokenAcesso, int>, ITokenAcessoRepository
	{
		#region ITokenAcessoRepository MEMBROS

		/// <summary>
		/// Obt�m Token dado um usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio a ser buscado o Token</param>
		/// <returns>Token de Acesso</returns>
		public TokenAcesso ObterPorUsuario(Usuario usuario)
		{
			DetachedCriteria criteria = CriarCriteria().Add(Restrictions.Eq("Usuario", usuario));
			return ObterPrimeiro(criteria);
		}

		#endregion
	}
}