namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections.Generic;
	using Adaptations;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o de resposit�rio de log de usuario com NH
	/// </summary>
	public class LogUsuarioRepository : NHRepository<LogUsuario, int>, ILogUsuarioRepository
	{
		#region ILogUsuarioRepository MEMBROS

		/// <summary>
		/// Obt�m Logs por usuario dado um per�odo
		/// </summary>
		/// <param name="usuario">Usu�rio a ser buscado os logs</param>
		/// <param name="inicial">Data Inicial</param>
		/// <param name="final">Data final</param>
		/// <returns>Lista de Logs do Usuario</returns>
		public IList<LogUsuario> ObterLogsPorUsuarioNoPeriodo(Usuario usuario, DateTime inicial, DateTime final)
		{
			DetachedCriteria criteria = DetachedCriteria.For<LogUsuario>().
				Add(Restrictions.Eq("Usuario", usuario) &&
					Restrictions.Between("AconteceuEm", inicial, final)).
				AddOrder(Order.Desc("AconteceuEm"));

			return ObterTodos(criteria);
		}

		/// <summary>
		/// Obt�m o �ltimo log do usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio a ser buscado o Log</param>
		/// <returns>�ltimo Log do Usu�rio</returns>
		public LogUsuario ObterUltimoLogPorUsuario(Usuario usuario)
		{
			DetachedCriteria criteria = DetachedCriteria.For<LogUsuarioVigente>().
				Add(Restrictions.Eq("Usuario", usuario));

			return ObterPrimeiro(criteria);
		}

		#endregion
	}
}