namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading;
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;
	using Model.IntegracaoSIV;
	using NHibernate.Criterion;

	/// <summary>
	/// Implementa��o da Interface de resposit�rio de MeuTranslogic
	/// </summary>
	public class MeuTranslogicRepository : NHRepository<MeuTranslogic, int>, IMeuTranslogicRepository
	{
		#region IMeuTranslogicRepository MEMBROS
		
		/// <summary>
		/// Obtem o Meu Translogic do usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio dono do Meu Translogic</param>
		/// <returns>Itens do Meu Translogic</returns>
		public IList<MeuTranslogic> ObterPorUsuario(Usuario usuario)
		{
			using (var session = OpenSession())
			{
				session.EnableFilter("Culture").SetParameter("CultureName", Thread.CurrentThread.CurrentUICulture.Name);
				
				var itens = ObterTodos(CriarCriteria().Add(
									Restrictions.Eq("Usuario", usuario)
									));

				session.DisableFilter("Culture");

                var itensSiv = ObterTodos(CriarCriteria().Add(
									Restrictions.Eq("Usuario", usuario)
                                    ).Add(Restrictions.IsNotNull("ItemMenuSiv")));

			    foreach (MeuTranslogic meuTranslogic in itensSiv)
			    {
                    itens.Add(meuTranslogic);
			    }

				return itens;
			}
		}

		/// <summary>
		/// Verifica se j� existe o menu cadastrado para o usu�rio naquele n�vel
		/// </summary>
		/// <param name="usuario">Dono do Meu Translogic</param>
		/// <param name="menu">Menu a ser verificado</param>
		/// <param name="pai">Menu Pai - <see cref="Menu"/></param>
		/// <returns>Se j� existe o menu</returns>
		public bool JaExiste(Usuario usuario, Menu menu, Menu pai)
		{
			var criteria = CriarCriteria().
				Add(Restrictions.Eq("Usuario", usuario)).
				Add(Restrictions.Eq("Menu", menu));

			if (pai != null)
			{
				criteria.Add(Restrictions.Eq("Pai", pai));
			}

			return Existe(criteria);
		}

        /// <summary>
        /// Verifica se j� existe o menu SIV cadastrado para o usu�rio no root(sem n�vel)
        /// </summary>
        /// <param name="usuario">Dono do Meu Translogic</param>
        /// <param name="itemMenuSiv">Menu SIV a ser verificado</param>
        /// <returns>Se j� existe o menu</returns>
        public bool JaExiste(Usuario usuario, ItemMenuSiv itemMenuSiv)
        {
            var criteria = CriarCriteria().
                Add(Restrictions.Eq("Usuario", usuario)).
                Add(Restrictions.Eq("ItemMenuSiv", itemMenuSiv));

            return Existe(criteria);
        }

		/// <summary>
		/// Verifica se j� existe o menu cadastrado para o usu�rio no root(sem n�vel)
		/// </summary>
		/// <param name="usuario">Dono do Meu Translogic</param>
		/// <param name="menu">Menu a ser verificado</param>
		/// <returns>Se j� existe o menu</returns>
		public bool JaExiste(Usuario usuario, Menu menu)
		{
			return JaExiste(usuario, menu, null);
		}

		/// <summary>
		/// Obtem a �ltima ordem
		/// </summary>
		/// <param name="usuario"> Dono do MeuTranslogic </param>
		/// <param name="pai"> Qual o pai a ser considerado </param>
		/// <returns> �ltima ordem.</returns>
		public int ObterUltimaOrdem(Usuario usuario, Menu pai)
		{
			var criteria = CriarCriteria().
				Add(Restrictions.Eq("Usuario", usuario)).
				AddOrder(Order.Desc("Ordem"))/*.
				SetProjection(Projections.Property("Ordem")).
				SetMaxResults(1)*/;

			if (pai != null)
			{
				criteria.Add(Restrictions.Eq("Pai", pai));
			}

			// IList<int> ints = ExecutarCriteria<int>(criteria);
			// return ints.Sum();
			MeuTranslogic ultimo = ObterPrimeiro(criteria);

			if (ultimo == null)
			{
				return 0;
			}
			
			return ultimo.Ordem;
		}

		#endregion
	}
}