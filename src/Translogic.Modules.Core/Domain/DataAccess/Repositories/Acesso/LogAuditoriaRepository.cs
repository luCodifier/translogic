namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;

	/// <summary>
	/// Implementação de Repositório de log de auditoria com NH
	/// </summary>
	public class LogAuditoriaRepository : NHRepository<LogAuditoria, int>, ILogAuditoriaRepository
	{
	}
}