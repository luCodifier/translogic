namespace Translogic.Modules.Core.Domain.DataAccess.Repositories.Acesso
{
	using ALL.Core.AcessoDados;
	using Model.Acesso;
	using Model.Acesso.Repositories;

	/// <summary>
	/// Implementação de persistência de repositório com NH da classe <see cref="SessaoExpirada"/>
	/// </summary>
	public class SessaoExpiradaRepository : NHRepository<SessaoExpirada, int>, ISessaoExpiradaRepository
	{
	}
}