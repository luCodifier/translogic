namespace Translogic.Modules.Core.Domain.DataAccess.Listeners
{
	using System;
	using Castle.Core.Logging;
	using Microsoft.Practices.ServiceLocation;
	using Model.Acesso;
	using NHibernate.Event;
	using Services.Acesso;

	/// <summary>
	/// Listener de auditoria de objetos persistidos
	/// </summary>
	public class AuditoriaListener : IPostInsertEventListener, IPostUpdateEventListener, IPostDeleteEventListener
	{
		#region ATRIBUTOS READONLY & EST�TICOS
		private static AuditoriaService _auditoriaService;
		#endregion

		#region ATRIBUTOS

		private ILogger _logger = NullLogger.Instance;

		#endregion

		#region PROPRIEDADES
		/// <summary>
		/// Servi�o de auditoria
		/// </summary>
		public AuditoriaService AuditoriaService
		{
			get
			{
				if (_auditoriaService == null)
				{
					_auditoriaService = ServiceLocator.Current.GetInstance<AuditoriaService>();
				}

				return _auditoriaService;
			}
		}

		/// <summary>
		/// Inst�ncia do Logger
		/// </summary>
		public ILogger Logger
		{
			get { return _logger; }
			set { _logger = value; }
		}

		#endregion

		#region IPostDeleteEventListener MEMBROS
		/// <summary>
		/// Evento de exclus�o de um objeto auditado
		/// </summary>
		/// <param name="event">Evento de exclus�o</param>
		public void OnPostDelete(PostDeleteEvent @event)
		{
			if (!(@event.Entity is IAuditoria))
			{
				return;
			}

			try
			{
				AuditoriaService.AuditarExclusao(@event.Entity, @event.Id, @event.Persister.PropertyNames, @event.DeletedState);
			}
			catch (Exception e)
			{
				Logger.Error(string.Format("Falha ao auditar exclus�o na entidade {0} - {1}", @event.Persister.EntityName, @event.Entity), e);
			}
		}

		#endregion

		#region IPostInsertEventListener MEMBROS
		/// <summary>
		/// Evento de inser��o de um objeto auditado
		/// </summary>
		/// <param name="event">Evento de Inser��o</param>
		public void OnPostInsert(PostInsertEvent @event)
		{
			if (!(@event.Entity is IAuditoria))
			{
				return;
			}

			try
			{
				AuditoriaService.AuditarInsercao(@event.Entity, @event.Id, @event.Persister.PropertyNames, @event.State);
			}
			catch (Exception e)
			{
				Logger.Error(string.Format("Falha ao auditar inser��o na entidade {0} - {1}", @event.Persister.EntityName, @event.Entity), e);
			}
		}

		#endregion

		#region IPostUpdateEventListener MEMBROS
		/// <summary>
		/// Evento de atualiza��o de um objeto auditado
		/// </summary>
		/// <param name="event">Evento de atualiza��o</param>
		public void OnPostUpdate(PostUpdateEvent @event)
		{
			if (!(@event.Entity is IAuditoria))
			{
				return;
			}

			try
			{
				AuditoriaService.AuditarAtualizacao(@event.Entity, @event.Id, @event.Persister.PropertyNames, @event.State, @event.OldState);
			}
			catch (Exception e)
			{
				Logger.Error(string.Format("Falha ao auditar atualiza��o na entidade {0} - {1}", @event.Persister.EntityName, @event.Entity), e);
			}
		}

		#endregion
	}
}