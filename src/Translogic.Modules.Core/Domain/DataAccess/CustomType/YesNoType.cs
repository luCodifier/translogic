﻿namespace Translogic.Modules.Core.Domain.DataAccess.CustomType
{
    using System;
    using System.Data;
    using NHibernate;
    using NHibernate.SqlTypes;
    using NHibernate.UserTypes;

    /// <summary>
    /// Classe de customização de Char para Boolean
    /// </summary>
    public class YesNoType : IUserType
    {
        /// <summary>
        /// Propriedade IsMutable
        /// </summary>
        public bool IsMutable
        {
            get { return false; }
        }

        /// <summary>
        /// Propriedade ReturnedType
        /// </summary>
        public Type ReturnedType
        {
            get { return typeof(YesNoType); }
        }

        /// <summary>
        /// Propriedade SqlTypes
        /// </summary>
        public SqlType[] SqlTypes
        {
            get { return new[] { NHibernateUtil.String.SqlType }; }
        }

        /// <summary>
        /// Method NullSafeGet
        /// </summary>
        /// <param name="rs">IDataReader rs </param>
        /// <param name="names">string[] names</param>
        /// <param name="owner">object owner</param>
        /// <returns>Return object</returns>
        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var obj = NHibernateUtil.String.NullSafeGet(rs, names[0]);

            if (obj == null)
            {
                return null;
            }

            var yesNo = (string)obj;

            if (yesNo != "Y" && yesNo != "N")
            {
                throw new Exception("Expected data to be 'Y' or 'N'");
            }

            return yesNo == "Y";
        }

        /// <summary>
        /// Method NullSafeSet
        /// </summary>
        /// <param name="cmd">IDbCommand cmd</param>
        /// <param name="value">object value</param>
        /// <param name="index">Parameter int index</param>
        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            if (value == null)
            {
                ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
            }
            else
            {
                var yes = (bool)value;
                ((IDataParameter)cmd.Parameters[index]).Value = yes ? "Y" : "N";
            }
        }

        /// <summary>
        /// Method DeepCopy
        /// </summary>
        /// <param name="value">object value</param>
        /// <returns>Return object</returns>
        public object DeepCopy(object value)
        {
            return value;
        }

        /// <summary>
        /// Method Replace
        /// </summary>
        /// <param name="original">object original</param>
        /// <param name="target">object target</param>
        /// <param name="owner">object owner</param>
        /// <returns>Return object</returns>
        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        /// <summary>
        /// Method Assemble
        /// </summary>
        /// <param name="cached">object cached</param>
        /// <param name="owner">object owner</param>
        /// <returns>Return object</returns>
        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        /// <summary>
        /// Object Disassemble
        /// </summary>
        /// <param name="value">Object value</param>
        /// <returns>Return object</returns>
        public object Disassemble(object value)
        {
            return value;
        }

        /// <summary>
        /// Method Equals
        /// </summary>
        /// <param name="x">Parameter x</param>
        /// <param name="y">Parameter y</param>
        /// <returns>Return of method Equals</returns>
        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Equals(y);
        }

        /// <summary>
        /// Method GetHashCode
        /// </summary>
        /// <param name="x">Parametro x</param>
        /// <returns>Método GetHashCode</returns>
        public int GetHashCode(object x)
        {
            return x == null ? typeof(bool).GetHashCode() + 473 : x.GetHashCode();
        }
    }
}