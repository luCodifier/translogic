namespace Translogic.Modules.Core.Domain.DataAccess.CustomType
{
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Acesso;
	using Model.Diversos;
	using Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;

	/// <summary>
	/// Tipo customizado de <see cref="EventoUsuarioCodigo"/>
	/// </summary>
	public class EventoUsuarioCodigoCustomType : DescribedEnumStringType<EventoUsuarioCodigo>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoPessoaEnum"/>
	/// </summary>
	public class TipoPessoaCustomType : DescribedEnumStringType<TipoPessoaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="OperacaoAuditoria"/>
	/// </summary>
	public class OperacaoAuditoriaCustomType : DescribedEnumStringType<OperacaoAuditoria>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoVeiculoEnum"/>
	/// </summary>
	public class TipoVeiculoCustomType : DescribedEnumStringType<TipoVeiculoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="AbastecimentoEnum"/>
	/// </summary>
	public class AbastecimentoEnumCustomType : DescribedEnumStringType<AbastecimentoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="SituacaoTremEnum"/>
	/// </summary>
	public class SituacaoTremEnumCustomType : DescribedEnumStringType<SituacaoTremEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoTremEnum"/>
	/// </summary>
	public class TipoTremEnumCustomType : DescribedEnumStringType<TipoTremEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="SituacaoComposicaoEnum"/>
	/// </summary>
	public class TipoSituacaoComandoLocomotivaEnumCustomType : DescribedEnumStringType<TipoSituacaoComandoLocomotivaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="SituacaoComposicaoEnum"/>
	/// </summary>
	public class SituacaoComposicaoEnumCustomType : DescribedEnumStringType<SituacaoComposicaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="FuncaoMaquinistaEnum"/>
	/// </summary>
	public class FuncaoMaquinistaEnumCustomType : DescribedEnumStringType<FuncaoMaquinistaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="MedidaTempoEnum"/>
	/// </summary>
	public class MedidaTempoEnumCustomType : DescribedEnumStringType<MedidaTempoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="EstadoFreioEnum"/>
	/// </summary>
	public class EstadoFreioEnumCustomType : DescribedEnumStringType<EstadoFreioEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="EstadoLocomotivaEnum"/>
	/// </summary>
	public class EstadoLocomotivaEnumCustomType : DescribedEnumStringType<EstadoLocomotivaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="CarregadoVazioEnum"/>
	/// </summary>
	public class CarregadoVazioEnumCustomType : DescribedEnumStringType<CarregadoVazioEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoGrupoFluxoEnum"/>
	/// </summary>
	public class TipoGrupoFluxoEnumCustomType : DescribedEnumStringType<TipoGrupoFluxoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoUnidadeMedidaEnum"/>
	/// </summary>
	public class TipoUnidadeMedidaEnumCustomType : DescribedEnumStringType<TipoUnidadeMedidaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="FerroviaFaturamentoEnum"/>
	/// </summary>
	public class FerroviaFaturamentoEnumCustomType : DescribedEnumStringType<FerroviaFaturamentoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoFluxoEnum"/>
	/// </summary>
	public class TipoFluxoEnumCustomType : DescribedEnumStringType<TipoFluxoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="PedidoEmergenciaEnum"/>
	/// </summary>
	public class PedidoEmergenciaEnumCustomType : DescribedEnumStringType<PedidoEmergenciaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoEnvioCteEnum" />
	/// </summary>
	public class TipoTrafegoFluxoEnumCustomType : DescribedEnumStringType<TipoTrafegoFluxoEnum>
	{
	}

    /// <summary>
    /// Tipo customizado de <see cref="TipoEnvioCteEnum" />
    /// </summary>
    public class StatusEnvioEmailEnumCustomType : DescribedEnumStringType<StatusEnvioEmailEnum>
    {
    }

    /// <summary>
    /// Tipo customizado de <see cref="TipoEnvioCteEnum" />
    /// </summary>
    public class NaturezaDaOperacaoEnumCustomType : DescribedEnumStringType<NaturezaDaOperacaoEnum>
    {
    }

	/// <summary>
	/// Tipo customizado de <see cref="UsuarioConfiguracaoEnum" />
	/// </summary>
	public class UsuarioConfiguracaoEnumCustomType : DescribedEnumStringType<UsuarioConfiguracaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoSituacaoTracaoLocomotivaEnum"/>
	/// </summary>
	public class TipoSituacaoTracaoLocomotivaEnumCustomType : DescribedEnumStringType<TipoSituacaoTracaoLocomotivaEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoSituacaoPosicaoLocomotivaEnum"/>
	/// </summary>
	public class TipoSituacaoPosicaoLocomotivaEnumCustomType : DescribedEnumStringType<TipoSituacaoPosicaoLocomotivaEnum>
	{
	}

    /// <summary>
    /// Tipo customizado de <see cref="LimitadoPorEnumCustomType"/>
    /// </summary>
    public class LimitadoPorEnumCustomType : DescribedEnumStringType<LimitadoPorEnum>
    {
    }

    /// <summary>
    /// Tipo customizado de <see cref="EnviaMensagemAvisoEnumCustomType"/>
    /// </summary>
    public class EnviaMensagemAvisoEnumCustomType : DescribedEnumStringType<EnviaMensagemAvisoEnum>
    {
    }

    /// <summary>
    /// Tipo customizado de <see cref="SituacaoTremPxEnumCustomType"/>
    /// </summary>
    public class SituacaoTremPxEnumCustomType : DescribedEnumStringType<SituacaoTremPxEnum>
    {
    }

    /// <summary>
    /// Custom type para tipo de altera��o OnTime
    /// </summary>
    public class TipoAlteracaoOnTimeCustomType : DescribedEnumStringType<TipoAlteracaoOnTime>
    {
    }
}