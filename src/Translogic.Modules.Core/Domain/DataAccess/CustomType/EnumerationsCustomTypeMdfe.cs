﻿namespace Translogic.Modules.Core.Domain.DataAccess.CustomType
{
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Diversos.Mdfe;

	/// <summary>
	/// Tipo customizado de <see cref="TipoLogMdfeEnum" />
	/// </summary>
	public class TipoLogMdfeEnumCustomType : DescribedEnumStringType<TipoLogMdfeEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="SituacaoMdfeEnum"/>
	/// </summary>
	public class SituacaoMdfeEnumCustomType : DescribedEnumStringType<SituacaoMdfeEnum>
	{
	}
}
