﻿namespace Translogic.Modules.Core.Domain.DataAccess.CustomType
{
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Diversos.Cte;

	/// <summary>
	/// Tipo customizado de <see cref="SituacaoCteEnum"/>
	/// </summary>
	public class SituacaoCteEnumCustomType : DescribedEnumStringType<SituacaoCteEnum>
	{
	}

	/// <summary>
    /// Tipo customizado de <see cref="TipoServicoCteEnum"/>
	/// </summary>
    public class TipoServicoCteEnumCustomType : DescribedEnumStringType<TipoServicoCteEnum>
	{
	}

    /// <summary>
    /// Tipo customizado de <see cref="TipoCteEnum"/>
    /// </summary>
    public class TipoCteEnumCustomType : DescribedEnumStringType<TipoCteEnum>
    {
    }

    /// <summary>
    /// Tipo customizado de <see cref="StatusCteEnum"/>
    /// </summary>
    public class StatusCteEnumCustomType : DescribedEnumStringType<StatusCteEnum>
    {
    }

	/// <summary>
	/// Tipo customizado de <see cref="TipoLogCteEnum" />
	/// </summary>
	public class TipoLogCteEnumCustomType : DescribedEnumStringType<TipoLogCteEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoOperacaoCteEnum" />
	/// </summary>
	public class TipoOperacaoCteEnumCustomType : DescribedEnumStringType<TipoOperacaoCteEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoEnvioCteEnum" />
	/// </summary>
	public class TipoEnvioCteEnumCustomType : DescribedEnumStringType<TipoEnvioCteEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoArquivoEnvioEnum" />
	/// </summary>
	public class TipoArquivoEnvioEnumCustomType : DescribedEnumStringType<TipoArquivoEnvioEnum>
	{
	}
}
