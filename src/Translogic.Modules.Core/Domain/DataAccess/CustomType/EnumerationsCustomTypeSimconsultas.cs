﻿namespace Translogic.Modules.Core.Domain.DataAccess.CustomType
{
	using ALL.Core.AcessoDados.TiposCustomizados;
	using Model.Diversos;
	using Model.Diversos.Simconsultas;

	/// <summary>
	/// Tipo customizado de <see cref="FormaPagamentoEnum"/>
	/// </summary>
	public class FormaPagamentoEnumCustomType : DescribedEnumStringType<FormaPagamentoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoNotaFiscalEnum"/>
	/// </summary>
	public class TipoNotaFiscalEnumCustomType : DescribedEnumStringType<TipoNotaFiscalEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="FormatoImpressaoEnum"/>
	/// </summary>
	public class FormatoImpressaoEnumCustomType : DescribedEnumStringType<FormatoImpressaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoEmissaoEnumCustomType"/>
	/// </summary>
	public class TipoEmissaoEnumCustomType : DescribedEnumStringType<TipoEmissaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="TipoAmbienteEnum"/>
	/// </summary>
	public class TipoAmbienteEnumCustomType : DescribedEnumStringType<TipoAmbienteEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="FinalidadeEmissaoEnum"/>
	/// </summary>
	public class FinalidadeEmissaoEnumCustomType : DescribedEnumStringType<FinalidadeEmissaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="ProcessoEmissaoEnum"/>
	/// </summary>
	public class ProcessoEmissaoEnumCustomType : DescribedEnumStringType<ProcessoEmissaoEnum>
	{
	}

	/// <summary>
	/// Tipo customizado de <see cref="ProcessoEmissaoEnum"/>
	/// </summary>
	public class ModeloFreteEnumCustomType : DescribedEnumStringType<ModeloFreteEnum>
	{
	}
}