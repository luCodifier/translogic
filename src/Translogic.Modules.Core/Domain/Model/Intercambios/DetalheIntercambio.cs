﻿namespace Translogic.Modules.Core.Domain.Model.Intercambios
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
	/// Model Detalhe Intercambio
	/// </summary>
	public class DetalheIntercambio : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
		/// Gets or sets Registro Intercambio
        /// </summary>
		public virtual RegistroIntercambio RegistroIntercambio { get; set; }

		/// <summary>
		/// Gets or sets Vagao
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		/// Gets or sets Locomotiva
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		/// <summary>
		/// Gets or sets ObsIntercambio
		/// </summary>
		public virtual long ObsIntercambio { get; set; }

		/// <summary>
		/// Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

      #endregion
    }
}
