﻿namespace Translogic.Modules.Core.Domain.Model.Intercambios
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
	/// Model Registro Intercambio
	/// </summary>
	public class RegistroIntercambio : EntidadeBase<int?>
    {
        #region PROPRIEDADES

		/// <summary>
		///  Gets or sets Area Operacional Abriga
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacionalAbriga { get; set; }

		/// <summary>
		///  Gets or sets Empresa
		/// </summary>
		public virtual Empresa Empresa { get; set; }

		/// <summary>
		///  Gets or sets Num Intercambio
		/// </summary>
		public virtual int NumIntercambio { get; set; }

		/// <summary>
		///  Gets or sets Dt Intercambio
		/// </summary>
		public virtual DateTime DtIntercambio { get; set; }

		/// <summary>
		///  Gets or sets Ind Registro Intercambio
		/// </summary>
		public virtual char IndRegistroIntercambio { get; set; }

		/// <summary>
		///  Gets or sets Ind Tipo Intercambio
		/// </summary>
		public virtual char IndTipoIntercambio { get; set; }
		
		/// <summary>
		///  Gets or sets Obs Intercambio
		/// </summary>
		public virtual long ObsIntercambio { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets Id Pedido
		/// </summary>
		public virtual int IdPedido { get; set; }

		/// <summary>
		///  Gets or sets Pedido Derivado
		/// </summary>
		public virtual PedidoDerivado PedidoDerivado { get; set; }

      #endregion
    }
}
