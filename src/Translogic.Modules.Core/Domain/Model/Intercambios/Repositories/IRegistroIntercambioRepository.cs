namespace Translogic.Modules.Core.Domain.Model.Intercambios.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Intercambios;

    /// <summary>
	/// Interface de repositório de RegistroIntercambio
	/// </summary>
	public interface IRegistroIntercambioRepository : IRepository<RegistroIntercambio, int?>
	{
	}
}