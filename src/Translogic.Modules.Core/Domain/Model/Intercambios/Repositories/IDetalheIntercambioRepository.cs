namespace Translogic.Modules.Core.Domain.Model.Intercambios.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Intercambios;

    /// <summary>
	/// Interface de repositório de DetalheIntercambio
	/// </summary>
	public interface IDetalheIntercambioRepository : IRepository<DetalheIntercambio, int?>
	{
	}
}