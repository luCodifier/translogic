﻿namespace Translogic.Modules.Core.Domain.Model.ArquivoBaldeio
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;

    /// <summary>
    /// Classe Arquivo Baldeio (Carta de anexada do baldeio)
    /// </summary>
    public class CartaBaldeio : EntidadeBase<int>
    {
        #region Propriedades
        /// <summary>
        /// Arquivo PDF (BINARIO em array de bytes)
        /// </summary>        
        public virtual byte[] ArquivoPdf { get; set; }

        /// <summary>
        /// Nome do arquivo
        /// </summary>
        public virtual string NomeArquivo { get; set; }
        
        /// <summary>
        /// data de atualizaçao do arquivo
        /// </summary>
        public virtual DateTime DataAtualizacao { get; set; }
        
        /// <summary>
        /// Nome do usuario 
        /// </summary>
        public virtual string Usuario { get; set; }
        
        /// <summary>
        /// relacionamento com a classe baldeio
        /// </summary>
        public virtual Baldeio Baldeio { get; set; }
        
        #endregion

    }
}