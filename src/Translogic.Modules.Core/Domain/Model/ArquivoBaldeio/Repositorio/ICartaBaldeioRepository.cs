﻿namespace Translogic.Modules.Core.Domain.Model.ArquivoBaldeio.Repositorio
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;

    public interface ICartaBaldeioRepository : IRepository<CartaBaldeio, int>
    {
        /// <summary>
        /// Obter Arquivo do baldeio por id do Baldeio
        /// </summary>
        /// <param name="idBaldeio"></param>
        /// <returns></returns>
        CartaBaldeioDto ObterArquivoBaldeio(int idArquivoBaldeio);

        /// <summary>
        /// Obter pelo id do Baldeio
        /// </summary>
        /// <param name="idBaldeio"></param>
        /// <returns></returns>
        CartaBaldeio ObterPorBaldeio(int idBaldeio);
    }
}
