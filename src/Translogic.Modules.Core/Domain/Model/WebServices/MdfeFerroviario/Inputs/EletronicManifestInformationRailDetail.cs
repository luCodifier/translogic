﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Inputs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Detalhe da pesquisa referente aos dados MDF-e Ferroviário
    /// </summary>
    [DataContract]
    public class EletronicManifestInformationRailDetail
    {
        /// <summary>
        /// Data início de pesquisa dos dados MDF-e
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Data fim de pesquisa dos dados MDF-e
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime EndDate { get; set; }
    }
}