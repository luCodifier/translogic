﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema do Mdfe Ferroviário
    /// </summary>
    [DataContract]
    public class InformationEletronicManifestInformationRail
    {
        /// <summary>
        /// Raiz do esquema do CT-e Ferroviário
        /// </summary>
        [DataMember]
        public InformationEletronicManifestInformationRailType InformationEletronicManifestInformationRailType { get; set; }
    }
}