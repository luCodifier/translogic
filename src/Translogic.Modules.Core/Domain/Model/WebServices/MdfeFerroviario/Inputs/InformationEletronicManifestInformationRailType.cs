﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Objeto de input para o request
    /// </summary>
    [DataContract]
    public class InformationEletronicManifestInformationRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. 
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Detalhe da pesquisa referente aos dados MDF-e Ferroviário
        /// </summary>
        [DataMember]
        public EletronicManifestInformationRailDetail EletronicManifestInformationRailDetail { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: Alinhamento prévio.
        /// </summary>
        [DataMember]
        public string OtherXml { get; set; }
    }
}