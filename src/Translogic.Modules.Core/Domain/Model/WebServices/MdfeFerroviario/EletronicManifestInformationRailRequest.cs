﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario
{
    using System.Runtime.Serialization;
    using Inputs;

    /// <summary>
    /// Mensagem de request para o input do service
    /// </summary>
    [DataContract]
    public class EletronicManifestInformationRailRequest
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public EletronicManifestInformationRailRequest()
        {
            InformationEletronicManifestInformationRail = new InformationEletronicManifestInformationRail();
        }

        /// <summary>
        /// Construtor passando objeto do tipo InformationEletronicManifestInformationRail
        /// </summary>
        /// <param name="informationEletronicManifestInformationRail">Objeto de input</param>
        public EletronicManifestInformationRailRequest(InformationEletronicManifestInformationRail informationEletronicManifestInformationRail)
        {
            InformationEletronicManifestInformationRail = informationEletronicManifestInformationRail;
        }

        /// <summary>
        /// Dados das mdfe
        /// </summary>
        [DataMember]
        public InformationEletronicManifestInformationRail InformationEletronicManifestInformationRail { get; set; }
    }
}