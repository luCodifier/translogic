﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario
{
    using System.Runtime.Serialization;
    using Outputs;

    /// <summary>
    /// Response WS mdfe ferroviario
    /// </summary>
    [DataContract]
    public class EletronicManifestInformationRailResponse
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public EletronicManifestInformationRailResponse()
        {
            InformationEletronicManifestInformationRailOutput = new InformationEletronicManifestInformationRailOutput();
        }

        /// <summary>
        /// Construtor que recebe objeto do tipo InformationEletronicManifestInformationRailOutput
        /// </summary>
        /// <param name="informationEletronicManifestInformationRailOutput">Objeto de output</param>
        public EletronicManifestInformationRailResponse(InformationEletronicManifestInformationRailOutput informationEletronicManifestInformationRailOutput)
        {
            InformationEletronicManifestInformationRailOutput = informationEletronicManifestInformationRailOutput;
        }

        /// <summary>
        /// Objeto utilizado para o output do servico
        /// </summary>
        [DataMember]
        public InformationEletronicManifestInformationRailOutput InformationEletronicManifestInformationRailOutput { get; set; }
    }
}