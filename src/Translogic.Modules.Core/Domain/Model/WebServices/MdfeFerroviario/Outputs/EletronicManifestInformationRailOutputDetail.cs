﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Outputs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Dados que compõem o MDF-e.
    /// </summary>
    [DataContract]
    public class EletronicManifestInformationRailOutputDetail
    {
        /// <summary>
        /// ID de referência do MDF-e
        /// </summary>
        [DataMember]
        public decimal EletronicManifestId { get; set; }

        /// <summary>
        /// Data e hora saída da composição
        /// </summary>
        [DataMember]
        public DateTime DateTimeOutputTrainComposition { get; set; }

        /// <summary>
        /// chave eletrônica do MDF-e
        /// </summary>
        [DataMember]
        public string EletronicManifestKey { get; set; }
    }
}