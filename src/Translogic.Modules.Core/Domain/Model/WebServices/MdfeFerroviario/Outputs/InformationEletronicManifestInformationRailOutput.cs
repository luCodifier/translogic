﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema das informações do MDF-e
    /// </summary>
    [DataContract]
    public class InformationEletronicManifestInformationRailOutput
    {
        /// <summary>
        /// Objeto Raiz do esquema das informações do MDF-e
        /// </summary>
        [DataMember]
        public InformationEletronicManifestInformationRailOutputType InformationEletronicManifestInformationRailOutputType { get; set; }
    }
}