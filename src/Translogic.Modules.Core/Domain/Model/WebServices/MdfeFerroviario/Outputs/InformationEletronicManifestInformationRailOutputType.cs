﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.MdfeFerroviario.Outputs
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema das informações do MDF-e
    /// </summary>
    [DataContract]
    public class InformationEletronicManifestInformationRailOutputType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi utilizado na validação da mensagem. 
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Estrutura de dados referente aos MDF-e Ferroviários
        /// </summary>
        [DataMember]
        public List<EletronicManifestInformationRailOutputDetail> EletronicManifestInformationRailDetail { get; set; }

        /// <summary>
        /// Mensagem de retorno
        /// </summary>
        [DataMember]
        public string ReturnMessage { get; set; }
    }
}