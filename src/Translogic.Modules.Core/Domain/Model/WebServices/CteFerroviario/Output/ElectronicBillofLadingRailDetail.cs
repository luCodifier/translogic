﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Estrutura de dados referente aos CT-e Ferroviários
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingRailDetail
    {
        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: previamente acordados.
        /// </summary>
        [DataMember(Order = 0)]
        public string OtherXml { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: previamente acordados.
        /// </summary>
        [DataMember(Order = 1)]
        public ElectronicBillofLadingType[] ElectronicBillofLadingData { get; set; }
    }
}