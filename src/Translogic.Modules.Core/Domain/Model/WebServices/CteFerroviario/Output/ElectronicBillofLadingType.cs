﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Tipo de dado para definir um CT-e
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingType
    {
        /// <summary>
        /// Data e hora saída da composição
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime DateTimeOutputTrainComposition { get; set; }

        /// <summary>
        /// Situação do CT-e emitido pela ALL. Exemplo: "AUTORIZADO" e etc
        /// </summary>
        [DataMember(Order = 1)]
        public string EletronicManifestStatus { get; set; }

        /// <summary>
        /// chave eletrônica do CT-e
        /// </summary>
        [DataMember(Order = 2)]
        public string EletronicManifestKey { get; set; }

        /// <summary>
        /// ID de referência do CT-e
        /// </summary>
        [DataMember(Order = 3)]
        public decimal EletronicManifestId { get; set; }

        /// <summary>
        /// Chave do MDFE
        /// </summary>
        [DataMember(Order = 4)]
        public string MdfeKey { get; set; }

        /// <summary>
        /// Data de Encerramento da OS
        /// </summary>
        [DataMember(Order = 5)]
        public DateTime DataEncerramentoOrdemServico { get; set; }
    }
}