﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output.Enumerations;

    /// <summary>
    /// Tipo de dado para definir um CT-e
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi utilizado na validação da mensagem. Formato: 9.99x (Versão + Release + Patch)
        /// </summary>
        [DataMember(Order = 0)]
        public string VersionIdentifier { get; set; }
        
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi utilizado na validação da mensagem. Formato: 9.99x (Versão + Release + Patch)
        /// </summary>
        [DataMember(Order = 1)]
        public ElectronicBillofLadingRailDetail ElectronicBillofLadingRailDetail { get; set; }
    }
}