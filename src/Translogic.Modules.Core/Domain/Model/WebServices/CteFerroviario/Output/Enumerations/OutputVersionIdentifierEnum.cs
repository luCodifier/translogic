namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output.Enumerations
{
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// Vers�o do Esquema Indica qual a vers�o Esquema foi utilizado na valida��o da mensagem. Formato: 9.99x (Vers�o + Release + Patch)
    /// </summary>
    [DataContract]
    public enum OutputVersionIdentifierEnum
    {
        /// <summary>
        /// Versao 1.00C
        /// </summary>
        [XmlEnum("1.00C")]
        Item100C,

        /// <summary>
        /// Versao 1.00c
        /// </summary>
        [XmlEnum("1.00c")]
        Item100c,
    }
}