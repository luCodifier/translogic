﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs;

    /// <summary>
    /// Mensagem de request para confirmação de notificação
    /// </summary>
    [DataContract]
    public class InformationElectronicNotificationRequest
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public InformationElectronicNotificationRequest()
        {
            InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType();
        }

        /// <summary>
        /// Construtor inicializando
        /// </summary>
        /// <param name="informationElectronicBillofLadingRailType">Objeto do tipo InformationElectronicBillofLadingRailType</param>
        public InformationElectronicNotificationRequest(InformationElectronicBillofLadingRailType informationElectronicBillofLadingRailType)
        {
            InformationElectronicBillofLadingRailType = informationElectronicBillofLadingRailType;
        }

        /// <summary>
        /// ctes de retorno.
        /// </summary>
        [DataMember]
        public InformationElectronicBillofLadingRailType InformationElectronicBillofLadingRailType { get; set; }
    }
}