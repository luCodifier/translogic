﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario
{
    /// <summary>
    /// Constantes de configuração
    /// </summary>
    internal static class CONSTS
    {
        /// <summary>
        /// Namespace dos models de input
        /// </summary>
        internal const string XML_NAMESPACE_INPUT = "http://xmlns.cte.com.br/ferrovias/ElectronicBillofLadingInput";

        /// <summary>
        /// Namespace dos models de output
        /// </summary>
        internal const string XML_NAMESPACE_OUTPUT = "http://xmlns.cte.com.br/ferrovias/ElectronicBillofLadingOutput";

        /// <summary>
        /// Namespace dos models de notificação
        /// </summary>
        internal const string XML_NAMESPACE_NOTIFICATION = "http://xmlns.cte.com.br/ferrovias/ElectronicBillofLadingNotification";
    }
}