namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Remetente da Mensagem Indica as informa��es do emissor da mensagem.
    /// </summary>
    [DataContract]
    public class MessageSenderIdentification
    {
        /// <summary>
        /// Usu�rio que identifica o emissor com intuito de verificar as permiss�es deste nos sistemas (Ex.: CL000531, TR001234, etc.).
        /// </summary>
        [DataMember(Order = 0)]
        public string SenderUser { get; set; }
    }
}