﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Corpo da Nota de Expedição Estrutura de dados que dá detalhes sobre a nota de expedição.
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingRailDetail
    {
        /// <summary>
        /// Data início de pesquisa dos dados CT-e
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Data fim de pesquisa dos dados CT-e
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime EndDate { get; set; }
    }
}