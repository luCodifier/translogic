namespace Translogic.Modules.Core.Domain.Model.Ctes.Inputs.Enumerations
{
    using System;
    using System.Xml.Serialization;

    /// <summary>
    /// Vers�o do Esquema Indica qual a vers�o Esquema foi utilizado na valida��o da mensagem. Formato: 9.99x (Vers�o + Release + Patch)
    /// </summary>
    [Serializable]
    [XmlType(AnonymousType = true, Namespace = CONSTS.XML_NAMESPACE_INPUT, TypeName = "VersionIdentifier")]
    public enum InputVersionIdentifierEnum
    {
        /// <summary>
        /// Versao 1.00C
        /// </summary>
        [XmlEnum("1.00C")]
        Item100C,

        /// <summary>
        /// Versao 1.00c
        /// </summary>
        [XmlEnum("1.00c")]
        Item100c,
    }
}