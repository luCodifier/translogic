namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs.Enumerations
{
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// Opera��o Indica a �ltima transa��o realizada com a Nota de Expedi��o
    /// </summary>
    [DataContract]
    public enum TransactionEnum
    {
        /// <summary>
        /// Transa��o de Inser��o
        /// </summary>
        [XmlEnum("INSERIR")]
        Inserir,

        /// <summary>
        /// Transa��o de Atualiza��o
        /// </summary>
        [XmlEnum("ATUALIZAR")]
        Atualizar,

        /// <summary>
        /// Transa��o de cancelamento
        /// </summary>
        [XmlEnum("CANCELAR")]
        Cancelar,
    }
}