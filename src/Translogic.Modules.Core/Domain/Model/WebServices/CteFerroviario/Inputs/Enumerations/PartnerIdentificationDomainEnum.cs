namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs.Enumerations
{
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// Dom�nio da Identifica��o da Empresa Este campo indica que padr�o/dom�nio/tipo de codifica��o o parceiro est� enviando. 
    /// Valores Poss�veis: CPF - C�digo de Pessoa F�sica 
    /// CNPJ - C�digo de Pessoa Jur�dica 
    /// VALE -Codifica��o interna da Vale 
    /// MRS - Codifica��o interna da MRS 
    /// ALL - Codifica��o interna da ALL 
    /// Outros dom�nios podem ser acrescidos desde que previamente acordados
    /// </summary>
    [DataContract]
    public enum PartnerIdentificationDomainEnum
    {
        /// <summary>
        /// C�digo de Pessoa Jur�dica 
        /// </summary>
        [DataMember]
        [XmlEnum("CNPJ")]
        Cnpj,

        /// <summary>
        /// C�digo de Pessoa F�sica 
        /// </summary>
        [DataMember]
        [XmlEnum("CPF")]
        Cpf,

        /// <summary>
        /// Codifica��o interna da Vale 
        /// </summary>
        [DataMember]
        [XmlEnum("VALE")]
        Vale,

        /// <summary>
        /// Codifica��o interna da MRS 
        /// </summary>
        [DataMember]
        [XmlEnum("MRS")]
        Mrs,

        /// <summary>
        /// Codifica��o interna da ALL
        /// </summary>
        [DataMember]
        [XmlEnum("ALL")]
        All
    }
}