﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema de Nota de Expedição.
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi utilizado na validação da mensagem. Formato: 9.99x (Versão + Release + Patch)
        /// </summary>
        [DataMember(Order = 0)]
        public string VersionIdentifier { get; set; }

        /*
        /// <summary>
        /// Cabeçalho da Mensagem Estrutura com as informações técnicas da mensagem.
        /// </summary>
        [DataMember(Order = 1)]
        public DeliveryHeader DeliveryHeader { get; set; }
        */

        /// <summary>
        /// Detalhe da pesquisa referente aos dados CT-e Ferroviário
        /// </summary>
        [DataMember(Order = 2)]
        public ElectronicBillofLadingRailDetail ElectronicBillofLadingRailDetail { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: Alinhamento prévio.
        /// </summary>
        [DataMember(Order = 3)]
        public string OtherXml { get; set; }
    }
}