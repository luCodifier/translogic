namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs.Enumerations;

    /// <summary>
    /// Tipo de dados para identifica��o do parceiro, que pode ser uma empresa, pessoa f�sica ou outra organiza��o
    /// </summary>
    [DataContract]
    public class PartnerIdentificationType
    {
        /// <summary>
        /// Dom�nio da Identifica��o da Empresa Este campo indica que padr�o/dom�nio/tipo de codifica��o o parceiro est� enviando. 
        /// </summary>
        [DataMember(Order = 0)]
        public string PartnerIdentificationDomain { get; set; }

        /// <summary>
        /// Identifica��o da Empresa 
        /// Indica a identifica��o �nica da empresa segundo a defini��o do dom�nio da informa��o.
        /// </summary>
        /// <remarks></remarks>
        [DataMember(Order = 1)]
        public string BusinessIdentifier { get; set; }

        /// <summary>
        /// Raz�o Social ou Nome Indica a Raz�o Social ou o Nome da Empresa
        /// </summary>
        [DataMember(Order = 2)]
        public string BusinessName { get; set; }
    }
}