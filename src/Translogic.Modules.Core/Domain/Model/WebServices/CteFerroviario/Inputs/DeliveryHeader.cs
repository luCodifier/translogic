namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs
{
    using System;
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs.Enumerations;

    /// <summary>
    /// Cabe�alho da Mensagem Estrutura com as informa��es t�cnicas da mensagem.
    /// </summary>
    [DataContract]
    public class DeliveryHeader
    {
        /// <summary>
        /// Data de Envio da Mensagem Indica a data e a hora do envio da mensagem. Formato: YYYY-MM-DDTHH:MM:SS
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime MessageDateTime { get; set; }

        /// <summary>
        /// Remetente da Mensagem Indica as informa��es do emissor da mensagem.
        /// </summary>
        [DataMember(Order = 1)]
        public MessageSenderIdentification MessageSenderIdentification { get; set; }

        /// <summary>
        /// Destinat�rio da Mensagem Indica as informa��es do recebedor da mensagem.
        /// </summary>
        [DataMember(Order = 2)]
        public PartnerIdentificationType MessageReceiverIdentification { get; set; }

        /// <summary>
        /// Identificador da Mensagem Identificador �nico alfanum�rico que representa o Numero do Protocolo de envio gerado pelo Emissor da Nota de Expedi��o.
        /// </summary>
        [DataMember(Order = 3)]
        public string MessageProtocol { get; set; }

        /// <summary>
        /// Tipo da Mensagem Valores Poss�veis: 
        /// DESP_FERR - Despacho entre ferrovias. 
        /// NOTA_EXP_FERR - Nota de Expedi��o Ferrovi�ria recebida via integra�ao eletronica. 
        /// NOTA_EXP_PORTAL - Nota de Expedi��o Ferrovi�ria cadastrada via portal. 
        /// NOTA_EXP_RODO - Nota de Expedi��o Rodovi�ria 
        /// LOT_CAR - Lote Carregamento 
        /// INC_VAG - Inclus�o de Vag�es
        /// </summary>
        [DataMember(Order = 4)]
        public string MessageType { get; set; }

        /// <summary>
        /// Opera��o Indica a �ltima transa��o realizada com a Nota de Expedi��o. 
        /// Valores poss�veis: INSERIR - Inser��o 
        /// ATUALIZAR - Atualiza��o 
        /// CANCELAR - Cancelamento
        /// </summary>
        [DataMember(Order = 5)]
        public TransactionEnum Transaction { get; set; }

        /// <summary>
        /// C�digo Interno de Identifica��o da Nota de Expedi��o no parceiro onde a Nota Expedi��o foi gerada.
        /// </summary>
        [DataMember(Order = 6)]
        public string ReferenceId { get; set; }

        /// <summary>
        /// Data e hora da gera��o da Nota de Expedi��o. Formato: YYYY-MM-DDTHH:MM:SS
        /// </summary>
        [DataMember(Order = 7)]
        public DateTime DateTimeGenerated { get; set; }

        /// <summary>
        /// Coment�rios da Nota de Expedi��o.
        /// </summary>
        [DataMember(Order = 8)]
        public string Comments { get; set; }
    }
}