﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Output;

    /// <summary>
    /// Classe de retorno ao confirmar
    /// </summary>
    [DataContract]
    public class InformationElectronicNotificationResponse
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public InformationElectronicNotificationResponse()
        {
            InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType();
        }

        /// <summary>
        /// Construtor com parametros inicializando propriedades
        /// </summary>
        /// <param name="informationElectronicBillofLadingRailType">Objeto do tipo <see cref="InformationElectronicBillofLadingRailType"/></param>
        public InformationElectronicNotificationResponse(InformationElectronicBillofLadingRailType informationElectronicBillofLadingRailType)
        {
            InformationElectronicBillofLadingRailType = informationElectronicBillofLadingRailType;
        }

        /// <summary>
        /// Nota de Expedição de retorno.
        /// </summary>
        [DataMember]
        public InformationElectronicBillofLadingRailType InformationElectronicBillofLadingRailType { get; set; }
    }
}