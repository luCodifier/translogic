﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs;

    /// <summary>
    /// Mensagem de Response
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingExportCompletedRailResponse
    {
        /// <summary>
        /// construtor vazio
        /// </summary>
        public EletronicBillofLadingExportCompletedRailResponse()
        {
            EletronicBillofLadingExportCompletedRailOutput = new EletronicBillofLadingExportCompletedRailOutput();
        }

        /// <summary>
        /// construtor recebendo objeto
        /// </summary>
        /// <param name="_eletronicBillofLadingExportCompletedRailOutput">Objeto do tipo EletronicBillofLadingExportCompletedRailOutput</param>
        public EletronicBillofLadingExportCompletedRailResponse(EletronicBillofLadingExportCompletedRailOutput _eletronicBillofLadingExportCompletedRailOutput)
        {
            EletronicBillofLadingExportCompletedRailOutput = _eletronicBillofLadingExportCompletedRailOutput;
        }

        /// <summary>
        /// Declaracao da variavel
        /// </summary>
        [DataMember]
        public EletronicBillofLadingExportCompletedRailOutput EletronicBillofLadingExportCompletedRailOutput { get; set; }
    }
}