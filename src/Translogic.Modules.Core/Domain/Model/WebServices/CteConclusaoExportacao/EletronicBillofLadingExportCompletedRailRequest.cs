﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Inputs;

    /// <summary>
    /// Request da mensagem de envio
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingExportCompletedRailRequest
    {
        /// <summary>
        /// construtor vazio
        /// </summary>
        public EletronicBillofLadingExportCompletedRailRequest()
        {
            ElectronicBillofLadingExportCompletedRail = new ElectronicBillofLadingExportCompletedRail();
        }

        /// <summary>
        /// construtor recebendo objeto
        /// </summary>
        /// <param name="electronicBillofLadingExportCompletedRail">Objeto do tipo ElectronicBillofLadingExportCompletedRail</param>
        public EletronicBillofLadingExportCompletedRailRequest(ElectronicBillofLadingExportCompletedRail electronicBillofLadingExportCompletedRail)
        {
            ElectronicBillofLadingExportCompletedRail = electronicBillofLadingExportCompletedRail;
        }

        /// <summary>
        /// Declaracao da variavel
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingExportCompletedRail ElectronicBillofLadingExportCompletedRail { get; set; }
    }
}