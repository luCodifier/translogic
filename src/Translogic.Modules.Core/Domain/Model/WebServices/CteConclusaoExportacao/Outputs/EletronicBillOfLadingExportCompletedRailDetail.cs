﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Estrutura de dados referete aos dados do CT-e que concluíu a exportação
    /// </summary>
    [DataContract]
    public class EletronicBillOfLadingExportCompletedRailDetail
    {
        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: previamente acordados.
        /// </summary>
        [DataMember]
        public string OtherXml { get; set; }

        /// <summary>
        /// Esquema raiz da saída de dados do CT-e que concluiram a exportação
        /// </summary>
        [DataMember]
        public List<ElectronicBillofLadingExportCompletedData> ElectronicBillofLadingExportCompletedData { get; set; }
    }
}