﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Objeto responsavel por carregar os dados das notas
    /// </summary>
    [DataContract]
    public class WaybillInformation
    {
        /// <summary>
        /// ID de referência da NF-e, do CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 0)]
        public string WaybillInvoiceExportCompletedId { get; set; }

        /// <summary>
        /// Chave eletrônica da NF-e, referênciada no CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 1)]
        public string WaybillInvoiceKey { get; set; }
    }
}