﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Esquema raiz da saída de dados do CT-e que concluiram a exportação
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingExportCompletedRailOutput
    {
        /// <summary>
        /// Objeto principal da raiz do esquema
        /// </summary>
        [DataMember]
        public EletronicBillofLadingExportCompletedRailType EletronicBillofLadingExportCompletedRailType { get; set; }
    }
}