﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Esquema raiz da saída de dados do CT-e que concluiram a exportação
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingExportCompletedData
    {
        /// <summary>
        /// ID de referência do CT-e que concluíu a exportação.
        /// </summary>
        [DataMember(Order = 0, Name = "ElectronicBillofLadingExportCompletedId")]
        public virtual decimal CteId { get; set; }

        /*
        /// <summary>
        /// ID de referência da NF-e, do CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 1, Name = "WaybillInvoiceExportCompletedId")]
        public List<decimal> NfeId { get; set; }

        /// <summary>
        /// Chave eletrônica da NF-e, referênciada no CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 2)]
        public List<string> WaybillInvoiceKey { get; set; }
        */

        /// <summary>
        /// Informações das notas fiscais
        /// </summary>
        [DataMember(Order = 1)]
        public virtual List<WaybillInformation> Waybills { get; set; }

        /// <summary>
        /// Chave eletrônica do CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 2, Name = "ElectronicBillofLadingKey")]
        public virtual string CteKey { get; set; }

        /// <summary>
        /// Data/Hora de chegada do vagão
        /// </summary>
        [DataMember(Order = 3)]
        public virtual DateTime DateTimeArrival { get; set; }

        /// <summary>
        /// Recinto Alfandegado (Terminal)
        /// </summary>
        [DataMember(Order = 4)]
        public virtual string BondedEnclosure { get; set; }

        /// <summary>
        /// Peso Descarregado
        /// </summary>
        [DataMember(Order = 5)]
        public virtual decimal UnloadedWeight { get; set; }

        /// <summary>
        /// Identificador do carregamento
        /// </summary>
        public virtual decimal Carregamento { get; set; }
    }

    /// <summary>
    /// Classe temp para retorno da base
    /// </summary>
    public class ElectronicBillofLadingExportDtoData : ElectronicBillofLadingExportCompletedData
    {
        /// <summary>
        /// Id da nota
        /// </summary>
        public virtual decimal NotaId { get; set; }

        /// <summary>
        /// Chave da nota
        /// </summary>
        public virtual string NotaChave { get; set; }
    }
}