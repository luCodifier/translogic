﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Objeto principal do esquema
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingExportCompletedRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. 
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Mensagem de retorno em caso de erro ou informação referente a pesquisa
        /// </summary>
        [DataMember]
        public string ReturnMessage { get; set; }

        /// <summary>
        /// Estrutura de dados referete aos dados do CT-e que concluíu a exportação
        /// </summary>
        [DataMember]
        public EletronicBillOfLadingExportCompletedRailDetail EletronicBillOfLadingExportCompletedRailDetail { get; set; }
    }
}