﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Inputs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Informar periodo de pesquisa para buscar os dados do CT-e que concluíram a exportação
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingExportCompletedRailDetail
    {
        /// <summary>
        /// Data início de pesquisa dos dados do CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Data início de pesquisa dos dados do CT-e que concluíu a exportação
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime EndDate { get; set; }
    }
}