﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Objeto principal do esquema
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingExportCompletedRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. Formato: 9.99x
        /// (Versão + Release + Patch).
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Informar periodo de pesquisa para buscar os dados do CT-e que concluíram a exportação
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingExportCompletedRailDetail ElectronicBillofLadingExportCompletedRailDetail { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para
        /// utilização: Alinhamento prévio.
        /// </summary>
        [DataMember]
        public string OtherXml { get; set; }
    }
}