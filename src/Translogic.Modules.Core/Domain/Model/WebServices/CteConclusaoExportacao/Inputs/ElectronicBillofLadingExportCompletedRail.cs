﻿namespace Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema do CT-e que concluíu a exportação
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingExportCompletedRail
    {
        /// <summary>
        /// Objeto Principal do esquema
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingExportCompletedRailType ElectronicBillofLadingExportCompletedRailType { get; set; }
    }
}