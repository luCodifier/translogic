﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;

    /// <summary>
    /// Mensagem de Response
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingCancelledRailResponse
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public EletronicBillofLadingCancelledRailResponse()
        {
            EletronicBillofLadingCancelledRailType = new EletronicBillofLadingCancelledRailType();
        }

        /// <summary>
        /// Construtor recebendo um tipo
        /// </summary>
        /// <param name="eletronicBillofLadingCancelledRailType">Objeto do tipo EletronicBillofLadingCancelledRailType</param>
        public EletronicBillofLadingCancelledRailResponse(EletronicBillofLadingCancelledRailType eletronicBillofLadingCancelledRailType)
        {
            EletronicBillofLadingCancelledRailType = eletronicBillofLadingCancelledRailType;
        }

        /// <summary>
        /// Declaracao da variavel
        /// </summary>
        [DataMember]
        public EletronicBillofLadingCancelledRailType EletronicBillofLadingCancelledRailType { get; set; }
    }
}