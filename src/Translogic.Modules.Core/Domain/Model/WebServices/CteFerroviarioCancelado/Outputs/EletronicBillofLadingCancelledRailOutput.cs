﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs
{
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Eletronic Bill of Lading Cancelled Rail Output
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingCancelledRailOutput
    {
        /// <summary>
        /// Eletronic Bill of Lading Cancelled Rail Type
        /// </summary>
        [DataMember]
        public EletronicBillofLadingCancelledRailType EletronicBillofLadingCancelledRailType { get; set; }
    }
}