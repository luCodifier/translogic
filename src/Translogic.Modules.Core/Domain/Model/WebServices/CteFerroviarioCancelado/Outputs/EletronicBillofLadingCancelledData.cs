﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using WebServices.CteFerroviarioCancelado.Outputs;

    /// <summary>
    /// Dados dos Refaturamento do CT-e, são composto por CT-e Cancelado e CT-e autorizado com base no CT-e cancelado
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingCancelledData
    {
        /// <summary>
        /// ID de referência do CT-e cancelado
        /// </summary>
        [DataMember(Order = 0, Name = "ElectronicBillofLadingCancelledId")]
        public decimal CteCancelledId { get; set; }

        /// <summary>
        /// ID de referência do CT-e, gerado a partir do CT-e cancelado
        /// </summary>
        [DataMember(Order = 2, Name = "ElectronicBillofLadingId")]
        public decimal CteLadingId { get; set; }

        /// <summary>
        /// Chave eletrônica do CT-e canelado
        /// </summary>
        [DataMember(Order = 5, Name = "ElectronicBillofLadingCancelledKey")]
        public string CteCancelledKey { get; set; }

        /// <summary>
        /// Chave eletrônica do novo CT-e
        /// </summary>
        [DataMember(Order = 7, Name = "ElectronicBillofLadingKey")]
        public string CteLadingKey { get; set; }

        /// <summary>
        /// Carrega varias notas para um despacho cancelado
        /// </summary>
        [DataMember(Order = 8)]
        public List<WayBillsInfo> WayBillsInfoCancelled { get; set; }

        /// <summary>
        /// Carrega varias notas para um novo despacho
        /// </summary>
        [DataMember(Order = 9)]
        public List<WayBillsInfo> WayBillsInfoLading { get; set; }
    }

    /// <summary>
    /// Dto para carregar as notas
    /// </summary>
    public class EletronicBillofLadingCancelledDataDto : EletronicBillofLadingCancelledData
    {
        /// <summary>
        /// Chave eletrônica da NF-e, referênciado no CT-e autorizado.
        /// </summary>
        [DataMember(Order = 6)]
        public string WaybillInvoiceKey { get; set; }

        /// <summary>
        /// ID de referência da NF-e, que está vinculada ao CT-e autorizado
        /// </summary>
        [DataMember(Order = 3)]
        public decimal WaybillInvoiceId { get; set; }

        /// <summary>
        /// Chave eletrônica da NF-e, referênciada no CT-e cancelado
        /// </summary>
        [DataMember(Order = 4)]
        public string WaybillInvoiceCancelledKey { get; set; }

        /// <summary>
        /// ID de referência da NF-e, do CT-e cancelado
        /// </summary>
        [DataMember(Order = 1)]
        public decimal WaybillInvoiceCancelledId { get; set; }
    }
}