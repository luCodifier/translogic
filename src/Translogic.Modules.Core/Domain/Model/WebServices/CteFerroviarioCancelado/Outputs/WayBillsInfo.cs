﻿namespace Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviarioCancelado.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Carregas as notas do cte
    /// </summary>
    [DataContract]
    public class WayBillsInfo
    {
        /// <summary>
        /// ID de referência da NF-e, do CT-e cancelado
        /// </summary>
        [DataMember(Order = 1)]
        public decimal WaybillId { get; set; }

        /// <summary>
        /// Chave eletrônica da NF-e, referênciada no CT-e cancelado
        /// </summary>
        [DataMember(Order = 2)]
        public string WaybillKey { get; set; }
    }
}