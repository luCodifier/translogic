﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs
{
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Eletronic Bill of Lading Cancelled Rail Type
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingCancelledRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. Formato: 9.99x
        /// (Versão + Release + Patch).
        /// </summary>
        [DataMember(Order = 0)]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Estrutura de dados referete ao refaturamento fiscal
        /// </summary>
        [DataMember(Order = 1)]
        public EletronicBillOfLadingCancelledRailDetail EletronicBillOfLadingCancelledRailDetail { get; set; }

        /// <summary>
        /// Mensagem complementar
        /// </summary>
        [DataMember(Order = 2)]
        public string Message { get; set; }
    }
}