﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    
    /// <summary>
    ///  Estrutura de dados referete ao refaturamento fiscal
    /// </summary>
    [DataContract]
    public class EletronicBillOfLadingCancelledRailDetail
    {
        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: previamente acordados.
        /// </summary>
        [DataMember(Order = 0)]
        public string OtherXml { get; set; }

        /// <summary>
        /// Dados dos Refaturamento do CT-e, são composto por CT-e Cancelado e CT-e autorizado com base no CT-e cancelado
        /// </summary>
        [DataMember(Order = 1)]
        public List<EletronicBillofLadingCancelledData> EletronicBillofLadingCancelledData { get; set; }
    }
}