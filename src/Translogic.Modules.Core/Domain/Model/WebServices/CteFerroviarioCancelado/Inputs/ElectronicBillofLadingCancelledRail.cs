﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema do CT-e Refaturado
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingCancelledRail
    {
        /// <summary>
        /// Electronic Bill of Lading Cancelled Rail Type
        /// </summary>
        [DataMember(Order = 0)]
        public ElectronicBillofLadingCancelledRailType ElectronicBillofLadingCancelledRailType { get; set; }
    }
}