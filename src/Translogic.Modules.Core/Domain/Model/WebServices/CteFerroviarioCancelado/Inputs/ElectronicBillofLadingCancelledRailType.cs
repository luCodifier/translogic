﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema do CT-e Refaturado
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingCancelledRailType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. Formato: 9.99x
        /// (Versão + Release + Patch).
        /// </summary>
        [DataMember(Order = 0)]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Informar periodo de pesquisa do CT-e Cancelado
        /// </summary>
        [DataMember(Order = 1)]
        public ElectronicBillofLadingCancelledRailDetail ElectronicBillofLadingCancelledRailDetail { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para
        /// utilização: Alinhamento prévio.
        /// </summary>
        [DataMember(Order = 2)]
        public string OtherXml { get; set; }
    }
}