﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Informar periodo de pesquisa do CT-e Cancelado
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingCancelledRailDetail
    {
        /// <summary>
        /// Data início de pesquisa dos dados do CT-e Cancelado
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Data início de pesquisa dos dados do CT-e Cancelado
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime EndDate { get; set; }
    }
}