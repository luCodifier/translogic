﻿namespace Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs;

    /// <summary>
    /// Request da mensagem de envio
    /// </summary>
    [DataContract]
    public class EletronicBillofLadingCancelledRailRequest
    {
        /// <summary>
        /// construtor vazio
        /// </summary>
        public EletronicBillofLadingCancelledRailRequest()
        {
            ElectronicBillofLadingCancelledRailType = new ElectronicBillofLadingCancelledRailType();
        }

        /// <summary>
        /// construtor inicializado
        /// </summary>
        /// <param name="electronicBillofLadingCancelledRailType">Objeto do tipo ElectronicBillofLadingCancelledRailType</param>
        public EletronicBillofLadingCancelledRailRequest(ElectronicBillofLadingCancelledRailType electronicBillofLadingCancelledRailType)
        {
            ElectronicBillofLadingCancelledRailType = electronicBillofLadingCancelledRailType;
        }

        /// <summary>
        /// Declaracao da variavel
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingCancelledRailType ElectronicBillofLadingCancelledRailType { get; set; }
    }
}