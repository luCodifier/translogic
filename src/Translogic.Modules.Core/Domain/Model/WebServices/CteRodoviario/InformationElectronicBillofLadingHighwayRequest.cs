﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario.Inputs;

    /// <summary>
    /// Classe de Request do Servico Cte Rodoviario
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingHighwayRequest
    {
        /// <summary>
        /// Construtor Vazio
        /// </summary>
        public InformationElectronicBillofLadingHighwayRequest()
        {
            InformatioElectronicBillofLadingHighway = new InformatioElectronicBillofLadingHighway();
        }

        /// <summary>
        /// Construtor recebendo objeto do tipo InformatioElectronicBillofLadingHighway
        /// </summary>
        /// <param name="_informatioElectronicBillofLadingHighway">Objeto de Input</param>
        public InformationElectronicBillofLadingHighwayRequest(InformatioElectronicBillofLadingHighway _informatioElectronicBillofLadingHighway)
        {
            InformatioElectronicBillofLadingHighway = _informatioElectronicBillofLadingHighway;
        }

        /// <summary>
        /// Objeto do tipo InformatioElectronicBillofLadingHighway para o Request
        /// </summary>
        [DataMember]
        public InformatioElectronicBillofLadingHighway InformatioElectronicBillofLadingHighway { get; set; }
    }
}