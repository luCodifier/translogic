﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario
{
    using System.Runtime.Serialization;
    using Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs;

    /// <summary>
    /// Response WS Cte Rodoviario
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingHighwayResponse
    {
        /// <summary>
        /// Construtor vazio
        /// </summary>
        public InformationElectronicBillofLadingHighwayResponse()
        {
            InformationElectronicBillofLadingHighwayOutput = new InformationElectronicBillofLadingHighwayOutput();
        }

        /// <summary>
        /// Construtor recebendo objeto de output
        /// </summary>
        /// <param name="_informationElectronicBillofLadingHighwayOutput">Objeto de Output</param>
        public InformationElectronicBillofLadingHighwayResponse(InformationElectronicBillofLadingHighwayOutput _informationElectronicBillofLadingHighwayOutput)
        {
            InformationElectronicBillofLadingHighwayOutput = _informationElectronicBillofLadingHighwayOutput;
        }

        /// <summary>
        /// Objeto utilizado para o output do servico
        /// </summary>
        [DataMember]
        public InformationElectronicBillofLadingHighwayOutput InformationElectronicBillofLadingHighwayOutput { get; set; }
    }
}