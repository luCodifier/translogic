﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Objeto Raiz do esquema de CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingHighwayType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem. 
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }
        
        /// <summary>
        /// Mensagem de retorno em caso de erro ou informação referente a pesquisa
        /// </summary>
        [DataMember]
        public string ReturnMessage { get; set; }
        
        /// <summary>
        /// Estrutura de dados referente aos CT-e Rodoviário
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingHighwayDetail ElectronicBillofLadingHighwayDetail { get; set; }
    }
}