﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Esquema raiz da saída de dados do CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingData
    {
        /// <summary>
        /// ID de referência do CT-e Rodoviário
        /// </summary>
        [DataMember]
        public decimal ElectronicBillofLadingId { get; set; }
        
        /// <summary>
        /// Base Aduaneira. Terminal que recebeu a mercadoria
        /// </summary>
        [DataMember]
        public string CustomsBase { get; set; }
        
        /// <summary>
        /// Descrição da Base Aduaneira
        /// </summary>
        [DataMember]
        public string DescriptionCustomsBase { get; set; }
        
        /// <summary>
        /// Data e hora da chegada do DACTE Rodoviário
        /// </summary>
        [DataMember]
        public DateTime DateDischarge { get; set; }
        
        /// <summary>
        /// chave eletrônica do CT-e Rodo
        /// </summary>
        [DataMember]
        public string ElectronicBillofLadingKey { get; set; }

        /// <summary>
        /// Dados das notas fiscais 
        /// </summary>
        [DataMember]
        public List<WaybillInformation> WaybillInformation { get; set; }
    }

    /// <summary>
    /// temporaria para consulta ao banco
    /// </summary>
    public class ElectronicBillofLadingDataDto : ElectronicBillofLadingData
    {
        /// <summary>
        /// chave eletrônica da NF-e Rodo
        /// </summary>
        [DataMember]
        public string DtoWaybillInvoiceKey { get; set; }

        /// <summary>
        /// Peso Declarado em KG
        /// </summary>
        [DataMember]
        public decimal DtoDeclaredWeight { get; set; }

        /// <summary>
        /// Peso aferido em KG
        /// </summary>
        [DataMember]
        public decimal DtoMeasuredWeight { get; set; }

        /// <summary>
        /// Diferenca de peso entre o declarado e o aferido
        /// </summary>
        [DataMember]
        public decimal DtoWeightDifference { get; set; }
    }
}