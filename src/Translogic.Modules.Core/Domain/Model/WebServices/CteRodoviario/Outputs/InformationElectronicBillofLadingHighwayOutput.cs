﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema de CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingHighwayOutput
    {
        /// <summary>
        /// Objeto Raiz do esquema de CT-e Rodoviário
        /// </summary>
        [DataMember]
        public InformationElectronicBillofLadingHighwayType InformationElectronicBillofLadingHighwayType { get; set; }
    }
}