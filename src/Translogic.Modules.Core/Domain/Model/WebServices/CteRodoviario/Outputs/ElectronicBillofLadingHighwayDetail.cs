﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Estrutura de dados referente aos CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingHighwayDetail
    {
        /// <summary>
        /// Informações Complementares. Pré-requisito para utilização: previamente acordados.
        /// </summary>
        [DataMember]
        public string OtherXml { get; set; }

        /// <summary>
        /// Esquema raiz da saída de dados do CT-e Rodoviário
        /// </summary>
        [DataMember]
        public List<ElectronicBillofLadingData> ElectronicBillofLadingData { get; set; }
    }
}