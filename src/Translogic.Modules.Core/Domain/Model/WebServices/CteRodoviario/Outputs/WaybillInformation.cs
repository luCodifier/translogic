﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Outputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Dados das notas fiscais
    /// </summary>
    [DataContract]
    public class WaybillInformation
    {
        /// <summary>
        /// chave eletrônica da NF-e Rodo
        /// </summary>
        [DataMember]
        public string WaybillInvoiceKey { get; set; }

        /// <summary>
        /// Peso Declarado em KG
        /// </summary>
        [DataMember]
        public decimal DeclaredWeight { get; set; }

        /// <summary>
        /// Peso aferido em KG
        /// </summary>
        [DataMember]
        public decimal MeasuredWeight { get; set; }

        /// <summary>
        /// Diferenca de peso entre o declarado e o aferido
        /// </summary>
        [DataMember]
        public decimal WeightDifference { get; set; }
    }
}