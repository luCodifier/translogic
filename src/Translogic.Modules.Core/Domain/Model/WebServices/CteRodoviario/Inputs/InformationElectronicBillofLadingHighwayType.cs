﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Raiz do esquema do CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class InformationElectronicBillofLadingHighwayType
    {
        /// <summary>
        /// Versão do Esquema Indica qual a versão Esquema foi
        /// utilizado na validação da mensagem.
        /// </summary>
        [DataMember]
        public string VersionIdentifier { get; set; }

        /// <summary>
        /// Detalhe da pesquisa referente aos dados CT-e Rodoviário
        /// </summary>
        [DataMember]
        public ElectronicBillofLadingHighwayDetail ElectronicBillofLadingHighwayDetail { get; set; }

        /// <summary>
        /// Informações Complementares. Pré-requisito para
        /// utilização: Alinhamento prévio.
        /// </summary>
        [DataMember]
        public string OtherXml { get; set; }
    }
}