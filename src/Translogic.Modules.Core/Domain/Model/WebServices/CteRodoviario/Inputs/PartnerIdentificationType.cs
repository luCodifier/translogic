﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Inputs
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Tipo de dados para identificação do parceiro, que pode ser    /// uma empresa, pessoa física ou outra organização
    /// </summary>
    [DataContract]
    public class PartnerIdentificationType
    {
        /// <summary>
        /// Domínio da Identificação da Empresa Este campo indica que                        
        /// padrão/domínio/tipo de codificação o parceiro está enviando. 
        /// Valores Possíveis: CPF - Código de Pessoa Física CNPJ - Código de Pessoa Jurídica ALL - 
        /// Codificação interna da ALL Outros domínios podem ser acrescidos desde que previamente acordados
        /// </summary>
        [DataMember]
        public string PartnerIdentificationDomain { get; set; }

        /// <summary>
        /// Identificação da Empresa Indica a identificação única da
        /// empresa segundo a definição do domínio da informação.
        /// </summary>
        [DataMember]
        public string BusinessIdentifier { get; set; }

        /// <summary>
        /// Razão Social ou Nome Indica a Razão Social ou o Nome da Empresa.
        /// </summary>
        [DataMember]
        public string BusinessName { get; set; }
    }
}