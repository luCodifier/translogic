﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Inputs
{
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Objeto Raiz do Schema
    /// </summary>
    [DataContract]
    public class InformatioElectronicBillofLadingHighway
    {
        /// <summary>
        /// Objeto principal do Request
        /// </summary>
        [DataMember]
        public InformationElectronicBillofLadingHighwayType InformationElectronicBillofLadingHighwayType { get; set; }
    }
}