﻿namespace Translogic.Modules.Core.Domain.Model.CteRodoviario.Inputs
{
    using System;
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Detalhe da pesquisa referente aos dados CT-e Rodoviário
    /// </summary>
    [DataContract]
    public class ElectronicBillofLadingHighwayDetail
    {
        /// <summary>
        /// Data início de pesquisa dos dados CT-e
        /// </summary>
        [DataMember(Order = 0)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Data fim de pesquisa dos dados CT-e
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime EndDate { get; set; }
    }
}