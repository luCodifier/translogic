﻿

namespace Translogic.Modules.Core.Domain.Model.LiberacaoFormacaoTrem.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
using System;

    public interface ILiberacaoFormacaoTremRepository : IRepository<LiberacaoFormacaoTrem, int>
    {
        /// <summary>
        /// Busca lista por parametros
        /// </summary>        
        /// <param name="nomeFornecedor"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoServico"></param>
        /// <returns></returns>        
        ResultadoPaginado<LiberacaoFormacaoTremDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal OrdemServico, string OrigemTrem, string DestinoTrem, string LocalAtual, string NomeSol, string NomeAutorizador, DateTime dataInicial, DateTime dataFinal, string SituacaoTrava);

        /// <summary>
        /// Retornar a consulta para exportação Liberacao Formacao Trem
        /// </summary>
        /// <param name="OrdemServico"></param>
        /// <param name="OrigemTrem"></param>
        /// <param name="DestinoTrem"></param>
        /// <param name="LocalAtual"></param>
        /// <param name="NomeSol"></param>
        /// <param name="NomeAutorizador"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="SituacaoTrava"></param>
        /// <returns></returns>
        IList<LiberacaoFormacaoTremDto> ObterLiberacaoFormacaoTremExportar(decimal OrdemServico, 
                                                                           string OrigemTrem, 
                                                                           string DestinoTrem, 
                                                                           string LocalAtual, 
                                                                           string NomeSol, 
                                                                           string NomeAutorizador, 
                                                                           DateTime dataInicial, 
                                                                           DateTime dataFinal, 
                                                                           string SituacaoTrava);
        
        /// <summary>
        /// Retornar o timmer definido em base para consulta automatica
        /// </summary>
        /// <returns></returns>
        int ObterTimmerConsultaLiberacaoFormacaoTrem();
    }
}
