﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.LocaisFornecedor;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.Fornecedor
{
    public class FornecedorOs : EntidadeBase<int>
    {
        #region Propriedades
        ///<sumary>
        /// Nome
        /// </sumary>
        public virtual string Nome { get; set; }

        ///<sumary>
        ///Login
        ///</sumary>
        public virtual string Login { get; set; }

        /// <summary>
        /// DtRegistro
        /// </summary>
        public virtual DateTime DtRegistro { get; set; }

        /// <summary>
        /// Ativo
        /// </summary>
        public virtual bool? Ativo { get; set; }

        /// <summary>
        /// Servicos
        /// </summary>
        public virtual IList<LocalFornecedor> LocaisTiposFornecedor { get; set; }

        #endregion
    }
}