﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe de Tipos de Lacres identificados no TFA (1 - Lacres OK, 2 - Lacres Parcial, 3 - Lacres Ausentes)
    /// </summary>
    public class TipoLacreTfa : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        ///   Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}