﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Via;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe TFA (Termo de Falta ou Avaria)
    /// </summary>
    public class Tfa : EntidadeBase<int>
    {
        #region Propriedades
        
        /// <summary>
		/// Processo vinculado ao TFA
		/// </summary>
		public virtual ProcessoSeguro ProcessoSeguro { get; set; }

        /// <summary>
		/// Peso Destino
		/// </summary>
		public virtual double? PesoDestino { get; set; }

        /// <summary>
		/// Tipo de Lacre informado no TFA
		/// </summary>
		public virtual TipoLacreTfa TipoLacreTfa { get; set; }

        /// <summary>
        /// Tipo de Gambito informado no TFA
        /// </summary>
        public virtual TipoGambitoTfa TipoGambitoTfa { get; set; }

        /// <summary>
		/// Tipo de Liberador da Descarga informado no TFA
		/// </summary>
		public virtual TipoLiberadorDescarga TipoLiberadorDescarga { get; set; }

        /// <summary>
		/// Tipo de Conclusão do Laudo informado no TFA
		/// </summary>
		public virtual TipoConclusaoTfa TipoConclusaoTfa { get; set; }

        /// <summary>
		/// Matrícula do Vistoriador informado no TFA
		/// </summary>
		public virtual string MatriculaVistoriador { get; set; }
	
	    /// <summary>
		/// Nome do representante do Cliente informado no TFA
		/// </summary>
		public virtual string NomeRepresentante { get; set; }

        /// <summary>
		/// Documento do representante do Cliente informado no TFA
		/// </summary>
        public virtual string DocumentoRepresentante { get; set; }
       
        /// <summary>
		/// Identifica se o Representante do Cliente assinou digitalmente o TFA S/N (Default N)
		/// </summary>
        public virtual bool AssinaturaRepresentante { get; set; }

        /// <summary>
        /// Imagem da assinatura do Cliente no TFA (binário)
        /// </summary>
        public virtual byte[] ImagemAssinaturaRepresentante { get; set; }

        /// <summary>
        /// Data de Ciência do TFA pelo Representante/Cliente
        /// </summary>
        public virtual DateTime? DataClienteCiente { get; set; }        

        /// <summary>
		/// Identifica se o Cliente ou o Representante teve Ciência do TFA S/N (Default N)
		/// </summary>
		public virtual bool? StatusClienteCiente { get; set; }

        /// <summary>
		/// Identifica se o TFA foi gerado no aplicativo mobile (S) ou no Translogic (N - Default)
		/// </summary>
		public virtual bool OrigemProcessoApp { get; set; }
	
        /// <summary>
        /// ID do TFA_CACHE, caso o TFA tenha sido criado pelo aplicativo e tenha gerado divergência
        /// </summary>
        public virtual int? IdTfaCache { get; set; }

        #endregion

    }
}