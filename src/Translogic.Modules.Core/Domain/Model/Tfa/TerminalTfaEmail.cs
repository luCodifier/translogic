﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    /// Classe de email vinculado ao Terminal para notificação sobre TFAs
    /// </summary>
    public class TerminalTfaEmail : EntidadeBase<int>
    {
        /// <summary>
        /// Terminal
        /// </summary>
        public virtual TerminalTfa Terminal { get; set; }

        /// <summary>
        /// E-mail para notificação sobre TFAs
        /// </summary>
        public virtual string Email { get; set; }
    }
}