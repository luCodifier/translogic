﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe de Tipos de Conclusões do TFA (1 - Devido, 2 - Indevido, 3 - Cancelado)
    /// </summary>
    public class TipoConclusaoTfa: EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        ///   Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}