﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe de LOGs de eventos do TFA 
    /// </summary>                                        
    public class LogTfa : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// ID do TFA vinculado ao LOG de Evento
        /// </summary>
        public virtual int? IdTfa { get; set; }
        
        /// <summary>
        /// Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Código do Tipo de LOG de Evento
        /// </summary>
        public virtual int? CodTipoLogTfa { get; set; }

        #endregion
    }
}