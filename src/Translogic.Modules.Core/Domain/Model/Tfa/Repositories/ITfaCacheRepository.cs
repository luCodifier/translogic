﻿

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Tfa;
    using Translogic.Core.Infrastructure.Web;
    using System;

    public interface ITfaCacheRepository : IRepository<TfaCache, int>
    {

    }
}
