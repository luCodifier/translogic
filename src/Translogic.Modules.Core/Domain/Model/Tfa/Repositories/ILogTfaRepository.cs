﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    public interface ILogTfaRepository : IRepository<LogTfa, int>
    {
    }
}
