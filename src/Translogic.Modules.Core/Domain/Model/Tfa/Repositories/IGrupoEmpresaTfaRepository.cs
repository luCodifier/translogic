﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using Translogic.Core.Infrastructure.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    /// <summary>
    /// Interface de Repositório de GrupoEmpresaTfa
    /// </summary>
    public interface IGrupoEmpresaTfaRepository : IRepository<GrupoEmpresaTfa, int?>
    {
        /// <summary>
        /// Obtem o grupo de empresa pelo nome
        /// </summary>
        /// <param name="nomeGrupo">Nome do Grupo</param>
        /// <returns></returns>
        GrupoEmpresaTfa ObterPorNome(string nomeGrupo);

        /// <summary>
        /// Retorna resultado paginado de Grupos de Empresas, de acordo com o nome passado como parâmetro (LIKE%)
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="nomeGrupo">Nome do Grupo de Empresas</param>
        /// <returns>Resultado Paginado de GrupoEmpresaTfa</returns>
        ResultadoPaginado<GrupoEmpresaTfa> ObterPaginadoPorNomeGrupo(DetalhesPaginacaoWeb paginacao, string nomeGrupo);

       
        /// <summary>
        /// Verifica se existe(m) Empresa(s) vinculada(s) ao ID do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>retorna true caso exista(m) empresa(s) vinculada(s) ao grupo de Empresas</returns>
        bool ExisteEmpresaVinculada(int idGrupoEmpresa);

        /// <summary>
        /// Verifica se existe(m) usuário(s) vinculado(s) ao ID do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns>retorna true caso exista(m) usuário(s) vinculado(s) ao grupo de Empresas</returns>
        bool ExisteUsuarioCAALVinculado(int idGrupoEmpresa);
    }
}
