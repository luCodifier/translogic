﻿

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Tfa;
    using Translogic.Core.Infrastructure.Web;
    using System;

    public interface ITfaRepository : IRepository<Tfa, int>
    {
        /// <summary>
        /// Busca lista por parametros
        /// </summary>        
        /// <param name="nomeFornecedor"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoServico"></param>
        /// <returns></returns>        
        ResultadoPaginado<TfaConsultaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                     DateTime? dtInicioSinistros, DateTime? dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexoTFA);

        /// <summary>
        /// Retornar a consulta para exportação Liberacao Formacao Trem
        /// </summary>
        /// <param name="OrdemServico"></param>
        /// <param name="OrigemTrem"></param>
        /// <param name="DestinoTrem"></param>
        /// <param name="LocalAtual"></param>
        /// <param name="NomeSol"></param>
        /// <param name="NomeAutorizador"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="SituacaoTrava"></param>
        /// <returns></returns>
        IEnumerable<TfaExportaDto> ObterConsultaTFAExportar(DateTime? dtInicioProcesso, DateTime? dtFinalProcesso, string vagao, string terminal, string un, string up, string malha, string provisionado,
                                                     DateTime? dtInicioSinistros, DateTime? dtFinalSinistros, string numProcesso, string os, string situacao, string causa, string numTermo, string sindicancia, string anexoTFA);

        IList<UnidadeNegocioDto> ObterListaUnidadeNegocio();

        /// <summary>
        /// Cria o TFA automaticamente, de acordo com o número do Processo informado
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo para Geração automática do TFA</param>
        /// <returns></returns>
        Tfa CriarTfaProcesso(int numeroProcesso);


        /// <summary>
        /// Retornar a consulta para geração do PDF do TFA
        /// </summary>
        /// <param name="numProcesso">Número do Processo vinculado ao TFA</param>
        /// <returns>TFA para ageração do PDF</returns>
        TfaDto ObterConsultaTFAPdf(int numProcesso);


        /// <summary>
        /// Buscar o tfa pelo número do processo
        /// </summary>
        /// <param name="numProcesso"></param>
        /// <returns></returns>
        Tfa ObterPorNumProcesso(int numProcesso);

        /// <summary>
        /// Buscar o Modelo do Tfa pelo número do processo
        /// </summary>
        /// <param name="numProcesso"></param>
        /// <returns></returns>
        TfaEmailDto ObterModelEmailTfa(int numProcesso);

        /// <summary>
        /// Buscar o TFA pelo número do idTfaCache
        /// </summary>
        /// <param name="idTfaCache">Id do TfaCache</param>
        /// <returns></returns>
        Tfa ObterPorIdTfaCache(int idTfaCache);

        /// <summary>
        /// Atualizar Processo Seguro
        /// </summary>
        /// <param name="processos">Processos separados por virgula</param>
        void AtualizarProcessoSeguro(string processos, DateTime dtPrepPagamento, string formaPagamento, DateTime dtPagamento, DateTime dtEncerramento, string usuario);

        /// <summary>
        /// Atualizar Analise Seguro
        /// </summary>
        /// <param name="processos">Processos separados por virgula</param>
        void AtualizarAnaliseSeguro(string processos, string analiseProcesso, string usuario);

    }
}
