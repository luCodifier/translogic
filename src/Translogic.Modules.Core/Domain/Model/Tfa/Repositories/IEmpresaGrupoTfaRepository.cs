﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    /// <summary>
    /// Interface do Repositório EmpresaGrupoTfa
    /// </summary>
    public interface IEmpresaGrupoTfaRepository : IRepository<EmpresaGrupoTfa, int?>
    {
        /// <summary>
        /// Retorna todas as empresas do Grupo de Empresas passado como parâmetro
        /// </summary>
        /// <param name="paginacao">Dados para paginação em tela</param>
        /// <param name="idGrupoEmpresa">ID do Grupo de Empresas</param>
        /// <returns></returns>
        ResultadoPaginado<EmpresaGrupoTfa> ObterTodosPorIdGrupoEmpresa(DetalhesPaginacaoWeb paginacao, int idGrupoEmpresa);

        /// <summary>
        /// Recupera uma EmpresaGrupoTfa pelo número do CNPJ 
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns></returns>
        EmpresaGrupoTfa ObterPorCnpj(string cnpj);

        
        /// <summary>
        /// Retorna a lista de e-mails cadastrados para a empresa do Processo de Seguro
        /// </summary>
        /// <param name="numeroProcesso">Número do Processo</param>
        /// <returns>Retorna a lista de e-mails cadastrados para a empresa do Processo de Seguro</returns>
        List<string> ObterListaEmailTfa(int numeroProcesso);

    }
}
