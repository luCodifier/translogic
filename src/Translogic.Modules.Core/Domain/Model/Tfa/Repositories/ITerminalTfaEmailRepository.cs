﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    public interface ITerminalTfaEmailRepository : IRepository<TerminalTfaEmail, int>
    {
        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="paginacao">objeto com os parâmetros de paginação</param>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista paginada de Emails</returns>
        ResultadoPaginado<TerminalTfaEmail> ObterTodosPorIdTerminal(DetalhesPaginacaoWeb paginacao, int idTerminal);

        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista de Emails</returns>
        IList<TerminalTfaEmail> ObterTodosPorIdTerminal(int idTerminal);

        /// <summary>
        /// Obtem a lista de Emails vinculados ao terminal, de acordo com o ID passado como parâmetro retornando DTO
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns>Retorna lista de Emails</returns>
        IList<TerminalTfaEmailDto> ObterTodosPorIdTerminalDto(int idTerminal);

        /// <summary>
        /// Obtem o email do terminal pelo IdTerminal e email
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <param name="email">e-mail</param>
        /// <returns>retorna o email dop terminal pelo IdTerminal e email</returns>
        TerminalTfaEmail ObterPoridTerminalEmail(int idTerminal, string email);
    }
}
