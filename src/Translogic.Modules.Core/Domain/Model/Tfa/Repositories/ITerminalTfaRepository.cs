﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    public interface ITerminalTfaRepository : IRepository<TerminalTfa, int?>
    {
        /// <summary>
        /// Obtem a lista de DTOs de Terminais
        /// </summary>
        /// <returns>Retorna lista de DTOs de Terminais</returns>
        IList<TerminalDto> ObterListaTerminalDto();


        /// <summary>
        /// Obtem o TerminalTfa pelo ID do Terminal  informado
        /// </summary>
        /// <param name="idTerminal">Id do Terminal</param>
        /// <returns></returns>
        TerminalTfa ObterPorIdTerminal(int idTerminal);

        
    }
}
