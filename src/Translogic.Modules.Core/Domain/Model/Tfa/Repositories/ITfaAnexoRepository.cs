﻿

namespace Translogic.Modules.Core.Domain.Model.Tfa.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Tfa;

    public interface ITfaAnexoRepository : IRepository<TfaAnexo, int?>
    {
        /// <summary>
        /// Obtem OBJETO TFA ANEXO
        /// </summary>
        /// <param name="numTermo"></param>
        /// <returns> retorna OBJETO TFA ANEXO</returns>
        TfaAnexo ObterPorNumProcesso(string numProcesso);
    }
}
