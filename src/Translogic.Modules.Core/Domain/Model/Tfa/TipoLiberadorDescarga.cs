﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe de Tipos de Liberadores de Descarga identificados no TFA (1 - Terminal, 2 - Cliente, 3 - Ferrovia)
    /// </summary>
    public class TipoLiberadorDescarga : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        ///   Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}