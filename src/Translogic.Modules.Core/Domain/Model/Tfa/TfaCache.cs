﻿using System;
using System.Text;
using System.Collections.Generic;
using ALL.Core.Dominio;


namespace Translogic.Modules.Core.Domain.Model.Tfa
{

    public class TfaCache : EntidadeBase<int>
    {
                
        /// <summary>
        /// Número do Vagão
        /// </summary>
        public virtual string NumeroVagao { get; set; }

        /// <summary>
        /// Número do Despacho
        /// </summary>
        public virtual int? NumeroDespacho { get; set; }

        /// <summary>
        /// Serie do despacho
        /// </summary>
        public virtual string SerieDespacho { get; set; }

        /// <summary>
        /// Peso na Origem
        /// </summary>
        public virtual double? PesoOrigem { get; set; }

        /// <summary>
        /// Peso no Destino
        /// </summary>
        public virtual double? PesoDestino { get; set; }

        /// <summary>
        /// ID do Local de Vistoria
        /// </summary>
        public virtual int? IdLocalVistoria { get; set; }

        /// <summary>
        /// ID da Causa do Dano
        /// </summary>
        public virtual int? IdCausaDano { get; set; }

        /// <summary>
        /// Código do Tipo de Lacre (1 - Lacres OK, 2 - Lacre Parcial, 3 - Lacres Ausentes)
        /// </summary>
        public virtual int? CodTipoLacre { get; set; }

        /// <summary>
        /// Código do Tipo de Gambito (1 - Gambitos OK, 2 - Gambitos Parcial, 3 - Gambitos Ausentes)
        /// </summary>
        public virtual int? CodTipoGambito { get; set; }

        /// <summary>
        /// Indicador se o Processo é 1 - Devido , 2 - Indevido, 3 - Cancelado
        /// </summary>
        public virtual int? CodTipoConclusaoTfa { get; set; }

        /// <summary>
        /// Considerações Adicionais
        /// </summary>
        public virtual string ConsideracoesAdicionais { get; set; }

        /// <summary>
        /// Nome do Representante do Cliente
        /// </summary>
        public virtual string NomeRepresentante { get; set; }

        /// <summary>
        /// Número do Documento do Representante do Cliente
        /// </summary>
        public virtual string DocumentoRepresentante { get; set; }

        /// <summary>
        /// Imagem da Assinatura do Representante do Cliente
        /// </summary>
        public virtual byte[] ImgAssinaturaRepresentante { get; set; }

        /// <summary>
        /// Indica se o Responsável está ciente sobre o TFA (assinou no aplicativo)
        /// </summary>
        public virtual short ResponsavelCiente { get; set; }

        /// <summary>
        /// Data e Hora da ocorrência do Sinistro
        /// </summary>
        public virtual DateTime? DataHoraSinistro { get; set; }

        /// <summary>
        /// Data e Hora da realização da Vistoria
        /// </summary>
        public virtual DateTime? DataHoraVistoria { get; set; }

        /// <summary>
        /// Matrícula do Vistoriador
        /// </summary>
        public virtual string MatriculaVistoriador { get; set; }

        /// <summary>
        /// Nome do Vistoriador
        /// </summary>
        public virtual string NomeVistoriador { get; set; }
        
        /// <summary>
        /// Indica se a Edição do Número de Despacho e Vagão foi feita manualmente
        /// </summary>
        public virtual short EdicaoManual { get; set; }

        /// <summary>
        /// Código do Tipo de Liberador da Descarga (1 - Terminal, 2 - Cliente, 3 - Rumo)
        /// </summary>
        public virtual  int? CodTipoLiberadorDescarga { get; set; }

        /// <summary>
        ///   Número da Sindicância
        /// </summary>
        public virtual  long? Sindicancia { get; set; }

        /// <summary>
        /// Matrícula do Usuário que realizou a alteração
        /// </summary>
        public virtual string MatriculaUsuarioAlteracao { get; set; }

        /// <summary>
        /// Data/Hora da Inclusão
        /// </summary>
        public virtual DateTime DataHoraInclusao { get; set; }

        /// <summary>
        /// Data/Hora da Alteração
        /// </summary>
        public virtual DateTime DataHoraAlteracao { get; set; }
    }
}
