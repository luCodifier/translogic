﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    
    /// <summary>
    /// Enumeration para o indicador/flag do campo processoIndevido  na tabela processo seguro
    /// Lógica booleana inversa
    /// </summary>
    public enum IndicadorProcessoIndevidoEnum
	{
		/// <summary>
        /// Devido
		/// </summary>
		[Description("N")]
		Devido = 1,

		/// <summary>
        /// Indevido
		/// </summary>
		[Description("S")]
		Indevido = 2,

        /// <summary>
        /// Cancelado
		/// </summary>
		[Description("C")]
		Cancelado = 3
	}

    /// <summary>
    /// Enumeration para tipos de conclusão do TFA
    /// </summary>
    public enum TipoConclusaoTfaEnum
    {
        /// <summary>
        /// Devido
        /// </summary>
        [Description("S")]
        Devido = 1,

        /// <summary>
        /// Indevido
        /// </summary>
        [Description("N")]
        Indevido = 2,

        /// <summary>
        /// Cancelado
        /// </summary>
        [Description("C")]
        Cancelado = 3
    }


    /// <summary>
    /// Enumeration para tipos de Gambitos do TFA
    /// </summary>
    public enum TipoGambitoTfaEnum
    {
        /// <summary>
        /// GambitosOK
        /// </summary>
        [Description("S")]
        GambitosOK = 1,

        /// <summary>
        /// GambitosParcial
        /// </summary>
        [Description("P")]
        GambitosParcial = 2,

        /// <summary>
        /// GambitosAusentes
        /// </summary>
        [Description("N")]
        GambitosAusentes = 3
    }

    /// <summary>
    /// Enumeration para tipos de lacres do TFA
    /// </summary>
    public enum TipoLacreTfaEnum
    {
        /// <summary>
        /// LacresOK
        /// </summary>
        [Description("S")]
        LacresOK = 1,

        /// <summary>
        /// LacresParcial
        /// </summary>
        [Description("P")]
        LacresParcial = 2,

        /// <summary>
        /// LacresAusentes
        /// </summary>
        [Description("N")]
        LacresAusentes = 3
    }

    /// <summary>
    /// Enumeration para Status do TFA
    /// </summary>
    public enum StatusTfaEnum
    {
        /// <summary>
        /// Criado
        /// </summary>
        [Description("Criando")]
        Criando = 1,

        /// <summary>
        /// Alterado
        /// </summary>
        [Description("Alterando")]
        Alterando = 2,

        /// <summary>
        /// Excluído
        /// </summary>
        [Description("Excluíndo")]
        Excluindo = 3,

        /// <summary>
        /// ClienteCiente
        /// </summary>
        [Description("Cliente Ciente")]
        ClienteCiente = 4,

        /// <summary>
        /// ClienteNaoCiente
        /// </summary>
        [Description("Cliente Não Ciente")]
        ClienteNaoCiente = 5
    }

    /// <summary>
    /// Enumeration para tipos de Logs de TFA
    /// </summary>
    public enum TiPoLogTfaEnum
    {
        /// <summary>
        /// 1- Erro ao Enviar E-mail de Criação de TFA
        /// </summary>
        [Description("Erro ao Enviar E-mail de Criação de TFA")]
        ErroEnvioEmailNovoTfa = 1,

        /// <summary>
        /// 2 - Erro ao Enviar E-mail de Edição de TFA
        /// </summary>
        [Description("Erro ao Enviar E-mail de Edição de TFA")]
        ErroEnvioEmailEditarTfa = 2,

        /// <summary>
        /// 3 - Erro ao Enviar E-mail de Aceite de TFA
        /// </summary>
        [Description("Erro ao Enviar E-mail de Aceite de TFA")]
        ErroEnvioEmailAceiteTfa = 3,

        /// <summary>
        /// 4 - Erro ao Enviar E-mail de Recomendação de Manutenção de Vagão
        /// </summary>
        [Description("Erro ao Enviar E-mail de Recomendação de Manutenção de Vagão")]
        ErroEnvioEmailManutencaoVagao = 4
    }
}