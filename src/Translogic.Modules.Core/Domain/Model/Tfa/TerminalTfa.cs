﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    /// Classe de Terminal para gestão de envio de emails para notificação sobre TFAs
    /// </summary>
    public class TerminalTfa : EntidadeBase<int?>
    {
        /// <summary>
        /// ID do Terminal
        /// </summary>
        public virtual int IdTerminal { get; set; }
    }
}