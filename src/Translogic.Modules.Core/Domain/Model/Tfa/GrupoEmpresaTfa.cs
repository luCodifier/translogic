﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    /// Grupo de Empresa para Gestão de TFAs
    /// </summary>
    public class GrupoEmpresaTfa : EntidadeBase<int?>
    {
        /// <summary>
        /// Nome do Grupo de Empresas
        /// </summary>
        public virtual string NomeGrupo { get; set; }

        /// <summary>
        /// Lista da Empresas vinculadas ao Grupo de Empresas
        /// </summary>
        public virtual IList<EmpresaGrupoTfa> Empresas { get; set; }
    }
}