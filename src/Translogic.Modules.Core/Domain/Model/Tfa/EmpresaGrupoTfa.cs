﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    /// Empresa participande de um Grupo de Empresas para Gestão de TFAs
    /// </summary>
    public class EmpresaGrupoTfa : EntidadeBase<int?>
    {
        /// <summary>
        /// Grupo de Empresas no qual a empresa está vinculada
        /// </summary>
        public virtual GrupoEmpresaTfa GrupoEmpresa { get; set; }

        /// <summary>
        /// Empresa vinculada ao Grupo de Empresas
        /// </summary>
        public virtual IEmpresa Empresa { get; set; }
    }
}