﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe de Tipos de Gambitos identificados no TFA (1 - Gambitos OK, 2 - Gambitos Parcial, 3 - Gambitos Ausentes)
    /// </summary>
    public class TipoGambitoTfa : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        ///   Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}