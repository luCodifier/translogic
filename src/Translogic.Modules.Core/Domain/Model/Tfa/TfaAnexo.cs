﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Via;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.Tfa
{
    /// <summary>
    ///   Classe TfaAnexo
    /// </summary>
    public class TfaAnexo : EntidadeBase<int?>
    {
        #region Propriedades

        /// <summary>
        /// Número do Processo de Seguro Vinculado ao TFA
        /// </summary>
        public virtual decimal NumProcesso { get; set; }

        /// <summary>
        /// TFA vinculado ao anexo
        /// </summary>
        public virtual Tfa Tfa { get; set; }

        /// <summary>
        /// Arquivo PDF do TFA (binário)
        /// </summary>
        public virtual byte[] ArquivoPdf { get; set; }

        #endregion
    }
}