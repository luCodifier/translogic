﻿
namespace Translogic.Modules.Core.Domain.Model.Rota
{
    using ALL.Core.Dominio;
    using System;

    public class GrupoRota : EntidadeBase<int>
    {
        public virtual string Grupo { get; set; }
      
        public virtual string Usuario { get; set; }

        public GrupoRota()
        {
            VersionDate = DateTime.Now;
        }

    }
}