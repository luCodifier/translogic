﻿namespace Translogic.Modules.Core.Domain.Model.Rota.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using System;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IGrupoRotaRepository : IRepository<GrupoRota, int>
    {
        ResultadoPaginado<GrupoRotaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal);

        IList<GrupoRotaDto> ObterGrupoRotas();
        IList<RotaGrupoRotaDto> ObterRotas();
        GrupoRota Obter(int id);

        void Salvar(GrupoRota grupoRota);
        void Excluir(GrupoRota grupoRota);

        ResultadoPaginado<GrupoRotaDto> ObterAssociacoes(
            DetalhesPaginacaoWeb detalhesPaginacaoWeb,
            DateTime? dataInicial, DateTime? dataFinal,
            string grupoRota, string origem,
            string destino, string rota);
    }
}