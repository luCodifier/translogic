﻿namespace Translogic.Modules.Core.Domain.Model.Rota.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using System;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IGrupoRotaRotaRepository : IRepository<GrupoRotaRota, int>
    {
        GrupoRotaRota Obter(int id);

        void Salvar(GrupoRotaRota grupoRota);
        void Excluir(GrupoRotaRota grupoRota);

        ResultadoPaginado<GrupoRotaRotaDto> ObterAssociacoes(
            DetalhesPaginacaoWeb detalhesPaginacaoWeb,
            DateTime? dataInicial, DateTime? dataFinal,
            string grupoRota, string origem,
            string destino, string rota);
    }
}