﻿namespace Translogic.Modules.Core.Domain.Model.Rota.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using System;
    using System.Collections.Generic;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IGrupoRotaLimiteRepository : IRepository<Limite, int>
    {
        ResultadoPaginado<LimiteDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal, string grupo, string frota, string mercadoria);
        
        IList<FrotaDto> ObterFrotas();
        Limite ObterLimite(int id); 

        void Salvar(Limite limite);
        void Excluir(Limite limite);
    }
}