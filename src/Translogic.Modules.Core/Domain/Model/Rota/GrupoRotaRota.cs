﻿
namespace Translogic.Modules.Core.Domain.Model.Rota
{
    using ALL.Core.Dominio;
    using System;
    public class GrupoRotaRota : EntidadeBase<int>
    {
        public virtual int IdGrupo { get; set; }
        public virtual int IdRota { get; set; }
        public virtual string Usuario { get; set; }

        public GrupoRotaRota()
        {
            VersionDate = DateTime.Now;
        }
    }
}