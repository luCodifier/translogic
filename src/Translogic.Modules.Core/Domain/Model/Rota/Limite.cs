﻿namespace Translogic.Modules.Core.Domain.Model.Rota
{
    using ALL.Core.Dominio;
    using System;

    public class Limite : EntidadeBase<int>
    {
        public virtual int IdGrupo { get; set; }
        public virtual int IdFrota { get; set; }
        public virtual int IdMercadoria { get; set; }
        public virtual decimal Peso { get; set; }

        public virtual string Usuario { get; set; }

        public Limite()
        {
            VersionDate = DateTime.Now;
        }

    }
}