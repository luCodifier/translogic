namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio de empresa cliente
	/// </summary>
	public interface IEmpresaClienteRepository : IRepository<EmpresaCliente, int?>
	{
		/// <summary>
		/// M�todo para obter a empresa cliente por cnpj
		/// </summary>
		/// <param name="cnpj"> Cnpj da empresa </param>
		/// <returns> Retorna e empresa cliente </returns>
		EmpresaCliente ObterPorCnpj(string cnpj);
        
		/// <summary>
		/// Obtem empresa cliente pela sigla do SAP
		/// </summary>
		/// <param name="siglaSap">Sigla do SAP </param>
		/// <returns>Retorna a empresa cliente</returns>
		EmpresaCliente ObterPorSiglaSap(string siglaSap);

		/// <summary>
		/// Obt�m a empresa pelo CNPJ
		/// </summary>
		/// <param name="cnpj"> Cnpj da empresa. </param>
		/// <param name="codigoUf"> Codigo da Unidade federativa. </param>
		/// <returns> Retorna objeto empresa cliente </returns>
		EmpresaCliente ObterPorCnpj(string cnpj, string codigoUf);

		/// <summary>
		/// Obt�m todas empresas que possuam o cnpj
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa</param>
		/// <returns>Lista de empresas</returns>
		IList<EmpresaCliente> ObterEmpresasPorCnpj(string cnpj);

        /// <summary>
        /// Obt�m todas empresas cujo Cnpj cont�m uma parte do cnpj informado como par�metro
        /// </summary>
        /// <param name="cnpj">Cnpj da empresa (Parcial ou Completo)</param>
        /// <returns>Lista de empresas</returns>
	    IList<EmpresaCliente> ObterEmpresasContemCnpj(string cnpj);

		/// <summary>
		/// Obt�m a empresa Cliente pelo cnpj da ferrovia
		/// </summary>
		/// <param name="cnpj">CNPJ a ser consultado</param>
		/// <returns>Objeto EmpresaCliente</returns>
		Empresa ObterEmpresaClientePorCnpjDaFerrovia(string cnpj);

		/// <summary>
		/// Obt�m a empresa Cliente pelo cnpj da ferrovia
		/// </summary>
		/// <param name="cnpj">CNPJ a ser consultado</param>
		/// <param name="uf">Unidade Federativa</param>
		/// <returns>Objeto EmpresaCliente</returns>
		Empresa ObterEmpresaClientePorCnpjDaFerrovia(string cnpj, string uf);

		/// <summary>
		/// Obt�m a empresa Cliente pelo cnpj ou razao social
		/// </summary>
		/// <param name="cnpj">CNPJ a ser consultado</param>
		/// <param name="razaoSocial">Razao Social da empresa</param>
		/// <returns>Objeto EmpresaCliente</returns>
		IList<EmpresaCliente> ObterEmpresaClientePorCnpjRazaoSocialDaFerrovia(string cnpj, string razaoSocial);

	    /// <summary>
	    /// Lista de cliente
	    /// </summary>
	    /// <param name="query"> Filtro para os clientes</param>
	    /// <returns>Lista de clientes</returns>
        List<EmpresaCliente> ObterListaDescResumida(string query);


        /// <summary>
        /// Lista de cliente - Auto complete bem r�pido (Melhor peformance)
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista de clientes</returns>
        List<EmpresaPesquisaPorNome> ObterListaNomesDescResumida(string query);

	    /// <summary>
	    /// Lista de Terminais
	    /// </summary>
	    /// <param name="query"> Filtro para os terminais</param>
	    /// <returns>Lista de terminais</returns>
	    List<EmpresaCliente> ObterListaNomesTerminais(string query);

        List<TaraEmpresa> GetAllTarasEmpresa();
    }
}