namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de empresa interface Cte
	/// </summary>
	public interface IEmpresaInterfaceCteRepository : IRepository<EmpresaInterfaceCte, int?>
	{
		/// <summary>
		/// Obtem a empresa pela Sigla
		/// </summary>
		/// <param name="siglaEmpresaFerrovia">Sigla da empresa</param>
		/// <param name="uf"> Estado da empresa</param>
		/// <returns>Retorna EmpresaInterfaceCte</returns>
		EmpresaInterfaceCte ObterPorSiglaUF(string siglaEmpresaFerrovia, string uf);
	}
}