namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de empresa
	/// </summary>
	public interface IEmpresaRepository : IRepository<Empresa, int?>
	{
        /// <summary>
        /// Retorna a Empresa por CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ da Empresa</param>
        /// <returns>Objeto Empresa</returns>
        Empresa ObterPorCnpj(string cnpj);
	}
}