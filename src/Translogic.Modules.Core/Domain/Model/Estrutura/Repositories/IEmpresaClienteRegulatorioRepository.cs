﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Diversos;
    using Diversos.Ibge;
    using Translogic.Modules.Core.Domain.Model.Estrutura;

    /// <summary>
    /// Repositorio de clientes
    /// </summary>
    public interface IEmpresaClienteRegulatorioRepository : IRepository<Empresa, int?>
    {
        /// <summary>
        /// Lista os clientes por razão social
        /// </summary>
        /// <param name="razaoSocial">Nome a pesquisar</param>
        /// <returns>Clientes resultantes</returns>
        IEnumerable<Empresa> ListarPorRazaoSocial(string razaoSocial);

        /// <summary>
        /// Lista os clientes por CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ a pesquisar</param>
        /// <returns>Clientes resultantes</returns>
        IEnumerable<Empresa> ListarPorCNPJ(string cnpj);

        /// <summary>
        /// Obtem o cliente pelo código
        /// </summary>
        /// <param name="sigla">Código a pesquisar</param>
        /// <returns>Cliente resultante</returns>
        IEnumerable<Empresa> ListarPorSigla(string sigla);

        /// <summary>
        /// Método para Obter os Dados da Tabela Categorias
        /// </summary>
        /// <param name="pagination"> Paginação da Grid </param>
        /// <param name="razaoSocial"> Razão social </param>
        /// <param name="nomeFantasia"> Nome fantasia </param>
        /// <param name="cnpj"> Cnpj da empresa </param>     
        /// <returns> Retorna Ilist de Categorias </returns>
        ResultadoPaginado<Empresa> ObterPorParametro(DetalhesPaginacao pagination, string razaoSocial, string nomeFantasia, string cnpj);

        /// <summary>
        /// Lista todos os estados (UFs)
        /// </summary>
        /// <returns>Listagem dos estados</returns>
        IEnumerable<Estado> ListarEstados();

        /// <summary>
        /// Lista as cidades de um determinado estado
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Cidades do estado</returns>
        IEnumerable<CidadeIbge> ListarCidades(string sigla);

        /// <summary>
        /// Obtem o estado (UF)
        /// </summary>
        /// <param name="sigla">Estado desejado</param>
        /// <returns>Obtem o estado desejado</returns>
        Estado ObterEstado(string sigla);

        /// <summary>
        /// Obtem a cidade
        /// </summary>
        /// <param name="cidadeId">Id da cidade desejada</param>
        /// <returns>Obtem a cidade desejada</returns>
        CidadeIbge ObterCidade(int cidadeId);
    }
}