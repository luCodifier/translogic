﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Diversos;
    
    /// <summary>
    /// Interface de repositório de Unidade de Produção
    /// </summary>
    public interface IUnidadeProducaoRepository : IRepository<UnidadeProducao, int?>
    {
		/// <summary>
		/// Obtem todas as unidades de produção.
		/// </summary>
		/// <param name="malha"> A malha onde está o ponto de abastecimento </param>			
		/// <returns>Lista de Unidade de produção</returns>
    	IList<UnidadeProducao> ObterPorMalha(Malha malha);

    	/// <summary>
    	/// Obtem todas as unidades de produção filtrando por Id.
    	/// </summary>
    	/// <param name="listaIdsUps">Lista de Id para filtro </param>			
    	/// <returns>Lista de Unidade de produção</returns>
    	IList<UnidadeProducao> ObterPorListaId(int[] listaIdsUps);
    }
}
