﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
    using ALL.Core.AcessoDados;
    
    /// <summary>
    /// Interface de repositório de EmpresaClientePais
    /// </summary>
    public interface IEmpresaClientePaisRepository : IRepository<EmpresaClientePais, int?>
    {
        /// <summary>
        /// Obtém o país da empresa
        /// </summary>
        /// <param name="empresa">Empresa cliente</param>
        /// <returns>Objeto EmpresaClientePais</returns>
        EmpresaClientePais ObterPorEmpresa(EmpresaCliente empresa);
    }
}
