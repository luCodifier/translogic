namespace Translogic.Modules.Core.Domain.Model.Estrutura.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio de empresa ferrovia
	/// </summary>
	public interface IEmpresaFerroviaRepository : IRepository<EmpresaFerrovia, int?>
	{
		/// <summary>
		/// Obt�m a empresa ferrovia pela sigla da empresa
		/// </summary>
		/// <param name="siglaEmpresaFerrovia">Sigla da empresa Ferrovia</param>
		/// <returns>Retorna a empresa ferrovia</returns>
		EmpresaFerrovia ObterPorSigla(string siglaEmpresaFerrovia);

		/// <summary>
		/// Obt�m a empresa ferrovia pela sigla da empresa e pela unidade federativa
		/// </summary>
		/// <param name="siglaEmpresaFerrovia">Sigla da empresa Ferrovia</param>
		/// <param name="uf">Unidade Federativa</param>
		/// <returns>Retorna a empresa ferrovia</returns>
		EmpresaFerrovia ObterPorSiglaUf(string siglaEmpresaFerrovia, string uf);
	}
}