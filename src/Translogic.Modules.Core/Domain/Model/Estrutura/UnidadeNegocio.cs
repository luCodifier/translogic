﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa as Unidades de Negócio de uma <see cref="EmpresaFerrovia">Empresa Ferroviária</see>
	/// </summary>
	public class UnidadeNegocio : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Indicador de Ativo
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Código da Unidade de Negócio
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição Detalhada da Unidade de Negócio
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição Resumida da Unidade de Negócio
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary cref="EmpresaFerrovia">
		/// Empresa da Unidade de Produção
		/// </summary>
		/// <see cref="EmpresaFerrovia"/>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary cref="Malha">
		/// Malha da Unidade de Negócio
		/// </summary>
		public virtual Malha Malha { get; set; }

		#endregion
	}
}