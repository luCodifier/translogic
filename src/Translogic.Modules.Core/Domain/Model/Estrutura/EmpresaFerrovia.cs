﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	/// <summary>
	/// Representa uma Empresa que faz "operações" nas linhas da ALL
	/// </summary>
	/// <remarks>
	/// Herda os atributos da classe Empresa
	/// </remarks>
	public class EmpresaFerrovia : Empresa
	{
	}
}