namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	/// <summary>
	/// Representa uma Empresa utilizado para o cte
	/// </summary>
	/// <remarks>
	/// Herda os atributos da classe Empresa
	/// </remarks>
	public class EmpresaInterfaceCte : Empresa
	{
	}
}