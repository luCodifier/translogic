﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
    using ALL.Core.Dominio;
    using System;

    public class TaraEmpresa
    {
        #region PROPRIEDADES
        public virtual decimal IdTaraEmpresa { get; set; }
        public virtual decimal IdEmpresa { get; set; }
        #endregion
    }
}