namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	using ALL.Core.Dominio;
	using Diversos;
	using Diversos.Ibge;

	/// <summary>
	/// Interface de Empresa
	/// </summary>
	public interface IEmpresa : IIdentificavel<int?>, IVersionavel
	{
		#region PROPRIEDADES

		/// <summary>
		/// Apelido da empresa
		/// </summary>
		string Apelido { get; set; }

		/// <summary>
		/// Celular da empresa
		/// </summary>
		string Celular { get; set; }

		/// <summary>
		/// CEP da Empresa
		/// </summary>
		string Cep { get; set; }

		/// <summary>
		/// Cgc da empresa quando o TipoPessoa for pessoa jur�dica
		/// </summary>
		string Cgc { get; set; }

		/// <summary>
		/// Nome do contato da empresa
		/// </summary>
		string Contato { get; set; }

		/// <summary>
		/// CPF da empresa quando o TipoPessoa for pessoa fisica
		/// </summary>
		string Cpf { get; set; }

		/// <summary>
		/// Descricao Detalhada da empresa
		/// </summary>
		string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descricao Resumida da Empresa
		/// </summary>
		string DescricaoResumida { get; set; }

		/// <summary>
		/// Email da empresa
		/// </summary>
		string Email { get; set; }

		/// <summary>
		/// Email da empresa para envio de xml
		/// </summary>
		string EmailXml { get; set; }

		/// <summary>
		/// Email da empresa para envio de pdf
		/// </summary>
		string EmailNfe { get; set; }

		/// <summary>
		/// Caso seja empresa agrupada
		/// </summary>
		IEmpresa EmpresaMae { get; set; }

		/// <summary>
		/// Endereco da empresa
		/// </summary>
		string Endereco { get; set; }

		/// <summary>
		/// <see cref="Translogic.Modules.Core.Domain.Model.Diversos.Estado"/> da Empresa
		/// </summary>
		Estado Estado { get; set; }

		/// <summary>
		/// Cidade com o c�digo do IBGE
		/// </summary>
		CidadeIbge CidadeIbge { get; set; }

		/// <summary>
		/// Fax da Empresa
		/// </summary>
		string Fax { get; set; }

		/// <summary>
		/// Indica se a empresa � para ser atendida preferencialmente
		/// </summary>
		bool? IndAtendimentoPreferencial { get; set; }

		/// <summary>
		/// Indica se a empresa pertence � ALL
		/// </summary>
		bool? IndEmpresaALL { get; set; }

		/// <summary>
		/// Inscricao Estadual da empresa
		/// </summary>
		string InscricaoEstadual { get; set; }

		/// <summary>
		/// Incricao Municipal da empresa
		/// </summary>
		string InscricaoMunicipal { get; set; }

		/// <summary>
		/// Nome Fantasia da empresa
		/// </summary>
		string NomeFantasia { get; set; }

		/// <summary>
		/// Observa��o da empresa
		/// </summary>
		string Observacao { get; set; }

		/// <summary>
		/// Raz�o social da empresa
		/// </summary>
		string RazaoSocial { get; set; }

		/// <summary>
		/// Sigla da empresa
		/// </summary>
		string Sigla { get; set; }

		/// <summary>
		/// Telefone1 da empresa
		/// </summary>
		string Telefone1 { get; set; }

		/// <summary>
		/// Telefone2 da empresa
		/// </summary>
		string Telefone2 { get; set; }

		/// <summary>
		/// Telex da empresa
		/// </summary>
		string Telex { get; set; }

		/// <summary>
		/// Tipo da Empresa - Cliente ou Ferrovia
		/// </summary>
		string Tipo { get; set; }

		/// <summary>
		/// Endere�o do SAP
		/// </summary>
		string EnderecoSap { get; set; }

		/// <summary>
		/// Tipo de pessoa - F�sica ou Jur�dica
		/// </summary>
		TipoPessoaEnum? TipoPessoa { get; set; }

        /// <summary>
        /// Tipo de Faturamento Bloqueado
		/// </summary>
        bool FaturamentoBloqueado { get; set; }

        /// <summary>
        /// Modal da empresa
        /// </summary>
        char Modal { get; set; }

		#endregion
	}
}