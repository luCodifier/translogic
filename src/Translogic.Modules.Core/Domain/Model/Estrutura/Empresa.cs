﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;
	using Diversos.Ibge;

	/// <summary>
	/// Representa uma super-classe genérica de Empresa, pois <see cref="EmpresaCliente"/> e <see cref="EmpresaFerrovia"/> possuem os mesmos atributos
	/// </summary>
	public abstract class Empresa : EntidadeBase<int?>, IEmpresa
	{
		#region IEmpresa MEMBROS

		/// <summary>
		/// Apelido da Empresa
		/// </summary>
		public virtual string Apelido { get; set; }

		/// <summary>
		/// Celular da Empresa
		/// </summary>
		public virtual string Celular { get; set; }

		/// <summary>
		/// Cep do Endereço da Empresa
		/// </summary>
		public virtual string Cep { get; set; }

		/// <summary>
		/// CGC, quando o atributo TipoPessoa estiver setado com o valor 'J'
		/// </summary>
		public virtual string Cgc { get; set; }

		/// <summary>
		/// Nome da pessoa de contato da Empresa
		/// </summary>
		public virtual string Contato { get; set; }

		/// <summary>
		/// CPF, quando o atributo TipoPessoa estiver setado com o valor 'F'
		/// </summary>
		public virtual string Cpf { get; set; }

		/// <summary>
		/// Descrição Detalhada da Empresa
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição resumida da Empresa
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// Email da Empresa
		/// </summary>
		public virtual string Email { get; set; }

		/// <summary>
		/// Email da empresa para envio de xml
		/// </summary>
		public virtual string EmailXml { get; set; }

		/// <summary>
		/// Email da empresa para envio de pdf
		/// </summary>
		public virtual string EmailNfe { get; set; }

		/// <summary>
		/// Quando uma empresa pertencer a um grupo de empresas, este atributo deve ser preenchido com a Empresa do Grupo
		/// </summary>
		public virtual IEmpresa EmpresaMae { get; set; }

		/// <summary>
		/// Endereço da Empresa
		/// </summary>
		public virtual string Endereco { get; set; }

		/// <summary>
		/// <see cref="Estado"/> em que a Empresa está localizada
		/// </summary>
		public virtual Estado Estado { get; set; }

		/// <summary>
		/// <see cref="CidadeIbge"/> que tem o código da cidade no IBGE
		/// </summary>
		public virtual CidadeIbge CidadeIbge { get; set; }

		/// <summary>
		/// Fax da Empresa
		/// </summary>
		public virtual string Fax { get; set; }

		/// <summary>
		/// Indica se a empresa deve ser atendida preferencialmente
		/// </summary>
		public virtual bool? IndAtendimentoPreferencial { get; set; }

		/// <summary>
		/// Indica se a Empresa pertence ou não à ALL
		/// </summary>
		public virtual bool? IndEmpresaALL { get; set; }

		/// <summary>
		/// Inscrição Estadual da Empresa
		/// </summary>
		public virtual string InscricaoEstadual { get; set; }

		/// <summary>
		/// Inscrição Municipal da Empresa
		/// </summary>
		public virtual string InscricaoMunicipal { get; set; }

		/// <summary>
		/// Nome Fantasia da Empresa
		/// </summary>
		public virtual string NomeFantasia { get; set; }

		/// <summary>
		/// Observação referente à empresa
		/// </summary>
		public virtual string Observacao { get; set; }

		/// <summary>
		/// Razão Social da Empresa
		/// </summary>
		public virtual string RazaoSocial { get; set; }

		/// <summary>
		/// Sigla da Empresa
		/// </summary>
		public virtual string Sigla { get; set; }

		/// <summary>
		/// Telefone primário da Empresa
		/// </summary>
		public virtual string Telefone1 { get; set; }

		/// <summary>
		/// Telefone secundário da Empresa
		/// </summary>
		public virtual string Telefone2 { get; set; }

		/// <summary>
		/// Telex da Empresa
		/// </summary>
		public virtual string Telex { get; set; }

		/// <summary>
		/// Tipo da Empresa
		/// </summary>
		/// <remarks>
		/// Este campo não possui o atributo DataMember, pois não é serializado.
		/// É este campo que define se a empresa é <see cref="EmpresaCliente"/> ou <see cref="EmpresaFerrovia"/>
		/// </remarks>
		public virtual string Tipo { get; set; }

		/// <summary>
		/// Endereço do SAP
		/// </summary>
		public virtual string EnderecoSap { get; set; }

		/// <summary>
		/// Tipo de Pessoa (Fisica/Juridica) - <see cref="TipoPessoaEnum"/>
		/// </summary>
		public virtual TipoPessoaEnum? TipoPessoa { get; set; }

        /// <summary>
        /// Tipo de Faturamento Bloqueado
        /// </summary>
        public virtual bool FaturamentoBloqueado { get; set; }

        /// <summary>
        /// Modal da empresa
        /// </summary>
        public virtual char Modal { get; set; }

		#endregion
	}
}