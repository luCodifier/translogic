﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
    using System;
    using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa uma Unidade de Produção
	/// </summary>
	public class UnidadeProducao : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Unidade de Produção
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição Detalhada da Unidade de Produção
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição Resumida da Unidade de Produção
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary cref="EmpresaFerrovia">
		/// Empresa da Unidade de Produção
		/// </summary>
		/// <see cref="EmpresaFerrovia"/>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Malha da Unidade de Produção
		/// </summary>
		public virtual Malha Malha { get; set; }

		#endregion
	}
}