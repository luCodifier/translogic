﻿namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	/// <summary>
	/// Representa uma Empresa que é Cliente da ALL
	/// </summary>
	/// <remarks>
	/// Herda os atributos da classe Empresa
	/// </remarks>
	public class EmpresaCliente : Empresa
	{
	}
}