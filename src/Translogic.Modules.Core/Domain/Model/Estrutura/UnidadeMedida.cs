namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa uma Unidade de Medida
	/// </summary>
	public class UnidadeMedida : EntidadeBase<string>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descri��o da Unidade de Medida
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Tipo da Unidade de medida  - <see cref="TipoUnidadeMedidaEnum"/>
		/// </summary>
		public virtual TipoUnidadeMedidaEnum Tipo { get; set; }

		#endregion
	}
}