﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
    public class EmpresaPesquisaPorNome
    {
        public virtual int Id { get; set; }

        public virtual string DescricaoResumida { get; set; }

        public virtual string Cgc { get; set; }
    }
}