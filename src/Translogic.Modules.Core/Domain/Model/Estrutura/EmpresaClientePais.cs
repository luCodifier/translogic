namespace Translogic.Modules.Core.Domain.Model.Estrutura
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa uma Unidade de Medida
	/// </summary>
	public class EmpresaClientePais : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Pa�s da empresa
		/// </summary>
		public virtual string Pais { get; set; }

		/// <summary>
		/// Empresa cliente
		/// </summary>
		public virtual EmpresaCliente EmpresaCliente { get; set; }

		#endregion
	}
}