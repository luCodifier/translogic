﻿

namespace Translogic.Modules.Core.Domain.Model.RelatorioFaturamento
{
    using ALL.Core.Dominio;

    public class RelatorioFaturamento : EntidadeBase<int>
    {

        #region Propriedades

        /// <summary>
        /// Código do painel de expedicao
        /// </summary>
        public virtual string Codigo { get; set; }

        #endregion

    }
}