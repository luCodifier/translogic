﻿

namespace Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao
{
    using ALL.Core.Dominio;

    public class RelatorioFichaRecomendacao : EntidadeBase<int>
    {

        #region Propriedades

        /// <summary>
        /// Numero OS
        /// </summary>
        public decimal NumOS { get; set; }
        /// <summary>
        /// Prefixo
        /// </summary>
        public string Prefixo { get; set; }
        /// <summary>
        /// Origem
        /// </summary>
        public string Origem { get; set; }
        /// <summary>
        /// Destino
        /// </summary>
        public string Destino { get; set; }
        /// <summary>
        /// Local
        /// </summary>
        public string Local { get; set; }
        /// <summary>
        /// Data Chegada Prevista
        /// </summary>
        public string DtChegadaPrevista { get; set; }
        /// <summary>
        /// Quantidade Avarivas
        /// </summary>
        public string QtdAvaria { get; set; }
        /// <summary>
        /// Quantidade Vagões
        /// </summary>
        public string QtdVagao { get; set; }

        #endregion

    }
}