﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto;
using System;
using Translogic.Core.Infrastructure.Web;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.RelatorioFichaRecomendacao.Repositories
{
    public interface IRelatorioFichaRecomendacaoRepository : IRepository<RelatorioFichaRecomendacao, int>
    {
        /// <summary>
        /// Retornar a consulta para grid Trens Recomendados
        /// </summary>
        /// <param name="detalhesPaginacao"></param>
        /// <param name="trem"></param>
        /// <param name="numOS"></param>
        /// <param name="avaria"></param>
        /// <returns></returns>
        ResultadoPaginado<RelatorioFichaRecomendacaoDto> ObterTrensRecomendados(DetalhesPaginacaoWeb detalhesPaginacao, string trem, decimal? numOS, string avaria);

        /// <summary>
        ///  Retornar a consulta para Ficha de Recomendação exportação e impressão
        /// </summary>
        /// <param name="numOS"></param>
        /// <returns></returns>
        IList<RelatorioFichaRecomendacaoExportDto> ObterListaFichaRecomendacaoExportar(decimal numOS);
    }
}