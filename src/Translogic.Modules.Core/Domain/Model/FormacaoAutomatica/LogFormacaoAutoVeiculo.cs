﻿namespace Translogic.Modules.Core.Domain.Model.FormacaoAutomatica
{
    using Acesso;
    using ALL.Core.Dominio;
    using System;

    /// <summary>
    /// Representa Formacao automatica veiculos
    /// </summary>
    public class LogFormacaoAutoVeiculo : EntidadeBase<int>
    {
        #region PROPRIEDADES
        /// <summary>
        /// LFA_ID
        /// </summary>
        public virtual LogFormacaoAuto Lfa { get; set; }

        /// <summary>
        /// LFAV_NUM_VEI
        /// </summary>        
        public virtual decimal LfavNumVei { get; set; }

        /// <summary>
        /// LFAV_TP_VEICULO
        /// </summary>        
        public virtual decimal LfavTpVei { get; set; }

        /// <summary>
        /// LFAV_POSIT
        /// </summary>        
        public virtual string LfavPosit { get; set; }

        /// <summary>
        /// LFAV_DESC_ANEX
        /// </summary>        
        public virtual string LfaDescricaoAnex { get; set; }

        /// <summary>
        /// LFA TENTATIVAS
        /// </summary>        
        public virtual DateTime LfavTimestamp { get; set; }
        #endregion
    }
}