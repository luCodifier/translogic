﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;
namespace Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories
{

    /// <summary>
    /// Interface de repositório de Consulta Trem
    /// </summary>
    public interface ILogFormacaoAutoVeiculoRepository : IRepository<LogFormacaoAutoVeiculo, int>
    {
        ResultadoPaginado<FormacaoAutoVeiculoDto> ObterConsultaFormacaoAutomaticaVeiculos(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os);
    }
}