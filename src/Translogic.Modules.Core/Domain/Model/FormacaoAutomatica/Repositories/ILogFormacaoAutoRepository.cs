﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
namespace Translogic.Modules.Core.Domain.Model.FormacaoAutomatica.Repositories
{

    /// <summary>
    /// Interface de repositório de Consulta Trem
    /// </summary>
    public interface ILogFormacaoAutoRepository : IRepository<LogFormacaoAuto, int>
    {
        /// <summary>
        /// Pesquisar Formacao Automatica
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="formacaoAutomaticaRequestDto">Filtros pesquisa</param>
        /// <returns>Lista de dados para GRID</returns>
        ResultadoPaginado<FormacaoAutoDto> ObterConsultaFormacaoAutomatica(DetalhesPaginacaoWeb detalhesPaginacaoWeb, FormacaoAutoRequestDto formacaoAutomaticaRequestDto);

        IEnumerable<FormacaoAutoDto> ObterConsultaFormacaoAutomaticaExportar(FormacaoAutoRequestDto formacaoAutoRequestDto);

        void ZeraTentativas(decimal OS);
    }
}