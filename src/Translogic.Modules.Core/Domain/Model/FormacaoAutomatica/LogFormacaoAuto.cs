﻿namespace Translogic.Modules.Core.Domain.Model.FormacaoAutomatica
{
    using Acesso;
    using ALL.Core.Dominio;
    using System;

    /// <summary>
    /// Representa Formacao automatica
    /// </summary>
    public class LogFormacaoAuto : EntidadeBase<int>
    {
        #region PROPRIEDADES
        /// <summary>
        /// OS
        /// </summary>
        public virtual decimal OS { get; set; }

        /// <summary>
        /// LFA ORIGEM
        /// </summary>        
        public virtual string LfaOrigem  { get; set; }

        /// <summary>
        /// LFA DESTINO
        /// </summary>        
        public virtual string LfaDestino { get; set; }

        /// <summary>
        /// LFA DESCRIÇÃO OS
        /// </summary>        
        public virtual string LfaDescricaoOS { get; set; }

        /// <summary>
        /// LFA DESCRIÇÃO FTREM
        /// </summary>        
        public virtual string LfaDescricaoFTREM { get; set; }

        /// <summary>
        /// LFA DESCRIÇÃO LIB
        /// </summary>        
        public virtual string LfaDescricaoLIB { get; set; }
        
        /// <summary>
        /// LFA TENTATIVAS
        /// </summary>        
        public virtual int LfaTentativas { get; set; }

        /// <summary>
        /// TIMESTAMP
        /// </summary>
        public virtual DateTime LfaTimestamp { get; set; }
        #endregion
    }
}