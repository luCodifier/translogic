﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;
	using Equipagem;
	using Veiculo;
	using Via;

	/// <summary>
	/// Representa a composição de um <see cref="Trem"/>.
	/// Um Trem pode possuir várias composições durante o percurso de sua Rota
	/// </summary>
	public class ComposicaoEquipamento : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
        /// Equipamento da Composicao
        /// </summary>
        public virtual Equipamento Equipamento { get; set; }

        /// <summary>
        /// Composicao da CompMaquinista
        /// </summary>
        public virtual Composicao Composicao { get; set; }

        #endregion
	}
}