namespace Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using DataAccess.Repositories.Trem.MovimentacaoTrem;
	using Via;

	/// <summary>
	/// Interface para o reposit�rio <see cref="MovimentacaoTremRepository"/>
	/// </summary>
	public interface IMovimentacaoTremRepository : IRepository<MovimentacaoTrem, int?>
	{
		/// <summary>
		/// Obt�m as movimenta��es dos trens que passaram em determinado per�odo e area operacional
		/// </summary>
		/// <param name="dataInicio">Data inicial do per�odo</param>
		/// <param name="areaOperacional">Area operacional - <see cref="IAreaOperacional"/></param>
		/// <returns>Lista de movimenta��es</returns>
		IList<MovimentacaoTrem> ObterPorPeriodoAreaOperacional(DateTime dataInicio, IAreaOperacional areaOperacional);

		/// <summary>
		/// Obt�m as movimenta��es dos trens que passaram em determinado per�odo e area operacional
		/// </summary>
		/// <param name="trem">Trem - <see cref="Trem"/></param>
		/// <param name="areaOperacional">Area operacional - <see cref="IAreaOperacional"/></param>
		/// <returns>Lista de movimenta��es</returns>
		MovimentacaoTrem ObterPorTremAreaOperacional(Model.Trem.Trem trem, IAreaOperacional areaOperacional);
	}
}