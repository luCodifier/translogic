﻿namespace Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface para o repositório <see cref="MovimentacaoTremRealRepository"/>
    /// </summary>
    public interface IMovimentacaoTremRealRepository : IRepository<MovimentacaoTremReal, int>
    {
    }
}