﻿namespace Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos;

    /// <summary>
    /// Movimentacao de trem real - Data partida e chegada recuperada do PX/ATW
    /// </summary>
    public class MovimentacaoTremReal : EntidadeBase<int>
    {
        /// <summary>
        /// Data de Chegada Prevista
        /// </summary>
        /// <remarks>
        /// Muda conforme os apontamentos do usuário
        /// </remarks>
        public virtual DateTime? DataPartidaOrigemReal { get; set; }

        /// <summary>
        /// Data de Chegada Prevista
        /// </summary>
        /// <remarks>
        /// Muda conforme os apontamentos do usuário
        /// </remarks>
        public virtual DateTime? DataChegadaDestinoReal { get; set; }

        /// <summary>
        /// Situação do Trem no PX
        /// </summary>
        public virtual SituacaoTremPxEnum? SituacaoTremPx { get; set; }
    }
}