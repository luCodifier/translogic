﻿namespace Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Via;
	using Via.Circulacao;

	/// <summary>
	/// Representa a ocorrência da Movimentação que um Trem está realizando
	/// </summary>
	public class MovimentacaoTrem : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Rota da area operacional - <see cref="Translogic.Modules.Core.Domain.Model.Via.Circulacao.AreaOperacionalRota"/>
		/// </summary>
		public virtual AreaOperacionalRota AreaOperacionalRota { get; set; }

		/// <summary>
		/// Quantidade de horas de atraso.
		/// </summary>
		public virtual int? Atraso { get; set; }

		/// <summary>
		/// Composição em que o trem possui no momento
		/// </summary>
		public virtual Composicao Composicao { get; set; }

		/// <summary>
		/// Data de Chegada Prevista
		/// </summary>
		/// <remarks>
		/// Muda conforme os apontamentos do usuário
		/// </remarks>
		public virtual DateTime? DataChegadaPrevista { get; set; }

		/// <summary>
		/// Data de Chegada Programada
		/// </summary>
		/// <remarks>
		/// Gravado na Formação do Trem e não é alterada conforme os apontamentos
		/// </remarks>
		public virtual DateTime? DataChegadaProgramada { get; set; }

		/// <summary>
		/// Data de Chegada Realizada, apontado pelo usuário no Translogic
		/// </summary>
		public virtual DateTime? DataChegadaRealizada { get; set; }

		/// <summary>
		/// Data de Chegada Realizada, apontado pelo Sistema ACT
		/// </summary>
		public virtual DateTime? DataChegadaRealizadaACT { get; set; }

		/// <summary>
		/// Data de Saída Prevista
		/// </summary>
		/// <remarks>
		/// Muda conforme os apontamentos do usuário
		/// </remarks>
		public virtual DateTime? DataSaidaPrevista { get; set; }

		/// <summary>
		/// Data de Chegada Programada
		/// </summary>
		/// <remarks>
		/// Gravado na Formação do Trem e não é alterada conforme os apontamentos
		/// </remarks>
		public virtual DateTime? DataSaidaProgramada { get; set; }

		/// <summary>
		/// Data de Saída Realizada, apontado pelo usuário no Translogic
		/// </summary>
		public virtual DateTime? DataSaidaRealizada { get; set; }

		/// <summary>
		/// Data de Saída Realizada, apontado pelo Sistema ACT
		/// </summary>
		public virtual DateTime? DataSaidaRealizadaACT { get; set; }

		/// <summary>
		/// Estação de apontamento da Movimentação
		/// </summary>
		public virtual IAreaOperacional Estacao { get; set; }

		/// <summary>
		/// Trem que está sendo movimentado
		/// </summary>
		public virtual Trem Trem { get; set; }

		/// <summary>
		/// Usuário que fez o apontamento de chegada
		/// </summary>
		public virtual Usuario UsuarioChegada { get; set; }

		/// <summary>
		/// Usuário que fez o apontamento de saída
		/// </summary>
		public virtual Usuario UsuarioSaida { get; set; }

		#endregion
	}
}