namespace Translogic.Modules.Core.Domain.Model.Trem
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Veiculo.Locomotiva;
    using Via;

    /// <summary>
    /// Planejamento de quantidade de tra��o de locomotivas no trem
    /// </summary>
    public class PlanejamentoQuantidadeLocomotiva : EntidadeBase<int>
    {
        /// <summary>
        /// Ordem de servi�o <see cref="OrdemServico"/>
        /// </summary>
        public virtual OrdemServico.OrdemServico OrdemServico { get; set; }

        /// <summary>
        /// <see cref="SerieLocomotiva"/> do planejamento
        /// </summary>
        public virtual SerieLocomotiva Serie { get; set; }

        /// <summary>
        /// Quantidade de locomotivas anexadas no local
        /// </summary>
        public virtual int Quantidade { get; set; }

        /// <summary>
        /// Tipo de opera��o
        /// </summary>
        public virtual string TipoOperacao { get; set; }
    }
}