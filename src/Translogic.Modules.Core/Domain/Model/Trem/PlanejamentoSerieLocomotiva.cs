namespace Translogic.Modules.Core.Domain.Model.Trem
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Veiculo.Locomotiva;
    using Via;

    /// <summary>
    /// Planejamento de series de locomotivas no trem
    /// </summary>
    public class PlanejamentoSerieLocomotiva : EntidadeBase<int>
    {
        /// <summary>
        /// Ordem de servi�o <see cref="OrdemServico"/>
        /// </summary>
        public virtual OrdemServico.OrdemServico OrdemServico { get; set; }

        /// <summary>
        /// <see cref="EstacaoMae"/> onde ser�o anexadas ou desanexadas as locos
        /// </summary>
        public virtual EstacaoMae Local { get; set; }

        /// <summary>
        /// <see cref="SerieLocomotiva"/> do planejamento
        /// </summary>
        public virtual SerieLocomotiva Serie { get; set; }

        /// <summary>
        /// Quantidade de locomotivas anexadas no local
        /// </summary>
        public virtual int QtdeAnexadas { get; set; }

        /// <summary>
        /// Quantidade de locomotivas desanexadas no local
        /// </summary>        
        public virtual int QtdeDesanexadas { get; set; }

        /// <summary>
        /// Data de cadastro
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// <see cref="Usuario"/> que gravou o planejamento
        /// </summary>
        public virtual Usuario UsuarioCadastro { get; set; }
    }
}