﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using ALL.Core.Dominio;
	using Codificador;
	using QuadroEstados.Dimensoes;
	using Via;

	/// <summary>
	/// Representa a associação da Situação que o Trem se encontra 
	/// </summary>
	public class SituacaoTrem : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data em que o Trem saiu desta situação
		/// </summary>
		public virtual DateTime DataFinal { get; set; }

		/// <summary>
		/// Data em que o Trem entrou nesta situação
		/// </summary>
		public virtual DateTime DataInicial { get; set; }

		/// <summary>
		/// Estação em que o Evento ocorreu
		/// </summary>
		public virtual EstacaoMae Estacao { get; set; }

		/// <summary>
		/// Evento que gerou a Situação
		/// </summary>
		public virtual EventoTrem Evento { get; set; }

		/// <summary>
		/// Motivo do Evento <see cref="MotivoEvento"/>
		/// </summary>
		public virtual MotivoEvento Motivo { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public virtual string Prefixo { get; set; }

		/// <summary>
		/// Situação - <see cref="QuadroEstados.Dimensoes.Situacao"/>
		/// </summary>
		public virtual Situacao Situacao { get; set; }

		/// <summary>
		/// Trem - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Trem"/>
		/// </summary>
		public virtual Trem Trem { get; set; }

		#endregion
	}
}