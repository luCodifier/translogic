﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using System.Collections.Generic;
	using Acesso;
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;
	using Via;
	using Via.Circulacao;

	/// <summary>
	/// Representa o Trem
	/// </summary>
	public class Trem : EntidadeBase<int?>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor inicializando a lista de movimentações
		/// </summary>
		public Trem()
		{
			ListaMovimentacaoTrem = new List<MovimentacaoTrem.MovimentacaoTrem>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Classe do Trem - <see cref="ClasseTrem"/>
		/// </summary>
		public virtual ClasseTrem Classe { get; set; }

		/// <summary>
		/// Data de Cancelamento do Trem
		/// </summary>
		public virtual DateTime? DataCancelamento { get; set; }

		/// <summary>
		/// Data de Liberação do Trem
		/// </summary>
		public virtual DateTime? DataLiberacao { get; set; }

		/// <summary>
		/// Data Prevista de Chegada
		/// </summary>
		public virtual DateTime? DataPrevistaChegada { get; set; }

		/// <summary>
		/// Data Prevista de Partida
		/// </summary>
		public virtual DateTime? DataPrevistaPartida { get; set; }

		/// <summary>
		/// Data Realizada de Chegada
		/// </summary>
		public virtual DateTime? DataRealizadaChegada { get; set; }

		/// <summary>
		/// Data Realizada de Partida
		/// </summary>
		public virtual DateTime? DataRealizadaPartida { get; set; }

		/// <summary>
		/// Estação de Destino do Trem
		/// </summary>
		public virtual EstacaoMae Destino { get; set; }

		/// <summary>
		/// Empresa Operadora do Trem
		/// </summary>
		public virtual EmpresaFerrovia EmpresaOperadora { get; set; }

		/// <summary>
		/// Lista de Movimentações do Trem
		/// </summary>
		public virtual IList<MovimentacaoTrem.MovimentacaoTrem> ListaMovimentacaoTrem { get; set; }

		/// <summary>
		/// Ordem de Serviço do Trem
		/// </summary>
		public virtual OrdemServico.OrdemServico OrdemServico { get; set; }

		/// <summary>
		/// Estação de Origem do Trem
		/// </summary>
		public virtual EstacaoMae Origem { get; set; }

		/// <summary>
		/// Estação ou AreaOperacional Trecho em que um trem se encontra
		/// </summary>
		public virtual IAreaOperacional Parada { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public virtual string Prefixo { get; set; }

		/// <summary>
		/// Rota do Trem
		/// </summary>
		public virtual Rota Rota { get; set; }

		/// <summary>
		/// Situação trem - <see cref="SituacaoTremEnum"/>
		/// </summary>
		public virtual SituacaoTremEnum? Situacao { get; set; }

		/// <summary>
		/// Última movimentação do trem
		/// </summary>
		/// <remarks>Só estará preenchida caso o trem já tenha partido</remarks>
		public virtual MovimentacaoTrem.MovimentacaoTrem UltimaMovimentacao { get; set; }

		/// <summary>
		/// Usuário que Cancelou o Trem
		/// </summary>
		public virtual Usuario UsuarioCancelamento { get; set; }

		/// <summary>
		/// Usuário que encerrou o Trem
		/// </summary>
		public virtual Usuario UsuarioEncerramento { get; set; }

		/// <summary>
		/// Usuário que Liberou o Trem
		/// </summary>
		public virtual Usuario UsuarioLiberacao { get; set; }

		#endregion
	}
}