﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
	using System;
	using System.Collections.Generic;
	using Acesso;
	using ALL.Core.Dominio;
	using Estrutura;
	using Via;
	using Via.Circulacao;

	/// <summary>
	/// Representa uma Ordem de Serviço de um Pedido
	/// </summary>
	public class OrdemServico : EntidadeBase<int?>
	{
	    private IList<PlanejamentoSerieLocomotiva> _planejamentoSerieLocomotiva;
	    private IList<ParadaOrdemServicoProgramada> _paradasProgramadas;
		// private IList<PlanejamentoQuantidadeLocomotiva> _planejamentoQuantidadeLocomotiva;

	    /*fechamento*/
		/*supressao*/
		/*cadastro*/

        /// <summary>
        /// Construtor padrão
        /// </summary>
        public OrdemServico()
        {
            _planejamentoSerieLocomotiva = new List<PlanejamentoSerieLocomotiva>();
            _paradasProgramadas = new List<ParadaOrdemServicoProgramada>();
			// _planejamentoQuantidadeLocomotiva = new List<PlanejamentoQuantidadeLocomotiva>();
		}

	    #region PROPRIEDADES

		/// <summary>
		/// Data em que a OS foi cadastrada
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Data de Fechamento da OS
		/// </summary>
		public virtual DateTime? DataFechamento { get; set; }

		/// <summary>
		/// Data em que a OS foi Suprimida
		/// </summary>
		public virtual DateTime? DataSupressao { get; set; }

	    /// <summary>
	    /// Indica se a ordem de serviço deve ser cumprida pois é meta
	    /// </summary>
        public virtual bool? Garantido { get; set; }

        /// <summary>
        /// Duração da ordem de serviço
        /// </summary>
        public virtual DateTime Duracao { get; set; }

        /// <summary>
        /// Empresa responsável pelo trem
        /// </summary>
        public virtual EmpresaFerrovia Empresa { get; set; }

        #region Datas Oficiais
        /// <summary>
        /// Data oficial de confirmação da chegada prevista
        /// </summary>
        public virtual DateTime? DataChegadaPrevistaOficial { get; set; }

        /// <summary>
		/// Data oficial de confirmação da partida prevista
		/// </summary>
		public virtual DateTime? DataPartidaPrevistaOficial { get; set; }
        #endregion

        #region Datas da grade
        /// <summary>
        /// Data confirmação da chegada prevista
        /// </summary>
        public virtual DateTime? DataChegadaPrevistaGrade { get; set; }

        /// <summary>
        /// Data de confirmação da partida prevista
        /// </summary>
        public virtual DateTime? DataPartidaPrevistaGrade { get; set; }
        #endregion

        /// <summary>
		/// EstacaoMae de Destino
		/// </summary>
		public virtual EstacaoMae Destino { get; set; }

		/// <summary>
		/// Número da OS
		/// </summary>
		public virtual int? Numero { get; set; }

		/// <summary>
		/// EstacaoMae de Origem
		/// </summary>
		public virtual EstacaoMae Origem { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public virtual string Prefixo { get; set; }

		/// <summary>
		/// Quantidade de Vagões carregados
		/// </summary>
		public virtual int QtdeVagoesCarregados { get; set; }

		/// <summary>
		/// Quantidade de Vagoes vazios
		/// </summary>
		public virtual int QtdeVagoesVazios { get; set; }

		/// <summary>
		/// <see cref="Rota"/> da OS
		/// </summary>
		public virtual Rota Rota { get; set; }

		/// <summary>
		/// Situação da ordem de serviço - <see cref="SituacaoOrdemServico"/>
		/// </summary>
		public virtual SituacaoOrdemServico Situacao { get; set; }

		/// <summary>
		/// Usuário que cadastrou a OS
		/// </summary>
		public virtual Usuario UsuarioCadastro { get; set; }

		/// <summary>
		/// Usuário que fechou a OS
		/// </summary>
		public virtual Usuario UsuarioFechamento { get; set; }

		/// <summary>
		/// Usuário que suprimiu a OS
		/// </summary>
		public virtual Usuario UsuarioSupressao { get; set; }

        /// <summary>
        /// Planejamento de locomotivas
        /// </summary>
        public virtual IList<PlanejamentoSerieLocomotiva> PlanejamentoSerieLocomotivas
        {
            get
            {
                return _planejamentoSerieLocomotiva;
            }

            set
            {
                _planejamentoSerieLocomotiva = value;
            }
        }

		/// <summary>
		/// Paradas programadas para esta Ordem de Serviço
		/// </summary>
		public virtual IList<ParadaOrdemServicoProgramada> ParadasProgramadas
		{
			get
			{
				return _paradasProgramadas;
			}

			set
			{
				_paradasProgramadas = value;
			}
		}

/*
		/// <summary>
		/// Paradas programadas para esta Ordem de Serviço
		/// </summary>
		public virtual IList<PlanejamentoQuantidadeLocomotiva> PlanejamentoQuantidadeLocomotivas
		{
			get
			{
				return _planejamentoQuantidadeLocomotiva;
			}

			set
			{
				_planejamentoQuantidadeLocomotiva = value;
			}
		}
*/

		/// <summary>
	    /// Indica se a ordem de serviço foi suprimida (cancelada) ou não
	    /// </summary>
	    public virtual bool Suprimido
	    {
	        get
	        {
	            return DataSupressao.HasValue;
	        }
	    }

        /// <summary>
        /// Indica se foi fechada a programação da ordem de serviço
        /// </summary>
        public virtual bool FechadaProgramacao
        {
            get
            {
                return DataFechamento.HasValue;
            }
        }

        /// <summary>
        /// Id da Pre OS
        /// </summary>
        public virtual int? IdPreOs { get; set; }

		#endregion
	}
}