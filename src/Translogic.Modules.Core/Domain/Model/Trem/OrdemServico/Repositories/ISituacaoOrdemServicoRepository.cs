namespace Translogic.Modules.Core.Tests.Domain.Services.Trem.OrdemService
{
    using ALL.Core.AcessoDados;
    using Core.Domain.DataAccess.Repositories.Trem.OrdemServico;
    using Core.Domain.Model.Trem.OrdemServico;

    /// <summary>
    /// Interface para o repositório <see cref="SituacaoOrdemServicoRepository"/>
    /// </summary>
    public interface ISituacaoOrdemServicoRepository : IRepository<SituacaoOrdemServico, int?>
    {
    }
}