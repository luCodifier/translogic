namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using DataAccess.Adaptations;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="PlanejamentoSerieLocomotiva"/>
    /// </summary>
    public interface IPlanejamentoSerieLocomotivaRepository : IRepository<PlanejamentoSerieLocomotiva, int>
    {
        /// <summary>
        /// Obter lista de <see cref="PlanejamentoSerieLocomotiva"/> vigentes por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
        /// <returns>Lista de <see cref="PlanejamentoSerieLocomotivaVigente"/> vigentes</returns>
        IList<PlanejamentoSerieLocomotivaVigente> ObterVigentes(int idOrdemServico);

        /// <summary>
        /// Apagar todos os planejamentos por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
        void ApagarVigentes(int idOrdemServico);
    }
}