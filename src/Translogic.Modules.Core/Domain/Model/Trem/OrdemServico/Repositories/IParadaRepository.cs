using System;
using System.Collections.Generic;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Diversos;

namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
    /// <summary>
    ///     Contrato do Repositório de <see cref="PlanejamentoSerieLocomotiva" />
    /// </summary>
    public interface IParadaOrdemServicoProgramadaRepository : IRepository<ParadaOrdemServicoProgramada, int?>
    {
        IList<ParadaOrdemServicoProgramada> ObterParadas(int? os,
            string prefixo, string origem, string destino, DateTime dataInicial,
            DateTime dataFinal, CadCorredor corredor);
    }
}