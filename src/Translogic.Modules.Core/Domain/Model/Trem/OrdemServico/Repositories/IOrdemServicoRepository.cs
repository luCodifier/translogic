namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
    using Dto;
	using Interfaces.Trem.OnTime;
	using Interfaces.Trem.OrdemServico;

    /// <summary>
	/// Contrato do Reposit�rio de <see cref="OrdemServico"/>
	/// </summary>
	public interface IOrdemServicoRepository : IRepository<OrdemServico, int?>
	{
        /// <summary>
        /// Insere uma altera��o de Os no OnTime
        /// </summary>
        /// <param name="objAltOSOnTime">Altera��o de Os para fins de penaliza��o no OnTime</param>
        void InserirAlteracaoOsOnTime(AlteracaoOSOnTime objAltOSOnTime);

		/// <summary>
		/// Obtem todas as OSs suprimidas no intervalo informado
		/// </summary>
		/// <param name="dataInicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
		/// <param name="dataFinal">Data final (Data oficial de confirma��o da partida prevista)</param>
		/// <returns>Lista de <see cref="OrdemServico"/></returns>
		IList<OrdemServico> ObterOSSuprimidas(DateTime dataInicial, DateTime dataFinal);

		/// <summary>
		/// Obtem as OS por id do TipoSituacao
		/// </summary>
		/// <param name="idTipoSituacao">Id da situa��o da OS</param>
        /// <param name="prefixo">Prefixo das ordens de servico</param>
        /// <param name="origem">Origem da ordem de servi�o</param>
        /// <param name="destino">Destino da ordem de servi�o</param>
		/// <param name="inicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
		/// <param name="final">Data final (Data oficial de confirma��o da partida prevista)</param>
		/// <returns>Lista de <see cref="OrdemServico"/></returns>
		IList<OrdemServico> ObterPorTipoSituacaoEDataPartidaPrevistaOficial(int idTipoSituacao, string prefixo, string origem, string destino, DateTime inicial, DateTime final);

        /// <summary>
        /// Persiste a <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� persistida</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        new OrdemServico Inserir(OrdemServico ordemServico);

        /// <summary>
        /// Atualizar uma nova <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="atualizar">Ordem de servi�o que ser� persistida</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        new OrdemServico Atualizar(OrdemServico atualizar);

        /// <summary>
        /// Atualizar uma nova <see cref="OrdemServico"/>
        /// </summary>
        /// <param name="idOs"> Id da Ordem de servi�o que ser� persistida</param>
        /// <param name="dataPartida"> Data Partida da Ordem de servi�o que ser� persistida</param>
        /// <param name="usuario">Usuario que solicitou a altera��o.</param>
        /// <returns>A ordem de servi�o persistida com os dados atualizados</returns>
        OrdemServico AtualizarDataPartidaOficial(int idOs, DateTime dataPartida, string usuario);

        /// <summary>
        /// Retorna as OS que tiveram a data de partida alterada
        /// </summary>
        /// <param name="inicial">Data inicial da �ltima altera��o</param>
        /// <param name="final">Data final da �ltima altera��o</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        IList<LogAlteracaoOsDto> ObterAlteracoesDataPartida(DateTime inicial, DateTime? final);

        /// <summary>
        /// Fechar o planejamento de ordem servi�o da grade
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechado o planejamento</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        OrdemServico FecharPlanejamento(OrdemServico ordemServico);

        /// <summary>
        /// Fechar a programacao da ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechada a programa��o</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        OrdemServico FecharProgramacao(OrdemServico ordemServico);

        /// <summary>
        /// Suprimir (cancelar) ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� suprimida</param>
        void Suprimir(OrdemServico ordemServico);
        
        /// <summary>
        /// Obtem uma OS pelo id da pre OS
        /// </summary>
        /// <param name="idPreOs">Id da pre OS</param>
        /// <returns>A OrdemServico desta PreOS</returns>
        OrdemServico ObterPorIdPreOs(int idPreOs);

    	/// <summary> 
    	/// Obt�m a ordem de servi�o pelo n�mero
    	/// </summary>
    	/// <param name="numeroOrdemServico"> Numero da ordem servico. </param>
    	/// <returns> Objeto <see cref="OrdemServico"/> </returns>
        OrdemServico ObterPorNumero(int numeroOrdemServico);

        /// <summary> 
        /// Obt�m as locomotivas da ordem de servi�o pelo n�mero
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem servico. </param>
        /// <returns> Lista de locomotivas na OS </returns>
        IList<LocomotivaDto> ObterLocomotivasPorOS(int numeroOrdemServico);

		/// <summary>
		/// Obtem a lista de OSs para OnTime
		/// </summary>
		/// <param name="requisicao">Requisi��o de consulta</param>
		/// <returns>OSs encontradas para o intervalo</returns>
        IList<OrdemServicoOnTimeDto> ObterOrdensServicoOnTime(RequisicaoOrdemServicoOnTime requisicao);
        
        /// <summary>
        /// Obtem a lista de Partidas de OSs para OnTime
        /// </summary>
        /// <param name="codigoUpOrigem">Up de Origem</param>
        /// <param name="codigoUpDestino">Up de Destino</param>
        /// <param name="estOrigem">Esta��o de Origem</param>
        /// <param name="estDestino">Esta��o de Destino</param>
        /// <param name="prefixo">Prefixo do trem</param>
        /// <param name="numOS">N�mero da OS</param>
        /// <param name="dataCadastroInicial">Data de cadastro inicial</param>
        /// <param name="dataCadastroFinal">Data de cadastro final</param>
        /// <param name="dataEdicaoInicial">Data de edi��o inicial</param>
        /// <param name="dataEdicaoFinal">Data de edi��o final</param>
        /// <param name="dataSupressaoInicial">Data de supress�o inicial</param>
        /// <param name="dataSupressaoFinal">Data de supress�o final</param>
        /// <param name="partidaPlanejadaInicial">Partida planejada inicial</param>
        /// <param name="partidaPlanejadaFinal">Partida planejada Final</param>
        /// <param name="chegadaPlanejadaInicial">Chegada planejada inicial</param>
        /// <param name="chegadaPlanejadaFinal">Chegada planejada Final</param>
        /// <param name="prefixosIncluir">Prefixos a incluir</param>
        /// <param name="prefixosExcluir">Prefixos a excluir</param>
        /// <returns>OSs encontradas para o intervalo</returns>
        IList<PartidaTremOnTime> ObterPartidasOnTime(
            string codigoUpOrigem,
            string codigoUpDestino,
            string estOrigem,
            string estDestino,
            string prefixo,
            string numOS,
            DateTime? dataCadastroInicial,
            DateTime? dataCadastroFinal,
            DateTime? dataEdicaoInicial,
            DateTime? dataEdicaoFinal,
            DateTime? dataSupressaoInicial,
            DateTime? dataSupressaoFinal,
            DateTime? partidaPlanejadaInicial,
            DateTime? partidaPlanejadaFinal,
            DateTime? chegadaPlanejadaInicial,
            DateTime? chegadaPlanejadaFinal,
            string prefixosIncluir,
            string prefixosExcluir);

        /// <summary>
        /// Obt�m todos os IDs das OS que ainda n�o tiveram a documenta��o gerada
        /// </summary>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das OSs para gera��o de documenta��o</returns>
        IList<OsComposicaoDto> ObterOsPendenteGeracao(int terminalDestinoId);

        /// <summary>
        /// Obt�m todos os IDs das OS que tiveram a documenta��o gerada, por�m houve composi��o diferente da gerada
        /// </summary>
        /// <returns>Retorna lista de ids das OSs para atualiza��o dos documentos</returns>
        IList<OsComposicaoDto> ObterOsPendenteAtualizacao();

        /// <summary>
        /// Obt�m todos os ID dos documentos pendentes a serem enviados pelo job de envio de documenta��o de refaturamento
        /// </summary>
        /// <param name="recebedorId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids dos documentos para envio</returns>
        IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadasRefaturamento(int recebedorId);

        /// <summary>
        /// Obt�m todos os ID das composi��es e trens pendentes a serem enviados pelo job de envio de documenta��o:
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter passado para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadas(int areaOperacionalEnvioId, int terminalDestinoId);

        /// <summary>
        /// Obt�m todos os ID das composi��es e trens pendentes a serem enviados pelo job de envio de documenta��o no momento que chegou na esta��o parametrizada
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter CHEGADO para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        IList<OsComposicaoDto> ObterOsComposicoesNaoEnviadasChegada(int areaOperacionalEnvioId, int terminalDestinoId);
	}
}