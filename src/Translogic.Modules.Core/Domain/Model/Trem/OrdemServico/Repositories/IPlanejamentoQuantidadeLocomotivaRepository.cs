namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using DataAccess.Adaptations;

    /// <summary>
	/// Contrato do Reposit�rio de <see cref="PlanejamentoQuantidadeLocomotiva"/>
    /// </summary>
	public interface IPlanejamentoQuantidadeLocomotivaRepository : IRepository<PlanejamentoQuantidadeLocomotiva, int>
    {
        /// <summary>
		/// Obter lista de <see cref="PlanejamentoQuantidadeLocomotiva"/> vigentes por id da ordem de servi�o
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o</param>
        /// <returns>Lista de <see cref="PlanejamentoSerieLocomotivaVigente"/> vigentes</returns>
		IList<PlanejamentoQuantidadeLocomotiva> ObterPorOrdemServico(int idOrdemServico);
    }
}