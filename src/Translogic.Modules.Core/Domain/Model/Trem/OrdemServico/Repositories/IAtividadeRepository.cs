namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using DataAccess.Adaptations;

    /// <summary>
	/// Contrato do Repositório de <see cref="AtividadesParadaOrdemServicoProgramada"/>
    /// </summary>
	public interface IAtividadeParadaOrdemServicoProgramadaRepository : IRepository<AtividadesParadaOrdemServicoProgramada, int?>
    {
    }
}