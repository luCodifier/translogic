﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
    using System;
    using System.ComponentModel;
    using ALL.Core.Dominio;
    using Diversos;

    /// <summary>
    /// Tipo de Alteração de OnTime
    /// </summary>
    public enum TipoAlteracaoOnTime
    {
        /// <summary>
        /// Cancelamento de OS para fins de penalização
        /// </summary>
        [Description("C")]
        Cancelamento = 'C',

        /// <summary>
        /// Alteração de partida real para uso da partida do TL
        /// </summary>
        [Description("P")]
        PartidaReal = 'P'
    }

    /// <summary>
    /// Representa uma alteração na OS para efeito de indicador, não altera a OS real
    /// </summary>
    public class AlteracaoOSOnTime : EntidadeBase<int>
    {
        /// <summary>
        /// OS na qual as alterações se refletirão
        /// </summary>
        public virtual OrdemServico OrdemServico { get; set; }

        /// <summary>
        /// Tipo de alteração
        /// </summary>
        public virtual TipoAlteracaoOnTime Tipo { get; set; }

        /// <summary>
        /// Situação do Trem sobrescrita
        /// </summary>
        public virtual SituacaoTremPxEnum? SituacaoTremPx { get; set; }

        /// <summary>
        /// Situação da OS sobrescrita
        /// </summary>
        public virtual SituacaoOrdemServico SituacaoOS { get; set; }

        /// <summary>
        /// Data de partida real
        /// </summary>
        public virtual DateTime? DataPartidaReal { get; set; }

        /// <summary>
        /// Data de chegada real
        /// </summary>
        public virtual DateTime? DataChegadaReal { get; set; }

        /// <summary>
        /// Usuário que efetuou
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
        /// Data de cadastro da alteração
        /// </summary>
        public virtual DateTime Data { get; set; }
    }
}