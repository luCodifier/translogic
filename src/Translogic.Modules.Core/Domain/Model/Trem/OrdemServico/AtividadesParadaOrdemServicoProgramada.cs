﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa o apontamento do período da Parada programada
	/// </summary>
	public class AtividadesParadaOrdemServicoProgramada : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Término de execução da Atividade
		/// </summary>
		public virtual DateTime DataFim { get; set; }

		/// <summary>
		/// Data de Início de execução da Atividade
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Duração da Atividade que será executada na Estação <see cref="DuracaoAtividadeEstacao"/>
		/// </summary>
		public virtual DuracaoAtividadeEstacao DuracaoAtividadeEstacao { get; set; }

		/// <summary>
		/// Programação da Parada da Ordem de Serviço <see cref="ParadaOrdemServicoProgramada"/>
		/// </summary>
		public virtual ParadaOrdemServicoProgramada ParadaOrdemServicoProgramada { get; set; }

		#endregion
	}
}