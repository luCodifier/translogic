﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Indica a programação de quantos vagões(carregados/vazios) da Ordem de Serviço 
	/// serão anexados/desanexados 
	/// em determinada Estação
	/// </summary>
	public class ParadaOrdemServicoProgramada : EntidadeBase<int?>
	{
        private IList<AtividadesParadaOrdemServicoProgramada> _atividades;
        
        #region PROPRIEDADES

        /// <summary>
        /// Construtor padrão
        /// </summary>
	    public ParadaOrdemServicoProgramada()
	    {
	        _atividades = new List<AtividadesParadaOrdemServicoProgramada>();
	    }

	    /// <summary>
		/// Data de Chegada
		/// </summary>
		public virtual DateTime DataChegada { get; set; }

		/// <summary>
		/// Data de Partida
		/// </summary>
		public virtual DateTime DataPartida { get; set; }

		/// <summary>
		/// Duração para realizar as atividades
		/// </summary>
		public virtual DateTime Duracao { get; set; }

		/// <summary>
		/// <see cref="IAreaOperacional"/> da rota da OS
		/// </summary>
		public virtual EstacaoMae Estacao { get; set; }

		/// <summary>
		/// Ordem de Serviço
		/// </summary>
		public virtual OrdemServico OrdemServico { get; set; }

		/// <summary>
		/// Qyantidade de Vagoes anexados Carregados
		/// </summary>
		public virtual int QtdeVagoesAnexadosCarregado { get; set; }

		/// <summary>
		/// Quantidade de Vagões Anexados vazios
		/// </summary>
		public virtual int QtdeVagoesAnexadosVazio { get; set; }

		/// <summary>
		/// Quantidade de Vagoes dexanexados carregados
		/// </summary>
		public virtual int QtdeVagoesDesanexadosCarregado { get; set; }

		/// <summary>
		/// Quantidade de Vagoes desanexados vazios
		/// </summary>
		public virtual int QtdeVagoesDesanexadosVazio { get; set; }

	    /// <summary>
        /// Lista de atividades que ocorrerão na parada programada
        /// </summary>
        public virtual IList<AtividadesParadaOrdemServicoProgramada> Atividades
	    {
	        get { return _atividades; }
	        set { _atividades = value; }
	    }

	    #endregion
	}
}