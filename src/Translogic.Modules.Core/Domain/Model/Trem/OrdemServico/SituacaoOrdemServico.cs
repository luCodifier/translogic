﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a Situação em que uma OS pode estar
	/// </summary>
	public class SituacaoOrdemServico : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descrição da Situação da Ordem de Serviço
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Tipo da situação da ordem de serviço
		/// </summary>
		public virtual string Tipo { get; set; }

		#endregion
	}
}