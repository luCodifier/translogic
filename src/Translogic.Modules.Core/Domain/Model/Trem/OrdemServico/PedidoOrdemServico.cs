﻿namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico
{
	using Acesso;
	using ALL.Core.Dominio;
	using Diversos;
	using FluxosComerciais.Pedidos;
	using Via;

	/// <summary>
	/// Associa o Pedido à uma Ordem de Serviço
	/// </summary>
	public class PedidoOrdemServico : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Estação de anexação
		/// </summary>
		public virtual EstacaoMae EstacaoAnexacao { get; set; }

		/// <summary>
		/// Estação de Desanexação
		/// </summary>
		public virtual EstacaoMae EstacaoDesanexacao { get; set; }

		/// <summary>
		/// Indica a lotação - <see cref="CarregadoVazioEnum"/>
		/// </summary>
		public virtual CarregadoVazioEnum? IndCarregadoVazio { get; set; }

		/// <summary>
		/// Número do PedidoOrdemServico
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		/// Ordem de serviço do pedido - <see cref="OrdemServico"/> 
		/// </summary>
		public virtual OrdemServico OrdemServico { get; set; }

		/// <summary>
		/// Pedido - <see cref="Pedido"/>
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		/// Quantidade que foi tracionada
		/// </summary>
		public virtual int? QuantidadeTracionada { get; set; }

		/// <summary>
		/// Usuário que gravou o pedido
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}