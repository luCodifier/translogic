namespace Translogic.Modules.Core.Domain.Model.Trem.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="Trem"/>
    /// </summary>
    public interface IComposicaoMaquinistaRepository : IRepository<ComposicaoMaquinista, int?>
    {
        /// <summary>
        /// Obtem a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao da ComposicaoMaquinista</param>
        /// <returns>A ComposicaoMaquinista</returns>
        ComposicaoMaquinista ObterPorComposicao(Composicao composicao);
    }
}