namespace Translogic.Modules.Core.Domain.Model.Trem.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Model.Trem;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="Composicao"/>
    /// </summary>
    public interface IComposicaoRepository : IRepository<Composicao, int?>
    {
        /// <summary>
        /// Obtem a ultima composicao do trem
        /// </summary>
        /// <param name="idTrem">Id do Trem</param>
        /// <returns>Ultima composicao</returns>
        Composicao ObterUltimaPorIdTrem(int idTrem);

        /// <summary>
        /// Obtem os dados da composicao
        /// </summary>
        /// <param name="idComposicao">Identificador da composicao</param>
        /// <returns>Retorna os dados da composi��o</returns>
        Composicao ObterDadosComposicao(int idComposicao);

        /// <summary>
        /// Obtem composicao por trem
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="idTrem">Identificador do trem</param>
        /// <returns>Retorna os dados da composi��o</returns>
        ResultadoPaginado<Composicao> ObterComposicaoPorTrem(DetalhesPaginacao detalhesPaginacao, int idTrem);

        /// <summary>
        /// Obtem composicao por trem
        /// </summary>
        /// <param name="idComposicao">Identificador do trem</param>
        /// <returns>Retorna os dados da composi��o</returns>
        IList<VagaoComposicaoDto> ObterVagoesComposicao(int idComposicao);

        /// <summary>
        /// Obtem vag�es por filtro
        /// </summary>
        /// <param name="dataInicio">Data de In�cio do Per�odo</param>
        /// <param name="dataTermino">Data de T�rmino do Per�odo</param>
        /// <param name="vagoes">Lista de Vag�es</param>
        /// <param name="estacoes">Esta��o para filtro</param>
        /// <param name="idLinha">Id da Linha corrente dos vag�es</param>
        /// <returns>Retorna os dados da composi��o</returns>
        IList<VagaoComposicaoDto> ObterVagoesPorFiltro(DateTime dataInicio, DateTime dataTermino, string[] vagoes, string[] estacoes, int? idLinha);

        /// <summary>
        /// Obtem as notas da composicao por trem
        /// </summary>
        /// <param name="idComposicao">Identificador da composicao</param>
        /// <param name="vagoes">Identificador dos vagoes</param>
        /// <returns>Retorna as chaves das nfe</returns>
        IList<string> ObterNfeVagoesComposicao(int idComposicao, IList<int> vagoes);

        /// <summary>
        /// Obtem as notas dos vag�es em p�tios vigentes
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna as chaves das nfe</returns>
        IList<string> ObterNfeItensDespacho(IList<int> itensDespachos);


        /// <summary>
        /// Obtem as notas dos vag�es que possuem PDF anexado no processo de faturamento
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna os Ids das nfe</returns>
        IList<decimal> ObterNfePdfItensDespacho(IList<int> itensDespachos);


        /// <summary>
        /// Obtem as notas dos vag�es que possuem PDF anexado no processo de faturamento
        /// </summary>
        /// <param name="itensDespachos">Identificador dos itens de despacho</param>
        /// <returns>Retorna os Ids das nfe</returns>
        IList<ItemDespachoNfeDto> ObterNfePdfItensDespachoDto(IList<int> itensDespachos);

        /// <summary>
        /// Obtem os laudos das mercadorias dos vag�es
        /// </summary>
        /// <param name="listaCodigosVagoes">Identificador dos C�digos dos Vag�es</param>
        /// <returns>Retorna os dados dos laudos das mercadorias dos vag�es</returns>
        IList<LaudoMercadoriaDto> ObterLaudoMercadoriaPdfItensDespachoDto(IList<string> listaCodigosVagoes);

        /// <summary>
        /// Obt�m todos os ID das composi��es e trens pendentes a serem enviados pelo job de envio de documenta��o:
        /// </summary>
        /// <param name="areaOperacionalEnvioId">�rea operacional m�e onde o trem deve ter passado para enviar a documenta��o</param>
        /// <param name="terminalDestinoId">Terminal de destino que receber� o e-mail</param>
        /// <returns>Retorna lista de ids das composi��es para envio</returns>
        IList<decimal> ObterComposicoesNaoEnviadas(int areaOperacionalEnvioId, int terminalDestinoId);
    }
}