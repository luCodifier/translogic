namespace Translogic.Modules.Core.Domain.Model.Trem.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Contrato do Repositório de <see cref="TremRapido"/>
	/// </summary>
	public interface ITremRapidoRepository : IRepository<TremRapido, int?>
	{
		/// <summary>
		/// Obtem trens rápidos vigentes
		/// </summary>
		/// <returns>Trens Rapidos Vigentes</returns>
		IList<TremRapido> ObterTrensRapidosVigentes();
	}
}