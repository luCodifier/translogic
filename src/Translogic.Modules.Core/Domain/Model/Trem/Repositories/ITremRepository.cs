namespace Translogic.Modules.Core.Domain.Model.Trem.Repositories
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="Trem"/>
    /// </summary>
    public interface ITremRepository : IRepository<Trem, int?>
    {
        /// <summary>
        /// Obter um trem por id da ordem de servico <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/>
        /// </summary>
        /// <param name="idOrdemServico">Id da ordem de servi�o <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        Trem ObterPorIdOrdemServico(int idOrdemServico);

        /// <summary>
        /// Obt�m um trem dado o n�mero da Ordem de servi�o
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem de servico. </param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        Trem ObterPorNumeroOrdemServico(int numeroOrdemServico);

        /// <summary>
        /// Obt�m um trem dado o id
        /// </summary>
        /// <param name="idTrem"> Id do trem. </param>
        /// <returns>Um trem e suas dependencias para o servico do EDI</returns>
        Trem ObterPorIdComFetch(int idTrem);

        /// <summary>
        /// Obt�m um trem dado o n�mero da Ordem de servi�o
        /// </summary>
        /// <param name="numeroOrdemServico"> Numero da ordem de servico. </param>
        /// <returns>Um trem que foi criado baseado na <see cref="Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.OrdemServico"/></returns>
        Trem ObterPorNumeroOrdemServicoSemFetch(int numeroOrdemServico);

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        ResultadoPaginado<TremDto> Listar(DetalhesPaginacao detalhesPaginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, int? idTrem);

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="chaveMdfe">Chave Mdfe</param>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        ResultadoPaginado<TremDto> Listar(DetalhesPaginacao detalhesPaginacao, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string chaveMdfe, int? idTrem);

        /// <summary>
        /// Listar os trens de acordo com o despacho informado
        /// </summary>
        /// <param name="detalhesPaginacao">Detalhes de paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de trens de acordo com o despacho</returns>
        ResultadoPaginado<TremDto> ListarPorDespacho(DetalhesPaginacao detalhesPaginacao, int idDespacho);
        TremDto ObterTremComposicao(int idTrem, int idComposicao);
        void UpdateTremIdPVT(int idTrem);
    }
}