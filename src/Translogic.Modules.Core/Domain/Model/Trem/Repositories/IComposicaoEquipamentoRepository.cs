namespace Translogic.Modules.Core.Domain.Model.Trem.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="Trem"/>
    /// </summary>
    public interface IComposicaoEquipamentoRepository : IRepository<ComposicaoEquipamento, int?>
    {
        /// <summary>
        /// Obtem a partir da composicao
        /// </summary>
        /// <param name="composicao">A Composicao do Equipamento</param>
        /// <returns>A ComposicaoEquipamento</returns>
        IList<ComposicaoEquipamento> ObterPorComposicao(Composicao composicao);
    }
}