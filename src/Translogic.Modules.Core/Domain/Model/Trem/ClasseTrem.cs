﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa uma Classe de Trem
	/// </summary>
	public class ClasseTrem : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descrição da Classe do trem
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Codigo da Classe do trem
		/// </summary>
		public virtual string Codigo { get; set; }

		#endregion
	}
}