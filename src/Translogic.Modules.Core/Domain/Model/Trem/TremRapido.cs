namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o Trem Rapido
	/// </summary>
	public class TremRapido : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Trechos (Origem - Destino)
		/// </summary>
		public virtual string Trechos { get; set; }

		/// <summary>
		/// Prefixos dos trens
		/// </summary>
		public virtual string Prefixos { get; set; }

		/// <summary>
		/// Data Inicial
		/// </summary>
		public virtual DateTime DataInicial { get; set; }

		/// <summary>
		/// Data Final
		/// </summary>
		public virtual DateTime? DataFinal { get; set; }

		#endregion
	}
}