﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;

	/// <summary>
	/// Representa a associação de um Evento ao Trem
	/// </summary>
	public class EventoTrem : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data em que ocorreu o Evento no Trem
		/// </summary>
		public virtual DateTime DataOcorrencia { get; set; }

		/// <summary>
		/// Evento do Trem - <see cref="Translogic.Modules.Core.Domain.Model.Codificador.Evento"/>
		/// </summary>
		public virtual Evento Evento { get; set; }

		/// <summary>
		/// Trem a ser logado o evento - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Trem"/>
		/// </summary>
		public virtual Trem Trem { get; set; }

		/// <summary>
		/// Usuário que registrou a ocorrencia do Evento no Trem
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}