﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Via;

    /// <summary>
    /// Representa um vagão do trem
    /// </summary>
    /// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
    public class Conteiner : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /*/// <summary>
        /// Vagão do conteiner
        /// </summary>
        /// <remarks>
        /// Este campo não é mais utilizado pois para identificar em qual
        /// vagão o conteiner está é possível achar pelo VagaoConteinerVigente</remarks>
        // public virtual Vagao.Vagao Vagao { get; set; }*/

        /// <summary>
        /// Indicativo de carga
        /// </summary>
        public virtual int Carga { get; set; }

        /// <summary>
        /// Id do vagão
        /// </summary>
        public virtual int? Vagao { get; set; }

        /// <summary>
        /// Número do Conteiner
        /// </summary>
        public virtual string Numero { get; set; }
        
        /// <summary>
        /// Digito verificador
        /// </summary>
        public virtual int DigitoVerificador { get; set; }
        
        /// <summary>
        /// Tipo do conteiner
        /// </summary>
        public virtual string TipoConteiner { get; set; }

        /// <summary>
        /// Time Stamp Conteiner
        /// </summary>
        public virtual DateTime TimeStamp { get; set; }
        
        /// <summary>
        /// Ind PRP Conteiner
        /// </summary>
        public virtual string IndPrp { get; set; }

         /// <summary>
        /// Estação de origem
        /// </summary>
        public virtual IAreaOperacional Estacao { get; set; }

        /// <summary>
        /// Indicador CR Id Conteiner
        /// </summary>
        public virtual int IdCr { get; set; }

        /// <summary>
        /// Indicador Tc Id
        /// </summary>
        public virtual int TcId { get; set; }

        /// <summary>
        /// Leasing do Conteiner
        /// </summary>
        public virtual string Leasing { get; set; }
        
        /// <summary>
        /// Indicador de ativo Conteiner
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Indicador de recomendação
        /// </summary>
        public virtual string Recomendado { get; set; }

        /// <summary>
        /// Motivo de recomendação Conteiner
        /// </summary>
        public virtual string Motivo { get; set; }

        /// <summary>
        /// Indicativo de carga
        /// </summary>
        public virtual string IndicativoCarga { get; set; }

        /// <summary>
        /// Taxa de carga Conteiner
        /// </summary>
        public virtual double TxLsg { get; set; }

        /// <summary>
        /// Data inicio leasing Conteiner
        /// </summary>
        public virtual DateTime InicioLsg { get; set; }

        /// <summary>
        /// Duração do leasing
        /// </summary>
        public virtual int DurLsg { get; set; }

        /// <summary>
        /// Vencimento do leasing Conteiner
        /// </summary>
        public virtual DateTime VencimentoLsg { get; set; }

        /// <summary>
        /// Usuário do conteiner
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Leasing do conteiner
        /// </summary>
        public virtual int LeasingLsg { get; set; }

        #endregion
    }
}