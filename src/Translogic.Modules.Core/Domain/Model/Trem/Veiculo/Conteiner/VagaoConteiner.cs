﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using FluxosComerciais;
    using Via;

    /// <summary>
    /// Representa um vagão do trem
    /// </summary>
    /// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
    public class VagaoConteiner : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Vagão do conteiner
        /// </summary>
        public virtual Vagao.Vagao Vagao { get; set; }

        /// <summary>
        /// Conteiner do vagão
        /// </summary>
        public virtual Conteiner Conteiner { get; set; }

        /// <summary>
        /// Fluxo comercial do Conteiner
        /// </summary>
        public virtual FluxoComercial FluxoComercial { get; set; }

        /// <summary>
        /// Data inicio conteiner
        /// </summary>
        public virtual DateTime DataInicio { get; set; }
        
        /// <summary>
        /// Data fim conteiner
        /// </summary>
        public virtual DateTime DataFim { get; set; }

        /// <summary>
        /// Area operacional início
        /// </summary>
        public virtual IAreaOperacional EstacaoInicio { get; set; }

        /// <summary>
        /// Area operacional fim
        /// </summary>
        public virtual IAreaOperacional EstacaoFim { get; set; }

        /// <summary>
        /// Indicativo de carga
        /// </summary>
        public virtual string IndicativoCarga { get; set; }

        /// <summary>
        /// Usuário cadastro vagao conteiner
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Time Stamp vagão conteiner
        /// </summary>
        public virtual DateTime TimeStamp { get; set; }

        /// <summary>
        /// Codigo argentina
        /// </summary>
        public virtual string CodigoArgentina { get; set; }

        /// <summary>
        /// Trem argentina
        /// </summary>
        public virtual string TremArgentina { get; set; }

        /// <summary>
        /// Indicador de IdFcr
        /// </summary>
        public virtual int IdFcr { get; set; }

        /// <summary>
        /// Indicador de recomendação
        /// </summary>
        public virtual string VagaoFic { get; set; }
        #endregion
    }
}