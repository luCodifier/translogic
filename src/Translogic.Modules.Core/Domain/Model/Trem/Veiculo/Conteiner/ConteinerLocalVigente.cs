﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Via;

    /// <summary>
    /// Representa um vagão do trem
    /// </summary>
    /// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
    public class ConteinerLocalVigente : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Código do Conteiner
        /// </summary>
		public virtual int CodConteiner { get; set; }

		/// <summary>
		/// Area operacional início
		/// </summary>
		public virtual IAreaOperacional EstacaoOrigem { get; set; }
        
        #endregion
    }
}