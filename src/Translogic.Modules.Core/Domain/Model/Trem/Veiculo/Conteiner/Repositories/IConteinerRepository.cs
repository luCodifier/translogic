﻿
namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;


	/// <summary>
	/// Contrato do Repositório de <see cref="IConteinerRepository"/>
	/// </summary>
	public interface IConteinerRepository : IRepository<Conteiner, int?>
	{
		/// <summary>
		/// Retorna o Conteiner a partir do código
		/// </summary>
		/// <param name="codigo">Código do conteiner</param>
		/// <returns>Objeto Conteiner</returns>
		Conteiner ObterPorCodigo(string codigo);

	    /// <summary>
	    /// Retorna o Conteiner a partir do código sem estado
	    /// </summary>
	    /// <param name="codigo">Código do conteiner</param>
	    /// <returns>Objeto Conteiner</returns>
	    Conteiner ObterPorCodigoSemEstado(string codigo);

		/// <summary>
		/// Voa o vagão para a estação
		/// </summary>
		/// <param name="idConteiner">Número do conteiner</param>
		/// <param name="idAoOri"> Id da área operacional mãe do destino do contêiner</param>
		/// <param name="usuario">Usuário logado no sistema</param>
		void VoarConteiner(string idConteiner, int idAoOri, string usuario);

        /// <summary>
        ///    Retorna a lista de Conteiners do vagao vigente a partir da chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave o cte</param>
        /// <returns>Contagem do total de conteiners</returns>
	    int ObterTotalConteinersVagaoVigente(string chaveCte);

	    /// <summary>
	    ///    Retorna a lista de Conteiners do vagao vigente e fluxo a partir da chave do cte
	    /// </summary>
	    /// <param name="chaveCte">Chave o cte</param>
	    /// <returns>Contagem do total de conteiners</returns>
	    int ObterTotalConteinersVagaoVigenteFluxo(string chaveCte);

		/// <summary>
		/// Retorna a lista de Conteiners a partir dos códigos
		/// </summary>
		/// <param name="codigos">Código do conteiner</param>
		/// <returns>Objeto de lista de Conteiner</returns>
		IList<Conteiner> ObterPorCodigo(string[] codigos);

        /// <summary>
        /// Retorna a lista de Conteiners a partir do id vagao
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        /// <returns>Objeto de lista de Conteiner</returns>
	    IList<Conteiner> ObterPorVagaoVigente(int idVagao);

	    /// <summary>
	    /// Retorna a lista de Conteiners a partir do id vagao , fluxo e CTE
	    /// </summary>
	    /// <param name="idVagao">Id do vagão</param>
	    /// <param name="chaveCte">Cte</param>
	    /// <returns>Objeto de lista de Conteiner</returns>
	    IList<ConteinerDto> ObterPorVagaoVigenteFluxoCte(string chaveCte);
 
	    /// <summary>
	    /// Retorna a lista de Conteiners a partir do id vagao e CTE
	    /// </summary>
	    /// <param name="idVagao">Id do vagão</param>
        /// <param name="chaveCte">Cte</param>
	    /// <returns>Objeto de lista de Conteiner</returns>
	    IList<ConteinerDto> ObterPorVagaoVigenteCte(string chaveCte);
	}
}
