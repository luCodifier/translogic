namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Contrato do Reposit�rio de <see cref="IVagaoConteinerVigenteRepository"/>
	/// </summary>
	public interface IVagaoConteinerVigenteRepository : IRepository<VagaoConteinerVigente, int?>
	{
		/// <summary>
		/// Obt�m o vag�o conteiner por vag�o
		/// </summary>
		/// <param name="vagao">Vag�o que tem o conteiner</param>
		/// <returns>Retorna o objeto vag�o conteiner</returns>
		VagaoConteinerVigente ObterPorVagao(Vagao.Vagao vagao);

		/// <summary>
		/// Obt�m o vag�o conteiner por c�digo do vag�o
		/// </summary>
		/// <param name="codigoVagao">C�digo do vag�o</param>
		/// <returns>Retorna o objeto vag�o conteiner</returns>
		VagaoConteinerVigente ObterPorCodigoVagao(string codigoVagao);

        /// <summary>
        /// Obt�m o vag�o conteiner por conteiner
        /// </summary>
        /// <param name="conteiner">Objeto conteiner</param>
        /// <returns>Retorna o objeto vag�o conteiner</returns>
	    VagaoConteinerVigente ObterPorConteiner(Conteiner conteiner);
	}
}