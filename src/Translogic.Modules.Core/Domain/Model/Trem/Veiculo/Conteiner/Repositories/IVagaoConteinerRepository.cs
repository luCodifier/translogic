﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    
    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoConteinerRepository"/>
    /// </summary>
    public interface IVagaoConteinerRepository : IRepository<VagaoConteiner, int?>
    {
        /// <summary>
        /// Retorna o Vagao a partir do código
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>O Vagao objeto</returns>
        VagaoConteiner ObterPorCodigo(string codigo);

        /// <summary>
        /// Retorna o conteiners do vagão
        /// </summary> 
        /// <param name="vagao">Código do vagão</param>
        /// <returns>O VagaoConteiner objeto</returns>
        IList<VagaoConteiner> ObterPorVagao(int vagao);
    }
}
