﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner.Repositories
{
    using ALL.Core.AcessoDados;
    
    /// <summary>
	/// Contrato do Repositório de <see cref="IConteinerLocalVigenteRepository"/>
    /// </summary>
	public interface IConteinerLocalVigenteRepository : IRepository<ConteinerLocalVigente, int?>
    {
		/// <summary>
		/// Obtem um ConteinerLocalVigente através do código do conteiner
		/// </summary>
		/// <param name="codigo">Código do conteiner</param>
		/// <returns>Retorna um ConteinerLocalVigente</returns>
		ConteinerLocalVigente ObterPorCodConteiner(int? codigo);
    }
}
