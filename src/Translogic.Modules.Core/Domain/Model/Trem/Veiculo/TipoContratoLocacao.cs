namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa um tipo de contrato de loca��o de ve�culos
	/// </summary>
	public class TipoContratoLocacao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo do TipoContratoLocacao
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Data de In�cio do TipoContratoLocacao
		/// </summary>
		public virtual DateTime DataInicioContrato { get; set; }

		/// <summary>
		/// Data de T�rmino do TipoContratoLocacao
		/// </summary>
		public virtual DateTime DataTerminoContrato { get; set; }

		/// <summary>
		/// Descri��o do TipoContratoLocacao
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Usu�rio que gravou o TipoContratoLocacao
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}