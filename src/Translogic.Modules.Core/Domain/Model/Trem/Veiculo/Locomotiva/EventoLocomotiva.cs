﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;

	/// <summary>
	/// Representa o Evento de uma Locomotiva
	/// </summary>
	public class EventoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Ocorrencia do Evento da Locomotiva
		/// </summary>
		public virtual DateTime DataOcorrencia { get; set; }

		/// <summary>
		/// Evento da locomotiva - <see cref="Codificador.Evento"/>
		/// </summary>
		public virtual Evento Evento { get; set; }

		/// <summary>
		/// Locomotiva a ser logado o Evento - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Locomotiva"/>
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		/// <summary>
		/// Usuário que gerou o evento na locomotiva
		/// </summary>
		public virtual Usuario UsuarioSolicitante { get; set; }

		#endregion
	}
}