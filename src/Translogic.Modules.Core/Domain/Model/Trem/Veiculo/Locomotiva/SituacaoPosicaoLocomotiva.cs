﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;
	using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Representa a Situação da posição de uma Locomotiva
	/// </summary>
	public class SituacaoPosicaoLocomotiva : EntidadeBase<int?>
	{
		private TipoSituacaoPosicaoLocomotivaEnum? _tipoSituacaoPosicaoLocomotiva;

		#region PROPRIEDADES

		/// <summary>
		/// Data de Inicio da Situacao
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de Termino da Situacao
		/// </summary>
		public virtual DateTime? DataFim { get; set; }

		/// <summary>
		/// Composicao da locomotiva - <see cref="ComposicaoLocomotiva"/>
		/// </summary>
		public virtual ComposicaoLocomotiva ComposicaoLocomotiva { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		/// <summary>
		/// Tipo da situação da posição da Locomotiva - <see cref="TipoSituacaoPosicaoLocomotivaEnum"/>
		/// </summary>
		public virtual TipoSituacaoPosicaoLocomotivaEnum? TipoSituacaoComandoLocomotiva
		{
			get { return !this._tipoSituacaoPosicaoLocomotiva.HasValue ? TipoSituacaoPosicaoLocomotivaEnum.Frente : this._tipoSituacaoPosicaoLocomotiva; }
			set { this._tipoSituacaoPosicaoLocomotiva = value; }
		}

		#endregion
	}
}