﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Representa a Situação de uma Locomotiva
	/// </summary>
	public class SituacaoComandoLocomotiva : EntidadeBase<int?>
	{
		private TipoSituacaoComandoLocomotivaEnum? _tipoSituacaoLocomotiva;

		#region PROPRIEDADES

		/// <summary>
		/// Data de Inicio da Situacao
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de Termino da Situacao
		/// </summary>
		public virtual DateTime? DataFim { get; set; }

		/// <summary>
		/// Composicao da locomotiva - <see cref="ComposicaoLocomotiva"/>
		/// </summary>
		public virtual ComposicaoLocomotiva ComposicaoLocomotiva { get; set; }
		
		/// <summary>
		/// Tipo da Locomotiva - <see cref="TipoSituacaoComandoLocomotivaEnum"/>
		/// </summary>
		public virtual TipoSituacaoComandoLocomotivaEnum? TipoSituacaoComandoLocomotiva
		{
			get { return !_tipoSituacaoLocomotiva.HasValue ? TipoSituacaoComandoLocomotivaEnum.Comandada : _tipoSituacaoLocomotiva; }
			set { _tipoSituacaoLocomotiva = value; }
		}

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		#endregion
	}
}