﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Diversos;
    using Estrutura;

    /// <summary>
    /// Representa uma Locomotiva
    /// </summary>
    public class Locomotiva : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Bitola da locomotiva - <see cref="Diversos.Bitola"/>
        /// </summary>
        public virtual Bitola Bitola { get; set; }

        /// <summary>
        /// Capacidade de óleo no carter
        /// </summary>
        public virtual double? CapacidadeOleoCarter { get; set; }

        /// <summary>
        /// Capacidade do tanque d´agua da locomotiva
        /// </summary>
        public virtual double? CapacidadeTanqueAgua { get; set; }

        /// <summary>
        /// Capacidade do tanque de combustível
        /// </summary>
        public virtual double? CapacidadeTanqueCombustivel { get; set; }

        /// <summary>
        /// Código da Locomotiva
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Código do usuário no SAP
        /// </summary>
        public virtual string CodigoSAP { get; set; }

        /// <summary>
        /// Data de Fabricação da Locomotiva
        /// </summary>
        public virtual DateTime? DataFabricacao { get; set; }

        /// <summary>
        /// Data de Início de operação da Locomotiva
        /// </summary>
        public virtual DateTime? DataInicioOperacao { get; set; }

        /// <summary>
        /// Empresa Operadora da locomotiva - <see cref="EmpresaFerrovia"/>
        /// </summary>
        public virtual EmpresaFerrovia EmpresaOperadora { get; set; }

        /// <summary>
        /// Empresa proprietária da locomotiva - <see cref="IEmpresa"/>
        /// </summary>
        public virtual IEmpresa EmpresaProprietaria { get; set; }

        /// <summary>
        /// Estado em que a locomotiva se encontra - <see cref="EstadoLocomotivaEnum"/>
        /// </summary>
        public virtual EstadoLocomotivaEnum? Estado { get; set; }

        /// <summary>
        /// Folha de especificação - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.FolhaEspecificacaoLocomotiva"/>
        /// </summary>
        public virtual FolhaEspecificacaoLocomotiva FolhaEspecificacao { get; set; }

        /// <summary>
        /// Indica se a Locomotiva está ativa
        /// </summary>
        public virtual bool? IndAtivo { get; set; }

        /// <summary>
        /// Indica se a Locomotiva é locada
        /// </summary>
        /// <remarks>Sim - locada , Não - próprio da ALL</remarks>
        public virtual bool? IndLocado { get; set; }

        /// <summary>
        /// Quantidade Remanescente 
        /// </summary>
        public virtual double? QuantidadeRemanescente { get; set; }

        /// <summary>
        /// Série da locomotiva - <see cref="SerieLocomotiva"/>
        /// </summary>
        public virtual SerieLocomotiva Serie { get; set; }

        /// <summary>
        /// Tipo de contrato de locação - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.TipoContratoLocacao"/>
        /// </summary>
        public virtual TipoContratoLocacao TipoContratoLocacao { get; set; }

        /// <summary>
        ///  Usuário que cadastrou a locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Acesso.Usuario"/>
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Código do usuário no SAP
        /// </summary>
        public virtual string PropriedadeANTT { get; set; }

        /// <summary>
        /// Data da ultima alteração
        /// </summary>
        public virtual DateTime? DataUltimaAlteracao { get; set; }

        #endregion
    }
}