﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Representa as todas as <see cref="Composicao">Composições</see> da <see cref="Locomotiva"/>
	/// </summary>
	public class ComposicaoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// <see cref="Composicao"/> a ser associada
		/// </summary>
		public virtual Composicao Composicao { get; set; }

		/// <summary>
		/// Linha em que o <see cref="Trem"/> foi formado
		/// </summary>
		public virtual ElementoVia ElementoVia { get; set; }

		/// <summary>
		/// <see cref="Locomotiva"/> a ser associada
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		/// <summary>
		/// Sequencia da Locomotiva na composição do Trem
		/// </summary>
		public virtual int? Sequencia { get; set; }

		#endregion
	}
}