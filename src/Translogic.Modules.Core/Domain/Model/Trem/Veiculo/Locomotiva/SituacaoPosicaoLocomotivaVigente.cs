﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;
	using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Representa a Situação vigente da posição de uma Locomotiva
	/// </summary>
	public class SituacaoPosicaoLocomotivaVigente : SituacaoPosicaoLocomotiva
	{
	}
}