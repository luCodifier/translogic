﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
    using System;
    using ALL.Core.Dominio;
    using Via;

    /// <summary>
    /// Localização da locomotiva no pátio
    /// </summary>
    public class LocomotivaPatio : EntidadeBase<int>
    {
        /// <summary>
        /// Locomotiva do registro
        /// </summary>
        public Locomotiva Locomotiva { get; set; }

        /// <summary>
        /// Trem corrente
        /// </summary>
        public Trem Trem { get; set; }

        /// <summary>
        /// Pátio atual
        /// </summary>
        public IAreaOperacional AreaOperacional { get; set; }

        /// <summary>
        /// Linha da locomotiva
        /// </summary>
        public ElementoVia ElementoVia { get; set; }

        /// <summary>
        /// Data de cadastro
        /// </summary>
        public DateTime DataCadastro { get; set; }

        /// <summary>
        /// Data de saída do Pátio
        /// </summary>
        public DateTime? DataSaidaPatio { get; set; }

        /// <summary>
        /// Data Chegada no Patio
        /// </summary>
        public DateTime? DataChegadaPatio { get; set; }

        /// <summary>
        /// Código da Locomotiva
        /// </summary>
        public string CodigoLocomotiva { get; set; }

        /// <summary>
        /// Número de sequência no pátio
        /// </summary>
        public int NumeroSequenciaPatio { get; set; }
    }
}