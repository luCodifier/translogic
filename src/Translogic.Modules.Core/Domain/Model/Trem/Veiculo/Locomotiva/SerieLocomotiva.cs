namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using ALL.Core.Dominio;
	using Estrutura;

	/// <summary>
	/// Representa a S�rie da Locomotiva
	/// </summary>
	public class SerieLocomotiva : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo da Serie
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descri��o da Serie
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Empresa proprietaria
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		/// N�mero de Eixos
		/// </summary>
		public virtual int? NumeroEixos { get; set; }

		/// <summary>
		/// Valor do KGF
		/// </summary>
		public virtual double? ValorKGF { get; set; }

		#endregion
	}
}