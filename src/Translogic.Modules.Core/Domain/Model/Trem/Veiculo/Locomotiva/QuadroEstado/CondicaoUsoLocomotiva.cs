﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associacao da condiçao de uso com a locomotiva
	/// </summary>
	public class CondicaoUsoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Condição de uso -  <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.CondicaoUso"/>
		/// </summary>
		public virtual CondicaoUso CondicaoUso { get; set; }

		/// <summary>
		/// Data de Inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Termino
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento da locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.EventoLocomotiva"/>
		/// </summary>
		public virtual EventoLocomotiva EventoLocomotiva { get; set; }

		/// <summary>
		/// Locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Locomotiva"/>
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		#endregion
	}
}