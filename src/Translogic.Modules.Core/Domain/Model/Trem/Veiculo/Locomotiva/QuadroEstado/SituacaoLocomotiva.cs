﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using Codificador;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associação de uma situação com uma locomotiva dado um evento
	/// </summary>
	public class SituacaoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de início
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de Locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.EventoLocomotiva"/>
		/// </summary>
		public virtual EventoLocomotiva EventoLocomotiva { get; set; }

		/// <summary>
		/// Locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Locomotiva"/>
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		/// <summary>
		/// Motivo do evento - <see cref="Translogic.Modules.Core.Domain.Model.Codificador.MotivoEvento"/>
		/// </summary>
		public virtual MotivoEvento MotivoEvento { get; set; }

		/// <summary>
		/// Situação - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Situacao"/>
		/// </summary>
		public virtual Situacao Situacao { get; set; }

		#endregion
	}
}