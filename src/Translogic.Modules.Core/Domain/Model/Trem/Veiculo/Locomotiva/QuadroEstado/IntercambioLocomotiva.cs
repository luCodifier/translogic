﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associacao do intercambio com a locomotiva dado um evento
	/// </summary>
	public class IntercambioLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de termino
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Intercambio <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Intercambio"/>
		/// </summary>
		public virtual Intercambio Intercambio { get; set; }

		/// <summary>
		/// Locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Locomotiva"/>
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		#endregion
	}
}