﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associação da localizacao de uma locomotiva
	/// </summary>
	public class LocalizacaoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.EventoLocomotiva"/>
		/// </summary>
		public virtual EventoLocomotiva EventoLocomotiva { get; set; }

		/// <summary>
		/// Localização - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Localizacao"/>
		/// </summary>
		public virtual Localizacao Localizacao { get; set; }

		/// <summary>
		/// Locomotiva - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Locomotiva"/>
		/// </summary>
		public virtual Locomotiva Locomotiva { get; set; }

		#endregion
	}
}