namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface para <see cref="ISituacaoTracaoLocomotivaRepository"/>
    /// </summary>
    public interface ISituacaoTracaoLocomotivaRepository : IRepository<SituacaoTracaoLocomotiva, int?>
    {
		/// <summary>
		/// Obt�m a situa��o de tra��o da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <param name="dataReferencia">Data de Referencia</param>
		/// <returns> Objeto da situa��o de tra��o </returns>
		SituacaoTracaoLocomotiva ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva, DateTime dataReferencia);
    }
}