namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface para <see cref="ISituacaoPosicaoLocomotivaRepository"/>
    /// </summary>
	public interface ISituacaoPosicaoLocomotivaRepository : IRepository<SituacaoPosicaoLocomotiva, int?>
    {
		/// <summary>
		/// Obt�m a situa��o de posicao da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <returns> Objeto da situa��o de posicao </returns>
		SituacaoPosicaoLocomotiva ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva, DateTime dataReferencia);
    }
}