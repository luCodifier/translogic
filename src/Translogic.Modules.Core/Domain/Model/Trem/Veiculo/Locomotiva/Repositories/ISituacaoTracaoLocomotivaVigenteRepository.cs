namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;

	/// <summary>
	/// Interface para <see cref="SituacaoTracaoLocomotivaVigenteRepository"/>
    /// </summary>
    public interface ISituacaoTracaoLocomotivaVigenteRepository : IRepository<SituacaoTracaoLocomotivaVigente, int?>
    {
		/// <summary>
		/// Obt�m a situa��o de tra��o da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <returns> Objeto da situa��o de tra��o </returns>
		SituacaoTracaoLocomotivaVigente ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva);
    }
}