namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface para <see cref="ISituacaoPosicaoLocomotivaVigenteRepository "/>
    /// </summary>
	public interface ISituacaoPosicaoLocomotivaVigenteRepository : IRepository<SituacaoPosicaoLocomotivaVigente, int?>
    {
		/// <summary>
		/// Obt�m a situa��o de posicao da composicao da locomotiva
		/// </summary>
		/// <param name="composicaoLocomotiva"> Composicao da Locomotiva a ser buscada a situa��o. </param>
		/// <returns> Objeto da situa��o de posicao </returns>
		SituacaoPosicaoLocomotivaVigente ObterPorComposicaoLocomotiva(ComposicaoLocomotiva composicaoLocomotiva);
    }
}