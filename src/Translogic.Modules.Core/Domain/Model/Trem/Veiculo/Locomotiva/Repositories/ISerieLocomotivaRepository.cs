namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
    using ALL.Core.AcessoDados;
    using DataAccess.Repositories.Trem.Veiculo.Locomotiva;

    /// <summary>
    /// Interface para <see cref="SerieLocomotivaRepository"/>
    /// </summary>
    public interface ISerieLocomotivaRepository : IRepository<SerieLocomotiva, int?>
    {
    }
}