namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using DataAccess.Repositories.Trem.Veiculo.Locomotiva;

    /// <summary>
    /// Interface para <see cref="LocomotivaRepository"/>
    /// </summary>
    public interface IComposicaoLocomotivaRepository : IRepository<ComposicaoLocomotiva, int>
    {
        /// <summary>
        /// Retorna as ComposicaoLocomotiva a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao de referencia</param>
        /// <returns>Lista de ComposicaoLocomotiva</returns>
        IList<ComposicaoLocomotiva> ObterPorComposicao(Composicao composicao);
    }
}