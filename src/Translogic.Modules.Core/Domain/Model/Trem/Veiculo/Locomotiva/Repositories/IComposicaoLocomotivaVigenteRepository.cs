namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;

	/// <summary>
    /// Interface para <see cref="ComposicaoLocomotivaVigenteRepository"/>
    /// </summary>
    public interface IComposicaoLocomotivaVigenteRepository : IRepository<ComposicaoLocomotivaVigente, int>
    {
        /// <summary>
		/// Retorna as ComposicaoLocomotivaVigente a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao de referencia</param>
		/// <returns>Lista de ComposicaoLocomotivaVigente</returns>
        IList<ComposicaoLocomotivaVigente> ObterPorComposicao(Composicao composicao);
    }
}