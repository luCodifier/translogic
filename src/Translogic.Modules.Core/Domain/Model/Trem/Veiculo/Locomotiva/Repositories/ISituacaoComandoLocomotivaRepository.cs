namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface para <see cref="ISituacaoComandoLocomotivaRepository"/>
    /// </summary>
    public interface ISituacaoComandoLocomotivaRepository : IRepository<SituacaoComandoLocomotiva, int?>
    {
		/// <summary>
		/// Obt�m as situa��es de comando dada uma loco e data
		/// </summary>
		/// <param name="locomotiva"> Locomotiva a ser buscada a situa��o de comando. </param>
		/// <param name="data"> Data que a locomotiva estava no comando </param>
		/// <returns> Lista de Situa��es </returns>
		IList<SituacaoComandoLocomotiva> ObterComandantePorLocomotivaData(Locomotiva locomotiva, DateTime data);
    }
}