namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;

	/// <summary>
    /// Interface para <see cref="LocomotivaRepository"/>
    /// </summary>
    public interface ILocomotivaRepository : IRepository<Locomotiva, int>
    {
		/// <summary>
		/// Obt�m as locomotivas dad um c�digo
		/// </summary>
		/// <param name="codigo"> C�digo da locomotiva. </param>
		/// <returns> Lista de Locomotivas </returns>
		IList<Locomotiva> ObterPorCodigo(string codigo);

		/// <summary>
		/// Obt�m as locomotivas
		/// </summary>
		/// <returns> Lista de Locomotivas </returns>
		IList<Locomotiva> ObterLocomotivas();
    }
}