namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using DataAccess.Repositories.Trem.Veiculo.Locomotiva;

    /// <summary>
    /// Interface para <see cref="EventoLocomotivaRepository"/>
    /// </summary>
    public interface IEventoLocomotivaRepository : IRepository<EventoLocomotiva, int>
    {
        /// <summary>
        /// Obtem todos os eventos que ocorreram com essas locomotivas no intervalo
        /// </summary>
        /// <param name="ini">data de inicio</param>
        /// <param name="fim">data de fim</param>
        /// <param name="locos">locomotivas de interesse</param>
        /// <returns>Lista de Eventos</returns>
        IList<EventoLocomotiva> ObterPorDataELocomotivas(DateTime ini, DateTime fim, IList<Locomotiva> locos);
    }
}