﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;
	using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Representa a Situação de tração de uma Locomotiva
	/// </summary>
	public class SituacaoTracaoLocomotiva : EntidadeBase<int?>
	{
		private TipoSituacaoTracaoLocomotivaEnum? _tipoSituacaoTracaoLocomotiva;

		#region PROPRIEDADES

		/// <summary>
		/// Data de Inicio da Situacao
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de Termino da Situacao
		/// </summary>
		public virtual DateTime? DataFim { get; set; }

		/// <summary>
		/// Composicao da locomotiva - <see cref="ComposicaoLocomotiva"/>
		/// </summary>
		public virtual ComposicaoLocomotiva ComposicaoLocomotiva { get; set; }

		/// <summary>
		/// Data de cadastro manual
		/// </summary>
		/// <remarks>Esta data é informada pelo operador do sistema.</remarks>
		public virtual DateTime? DataCadastroManual { get; set; }
		
		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		/// <summary>
		/// Tipo da Locomotiva - <see cref="TipoSituacaoComandoLocomotivaEnum"/>
		/// </summary>
		public virtual TipoSituacaoTracaoLocomotivaEnum? TipoSituacaoComandoLocomotiva
		{
			get { return !_tipoSituacaoTracaoLocomotiva.HasValue ? TipoSituacaoTracaoLocomotivaEnum.Tracionando : _tipoSituacaoTracaoLocomotiva; }
			set { _tipoSituacaoTracaoLocomotiva = value; }
		}

		#endregion
	}
}