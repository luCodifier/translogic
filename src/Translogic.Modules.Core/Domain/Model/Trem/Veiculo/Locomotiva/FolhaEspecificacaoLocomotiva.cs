namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a Folha de especifica��o da Locomotiva
	/// </summary>
	public class FolhaEspecificacaoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Altura total da locomotiva
		/// </summary>
		public virtual double? AlturaTotal { get; set; }

		/// <summary>
		/// Arranjo da Locomotiva
		/// </summary>
		public virtual string Arranjo { get; set; }

		/// <summary>
		/// Capacidade do tanque de combust�vel
		/// </summary>
		public virtual double? CapacidadeCombustivel { get; set; }

		/// <summary>
		/// Capacidade de �leo no Carter
		/// </summary>
		public virtual double? CapacidadeOleoCarter { get; set; }

		/// <summary>
		/// Capacidade de passageiros
		/// </summary>
		public virtual int? CapacidadePassageiros { get; set; }

		/// <summary>
		/// Capacidade do Tanque de �gua
		/// </summary>
		public virtual double? CapacidadeTanqueAgua { get; set; }

		/// <summary>
		/// Comprimento das esteiras
		/// </summary>
		public virtual double? ComprimentoEsteiras { get; set; }

		/// <summary>
		/// Comprimento do Pe�o
		/// </summary>
		public virtual double? ComprimentoPeao { get; set; }

		/// <summary>
		/// Comprimento total da locomotiva
		/// </summary>
		public virtual double? ComprimentoTotal { get; set; }

		/// <summary>
		/// Diagrama ALL
		/// </summary>
		public virtual int? DiagramaALL { get; set; }

		/// <summary>
		/// Diagrama RFFSA
		/// </summary>
		public virtual int? DiagramaRFFSA { get; set; }

		/// <summary>
		/// Esfor�o cont�nuo da locomotiva
		/// </summary>
		public virtual double? EsforcoContinuo { get; set; }

		/// <summary>
		/// Esfor�o m�ximo da locomotiva
		/// </summary>
		public virtual double? EsforcoMaximo { get; set; }

		/// <summary>
		/// Id da caracter�stica
		/// </summary>
		/// <remarks>
		/// Cadastrado manualmente n�o possui relacionamento com outra entidade
		/// </remarks>
		public virtual string IdCaracteristica { get; set; }

		/// <summary>
		/// Largura total da locomotiva
		/// </summary>
		public virtual double? LarguraTotal { get; set; }

		/// <summary>
		/// Modelo da Locomotiva
		/// </summary>
		public virtual string Modelo { get; set; }

		/// <summary>
		/// Motor da locomotiva
		/// </summary>
		public virtual string Motor { get; set; }

		/// <summary>
		/// Peso aderente
		/// </summary>
		public virtual double? PesoAderente { get; set; }

		/// <summary>
		/// Peso do eixo da locomotiva
		/// </summary>
		public virtual double? PesoEixo { get; set; }

		/// <summary>
		/// Peso total da locomotiva
		/// </summary>
		public virtual double? PesoTotal { get; set; }

		/// <summary>
		/// Pot�ncia L�quida da Locomotiva
		/// </summary>
		public virtual double? PotenciaLiquida { get; set; }

		/// <summary>
		/// Pot�ncia Nominal da Locomotiva
		/// </summary>
		public virtual double? PotenciaNominal { get; set; }

		/// <summary>
		/// Raio m�nimo
		/// </summary>
		public virtual double? RaioMinimo { get; set; }

		/// <summary>
		/// S�rie da locomotiva - <see cref="SerieLocomotiva"/>
		/// </summary>
		public virtual SerieLocomotiva Serie { get; set; }

		/// <summary>
		/// Tipo de Tra��o da Locomotiva
		/// </summary>
		public virtual string TipoTracao { get; set; }

		/// <summary>
		/// Velocidade cont�nua que a locomotiva pode trafegar
		/// </summary>
		public virtual double? VelocidadeContinua { get; set; }

		/// <summary>
		/// Velocidade m�xima da locomotiva
		/// </summary>
		public virtual double? VelocidadeMaxima { get; set; }

		#endregion
	}
}