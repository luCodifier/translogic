﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;
    using Model.Acesso;

    /// <summary>
    /// Classe de Limite de peso por frota
    /// </summary>
    public class LimiteExcecaoFrota : EntidadeBase<int>
    {
        /// <summary>
        /// propriedade Data Inicial
        /// </summary>
        public virtual DateTime DataInicial { get; set; }

        /// <summary>
        /// propriedade Data Final
        /// </summary>
        public virtual DateTime? DataFinal { get; set; }

        /// <summary>
        /// Usuário que cadastrou o limite
        /// </summary>
        public virtual Usuario UsuarioCadastro { get; set; }

        /// <summary>
        /// Limite máximo permitido
        /// </summary>
        public virtual decimal PesoMaximo { get; set; }

        /// <summary>
        /// Frota vagão
        /// </summary>
        public virtual FrotaVagao Frota { get; set; }
    }
}