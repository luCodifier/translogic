﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa uma Série de Vagão
	/// </summary>
	public class SerieVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Série do Vagão
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Série do Vagão
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Tipo do vagão - <see cref="TipoVagao"/>
		/// </summary>
		public virtual TipoVagao Tipo { get; set; }

		#endregion
	}
}