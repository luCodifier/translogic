﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa a Folha de Especificação de um vagão
	/// TODO: VERIFICAR O CAMPO FC_IDT_CRT
	/// </summary>
	public class FolhaEspecificacaoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Altura do Vagão
		/// </summary>
		public virtual double Altura { get; set; }

		/// <summary>
		/// Altura do Fueiro
		/// </summary>
		public virtual double AlturaFueiro { get; set; }

		/// <summary>
		/// Altura da Porta
		/// </summary>
		public virtual double AlturaPorta { get; set; }

		/// <summary>
		/// Altura útil do vagão
		/// </summary>
		public virtual double AlturaUtil { get; set; }

		/// <summary>
		/// Bitola do Vagão
		/// </summary>
		public virtual Bitola Bitola { get; set; }

		/// <summary>
		/// Capacidade Nominal
		/// </summary>
		public virtual double CapacidadeNominal { get; set; }

		/// <summary>
		/// Código da Folha de especificação
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Comprimento do Vagão
		/// </summary>
		public virtual double Comprimento { get; set; }

		/// <summary>
		/// Comprimento da Borda
		/// </summary>
		public virtual double ComprimentoBorda { get; set; }

		/// <summary>
		/// Comprimento das Esteiras
		/// </summary>
		public virtual double ComprimentoEsteiras { get; set; }

		/// <summary>
		/// Comprimento do Peão
		/// </summary>
		public virtual double ComprimentoPeao { get; set; }

		/// <summary>
		/// Comprimento útil do vagão
		/// </summary>
		public virtual double ComprimentoUtil { get; set; }

		/// <summary>
		/// Possui sistema de descarga
		/// </summary>
		public virtual bool? IndPossuiSistemaDescarga { get; set; }

		/// <summary>
		/// Largura do Vagão
		/// </summary>
		public virtual double Largura { get; set; }

		/// <summary>
		/// Largura da porta
		/// </summary>
		public virtual double LarguraPorta { get; set; }

		/// <summary>
		/// Largura útil do vagão
		/// </summary>
		public virtual double LarguraUtil { get; set; }

		/// <summary>
		/// Número do documento do Diagrama da ALL
		/// </summary>
		public virtual int NumDocDiagramaALL { get; set; }

		/// <summary>
		/// Número do documento do Diagrama da RFFSA
		/// </summary>
		public virtual int NumDocDiagramaRFFSA { get; set; }

		/// <summary>
		/// Valor do peso da tara
		/// </summary>
		public virtual double PesoTara { get; set; }

		/// <summary>
		/// Quantidade de Bocas
		/// </summary>
		public virtual int QuantidadeBocas { get; set; }

		/// <summary>
		/// Série do vagão - <see cref="SerieVagao"/>
		/// </summary>
		public virtual SerieVagao Serie { get; set; }

		/// <summary>
		/// Tipo do desenho
		/// </summary>
		public virtual string TipoDesenho { get; set; }

		/// <summary>
		/// Tipo do Fueiro
		/// </summary>
		public virtual string TipoFueiro { get; set; }

		/// <summary>
		/// Volume Médio
		/// </summary>
		public virtual double VolumeMedio { get; set; }

		#endregion
	}
}