﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Associa o <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/> a <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.FrotaVagao"/>
	/// </summary>
	public class VagaoFrota : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Frota do vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.FrotaVagao"/>
		/// </summary>
		public virtual FrotaVagao FrotaVagao { get; set; }

		/// <summary>
		/// Vagão a ser inserido na frota - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}