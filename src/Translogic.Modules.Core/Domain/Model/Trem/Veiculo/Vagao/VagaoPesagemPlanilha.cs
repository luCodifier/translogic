﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe VagaoMedianaTemp
	/// </summary>
    public class VagaoPesagemPlanilha : EntidadeBase<decimal?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Codigo do Vagao
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        /// Tara do vagão
        /// </summary>
        public virtual decimal Tara { get; set; }

        /// <summary>
        /// Data do Registro
        /// </summary>
        public virtual DateTime DataRegistro { get; set; }

        /// <summary>
        /// Codigo do Usuario
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
        /// Processado
        /// </summary>
        public virtual bool IndProcessado { get; set; }
        
        /// <summary>
        /// Série do vagão - <see cref="SerieVagao"/>
        /// </summary>
        public virtual string Serie { get; set; }

        /// <summary>
        /// Data da Pesagem
        /// </summary>
        public virtual DateTime DataPesagem { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        public virtual string Cliente { get; set; }

        /// <summary>
        /// Código Termninal
        /// </summary>
        public virtual string Terminal { get; set; }

        /// <summary>
        /// Peso Bruto do vagão
        /// </summary>
        public virtual decimal PesoBruto { get; set; }

        /// <summary>
        /// Processado com erro
        /// </summary>
        public virtual bool IndProcessadoComErro { get; set; }

        /// <summary>
        /// Timestamp do registro
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        ///     propriedade DataRegistro
        /// </summary>
        public virtual string DataRegistroStr
        {
            get { return DataRegistro != null ? DataRegistro.ToString() : ""; }
        }

        /// <summary>
        /// Indica como marcado para a temp mediana
        /// </summary>
        private bool MarcadoTempMediana = true;

        /// <summary>
        /// Indica se deve considerar para a temp mediana
        /// </summary>
        public virtual bool TrazerMarcadoPraTempMediana
        {
            get { return MarcadoTempMediana; }
            set { MarcadoTempMediana = value; }
        }

        #endregion
    }
}