﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;

    using ALL.Core.Dominio;

    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
    ///     Representa um vagão do trem
    /// </summary>
    /// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
    public class VagaoLimitePesoBase : EntidadeBase<int?>
    {
        /// <summary>
        ///     propriedade DataFinal
        /// </summary>
        public virtual DateTime? DataFinal { get; set; }

        /// <summary>
        ///     propriedade DataInicial
        /// </summary>
        public virtual DateTime? DataInicial { get; set; }

        /// <summary>
        ///     propriedade DataLiberacao
        /// </summary>
        public virtual DateTime? DataLiberacao { get; set; }

        /// <summary>
        ///     propriedade FluxoComercial
        /// </summary>
        public virtual FluxoComercial FluxoComercial { get; set; }

        /// <summary>
        ///     propriedade LimitadoPor
        /// </summary>
        public virtual LimitadoPorEnum LimitadoPor { get; set; }

        /// <summary>
        ///     propriedade Limite
        /// </summary>
        public virtual double? Limite { get; set; }

        /// <summary>
        ///     propriedade PesoInformado
        /// </summary>
        public virtual double? PesoInformado { get; set; }

        /// <summary>
        ///     Usuário que cadastrou o vagão
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        ///     propriedade Vagao
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        ///     Método ObterTravadoPor
        /// </summary>
        public virtual string ObterTravadoPor()
        {
            switch (this.LimitadoPor)
            {
                case LimitadoPorEnum.Manga:
                    return "Manga";
                case LimitadoPorEnum.Via:
                    return "Via";
                case LimitadoPorEnum.Minimo:
                    return "Peso Mínimo";
                case LimitadoPorEnum.PesoMinVagao:
                    return "Peso Mín (Vagão)";
                case LimitadoPorEnum.PesoMaxFrota:
                    return "Peso Max (Frota)";
                default:
                    return string.Empty;
            }
        }
    }

    /// <summary>
    ///     Representa um vagão do trem
    /// </summary>
    /// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
    public class VagaoLimitePeso : VagaoLimitePesoBase
    {
    }
}