﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
    /// Classe de Base de Limite mínimo
    /// </summary>
    public class LimiteMinimoBase : EntidadeBase<int>
    {
        /// <summary>
        /// propriedade Menor Valor da Rota
        /// </summary>
        public virtual decimal MenorValorRota { get; set; }
        
        /// <summary>
        /// Mercadoria do fluxo
        /// </summary>
        public virtual Mercadoria Mercadoria { get; set; }

        /// <summary>
        /// Código da Mercadoria
        /// </summary>
        public virtual string CodigoMercadoria { get; set; }

        /// <summary>
        /// Serie do vagão
        /// </summary>
        public virtual SerieVagao SerieVagao { get; set; }
        
        /// <summary>
        /// Código da Serie do Vagao
        /// </summary>
        public virtual string CodigoSerieVagao { get; set; }

        /// <summary>
        /// Três primeiros dígitos do vagão.
        /// </summary>
        public virtual string InicialVagao { get; set; }
        
        /// <summary>
        /// TU minima permitida
        /// </summary>
        public virtual decimal TuMedia { get; set; }
    }
}