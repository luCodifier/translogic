﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using QuadroEstado;
    using QuadroEstados.Dimensoes;

    /// <summary>
    /// Estado atual do vagão
    /// </summary>
    public class EstadoVagao
    {
        /// <summary>
        /// Vagão de referência
        /// </summary>
        public Vagao Vagao { get; set; }

        /// <summary>
        /// Código do Vagão
        /// </summary>
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Id do Quadro/Situação
        /// </summary>
        public int? IdQuadroSituacao { get; set; }

        /// <summary>
        /// Localização atual
        /// </summary>
        public Localizacao Localizacao { get; set; }

        /// <summary>
        /// Localização do Vagão
        /// </summary>
        public LocalizacaoVagao LocalizacaoVagao { get; set; }

        /// <summary>
        /// Data de registro de localização do vagão
        /// </summary>
        public DateTime? DataLocalizacaoVagao { get; set; }

        /// <summary>
        /// Registro do Responsável
        /// </summary>
        public Responsavel Responsavel { get; set; }

        /// <summary>
        /// Responsável Vagão
        /// </summary>
        public ResponsavelVagao ResponsavelVagao { get; set; }

        /// <summary>
        /// Data de registro do resposável pelo vagão.
        /// </summary>
        public DateTime? DataResponsavelVagao { get; set; }

        /// <summary>
        /// Registro de Situação do Vagão
        /// </summary>
        public Situacao Situacao { get; set; }

        /// <summary>
        /// Situação do Vagão
        /// </summary>
        public SituacaoVagao SituacaoVagao { get; set; }

        /// <summary>
        /// Data da Situação do Vagão
        /// </summary>
        public DateTime? DataSituacaoVagao { get; set; }

        /// <summary>
        /// Lotação do Vagão
        /// </summary>
        public Lotacao Lotacao { get; set; }

        /// <summary>
        /// Registro de Lotação/Vagão
        /// </summary>
        public LotacaoVagao LotacaoVagao { get; set; }

        /// <summary>
        /// Data do registro Locação/Vagão
        /// </summary>
        public DateTime? DataLotacaoVagao { get; set; }

        /// <summary>
        /// Intercâmbio do Vagão
        /// </summary>
        public Intercambio Intercambio { get; set; }

        /// <summary>
        /// Registro de Intercâmbio/Vagão
        /// </summary>
        public IntercambioVagao IntercambioVagao { get; set; }

        /// <summary>
        /// Data do registro de intercâmbio/vagão
        /// </summary>
        public DateTime? DataIntercambioVagao { get; set; }

        /// <summary>
        /// Condição de uso
        /// </summary>
        public CondicaoUso CondicaoUso { get; set; }

        /// <summary>
        /// Registro de condição uso/vagão
        /// </summary>
        public CondicaoUsoVagao CondicaoUsoVagao { get; set; }

        /// <summary>
        /// Data do registro da consição de uso/vagão
        /// </summary>
        public DateTime? DataCondicaoUsoVagao { get; set; }

        /*
        VAGAO*              - VG_ID_VG	INTEGER	N			
                              VG_COD_VAG	VARCHAR2(7)	Y	
        QUADRO_SITUACAO*****- QS_IDT_QDS	INTEGER	Y			
        LOCALIZACAO*        - LO_IDT_LCL	INTEGER	Y			
        LOCAL_VAGAO*        - LV_IDT_LCV	INTEGER	Y			
                              LV_TIMESTAMP	DATE	Y			
        RESPONSAVEL*        - RP_IDT_RSP	INTEGER	Y			
        RESP_VAGAO*         - RV_IDT_RPV	INTEGER	Y			
                              RV_TIMESTAMP	DATE	Y			
        SITUACAO*           - SI_IDT_STC	INTEGER	Y			
        SITUACAO_VAGAO*     - SC_IDT_STV	INTEGER	Y			
                              SC_TIMESTAMP	DATE	Y			
        CARGA*              - SH_IDT_STC	INTEGER	Y			
        LOTACAO*            - SG_IDT_STC	INTEGER	Y			
                              SG_TIMESTAMP	DATE	Y			
        INTERCAMBIO*        - TB_IDT_INT	INTEGER	Y			
        INTERC_VAGAO*       - SD_IDT_STI	INTEGER	Y			
                              SD_TIMESTAMP	DATE	Y			
        CONDICAO_USO        - CD_IDT_CUS	INTEGER	Y			
        CONDUSO_VAGAO       - CI_IDT_CDV	INTEGER	Y			
                              CI_TIMESTAMP	DATE	Y			
         */
    }
}