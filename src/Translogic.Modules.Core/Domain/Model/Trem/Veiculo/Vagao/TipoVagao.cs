﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa um Tipo de Vagão
	/// </summary>
	public class TipoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Classe do vagão - <see cref="ClasseVagao"/>
		/// </summary>
		public virtual ClasseVagao Classe { get; set; }

		/// <summary>
		/// Código do Tipo do Vagão
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do Tipo do Vagão
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}