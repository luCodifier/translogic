﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;

	/// <summary>
	/// Representa a ocorrencia de um evento de um vagao
	/// </summary>
	public class EventoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de ocorrencia do evento
		/// </summary>
		public virtual DateTime? DataOcorrencia { get; set; }

		/// <summary>
		/// Evento ocorrido no vagao - <see cref="Evento"/>
		/// </summary>
		public virtual Evento Evento { get; set; }

		/// <summary>
		/// <see cref="EventoVagaoVoador"/> - caso exista
		/// </summary>
		public virtual EventoVagaoVoador EventoVagaoVoador { get; set; }

		/// <summary>
		/// Usuário que solicitou
		/// </summary>
		public virtual Usuario UsuarioSolicitante { get; set; }

		/// <summary>
		/// Vagão do evento - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}