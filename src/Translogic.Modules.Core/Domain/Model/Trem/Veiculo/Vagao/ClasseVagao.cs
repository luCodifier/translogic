﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a Classe de um Vagão
	/// </summary>
	public class ClasseVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Codigo da Classe do Vagao
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Classe do Vagao
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}