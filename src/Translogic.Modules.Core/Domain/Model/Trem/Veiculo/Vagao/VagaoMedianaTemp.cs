﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe VagaoMedianaTemp
	/// </summary>
    public class VagaoMedianaTemp : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Codigo do Vagao
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        /// Registro da Pesagem
        /// </summary>
        public virtual decimal RegistroPesagem { get; set; }

        /// <summary>
        /// Data do Registro
        /// </summary>
        public virtual DateTime DataRegistro { get; set; }

        #endregion
    }
}