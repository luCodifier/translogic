namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
	using Translogic.Modules.Core.Domain.Model.Via;
	
	/// <summary>
	/// Classe base de composicao vagao
	/// </summary>
	public abstract class BaseComposicaoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Composi��o do Trem - <see cref="Composicao"/>
		/// </summary>
		public virtual Composicao Composicao { get; set; }

		/// <summary>
		///  Linha em que o <see cref="Trem"/> foi formado
		/// </summary>
		public virtual ElementoVia ElementoVia { get; set; }

		/// <summary>
		///  Linha em que o <see cref="Vagao"/> foi desanexado
		/// </summary>
		public virtual ElementoVia ElementoViaDesanexacao { get; set; }

		/// <summary>
		///  Esta��o em que o <see cref="Vagao"/> foi desanexado
		/// </summary>
		public virtual EstacaoMae EstacaoMaeDesanexacao { get; set; }

		/// <summary>
		/// Indica se o Vag�o est� carregado
		/// </summary>
		public virtual bool? IndCarregado { get; set; }

		/// <summary>
		/// Pedido da composi��o - <see cref="Pedido"/>
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		/// Sequ�ncia do Vagao na Composi��o do Trem
		/// </summary>
		public virtual int? Sequencia { get; set; }

		/// <summary>
		/// Vag�o - <see cref="Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}