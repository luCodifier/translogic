﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;
	using FluxosComerciais.Pedidos;
	using Via;

	/// <summary>
	/// Representa a associação do vagão em um Pátio(<see cref="PatioManobra"/> / <see cref="PatioCirculacao"/>)
	/// </summary>
	/// <remarks>
	/// Campos nao mapeados, por não haver relevancia dos dados:
	/// PO_IDT_PRM, VP_COD_VAG, VP_IND_SIST, VP_POS_VP, VP_TIP_POS
	/// </remarks>
	public class VagaoPatio : EntidadeBase<int?>
	{
        #region PROPRIEDADES

        /// <summary>
        /// Data da ultima alteração
        /// </summary>
        public virtual DateTime? DataUltimaAlteracao { get; set; }

		/// <summary>
		/// Data de chegada do vagao na Estação Mãe
		/// </summary>
		public virtual DateTime? DataChegadaEstacaoMae { get; set; }

		/// <summary>
		/// Data de chegada do vagao no patio
		/// </summary>
		public virtual DateTime? DataChegadaPatio { get; set; }

		/// <summary>
		/// Data de saída do vagao na estação Mãe
		/// </summary>
		public virtual DateTime? DataSaidaEstacaoMae { get; set; }

		/// <summary>
		/// Data de Saída do vagao do pátio
		/// </summary>
		public virtual DateTime? DataSaidaPatio { get; set; }

		/// <summary>
		/// Linha em que o vagao se encontra no patio
		/// </summary>
		public virtual ElementoVia ElementoVia { get; set; }

		/// <summary>
		/// Indica se o vagao está carregado ou vazio
		/// </summary>
		public virtual bool? IndVagaoCarregado { get; set; }

		/// <summary>
		/// Número de sequencia do do vagao no patio
		/// </summary>
		public virtual int? NumeroSequenciaPatio { get; set; }

		/// <summary>
		/// <see cref="PatioCirculacao"/> ou <see cref="PatioManobra"/>
		/// </summary>
		public virtual IAreaOperacional Patio { get; set; }

		/// <summary>
		/// <see cref="FluxosComerciais.Pedidos.Pedido"/> para ver se o vagao tem carregamento
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		/// <see cref="Translogic.Modules.Core.Domain.Model.Trem.Trem"/> em que o Vagão saiu do patio
		/// </summary>
		public virtual Trem Trem { get; set; }

		/// <summary>
		/// Vagão - <see cref="Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}