﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Representa uma frota de vagao
	/// </summary>
	public class FrotaVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Frota do Vagão
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição detalhada da Frota do Vagão
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição resumida da Frota do Vagão
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// Empresa da Frota
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		/// Indica se a Frota é Cativa
		/// </summary>
		public virtual bool? IndFrotaCativa { get; set; }

		/// <summary>
		/// Indica se a Frota é Restrita
		/// </summary>
		public virtual bool? IndFrotaRestrita { get; set; }

		/// <summary>
		/// Indica se a Frota é de Unidade de Negócio
		/// </summary>
		/// <remarks>
		/// Quando o valor for 'Sim' a propriedade UnidadeNegocio deve ser preenchida
		/// </remarks>
		public virtual bool? IndUnidadeNegocio { get; set; }

		/// <summary>
		/// Unidade de Negócio da Frota
		/// </summary>
		public virtual UnidadeNegocio UnidadeNegocio { get; set; }

		#endregion
	}
}