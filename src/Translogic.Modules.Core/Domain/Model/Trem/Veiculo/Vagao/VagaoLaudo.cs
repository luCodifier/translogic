﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Objeto de Laudos dos vagoes
    /// </summary>
    public class VagaoLaudo : EntidadeBase<int?>
    {
        /// <summary>
        /// Codigo do vagao
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Numero identificador do laudo
        /// </summary>
        public virtual string NumeroLaudo { get; set; }

        /// <summary>
        /// Texto de Justificativa para o Laudo
        /// </summary>
        public virtual string Justificativa { get; set; }

        /// <summary>
        /// Data de Registro
        /// </summary>
        public virtual DateTime DataRegistro { get; set; }
    }
}