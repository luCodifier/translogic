﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using System;
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Representa a ocorrencia de um evento de vôo de um vagao
	/// </summary>
	public class EventoVagaoVoador : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Ocorrência do evento
		/// </summary>
		public virtual DateTime? DataOcorrencia { get; set; }

		/// <summary>
		/// Estação Mãe ou Patio de Manobra de destino
		/// </summary>
		public virtual IAreaOperacional Destino { get; set; }

		/// <summary>
		/// Motivo de vôo do vagão
		/// </summary>
		public virtual string Motivo { get; set; }

		/// <summary>
		/// Nome do solicitante
		/// </summary>
		public virtual string NomeSolicitante { get; set; }

		/// <summary>
		/// Estação Mãe ou Patio de Manobra de origem
		/// </summary>
		public virtual IAreaOperacional Origem { get; set; }

		/// <summary>
		/// Quantidade de vagões que serão "voados"
		/// </summary>
		public virtual int? QuantidadeVagoes { get; set; }

		/// <summary>
		/// Estação Mãe ou Patio de Manobra de solicitante
		/// </summary>
		public virtual IAreaOperacional Solicitante { get; set; }

		#endregion
	}
}