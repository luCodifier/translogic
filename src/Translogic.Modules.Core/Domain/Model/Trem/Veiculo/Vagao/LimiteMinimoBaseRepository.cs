﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using NHibernate.Transform;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories;

    /// <summary>
    /// Repositorio para <see cref="LimiteMinimoBase"/>
    /// </summary>
    public class LimiteMinimoBaseRepository : NHRepository<LimiteMinimoBase, int>, ILimiteMinimoBaseRepository
    {
        /// <summary>
        /// Obter o limite minimo base de acordo com o limite, mercadoria e vagão informados
        /// </summary>
        /// <param name="limite">Valor do limite informado</param>
        /// <param name="mercadoria">Mercadoria informada para se obter o limite minimo base</param>
        /// <param name="vagao">Vagão informado</param>
        /// <returns>Limite minimo base</returns>
        public LimiteMinimoBase ObterPorLimiteMercadoriaVagao(decimal limite, string mercadoria, Vagao vagao)
        {
            LimiteMinimoBase result = null;

            var inicialVagao = vagao.Codigo.Trim().Substring(0, 3);

            var parameters = new List<Action<IQuery>>();

            var hqlQuery =
                @"SELECT L 
                    FROM LimiteMinimoBase L 
                   WHERE L.MenorValorRota = :limite 
                     AND L.CodigoMercadoria = :mercadoria 
                     AND L.InicialVagao = :inicialVagao
                     AND L.SerieVagao.Id = :idSerieVagao";

            parameters.Add(q => q.SetParameter("limite", limite));
            parameters.Add(q => q.SetParameter("mercadoria", mercadoria));
            parameters.Add(q => q.SetParameter("inicialVagao", inicialVagao));
            parameters.Add(q => q.SetParameter("idSerieVagao", vagao.Serie.Id));

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                result = query.UniqueResult<LimiteMinimoBase>();
            }

            return result;
        }

        /// <summary>
        /// Obter o limite mínimo base de acordo com a TuMedia, mercadoria e vagão informados
        /// </summary>
        /// <param name="valorTuMedia">Valor da TuMedia</param>
        /// <param name="mercadoriaCod">Mercadoria informada para se obter o limite minimo base</param>
        /// <param name="vagao">Vagão informado</param>
        /// <returns>Limite minimo base</returns>
        public LimiteMinimoBase ObterPorTuMercadoriaVagao(decimal valorTuMedia, string mercadoriaCod, Vagao vagao)
        {
            LimiteMinimoBase result = null;

            var inicialVagao = vagao.Codigo.Trim().Substring(0, 3);

            var parameters = new List<Action<IQuery>>();

            var hqlQuery =
                @"SELECT L 
                    FROM LimiteMinimoBase L 
                   WHERE L.TuMedia = :tuMedia 
                     AND L.CodigoMercadoria = :mercadoria 
                     AND L.InicialVagao = :inicialVagao
                     AND L.SerieVagao.Id = :idSerieVagao";

            parameters.Add(q => q.SetParameter("tuMedia", valorTuMedia));
            parameters.Add(q => q.SetParameter("mercadoria", mercadoriaCod));
            parameters.Add(q => q.SetParameter("inicialVagao", inicialVagao));
            parameters.Add(q => q.SetParameter("idSerieVagao", vagao.Serie.Id));

            using (var session = OpenSession())
            {
                var query = session.CreateQuery(hqlQuery);

                foreach (var p in parameters)
                {
                    p.Invoke(query);
                }

                query.SetResultTransformer(Transformers.DistinctRootEntity);

                result = query.UniqueResult<LimiteMinimoBase>();
            }

            return result;
        }
    }
}