﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
	using System;
    using System.Collections.Generic;
	using Acesso;
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Representa um vagão do trem
	/// </summary>
	/// <remarks>Todos os vagões possuem uma série, porém na folha de especificação a série pode ser diferente</remarks>
	public class Vagao : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Bitola em que o vagão trafega
		/// </summary>
		public virtual Bitola Bitola { get; set; }

		/// <summary>
		/// Código do Vagão
		/// </summary>
		public virtual string Codigo { get; set; }
        
		/// <summary>
		/// Data da ultima alteração
		/// </summary>
        public virtual DateTime? DataUltimaAlteracao { get; set; }
        
		/// <summary>
		/// Data de Fabricação
		/// </summary>
		public virtual DateTime? DataFabricacao { get; set; }

		/// <summary>
		/// Data de fim de operação do Vagão
		/// </summary>
		public virtual DateTime? DataFimOperacao { get; set; }

		/// <summary>
		/// Data de Início da Locação
		/// </summary>
		public virtual DateTime? DataInicioLocacao { get; set; }

		/// <summary>
		/// Data de Início de Operação do Vagão
		/// </summary>
		public virtual DateTime? DataInicioOperacao { get; set; }

		/// <summary>
		/// Tempo de locação
		/// </summary>
		/// <remarks>A medida é de acordo com o atributo <see cref="TipoDuracaoLocacao"/></remarks>
		public virtual int? DuracaoLocacao { get; set; }

		/// <summary>
		/// <see cref="IEmpresa"/> - Empresa que fabricou o vagão
		/// </summary>
		public virtual IEmpresa EmpresaFabricante { get; set; }

		/// <summary>
		/// <see cref="EmpresaFerrovia"/> - Empresa Operadora do Vagão
		/// </summary>
		public virtual EmpresaFerrovia EmpresaOperadora { get; set; }

		/// <summary>
		/// <see cref="IEmpresa"/> - Empresa Proprietária do Vagão
		/// </summary>
		public virtual IEmpresa EmpresaProprietaria { get; set; }

		/// <summary>
		/// Indica o estado do Freio
		/// </summary>
		public virtual EstadoFreioEnum? EstadoFreio { get; set; }

		/// <summary>
		/// Folha de especificação do vagão - <see cref="FolhaEspecificacaoVagao"/>
		/// </summary>
		public virtual FolhaEspecificacaoVagao FolhaEspecificacao { get; set; }

		/// <summary>
		/// Indica se o vagão está Ativo
		/// </summary>
		public virtual bool IndAtivo { get; set; }

		/// <summary>
		/// Indica se o vagão está carregado no momento
		/// </summary>
		public virtual bool? IndCarregado { get; set; }

		/// <summary>
		/// Indica se o Vagao é locada
		/// </summary>
		/// <remarks>Sim - locada , Não - próprio da ALL</remarks>
		public virtual bool? IndLocado { get; set; }

		/// <summary>
		/// Indica se o vagão é madrinha
		/// </summary>
		public virtual bool? IndMadrinha { get; set; }

		/// <summary>
		/// Indica se o Vagão possui freio
		/// </summary>
		public virtual bool? IndTemFreio { get; set; }

		/// <summary>
		/// Número de Tremonhas do Vagão
		/// </summary>
		public virtual int? NumeroTremonhas { get; set; }

		/// <summary>
		/// Peso da Tara do Vagão
		/// </summary>
		public virtual double? PesoTara { get; set; }

		/// <summary>
		/// Peso total do vagão no momento
		/// </summary>
		public virtual double? PesoTotal { get; set; }

		/// <summary>
		/// Série do vagão - <see cref="SerieVagao"/>
		/// </summary>
		public virtual SerieVagao Serie { get; set; }

		/// <summary>
		/// Tipo de contrato de locação - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.TipoContratoLocacao"/>
		/// </summary>
		public virtual TipoContratoLocacao TipoContratoLocacao { get; set; }

		/// <summary>
		/// Medida de tempo para a locação
		/// </summary>
		public virtual MedidaTempoEnum? TipoDuracaoLocacao { get; set; }

		/// <summary>
		/// Usuário que cadastrou o vagão
		/// </summary>
		public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Lista de VagaoPatioVigente
        /// </summary>
        public virtual IList<VagaoPatioVigente> ListaVagaoPatioVigente { get; set; }
		

        /// <summary>
        /// Obtem série do vagão
        /// </summary>
        /// <returns></returns>
        public virtual string ObterSerie()
        {
            var valor = string.Empty;

            if (Serie != null)
                valor = Serie.Codigo;

            return valor;
        }

        #endregion
    }
}