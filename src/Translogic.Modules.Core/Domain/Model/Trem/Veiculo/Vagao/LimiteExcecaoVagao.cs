﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Base de Limite mínimo
    /// </summary>
    public class LimiteExcecaoVagao : EntidadeBase<int>
    {
        /// <summary>
        /// propriedade Menor Valor da Rota
        /// </summary>
        public virtual LimiteMinimoBase LimiteMinimoBase { get; set; }

        /// <summary>
        /// propriedade Data Inicial
        /// </summary>
        public virtual DateTime DataInicial { get; set; }

        /// <summary>
        /// propriedade Data Final
        /// </summary>
        public virtual DateTime? DataFinal { get; set; }

        /// <summary>
        /// Usuário que cadastrou o limite
        /// </summary>
        public virtual Usuario UsuarioCadastro { get; set; }

        /// <summary>
        /// Vagão do Limite
        /// </summary>
        public virtual Vagao Vagao { get; set; }
        
        /// <summary>
        /// Limite mNI permitido
        /// </summary>
        public virtual decimal PesoMinimo { get; set; }
    }
}