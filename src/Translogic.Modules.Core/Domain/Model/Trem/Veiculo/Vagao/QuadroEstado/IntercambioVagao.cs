﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associacao do Intercambio com um Vagao - table - INTERC_VAGAO
	/// </summary>
	public class IntercambioVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Início
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.EventoVagao"/>
		/// </summary>
		public virtual EventoVagao EventoVagao { get; set; }

		/// <summary>
		/// Intercambio - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Intercambio"/>
		/// </summary>
		public virtual Intercambio Intercambio { get; set; }

		/// <summary>
		/// Vagao - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}