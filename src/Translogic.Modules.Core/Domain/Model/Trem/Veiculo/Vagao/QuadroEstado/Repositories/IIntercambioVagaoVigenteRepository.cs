namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using QuadroEstado;

    /// <summary>
	/// Contrato do Reposit�rio de <see cref="IntercambioVagaoVigente"/>
	/// </summary>
	public interface IIntercambioVagaoVigenteRepository : IRepository<IntercambioVagaoVigente, int>
	{
        /// <summary>
        /// Obt�m o Intercambio vigente do vag�o pelo c�digo do vagao
        /// </summary>
        /// <param name="codigoVagao"> The codigo vagao. </param>
        /// <returns> Objeto IntercambioVagaoVigente </returns>
        IntercambioVagaoVigente ObterPorVagao(Vagao codigoVagao);
	}
}