﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associação do responsável com o vagao  - table - RESP_VAGAO
	/// </summary>
	public class ResponsavelVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.EventoVagao"/>
		/// </summary>
		public virtual EventoVagao EventoVagao { get; set; }

		/// <summary>
		/// Responsável - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Responsavel"/>
		/// </summary>
		public virtual Responsavel Responsavel { get; set; }

		/// <summary>
		/// Vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}