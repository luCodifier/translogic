namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using QuadroEstado;

    /// <summary>
	/// Contrato do Repositório de <see cref="SituacaoVagao"/>
	/// </summary>
	public interface ISituacaoVagaoRepository : IRepository<SituacaoVagao, int>
	{
	}
}