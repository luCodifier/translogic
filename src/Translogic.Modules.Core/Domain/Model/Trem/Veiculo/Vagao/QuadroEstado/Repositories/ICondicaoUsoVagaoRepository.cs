namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="CondicaoUsoVagao"/>
	/// </summary>
    public interface ICondicaoUsoVagaoRepository : IRepository<CondicaoUsoVagao, int>
	{
	}
}