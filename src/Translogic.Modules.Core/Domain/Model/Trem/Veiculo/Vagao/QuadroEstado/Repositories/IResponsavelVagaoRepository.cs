namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    
    /// <summary>
    /// Contrato do Repositório de <see cref="ResponsavelVagao"/>
	/// </summary>
	public interface IResponsavelVagaoRepository : IRepository<ResponsavelVagao, int>
	{
	}
}