namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="LocalizacaoVagaoVigente"/>
	/// </summary>
    public interface ILocalizacaoVagaoVigenteRepository : IRepository<LocalizacaoVagaoVigente, int>
	{
        /// <summary>
        /// Obt�m os vag�es por localiza��o
        /// </summary>
        /// <param name="codigoVagao">Codigo do Vag�o</param>
        /// <returns> Lista de LocalizacaoVagaoVigente </returns>
        IList<LocalizacaoVagaoVigente> ObterPorCodigoVagao(string codigoVagao);

        /// <summary>
        /// Obtem a localiza��o do vagao para saber se est� em intercambio
        /// </summary>
        /// <param name="vagao">Objeto Vagao</param>
        /// <returns>Objeto LocalizacaoVagaoVigente</returns>
        LocalizacaoVagaoVigente ObterEmOutraFerroviaPorVagao(Vagao vagao);
	}
}