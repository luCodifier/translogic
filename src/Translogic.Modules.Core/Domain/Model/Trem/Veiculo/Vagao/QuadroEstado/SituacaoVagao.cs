﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using Codificador;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associação da situação de um vagao - table - SITUACAO_VAGAO
	/// </summary>
	public class SituacaoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de Vagão - <see cref="Translogic.Modules.Core.Domain.Model.Codificador.Evento"/>
		/// </summary>
		public virtual EventoVagao EventoVagao { get; set; }

		/// <summary>
		/// Motivo do evento - <see cref="Translogic.Modules.Core.Domain.Model.Codificador.MotivoEvento"/>
		/// </summary>
		public virtual MotivoEvento MotivoEvento { get; set; }

		/// <summary>
		/// Situação - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Situacao"/>
		/// </summary>
		public virtual Situacao Situacao { get; set; }

		/// <summary>
		/// Vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}