﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associação da condição de uso do vagao
	/// </summary>
	public class CondicaoUsoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Condição de uso - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.CondicaoUso"/>
		/// </summary>
		public virtual CondicaoUso CondicaoUso { get; set; }

		/// <summary>
		/// Data de Inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Término
		/// </summary>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de Vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.EventoVagao"/>
		/// </summary>
		public virtual EventoVagao EventoVagao { get; set; }

		/// <summary>
		/// Vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}