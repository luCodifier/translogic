namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;

    /// <summary>
    /// Contrato do Repositório de <see cref="LocalizacaoVagao"/>
	/// </summary>
    public interface ILocalizacaoVagaoRepository : IRepository<LocalizacaoVagao, int>
	{
	}
}