namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using QuadroEstado;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="SituacaoVagaoVigente"/>
	/// </summary>
    public interface ISituacaoVagaoVigenteRepository : IRepository<SituacaoVagaoVigente, int>
	{
        /// <summary>
        /// Obt�m a situa��o do vag�o que est� vigente para o vag�o
        /// </summary>
        /// <param name="vagao">Objeto vagao</param>
        /// <returns>Objeto SituacaoVagaoVigente</returns>
        SituacaoVagaoVigente ObterPorVagao(Vagao vagao);
	}
}