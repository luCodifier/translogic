namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;
    using QuadroEstado;

    /// <summary>
    /// Contrato do Repositório de <see cref="IntercambioVagao"/>
	/// </summary>
	public interface IIntercambioVagaoRepository : IRepository<IntercambioVagao, int>
	{
	}
}