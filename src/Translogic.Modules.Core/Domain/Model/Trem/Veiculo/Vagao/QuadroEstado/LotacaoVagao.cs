﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado
{
	using System;
	using ALL.Core.Dominio;
	using QuadroEstados.Dimensoes;

	/// <summary>
	/// Representa a associacao do Vagao com a Carga
	/// </summary>
	/// <remarks>
	/// Lotacao = Carga
	/// </remarks>
	public class LotacaoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de Inicio
		/// </summary>
		public virtual DateTime? DataInicio { get; set; }

		/// <summary>
		/// Data de Término
		/// </summary>
		/// <remarks>
		/// Quando este atributo estiver nulo haverá o Vigente
		/// </remarks>
		public virtual DateTime? DataTermino { get; set; }

		/// <summary>
		/// Evento de vagão - <see cref="Veiculo.Vagao.EventoVagao"/>
		/// </summary>
		public virtual EventoVagao EventoVagao { get; set; }

		/// <summary>
		/// Lotação do vagão - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Lotacao"/>
		/// </summary>
		public virtual Lotacao Lotacao { get; set; }

		/// <summary>
		/// Vagão -  <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Vagao"/>
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		#endregion
	}
}