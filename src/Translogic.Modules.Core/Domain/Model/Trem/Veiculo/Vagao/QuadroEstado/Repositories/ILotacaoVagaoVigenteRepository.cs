namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="LotacaoVagao"/>
	/// </summary>
    public interface ILotacaoVagaoVigenteRepository : IRepository<LotacaoVagaoVigente, int>
	{
	}
}