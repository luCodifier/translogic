namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado.Repositories
{
    using System;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="LotacaoVagao"/>
	/// </summary>
    public interface ILotacaoVagaoRepository : IRepository<LotacaoVagao, int>
    {
        /// <summary>
        /// Indica se o vag�o est� carregado
        /// </summary>
        /// <param name="vagao">Vagao a ser verificado</param>
        /// <param name="data">Data a ser verificada</param>
        /// <returns>Retorna true se o vag�o estiver carregado.</returns>
        bool VerificarVagaoCarregado(Vagao vagao, DateTime data);
    }
}