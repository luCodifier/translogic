﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe VagaoTaraEdiDescarga
	/// </summary>
    [Serializable]
    public class VagaoLogTara : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        ///     propriedade CodVagao
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        ///     propriedade Usuario
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
        ///     propriedade TaraAntiga
        /// </summary>
        public virtual decimal TaraAntiga { get; set; }

        /// <summary>
        ///     propriedade TaraNova
        /// </summary>
        public virtual decimal TaraNova { get; set; }

        /// <summary>
        ///     propriedade DataRegistro
        /// </summary>
        public virtual DateTime? DataCadastro { get; set; }

        #endregion
    }
}