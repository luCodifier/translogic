﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
    /// Representa uma Liberacao de Anexação de Vagão Travados
	/// </summary>
    public class VagoesTravadosAnx : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        ///     propriedade Vagao
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        ///     propriedade DataFinal
        /// </summary>
        public virtual DateTime? DataTrava { get; set; }

        /// <summary>
        ///     propriedade DataLiberacao
        /// </summary>
        public virtual DateTime? DataLiberacao { get; set; }

        /// <summary>
        ///     Usuário que travou o vagão
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        ///     Usuário que liberou a anexação do vagão
        /// </summary>
        public virtual Usuario UsuarioLib { get; set; }

        /// <summary>
        ///     Local da Anexação do Vagão
        /// </summary>
        public virtual IAreaOperacional Local { get; set; }

        /// <summary>
        ///     propriedade DataAnexacao
        /// </summary>
        public virtual DateTime? DataAnexacao { get; set; }

        /// <summary>
        ///     propriedade Os
        /// </summary>
        public virtual int OS { get; set; }
        
        /// <summary>
        ///     propriedade Prefixo
        /// </summary>
        public virtual string Prefixo { get; set; }

        /// <summary>
        ///     propriedade DataRegistro
        /// </summary>
        public virtual DateTime? DataRegistro { get; set; }
        
        #endregion
    }
}