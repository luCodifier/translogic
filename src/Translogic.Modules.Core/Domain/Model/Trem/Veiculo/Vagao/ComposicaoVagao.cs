namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;

	/// <summary>
	/// Representa as todas as <see cref="Composicao">Composições</see> do <see cref="Vagao"/>
	/// </summary>
	public class ComposicaoVagao : BaseComposicaoVagao
	{
	}
}