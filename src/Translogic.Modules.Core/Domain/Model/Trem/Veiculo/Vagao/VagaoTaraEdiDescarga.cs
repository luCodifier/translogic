﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe VagaoTaraEdiDescarga
	/// </summary>
    public class VagaoTaraEdiDescarga : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        ///     propriedade Vagao
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        ///     propriedade CodVagao
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        ///     propriedade Tara
        /// </summary>
        public virtual decimal Tara { get; set; }

        /// <summary>
        ///     propriedade DataCadastro
        /// </summary>
        public virtual DateTime? DataCadastro { get; set; }

        /// <summary>
        ///     propriedade DataCadastro
        /// </summary>
        public virtual string DataCadastroStr
        {
            get { return DataCadastro.Value.ToString(); }
        }

        /// <summary>
        ///     propriedade CodVagao
        /// </summary>
        public virtual string Remetente { get; set; }

        /// <summary>
        ///     propriedade TaraTonelada
        /// </summary>
        public virtual decimal TaraTonelada { get; set; }

        /// <summary>
        ///     propriedade DataConsolidacao
        /// </summary>
        public virtual DateTime? DataConsolidacao { get; set; }

        #endregion
    }
}