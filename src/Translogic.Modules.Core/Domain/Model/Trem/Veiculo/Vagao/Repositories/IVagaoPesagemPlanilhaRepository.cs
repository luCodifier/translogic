﻿using System.Collections.Generic;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="VagaoPesagemPlanilhaRepository"/>
    /// </summary>
    public interface IVagaoPesagemPlanilhaRepository : IRepository<VagaoPesagemPlanilha, decimal?>
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        IList<VagaoPesagemPlanilha> ObterPorCodigo(string codigo);        
    }
}
