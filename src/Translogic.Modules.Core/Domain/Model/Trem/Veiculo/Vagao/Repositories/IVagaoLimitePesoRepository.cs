﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoLimitePesoRepository"/>
    /// </summary>
    public interface IVagaoLimitePesoRepository : IRepository<VagaoLimitePeso, int?>
    {
        /// <summary>
        /// Retorna os vagões limite peso associados ao id do vagão
        /// </summary>
        /// <param name="id">código do vagão</param>
        /// <returns>Lista de vagões limite peso associados ao id do vagão</returns>
        IList<VagaoLimitePeso> ObterPorVagao(int id);

        /// <summary>
        ///     Obter média do peso informado para vagão travado por peso mínimo
        /// </summary>
        /// <param name="idVagao">Id do vagão</param>
        double? ObterMediaPesoInformadoVagaoTravadoPorPesoMinimo(int idVagao);
    }
}
