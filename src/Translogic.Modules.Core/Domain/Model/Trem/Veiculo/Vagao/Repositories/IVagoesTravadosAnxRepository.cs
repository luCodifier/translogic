﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagoesTravadosAnxRepository"/>
    /// </summary>
    public interface IVagoesTravadosAnxRepository : IRepository<VagoesTravadosAnx, int?>
    {
        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="local">local do vagão</param>
        /// <param name="prefixo">prefixo do Trem</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        ResultadoPaginado<VagoesTravadosAnx> ObterPorFiltro(DetalhesPaginacao pagination, DateTime? dataInicial, DateTime? dataFinal, string local, string prefixo, params object[] codigosDosVagoes);
    }
}
