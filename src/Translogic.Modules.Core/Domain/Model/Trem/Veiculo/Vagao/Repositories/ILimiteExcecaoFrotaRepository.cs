namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Veiculo.Vagao;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="LimiteExcecaoFrota"/>
	/// </summary>
    public interface ILimiteExcecaoFrotaRepository : IRepository<LimiteExcecaoFrota, int>
	{
        /// <summary>
        /// Obt�m os limites de acordo com a frota informada
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o</param>
        /// <param name="frota">Frota para se obter os limites</param>
        /// <returns>Limites paginados de acordo com o filtro</returns>
        ResultadoPaginado<LimiteExcecaoFrota> ObterPorFiltroFrota(DetalhesPaginacao pagination, string frota);

        /// <summary>
        /// Obt�m o limite m�ximo para a frota
        /// </summary>
        /// <param name="idFrota">Frota para se obter o limite</param>
        /// <returns>Limite m�ximo de peso para a frota</returns>
        LimiteExcecaoFrota ObterPorFrotaPesoMaximo(int idFrota);

        /// <summary>
        /// Salva um limite m�ximo para uma frota
        /// </summary>
        /// <param name="frota">Frota para grava��o de limite m�ximo</param>
        /// <param name="pesoMaximo">Peso m�ximo para o limite por frota</param>
        /// <param name="usuario">Usu�rio que est� realizando a opera��o</param>
        void SalvarLimiteMinimoVagao(FrotaVagao frota, string pesoMaximo, Usuario usuario);

        /// <summary>
        /// Desabilita um limite para a frota setando a data final
        /// </summary>
        /// <param name="limiteExcecaoFrotaId">Id do limite para desabilitar</param>
        void RemoverLimiteMaximo(int limiteExcecaoFrotaId);
	}
}