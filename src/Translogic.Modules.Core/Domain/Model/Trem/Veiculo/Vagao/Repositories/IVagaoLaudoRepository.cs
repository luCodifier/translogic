﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IVagaoLaudoRepository : IRepository<VagaoLaudo, int?>
    {
    }
}