﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoMedianaTempRepository"/>
    /// </summary>
    public interface IVagaoMedianaTempRepository : IRepository<VagaoMedianaTemp, int?>
    {

    }
}
