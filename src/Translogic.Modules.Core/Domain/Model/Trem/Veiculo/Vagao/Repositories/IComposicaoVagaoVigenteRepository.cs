namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;
    using Veiculo.Vagao;

    /// <summary>
	/// Contrato do Reposit�rio de <see cref="ComposicaoVagaoVigente"/>
	/// </summary>
	public interface IComposicaoVagaoVigenteRepository : IRepository<ComposicaoVagaoVigente, int>
	{
        /// <summary>
        /// Retorna a ComposicaoVagao a partir do c�digo do vag�o
        /// </summary>
        /// <param name="vagao">Objeto Vagao</param>
        /// <returns>A ComposicaoVagaoVigente</returns>
        ComposicaoVagaoVigente ObterPorVagao(Vagao vagao);

		/// <summary>
		/// Retorna as ComposicaoVagaoVigente a partir da composicao
		/// </summary>
		/// <param name="composicao">Composicao de referencia</param>
		/// <returns>Lista de ComposicaoVagaoVigente</returns>
		IList<ComposicaoVagaoVigente> ObterPorComposicao(Composicao composicao);
	}
}