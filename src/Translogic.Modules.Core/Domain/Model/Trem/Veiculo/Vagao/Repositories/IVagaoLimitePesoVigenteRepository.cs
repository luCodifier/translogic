﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoLimitePesoVigenteRepository"/>
    /// </summary>
    public interface IVagaoLimitePesoVigenteRepository : IRepository<VagaoLimitePesoVigente, int?>
    {
        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="dataInicial">data Inicial</param>
        /// <param name="dataFinal">data Final</param>
        /// <param name="linhaDeOrigem">linha De Origem</param>
        /// <param name="linhaDeDestino">linha De Destino</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        ResultadoPaginado<VagaoLimitePesoVigente> ObterPorFiltro(DetalhesPaginacao pagination, DateTime? dataInicial, DateTime? dataFinal, string linhaDeOrigem, string linhaDeDestino, params string[] codigosDosVagoes);
    }
}
