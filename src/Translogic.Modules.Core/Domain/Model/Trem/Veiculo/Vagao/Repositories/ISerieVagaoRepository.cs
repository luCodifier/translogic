namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="SerieVagao"/>
	/// </summary>
	public interface ISerieVagaoRepository : IRepository<SerieVagao, int>
	{
	}
}