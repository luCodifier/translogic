﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoTaraEdiDescargaRepository"/>
    /// </summary>
    public interface IVagaoTaraEdiDescargaRepository : IRepository<VagaoTaraEdiDescarga, int?>
    {
        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        IList<VagaoTaraEdiDescarga> ObterPorCodigo(string codigo);

        /// <summary>
        /// Retorna o vagão por id
        /// </summary>
        /// <param name="idVagao">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        IList<VagaoTaraEdiDescarga> ObterPorVagaoId(int idVagao);

        /// <summary>
        /// Obter id dos vagoes com pesagem
        /// </summary>
        /// <returns>Retorna id dos vagoes com pesagem</returns>
        IList<decimal> ObterVagoesComPesagens();
    }
}
