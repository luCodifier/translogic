﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Repositório de <see cref="FrotaVagao"/>
    /// </summary>
    public interface IFrotaVagaoRepository : IRepository<FrotaVagao, int>
    {
        /// <summary>
        /// Retorna as frotas por filtro de código
        /// </summary>
        /// <param name="frota">código da frota</param>
        /// <returns>Lista de frotas</returns>
        IList<FrotaVagao> ObterFrotas(string frota);
    }
}