namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Veiculo.Vagao;

    /// <summary>
	/// Contrato do Repositório de <see cref="VagaoPatio"/>
	/// </summary>
	public interface IVagaoPatioRepository : IRepository<VagaoPatio, int?>
	{
	}
}