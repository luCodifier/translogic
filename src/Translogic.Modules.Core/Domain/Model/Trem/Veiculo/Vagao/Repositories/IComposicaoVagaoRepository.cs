namespace Translogic.Modules.Core.Domain.Model.Trem.OrdemServico.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using DataAccess.Adaptations;
	using Interfaces.Trem.OrdemServico;
	using Veiculo.Vagao;
	using Via;

    /// <summary>
	/// Contrato do Repositório de <see cref="OrdemServico"/>
	/// </summary>
	public interface IComposicaoVagaoRepository : IRepository<ComposicaoVagao, int>
	{
        /// <summary>
        /// Retorna a ComposicaoVagao a partir da composicao
        /// </summary>
        /// <param name="composicao">Composicao de referencia</param>
        /// <returns>A ComposicaoVagao</returns>
        IList<ComposicaoVagao> ObterPorComposicao(Composicao composicao);
	}
}