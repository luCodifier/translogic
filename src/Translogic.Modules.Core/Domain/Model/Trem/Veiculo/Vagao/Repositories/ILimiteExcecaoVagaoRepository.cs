namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Veiculo.Vagao;

    /// <summary>
    /// Contrato do Reposit�rio de <see cref="LimiteExcecaoVagao"/>
	/// </summary>
    public interface ILimiteExcecaoVagaoRepository : IRepository<LimiteExcecaoVagao, int>
    {
        /// <summary>
        /// Obt�m os vag�es por filtro
        /// </summary>
        /// <param name="paginacao">Detalhes de pagina��o</param>
        /// <param name="vagoes">Vag�es informados no filtro</param>
        /// <param name="mercadoria">Mercadoria informada no filtro</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        ResultadoPaginado<LimiteExcecaoVagao> ObterPorFiltros(DetalhesPaginacao paginacao, string vagoes, string mercadoria);

        /// <summary>
        /// Obt�m o limite m�nimo exce��o para o vag�o e mercadoria de acordo com os filtros
        /// </summary>
        /// <param name="vagao">Vag�o para verificar o limite</param>
        /// <param name="mercadoria">Mercadoria para o vag�o informado</param>
        /// <param name="pesoMinimo">Limite para o vag�o com a mercadoria informada</param>
        /// <returns>Limite para o vag�o de acordo com os filtros</returns>
        LimiteExcecaoVagao ObterPorVagaoMercadoriaPesoMinimo(string vagao, string mercadoria, decimal pesoMinimo);

        /// <summary>
        /// Obt�m um limite de exce��o pelo limite m�nimo base
        /// </summary>
        /// <param name="limiteMinimoBaseId">Id do limite m�nimo base</param>
        /// <returns>Limite para o vag�o de acordo com o LimiteMinimoBase informado</returns>
        LimiteExcecaoVagao ObterLimiteExcecaoVagaoPorLimiteMinimoBaseId(int limiteMinimoBaseId);

        /// <summary>
        /// Salva um limite m�nimo para o vag�o e mercadoria
        /// </summary>
        /// <param name="limite">limite de peso para esta mercadoria neste vag�o</param>
        /// <param name="vagao">vag�o que receber� o limite</param>
        /// <param name="pesoMinimo">o peso m�nimo que o vag�o aceita</param>
        /// <param name="usuario">Usu�rio que est� realizando a opera��o</param>
        void SalvarLimiteMinimoVagao(LimiteMinimoBase limite, Vagao vagao, string pesoMinimo, Usuario usuario);

        /// <summary>
        /// Desabilita o limite exce��o vag�o setando a data final
        /// </summary>
        /// <param name="idLimiteExcecaoVagao">Id do limite para desabilitar</param>
        void RemoverLimiteMinimoVagao(int idLimiteExcecaoVagao);
    }
}