﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoRepository"/>
    /// </summary>
    public interface IVagaoRepository : IRepository<Vagao, int?>
    {
        /// <summary>
        /// Retorna o Vagao a partir do código
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>O Vagao objeto</returns>
        Vagao ObterPorCodigo(string codigo);

        /// <summary>
        /// Retorna o vagão por código
        /// </summary>
        /// <param name="codigo">codigo vagão</param>
        /// <returns>O Vagao objeto</returns>
        IList<Vagao> ObterSeriePorCodigo(string codigo);

        /// <summary>
        /// Obtém um vagão pelo código exato
        /// </summary>
        /// <param name="codigo">Código do vagão</param>
        /// <returns>Vagão localizado</returns>
        Vagao ObterVagaoPorCodigoExato(string codigo);

        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        ResultadoPaginado<Vagao> ObterPorFiltro(DetalhesPaginacao pagination, params object[] codigosDosVagoes);
    }
}
