﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do respositório <see cref="LimiteMinimoBase"/>
    /// </summary>
    public interface ILimiteMinimoBaseRepository : IRepository<LimiteMinimoBase, int>
    {
        /// <summary>
        /// Obter o limite minimo base de acordo com o limite, mercadoria e vagão informados
        /// </summary>
        /// <param name="limite">Valor do limite informado</param>
        /// <param name="mercadoria">Mercadoria informada para se obter o limite minimo base</param>
        /// <param name="vagao">Vagão informado</param>
        /// <returns>Limite minimo base</returns>
        LimiteMinimoBase ObterPorLimiteMercadoriaVagao(decimal limite, string mercadoria, Vagao vagao);

        /// <summary>
        /// Obter o limite mínimo base de acordo com a TuMedia, mercadoria e vagão informados
        /// </summary>
        /// <param name="valorTuMedia">Valor da TuMedia</param>
        /// <param name="mercadoriaCod">Mercadoria informada para se obter o limite minimo base</param>
        /// <param name="vagao">Vagão informado</param>
        /// <returns>Limite minimo base</returns>
        LimiteMinimoBase ObterPorTuMercadoriaVagao(decimal valorTuMedia, string mercadoriaCod, Vagao vagao);
    }
}
