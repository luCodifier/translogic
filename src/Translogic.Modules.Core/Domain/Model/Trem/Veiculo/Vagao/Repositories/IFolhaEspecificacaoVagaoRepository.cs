﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da classe <see cref="FolhaEspecificacaoVagao"/>
	/// </summary>
	public interface IFolhaEspecificacaoVagaoRepository : IRepository<FolhaEspecificacaoVagao, int>
	{
	}
}