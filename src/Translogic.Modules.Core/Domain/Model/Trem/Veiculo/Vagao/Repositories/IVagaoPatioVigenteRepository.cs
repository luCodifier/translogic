namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Dto;
    using Veiculo.Vagao;

    /// <summary>
	/// Contrato do Reposit�rio de <see cref="VagaoPatioVigente"/>
	/// </summary>
	public interface IVagaoPatioVigenteRepository : IRepository<VagaoPatioVigente, int?>
    {
    	/// <summary>
    	/// Obt�m a lista de vag�es patio por id de esta��o m�e
    	/// </summary>
    	/// <param name="idEstacaoMae"> Id da estacao mae. </param>
    	/// <param name="idFluxo">Id do fluxo comercial</param>
    	/// <param name="codigoVagao">Codigo do vag�o</param>
    	/// <returns> Lista de Vag�es patios vigentes </returns>
    	IList<VagaoCarregamentoDto> ObterParaCarregamentoPorIdEstacaoMae(int idEstacaoMae, int idFluxo, string codigoVagao);

        /// <summary>
        /// Obt�m o p�tio vigente em que o vag�o est�.
        /// </summary>
        /// <param name="codigoVagao">C�digo do vag�o</param>
        /// <returns>Objeto VagaoPatioVigente</returns>
        VagaoPatioVigente ObterPorVagao(Vagao codigoVagao);

        /// <summary>
        /// Obter Por Filtro
        /// </summary>
        /// <param name="pagination">Detalhes Paginacao</param>
        /// <param name="local">linha De Origem</param>
        /// <param name="codigosDosVagoes">codigos Dos Vagoes</param>
        /// <returns>Resultado Paginado VagaoAcimaPesoVigente</returns>
        ResultadoPaginado<VagaoPatioVigente> ObterPorFiltro(DetalhesPaginacao pagination, string local,
                                                            params object[] codigosDosVagoes);
    }
}