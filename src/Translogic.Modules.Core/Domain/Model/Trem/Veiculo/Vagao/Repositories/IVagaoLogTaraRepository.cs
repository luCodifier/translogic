﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    /// <summary>
    /// Contrato do Repositório de <see cref="IVagaoLogTaraRepository"/>
    /// </summary>
    public interface IVagaoLogTaraRepository : IRepository<VagaoLogTara, int?>
    {
     
    }
}
