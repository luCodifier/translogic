﻿namespace Translogic.Modules.Core.Domain.Model.Trem.Veiculo
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa um Equipamento
	/// TODO: implementar a classe
	/// </summary>
	public class Equipamento : EntidadeBase<int>
	{
        /// <summary>
        /// Codigo do Equipamento
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Descricao resumida do Equipamento
        /// </summary>
        public virtual string DescricaoResumida { get; set; }

        /// <summary>
        /// Descricao detalhada do Equipamento
        /// </summary>
        public virtual string DescricaoDetalhada { get; set; }
	}
}