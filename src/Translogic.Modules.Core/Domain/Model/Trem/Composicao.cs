﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using Diversos;
	using Veiculo.Locomotiva;
	using Veiculo.Vagao;
	using Via;

	/// <summary>
	/// Representa a composição de um <see cref="Trem"/>.
	/// Um Trem pode possuir várias composições durante o percurso de sua Rota
	/// </summary>
	public class Composicao : EntidadeBase<int?>
	{
		private IList<ComposicaoVagao> _listaVagao;
		private IList<ComposicaoLocomotiva> _listaLocomotiva;

		#region PROPRIEDADES

		/// <summary>
		/// Initializes a new instance of the <see cref="Composicao"/> class.
		/// </summary>
		public Composicao()
		{
			_listaVagao = new List<ComposicaoVagao>();
			_listaLocomotiva = new List<ComposicaoLocomotiva>();
		}

		/// <summary>
		/// Data de Término da Composição
		/// </summary>
		public virtual DateTime? DataFim { get; set; }

		/// <summary>
		/// Data de Início da Composição
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de Liberação do Trem
		/// </summary>
		public virtual DateTime? DataLiberacao { get; set; }

		/// <summary>
		/// <see cref="EstacaoMae"/> de Destino
		/// </summary>
		public virtual EstacaoMae Destino { get; set; }

		/// <summary>
		/// <see cref="EstacaoMae"/> de origem
		/// </summary>
		public virtual EstacaoMae Origem { get; set; }

		/// <summary>
		/// Situação da Composição - <see cref="SituacaoComposicaoEnum"/>
		/// </summary>
		public virtual SituacaoComposicaoEnum? Situacao { get; set; }

		/// <summary>
		/// <see cref="Trem"/> da Composição
		/// </summary>
		public virtual Trem Trem { get; set; }

	    /// <summary>
	    /// Comprimento da composicao
	    /// </summary>
        public virtual double Comprimento { get; set; }

        /// <summary>
        /// TB da composicao
        /// </summary>
        public virtual double TB { get; set; }

		/// <summary>
		/// Lista de Vagões da composição.
		/// </summary>
		public virtual IList<ComposicaoVagao> ListaVagoes
		{
			get { return _listaVagao; }
			set { _listaVagao = value; }
		}

		/// <summary>
		/// Lista das locomotivas da composicao
		/// </summary>
		public virtual IList<ComposicaoLocomotiva> ListaLocomotivas
		{
			get { return _listaLocomotiva; }
			set { _listaLocomotiva = value; }
		}

		#endregion
	}
}