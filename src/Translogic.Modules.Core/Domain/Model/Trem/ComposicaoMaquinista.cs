﻿namespace Translogic.Modules.Core.Domain.Model.Trem
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;
	using Equipagem;
	using Via;

	/// <summary>
	/// Representa a composição de um <see cref="Trem"/>.
	/// Um Trem pode possuir várias composições durante o percurso de sua Rota
	/// </summary>
	public class ComposicaoMaquinista : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Timestamp da Composicao Maquinista
		/// </summary>
		public virtual DateTime Timestamp { get; set; }

		/// <summary>
        /// Data de Cadastro da Composição Maquinista
		/// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Maquinista da Composicao
        /// </summary>
        public virtual Maquinista Maquinista { get; set; }

        /// <summary>
        /// Composicao da CompMaquinista
        /// </summary>
        public virtual Composicao Composicao { get; set; }

        /// <summary>
        /// Função M:Maquinista, A:Ajudante
        /// </summary>
        public virtual string IndFuncao { get; set; }

		#endregion
	}
}