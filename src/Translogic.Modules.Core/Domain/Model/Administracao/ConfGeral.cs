﻿namespace Translogic.Modules.Core.Domain.Model.Administracao
{
    using ALL.Core.Dominio;
    using System;

    public class ConfGeral : EntidadeBase<string>
    {
        private string chave;

        public ConfGeral()
        {
            Data = DateTime.Now;
        }
        public ConfGeral(string chave)
            : this()
        {
            Id = chave.ToUpper();

        }

        public virtual string Valor { get; set; }
        public virtual string Descricao { get; set; }
        public virtual DateTime Data { get; protected set; }

        public virtual bool IsValid()
        {
            return !string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(Valor) && !string.IsNullOrEmpty(Descricao);
        }
    }
}