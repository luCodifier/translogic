﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Diversos;

namespace Translogic.Modules.Core.Domain.Model.Administracao
{
    public class LogConfGeral : EntidadeBase<Int32>
    {
        protected LogConfGeral()
        {
            Tipo = TipoLogConfGeralEnum.New;
            Data = DateTime.Now;
        }

        private TipoLogConfGeralEnum _tipo;

        public static LogConfGeral LogNovo(string chave, string usuario, string valor)
        {
            return new LogConfGeral
                       {
                           Chave = chave,
                           Usuario = usuario,
                           Anterior = valor,
                           Novo = valor
                       };
        }

        public static LogConfGeral LogEdicao(string chave, string usuario, string novoValor, string valorAnterior)
        {
            var log = LogNovo(chave, usuario, novoValor);
            log.Anterior = valorAnterior;
            log.Tipo = TipoLogConfGeralEnum.Edit;

            return log;
        }

        public static LogConfGeral LogExclusao(string chave, string usuario, string valorAnterior)
        {
            var log = LogNovo(chave, usuario, null);
            log.Anterior = valorAnterior;
            log.Tipo = TipoLogConfGeralEnum.Delete;

            return log;
        }

        public virtual string Chave { get; protected set; }
        public virtual string Usuario { get; protected set; }
        public virtual string Anterior { get; protected set; }
        public virtual string Novo { get; protected set; }
        public virtual TipoLogConfGeralEnum Tipo{ get; protected set; }
        public virtual DateTime Data { get; protected set; }
    }

}