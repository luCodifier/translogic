namespace Translogic.Modules.Core.Domain.Model.CadastroUnicoEstacoes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio de esta��o do cadastro �nico
	/// </summary>
	public interface IEstacaoRepository : IRepository<Estacao, int>
	{
	}
}