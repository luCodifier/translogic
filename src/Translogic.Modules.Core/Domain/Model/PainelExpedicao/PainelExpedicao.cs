﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao
{
    using ALL.Core.Dominio;

    public class PainelExpedicao : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Código do painel de expedicao
        /// </summary>
        public virtual string Codigo { get; set; }

        #endregion
    }
}