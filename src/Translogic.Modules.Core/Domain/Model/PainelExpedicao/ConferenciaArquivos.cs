﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao
{
    using System;

    public class ConferenciaArquivos
    {
        public decimal Id { get; set; }
        public decimal? IdAgrupamentoMd { get; set; }
        public decimal? IdAgrupamentoSimples { get; set; }
        public string Situacao { get; set; }
        public decimal VagaoId { get; set; }
        public string Vagao { get; set; }
        public decimal PesoTotal { get; set; }
        public string Fluxo { get; set; }
        public string FluxoSegmento { get; set; }
        public string FluxoMercadoria { get; set; }
        public string ChaveNota { get; set; }
        public decimal PesoNfe { get; set; }
        public decimal VolumeNfe { get; set; }
        public decimal PesoRateioNfe { get; set; }
        public string Conteiner { get; set; }
        public string ObservacaoRecusado { get; set; }
        public DateTime DataHoraCadastro { get; set; }
        public string MultiploDespacho { get; set; }
        public string MultiploDespachoStatus { get; set; }
    }
}