﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

    public interface IPainelExpedicaoRepository : IRepository<PainelExpedicao, int>
    {
        /// <summary>
        /// Obtem todos os operadores da configuração de cores
        /// </summary>
        /// <returns></returns>
        IList<Operador> ObterOperadores();

        /// <summary>
        /// Salva a configuração das cores
        /// </summary>
        void SalvarConfiguracoesCores(string usuario);


        /// <summary>
        /// Obtem as informacoes do Painel de Expedicao Visualizacao - Consolidacao das informacoes
        /// </summary>
        IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoVisualizacao(DetalhesPaginacao detalhesPaginacao, DateTime dataInicio,
                                                                    string uf,
                                                                    string origem,
                                                                    string origemFaturamento,
                                                                    string segmento,
                                                                    string malhaCentral,
                                                                    bool metricaLarga,
                                                                    bool metricaNorte,
                                                                    bool metricaSul);

        /// <summary>
        /// Obtem as informacoes do Painel de Expedicao Pedra - Consolidacao das informacoes Translogic X Sade
        /// </summary>
        IList<PainelExpedicaoVisualizacaoDto> ObterPainelExpedicaoPedra(DetalhesPaginacao detalhesPaginacao,
                                                                    string regiao,
                                                                    DateTime dataInicio,
                                                                    string uf,
                                                                    string origem,
                                                                    string origemFaturamento,
                                                                    string segmento,
                                                                    string malhaCentral,
                                                                    string pedra);


        /// <summary>
        /// Obtem as informacoes do Painel de Expedicao de Consulta - Informacoes detalhadas
        /// </summary>
        ResultadoPaginado<PainelExpedicaoConsultaDto> ObterPainelExpedicaoConsulta(DetalhesPaginacao detalhesPaginacao,
                                                                                DateTime dataInicial,
                                                                                DateTime dataFinal,
                                                                                string fluxo,
                                                                                string origem,
                                                                                string destino,
                                                                                string[] vagoes,
                                                                                string situacao,
                                                                                string lotacao,
                                                                                string localAtual,
                                                                                string cliente,
                                                                                string terminalFaturamento,
                                                                                string[] mercadorias,
                                                                                string segmentoId);

        /// <summary>
        /// Obtem as informações do Painel de Expedicao de Consulta de Documentos
        /// </summary>
        ResultadoPaginado<PainelExpedicaoConsultarDocumentosDto> ObterPainelExpedicaoConsultaDocumentos(
                                                                            DetalhesPaginacao pagination,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            bool statusCte,
                                                                            bool statusNfe,
                                                                            bool statusWebDanfe,
                                                                            bool statusTicket,
                                                                            bool statusDcl,
                                                                            bool statusLaudoMercadoria,
                                                                            bool outrasFerrovias,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel);

        /// <summary>
        /// Obtem as informações do relatório de Histórico de Faturamento
        /// </summary>
        ResultadoPaginado<RelatorioHistoricoFaturamentosDto> ObterHistoricoFaturamentos(
                                                                            DetalhesPaginacao pagination,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string recebedor,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            bool statusCte,
                                                                            bool statusNfe,
                                                                            bool statusWebDanfe,
                                                                            bool statusTicket,
                                                                            bool statusDcl,
                                                                            bool statusLaudoMercadoria,
                                                                            bool outrasFerrovias,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem,
                                                                            bool exportarExcel);

        /// <summary>
        /// Obtem as informações do relatório das informações do Laudo de Mercadorias
        /// </summary>
        ResultadoPaginado<PainelExpedicaoRelatorioLaudoDto> ObterPainelExpedicaoRelatorioLaudos(
                                                                            DetalhesPaginacao detalhesPaginacao,
                                                                            DateTime dataInicio,
                                                                            DateTime dataFinal,
                                                                            string fluxo,
                                                                            string origem,
                                                                            string destino,
                                                                            string[] vagoes,
                                                                            string cliente,
                                                                            string expedidor,
                                                                            string remetente,
                                                                            string[] mercadorias,
                                                                            string segmento,
                                                                            string os,
                                                                            string prefixo,
                                                                            string[] itensDespacho,
                                                                            string cnpjRaizRecebedor,
                                                                            string malhaOrigem);

        /// <summary>
        /// Obtem as informações do relatório das informações do Laudo de Mercadorias através do ID da OS
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do Recebedor</param>
        ICollection<PainelExpedicaoRelatorioLaudoDto> ObterRelatorioLaudosPorOsId(decimal osId, decimal recebedorId);

        /// <summary>
        /// Obtem as informacoes do Painel de Expedicao de Conferencia de Arquivos
        /// </summary>
        ResultadoPaginado<PainelExpedicaoConferenciaArquivosDto> ObterPainelExpedicaoConferenciaArquivos(DetalhesPaginacao detalhesPaginacao,
                                                                                DateTime dataCadastroBo,
                                                                                DateTime dataCadastroFinalBo,
                                                                                string fluxo,
                                                                                string origem,
                                                                                string destino,
                                                                                string serie,
                                                                                string[] vagoes,
                                                                                string localAtual,
                                                                                string cliente,
                                                                                string expedidor,
                                                                                string[] mercadorias,
                                                                                string segmentoId,
                                                                                bool arquivosPatio,
                                                                                PainelExpedicaoPerfilAcessoEnum perfilAcesso);

        /// <summary>
        /// Salva as configurações editadas
        /// </summary>
        /// <param name="registro">Dados do registro contendo o ID a ser atualizado</param>
        void SalvarAlteracoesConferenciaArquivos(PainelExpedicaoConferenciaArquivosDto registro);

        /// <summary>
        /// Atualiza o status do registro
        /// </summary>
        /// <param name="id">ID do registro a ser atualizado o status</param>
        /// <param name="recusar">Indicandor se foi recusado(true) ou aprovado(false)</param>
        void AtualizarStatusConferenciaArquivos(int id,
                                                bool recusar,
                                                int usuarioId,
                                                PainelExpedicaoPerfilAcessoEnum perfilAcessoUsuario,
                                                string observacaoRecusado);

        /// <summary>
        /// Obtem o arquivo do documento solicitado
        /// </summary>
        int ImprimirDocumentos(int id);

        /// <summary>
        /// Obtém o registro de bloqueio de despacho de uma determinada 
        /// estação, segmento e cliente
        /// </summary>
        DespachoLocalBloqueioDto ObterDespachoLocalBloqueio(int? idEstacaoOrigem,
            string segmento,
            string cnpjRaiz,
            bool verBloqueioTodosClientes);

        IList<EstacaoFaturamentoDTO> ObterOrigemFaturamentoPorUf(string uf);
    }
}
