﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories
{
    /// <summary>
    /// Interface de repositório para <see cref="Origem"/>
    /// </summary>
    public interface IOrigemRepository : IRepository<Origem, int>
    {
        IList<Origem> ObterOrigens();
        Origem ObterOrigem(int id);
        IList<Origem> ObterOrigemFaturamentoPorUf(string uf);
    }

}
