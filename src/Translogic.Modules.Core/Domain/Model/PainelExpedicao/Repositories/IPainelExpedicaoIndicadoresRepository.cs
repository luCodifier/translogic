﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories
{
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;

    public interface IPainelExpedicaoIndicadoresRepository : IRepository<Indicador, int>
    {
        Indicador ObterPorColunaCor(int coluna, int cor);
        IList<Indicador> ObterPorColuna(int coluna);
    }
}
