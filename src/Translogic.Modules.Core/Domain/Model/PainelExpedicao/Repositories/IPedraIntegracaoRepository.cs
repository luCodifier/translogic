﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

    public interface IPedraIntegracaoRepository : IRepository<PedraRealizado, int>
    {

        /// <summary>
        /// Obtem as informacoes do Painel de Expedicao Pedra - Consolidacao das informacoes Translogic X Sade
        /// </summary>
        IList<ConsultaPedraRealizadoDto> ObterPainelExpedicaoPedra(string operacao, string regiao, string dataPedra, string uf, string origem, string estacaoFaturamento, string segmento, string terminal, string ocultarPedraZerada);


    }
}
