﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    using System.ComponentModel;

    public enum ColunaEnum
    {
        [Description("Pendente Fat")]
        PendenteFaturamento = 1,
        [Description("CT-e Pendente")]
		CTePendente = 2,
		[Description("Pendente de Confirmacao")]
		PendenteConfirmacao = 3,
		[Description("Recusado Estação")]
		RecusadoEstacao = 4
    }
}