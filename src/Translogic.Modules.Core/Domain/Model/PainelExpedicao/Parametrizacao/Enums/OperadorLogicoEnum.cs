﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    using System.ComponentModel;

    public enum OperadorLogicoEnum
    {
        [Description("Nenhum")]
        Nenhum = 0,
        [Description("AND")]
        And = 1,
        [Description("OR")]
        Or = 2
    }
}