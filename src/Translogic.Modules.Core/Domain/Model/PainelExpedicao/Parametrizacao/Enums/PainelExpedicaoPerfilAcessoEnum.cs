﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    public enum PainelExpedicaoPerfilAcessoEnum
    {
        [Description("Pátio")]
        Patio = 1,

        [Description("Central")]
        Central = 2,

        [Description("CentralSuper")]
        CentralSuper = 3,

        [Description("Indefinido")]
        Indefinido = 0
    }

}