﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    using System.ComponentModel;

    public enum CorEnum
    {
        [Description("Não Definida")]
        NaoDefinida = 0,
        [Description("Vermelho")]
        Vermelho = 1,
        [Description("Amarelo")]
        Amarelo = 2,
        [Description("Verde")]
        Verde = 3
    }
}