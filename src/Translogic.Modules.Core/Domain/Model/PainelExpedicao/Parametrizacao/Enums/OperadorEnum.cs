﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    using System.ComponentModel;

    public enum OperadorEnum
    {
        [Description("Menor")]
        Menor = 1,

        [Description("Menor Igual")]
        MenorIgual = 2,

        [Description("Igual")]
        Igual = 3,

        [Description("Diferente")]
        Diferente = 4,

        [Description("Maior Igual")]
        MaiorIgual = 5,

        [Description("Maior")]
        Maior = 6
    }
}