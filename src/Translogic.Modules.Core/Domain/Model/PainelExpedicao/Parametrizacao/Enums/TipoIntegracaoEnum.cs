﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums
{
    using System.ComponentModel;

    public enum TipoIntegracaoEnum
    {
        [Description("Não Definida")]
        NaoDefinida = 0,
        [Description("Shipping Note")]
        ShippingNote = 1,
        [Description("Bizconnect")]
        Bizconnect = 2,
        [Description("CAALL")]
        Caall = 3,
        [Description("Manual")]
        Manual = 4
    }
}