﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    public class RegraEmailsDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Id da Operacao
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Nome da Operacao
        /// </summary>
        public virtual string Nome { get; set; }

        public virtual RegiaoDto Regiao { get; set; }

        public virtual string Destinatarios { get; set; }

        public virtual string TituloEmail { get; set; }

        public virtual string CorpoEmail { get; set; }

        /// <summary>
        /// Data que foi cadatrado na base
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Usuario que cadastrou na base
        /// </summary>
        public virtual int UsuarioCadastro { get; set; }

        /// <summary>
        /// Data/hora da ultima alteracao na base
        /// </summary>
        public virtual DateTime? DataAlteracao { get; set; }

        /// <summary>
        /// Id do Usuario que realizou a ultima alteracao na base
        /// </summary>
        public virtual int? UsuarioAlteracao { get; set; }

        #endregion
    }
}