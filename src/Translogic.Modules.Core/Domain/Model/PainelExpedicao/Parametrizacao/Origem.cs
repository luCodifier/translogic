﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{

    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Via;

    public class Origem : EntidadeBase<int>
    {
        #region Propriedades

        public virtual int IdOrigem { get; set; }

        public virtual IAreaOperacional AreaOperacional { get; set; }

		public virtual IAreaOperacional AreaOperacionalFaturamento { get; set; }

        public virtual  Regiao Regiao { get; set; }

        #endregion
    }


}