﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Via;

    public class ClienteTipoIntegracao : EntidadeBase<int>
    {
        #region Propriedades

        public virtual int IdTipo { get; set; }

        public virtual IEmpresa Empresa { get; set; }

        public virtual IAreaOperacional Estacao { get; set; }

        #endregion
    }
}