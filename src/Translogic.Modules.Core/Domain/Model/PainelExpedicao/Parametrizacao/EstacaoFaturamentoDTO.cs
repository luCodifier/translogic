﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    public class EstacaoFaturamentoDTO 
    {
        public virtual string Codigo { get; set; }

		public virtual string Descricao { get; set; }

        public virtual  string UF { get; set; }

    }
}