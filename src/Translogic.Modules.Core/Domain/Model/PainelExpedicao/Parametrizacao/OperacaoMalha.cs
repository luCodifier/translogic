﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{

    using ALL.Core.Dominio;

    public class OperacaoMalha : EntidadeBase<int>
    {
        #region Propriedades

        public virtual string Nome { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual int UsuarioCadastro { get; set; }

        public virtual DateTime? DataAlteracao { get; set; }

        public virtual int? UsuarioAlteracao { get; set; }

        #endregion
    }


}