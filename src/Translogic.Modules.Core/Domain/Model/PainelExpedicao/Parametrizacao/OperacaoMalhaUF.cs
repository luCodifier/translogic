﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{

    using ALL.Core.Dominio;

    public class OperacaoMalhaUF : EntidadeBase<int>
    {
        #region Propriedades

        public virtual decimal OperacaoId { get; set; }
        public virtual decimal EstadoId { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual decimal UsuarioCadastro { get; set; }
        public virtual DateTime? DataAlteracao { get; set; }
        public virtual decimal? UsuarioAlteracao { get; set; }

        #endregion
    }


}