﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao
{

    using ALL.Core.Dominio;

    public class Regiao : EntidadeBase<int>
    {
        #region Propriedades

        public virtual string Nome { get; set; }

        public virtual decimal OperacaoId { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        public virtual int UsuarioCadastro { get; set; }

        public virtual DateTime? DataAlteracao { get; set; }

        public virtual int? UsuarioAlteracao { get; set; }

        #endregion
    }


}