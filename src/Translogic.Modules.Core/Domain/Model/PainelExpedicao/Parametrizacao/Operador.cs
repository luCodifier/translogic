﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    using ALL.Core.Dominio;

    public class Operador : EntidadeBase<int>
    {
        #region Propriedades

        public virtual int IdOperador { get; set; }

        public virtual string Simbolo { get; set; }

        public virtual string Descricao { get; set; }

        #endregion
    }
}