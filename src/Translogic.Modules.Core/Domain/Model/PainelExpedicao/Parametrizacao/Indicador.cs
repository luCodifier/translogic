﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

    public class Indicador : EntidadeBase<int>
    {
        public virtual int IdIndicador { get; set; }

        public virtual int IdCor { get; set; }

        public virtual CorEnum CorEnum
        {
            get
            {
                CorEnum resultado;
                switch (IdCor)
                {
                    case (int)CorEnum.Vermelho:
                        resultado = CorEnum.Vermelho;
                        break;

                    case (int)CorEnum.Amarelo:
                        resultado = CorEnum.Amarelo;
                        break;

                    case (int)CorEnum.Verde:
                        resultado = CorEnum.Verde;
                        break;

                    default:
                        resultado = CorEnum.NaoDefinida;
                        break;
                }

                return resultado;
            }
        }

        public virtual Operador Operador1 { get; set; }

        public virtual decimal Valor1 { get; set; }

        public virtual Operador Operador2 { get; set; }

        public virtual decimal? Valor2 { get; set; }

        public virtual int? IdOperadorLogico { get; set; }

        public virtual OperadorLogicoEnum OperadorLogicoEnum
        {
            get
            {
                var resultado = OperadorLogicoEnum.Nenhum;
                if (IdOperadorLogico.HasValue)
                {
                    switch (IdOperadorLogico.Value)
                    {
                        case (int)OperadorLogicoEnum.And:
                            resultado = OperadorLogicoEnum.And;
                            break;

                        case (int)OperadorLogicoEnum.Or:
                            resultado = OperadorLogicoEnum.Or;
                            break;

                        default:
                            resultado = OperadorLogicoEnum.Nenhum;
                            break;
                    }
                }

                return resultado;
            }
        }

        public virtual int IdColuna { get; set; }

        public virtual ColunaEnum ColunaEnum
        {
            get
            {
                ColunaEnum resultado;
                switch (IdColuna)
                {
                    case (int)ColunaEnum.PendenteFaturamento:
                        resultado = ColunaEnum.PendenteFaturamento;
                        break;

                    case (int)ColunaEnum.CTePendente:
                        resultado = ColunaEnum.CTePendente;
                        break;

					case (int)ColunaEnum.PendenteConfirmacao:
						resultado = ColunaEnum.PendenteConfirmacao;
						break;

					case (int)ColunaEnum.RecusadoEstacao:
						resultado = ColunaEnum.RecusadoEstacao;
						break;

                    default:
                        resultado = ColunaEnum.CTePendente;
                        break;
                }

                return resultado;
            }
        }
    }
}