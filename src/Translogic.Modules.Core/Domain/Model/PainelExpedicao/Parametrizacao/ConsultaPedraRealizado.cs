﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao
{
    public class ConsultaPedraRealizado : EntidadeBase<int>
    {
        public virtual DateTime DataPedra { get; set; }
        public virtual string Operacao { get; set; }
        public virtual decimal OperacaoId { get; set; }
        public virtual string Regiao { get; set; }
        public virtual decimal RegiaoId { get; set; }
        public virtual decimal FluxoId { get; set; }
        public virtual decimal ReuniaoId { get; set; }
        public virtual string EstacaoFaturamento { get; set; }
        public virtual decimal EstacaoFaturamentoId { get; set; }
        public virtual string EstacaoOrigem { get; set; }
        public virtual decimal EstacaoOrigemId { get; set; }
        public virtual string Terminal { get; set; }
        public virtual decimal TerminalId { get; set; }
        public virtual string Segmento { get; set; }
        public virtual decimal PedraSade { get; set; }
        public virtual decimal Realizado { get; set; }
        public virtual decimal PedraEditado { get; set; }
        public virtual string StatusEditado { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual decimal UsuarioCadastro { get; set; }
        public virtual DateTime DataAlteracao { get; set; }
        public virtual decimal UsuarioAlteracao { get; set; }


    }
}