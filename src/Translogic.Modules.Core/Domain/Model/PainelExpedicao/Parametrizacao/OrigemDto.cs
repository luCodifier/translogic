﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao
{
    public class OrigemDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Código da área operacional
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Código da área operacional
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Descrição da origem (Area Operacional)
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}