﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request
{
    public class ImpressaoDocumentosVagao
    {
        public List<ImpressaoDocumentosVagaoItemDespacho> VagoesItensDespacho { get; set; }

        public bool ImprimeTicket { get; set; }

        public bool ImprimeCte { get; set; }

        public bool ImprimeNfe { get; set; }

        public bool ImprimeDcl { get; set; }
    }
}