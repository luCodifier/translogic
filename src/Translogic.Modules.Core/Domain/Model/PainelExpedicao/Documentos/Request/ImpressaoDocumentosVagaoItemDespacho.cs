﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request
{
    using System;

    public class ImpressaoDocumentosVagaoItemDespacho
    {
        public decimal IdItemDespacho { get; set; }

        public string CodigoVagao { get; set; }

        public DateTime DataCarga { get; set; }

        public decimal IdCte { get; set; }

        public decimal? IdComposicao { get; set; }

        public decimal? Sequencia { get; set; }

        public string CodigoFluxo { get; set; }

        public string SegmentoFluxo { get; set; }
    }
}