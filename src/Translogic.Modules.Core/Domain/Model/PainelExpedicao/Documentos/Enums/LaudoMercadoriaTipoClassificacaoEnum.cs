﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Enums
{
    using System.ComponentModel;

    public enum LaudoMercadoriaTipoClassificacaoEnum
    {
        [Description("Indefinido")]
        Indefinido = 0,
        [Description("Umidade")]
        Umidade = 1,
        [Description("Materia Estranha/Impura")]
        MateriaEstranhaImpura = 2,
        [Description("Total de Avariados")]
        TotalAvariados = 3,
        [Description("Danificado")]
        Danificado = 4,
        [Description("Quebrado")]
        Quebrado = 5,
        [Description("Ardido")]
        Ardido = 6,
        [Description("PH")]
        Ph = 7,
        [Description("Esverdeado")]
        Esverdeado = 8,
        [Description("Queimado")]
        Queimado = 9,
        [Description("Picado")]
        Picado = 10,
        [Description("Mofado")]
        Mofado = 11,
        [Description("Falling Number")]
        FallingNumber = 12,
        [Description("Germinado")]
        Germinado = 13,
        [Description("Danificado Insetos")]
        DanificadoInsetos = 14
    }
}