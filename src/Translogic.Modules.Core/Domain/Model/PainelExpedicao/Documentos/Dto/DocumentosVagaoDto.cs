﻿using System;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto
{
    public class DocumentosVagaoDto
    {
        public DocumentosVagaoDto()
        {
            Pdfs = new PdfMerge();
            CodigoFluxos = new List<string>();
        }

        public string CodigoVagao { get; set; }

        public PdfMerge Pdfs { get; set; }

        public DateTime DataCarga { get; set; }

        public decimal? Sequencia { get; set; }

        public ICollection<string> CodigoFluxos { get; set; }
    }
}