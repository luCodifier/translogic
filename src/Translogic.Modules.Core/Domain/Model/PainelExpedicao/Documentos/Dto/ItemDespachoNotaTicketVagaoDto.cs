﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto
{
    public class ItemDespachoNotaTicketVagaoDto
    {
        public int IdItemDespacho { get; set; }

        public string SerieVagao { get; set; }

        public NotaTicketVagao NotaTicketVagao { get; set; }
    }
}