﻿namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto
{
    public class ItemDespachoNfeDto
    {
        public decimal IdItemDespacho { get; set; }

        public decimal IdNfePdf { get; set; }

        public string ChaveNfe { get; set; }

        public byte[] Pdf { get; set; }
    }
}