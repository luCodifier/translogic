﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

namespace Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto
{
    public class ItemDespachoDespachoDto
    {
        public decimal IdItemDespacho { get; set; }

        public DespachoTranslogic Despacho { get; set; }

        public ItemDespachoDespachoDto() { }

        public static explicit operator ItemDespachoDespachoDto(ItemDespacho itemDespacho)
        {
            ItemDespachoDespachoDto itemDespachoDto = new ItemDespachoDespachoDto();
            itemDespachoDto.IdItemDespacho = Convert.ToDecimal(itemDespacho.Id.Value);
            itemDespachoDto.Despacho = itemDespacho.DespachoTranslogic;

            return itemDespachoDto;
        }
    }
}