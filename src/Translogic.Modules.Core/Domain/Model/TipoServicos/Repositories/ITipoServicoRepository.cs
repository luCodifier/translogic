﻿
namespace Translogic.Modules.Core.Domain.Model.TipoServicos.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model;
    using Translogic.Modules.Core.Util;
    using System.Collections.Generic;

    public interface ITipoServicoRepository : IRepository<TipoServico, int>
    {
        IList<TipoServicoDto> ObterTipoServico();

        /// <summary>
        /// Retorna o objeto TipoServico
        /// </summary>
        /// <param name="descricaoServico"></param>
        /// <returns></returns>
        TipoServico ObterTipoServicoPorDescricaoServico(string descricaoTipoServico);
      

    }
}
