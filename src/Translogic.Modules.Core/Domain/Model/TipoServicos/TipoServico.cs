﻿using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.TipoServicos
{
    public class TipoServico : EntidadeBase<int>
    {
        /// <summary>
        /// Serviço
        /// </summary>
        public virtual string Servico { get; set; }      

    }
}