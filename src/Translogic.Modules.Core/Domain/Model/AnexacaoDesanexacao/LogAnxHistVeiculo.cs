﻿namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao
{
    using Acesso;
    using ALL.Core.Dominio;
    using System;

    /// <summary>
    /// Representa Log Anx Hist Veiculo
    /// </summary>
    public class LogAnxHistVeiculo : EntidadeBase<int>
    {
        #region PROPRIEDADES
        /// <summary>
        /// OS
        /// </summary>
        public virtual decimal OS { get; set; }

        /// <summary>
        /// DATA AÇÃO
        /// </summary>
        public virtual DateTime DataAcao { get; set; }

        /// <summary>
        /// Numero Veiculo
        /// </summary>
        public virtual string NumeroVeiculo { get; set; }

        /// <summary>
        /// Numero Veiculo OLD
        /// </summary>
        public virtual string NumeroVeiculoOLD { get; set; }

        /// <summary>
        /// Local
        /// </summary>
        public virtual string Local { get; set; }

        /// <summary>
        /// Ação
        /// </summary>
        public virtual string Acao { get; set; }

        /// <summary>
        /// Processado
        /// </summary>
        public virtual string Processado { get; set; }

        /// <summary>
        /// TIMESTAMP
        /// </summary>
        public virtual DateTime Timestamp { get; set; }
        #endregion
    }
}