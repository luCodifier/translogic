﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories
{

    /// <summary>
    /// Interface de repositório de Consulta Trem
    /// </summary>
    public interface IAnexacaoDesanexacaoRepository : IRepository<AnexacaoDesanexacaoAuto, int>
    {
        /// <summary>
        /// Pesquisar Anexação e Desanexação automatica
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="anexacaoDesanexacaoRequestDto">Filtros pesquisa</param>
        /// <returns>Lista de dados para GRID</returns>
        ResultadoPaginado<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacao(DetalhesPaginacaoWeb detalhesPaginacaoWeb, AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto);

        /// <summary>
        /// Exportar Anexacao Desanexação
        /// </summary>
        /// <param name="anexacaoDesanexacaoRequestDto">Filtros pesquisa</param>
        /// <returns>Lista de dados para EXPORTAÇÃO EXCEL</returns>
        IEnumerable<AnexacaoDesanexacaoDto> ObterConsultaAnexacaoDesanexacaoExportar(AnexacaoDesanexacaoRequestDto anexacaoDesanexacaoRequestDto);

        /// <summary>
        /// Lista Veiculos do Trem
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="os"> os trem </param>
        /// <returns> Retorna Lista veiculos do trem</returns>
        ResultadoPaginado<VeiculoTremDto> ObterConsultaVeiculosTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, decimal os, string estacao, string acao);

        AnexacaoDesanexacaoDto ObterPorOS(decimal os, string numeroVeiculo);

        AnexacaoDesanexacaoAuto ObterPorOperacaoOsLocalParada(string operacao, decimal os, string localParada);

        string EncerrarParadaPKG(string xml);

        decimal ObterIdTrem(decimal os);

        decimal ObterIdLocal(string local);

        decimal ObterIdParada(decimal os, decimal idLocal);

        void UpdateSatusParada(string Operacao, decimal OS, string LocalParada);

        void ZeraTentativas(string Operacao, decimal OS);
    }
}