﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.Web;
namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories
{
    /// <summary>
    /// Interface de repositório LogAnxHistTrem
    /// </summary>
    public interface ILogAnxHistTremRepository : IRepository<LogAnxHistTrem, int>
    {
        LogAnxHistTrem ObterPorOS(decimal os);
    }
}