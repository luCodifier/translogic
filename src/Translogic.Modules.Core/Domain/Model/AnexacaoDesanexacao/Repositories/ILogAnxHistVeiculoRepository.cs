﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System.Collections.Generic;
using Translogic.Core.Infrastructure.Web;
namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao.Repositories
{

    /// <summary>
    /// Interface de repositório LogAnxHistVeiculo
    /// </summary>
    public interface ILogAnxHistVeiculoRepository : IRepository<LogAnxHistVeiculo, int>
    {
    }
}