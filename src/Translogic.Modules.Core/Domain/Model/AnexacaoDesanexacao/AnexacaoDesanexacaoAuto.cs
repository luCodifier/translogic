﻿namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao
{
    using Acesso;
    using ALL.Core.Dominio;
    using System;

    /// <summary>
    /// Representa anexacaoDesanexacao automatica
    /// </summary>
    public class AnexacaoDesanexacaoAuto : EntidadeBase<int>
    {
        #region PROPRIEDADES
        /// <summary>
        /// OS
        /// </summary>
        public virtual decimal OS { get; set; }

        /// <summary>
        /// Vagão Loco
        /// </summary>        
        public virtual string VagaoLoco { get; set; }

        /// <summary>
        /// Estação
        /// </summary>
        public virtual string Estacao { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Ação
        /// </summary>
        public virtual string Acao { get; set; }

        /// <summary>
        /// XML
        /// </summary>
        public virtual string XML { get; set; }

        /// <summary>
        /// Tentativas
        /// </summary>
        public virtual int Tentativas { get; set; }

        /// <summary>
        /// TIMESTAMP
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// Processado
        /// </summary>
        public virtual string Processado { get; set; }

        /// <summary>
        // Status Parada
        /// </summary>
        public virtual string StatusParada { get; set; }
        #endregion
    }
}