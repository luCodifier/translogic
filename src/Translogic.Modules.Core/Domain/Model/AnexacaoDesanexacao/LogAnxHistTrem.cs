﻿namespace Translogic.Modules.Core.Domain.Model.AnexacaoDesanexacao
{
    using Acesso;
    using ALL.Core.Dominio;
    using System;

    /// <summary>
    /// Representa Log Anx Hist Trem
    /// </summary>
    public class LogAnxHistTrem : EntidadeBase<int>
    {
        #region PROPRIEDADES
        /// <summary>
        /// OS
        /// </summary>
        public virtual decimal OS { get; set; }

        /// <summary>
        /// DATA AÇÃO
        /// </summary>
        public virtual DateTime DataAcao { get; set; }

        /// <summary>
        /// Ação
        /// </summary>
        public virtual string Acao { get; set; }

        /// <summary>
        /// TIMESTAMP
        /// </summary>
        public virtual DateTime Timestamp { get; set; }
        #endregion
    }
}