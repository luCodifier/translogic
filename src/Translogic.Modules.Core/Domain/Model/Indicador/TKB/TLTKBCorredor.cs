namespace Translogic.Modules.Core.Domain.Model.Indicador.TKB
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o Indicador TKB por Corredor
	/// </summary>
	public class TLTKBCorredor : EntidadeBase<int?>
	{
		#region PROPRIEDADES
		/// <summary>
		/// Trecho do Indicaor
		/// </summary>
		public virtual string Trecho { get; set; }

		/// <summary>
		/// Trecho diesel do indicador
		/// </summary>
		public virtual string TrechoDiesel { get; set; }

		/// <summary>
		/// Descri��o do corredor
		/// </summary>
		public virtual string Corredor { get; set; }

		/// <summary>
		/// Descricao do corredor complemento
		/// </summary>
		public virtual string CorredorComplemento { get; set; }

		/// <summary>
		/// Sentido que o trem passou
		/// </summary>
		public virtual string Sentido { get; set; }

		/// <summary>
		/// Descricao da unidade de produ��o
		/// </summary>
		public virtual string UP { get; set; }

		/// <summary>
		/// Quantidade consumida
		/// </summary>
		public virtual double Consumo { get; set; }
		#endregion
	}
}