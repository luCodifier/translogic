namespace Translogic.Modules.Core.Domain.Model.Indicador.TKB.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Contrato do Repositório de <see cref="TLTKBCorredor"/>
	/// </summary>
	public interface ITLTKBCorredorRepository : IRepository<TLTKBCorredor, int?>
	{
	}
}