namespace Translogic.Modules.Core.Domain.Model.Indicador.Vagao.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Contrato do Repositório de <see cref="IndLotacaoVagoes"/>
	/// </summary>
	public interface IIndLotacaoVagoesRepository : IRepository<IndLotacaoVagoes, int?>
	{
	}
}