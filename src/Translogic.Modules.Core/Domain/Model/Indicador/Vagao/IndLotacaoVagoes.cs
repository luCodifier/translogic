namespace Translogic.Modules.Core.Domain.Model.Indicador.Vagao
{
	using ALL.Core.Dominio;
	using FluxosComerciais;
	using Trem.Veiculo.Vagao;
	using Via;

	/// <summary>
	/// Representa o Indicador de Lota��o de Vag�es
	/// </summary>
	public class IndLotacaoVagoes : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Esta��o de origem do indicador
		/// </summary>
		public virtual EstacaoMae Origem { get; set; }

		/// <summary>
		/// Esta��o de destino do indicador
		/// </summary>
		public virtual EstacaoMae Destino { get; set; }

		/// <summary>
		/// Vag�o carregado
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		/// Mercadoria carragada no vag�o
		/// </summary>
		public virtual Mercadoria Mercadoria { get; set; }

		/// <summary>
		/// Fluxo Comercial do indicador
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		/// Peso carregado no vag�o
		/// </summary>
		public virtual double PesoCarregado { get; set; }

		/// <summary>
		/// Limite maximo de peso no vag�o
		/// </summary>
		public virtual double LimitePeso { get; set; }

		/// <summary>
		/// Valor da tara do vag�o
		/// </summary>
		public virtual double TaraVagao { get; set; }

		/// <summary>
		/// Tipo do limite do inidicador
		/// </summary>
		public virtual string TipoLimite { get; set; }

		#endregion
	}
}