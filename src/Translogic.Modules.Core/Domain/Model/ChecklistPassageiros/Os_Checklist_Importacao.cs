﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_IMPORTACAO
    /// Seq: OS_CHECKLIST_IMPORTACAO_SEQ
    /// </summary>
    public class Os_Checklist_Importacao : EntidadeBase<int>
    {
        public virtual  Os_Checklist_Aprovacao OsChecklistAprovacao { get; set; }
        public virtual  string TipoAprocacao { get; set; } // IDT_TIPO_APR CHAR(3) NOT NULL, -- VIA, LOC, TRA, VAG
        public virtual  string NomeArquivo { get; set; } //NM_ARQUIVO VARCHAR2(255) NOT NULL,
        public virtual  byte[] Arquivo { get; set; } //ARQUIVO_IMPORTADO BLOB NOT NULL

        public Os_Checklist_Importacao()
        {

        }

        public Os_Checklist_Importacao(
            Os_Checklist_Aprovacao osChecklistAprovacao,
            string tipoAprocacao,
            string nomeArquivo,
            byte[] arquivo)
        {
            this.OsChecklistAprovacao = osChecklistAprovacao;
            this.TipoAprocacao = tipoAprocacao;
            this.NomeArquivo = nomeArquivo;
            this.Arquivo = arquivo;
        }
    }
}