﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST
    /// Seq: OS_CHECKLIST_SEQ
    /// </summary>
    public class Os_Checklist : EntidadeBase<int>
    {
        public virtual  int IdOs { get; set; }
        public virtual  int Ida { get; set; } // 1 = Sim, 0 = Não
        public virtual  int Volta { get; set; } // 1 = Sim, 0 = Não
        public virtual  int Concluido { get; set; } // 1 = Sim, 0 = Não
        public virtual  string Usuario { get; set; } //Codigo usuario logado (matricula)
        public virtual  DateTime VersionDate { get; set; } //OLV_TIMESTAMP

        public Os_Checklist()
        {

        }

        public Os_Checklist(int idOsChecklist, int idOs, int ida, int volta, int concluido, string usuario)
        {
            this.Id = idOsChecklist;
            IdOs = idOs;
            Ida = ida;
            Volta = volta;
            Concluido = concluido;
            Usuario = usuario;
            VersionDate = DateTime.Now;
        }
    }
}
