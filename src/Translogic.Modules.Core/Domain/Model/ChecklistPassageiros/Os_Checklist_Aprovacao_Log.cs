﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_APROVACAO_LOG
    /// </summary>
    public class Os_Checklist_Aprovacao_Log : EntidadeBase<int>
    {
        public virtual Os_Checklist_Aprovacao OsChecklistAprovacao { get; set; }
        public virtual string Usuario { get; set; } //Codigo usuario logado (matricula)
        public virtual string Observacao { get; set; }
        public virtual char Aprovacao { get; set; } //IDT_APROVACAO
        public virtual char Importacao { get; set; } //IDT_IMPORTACAO
        public virtual DateTime VersionDate { get; set; } //OLV_TIMESTAMP

        public Os_Checklist_Aprovacao_Log()
        {

        }

        public Os_Checklist_Aprovacao_Log(Os_Checklist_Aprovacao osChecklistAprovacao, string usuario, string obs, char aprovacao, char importacao)
        {
            this.OsChecklistAprovacao = osChecklistAprovacao;
            this.Usuario = usuario;
            this.Observacao = obs;
            this.Aprovacao = aprovacao;
            this.Importacao = importacao;
            this.VersionDate = DateTime.Now;
        }
    }
}
