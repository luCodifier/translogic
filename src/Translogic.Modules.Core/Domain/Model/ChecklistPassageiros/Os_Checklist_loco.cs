﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_LOCO
    /// Seq: OS_CHECKLIST_LOCO_SEQ
    /// </summary>
    public class Os_Checklist_loco : EntidadeBase<int>
    {
        public virtual  Os_Checklist_Passageiro OsChecklistPassageiro { get; set; }

        public virtual  string Loco { get; set; }//LOCO VARCHAR2(5)
        public virtual  int Diesel { get; set; }//DIESEL NUMBER(10,3)
        public virtual  DateTime Vencimento { get; set; }//VENCIMENTO DATE
        public virtual  int Cativa { get; set; }//IDT_CATIVA NUMBER
        public virtual  string Obs { get; set; }//OBS VARCHAR2(2000)
        public virtual  int Ordem { get; set; }//ORDEM NUMBER
    }
}