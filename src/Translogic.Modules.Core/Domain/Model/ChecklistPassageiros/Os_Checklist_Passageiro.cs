﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_PASSAGEIRORESPOSTA1
    /// Seq: OS_CHECKLIST_PASSAGEIRO_SEQ
    /// </summary>
    public class Os_Checklist_Passageiro : EntidadeBase<int>
    {
        public virtual  Os_Checklist OsChecklist { get; set; }
        public virtual  char Status { get; set; } //STATUS CHAR(1) NOT NULL, -- P = Pendente, A = Em andamento, C = Concluído

        // CCo Circulação
        public virtual  string CcoSupervisorNome { get; set; } //CCO_SUP_NOME
        public virtual  string CcoSupervisorMatricula { get; set; } //CCO_SUP_MATR
        public virtual  string CtrSupervisorNome { get; set; }//CTR_SUP_NOME
        public virtual  string CtrSupervisorMatricula { get; set; }//CTR_SUP_MATR

        // Equipagem
        public virtual  string MaqEquipSupervisorNome { get; set; }//EQU_MAQ_NOME
        public virtual  string MaqEquipSupervisorMatricula { get; set; }//EQU_MAQ_MATR
        public virtual  string AuxEquipSupervisorNome { get; set; }//EQU_AUX_NOME
        public virtual  string AuxEquipSupervisorMatricula { get; set; }//EQU_AUX_MATR
        public virtual  string EquipSupervisorNome { get; set; }//EQU_SUP_NOME
        public virtual  string EquipSupervisorMatricula { get; set; }//EQP_SUP_MATR

        // Comentários adicionais
        public virtual string ComentAdicionais { get; set; }//COMENT_ADICIONAIS

        // Respostas CheckList
        public virtual  char Resposta1 { get; set; }//RESPOSTA1
        public virtual  char Resposta2 { get; set; }//RESPOSTA2
        public virtual  char Resposta3 { get; set; }//RESPOSTA3
        public virtual  char Resposta4 { get; set; }//RESPOSTA4
        public virtual  string Resposta5NumCruzamentos { get; set; }//RESPOSTA5_NUM NUMBER
        public virtual  string Resposta5Supervisor { get; set; }//RESPOSTA5_SUP
        public virtual  string Resposta5Matricula { get; set; }//RESPOSTA5_MATR
        public virtual  int? Resposta6TotalCruzamentos { get; set; }//RESPOSTA6
        public virtual  int? Resposta7QtdePararam { get; set; }//RESPOSTA7

        // Vagões
        public virtual  int? QtdeVagao { get; set; }// VAG_QTD NUMBER
        public virtual  decimal? Tb { get; set; }// VAG_TB NUMBER

        //Locomotivas
        //(1)
        public virtual string Locomotiva1 { get; set; }//LOCOMOTIVA_1
        public virtual decimal? Diesel1 { get; set; }//DIESEL_1
        public virtual DateTime? DataVencimento1 { get; set; }//DT_VENCIMENTO_1
        public virtual char Cativas1  { get; set; }//CATIVAS_1
        public virtual string Observacao1  { get; set; }//OBSERVACAO_1
        //(2)
        public virtual string Locomotiva2 { get; set; }//LOCOMOTIVA_2
        public virtual decimal? Diesel2 { get; set; }//DIESEL_2
        public virtual DateTime? DataVencimento2 { get; set; }//DT_VENCIMENTO_2
        public virtual char Cativas2 { get; set; }//CATIVAS_2
        public virtual string Observacao2 { get; set; }//OBSERVACAO_2
        //(3)
        public virtual string Locomotiva3 { get; set; }//LOCOMOTIVA_3
        public virtual decimal? Diesel3 { get; set; }//DIESEL_3
        public virtual DateTime? DataVencimento3 { get; set; }//DT_VENCIMENTO_3
        public virtual char Cativas3 { get; set; }//CATIVAS_3
        public virtual string Observacao3 { get; set; }//OBSERVACAO_3
        //(4)
        public virtual string Locomotiva4 { get; set; }//LOCOMOTIVA_4
        public virtual decimal? Diesel4 { get; set; }//DIESEL_4
        public virtual DateTime? DataVencimento4 { get; set; }//DT_VENCIMENTO_4
        public virtual char Cativas4 { get; set; }//CATIVAS_4
        public virtual string Observacao4 { get; set; }//OBSERVACAO_4
        public virtual int? TotalLoco { get; set; }

        // Previsão
        public virtual  DateTime? DataChegada { get; set; }//PREV_CHEGADA
        public virtual  DateTime? DataPrevisto { get; set; }//PREV_PREVISTO
        public virtual  DateTime? DataReal { get; set; }//PREV_REAL
        public virtual DateTime? DataChegadaReal { get; set; }

    }
}