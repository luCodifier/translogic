﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_PASSAGEIRO_LOG
    /// Seq: OS_CHECKLIST_PASS_LOG_SEQ
    /// </summary>
    public class Os_Checklist_Passageiro_Log : EntidadeBase<int>
    {
        public virtual  Os_Checklist_Passageiro OsChecklistPassageiro { get; set; }
        public virtual  string Usuario { get; set; } //Codigo usuario logado (matricula)
        public virtual  string Observacao { get; set; }
        public virtual DateTime VersionDate { get; set; } //OLV_TIMESTAMP

        public Os_Checklist_Passageiro_Log()
        {

        }

        public Os_Checklist_Passageiro_Log(Os_Checklist_Passageiro osChecklistPassageiro, string usuario, string observacao)
        {
            this.OsChecklistPassageiro = osChecklistPassageiro;
            this.Usuario = usuario;
            this.Observacao = observacao;
            this.VersionDate = DateTime.Now;
        }
    }
}