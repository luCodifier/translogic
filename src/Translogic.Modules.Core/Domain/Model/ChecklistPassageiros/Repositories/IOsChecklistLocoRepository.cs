﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IOsChecklistLocoRepository : IRepository<Os_Checklist_loco, int>
    {
    }
}