﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;
    using System;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using System.Collections.Generic;

    public interface IOsChecklistRepository : IRepository<Os_Checklist, int>
    {
        /// <summary>
        /// Verificar se OS é de passageiros
        /// </summary>
        /// <param name="os"></param>
        /// <returns></returns>
        bool OsDePassageiro(int os);

        /// <summary>
        /// Pesqusiar as OS combinadas com OS de Checklist de passageiros (Pelo menos um filtro é obrigatório)
        /// </summary>
        /// <param name="dataInicial">Data inicial</param>
        /// <param name="dataFInal">Data Final</param>
        /// <param name="os">Número da OS</param>
        /// <returns></returns>
        ResultadoPaginado<OsChecklistDto> BuscarOsCheklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, int os = 0);

        /// <summary>
        /// Buscar OS_CHECKLIST pelo id DA OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        Os_Checklist ObterOSCheckListPorIdOS(int idOs);

        /// <summary>
        /// Pesquisa dados trem por OS para exporatação PDF
        /// </summary>
        /// <param name="idOs">Filtro OS</param>
        /// <returns>Lista de dados para Exportação PDF</returns>
        OsChecklistExportacaoDto ObterConsultaExportacaoPDF(string idOs);
    }
}
