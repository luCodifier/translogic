﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IOsChecklistAprovacaoLogRepository : IRepository<Os_Checklist_Aprovacao_Log, int>
    {
    }
}