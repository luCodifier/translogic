﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;

    public interface IOsChecklistImportacaoRepository : IRepository<Os_Checklist_Importacao, int>
    {
        /// <summary>
        /// Obter Arquivo de importação
        /// </summary>
        /// <param name="idOsChecklistPassageiro"></param>
        /// <returns></returns>
        OsCheckListImportacaoDto ObterArquivo(int idCheckListAprovado, string quadro);

        /// <summary>
        /// Obter pelo idChecklist e quadro
        /// </summary>
        /// <param name="idCheckListAprovado"></param>
        /// <param name="quadro"></param>
        /// <returns></returns>
        Os_Checklist_Importacao ObterPorIdEArea(int idCheckListAprovado, string quadro);

        /// <summary>
        /// Obter Importação por numero da OS
        /// </summary>
        /// <param name="idCheckListAprovado"></param>
        /// <param name="quadro"></param>
        /// <returns></returns>
        IList<OsCheckListImportacaoDto> ObterImportacaoPorNumOS(int numOs);


    }
}