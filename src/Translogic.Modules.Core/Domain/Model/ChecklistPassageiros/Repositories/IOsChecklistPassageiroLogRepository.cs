﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IOsChecklistPassageiroLogRepository : IRepository<Os_Checklist_Passageiro_Log, int>
    {
    }
}