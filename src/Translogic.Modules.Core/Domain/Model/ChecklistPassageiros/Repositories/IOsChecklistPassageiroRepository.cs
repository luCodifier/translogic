﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;

    public interface IOsChecklistPassageiroRepository : IRepository<Os_Checklist_Passageiro, int>
    {
        /// <summary>
        /// Buscar OS CheckList Passageiro pelo id DO CHECKLIST
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns> Retorna OBJETO CheckList Passageiro</returns>
        Os_Checklist_Passageiro ObterOSPassageiroPorIDCheckList(int idOsCheckList);

        /// <summary>
        /// Buscar OS_CHECKLIST_PASSAGEIRO pelo id DO CHECKLIST_PASSAGEIRO
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns> Retorna OBJETO CheckList Passageiro</returns>
        Os_Checklist_Passageiro ObterOSPassageiroCheckList(int idOsCheckListPassageiro);

        /// <summary>
        /// Busca dados do trem pelo id da OS da (tabela T2)
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns>QtdeVagoes e TB </returns>
        OsDadosTremDto BuscaDadosTremPorIdOs(int idOs);

        /// <summary>
        /// Busca Locomotivas do Trem pelo id da OS da (tabela T2)
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns>Lista de Locomotivas</returns>
        IList<LocomotivaDto> BuscaLocomotivasTremPorIdOs(int idOs);
    }
}