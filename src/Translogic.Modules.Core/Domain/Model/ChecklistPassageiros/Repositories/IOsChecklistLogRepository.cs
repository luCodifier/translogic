﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IOsChecklistLogRepository : IRepository<Os_Checklist_Log, int>
    {
    }
}
