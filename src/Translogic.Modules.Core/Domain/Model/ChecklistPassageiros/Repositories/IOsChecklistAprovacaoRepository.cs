﻿
namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IOsChecklistAprovacaoRepository : IRepository<Os_Checklist_Aprovacao, int>
    {
        /// <summary>
        /// Obter aprovação(ões) de um checklist
        /// </summary>
        /// <param name="idOsChecklist">ID da tabela Checklist</param>
        /// <returns>Instância da classe</returns>
        Os_Checklist_Aprovacao ObterPorOsChecklist(int idOsChecklist);

        /// <summary>
        /// Verdadeiro ou falso se Checklist está concluído, ou seja, todas as áreas aprovadas
        /// </summary>
        /// <param name="idOsChecklist">ID da tabela Checklist</param>
        /// <returns>Verdadeiro ou falso</returns>
        bool Concluido(int idOsChecklist);
    }
}
