﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_LOG
    /// </summary>
    public class Os_Checklist_Log : EntidadeBase<int>
    {
        public virtual Os_Checklist OsChecklist { get; set; }
        public virtual string Usuario { get; set; } //Codigo usuario logado (matricula)
        public virtual string Observacao { get; set; }
        public virtual DateTime VersionDate { get; set; } //OLV_TIMESTAMP

        public Os_Checklist_Log()
        {

        }

        public Os_Checklist_Log(Os_Checklist osChecklist, string usuario, string observacao)
        {
            this.OsChecklist = osChecklist;
            this.Usuario = usuario;
            this.Observacao = observacao;
            this.VersionDate = DateTime.Now;
        }
    }
}
