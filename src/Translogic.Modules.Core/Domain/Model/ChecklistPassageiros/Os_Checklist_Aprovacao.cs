﻿namespace Translogic.Modules.Core.Domain.Model.ChecklistPassageiros
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;

    /// <summary>
    /// OS_CHECKLIST_APROVACAO
    /// Seq: OS_CHECKLIST_APROVACAO_SEQ
    /// </summary>
    public class Os_Checklist_Aprovacao : EntidadeBase<int>
    {
        public virtual  Os_Checklist OsChecklist { get; set; }

        //// Propriedades de aprovações da área VIA
        public virtual string ViaUsuarioMatricula { get; set; }//VIA_USR_MATR
        public virtual string ViaUsuarioNome { get; set; } //VIA_USR_NOME
        public virtual DateTime? ViaDataAprovacao { get; set; } //VIA_DT_APR 
        public virtual char ViaStatus { get; set; } //VIA_IDT_STATUS  -- A = APROVADO, R - REPROVADO
        public virtual char ViaImportacao { get; set; } //VIA_IDT_IMPORTACAO -- I = IMPORTAÇÃO, U -USUÁRIO (APROVAÇÕES)
        public virtual string ViaObs { get; set; } //VIA_OBS 

        //// Propriedades de aprovações da área Locoomitiva
        public virtual string LocoUsuarioMatricula { get; set; }//LOCO_USR_MATR
        public virtual string LocoUsuarioNome { get; set; } //LOCO_USR_NOME
        public virtual DateTime? LocoDataAprovacao { get; set; } //LOCO_DT_APR 
        public virtual char LocoStatus { get; set; } //LOCO_IDT_STATUS 
        public virtual char LocoImportacao { get; set; } //LOCO_IDT_IMPORTACAO 
        public virtual string LocoObs { get; set; } //LOCO_OBS 

        //// Propriedades de aprovações da área Tração
        public virtual string TracUsuarioMatricula { get; set; }//TRAC_USR_MATR
        public virtual string TracUsuarioNome { get; set; } //TRAC_USR_NOME
        public virtual DateTime? TracDataAprovacao { get; set; } //TRAC_DT_APR 
        public virtual char TracStatus { get; set; } //TRAC_IDT_STATUS 
        public virtual char TracImportacao { get; set; } //TRAC_IDT_IMPORTACAO 
        public virtual string TracObs { get; set; } //TRAC_OBS 


        //// Propriedades de aprovações da área Vagão
        public virtual string VagUsuarioMatricula { get; set; }//VAG_USR_MATR
        public virtual string VagUsuarioNome { get; set; } //VAG_USR_NOME
        public virtual DateTime? VagDataAprovacao { get; set; } //VAG_DT_APR 
        public virtual char VagStatus { get; set; } //VAG_IDT_STATUS 
        public virtual char VagImportacao { get; set; } //VAG_IDT_IMPORTACAO 
        public virtual string VagObs { get; set; } //VAG_OBS 
    }
}