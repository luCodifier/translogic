﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class TfaExportaDto
    {
        [Display(Name = "DESPACHO", Description="DESPACHO")]
        public decimal? Despacho { get; set; }//NRO_DESP
        [Display(Name = "SÉRIE")]
        public string SerieDespacho { get; set; }//SERIE_DESP
        [Display(Name = "OS")]
        public decimal? OS { get; set; }// PC_OS
        [Display(Name = "NUM PROCESSO")]
        public decimal? NumProcesso { get; set; }// PC_NUM_PROC
        [Display(Name = "NUM TERMO")]
        public string NumTermo { get; set; }//MO_NUM_FALTA
        [Display(Name = "STATUS")]
        public string Status { get; set; } //situação - verificar
        [Display(Name = "VAGÃO")]
        public string Vagao { get; set; }//VG_COD_VAG
        [Display(Name = "SÉRIE VAGÃO")]
        public string SerieVagao { get; set; }//SV_COD_SER    
        [Display(Name = "NF")]
        public string NF { get; set; }//MO_NOTA_FISCAL
        [Display(Name = "FLUXO")]
        public string Fluxo { get; set; }//FX_COD_FLX
        [Display(Name = "UN")]
        public string UN { get; set; }//UN_COD_UNG
        [Display(Name = "MALHA")]
        public string Malha { get; set; }//UP_IND_MALHA
        [Display(Name = "BITOLA")]
        public string Bitola { get; set; }//TB_COD_BITOLA
        [Display(Name = "EST. ORIGEM")]
        public string EstacaoOrigem { get; set; }//AO_COD_AOP
        [Display(Name = "EST. DESTINO")]
        public string EstacaoDestino { get; set; }//AO_COD_AOP
        [Display(Name = "CAUSA")]
        public string Causa { get; set; }//CS_DSC_CAUSA
        [Display(Name = "COD. CLIENTE")]
        public string CodCliente { get; set; }//CL_COD_SAP
        [Display(Name = "CLIENTE")]
        public string Cliente { get; set; }//CL_DSC_CLS
        [Display(Name = "COD. MERCADORIA")]
        public string CodMercadoria { get; set; }//MC_COD_MRC
        [Display(Name = "MERCADORIA")]
        public string Mercadoria { get; set; }//MC_DDT_PT
        [Display(Name = "DATA SINISTRO")]
        public string DtSinistro { get; set; } //PC_DT_SINISTRO
        [Display(Name = "DATA VISTORIA")]
        public string DtVistoria { get; set; }//PC_DT_VISTORIA
        [Display(Name = "DATA PROCESSO")]
        public string DtProcesso { get; set; }//PC_DT_PROC
        [Display(Name = "DATA PGTO CLIENTE")]
        public string DtPgtoCliente { get; set; }//PC_DT_PAGAMENTO
        [Display(Name = "UNIDADE")]
        public string Unidade { get; set; }//UN_COD_UNG
        [Display(Name = "PERCENTUAL")]
        public decimal? Percentual { get; set; }//RS_PERC_RATEIO
        [Display(Name = "PESO ORIGEM")]
        public decimal? PesoOrigem { get; set; }//DC_QTD_INF
        [Display(Name = "PERDA (TON) VAGÃO")]
        public decimal? PerdaTonVagao { get; set; }//MO_QTD_PERDA
        [Display(Name = "TOLERÂNCIA")]
        public decimal? Tolerancia { get; set; }//TL_TOLERANCIA
        [Display(Name = "CONSIDERAR TOLERÂNCIA")]
        public string ConsiderarTolerancia { get; set; }//CONSID_TOLERANCIA
        [Display(Name = "PERDA À PAGAR")]
        public decimal? PerdaPagar { get; set; }//calculado FALTANDO*******************
        [Display(Name = "VALOR MERCADORIA UNITÁRIO")]
        public decimal? ValorMercadoriaUnitario { get; set; }//MO_VLR_MERC
        [Display(Name = "VALOR À PAGAR PRODUTO")]
        public decimal? ValorPagarProduto { get; set; }//calculado FALTANDO*******************
        [Display(Name = "TARIFA DE FRETE")]
        public decimal? TarifaFrete { get; set; }//TARIFATOTAL
        [Display(Name = "VALOR À PAGAR DE FRETE")]
        public decimal? ValorPagarFrete { get; set; }//calculado FALTANDO*******************
        [Display(Name = "PISCOFIN")]
        public decimal? PisCofins { get; set; }//PISCOFINS
        [Display(Name = "ICMS")]
        public decimal? ICMS { get; set; }//ICMS
        [Display(Name = "IMPOSTO")]
        public decimal? Imposto { get; set; }//calculado FALTANDO*******************
        [Display(Name = "DESCONTO")]
        public decimal? Desconto { get; set; }//DESCONTO
        [Display(Name = "MOTIVO")]
        public string Motivo { get; set; }//MOTIVO
        [Display(Name = "TOTAL VAGÃO")]
        public decimal? TotalVagao { get; set; }//calculado
        [Display(Name = "FRANQUIA")]
        public decimal? Franquia { get; set; }//FM_VLR_FRANQUIA
        [Display(Name = "GRUPO DE CAUSA")]
        public string GrupoCausa { get; set; }//CS_DSC_CAUSA
        [Display(Name = "MÊS SINISTRO")]
        public string MesSinistro { get; set; }//PC_DT_CADASTRO
        [Display(Name = "SEGURO?")]
        public string Seguro { get; set; }//PC_IND_SEGURADO
        [Display(Name = "REGULADORA")]
        public decimal? Reguladora { get; set; } //SS_REGULADORA - iNCLUIR NA QRY
        [Display(Name = "DATA RESSARCIMENTO SEGURADORA")]
        public string DtRessarcimentoSeguradora { get; set; }
        [Display(Name = "VALOR RESSARCIMENTO SEGURADORA")]
        public decimal? ValorRessarcimentoSeguradora { get; set; }
        [Display(Name = "DATA CRÉDITO SALVADOS REGULADORA")]
        public string DtCreditoSalvadosReguladora { get; set; }
        [Display(Name = "VALOR CRÉDITO SALVADOS REGULADORA")]
        public decimal? ValorCreditoSalvadosReguladora { get; set; }
        [Display(Name = "DATA CRÉDITO SALVADOS UNIDADE")]
        public string DtCreditoSalvadosUnidade { get; set; }
        [Display(Name = "VALOR CRÉDITO SALVADOS UNIDADE")]
        public decimal? ValorCreditoSalvadosUnidade { get; set; }
        [Display(Name = "(TON) SALVADO")]
        public decimal? TonSalvado { get; set; } //SS_QTD_SVD
        [Display(Name = "CUSTO REGULAÇÃO")]
        public decimal? CustoRegulacao { get; set; }//SS_CUSTO_REG
        [Display(Name = "GAMBITAGEM")]
        public string Gambitagem { get; set; }//MO_IND_GAMB
        [Display(Name = "DATA ANALISE")]
        public string DtAnalise { get; set; }//AS_DT_ANALISE
        [Display(Name = "STATUS PAGAMENTO")]
        public string StatusPagamento { get; set; }//UD_DSC_UNID
        [Display(Name = "TEMPO ANALISE")]
        public decimal? TempoAnalise { get; set; }//AS_DT_ANALISE - PC_DT_SINISTRO FALTANDO*******************
        [Display(Name = "CLASSE ANALISE")]
        public string ClasseAnalise { get; set; } //função FALTANDO*******************
        [Display(Name = "VISTORIADOR")]
        public string Vistoriador { get; set; }//PC_VISTORIADOR
        [Display(Name = "TERMINAL ORIGEM")]
        public string TerminalOrigem { get; set; }//AO_ID_EST_OR
        [Display(Name = "TERMINAL DESTINO")]
        public string TerminalDestino { get; set; }//AO_ID_EST_OR
        [Display(Name = "TFA ANEXADO")]
        public string TfaAnexado { get; set; }
        [Display(Name = "SINDICÂNCIA")]
        public decimal? Sindicancia { get; set; }// SINDICANCIA
        [Display(Name = "CIENTE")]
        public string Ciente { get; set; }// CIENTE
        [Display(Name = "Nº CTE")]
        public string NumeroCte { get; set; }// NUMERO_CTE
        [Display(Name = "DATA CTe")]
        public string DataCte { get; set; }// DATA_CTE
        [Display(Name = "EMPRESA ORIGEM")]
        public string EmpresaOrigem { get; set; }// EMPRESA_ORIG
        [Display(Name = "EMPRESA DESTINO")]
        public string EmpresaDestino { get; set; }// EMPRESA_DEST
        [Display(Name = "Nº BALANÇA ORIGEM")]
        public string NumeroBalanca { get; set; }// NUM_BALANCA       
    }
}