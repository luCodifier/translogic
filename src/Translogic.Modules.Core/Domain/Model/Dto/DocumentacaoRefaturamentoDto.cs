﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class DocumentacaoRefaturamentoDto
    {
        public decimal OsId { get; set; }
        public decimal NumeroOs { get; set; }
        public string PrefixoTrem { get; set; }
        public decimal ComposicaoId { get; set; }
        public string CodigoVagao { get; set; }
        public decimal RecebedorId { get; set; } 
        public string Recebedor { get; set; }
        public decimal? MotivoId { get; set; }
    }
}