﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class ProcessamentoFaturamentoAutomaticoDto
    {
        public decimal Id { get; set; }
        public string Serie { get; set; }
        public string Vagao { get; set; }
        public decimal Fluxo { get; set; }
        public string Md { get; set; }
        public decimal StatusProcessamento { get; set; }
        public decimal? NumeroTentativas { get; set; }
        public DateTime? ProximaExecucao { get; set; }
        public string Observacao { get; set; }
        public DateTime Data { get; set; }
        public string Cliente { get; set; }
        public string Aprovado1380 { get; set; }
        public string Local { get; set; }
        public string LocalAtual { get; set; }
        public string Situacao { get; set; }
        public string Lotacao { get; set; }
        public string Localizacao { get; set; }
        public string CondicaoUso { get; set; }
        public decimal Volume { get; set; }
        public decimal PesoDespacho { get; set; }
        public decimal PesoTotalVagao { get; set; }
        public string PermiteManual { get; set; }
    }
}