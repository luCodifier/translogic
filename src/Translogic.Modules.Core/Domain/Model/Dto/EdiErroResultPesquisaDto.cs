﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class EdiErroResultPesquisaDto
    {
        public decimal IdMensagemRecebimento { get; set; }
        public decimal IdErroEdiNfe { get; set; }
        public string Agrupador { get; set; }
        public string Vagao { get; set; }
        public string Fluxo { get; set; }
        public DateTime DataErro { get; set; }
        public DateTime? DataEnvio { get; set; }
        public string ResponsavelEnvio { get; set; }
        public string ChaveNfe { get; set; }
        public DateTime? DataRegularizacao { get; set; }
        public string Reprocessado { get; set; }

        public bool Regularizado
        {
            get { return ((DataRegularizacao.HasValue) && (DataRegularizacao.Value != null)); }
        }

        public bool FoiReprocessado
        {
            get { return ((Reprocessado != null) && (Reprocessado.ToUpper() == "S")); }
        }
    }
}