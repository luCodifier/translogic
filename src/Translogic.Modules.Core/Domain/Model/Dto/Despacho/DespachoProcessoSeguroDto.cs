﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.Despacho
{
    /// <summary>
    /// Classe Dto para manipulação e consulta de dados na Geração de Processos de Seguro
    /// </summary>
    public class DespachoProcessoSeguroDto
    {
        /// <summary>
        /// ID do Despacho 
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Número do Despacho
        /// </summary>
        public int? NumeroDespacho { get; set; }

        /// <summary>
        /// Serie do Despacho
        /// </summary>
        public string Serie { get; set; }

    }
}