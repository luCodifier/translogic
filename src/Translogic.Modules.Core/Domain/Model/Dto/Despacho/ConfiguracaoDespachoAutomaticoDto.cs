﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.Despacho
{
    public class ConfiguracaoDespachoAutomaticoDto
    {
        public decimal Id { get; set; }
        public string Cliente { get; set; }
        public string AreaOperacional { get; set; }
        public string Localizacao { get; set; }
        public string Segmento { get; set; }
        public string Terminal { get; set; }
        public string Patio { get; set; }
        public string Manual { get; set; }
        public DateTime Data { get; set; }
    }
}