﻿namespace Translogic.Modules.Core.Domain.Model.Dto.Despacho
{
    using System;

    public class ConfiguracaoDespachoAutomaticoFiltroDto
    {
        public string Id { get; set; }
        public string Cliente { get; set; }
        public string LocalizacaoId { get; set; }
        public string AreaOperacional { get; set; }
        public string Segmento { get; set; }
        public string Terminal { get; set; }
        public string Patio { get; set; }
        public string Manual { get; set; }
        public string DtInicial { get; set; }
        public string DtFim { get; set; }


    }
}