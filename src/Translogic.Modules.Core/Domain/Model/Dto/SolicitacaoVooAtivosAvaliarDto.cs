﻿
using System;
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SolicitacaoVooAtivosAvaliarDto
    {
        public decimal Id { get; set; }
        public DateTime dtSolicitacao { get; set; }
        public string TipoAtivo  { get; set; }
        public string Solicitante { get; set; }
        public string AreaSolicitante { get; set; }
        public string Motivo { get; set; }
        public string Status { get; set; }

    }
}