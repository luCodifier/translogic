﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RelatorioFichaRecomendacaoExportDto
    {
        /// <summary>
        /// SEQ
        /// </summary>
        public decimal SEQ { get; set; }
        /// <summary>
        /// ID TREM
        /// </summary>
        public decimal IdTrem { get; set; }
        /// <summary>
        /// TREM
        /// </summary>
        public string Trem { get; set; }
        /// <summary>
        /// OS
        /// </summary>
        public decimal OS { get; set; }
        /// <summary>
        /// VAGAO
        /// </summary>
        public string Vagao { get; set; }
        /// <summary>
        /// RECOMENDACAO
        /// </summary>
        public string Recomendacao { get; set; }
        /// <summary>
        /// AVARIA FREIO
        /// </summary>
        public string AvariaFreio { get; set; }
        /// <summary>
        /// ORIGEM
        /// </summary>
        public string Origem { get; set; }
        /// <summary>
        /// DESTINO
        /// </summary>
        public string Destino { get; set; }
        /// <summary>
        /// MOTIVO RECOMENDACAO
        /// </summary>
        public string MotivoRecomendacao { get; set; }
        /// <summary>
        /// FREIO SITUACAO
        /// </summary>
        public string FreioSituacao { get; set; }

    }
}
