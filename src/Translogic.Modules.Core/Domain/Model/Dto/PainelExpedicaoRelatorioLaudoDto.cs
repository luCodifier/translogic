﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Enums;

    public class PainelExpedicaoRelatorioLaudoDto
    {
        public decimal Id { get; set; }
        public string Vagao { get; set; }
        public string NumeroNfe { get; set; }
        public string RemetenteNfe { get; set; }
        public DateTime DataCarregamento { get; set; }
        public string FluxoComercial { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string FluxoSegmento { get; set; }
        public string NumeroLaudo { get; set; }
        public string NumeroOsLaudo { get; set; }
        public DateTime DataRecebimentoLaudo { get; set; }
        public string LaudoTipo { get; set; }
        public string Empresa { get; set; }
        public string CnpjEmpresa { get; set; }
        public string CnpjEmpresaFormatado
        {
            get
            {
                var cnpj = CnpjEmpresa;
                if (string.IsNullOrEmpty(cnpj))
                {
                    return "00.000.000/0000-00";
                }

                var cnpjFormatado = cnpj.Replace(".", string.Empty).Replace("/", string.Empty).Replace("-", string.Empty).PadLeft(14, '0');
                cnpjFormatado = string.Format("{0}.{1}.{2}/{3}-{4}", cnpjFormatado.Substring(0, 2),
                                              cnpjFormatado.Substring(2, 3), cnpjFormatado.Substring(5, 3),
                                              cnpjFormatado.Substring(8, 4), cnpjFormatado.Substring(12, 2));

                return cnpjFormatado;
            }
        }

        public string Classificador { get; set; }
        public string CpfClassificador { get; set; }
        public string CpfClassificadorFormatado
        {
            get
            {
                var cpf = CpfClassificador;
                if (string.IsNullOrEmpty(cpf))
                {
                    return "000.000.000-00";
                }

                var cpfFormatado = cpf.Replace(".", string.Empty).Replace("-", string.Empty).PadLeft(11, '0');
                cpfFormatado = string.Format("{0}.{1}.{2}-{3}", cpfFormatado.Substring(0, 3),
                                             cpfFormatado.Substring(3, 3), cpfFormatado.Substring(6, 3),
                                             cpfFormatado.Substring(9, 2));

                return cpfFormatado;
            }
        }

        public string Produto { get; set; }
        public string CodigoClassificacao { get; set; }
        public LaudoMercadoriaTipoClassificacaoEnum CodigoClassificacaoEnum
        {
            get
            {
                var classificacaoEnum = LaudoMercadoriaTipoClassificacaoEnum.Indefinido;
                if (!String.IsNullOrEmpty(CodigoClassificacao))
                {
                    int codigo = 0;
                    if (Int32.TryParse(CodigoClassificacao, out codigo) && codigo > 0 && codigo < 15)
                    {
                        classificacaoEnum = (LaudoMercadoriaTipoClassificacaoEnum)codigo;
                    }
                }

                return classificacaoEnum;
            }
        }
        public string Classificacao { get; set; }
        public decimal Porcentagem { get; set; }

        public decimal? ClassificacaoUmidade { get; set; }
        public decimal? ClassificacaoMateriaEstranhaImpura { get; set; }
        public decimal? ClassificacaoTotalAvariados { get; set; }
        public decimal? ClassificacaoDanificado { get; set; }
        public decimal? ClassificacaoQuebrado { get; set; }
        public decimal? ClassificacaoArdido { get; set; }
        public decimal? ClassificacaoPh { get; set; }
        public decimal? ClassificacaoEsverdeado { get; set; }
        public decimal? ClassificacaoQueimado { get; set; }
        public decimal? ClassificacaoPicado { get; set; }
        public decimal? ClassificacaoMofado { get; set; }
        public decimal? ClassificacaoFallingNumber { get; set; }
        public decimal? ClassificacaoGerminado { get; set; }
        public decimal? ClassificacaoDanificadoInsetos { get; set; }
    }
}