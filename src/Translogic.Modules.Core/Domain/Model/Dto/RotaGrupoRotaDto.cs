﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RotaGrupoRotaDto
    {
        public decimal IdRota { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set; }
    }
}