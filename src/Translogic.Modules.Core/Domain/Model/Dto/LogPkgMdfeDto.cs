﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    /// <summary>
    ///     Classe de DTO para Log da Pkg de Mdfe
    /// </summary>
    public class LogPkgMdfeDto
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets DataHora
        /// </summary>
        public DateTime DataHora { get; set; }

        /// <summary>
        ///     Gets or sets Descricao
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        ///     Gets or sets Id
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        ///     Gets or sets NomeServico
        /// </summary>
        public string NomeServico { get; set; }

        /// <summary>
        ///     Gets or sets TipoLog
        /// </summary>
        public string TipoLog { get; set; }

        /// <summary>
        ///     Gets or sets MovimentacaoTrem
        /// </summary>
        public decimal MovimentacaoTrem { get; set; }

        /// <summary>
        ///     Gets or sets Trem
        /// </summary>
        public decimal Trem { get; set; }

        /// <summary>
        ///     Gets or sets AreaOperacional
        /// </summary>
        public decimal AreaOperacional { get; set; }

        /// <summary>
        ///     Gets or sets Composicao
        /// </summary>
        public decimal Composicao { get; set; }

        #endregion
    }
}