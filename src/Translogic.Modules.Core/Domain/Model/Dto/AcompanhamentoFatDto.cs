namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;

	/// <summary>
	/// Classe de dto de acompanhamento
	/// </summary>
    public class AcompanhamentoFatDto
	{
        /// <summary>
        /// Descricao Estado
        /// </summary>
		public string Estado { get; set; }

        /// <summary>
        /// Descricao Data
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Descricao Empresa
        /// </summary>
        public string Empresa { get; set; }

        /// <summary>
        /// Descricao Correntista
        /// </summary>
        public string Correntista { get; set; }

        /// <summary>
        /// Descricao Origem
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Descricao Destino
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Descricao Mercadoria
        /// </summary>
        public string Mercadoria { get; set; }

        /// <summary>
        /// Descricao Faturamento
        /// </summary>
        public string Faturamento { get; set; }

        /// <summary>
        /// Descricao Sistema
        /// </summary>
        public string Sistema { get; set; }

        /// <summary>
        /// Descricao Total de Notas fiscais
        /// </summary>
	    public decimal TotalNfe { get; set; }

        /// <summary>
        /// Descricao total com xml
        /// </summary>
	    public decimal ComXml { get; set; }

        /// <summary>
        /// Descricao total sem xml
        /// </summary>
        public decimal SemXml { get; set; }
	}
}