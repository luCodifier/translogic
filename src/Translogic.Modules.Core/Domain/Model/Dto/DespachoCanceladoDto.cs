﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class DespachoCanceladoDto
    {
        #region Properties

        /// <summary>
        ///     Id do Despacho
        /// </summary>
        public decimal IdDespachoCancelado { get; set; }

        /// <summary>
        ///     Ids dos despachos concatenados por - (hífen)
        /// </summary>
        public string IdProximoDespacho { get; set; }

        #endregion
    }
}