﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class StatusProcessamentoDto
    {
        public decimal Id { get; set; }
        public string Descricao { get; set; }
    }
}