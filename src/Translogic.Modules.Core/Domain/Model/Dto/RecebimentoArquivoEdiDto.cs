﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class RecebimentoArquivoEdiDto
    {
        public int VagaoId { get; set; }
        public string Cliente363 { get; set; }
        public string Conteiner { get; set; }
        public string Destino { get; set; }
        public DateTime DataHoraCadastro { get; set; }
        public string Expedidor { get; set; }
        public string Fluxo { get; set; }
        public string Mercadoria { get; set; }
        public string MultiploDespacho { get; set; }
        public string NotasFicais { get; set; }
        public string Observacao { get; set; }
        public string Origem { get; set; }
        public decimal PesoReal { get; set; }
        public decimal PesoTotal { get; set; }
        public string Recebedor { get; set; }
        public string Serie { get; set; }
        public string Situacao { get; set; }
        public string Vagao { get; set; }
        public decimal VolumeTotal { get; set; }
    }
}