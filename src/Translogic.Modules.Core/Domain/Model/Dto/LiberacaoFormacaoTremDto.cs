﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class LiberacaoFormacaoTremDto
    {
        public decimal IdSolLibTrava { get; set; }              //ID_SOL_LIB_TRAVA 
        public decimal IdTrem { get; set; }                     //ID_TREM 
        public decimal IdComposicao { get; set; }               //ID_COMPOSICAO 
        public decimal OrdemServico { get; set; }               //CD_ORDEM_SERVICO 
        public string PrefixoTrem { get; set; }                 //CD_PREFIXO_TREM 
        public string OrigemTrem { get; set; }                  //CD_ORIGEM_TREM 
        public string DestinoTrem { get; set; }                 //CD_DESTINO_TREM 
        public string LocalAtual { get; set; }                  //CD_LOCAL_ATUAL 
        public DateTime DtPartida { get; set; }                 //DT_PARTIDA 
        public string TipoTrava { get; set; }                   //DS_TIPO_TRAVA 
        public string MotivoTrava { get; set; }                 //DS_MOTIVO_TRAVA 
        public DateTime DtSolicitacao { get; set; }             //DT_SOLICITACAO 
        public string JustSol { get; set; }                     //DS_JUST_SOL 
        public string UsuSol { get; set; }                      //CD_USU_SOL 
        public string NomeSol { get; set; }                     //DS_NOME_SOL 
        public string TelFixo { get; set; }                     //DS_TEL_FIXO 
        public string TelMovel { get; set; }                    //DS_TEL_MOVEL 
        public DateTime DtResposta { get; set; }                //DT_RESPOSTA 
        public string NomeAutorizador { get; set; }             //DS_NOME_AUTORIZADOR 
        public string SituacaoTrava { get; set; }               //ST_TRAVA 

        public string OrigemDestinoTrem { get; set; }           //CD_ORIGEM_TREM / CD_DESTINO_TREM
        public DateTime DtPeriodoInicial { get; set; }                
        public DateTime DtPeriodoFinal { get; set; }                
    }
}