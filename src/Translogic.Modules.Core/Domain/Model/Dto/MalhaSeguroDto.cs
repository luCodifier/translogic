﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MalhaSeguroDto
    {
        public decimal IdMalha { get; set; }        
        public string Sigla { get; set; }        
        public string DescricaoMalha { get; set; }
    }
}