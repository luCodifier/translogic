﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.Baldeio
{
    public class BaldeioDto
    {
        public decimal IdBaldeio { get; set; }
        public decimal IdArquivoBaldeio { get; set; }
        public string Local { get; set; }
        public DateTime DataBaldeio { get; set; }
        public string VagaoCedente { get; set; }
        public string VagaoRecebedor { get; set; }
        public decimal QuantidadeBaldeada { get; set; }
        public decimal QuantidadeVagaoCedente { get; set; }
        public decimal Despacho { get; set; }
        public DateTime DataDespacho { get; set; }
        public string TemAnexo { get; set; }
    }
}