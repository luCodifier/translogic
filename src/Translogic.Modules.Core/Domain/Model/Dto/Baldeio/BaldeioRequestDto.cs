﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.Baldeio
{
    public class BaldeioRequestDto
    {
        public string Local { get; set; }
        public DateTime? dtInicial { get; set; }
        public DateTime? dtFinal { get; set; }
        public string VagaoCedente { get; set; }
        public string VagaoRecebedor { get; set; }        
    }
}