﻿namespace Translogic.Modules.Core.Domain.Model.Dto.Baldeio
{
    using System;

    public class CartaBaldeioDto
    {
        public decimal IdArquivoBaldeio { get; set; }
        public virtual byte[] ArquivoPdf { get; set; }
        public virtual string NomeArquivo { get; set; }
        public virtual DateTime DataAtualizacao { get; set; }
        public virtual string Usuario { get; set; }
        public virtual decimal IdBaldeio { get; set; }
    }
}