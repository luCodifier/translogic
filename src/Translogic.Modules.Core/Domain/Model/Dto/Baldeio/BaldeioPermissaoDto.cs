﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.Baldeio
{
    public class BaldeioPermissaoDto
    {
        public bool Pesquisar { get; set; }
        public bool Importar { get; set; }
        public bool ExcluirCarta { get; set; }
    }
}