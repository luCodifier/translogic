﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class PainelExpedicaoConsultarDocumentosDto
    {
        public decimal Id { get; set; }
        public string Vagao { get; set; }
        public DateTime DataCarga { get; set; }
        public string Fluxo { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string Cliente { get; set; }
        public string Expedidor { get; set; }
        public string Recebedor { get; set; }
        public string RemetenteFiscal { get; set; }

        public string CodigoMercadoria { get; set; }
        public string DescricaoMercadoria { get; set; }
        public string Mercadoria { get; set; }

        public decimal? Os { get; set; }
        public string Prefixo { get; set; }
        public decimal Seq { get; set; }

        public decimal IdDespacho { get; set; }
        public decimal IdDespachoItem { get; set; }
        public decimal IdCte { get; set; }
        public decimal IdMdfe { get; set; }

        public string Segmento { get; set; }

        public string PossuiCte { get; set; }
        public string PossuiTicket { get; set; }
        public string PossuiNfe { get; set; }
        public string PossuiWebDanfe { get; set; }
        public decimal QuantidadeNfeRecebidas { get; set; }
        public decimal TotalNfe { get; set; }
        public string PossuiXmlNfe { get; set; }
        public string PossuiDcl
        {
            get { return (IdDespacho > 0 ? "S" : "N"); }
        }

        public string PossuiLaudoMercadoria { get; set; }
        public string OutrasFerrovias { get; set; }

        public string PossuiMdfe { get; set; }

        public string NumeroCte { get; set; }
        public string UsuarioEventoCarregamento { get; set; }

        public string CnpjRecebedor { get; set; }
        public string MalhaOrigem { get; set; }

        public virtual string UsuarioMatricula
        {
            get
            {
                string matricula = "";
                if ((!String.IsNullOrEmpty(UsuarioEventoCarregamento)))
                {
                    if (UsuarioEventoCarregamento.IndexOf("-") >= 0)
                        matricula = UsuarioEventoCarregamento.Split('-')[0];
                }

                return matricula;
            }
        }

        public virtual string UsuarioNome
        {
            get
            {
                string nome = "";
                if ((!String.IsNullOrEmpty(UsuarioEventoCarregamento)))
                {
                    if (UsuarioEventoCarregamento.IndexOf("-") >= 0)
                        nome = UsuarioEventoCarregamento.Split('-')[1];

                }

                return nome;
            }
        }
    }
}