﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class UnidadeSeguroDto
    {
        public int IdUnidadeSegura { get; set; }
        public string TipoUnidade { get; set; }
        public string Descricao { get; set; }
    }
}