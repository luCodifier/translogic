﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System.Xml;
    using FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;

    /// <summary>
	/// Dto de processamento da nfe
	/// </summary>
	public class ProcessamentoNfeDto
	{
        /// <summary>
        /// Assinatura Padrao Simconsultas
        /// </summary>
        /// <param name="listaDeVolumes">lista De Volumes</param>
        /// <param name="namespaceManager">namespace Manager</param>
        /// <param name="nfe">nfe Simconsultas</param>
        /// <param name="nfeConfiguracaoEmpresa">Configuração da empresa emitente</param>
        /// <param name="peso_bruto">peso_bruto da StatusNFe</param>
        public delegate void AssinaturaPadraoSimconultas(XmlNodeList listaDeVolumes, XmlNamespaceManager namespaceManager, ref NotaFiscalEletronica nfe, NfeConfiguracaoEmpresa nfeConfiguracaoEmpresa, ref double peso_bruto);
		
        /// <summary>
        /// Priorizar Volume
		/// </summary>
        public bool PriorizarVolume { get; set; }

        /// <summary>
        /// Priorizar Produtos
        /// </summary>
        public bool PriorizarProdutos { get; set; }

        /// <summary>
        /// Funcao Simconsultas
        /// </summary>
        public AssinaturaPadraoSimconultas FuncaoSimconsultas { get; set; }
	}
}