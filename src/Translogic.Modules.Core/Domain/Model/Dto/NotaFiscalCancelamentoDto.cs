﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class NotaFiscalCancelamentoDto
    {
        public double IdNotaFiscal { get; set; }

        public string ChaveNotaFiscal { get; set; }
    }
}