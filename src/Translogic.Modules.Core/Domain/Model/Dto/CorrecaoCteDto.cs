﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Classe de DTO para Cte
    /// </summary>
    public class CorrecaoCteDto
    {
        /// <summary>
        /// Gets or sets CteId
        /// </summary>
        public int CteId { get; set; }

        /// <summary>
        /// Id do despacho
        /// </summary>
        public int IdDespacho { get; set; }

        /// <summary>
        /// Gets or sets Fluxo
        /// </summary>
        public string CodFluxo { get; set; }

        /// <summary>
        /// Gets or sets do Vagao
        /// </summary>
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets Conteiner
        /// </summary>
        public string Conteiner { get; set; }

        /// <summary>
        /// Gets or sets do Observacao
        /// </summary>
        public string Observacao { get; set; }

        /// <summary>
        /// Gets or sets do CorrecaoCte
        /// </summary>
        public bool CorrecaoCte { get; set; }

        /// <summary>
        /// Gets or sets do CorrecaoConteinerCte
        /// </summary>
        public bool CorrecaoConteinerCte { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp5
        /// </summary>
        public string SerieDesp5 { get; set; }

        /// <summary>
        /// Gets or sets NumDesp5
        /// </summary>
        public int NumDesp5 { get; set; }

        /// <summary>
        /// Gets or sets CodigoControle
        /// </summary>
        public string CodigoControle { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp6
        /// </summary>
        public int SerieDesp6 { get; set; }

        /// <summary>
        /// Gets or sets ToneladaUtil
        /// </summary>
        public double ToneladaUtil { get; set; }

        /// <summary>
        /// Gets or sets Volume
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Gets or sets NroCte
        /// </summary>
        public string NroCte { get; set; }

        /// <summary>
        /// Gets or sets SerieCte
        /// </summary>
        public string SerieCte { get; set; }

        /// <summary>
        /// Gets or sets ChaveCte
        /// </summary>
        public string ChaveCte { get; set; }

        /// <summary>
        /// Gets or sets DataEmissao
        /// </summary>
        public DateTime DataEmissao { get; set; }

        /// <summary>
        /// Gets or sets NumDesp6
        /// </summary>
        public int NumDesp6 { get; set; }

        /// <summary>
        /// Gets or sets CorrecaoConteiner
        /// </summary>
        public IList<CorrecaoConteinerDto> CorrecaoConteiner { get; set; }
    }

    /// <summary>
    /// Classe de DTO para Correcao de Conteiner
    /// </summary>
    public class CorrecaoConteinerDto
    {
        /// <summary>
        /// Gets or sets ConteinerOriginal
        /// </summary>
        public string ConteinerNfe { get; set; }

        /// <summary>
        /// Gets or sets ConteinerCorrigido
        /// </summary>
        public string ConteinerCorrigido { get; set; }

        /// <summary>
        ///  Gets or sets Chave Nota Fiscal Eletronica
        /// </summary>
        public string ChaveNfe { get; set; }
    }
}