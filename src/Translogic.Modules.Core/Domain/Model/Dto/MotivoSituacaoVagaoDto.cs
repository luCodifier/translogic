﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MotivoSituacaoVagaoDto {

        public string NumeroVagao { get; set; }
        public decimal IdVagao { get; set; }
        public string Local { get; set; }
        public string Serie { get; set; }
        public string Linha { get; set; }
        public decimal Sequencia { get; set; }
        public decimal IdLotacao { get; set; }
        public string DescricaoLotacao { get; set; }
        public decimal IdSituacao { get; set; }
        public string DescricaoSituacao { get; set; }
        public decimal IdCondicaoDeUso { get; set; }
        public string DescricaoCondicaoDeUso { get; set; }
        public decimal IdLocalizacao { get; set; }
        public string DescricaoLocalizacao { get; set; }
        public decimal IdVagaoMotivo { get; set; }
        public decimal IdMotivo { get; set; }
        public string DescricaoMotivo { get; set; }
        public DateTime? DataEmissao { get; set; }
        public string NomeUsuario { get; set; }
    }
}