﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSStatusDto
    {
        public decimal IdStatus { get; set; }
        public string DescricaoStatus { get; set; }
    }
}