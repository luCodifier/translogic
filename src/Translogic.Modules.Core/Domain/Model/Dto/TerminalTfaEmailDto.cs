﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class TerminalTfaEmailDto
    {
        public decimal IdTerminalEmail { get; set; }
        public decimal IdTerminal { get; set; }
        public string Email { get; set; }
    }
}