﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;
	using Diversos.Cte;

    /// <summary>
    /// Classe de DTO para Nota Cte
	/// </summary>
    public class NotaCteDto
	{
        /// <summary>
        /// Gets or sets CteId
        /// </summary>
        public int CteId { get; set; }

        /// <summary>
        /// Gets or sets Chave
        /// </summary>
        public string Chave { get; set; }

        /// <summary>
        /// Gets or sets Conteiner
        /// </summary>
        public int Conteiner { get; set; }

        /// <summary>
        /// Gets or sets SerieNotaFiscal
        /// </summary>
        public int SerieNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets NumeroNotaFiscal
        /// </summary>
        public int NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets PesoTotal
        /// </summary>
        public double PesoTotal { get; set; }

        /// <summary>
        /// Gets or sets PesoRateio
        /// </summary>
        public double PesoRateio { get; set; }

        /// <summary>
        /// Gets or sets Valor
        /// </summary>
        public double ValorNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets Remetente
        /// </summary>
        public string Remetente { get; set; }

        /// <summary>
        /// Gets or sets Destinatario
        /// </summary>
        public string Destinatario { get; set; }

        /// <summary>
        /// Gets or sets CnjpRemetente
        /// </summary>
        public string CnjpRemetente { get; set; }

        /// <summary>
        /// Gets or sets CnpjDestinatario
        /// </summary>
        public string CnpjDestinatario { get; set; }

        /// <summary>
        /// Gets or sets Tif
        /// </summary>
        public int Tif { get; set; }

        /// <summary>
        /// Gets or sets SiglaRemetente
        /// </summary>
        public string SiglaRemetente { get; set; }
    }
}
