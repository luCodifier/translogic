﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RelatorioFaturamentoDto
    {
        public DateTime Faturamento;

        public string FaturamentoFormatado
        {
            get { return  this.Faturamento.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        public string Vagao { get; set; }
        public string UFOrigem { get; set; }
        public string Origem { get; set; }
        public string UFDestino { get; set; }
        public string Destino { get; set; }
        public string TipoVagao { get; set; } 
        public string Fluxo { get; set; }
        public string Segmento { get; set; }
        public string Cliente { get; set; }
        public string Expedidor { get; set; }
        public string Recebedor { get; set; }
        public string Str363 { get; set; }
        public string Str1131 { get; set; }
        public string MD { get; set; } 
        public string PrefixoTrem { get; set; }
        public string OS { get; set; }
        public string Lotacao { get; set; } //FALTANDO
        public string Situacao { get; set; } //FALTANDO
        public decimal QuantNotaFiscal { get; set; } 

    }
}