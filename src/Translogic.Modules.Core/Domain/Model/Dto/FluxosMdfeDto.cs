﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// Dto com os fluxos do mdfe
    /// </summary>
    public class FluxosMdfeDto
    {
        /// <summary>
        /// Gets or sets IdEmpresa
        /// </summary>
        public virtual decimal IdEmpresa { get; set; }

        /// <summary>
        /// Gets or sets IdMdfe
        /// </summary>
        public virtual decimal IdMdfe { get; set; }

        /// <summary>
        /// Gets or sets EmpresaRecebedora
        /// </summary>
        public virtual string EmpresaRecebedora { get; set; }

        /// <summary>
        /// Gets or sets CnpjRecebedora
        /// </summary>
        public virtual string CnpjRecebedora { get; set; }

        /*
                /// <summary>
                /// Gets or sets EmpresaDestino
                /// </summary>
                public virtual string EmpresaDestino { get; set; }

                /// <summary>
                /// Gets or sets EmpresaOrigem
                /// </summary>
                public virtual string EmpresaOrigem { get; set; }

                /// <summary>
                /// Gets or sets Mercadoria
                /// </summary>
                public virtual string Mercadoria { get; set; }

                /// <summary>
                /// Gets or sets Mercadoria
                /// </summary>
                public virtual string CodigoMercadoria { get; set; }

                /// <summary>
                /// Gets or sets Origem
                /// </summary>
                public virtual string Origem { get; set; }

                /// <summary>
                /// Gets or sets Destino
                /// </summary>
                public virtual string Destino { get; set; } 
         */
    }
}