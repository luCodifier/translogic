﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MotivoSituacaoVagaoLotacaoDto {

        public decimal IdLotacao { get; set; }
        public string DescricaoLotacao { get; set; }              
    }
}