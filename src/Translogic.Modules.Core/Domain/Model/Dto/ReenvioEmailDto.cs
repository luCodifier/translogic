namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using Diversos.Cte;

    /// <summary>
    /// Classe ReenvioEmailDto
    /// </summary>
    public class ReenvioEmailDto
    {
        /// <summary>
        /// Email de envio para
        /// </summary>
        public virtual int IdXmlEnvio { get; set; } 
        
        /// <summary>
        /// Email de envio para
        /// </summary>
        public virtual int IdCte { get; set; }

        /// <summary>
        /// Email de envio para
        /// </summary>
        public virtual string EnvioPara { get; set; }

        /// <summary>
        /// Tipo de envio do Cte
        /// </summary>
        public virtual TipoEnvioCteEnum TipoEnvioCte { get; set; }

        /// <summary>
        /// Tipo do arquivo enviado por email
        /// </summary>
        public virtual TipoArquivoEnvioEnum? TipoArquivoEnvio { get; set; }

        /// <summary>
        /// Indica se o email para esse Cte est� ativo
        /// </summary>
        public virtual bool Erro { get; set; }
    }
}