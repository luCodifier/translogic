﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MotivoSituacaoVagaoSituacaoDto {

        public decimal IdSituacao { get; set; }
        public string DescricaoSituacao { get; set; }              
    }
}