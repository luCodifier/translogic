﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// DTO de TFA (Termo de Falta ou Avaria)
    /// </summary>
    public class TfaDto
    {
        #region Propriedades

        public virtual decimal ProcessoId { get; set; }
        public virtual string Cliente { get; set; }
        public virtual string TerminalOrigem { get; set; }
        public virtual string TerminalDestino { get; set; }
        public virtual string Unidade { get; set; }
        public virtual DateTime? DtVistoria { get; set; }
        public virtual string Vagao { get; set; }
        public virtual string SerieVagao { get; set; }
        public virtual decimal? Despacho { get; set; }
        public virtual string SerieDespacho { get; set; }
        public virtual string Nf { get; set; }
        public virtual decimal? PesoOrigem { get; set; }
        public virtual decimal? PesoDestino { get; set; }
        public virtual decimal? PerdaTonVagao { get; set; }
        public virtual decimal? ValorMercadoUnitario { get; set; }
        public virtual string Mercadoria { get; set; }
        public virtual string Causa { get; set; }
        public virtual string Lacres { get; set; }
        public virtual string Gambitagem { get; set; }
        public virtual string LiberadoPor { get; set; }
        public virtual string Conclusao { get; set; }
        public virtual string Historico { get; set; }
        public virtual string Vistoriador { get; set; }
        public virtual string MatriculaVistoriador { get; set; }
        public virtual string NomeRepresentante { get; set; }
        public virtual string DocRepresentante { get; set; }
        public virtual DateTime? DataRepresentante { get; set; }
        public virtual Byte[] ImagemAssinaturaRepresentante { get; set; }

        #endregion
    }
}