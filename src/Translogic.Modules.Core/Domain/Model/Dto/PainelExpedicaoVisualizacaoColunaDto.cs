﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class PainelExpedicaoVisualizacaoColunaDto
    {
        public decimal Valor { get; set; }
        public CorEnum Cor { get; set; }
        public bool Visible { get; set; }
        public int Indice { get; set; }

        public bool IsVisible
        {
            get { return Visible; }
            set { Visible = value; }
        }

        public string EstiloCelula
        {
            get
            {
                string corEstiloCelula = "corBranco";
                if (this.Cor == CorEnum.Vermelho)
                    return "corVermelho";
                else if (this.Cor == CorEnum.Amarelo)
                    return "corAmarelo";
                else if (this.Cor == CorEnum.Verde)
                    return "corVerde";

                return corEstiloCelula;
            }
        }

    }
}