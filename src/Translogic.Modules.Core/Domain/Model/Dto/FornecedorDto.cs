﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class FornecedorDto
    {
        public decimal IdFornecedor { get; set; }
        public string DescricaoFornecedor { get; set; }

        public string Local { get; set; }

        public string TipoServico { get; set; }
    }
}