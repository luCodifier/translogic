﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class HistoricoEnvioEmailDto
    {
        public string CodigoVagao { get; set; }
        public string NumeroCte { get; set; }
        public string ChaveCte { get; set; }
        public DateTime DataHoraCte { get; set; }
        public string TipoArquivo { get; set; }
        public DateTime DataHoraEnvio { get; set; }
        public string Status { get; set; }
        public string EnviadoPara { get; set; }
        public string EnviadoCc { get; set; }
        public string EnviadoCo { get; set; }
    }
}