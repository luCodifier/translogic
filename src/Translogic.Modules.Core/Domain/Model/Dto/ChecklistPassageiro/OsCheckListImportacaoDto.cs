﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    public class OsCheckListImportacaoDto
    {
        public decimal IdOsCheckListImportacao{get;set;}
        public decimal IdOsAprovacao { get; set; }
        public string TipoAprovacao { get; set; }
        public string NomeArquivo { get; set; }
        public byte[] Arquivo { get; set; }
    }
}


