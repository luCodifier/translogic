﻿namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    using System;

    public class OsChecklistExportacaoDto
    {
        public string Prefixo { get; set; }
        public string Controlador { get; set; }
        public string Supervisor { get; set; }
        public string QuantidadeVagoes { get; set; }
        public string QuantidadeLocomotivas { get; set; }
        public decimal OS { get; set; }
        public decimal? IdComposicao { get; set; }
        public decimal? IdTrem { get; set; }
        public decimal IdOS { get; set; }
        public string Tipo { get; set; }
        public string TB { get; set; }
        public string Status { get; set; }
        
        public decimal IdOSCheckList { get; set; }

        //ABA Aprovações
        //Via        
        public string ViaAprovador { get; set; }
        public string ViaMatricula { get; set; }
        public DateTime? ViaData { get; set; }
        public string ViaStatus { get; set; }
        public string ViaJustificativa { get; set; }
        //Locomotiva
        public string LocomotivaAprovador { get; set; }
        public string LocomotivaMatricula { get; set; }
        public DateTime? LocomotivaData { get; set; }
        public string LocomotivaStatus { get; set; }
        public string LocomotivaJustificativa { get; set; }
        //Tração
        public string TracaoAprovador { get; set; }
        public string TracaoMatricula { get; set; }
        public DateTime? TracaoData { get; set; }
        public string TracaoStatus { get; set; }
        public string TracaoJustificativa { get; set; }
        //Vagão
        public string VagaoAprovador { get; set; }
        public string VagaoMatricula { get; set; }
        public DateTime? VagaoData { get; set; }
        public string VagaoStatus { get; set; }
        public string VagaoJustificativa { get; set; }

        public string MaquinistaNome { get; set; }
        public string MaquinistaMatricula { get; set; }
        public string AuxiliarNome { get; set; }
        public string AuxiliarMatricula { get; set; }
        public string SupervisorNome { get; set; }
        public string SupervisorMatricula { get; set; }

        //ABA CheckList
        //Locomotiva 1
        public string CodigoLoco1 { get; set; }
        public decimal? DieselLoco1 { get; set; }
        public DateTime? DtVencimentoLoco1 { get; set; }
        public string CativasLoco1 { get; set; }
        public string AdicionaisLoco1 { get; set; }
        //Locomotiva 2
        public string CodigoLoco2 { get; set; }
        public decimal? DieselLoco2 { get; set; }
        public DateTime? DtVencimentoLoco2 { get; set; }
        public string CativasLoco2 { get; set; }
        public string AdicionaisLoco2 { get; set; }
        //Locomotiva 3
        public string CodigoLoco3 { get; set; }
        public decimal? DieselLoco3 { get; set; }
        public DateTime? DtVencimentoLoco3 { get; set; }
        public string CativasLoco3 { get; set; }
        public string AdicionaisLoco3 { get; set; }
        //Locomotiva 4
        public string CodigoLoco4 { get; set; }
        public decimal? DieselLoco4 { get; set; }
        public DateTime? DtVencimentoLoco4 { get; set; }
        public string CativasLoco4 { get; set; }
        public string AdicionaisLoco4 { get; set; }

        public string Resposta1 { get; set; }
        public string Resposta2 { get; set; }
        public string Resposta3 { get; set; }
        public string Resposta4 { get; set; }
        public decimal? Resposta5 { get; set; }
        public decimal? Resposta6 { get; set; }
        public decimal? Resposta7 { get; set; }

        public DateTime? ChegadaLocoOrigem { get; set; }
        public DateTime? Previsto { get; set; }
        public DateTime? Real { get; set; }

        public string ComentAdicionais { get; set; }//COMENT_ADICIONAIS
    }
}