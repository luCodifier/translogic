﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    public class OsDadosTremDto
    {
        public virtual decimal QtdeVagoes { get; set; }// VAG_QTD NUMBER
        public virtual decimal TB { get; set; }// VAG_TB NUMBER
        public virtual DateTime? DataPartidaPrevista { get; set; }// X1_DAT_PAR_PRV_OFI
        public virtual DateTime? DataPartidaReal { get; set; }// TR_DAT_LBR
        public virtual DateTime? DataChegadaReal { get; set; }// TR_DAT_LBR
        public virtual decimal IdTrem { get; set; } // TR_ID_TRM
        public virtual decimal IdComposicao { get; set; } //CP_ID_CPS

        public virtual IList<LocomotivaDto> listLocomotiva { get; set; }
    }
}