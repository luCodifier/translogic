﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;

    public class OsChecklistAprovacaoDto
    {
        public bool Salvar { get; set; }
        public int idOsChecklist { get; set; }
        public int idOs { get; set; }
        public bool Ida { get; set; }
        public string TipoAprovacao { get; set; } // Via, Loco, Tracao, Vagao
        public bool Aprovado { get; set; }
        public DateTime? DataHoraAprocacao { get; set; }
        public string Observacao { get; set; }
        public string Usuario { get; set; }
        public string Matricula { get; set; }
        public OsCheckListImportacaoDto OsCheckListImportacaoDto { get; set; }
    }
}