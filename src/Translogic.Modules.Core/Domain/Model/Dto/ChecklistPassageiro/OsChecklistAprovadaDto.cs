﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro;

    public class OsChecklistAprovadaDto
    {
        public int idOsChecklist { get; set; }
        public int idOsChecklistAprovacao { get; set; }
        public int idOs { get; set; }

        //// Propriedades de aprovações da área VIA
        public  string ViaUsuarioMatricula { get; set; }
        public  string ViaUsuarioNome { get; set; } 
        public  DateTime? ViaDataAprovacao { get; set; } 
        public  char ViaStatus { get; set; } //  -- A = APROVADO, R - REPROVADO
        public  char ViaImportacao { get; set; } // -- I = IMPORTAÇÃO, U -USUÁRIO (APROVAÇÕES)
        public  string ViaObs { get; set; }

        //// Propriedades de aprovações da área Locoomitiva
        public  string LocoUsuarioMatricula { get; set; }//LOCO_USR_MATR
        public  string LocoUsuarioNome { get; set; } //LOCO_USR_NOME
        public  DateTime? LocoDataAprovacao { get; set; } //LOCO_DT_APR 
        public  char LocoStatus { get; set; } //LOCO_IDT_STATUS 
        public  char LocoImportacao { get; set; } //LOCO_IDT_IMPORTACAO 
        public  string LocoObs { get; set; } //LOCO_OBS 

        //// Propriedades de aprovações da área Tração
        public  string TracUsuarioMatricula { get; set; }//TRAC_USR_MATR
        public  string TracUsuarioNome { get; set; } //TRAC_USR_NOME
        public  DateTime? TracDataAprovacao { get; set; } //TRAC_DT_APR 
        public  char TracStatus { get; set; } //TRAC_IDT_STATUS 
        public  char TracImportacao { get; set; } //TRAC_IDT_IMPORTACAO 
        public  string TracObs { get; set; } //TRAC_OBS 


        //// Propriedades de aprovações da área Vagão
        public  string VagUsuarioMatricula { get; set; }//VAG_USR_MATR
        public  string VagUsuarioNome { get; set; } //VAG_USR_NOME
        public  DateTime? VagDataAprovacao { get; set; } //VAG_DT_APR 
        public  char VagStatus { get; set; } //VAG_IDT_STATUS 
        public  char VagImportacao { get; set; } //VAG_IDT_IMPORTACAO 
        public  string VagObs { get; set; } //VAG_OBS 
        public List< OsCheckListImportacaoDto> OsCheckListImportacaoDto { get; set; }
    }
}