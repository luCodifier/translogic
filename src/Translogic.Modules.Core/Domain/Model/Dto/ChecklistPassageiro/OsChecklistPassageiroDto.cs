﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class OsChecklistPassageiroDto
    {
        public string Usuario { get; set; }
        public string Matricula { get; set; }
        
        //Tipo CHECKLIST
        public bool Ida { get; set; }
        public bool Volta { get; set; }

        public int idOsChecklistPassageiro { get; set; }
        public int idOsChecklist { get; set; }
        public int idOs { get; set; }
        public int numOs { get; set; }
        
        //CCO Circulação
        public string CCOtxtSupervisorCCO { get; set; }
        public string CCOtxtMatriculaSupervisorCCO { get; set; }
        public string CCOtxtControlador { get; set; }
        public string CCOtxtMatriculaControlador { get; set; }
        
        //EQUIPAGEM
        public string EquiptxtMaquinista { get; set; }
        public string EquiptxtMatriculaMaquinista { get; set; }
        public string EquiptxtAuxiliar { get; set; }
        public string EquiptxtMatriculaAuxiliar { get; set; }
        public string EquiptxtSupervisorEquipagem { get; set; }
        public string EquiptxtMatriculaSupervisorEquipagem { get; set; }
        
        //Comentários Adicionais
        public string txtComentAdicionais { get; set; }
        
        //VAGÕES
        public int? VagoestxtQTDVagoes { get; set; }
        public decimal? VagoestxtTB { get; set; }
        
        //LOCOMOTIVAS
        public string LocotxtLoco1 { get; set; }
        public decimal? LocotxtDiesel1 { get; set; }
        public DateTime? LocodtVencimento1 { get; set; }
        public bool? LocordbCativas1 { get; set; }
        public string LocotxtOBSCativas1 { get; set; }

        public string LocotxtLoco2 { get; set; }
        public decimal? LocotxtDiesel2 { get; set; }
        public DateTime? LocodtVencimento2 { get; set; }
        public bool? LocordbCativas2 { get; set; }
        public string LocotxtOBSCativas2 { get; set; }

        public string LocotxtLoco3 { get; set; }
        public decimal? LocotxtDiesel3 { get; set; }
        public DateTime? LocodtVencimento3 { get; set; }
        public bool? LocordbCativas3 { get; set; }
        public string LocotxtOBSCativas3 { get; set; }

        public string LocotxtLoco4 { get; set; }
        public decimal? LocotxtDiesel4 { get; set; }
        public DateTime? LocodtVencimento4 { get; set; }
        public bool? LocordbCativas4 { get; set; }
        public string LocotxtOBSCativas4 { get; set; }
        //CHECKLIST
        public bool? rdbListrdbTesteGradiente { get; set; }
        public bool? rdbListrdbTesteVazamento { get; set; }
        public bool? rdbListrdbTesteResposta { get; set; }
        public bool? rdbListrdbListaVagoes { get; set; }
        public string ListtxtNCruzamentosForaPO { get; set; }
        public string ListtxtSupervisorNCruzamentoForaPO { get; set; }
        public string ListtxtMatriculaNCruzamentoForaPO { get; set; }
        public int? ListtxtTotalCruzamentos { get; set; }
        public int? ListtxtQuantCruzamentosPararam { get; set; }
        //PREVISÃO
        public DateTime? PrevdtHoraChegadaLocoOrigem { get; set; }
        public DateTime? PrevtxtPrevisto { get; set; }
        public DateTime? PrevtxtReal { get; set; }
        public DateTime? DataChegadaReal { get; set; }
       
    }
}