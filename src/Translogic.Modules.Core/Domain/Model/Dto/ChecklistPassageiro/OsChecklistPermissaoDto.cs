﻿
namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    public class OsChecklistPermissaoDto
    {
        public bool AlteraStatusCheckList { get; set; }

        public bool Pesquisar { get; set; }
        public bool Gerar { get; set; }
        public bool Visualizar { get; set; }
        public bool Importar { get; set; }
        public bool Exportar { get; set; }
        public bool SalvarChecklist { get; set; }
        public bool ConcluirChecklist { get; set; }
        public bool EditarChecklist { get; set; }

        public bool AprovarVia { get; set; }
        public bool ReprovarVia { get; set; }
        public bool AprovarLoco { get; set; }
        public bool ReprovarLoco { get; set; }
        public bool AprovarTracao { get; set; }
        public bool ReprovarTracao { get; set; }
        public bool AprovarVagao { get; set; }
        public bool ReprovarVagao { get; set; }
    }
}