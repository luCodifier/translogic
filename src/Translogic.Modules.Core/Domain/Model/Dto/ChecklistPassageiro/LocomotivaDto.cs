﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    public class LocomotivaDto
    {
        public virtual string Locomotiva { get; set; }
    }
}