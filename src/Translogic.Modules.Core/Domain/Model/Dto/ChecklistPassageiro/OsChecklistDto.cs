﻿namespace Translogic.Modules.Core.Domain.Model.Dto.ChecklistPassageiro
{
    using System;

    public class OsChecklistDto
    {
        public decimal NumOs { get; set; }
        public decimal IdOsCheckList { get; set; }
        public decimal IdOsCheckListAprovacao { get; set; }
        public decimal IdOs { get; set; }
        public string Prefixo { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public DateTime Data { get; set; }
        public string Via { get; set; }
        public string Locomotiva { get; set; }
        public string Tracao { get; set; }
        public string Vagao { get; set; }
        public string Controlador { get; set; }
        public decimal Ida { get; set; }
        public decimal Volta { get; set; }
        public decimal Concluido { get; set; }

        public decimal HabilitaEditar
        {
            get
            {
                return this.Concluido == 0 ? 1 : 0;
            }
        }

    }
}