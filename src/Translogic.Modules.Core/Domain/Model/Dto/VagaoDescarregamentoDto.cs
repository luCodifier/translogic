﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    /// <summary>
    /// Lista para exibicao na tela de descarregamento
    /// </summary>
    public class VagaoDescarregamentoDto
    {
        /// <summary>
        /// Identificador de sequência
        /// </summary>
        public decimal IdLinha { get; set; }

        /// <summary>
        /// Código da linha
        /// </summary>
        public string Linha { get; set; }

        /// <summary>
        /// Identificador de sequência
        /// </summary>
        public decimal Sequencia { get; set; }

        /// <summary>
        /// Número da série
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// Código de vagão
        /// </summary>
        public string Vagao { get; set; }

        /// <summary>
        /// Id do vagão
        /// </summary>
        public decimal IdVagao { get; set; }

        /// <summary>
        /// ID do pedido (T2_PEDIDO)
        /// </summary>
        public decimal IdPedido { get; set; }

        /// <summary>
        /// Código do pedido (T2_PEDIDO)
        /// </summary>
        public string CodPedido { get; set; }

        /// <summary>
        /// ID do pedido de transporte
        /// </summary>
        public decimal IdPedidoTransporte { get; set; }

        /// <summary>
        /// Código do pedido de transporte
        /// </summary>
        public string CodPedidoTransporte { get; set; }

        /// <summary>
        /// Data do pedido de transporte
        /// </summary>
        public DateTime DataPedidoTransporte { get; set; }

        /// <summary>
        /// ID do pedido derivado
        /// </summary>
        public decimal? IdPedidoDerivado { get; set; }

        /// <summary>
        /// Data do pedido derivado
        /// </summary>
        public DateTime? DataPedidoDerivado { get; set; }

        /// <summary>
        /// Código da mercadoria
        /// </summary>
        public string Mercadoria { get; set; }

        /// <summary>
        /// Id da Area Operacional
        /// </summary>
        public decimal IdAreaOperacional { get; set; }

        /// <summary>
        /// Id do Vagao Patio
        /// </summary>
        public decimal IdVagaoPatio { get; set; }

        /// <summary>
        /// Id do Vagao Pedido
        /// </summary>
        public decimal IdVagaoPedido { get; set; }

        /// <summary>
        /// Id da condição de uso
        /// </summary>
        public decimal IdCondicaoUso { get; set; }

        /// <summary>
        /// Data da condição de uso
        /// </summary>
        public DateTime DataCondicaoUso { get; set; }

        /// <summary>
        /// Id da locação
        /// </summary>
        public decimal IdLocacao { get; set; }

        /// <summary>
        /// Data da locação
        /// </summary>
        public DateTime DataLocacao { get; set; }

        /// <summary>
        /// Id da localização
        /// </summary>
        public decimal IdLocalizacao { get; set; }

        /// <summary>
        /// Data da Ultima Pesagem
        /// </summary>
        public DateTime DataPesagem { get; set; }

        /// <summary>
        /// Erro ocorrido na descarga automatica
        /// </summary>
        public string Erro { get; set; }

        /// <summary>
        /// XML de erro retornado pela SP
        /// </summary>
        public string XMLErro { get; set; }
    }
}
