﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OsComposicaoDto
    {
        public decimal IdDocumentacao { get; set; }

        public decimal IdOs { get; set; }

        public decimal IdRecebedor { get; set; }

        public decimal IdComposicao { get; set; }

        public string PrefixoTrem { get; set; }

        public decimal? NumeroOs { get; set; }

        public string Recomposicao { get; set; }

        public bool HouveRecomposicao
        {
            get { return Recomposicao == "S"; }
        }

        public string Refaturamento { get; set; }

        public bool HouveRefaturamento
        {
            get { return Refaturamento == "S"; }
        }

        public string Chegada { get; set; }
        public bool HouveChegada
        {
            get { return Chegada == "S"; }
        }

        public DateTime? DataChegada { get; set; }

        public string Saida { get; set; }
        public bool HouveSaida
        {
            get { return Saida == "S"; }
        }
    }
}