﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// Dto para retornar a UF dos estados
    /// </summary>
    public class UFDto
    {
        /// <summary>
        /// UF do estado
        /// </summary>
        public string Uf { get; set; }
    }
}