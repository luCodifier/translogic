namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe RefaturamentoDto
	/// </summary>
	public class RefaturamentoDto
	{
		/// <summary>
		/// codigo do Ve�culo
		/// </summary>
		public virtual string Veiculo { get; set; }

		/// <summary>
		/// Sequencia do Vagao
		/// </summary>
		public virtual decimal SequenciaVagao { get; set; }

		/// <summary>
		///  S�rie do vag�o
		/// </summary>
		public virtual string SerieVagao { get; set; }

		/// <summary>
		///  Empresa Propriet�ria
		/// </summary>
		public virtual string EmpresaProprietaria { get; set; }

		/// <summary>
		/// Descricao Frota
		/// </summary>
		public virtual string Frota { get; set; }

		/// <summary>
		///  Origem do Trem
		/// </summary>
		public virtual string Origem { get; set; }

		/// <summary>
		///  Destino do Trem
		/// </summary>
		public virtual string Destino { get; set; }

		/// <summary>
		///  Mercadoria do Trem
		/// </summary>
		public virtual string Mercadoria { get; set; }

		/// <summary>
		///  Empresa Remetente
		/// </summary>
		public virtual string EmpresaRemetente { get; set; }

		/// <summary>
		///  Empresa Csg
		/// </summary>
		public virtual string EmpresaCsg { get; set; }

		/// <summary>
		///  Num pedido
		/// </summary>
		public virtual string Pedido { get; set; }

		/// <summary>
		///  Tu do vag�o
		/// </summary>
		public virtual decimal Tu { get; set; }

		/// <summary>
		///  Carga Bruta
		/// </summary>
		public virtual decimal CargaBruta { get; set; }

		/// <summary>
		///  Condi��o de Uso
		/// </summary>
		public virtual string CondUso { get; set; }

		/// <summary>
		///  Ind de Freio
		/// </summary>
		public virtual string IndFreio { get; set; }

		/// <summary>
		///  Descricao da Mercadoria
		/// </summary>
		public virtual string MercDescricao { get; set; }

		/// <summary>
		///  Descricao da Empresa Csg
		/// </summary>
		public virtual string EmpresaCsgDesc { get; set; }

		/// <summary>
		/// Comprimento do Vag�o
		/// </summary>
		public virtual decimal Comprimento { get; set; }

		/// <summary>
		///  numero da nota
		/// </summary>
		public virtual string Nota { get; set; }

		/// <summary>
		///  Tipo Bitola
		/// </summary>
		public virtual string Bitola { get; set; }

		/// <summary>
		///  Descricao da Classe
		/// </summary>
		public virtual string DescClasse { get; set; }

		/// <summary>
		///  Id do Despacho
		/// </summary>
		public virtual decimal IdDespacho { get; set; }

		/// <summary>
		///  Cod do Fluxo Comercial
		/// </summary>
		public virtual string CodFluxoComercial { get; set; }

		/// <summary>
		///  Id do Fluxo Comercial
		/// </summary>
		public virtual decimal IdFluxoComercial { get; set; }
		
		/// <summary>
		///  Id do Vag�o
		/// </summary>
		public virtual decimal IdVagao { get; set; }

		/// <summary>
		///  Id do Trem
		/// </summary>
		public virtual decimal IdTrem { get; set; }

		/// <summary>
		///  Id da Composicao
		/// </summary>
		public virtual decimal IdComposicao { get; set; }
	}
}