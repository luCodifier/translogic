namespace Translogic.Modules.Core.Domain.Model.Dto
{
	/// <summary>
	/// Dto de cancelamento de CTe
	/// </summary>
    public class TerminalSeguroDto
	{
		/// <summary>
		/// Id da area operacional
		/// </summary>
		public decimal IdTerminal { get; set; }

        /// <summary>
        /// Codigo do terminal
        /// </summary>
        public string CodTerminal { get; set; }

        /// <summary>
        /// Descricao do terminal
        /// </summary>
        public string DescricaoTerminal { get; set; }
	}
}