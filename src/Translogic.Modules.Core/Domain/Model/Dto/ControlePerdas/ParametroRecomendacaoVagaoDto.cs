﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.ControlePerdas;

namespace Translogic.Modules.Core.Domain.Model.Dto.ControlePerdas
{
    /// <summary>
    /// Classe DTo para transporte de parâmetros de Recomendação de Manutenção de Vagão no módulo Controle de Perdas
    /// </summary>
    public class ParametroRecomendacaoVagaoDto
    {
        /// <summary>
        /// lista de parâmetros de Recomendações  
        /// </summary>
        public List<RecomendacaoVagaoDto> Recomendacoes { get; set; }

        /// <summary>
        /// lista de tolerâncias  
        /// </summary>
        public List<RecomendacaoTolerancia> Tolerancias { get; set; }
    }

    /// <summary>
    ///   Classe Recomendação de Vagão - (Gestão de regras de recomendação)
    /// </summary>
    public class RecomendacaoVagaoDto
    {
        #region Propriedades

        /// <summary>
        /// Id da Recomendação do Vagão
        /// </summary>
        public virtual int? Id { get; set; }

        /// <summary>
        /// Id da Causa Seguro vinculado ao Recomendação do Vagão
        /// </summary>
        public virtual int IdCausaSeguro { get; set; }

        /// <summary>
        /// Identifica se deve ser considerada a Recomendação S/N (N - Default)
        /// </summary>
        public virtual bool Recomendacao { get; set; }

        /// <summary>
        /// Identifica se deve ser considerada a Tolerância S/N (N - Default)
        /// </summary>
        public virtual bool Tolerancia { get; set; }

        #endregion
    }
}