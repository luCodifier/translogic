﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// DTO de Parâmetros para Envio de Emails de TFA
    /// </summary>
    public class TfaEmailDto
    {
        #region Propriedades

        public virtual int NumeroProcesso{ get; set; }
        public virtual string NumeroVagao { get; set; }
        public virtual string SerieVagao { get; set; }
        public virtual string NumeroNotaFiscal { get; set; }
        public virtual string Justificativa { get; set; }

        #endregion
    }
}