﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSVagaoItemLotacaoDto
    {
        public decimal IdLotacao { get; set; }
        public string DescricaoLotacao { get; set; }       
    }
}