﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// Resumos de fluxos
    /// </summary>
    public class ResumoFluxosDto
    {
        public string Cliente { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
    }
}