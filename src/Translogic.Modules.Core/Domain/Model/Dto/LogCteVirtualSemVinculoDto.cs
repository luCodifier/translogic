﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class LogCteVirtualSemVinculoDto
    {
        public virtual decimal IdCteVirtualSemVinculo { get; set; }
        public virtual string Usuario { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual decimal IdMovimentacao { get; set; }
        public virtual decimal IdAreaOperacional { get; set; }
        public virtual decimal IdTrem { get; set; }
        public virtual decimal IdComposicao { get; set; }
    }
}