﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Text.RegularExpressions;

    public class PainelExpedicaoConferenciaArquivosDto
    {
        public decimal? IdAgrupamentoMd { get; set; }
        public decimal? IdAgrupamentoSimples { get; set; }
        public string Agrupador { get; set; }
        public decimal Id { get; set; }
        public string Situacao { get; set; }
        public string Serie { get; set; }
        public decimal VagaoId { get; set; }
        public string Vagao { get; set; }
        public decimal PesoTotal { get; set; }
        public string Fluxo { get; set; }
        public string FluxoSemCaracteres
        {
            get
            {
                var digitsOnly = new Regex(@"[^\d]");
                return String.IsNullOrEmpty(Fluxo) ? String.Empty : digitsOnly.Replace(Fluxo, "");   
            }
        }
        public string FluxoSegmento { get; set; }
        public string FluxoMercadoria { get; set; }
        public string Origem { get; set; }
        public string Recebedor { get; set; }
        public string Expedidor { get; set; }
        public string Cliente363 { get; set; }
        public string ChaveNota { get; set; }
        public decimal PesoNfe { get; set; }
        public decimal VolumeNfe { get; set; }
        public decimal PesoRateioNfe { get; set; }
        public decimal PesoReal { get; set; }
        public string Conteiner { get; set; }
        public string ObservacaoRecusado { get; set; }
        public DateTime DataHoraCadastro { get; set; }

        public virtual decimal? Id_Intercambio_Origem { get; set; }
        public decimal? NumeroDespacho { get; set; }
        public string SerieDespacho { get; set; }
        public DateTime? DataDespacho { get; set; }
        public string LocalAtual { get; set; }
        public string MultiploDespacho { get; set; }
        public bool CorSucesso { get; set; }
        public string MultiploDespachoStatus { get; set; }

        public bool LiberacaoRegistroMultiploDespacho
        {
            get
            {
                var liberado = (MultiploDespachoStatus == "S");
                return liberado;
            }
        }

        public virtual bool Intercambio
        {
            get
            {
                return Id_Intercambio_Origem.HasValue;
            }
        }
    }
}