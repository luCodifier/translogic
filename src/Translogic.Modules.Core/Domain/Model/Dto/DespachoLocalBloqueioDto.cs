﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class DespachoLocalBloqueioDto
    {
        public decimal Id { get; set; }
        public decimal IdEstacao { get; set; }
        public string CodigoEstacao { get; set; }
        public string DescricaoEstacao { get; set; }
        public string Segmento { get; set; }
        public string CnpjRaiz { get; set; }
        public string Cliente { get; set; }
        public DateTime? DataBloqueio { get; set; }
        public string UsuarioBloqueio { get; set; }
    }
}