﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    public class LimiteDto
    {
        public decimal IdLimite { get; set; }
        public decimal IdGrupo { get; set; }
        public string Grupo { get; set; }
        public decimal IdFrota { get; set; }
        public string Frota { get; set; }
        public decimal IdMercadoria { get; set; }
        public string Mercadoria { get; set; }
        public decimal Tolerancia { get; set; }        
        public decimal Peso { get; set; }

        public string Usuario { get; set; }
        public string Evento { get; set; }
        public DateTime VersionDate { get; set; }
    }
}