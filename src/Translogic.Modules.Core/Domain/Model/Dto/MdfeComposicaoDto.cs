namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

    /// <summary>
    /// Classe Dto para o Mdfe
    /// </summary>
    public class DadosMdfeComposicaoDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Id { get; set; }

        /// <summary>
        /// Gets or sets IdVagao
        /// </summary>
        public virtual decimal IdVagao { get; set; }

        /// <summary>
        /// Gets or sets Sequencia
        /// </summary>
        public virtual decimal Sequencia { get; set; }

        /// <summary>
        /// Gets or sets CodigoVagao
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets SerieVagao
        /// </summary>
        public virtual string SerieVagao { get; set; }

        /// <summary>
        /// Gets or sets SerieCte
        /// </summary>
        public virtual string SerieCte { get; set; }

        /// <summary>
        /// Gets or sets NumeroCte
        /// </summary>
        public virtual string NumeroCte { get; set; }

        /// <summary>
        /// Gets or sets idCte
        /// </summary>
        public virtual decimal IdCte { get; set; }

        /// <summary>
        /// Gets or sets EmpresaDestino
        /// </summary>
        public virtual string EmpresaDestino { get; set; }

        /// <summary>
        /// Gets or sets EmpresaOrigem
        /// </summary>
        public virtual string EmpresaOrigem { get; set; }

        /// <summary>
        /// Gets or sets EmpresaOrigem
        /// </summary>
        public virtual string Mercadoria { get; set; }

        /// <summary>
        /// Gets or sets ProdutoNfe
        /// </summary>
        public virtual string ProdutoNfe { get; set; }

        /// <summary>
        /// Gets or sets SituacaoCte
        /// </summary>
        public virtual string SituacaoCte { get; set; }

        /// <summary>
        ///  Gets or sets Num Nota
        /// </summary>
        public virtual string NumeroNota { get; set; }

        /// <summary>
        ///  Gets or sets Serie Nota
        /// </summary>
        public virtual string SerieNota { get; set; }

        /// <summary>
        ///  Gets or sets Peso Nota Fiscal
        /// </summary>
        public virtual decimal PesoTotalNf { get; set; }

        /// <summary>
        ///  Gets or sets Peso Nota Fiscal
        /// </summary>
        public virtual decimal PesoRateio { get; set; }

        /// <summary>
        ///  Gets or sets Data da Nota Fiscal
        /// </summary>
        public virtual DateTime DataNfe { get; set; }

        /// <summary>
        /// Gets or sets PesoVagao
        /// </summary>
        public virtual decimal PesoVagao { get; set; }

        /// <summary>
        ///  Gets or sets Data de Carregamento
        /// </summary>
        public virtual DateTime DataCarregamento { get; set; }

        /// <summary>
        /// Gets or sets Local de Carregamento
        /// </summary>
        public virtual string LocalCarregamento { get; set; }
    }

    /// <summary>
    /// Classe Dto para o Mdfe
    /// </summary>
    public class MdfeComposicaoDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Id { get; set; }

        /// <summary>
        /// Gets or sets IdVagao
        /// </summary>
        public virtual decimal IdVagao { get; set; }

        /// <summary>
        /// Gets or sets Sequencia
        /// </summary>
        public virtual decimal Sequencia { get; set; }

        /// <summary>
        /// Gets or sets CodigoVagao
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets SerieVagao
        /// </summary>
        public virtual string SerieVagao { get; set; }
        
        /// <summary>
        /// Gets or sets SerieCte
        /// </summary>
        public virtual string SerieCte { get; set; }

        /// <summary>
        /// Gets or sets PesoVagao
        /// </summary>
        public virtual decimal PesoVagao { get; set; }

        /// <summary>
        /// Gets or sets NumeroCte
        /// </summary>
        public virtual string NumeroCte { get; set; }

        /// <summary>
        /// Gets or sets idCte
        /// </summary>
        public virtual decimal IdCte { get; set; }

        /// <summary>
        /// Gets or sets SituacaoCte
        /// </summary>
        public virtual string SituacaoCte { get; set; }

        /// <summary>
        /// Gets or sets EmpresaDestino
        /// </summary>
        public virtual string EmpresaDestino { get; set; }

        /// <summary>
        /// Gets or sets EmpresaOrigem
        /// </summary>
        public virtual string EmpresaOrigem { get; set; }

        /// <summary>
        /// Gets or sets EmpresaOrigem
        /// </summary>
        public virtual string Mercadoria { get; set; }

        /// <summary>
        ///  Gets or sets Num Nota
        /// </summary>
        public virtual IList<MdfeComposicaoNotasDto> Notas { get; set; }

        /// <summary>
        ///  Gets or sets Data de Carregamento
        /// </summary>
        public virtual DateTime DataCarregamento { get; set; }

        /// <summary>
        /// Gets or sets Local de Carregamento
        /// </summary>
        public virtual string LocalCarregamento { get; set; }
    }

    /// <summary>
    /// Classe Dto para o Mdfe
    /// </summary>
    public class MdfeComposicaoNotasDto
    {
        /// <summary>
        ///  Gets or sets Num Nota
        /// </summary>
        public virtual string NumeroNota { get; set; }

        /// <summary>
        ///  Gets or sets Serie Nota
        /// </summary>
        public virtual string SerieNota { get; set; }

        /// <summary>
        ///  Gets or sets Peso Nota Fiscal
        /// </summary>
        public virtual decimal PesoTotalNF { get; set; }

        /// <summary>
        ///  Gets or sets Peso Nota Fiscal
        /// </summary>
        public virtual decimal PesoRateio { get; set; }

        /// <summary>
        ///  Gets or sets Data Nota Fiscal
        /// </summary>
        public virtual DateTime DataNfe { get; set; }
      
        /// <summary>
        ///  Gets or sets ProdutoNfe
        /// </summary>
        public virtual string Produto { get; set; }
    }
}