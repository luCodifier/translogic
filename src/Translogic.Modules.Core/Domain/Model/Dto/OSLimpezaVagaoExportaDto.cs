﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSLimpezaVagaoExportaDto
    {
        public DateTime Data { get; set; } 
        public decimal IdOs { get; set; }
        public decimal IdOsParcial { get; set; }
        public string Fornecedor { get; set; }
        public string Local { get; set; }
        public DateTime DataHoraEntregaOSFornecedor { get; set; }
        public DateTime DataHoraDevolucaoOS { get; set; }
        public string NumeroVagao { get; set; }
        public decimal Limpeza { get; set; }
        public decimal Lavagem { get; set; }
        public decimal Retrabalho { get; set; }
        public string Observacao { get; set; } 
        public string NomeAprovadorRumo { get; set; }
        public string Status { get; set; }
        public DateTime UltimaAlteracao { get; set; }
        public string Usuario { get; set; }
    }
}