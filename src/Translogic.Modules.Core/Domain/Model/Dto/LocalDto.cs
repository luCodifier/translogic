﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSLocalDto
    {
        public decimal IdLocal { get; set; }
        public string Local { get; set; }
    }
}