﻿using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem.Enumeradores;
using System;
namespace Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem
{
    public class PerguntaCheckListDto
    {
        public decimal IdPergunta { get; set; }
        public decimal OrdemPergunta { get; set; }        
        public string Pergunta { get; set; }
        public string DescricaoAgrupador { get; set; }
        public decimal IdAgrupador { get; set; }
        public string TipoPerguntaCheckList { get; set; }        
        //public TipoPerguntaCheckList TipoPerguntaCheckList { get; set; }
    }
}