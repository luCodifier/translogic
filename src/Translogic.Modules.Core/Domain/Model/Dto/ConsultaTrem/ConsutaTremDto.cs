﻿namespace Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem
{
    using System;

    public class ConsultaTremDto
    {
        public decimal IdTrem { get; set; }
        public decimal NumOs { get; set; }
        public string Trem { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public decimal HorasAPT { get; set; }
        public string HorasAPT1 { get; set; }
        public DateTime? PartidaPrevisao { get; set; }
        public DateTime? PartidaReal { get; set; }
        public string Situacao { get; set; }
        public string LocalAtual { get; set; }
        public DateTime? DataHora { get; set; }
        public string PosicaoVirtual { get; set; }
        public DateTime? Desde { get; set; }
        public decimal TLocos { get; set; }
        public decimal LT { get; set; }
        public decimal LR { get; set; }
        public decimal TVagoes { get; set; }
        public decimal VC { get; set; }
        public decimal VZ { get; set; }
        public decimal Comprimento { get; set; }
        public decimal Peso { get; set; }
        public decimal Onu { get; set; }
        public decimal TotalOnu { get; set; }
    }
}