namespace Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem.Enumeradores
{
    using System.ComponentModel;

    /// <summary>
    /// Enumerador do Tipo Pergunta CheckList
    /// </summary>
    public enum TipoPerguntaCheckList
    {
        /// <summary>
        /// Mercadoria
        /// </summary>
        [Description("M")] MercadoriaPerigosa,

        /// <summary>
        /// Papeleta
        /// </summary>
        [Description("P")] Papeleta,
    }
}