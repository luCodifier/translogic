﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class ConfiguracaoEnvioDocumentacaoDto
    {
        public decimal RecebedorId { get; set; }
        public string Recebedor { get; set; }
    }
}