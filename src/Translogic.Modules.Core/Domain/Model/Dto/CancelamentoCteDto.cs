namespace Translogic.Modules.Core.Domain.Model.Dto
{
	/// <summary>
	/// Dto de cancelamento de CTe
	/// </summary>
	public class CancelamentoCteDto
	{
		/// <summary>
		/// Id do motivo de cancelamento
		/// </summary>
		public int IdMotivoCancelamento { get; set; }

		/// <summary>
		/// Array de dtos de cte a serem cancelados
		/// </summary>
		public CteDto[] Dtos { get; set; }

        /// <summary>
        /// N�mero de vag�es selecionados
        /// </summary>
        public int? NumeroVagoesSelecionados { get; set; }
	}
}