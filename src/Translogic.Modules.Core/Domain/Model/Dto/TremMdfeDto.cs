﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;
    using Diversos.Cte;
	using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;

    /// <summary>
    /// Classe de DTO para Trem
    /// </summary>
    public class TremMdfeDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public int Id { get; set; }
		
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Prefixo { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public int OS { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
		public SituacaoMdfeEnum SituacaoAtual { get; set; }

        /// <summary>
        /// Gets or sets DataRealizadaPartida
        /// </summary>
        public DateTime DataRealizadaPartida { get; set; }

        /// <summary>
        /// Mensagem Informativa
        /// </summary>
        public string Informativo { get; set; }

        /// <summary>
        /// Mensagem de ultimo evento
        /// </summary>
        public string UltimoEvento { get; set; }

        /// <summary>
        /// Gets or sets DataEvento
        /// </summary>
        public DateTime DataEvento { get; set; }
        
    }
}
