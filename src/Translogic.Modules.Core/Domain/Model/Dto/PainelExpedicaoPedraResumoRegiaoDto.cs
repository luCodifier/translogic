﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class PainelExpedicaoPedraResumoRegiaoDto
    {
        public string Regiao { get; set; }
        public Int32 PlanejadoSade { get; set; }
        public Int32 Realizado { get; set; }
        public Int32 Saldo
        {
            get
            {
                return PlanejadoSade - Realizado;
            }

        }
        public decimal Porcentagem { get; set; }
    }
}