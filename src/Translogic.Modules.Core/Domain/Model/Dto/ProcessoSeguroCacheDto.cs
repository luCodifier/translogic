﻿using System;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
	/// <summary>
	///   Classe de DTO para Processo Seguro Cache
	/// </summary>
	public class ProcessoSeguroCacheDto
	{
		/// <summary>
		///   Gets or sets ProcessoSeguroCacheId
		/// </summary>
		public int ProcessoSeguroCacheId { get; set; }

		/// <summary>
		///   Gets or sets Usuario
		/// </summary>
		public Usuario Usuario { get; set; }

		/// <summary>
		///   Gets or sets Despacho
		/// </summary>
		public int? Despacho { get; set; }

		/// <summary>
		///   Gets or sets Serie
		/// </summary>
		public string Serie { get; set; }

		/// <summary>
		///   Gets or sets DataSinistro
		/// </summary>
		public DateTime? DataSinistro { get; set; }

		/// <summary>
		///   Gets or sets Laudo
		/// </summary>
		public int? Laudo { get; set; }

        /// <summary>
        ///   Valor Mercadoria
        /// </summary>
        public virtual double? ValorMercadoria { get; set; }

		/// <summary>
		///   Gets or sets Terminal
		/// </summary>
		public decimal Terminal { get; set; }

        /// <summary>
        ///   Gets or sets Terminal
        /// </summary>
        public string TerminalDesc { get; set; }

		/// <summary>
		///   Gets or sets Perda
		/// </summary>
		public double? Perda { get; set; }

		/// <summary>
		///   Gets or sets Causa
		/// </summary>
		public CausaSeguro Causa { get; set; }

		/// <summary>
		///   Gets or sets Gambit
		/// </summary>
		public string Gambit { get; set; }

		/// <summary>
		///   Gets or sets Lacre
		/// </summary>
		public string Lacre { get; set; }

		/// <summary>
		///   Gets or sets Vistoriador
		/// </summary>
		public string Vistoriador { get; set; }

		/// <summary>
		///   Gets or sets Avaliacao
		/// </summary>
		public string Avaliacao { get; set; }

		/// <summary>
		///   Gets or sets Unidade
		/// </summary>
		public UnidadeSeguro Unidade { get; set; }

		/// <summary>
		///   Gets or sets Rateio
		/// </summary>
		public double? Rateio { get; set; }

		/// <summary>
		///   Gets or sets Historico
		/// </summary>
		public string Historico { get; set; }

		/// <summary>
		///   Gets or sets Nota
		/// </summary>
		public string Nota { get; set; }

		/// <summary>
		///   Gets or sets Cliente
		/// </summary>
		public string Cliente { get; set; }

		/// <summary>
		///   Gets or sets Produto
		/// </summary>
		public string Produto { get; set; }

		/// <summary>
		///   Gets or sets Peso
		/// </summary>
		public double? Peso { get; set; }

		/// <summary>
		///   Gets or sets Tipo
		/// </summary>
		public string Tipo { get; set; }

		/// <summary>
		///   Gets or sets Vagao
		/// </summary>
		public string Vagao { get; set; }

		/// <summary>
		///   Gets or sets Origem
		/// </summary>
		public string Origem { get; set; }

        /// <summary>
        ///   Gets or sets Piscofins
        /// </summary>
        public double? Piscofins { get; set; }

        /// <summary>
        ///   Gets or sets Conta Contábil
        /// </summary>
        public ContaContabilSeguro ContaContabil { get; set; }

        /// <summary>
        ///   Gets or sets Sindicancia
        /// </summary>
        public long? Sindicancia { get; set; }
	}
}