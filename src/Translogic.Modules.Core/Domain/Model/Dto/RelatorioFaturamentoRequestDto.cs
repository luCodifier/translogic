﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RelatorioFaturamentoRequestDto
    {
        public DateTime DataInicio { get; set; }
        public DateTime DataFinal { get; set; }
        public string Fluxo { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string[] Vagoes { get; set; }
        public string Situacao { get; set; }
        public string Cliente { get; set; }
        public string Expeditor { get; set; }
        public string Segmento { get; set; }
        public string Prefixo { get; set; }
        public string Os { get; set; }
        public string Malha { get; set; }
        public string Uf { get; set; }
        public string Estacao { get; set; }
    }
}