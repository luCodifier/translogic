namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;

	/// <summary>
	/// Dto dos Despachos realizados para uma Nfe - Tela 1154
	/// </summary>
	public class DespachosNfeDto
	{
		/// <summary>
		/// C�digo do Vag�o
		/// </summary>
		public string CodigoVagao { get; set; }

		/// <summary>
		///  Fluxo Comercial
		/// </summary>
		public string FluxoComercial { get; set; }

		/// <summary>
		///  N�mero do Despacho
		/// </summary>
		public decimal NumeroDespacho { get; set; }

		/// <summary>
		///  S�rie do despacho
		/// </summary>
		public string SerieDespacho { get; set; }

		/// <summary>
		///  Data do Despacho
		/// </summary>
		public DateTime DataDespacho { get; set; }

		/// <summary>
		///  C�digo Conteriner
		/// </summary>
		public string Conteiner { get; set; }

		/// <summary>
		///  Chave do Cte
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// Origem do Cte
		/// </summary>
		public string Origem { get; set; }

		/// <summary>
		///  Destino do Cte
		/// </summary>
		public string Destino { get; set; }

        /// <summary>
        ///  Status do Cte
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        ///  PesoUtilizado do Cte
        /// </summary>
        public decimal PesoUtilizado { get; set; }
	}
}