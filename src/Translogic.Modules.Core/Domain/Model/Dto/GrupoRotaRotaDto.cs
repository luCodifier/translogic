﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    public class GrupoRotaRotaDto
    {
        public decimal IdGrupoRotaRota { get; set; }
        public decimal IdGrupo { get; set; }
        public string Grupo { get; set; }
        public decimal IdRota { get; set; }
        public string Rota { get; set; }
        public string Evento { get; set; }
        public DateTime VersionDate { get; set; }
        public string Usuario { get; set; }
    }
}