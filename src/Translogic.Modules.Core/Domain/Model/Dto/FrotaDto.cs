﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class FrotaDto
    {
        public decimal IdFrota { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}