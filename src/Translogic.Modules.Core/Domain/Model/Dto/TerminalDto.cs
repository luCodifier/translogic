﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class TerminalDto
    {
        public int IdTerminal { get; set; }
        public string DescricaoTerminal { get; set; }
        public string Tipo { get; set; }
    }
}