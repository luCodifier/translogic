﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class NotaFiscalPdfIdDto
    {
        /// <summary>
        /// ID da NF-e da tabela EDI.EDI2_NFE
        /// </summary>
        public int? IdNfeEdi { get; set; }

        /// <summary>
        /// ID da NF-e da tabela NFE_SIMCONSULTAS
        /// </summary>
        public int? IdNfeSimConsultas { get; set; }

        /// <summary>
        /// Chave da Nota Fiscal
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Pdf da Nota Fiscal
        /// </summary>
        public byte[] Pdf { get; set; }
    }
}