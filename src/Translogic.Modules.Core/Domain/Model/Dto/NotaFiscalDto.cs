﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Dto para obter dados de Nota Fiscal
    /// </summary>
    public class NotaFiscalDto
    {
        /// <summary>
        /// Id da Nota Fiscal
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// Tipo da Nota Fiscal
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Série da Nota Fiscal
        /// </summary>
        public string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Número da Nota Fiscal
        /// </summary>
        public string NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Peso total da NF
        /// </summary>
        public decimal PesoBruto { get; set; }

        /// <summary>
        /// Soma dos pesos rateio utilizados na NF 
        /// </summary>
        public decimal PesoUtilizado { get; set; }

        /// <summary>
        /// Chave da nfe da Nota Fiscal
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Peso rateado da Nota Fiscal
        /// </summary>
        public decimal PesoRateio { get; set; }

        /// <summary>
        /// Valor da Nota Fiscal
        /// </summary>
        public decimal ValorNotaFiscal { get; set; }

        /// <summary>
        /// Valor Total da Nota Fiscal
        /// </summary>
        public decimal ValorTotalNotaFiscal { get; set; }

        /// <summary>
        /// Peso total da Nota Fiscal
        /// </summary>
        public decimal PesoTotal { get; set; }

        /// <summary>
        /// Data da Nota Fiscal
        /// </summary>
        public DateTime DataEmissao { get; set; }

        /// <summary>
        /// Cnpj do emitente da Nota Fiscal
        /// </summary>
        public string CnpjEmitente { get; set; }

        /// <summary>
        /// Inscrição estadual do emitente da Nota Fiscal
        /// </summary>
        public string InscEstadualEmitente { get; set; }

        /// <summary>
        /// Sigla do estado do emitente da Nota Fiscal
        /// </summary>
        public string UfEmitente { get; set; }

        /// <summary>
        /// Razão Social do emitente da Nota Fiscal
        /// </summary>
        public string RazaoSocialEmitente { get; set; }

        /// <summary>
        /// Cnpj do destinatario da Nota Fiscal
        /// </summary>
        public string CnpjDestinatario { get; set; }

        /// <summary>
        /// Inscrição destinatario do emitente da Nota Fiscal
        /// </summary>
        public string InscEstadualDestinatario { get; set; }

        /// <summary>
        /// Sigla do estado do destinatario da Nota Fiscal
        /// </summary>
        public string UfDestinatario { get; set; }

        /// <summary>
        /// Razão Social do destinatario da Nota Fiscal
        /// </summary>
        public string RazaoSocialDestinatario { get; set; }

        /// <summary>
        /// Conteiner da Nota Fiscal
        /// </summary>
        public string Conteiner { get; set; }
    }
}