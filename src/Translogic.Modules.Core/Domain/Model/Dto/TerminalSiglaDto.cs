﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class TerminalSiglaDto
    {
        public string CodigoTerminal { get; set; }
        public string SiglaTerminal { get; set; }
    }
}