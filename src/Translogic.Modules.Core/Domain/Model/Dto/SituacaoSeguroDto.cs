﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class SituacaoSeguroDto
    {
        public string Id { get; set; } 
        public string Descricao { get; set; } 
    }
}