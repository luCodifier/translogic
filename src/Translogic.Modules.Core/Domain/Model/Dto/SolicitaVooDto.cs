﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SolicitaVooDto
    {
        public int TipoAtivo { get; set; }
        public string Ativos { get; set; }
        public string Patio { get; set; }
        public string Linha { get; set; }
        public string Os { get; set; }

        public string Result { get; set; }
    }
}