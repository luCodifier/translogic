﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SolicitacaoVooAtivoItemPkgDto
    {
        public Decimal IdAtivo { get; set; }
        public string Ativo { get; set; }
        public Decimal IdLocal { get; set; }
        public Decimal IdResponsavel { get; set; }
        public Decimal IdSituacao { get; set; }
        public Decimal IdLotacao { get; set; }
        public Decimal IdIntercambio { get; set; }
        public Decimal IdCondUso { get; set; }
        public Decimal IdPatioDestino { get; set; }
        public Decimal IdLinhaOrigem { get; set; }
        public Decimal IdPatioOrigem { get; set; }
        public Decimal IdLinhaDestino { get; set; }
        public Decimal? IdEstadoFuturo { get; set; }
        public string CodEstacao { get; set; }
        public Decimal? NumeroSequencia { get; set; }
    }
}