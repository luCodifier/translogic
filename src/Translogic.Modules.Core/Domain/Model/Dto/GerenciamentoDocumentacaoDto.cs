﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class GerenciamentoDocumentacaoDto
    {
        public decimal IdOs { get; set; }
        public decimal IdComposicao { get; set; }
        public decimal IdDespacho { get; set; }
        public decimal IdItemDespacho { get; set; }

        public string CodigoVagao { get; set; }
        public string CodigoFluxo { get; set; }

        public DateTime DataCarga { get; set; }
        public decimal Sequencia { get; set; }

        public string SegmentoFluxo { get; set; }

        public string Cliente { get; set; }

        public decimal? IdNfePdf { get; set; }

        public decimal? LaId { get; set; }
        public string LaClienteNome { get; set; }
        public string LaClienteCnpj { get; set; }
        public string LaClienteCnpjFormatado
        {
            get
            {
                var cnpj = LaClienteCnpj;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cnpj))
                {
                    cnpj = cnpj.Replace(".", String.Empty).Replace("//", String.Empty).Replace("-", String.Empty).PadLeft(14, '0');
                    resultado = String.Format("{0}.{1}.{2}//{3}-{4}", cnpj.Substring(0, 2), cnpj.Substring(2, 3), cnpj.Substring(5, 3), cnpj.Substring(8, 4), cnpj.Substring(12, 2));
                }

                return resultado;
            }
        }
        public DateTime? LaDataClassificacao { get; set; }
        public string LaNumeroLaudo { get; set; }
        public string LaNumeroOsLaudo { get; set; }
        public decimal? LaPesoLiquido { get; set; }
        public string LaDestino { get; set; }
        public string LaClassificadorNome { get; set; }
        public string LaClassificadorCpf { get; set; }
        public string LaClassificadorCpfFormatado
        {
            get
            {
                var cpf = LaClassificadorCpf;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cpf))
                {
                    cpf = cpf.Replace(".", String.Empty).Replace("-", String.Empty).PadLeft(11, '0');
                    resultado = String.Format("{0}.{1}.{2}-{3}", cpf.Substring(0, 3), cpf.Substring(3, 3), cpf.Substring(6, 3), cpf.Substring(9, 2));
                }

                return resultado;
            }
        }
        public string LaAutorizadoPor { get; set; }
        public string LaTipoLaudo { get; set; }
        public string LaEmpresaNome { get; set; }
        public string LaNumeroNfe { get; set; }
        public string LaChaveNfe { get; set; }

        public decimal? LaEmpresaGeradoraId { get; set; }
        public string LaEmpresaGeradoraNome { get; set; }
        public string LaEmpresaGeradoraCnpj { get; set; }
        public string LaEmpresaGeradoraEndereco { get; set; }
        public string LaEmpresaGeradoraCep { get; set; }
        public string LaEmpresaGeradoraCidadeDesc { get; set; }
        public string LaEmpresaGeradoraEstadoSigla { get; set; }
        public string LaEmpresaGeradoraIe { get; set; }
        public string LaEmpresaGeradoraTelefone { get; set; }
        public string LaEmpresaGeradoraFax { get; set; }

        public string LaProduto { get; set; }
        public string LaClassificacao { get; set; }
        public decimal? LaClassificacaoOrdem { get; set; }
        public decimal? LaPorcentagem { get; set; }
        public string LaDescricaoProduto { get; set; }

        public string ChaveNfe { get; set; }
        public decimal? Numero { get; set; }
        public decimal? Peso { get; set; }
        public decimal? PesoDisponivel { get; set; }
        public string Produto { get; set; }
        public string Serie { get; set; }
    }
}