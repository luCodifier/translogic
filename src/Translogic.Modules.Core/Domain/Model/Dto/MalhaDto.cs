﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MalhaDto
    {
        public int IdMalha { get; set; }
        public int Posicao { get; set; }
        public string Sigla { get; set; }
        public string DescricaoMalha { get; set; }
    }
}