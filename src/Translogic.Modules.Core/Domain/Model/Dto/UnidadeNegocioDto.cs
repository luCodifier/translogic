﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class UnidadeNegocioDto
    {
        public decimal Id { get; set; }
        public string Codigo { get; set; }
    }
}