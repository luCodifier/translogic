﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Classe de DTO para Trem
    /// </summary>
    public class TremDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public decimal Id { get; set; }
		
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Prefixo { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public decimal OS { get; set; }

        /// <summary>
        /// Gets or sets DataRealizadaPartida
        /// </summary>
        public DateTime DataRealizadaPartida { get; set; }

        /// <summary>
        /// Lista do Relatorio Ficha Recomendação
        /// </summary>
        public virtual IList<RelatorioFichaRecomendacaoExportDto> ListaRelatorioFichaRecomendacao { get; set; }
    }
}