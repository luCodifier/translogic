﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Interfaces.Trem.OrdemServico;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class LocalTipoServicoFornecedorDto
    {
        public decimal IdFornecedorOS { get; set; }
        public decimal IdLocalFornecedor { get; set; }
        public decimal IdTipoServico { get; set; }
        public string Local { get; set; }
        public string Servico { get; set; }        

    }
}