﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SolicitacaoVooAtivoItemDto
    {
        public Decimal Selecionavel { get; set; }
        public Decimal IdAtivo { get; set; }
        public string Ativo { get; set; }
        public string TipoAtivo { get; set; }
        public DateTime DataEvento { get; set; }
        public string PatioOrigem { get; set; }
        public string PatioDestino { get; set; }
        public string Situacao { get; set; }
        public string CondicaoUso { get; set; }
        public string Local { get; set; }
        public string Responsavel { get; set; }
        public string Lotacao { get; set; }
        public string Intercambio { get; set; }
        public string StatusItem { get; set; }
        public string EstadoFuturo { get; set; }
        public DateTime OlvTimestamp { get; set; }
        public DateTime dtDespacho { get; set; }
        
        public string Linha { get; set; }
        public string Serie { get; set; }
        public decimal Sequencia { get; set; }
        public string Bitola { get; set; }
    }
}