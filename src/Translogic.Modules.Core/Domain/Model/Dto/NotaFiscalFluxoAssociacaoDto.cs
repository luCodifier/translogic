﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;

    public class NotaFiscalFluxoAssociacaoDto
    {
        /// <summary>
        /// Chave da nfe da Nota Fiscal
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Código do Fluxo
        /// </summary>
        public string Fluxo { get; set; }

        /// <summary>
        /// Codigo da Estação de Origem
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Codigo da Estação de Destino
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Cnpj do Remetente Fiscal
        /// </summary>
        public string RemetenteFiscalCnpj { get; set; }

        /// <summary>
        /// Descrição do Remetente Fiscal
        /// </summary>
        public string RemetenteFiscal { get; set; }

        /// <summary>
        /// Cnpj do Destinatario Fiscal
        /// </summary>
        public string DestinatarioFiscalCnpj { get; set; }

        /// <summary>
        /// Descrição do Destinatario Fiscal
        /// </summary>
        public string DestinatarioFiscal { get; set; }

        /// <summary>
        /// Cnpj do Tomador
        /// </summary>
        public string TomadorCnpj { get; set; }

        /// <summary>
        /// Descrição do Tomador
        /// </summary>
        public string Tomador { get; set; }

        /// <summary>
        /// Descrição do Expedidor
        /// </summary>
        public string Expedidor { get; set; }

        /// <summary>
        /// Descrição do Recebedor
        /// </summary>
        public string Recebedor { get; set; }

        /// <summary>
        /// Descrição da Mercadoria
        /// </summary>
        public string Mercadoria { get; set; }

        /// <summary>
        /// Vigência Final do Fluxo
        /// </summary>
        public DateTime FluxoVigenciaFinal { get; set; }
    }
}