﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    public class CteVirtuaisSemVinculoMdfeDto
    {
        public decimal ID_MOVIMENTACAO { get; set; }
        public decimal ID_AREA_OPERACIONAL { get; set; }
        public decimal ID_TREM { get; set; }
        public string NUM_CTE_VIRTUAL { get; set; }
        public string SERIE_CTE_VIRTUAL { get; set; }
        public DateTime DATA_EMISSAO_VIRTUAL { get; set; }
        public decimal COMPOSICAO { get; set; }
        public string PREFIXO_TREM { get; set; }
        public decimal FLUXO { get; set; }
        public string COD_FLUXO { get; set; }
        public string VAGAO { get; set; }
        public decimal? ID_OS { get; set; }
        public decimal ID_DESPACHO { get; set; }
    }
}