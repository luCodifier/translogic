﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class LaudoMercadoriaDto
    {
        public decimal Id { get; set; }
        public decimal ItemDespachoId { get; set; }
        public string CodigoVagao { get; set; }
        public string ClienteNome { get; set; }
        public string ClienteCnpj { get; set; }
        public string ClienteCnpjFormatado
        {
            get
            {
                var cnpj = ClienteCnpj;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cnpj))
                {
                    cnpj = cnpj.Replace(".", String.Empty).Replace("//", String.Empty).Replace("-", String.Empty).PadLeft(14, '0');
                    resultado = String.Format("{0}.{1}.{2}//{3}-{4}", cnpj.Substring(0, 2), cnpj.Substring(2, 3), cnpj.Substring(5, 3), cnpj.Substring(8, 4), cnpj.Substring(12, 2));
                }

                return resultado;
            }
        }
        public DateTime DataClassificacao { get; set; }
        public string NumeroLaudo { get; set; }
        public string NumeroOsLaudo { get; set; }
        public decimal PesoLiquido { get; set; }
        public string Destino { get; set; }
        public string ClassificadorNome { get; set; }
        public string ClassificadorCpf { get; set; }
        public string ClassificadorCpfFormatado
        {
            get
            {
                var cpf = ClassificadorCpf;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cpf))
                {
                    cpf = cpf.Replace(".", String.Empty).Replace("-", String.Empty).PadLeft(11, '0');
                    resultado = String.Format("{0}.{1}.{2}-{3}", cpf.Substring(0, 3), cpf.Substring(3, 3), cpf.Substring(6, 3), cpf.Substring(9, 2));
                }

                return resultado;
            }
        }
        public string AutorizadoPor { get; set; }
        public string TipoLaudo { get; set; }
        public string EmpresaNome { get; set; }
        public string NumeroNfe { get; set; }
        public string ChaveNfe { get; set; }

        public decimal EmpresaGeradoraId { get; set; }
        public string EmpresaGeradoraNome { get; set; }
        public string EmpresaGeradoraCnpj { get; set; }
        public string EmpresaGeradoraEndereco { get; set; }
        public string EmpresaGeradoraCep { get; set; }
        public string EmpresaGeradoraCidadeDescricao { get; set; }
        public string EmpresaGeradoraEstadoSigla { get; set; }
        public string EmpresaGeradoraIe { get; set; }
        public string EmpresaGeradoraTelefone { get; set; }
        public string EmpresaGeradoraFax { get; set; }

        public string Produto { get; set; }
        public string Classificacao { get; set; }
        public decimal? ClassificacaoOrdem { get; set; }
        public decimal Porcentagem { get; set; }
        public string DescricaoProduto { get; set; }
    }
}