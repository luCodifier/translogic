﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class ConsultaPedraRealizadoDto_excluir
    {
        public decimal PedraRealizadoId { get; set; }
        
        public DateTime DataPedra { get; set; }
        public string Operacao { get; set; }
        public decimal OperacaoId { get; set; }
        public string Regiao { get; set; }
        public decimal RegiaoId { get; set; }
        public decimal FluxoId { get; set; }
        public decimal ReuniaoId { get; set; }
        public string EstacaoFaturamento { get; set; }
        public decimal EstacaoFaturamentoId { get; set; }
        public string EstacaoOrigem { get; set; }
        public decimal EstacaoOrigemId { get; set; }
        public string CodigoLinha { get; set; }
        public string Terminal { get; set; }
        public decimal TerminalId { get; set; }
        public string Segmento { get; set; }
        public decimal PedraSade { get; set; }
        public decimal Realizado { get; set; }
        public decimal PedraEditado { get; set; }
        public string StatusEditado { get; set; }
        public DateTime DataCadastro { get; set; }
        public decimal UsuarioCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }
        public decimal UsuarioAlteracao { get; set; }

        /// <summary>
        /// Pedra atual, caso não venha do SADE pega a editada pelo usuario se tiver
        /// </summary>
        public decimal Pedra
        {
            get
            {
                return PedraEditado > 0 ? PedraEditado : PedraSade;
            }
        }

        /// <summary>
        /// Saldo Calculado
        /// </summary>
        public decimal Saldo
        {
            get
            {
                return Realizado - Pedra;
            }
        }

        /// <summary>
        /// Mostra a porcentagem do que foi realizado, levando em consideração se for zero 
        /// a pedra ou o realizado irá exibir zero para não dar DIVIDE BY ZERO EXCEPTION
        /// </summary>
        public decimal Porcentagem
        {
            get
            {
                if (Realizado > 0 && Pedra > 0)
                    return Math.Round((Realizado / (PedraSade == 0 ? PedraEditado : PedraSade)) * 100);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Verifica se a Pedra foi Editada, mostrando uma cor diferente na tela do usuário
        /// </summary>
        public bool PedraEditadaFlag
        {
            get
            {
                return PedraEditado > 0;
            }
        }


    }
}