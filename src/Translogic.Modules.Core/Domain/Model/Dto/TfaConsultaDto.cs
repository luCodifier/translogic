﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// DTO de Consulta de TFA (Termo de Falta ou Avaria)
    /// </summary>
    public class TfaConsultaDto
    {
        public string NumTermo { get; set; }
        public decimal IdProcesso { get; set; }
        public decimal NumProcesso { get; set; }
        public DateTime? DataProcesso { get; set; }
        public DateTime? DataAnalise { get; set; }
        public DateTime? DataProvisao { get; set; }
        public DateTime? DataPagamento { get; set; }
        public DateTime? DataEncerrado { get; set; }
        public string Situacao { get; set; }
        public string UnidEnvolvida { get; set; }
        public decimal? OrdemServico { get; set; }
        public string ContaContabil { get; set; }
        public string Malha { get; set; }
        public decimal TfaAnexo { get; set; }
        public string StatusCiente { get; set; }
        public decimal PermiteExcluir { get; set; }
    }
}