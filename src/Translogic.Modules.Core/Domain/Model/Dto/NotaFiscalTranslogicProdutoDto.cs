﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class NotaFiscalTranslogicProdutoDto
    {
        public NotaFiscalTranslogic notaFiscalTranslogic { get; set; }

        public string unidadeMedidaProduto { get; set; }

        public double valorUnitarioProduto { get; set; }

        public double quantidadeProduto { get; set; }
    }
}