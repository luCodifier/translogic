﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica
{
    public class FormacaoAutoRequestDto
    {       
        public DateTime? dtInicial { get; set; }
        public DateTime? dtFinal { get; set; }
        public string StatusProcessamento { get; set; }
        public string OS { get; set; }
        public string TremPrefixo { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string Local { get; set; }
    }
}