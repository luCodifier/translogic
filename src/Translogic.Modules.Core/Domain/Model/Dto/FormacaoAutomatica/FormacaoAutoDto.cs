﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.FormacaoAutomatica
{
    public class FormacaoAutoDto
    {
        public decimal LfaId { get; set; }
        public string Operacao { get; set; }
        public decimal OS { get; set; }
        public string Trem { get; set; }
        public string LfaOrigem { get; set; }
        public string LfaDestino { get; set; }
        public string StatusProcessamento { get; set; }
        public string StatusLiberacao { get; set; }
        public decimal LfaTentativas { get; set; }
        public DateTime DtProximaExecucao { get; set; }
        public string Obs { get; set; }
        public decimal LIdMensagem { get; set; }
    }
}