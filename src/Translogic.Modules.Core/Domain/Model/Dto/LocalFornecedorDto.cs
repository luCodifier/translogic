﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class LocalFornecedorDto
    {
        public  FornecedorOsDto FornecedorOsDto { get; set; }
        public  TipoServicoDto TipoServicoDto { get; set; }
        public decimal idTipoServico { get; set; }
        public decimal IdFornecedorOs { get; set; }
        public decimal IdLocal { get; set; }
        public string Local { get; set; }
        public string Servico { get; set; }
        
    }
}