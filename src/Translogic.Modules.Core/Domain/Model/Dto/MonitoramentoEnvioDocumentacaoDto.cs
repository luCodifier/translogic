﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MonitoramentoEnvioDocumentacaoDto
    {
        public decimal DocumentacaoId { get; set; }
        public decimal OsNumero { get; set; }
        public string TremPrefixo { get; set; }
        public string TremEncerrado { get; set; }
        public string Enviado { get; set; }
        public string EstacaoEnvio { get; set; }
        public string Envio { get; set; }
        public DateTime? CriadoEm { get; set; }
        public DateTime? EnviadoEm { get; set; }
        public string EnvioManual { get; set; }
        public string UsuarioNome { get; set; }
        public string Recebedor { get; set; }
        public string DocumentacaoExcluida { get; set; }
        public string LinkDownload { get; set; }
        public string Refaturamento { get; set; }
        public string Recomposicao { get; set; }

        public DateTime? DataChegadaTrem { get; set; }
        public string PassouAreaParametrizada { get; set; }
        public decimal QuantidadeVagoes { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string Atual { get; set; }
        public string DocumentacaoCompleta { get; set; }
    }
}