﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using Diversos.Cte;

    /// <summary>
    /// Classe de DTO para Manutencao Cte
    /// </summary>
    public class ManutencaoCteDto
    {
        /// <summary>
        /// Gets or sets CteId
        /// </summary>
        public int CteId { get; set; }

        /// <summary>
        /// Id do despacho
        /// </summary>
        public int IdDespacho { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp5
        /// </summary>
        public string SerieDesp5 { get; set; }

        /// <summary>
        /// Gets or sets NumDesp5
        /// </summary>
        public int NumDesp5 { get; set; }

        /// <summary>
        /// Gets or sets CodigoControle
        /// </summary>
        public string CodigoControle { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp6
        /// </summary>
        public int SerieDesp6 { get; set; }

        /// <summary>
        /// Gets or sets NumDesp6
        /// </summary>
        public int NumDesp6 { get; set; }

        /// <summary>
        /// Gets or sets VagaoId
        /// </summary>
        public int VagaoId { get; set; }

        /// <summary>
        /// Gets or sets CodVagao
        /// </summary>
        public string CodVagao { get; set; }

        /// <summary>
        /// Gets or sets Fluxo
        /// </summary>
        public string Fluxo { get; set; }

        /// <summary>
        /// Gets or sets CodigoOrigem
        /// </summary>
        public string CodigoOrigem { get; set; }

        /// <summary>
        /// Gets or sets CodigoDestino
        /// </summary>
        public string CodigoDestino { get; set; }

        /// <summary>
        /// Gets or sets CodigoMercadoria
        /// </summary>
        public string CodigoMercadoria { get; set; }

        /// <summary>
        /// Gets or sets ToneladaUtil
        /// </summary>
        public double ToneladaUtil { get; set; }

        /// <summary>
        /// Gets or sets Volume
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Gets or sets NroCte
        /// </summary>
        public string NroCte { get; set; }

        /// <summary>
        /// Gets or sets SerieCte
        /// </summary>
        public string SerieCte { get; set; }

        /// <summary>
        /// Gets or sets ChaveCte
        /// </summary>
        public string ChaveCte { get; set; }

        /// <summary>
        /// Gets or sets DataEmissao
        /// </summary>
        public DateTime DataEmissao { get; set; }

        /// <summary>
		/// Gets or sets DataCancDespacho
        /// </summary>
		public DateTime? DataCancDespacho { get; set; }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        public StatusCteEnum Status { get; set; }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        public SituacaoCteEnum Situacao { get; set; }
        
        /// <summary>
        /// Gets or sets SituacaoCte como string
        /// </summary>
        public string SituacaoCte { get; set; }

        /// <summary>
        /// Gets or sets CteComAgrupamentoNaoCancelado
        /// </summary>
        public bool CteComAgrupamentoNaoCancelado { get; set; }

        /// <summary>
        /// Gets or sets IdNfeAnulacao
        /// </summary>
        public int? IdNfeAnulacao { get; set; }
        
        /// <summary>
        /// Gets or sets DataAnulacao
        /// </summary>
        public DateTime? DataAnulacao { get; set; }

        /// <summary>
        /// Gets or sets MotivoCte
        /// </summary>
        public string MotivoDescricao { get; set; }

        /// <summary>
        /// Gets or sets StatusRetornoDescricaoo
        /// </summary>
        public string StatusRetornoDescricao { get; set; }

        /// <summary>
        /// Status do CTE anulação
        /// </summary>
        public virtual SituacaoCteEnum SituacaoCteAnulacao { get; set; }

        /// <summary>
        /// Tipo de servico cte
        /// </summary>
        public int? TipoServicoCte { get; set; }

    }
}
