namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System.Collections.Generic;
	using System.Xml.Serialization;
	
	/// <summary>
	///  Classe RefatVagoesDto
	/// </summary>
	public class RefatVagoesDto
	{
		/// <summary>
		/// id do trem
		/// </summary>
		public virtual string IdTrem { get; set; }

		/// <summary>
		/// Id da composi��o
		/// </summary>
		public virtual string IdComposicao { get; set; }

		/// <summary>
		///  C�digo do Usu�rio
		/// </summary>
		public virtual string Usuario { get; set; }

        /// <summary>
        /// id do Motivo
        /// </summary>
        public virtual string IdMotivo { get; set; }

        /// <summary>
        /// Solicitante
        /// </summary>
        public virtual string Solicitante { get; set; }

        /// <summary>
        /// Justificativa
        /// </summary>
        public virtual string Justificativa { get; set; }

		/// <summary>
		/// lista de vag�es
		/// </summary>
		public virtual IList<RefatVagaoDto> ListaVagao { get; set; }
	}
}