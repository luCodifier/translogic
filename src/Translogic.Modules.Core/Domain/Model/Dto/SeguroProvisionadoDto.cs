﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SeguroProvisionadoDto
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
    }
}