﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MalhaEmitenteDto
    {
        /// <summary>
        /// Código da Malha
        /// </summary>
        public virtual string CodigoMalha { get; set; }

        /// <summary>
        /// Dominio
        /// </summary>
        public virtual string DominioEmitente { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public virtual string NomeEmitente { get; set; }

        /// <summary>
        /// CNPJ
        /// </summary>
        public virtual string CnpjEmitente { get; set; }

    }
}