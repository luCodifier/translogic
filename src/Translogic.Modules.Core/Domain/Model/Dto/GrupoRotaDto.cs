﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    public class GrupoRotaDto
    {
        public decimal IdGrupo { get; set; }
        public string Grupo { get; set; }
        public string Evento { get; set; }
        public DateTime VersionDate { get; set; }
        public string Usuario { get; set; }
        public decimal IdRota { get; set; }
        public string Rota { get; set; }
    }
}