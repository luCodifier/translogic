﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class NotaTicketVagaoClienteDto
    {
        public decimal NotaTicketVagaoId { get; set; }
        public string Cliente { get; set; }
        public decimal IdItemDespacho { get; set; }

        public NotaTicketVagaoClienteDto()
        { }

    }
}