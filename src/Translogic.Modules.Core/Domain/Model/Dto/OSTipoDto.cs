﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSTipoDto
    {
        public int IdTipo { get; set; }
        public string DescricaoTipo { get; set; }
    }
}