﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;

	/// <summary>
	/// Classe de DTO para consulta de Desempenho de Viagem Maquinista Instrutor
	/// </summary>
	public class ManutencaoDespachoDto
	{
		/// <summary>
		/// Gets or sets Id
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets Motivo
		/// </summary>
		public string Motivo { get; set; }

		/// <summary>
		/// Gets or sets Serie
		/// </summary>
		public string Serie { get; set; }

		/// <summary>
		/// Gets or sets Vagao
		/// </summary>
		public int Vagao { get; set; }

		/// <summary>
		/// Gets or sets Origem
		/// </summary>
		public string Origem { get; set; }

		/// <summary>
		/// Gets or sets OriInterc
		/// </summary>
		public string OriInterc { get; set; }

		/// <summary>
		/// Gets or sets Destino
		/// </summary>
		public string Destino { get; set; }

		/// <summary>
		/// Gets or sets SerDes
		/// </summary>
		public int SerDes { get; set; }

		/// <summary>
		/// Gets or sets NumDes
		/// </summary>
		public int NumDes { get; set; }

		/// <summary>
		/// Gets or sets Ser6
		/// </summary>
		public string Ser6 { get; set; }

		/// <summary>
		/// Gets or sets Num6
		/// </summary>
		public int Num6 { get; set; }

		/// <summary>
		/// Gets or sets DtDesp
		/// </summary>
		public DateTime DtDesp { get; set; }

		/// <summary>
		/// Gets or sets Mercadoria
		/// </summary>
		public string Mercadoria { get; set; }

		/// <summary>
		/// Gets or sets Fluxo
		/// </summary>
		public string Fluxo { get; set; }

		/// <summary>
		/// Gets or sets Tu
		/// </summary>
		public double Tu { get; set; }

		/// <summary>
		/// Gets or sets Volume
		/// </summary>
		public double Volume { get; set; }

		/// <summary>
		/// Gets or sets SerNot
		/// </summary>
		public int SerNot { get; set; }

		/// <summary>
		/// Gets or sets Nota
		/// </summary>
		public int Nota { get; set; }

		/// <summary>
		/// Gets or sets Chave
		/// </summary>
		public string Chave { get; set; }

		/// <summary>
		/// Gets or sets Peso
		/// </summary>
		public double Peso { get; set; }

		/// <summary>
		/// Gets or sets DtNota
		/// </summary>
		public DateTime DtNota { get; set; }

		/// <summary>
		/// Gets or sets Valor
		/// </summary>
		public double Valor { get; set; }

		/// <summary>
		/// Gets or sets Usuario
		/// </summary>
		public string Usuario { get; set; }

		/// <summary>
		/// Gets or sets Conteiner
		/// </summary>
		public string Conteiner { get; set; }
	}
}
