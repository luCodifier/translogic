﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// Classe DTO que retorna os dados de Mdfes com Erro para o semáforo
    /// </summary>
    public class SemaforoMdfeDto
    {
        /// <summary>
        /// Sigla da UF da Ferrovia
        /// </summary>
        public string Uf { get; set; }

        /// <summary>
        /// Quantidade de Mdfes com erro em menos de 12 horas
        /// </summary>
        public decimal QtdZeroDozeHoras { get; set; }

        /// <summary>
        /// Quantidade de Mdfes com erro em mais de 12 horas e menos de 24 horas
        /// </summary>
        public decimal QtdDozeVinteQuatroHoras { get; set; }

        /// <summary>
        /// Quantidade de Mdfes com erro em mais de 24 horas
        /// </summary>
        public decimal QtdMaisQueVinteQuatroHoras { get; set; }
    }
}