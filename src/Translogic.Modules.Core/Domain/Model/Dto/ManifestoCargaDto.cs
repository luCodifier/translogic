﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class ManifestoCargaDto
    {
        #region Dados do Trem

        public string TremPrefixo { get; set; }
        public string TremOrigem { get; set; }
        public string TremDestino { get; set; }
        public decimal OsNumero { get; set; }
        public decimal TotalVagoes { get; set; }

        #endregion Dados do Trem

        #region Dados do Recebedor

        public string RecebedorNome { get; set; }
        public string RecebedorCnpj { get; set; }
        public string RecebedorCnpjFormatado
        {
            get
            {
                var cnpjFormatado = RecebedorCnpj;
                if (string.IsNullOrEmpty(cnpjFormatado))
                {
                    return string.Empty;
                }

                cnpjFormatado = cnpjFormatado.PadLeft(14, '0');

                cnpjFormatado = string.Format("{0}.{1}.{2}/{3}-{4}",
                    cnpjFormatado.Substring(0, 2),
                    cnpjFormatado.Substring(2, 3),
                    cnpjFormatado.Substring(5, 3),
                    cnpjFormatado.Substring(8, 4),
                    cnpjFormatado.Substring(12, 2));

                return cnpjFormatado;
            }
        }

        #endregion

        #region Dados dos Vagões do Trem

        public string VagaoSerie { get; set; }
        public string VagaoCodigo { get; set; }
        public decimal VagaoSequencia { get; set; }
        public decimal PesoLiquidoVagao { get; set; }
        public decimal PesoTaraVagao { get; set; }
        public decimal PesoBrutoVagao { get; set; }

        public string Mercadoria { get; set; }
        public DateTime DataCarregamento { get; set; }

        public string NumeroCte { get; set; }

        #region Dados das Notas Fiscais do Vagão

        public string NotaFiscalSerie { get; set; }
        public string NotaFiscalNumero { get; set; }
        public DateTime NotaFiscalData { get; set; }
        public string NotaFiscalRemetente { get; set; }
        public string NotaFiscalDestinatario { get; set; }
        public decimal NotaFiscalPesoRateio { get; set; }
        public decimal NotaFiscalPesoTotal { get; set; }

        #endregion Dados das Notas Fiscais do Vagão

        #endregion Dados dos Vagões do Trem
    }
}