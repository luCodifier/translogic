﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class VagaoHistoricoTaraDto
    {
        [Display(Name = "Vagão")]
        public string CODVAGAO { get; set; }
        [Display(Name = "Série")]
        public string SERIE { get; set; }
        [Display(Name = "Data")]
        public DateTime DATA { get; set; }
        [Display(Name = "Tara Anterior")]
        public decimal TARAANTERIOR { get; set; }
        [Display(Name = "Tara Atual")]
        public decimal TARAATUAL { get; set; }
        [Display(Name = "Tara Mediana")]
        public decimal TARAMEDIANA { get; set; }
    }
}