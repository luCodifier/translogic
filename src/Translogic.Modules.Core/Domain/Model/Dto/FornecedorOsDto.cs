﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class FornecedorOsDto
    {
        public decimal IdFornecedorOs { get; set; }
        public string ConcaLocalServico { get; set; }
        public IList<LocalTipoServicoFornecedorDto> LocaisTiposServicos { get; set; }

        public string Nome { get; set; }
        public string Login { get; set; }
        public DateTime DtRegistro { get; set; }
        public bool? Ativo { get; set; }         
    }
}