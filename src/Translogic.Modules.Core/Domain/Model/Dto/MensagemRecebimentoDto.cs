﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using Translogic.Modules.Core.Domain.Model.Edi;

    public class MensagemRecebimentoDto
    {
        public virtual decimal Id { get; set; }
        public virtual string ArquivoMensagem { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual string Situacao { get; set; }
        public virtual string PendenciaNotas { get; set; }
        public virtual decimal ?EdiErroId { get; set; }
        public virtual bool HaPendenciaNotas
        {
            get { return (String.IsNullOrEmpty(PendenciaNotas) || PendenciaNotas == "N"); }
        }
    }
}