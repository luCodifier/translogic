﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSRevistamentoVagaoItemDto
    {
        #region Parâmetros de Entrada
        public decimal IdItem { get; set; } //ID_ITEM
        public decimal IdOs { get; set; } //ID_OS
        public decimal IdVagao { get; set; } //ID_VAGAO  
        public string Observacao { get; set; } //OBSERVACAO     
        public bool? Retirar { get; set; } //RETRABALHO
        public bool? Concluido { get; set; } //CONCLUIDO
        public string ConcluidoDescricao { get; set; }
        public string RetirarDescricao { get; set; }
        public bool? Comproblema { get; set; } //CONCLUIDO
        public string ComproblemaDescricao { get; set; }
        
        #endregion

        #region Parâmetros de Retorno

        public string NumeroVagao { get; set; }
        public string Local { get; set; }
        public string Serie { get; set; }
        public string Linha { get; set; }
        public decimal Sequencia { get; set; }
        public decimal IdLotacao { get; set; }
        public string DescricaoLotacao { get; set; }
        public decimal IdSituacao { get; set; }
        public string DescricaoSituacao { get; set; }
        public decimal IdCondicaoDeUso { get; set; }
        public string DescricaoCondicaoDeUso { get; set; }
        public decimal IdLocalizacao { get; set; }
        public string DescricaoLocalizacao { get; set; }

        public decimal? Marcado { get; set; } // Define se o vagão já está na OS

        #endregion

    }
}