﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    
    /// <summary>
    /// Classe Dto para os dados do vagao faturado
    /// </summary>
    public class VagaoTicketDto
    {
        /// <summary>
        /// Código do Vagão
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Peso Bruto do Vagão
        /// </summary>
        public virtual string PesoBruto { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual string PesoTara { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual string PesoLiquido { get; set; }

        /// <summary>
        /// Data de cadastro
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }


    }
}