﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class ToleranciaMercadoriaDto
    {
        public decimal Id { get; set; }
        public decimal Tolerancia { get; set; }
        public decimal IdMercadoria { get; set; }
        public string Mercadoria { get; set; }
        public string Usuario { get; set; }
        public DateTime VersionDate { get; set; }
    }
}