﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RelatorioFaturamentoExportDto
    {
        public string Vagao { get; set; } //VG_COD_VAG 9
        public string Serie { get; set; } //SV_COD_SER 3
        public string Segmento { get; set; } //FX_COD_SEGMENTO 9
        public DateTime Faturamento { get; set; } //DP_DT 19
        public string UFOrigem { get; set; } //ES_SGL_EST 2 
        public string Origem { get; set; } //AO_COD_AOP 3
        public string UFDestino { get; set; } //ES_SGL_EST 2
        public string Destino { get; set; } //AO_COD_AOP 3
        //public string TipoVagao { get; set; }
        public string Fluxo { get; set; } //FX_COD_FLX 9
        public string MD { get; set; } //MD 3
        public string NotaFiscal { get; set; } //NF_NRO_NF 7
        public decimal ValorNFCliente { get; set; } //NF_VAL_NFL 13 LEFT
        public string ChaveNFe { get; set; } //NFE_CHAVE_NFE 44
        public decimal Volume { get; set; } //NFE_VOLUME 9
        public decimal PesoRateio { get; set; } //NFE_PESO 9
        public decimal PesoTotal { get; set; } //NFE_VOLUME 9
        public decimal Tara { get; set; } //NVL(VG.VG_NUM_TRA, FE.FC_TAR) 8
        public decimal TB { get; set; } //NVL(VG.VG_NUM_TRA, FE.FC_TAR) + IDP.ID_NUM_PCL 9
        public string Cliente { get; set; } //CLIENTE_DSC 30
        public string Expedidor { get; set; } //EP_DSC_RSM 30
        public string Recebedor { get; set; } //EP_DSC_RSM 30
        public string RemetenteFiscal { get; set; } //EP_DSC_RSM 30
        public string Containers { get; set; } //NUM_CONTAINER 12
        public string Str363 { get; set; } //IDT_SIT 3
        public string Str1131 { get; set; } //VB_COD_TRA 3
        public string PrefixoTrem { get; set; } //TR_PFX_TRM 3
        public string OS { get; set; } //X1_NRO_OS 7
    }
}