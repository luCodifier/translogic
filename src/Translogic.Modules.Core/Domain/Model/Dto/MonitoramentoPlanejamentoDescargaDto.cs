﻿using System;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MonitoramentoPlanejamentoDescargaDto
    {
        public string Vagao { get; set; }
        public string Serie { get; set; }
        public DateTime DataSaidaEstacao { get; set; }
        public string StatusCte { get; set; }
        public Decimal NumeroCte { get; set; }
        public string ChaveCte { get; set; }
        public string Pdf { get; set; }
        public string Xml { get; set; }
        public string Estacao { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public decimal NumOS { get; set; }
        public string Prefixo { get; set; }
    }

    public class MonitoramentoDescarga
    {
        public string EmpresaAgrupada { get; set; }
        public string empCnpj { get; set; }
        public string empRazaoSocial { get; set; }
        public decimal empNumOS { get; set; }
        public string empPrefixo { get; set; }
        public string empEstacao { get; set; }
        public List<MonitoramentoPlanejamentoDescargaDto> MonitoramentoPlanejamentoDescarga { get; set; }
        public MonitoramentoDescarga()
        {
            MonitoramentoPlanejamentoDescarga = new List<MonitoramentoPlanejamentoDescargaDto>();
        }
    }
}