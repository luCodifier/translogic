﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSLimpezaVagaoDto
    {
        #region Parâmetros de Enrada
        public decimal IdOs { get; set; } //ID_OS
        public decimal IdOsParcial { get; set; } //ID_OS_PARCIAL
        public DateTime Data { get; set; } //DT_HR
        public int IdFornecedor { get; set; } //ID_FORNECEDOR
        public int idLocal { get; set; }//ID_LOCAL
        public int IdTipo { get; set; }//ID_TIPO_SERVICO
        public int IdStatus { get; set; }//ID_STATUS
        public DateTime DataHoraEntregaOSFornecedor { get; set; }//DT_HR_ENTR_FORN
        public DateTime DataHoraDevolucaoOS { get; set; }//DT_HR_DEVO_RUMO
        public string NomeAprovadorRumo { get; set; }//NOME_APROV_RUMO
        public string MatriculaAprovadorRumo { get; set; }//MATR_APROV_RUMO
        public string NomeAprovadorFornecedor { get; set; }//NOME_APROV_FORN
        public string MatriculaAprovadorFornecedor { get; set; }//MATR_APROV_FORN
        public string JustificativaCancelamento { get; set; }//JUST_CANCE
        public DateTime UltimaAlteracao { get; set; }//DT_ULT_ALT
        public string Usuario { get; set; }//USUARIO
        #endregion

        #region Parâmetros de Retorno
        public string Fornecedor { get; set; }
        public string LocalServico { get; set; }
        public string Tipo { get; set; }
        public string Status { get; set; }
        public decimal QtRetrabalho { get; set; }
        #endregion
    }
}