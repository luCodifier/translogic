﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class TipoServicoDto
    {
        public decimal IdTipoServico { get; set; }
        public string DescricaoServico { get; set; }
    }
}