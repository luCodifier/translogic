﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class SolicitacaoVooAtivoDto
    {
        public int Id { get; set; }
        public string Motivo { get; set; }
        public string Justificativa { get; set; }
        public DateTime dtHrChegada { get; set; }
        public string Solicitante { get; set; }
        public string AreaResponsavel { get; set; }
        public string JustificativaCCO { get; set; }
        public string ObservacaoCCO { get; set; }
        public int tipoAtivo { get; set; }
        public string PatioDestino { get; set; }
        public string PatioSolicitante { get; set; }
        public int Status { get; set; }
    }
}