namespace Translogic.Modules.Core.Domain.Model.Dto
{
	/// <summary>
	/// Classe RefatVagaoDto
	/// </summary>
	public class RefatVagaoDto
	{
		// <VAGAO ID="" ID_DESPACHO="" CONTRATO="" />

		/// <summary>
		/// Idenditicador do vag�o
		/// </summary>
		public virtual string IdVagao { get; set; }

		/// <summary>
		/// prefixo trem
		/// </summary>
		public virtual string IdDespacho { get; set; }

		/// <summary>
		/// prefixo trem
		/// </summary>
		public virtual string Contrato { get; set; }
	}
}