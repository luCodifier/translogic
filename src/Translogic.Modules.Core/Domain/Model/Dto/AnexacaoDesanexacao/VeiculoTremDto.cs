﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao
{
    public class VeiculoTremDto
    {
        public string VagaoLocomotiva { get; set; }
        public string StatusProcessamento { get; set; }
        public string Cad { get; set; }
        public string Pedido { get; set; }
        public string Localizacao { get; set; }
        public string Situacao { get; set; }
        public string Lotacao { get; set; }
        public string Recomendacao { get; set; }
        public string LocalParada { get; set; }
        public string Acao { get; set; }
        public string DescricaoErro { get; set; }
    }
}