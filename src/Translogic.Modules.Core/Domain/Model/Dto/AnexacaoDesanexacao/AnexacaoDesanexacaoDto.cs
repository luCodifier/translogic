﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao
{
    public class AnexacaoDesanexacaoDto
    {
        public string Operacao { get; set; }
        public decimal OS { get; set; }
        public string Trem { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string LocalParada { get; set; }
        public string StatusProcessamento { get; set; } 
        public string StatusParada { get; set; }
        public decimal Tentativas { get; set; }
        public DateTime dtProximaExecucao { get; set; }
        public string Obs { get; set; }

    }
}