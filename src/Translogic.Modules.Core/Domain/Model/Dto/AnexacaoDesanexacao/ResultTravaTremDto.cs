﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao
{
    public class ResultTravaTremDto
    {
        public string criticidade { get; set; }
        public string conformidade { get; set; }
        public string retornoPKGEncerrarParada { get; set; }
    }
}