namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Dto de vag�o do carregamento com dados de notas fiscais
    /// </summary>
    public class NotaFiscalVagaoCarregamentoDto
    {
        private DateTime? _dataNotaFiscal;

        /// <summary>
        /// Gets or sets CodigoVagao.
        /// </summary>
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets Conteiner.
        /// </summary>
        public string Conteiner { get; set; }

        /// <summary>
        /// Gets or sets IdVagao.
        /// </summary>
        public int IdVagao { get; set; }

        /// <summary>
        /// Gets or sets ChaveNfe.
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Gets or sets SerieNotaFiscal.
        /// </summary>
        public string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets NumeroNotaFiscal.
        /// </summary>
        public string NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets PesoTotal.
        /// </summary>
        public double? PesoTotal { get; set; }

        /// <summary>
        /// Gets or sets PesoRateio.
        /// </summary>
        public double? PesoRateio { get; set; }

        /// <summary>
        /// Gets or sets ValorNotaFiscal.
        /// </summary>
        public double ValorNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets ValorTotalNotaFiscal.
        /// </summary>
        public double ValorTotalNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets Remetente.
        /// </summary>
        public string Remetente { get; set; }

        /// <summary>
        /// Gets or sets Destinatario.
        /// </summary>
        public string Destinatario { get; set; }

        /// <summary>
        /// Gets or sets CnpjRemetente.
        /// </summary>
        public string CnpjRemetente { get; set; }

        /// <summary>
        /// Gets or sets CnpjDestinatario.
        /// </summary>
        public string CnpjDestinatario { get; set; }

        /// <summary>
        /// Gets or sets SiglaRemetente.
        /// </summary>
        public string SiglaRemetente { get; set; }

        /// <summary>
        /// Gets or sets SiglaDestinatario.
        /// </summary>
        public string SiglaDestinatario { get; set; }

        /// <summary>
        /// Gets or sets DataNotaFiscal.
        /// </summary>
        public string DataNotaFiscalString { get; set; }

        /// <summary>
        /// Gets DataNotaFiscal.
        /// </summary>
        public DateTime? DataNotaFiscal
        {
            get
            {
                return !string.IsNullOrEmpty(DataNotaFiscalString)
                         ? (DateTime?)DateTime.ParseExact(DataNotaFiscalString, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                         : null;
            }

            set 
            { 
                _dataNotaFiscal = value; 
            }
        }

        /// <summary>
        /// Gets or sets TIF.
        /// </summary>
        public string TIF { get; set; }

        /// <summary>
        /// Gets or sets UfRemetente.
        /// </summary>
        public string UfRemetente { get; set; }

        /// <summary>
        /// Gets or sets UfDestinatario.
        /// </summary>
        public string UfDestinatario { get; set; }

        /// <summary>
        /// Gets or sets InscricaoEstadualRemetente.
        /// </summary>
        public string InscricaoEstadualRemetente { get; set; }

        /// <summary>
        /// Gets or sets InscricaoEstadualDestinatario.
        /// </summary>
        public string InscricaoEstadualDestinatario { get; set; }

        /// <summary>
        /// Gets or sets TaraVagao.
        /// </summary>
        public double? TaraVagao { get; set; }

        /// <summary>
        /// Gets or sets VolumeVagao.
        /// </summary>
        public double? VolumeVagao { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether MultiploDespacho.
        /// </summary>
        public bool MultiploDespacho { get; set; }

        /// <summary>
        /// Gets or sets PesoTotalVagao.
        /// </summary>
        public double PesoTotalVagao { get; set; }

        /// <summary>
        /// Gets or sets PesoDclVagao.
        /// </summary>
        public double PesoDclVagao { get; set; }

        /// <summary>
        /// Gets or sets VolumeNotaFiscal.
        /// </summary>
        public double? VolumeNotaFiscal { get; set; }

        /// <summary>
        /// Gets or sets ChaveCte.
        /// </summary>
        public string ChaveCte { get; set; }
    }
}