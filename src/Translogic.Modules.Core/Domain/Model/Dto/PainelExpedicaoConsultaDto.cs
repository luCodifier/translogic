﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class PainelExpedicaoConsultaDto
    {
        public string TipoRegistro { get; set; }

        public string SerieVagao { get; set; }
        public string Vagao { get; set; }
        public string Lotacao { get; set; }
        public string Situacao { get; set; }

        public decimal PesoTara { get; set; }
        public decimal PesoRealCarregado { get; set; }
        public decimal PesoBruto { get; set; }

        public string PesoTu
        {
            get
            {
                var tu = PesoRealCarregado.ToString("N3");
                return tu;
            }
        }
        public string PesoTb
        {
            get
            {
                var tb = PesoBruto.ToString("N3");
                return tb;
            }
        }

        public string PesoTt
        {
            get
            {
                var tt = PesoTara.ToString("N3");
                return tt;
            }
        }

        public string Fluxo { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string TerminalFaturamento { get; set; }
        public string Cliente { get; set; }
        public string Mercadoria { get; set; }
        public string Container { get; set; }
        public string Local { get; set; }
        public string Trem { get; set; }
        public string Faturado { get; set; }
        public string FaturadoManual { get; set; }
        public string ErroFaturamento { get; set; }
        public string CteStatus { get; set; }
        public string Segmento { get; set; }
        public string Ticket { get; set; }
        public string MultiploDespacho { get; set; }
        public string Aprovador { get; set; }

        public DateTime? Data { get; set; }
        public DateTime? Data_Bo { get; set; }
        public DateTime? HorarioRecebimentoFaturamento { get; set; }
        public DateTime? HorarioRecebimentoArquivo { get; set; }
        public DateTime? HorarioFaturamento { get; set; }
        public DateTime? HorarioLiberacao1380 { get; set; }

        public string UsuarioEventoCarregamento { get; set; }

        public virtual string UsuarioMatricula
        {
            get
            {
                string matricula = "";
                if ((!String.IsNullOrEmpty(UsuarioEventoCarregamento)))
                {
                    if (UsuarioEventoCarregamento.IndexOf("-") >= 0)
                        matricula = UsuarioEventoCarregamento.Split('-')[0];
                }

                return matricula;
            }
        }

        public virtual string UsuarioNome
        {
            get
            {
                string nome = "";
                if ((!String.IsNullOrEmpty(UsuarioEventoCarregamento)))
                {
                    if (UsuarioEventoCarregamento.IndexOf("-") >= 0)
                        nome = UsuarioEventoCarregamento.Split('-')[1];

                }

                return nome.Trim();
            }
        }

        public PainelExpedicaoConsultaDto()
        {
        }
    }
}