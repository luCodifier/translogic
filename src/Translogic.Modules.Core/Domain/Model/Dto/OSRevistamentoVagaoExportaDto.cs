﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSRevistamentoVagaoExportaDto
    {
        public decimal IdOs { get; set; }
        public decimal IdOsParcial { get; set; } //ID_OS_PARCIAL
        public DateTime Data { get; set; } //DT_HR
        public string Local { get; set; }
        public string Fornecedor { get; set; }
        public DateTime? HoraInicio { get; set; } //DT_HR_INICIO
        public DateTime? HoraTermino { get; set; } //DT_HR_TERMINO
        public string NumeroVagao { get; set; }
        public decimal Retirar { get; set; } //RETRABALHO
        public decimal QtdeVagoesVedados { get; set; } //QTDE_VAGOES_VEDADOS
        public decimal QtdeVagoesGambitados { get; set; } //QTDE_VAGOES_GAMBITADOS
        public decimal ProblemaVedacao { get; set; } //CONCLUIDO
        public string Observacao { get; set; }
        public string NomeManutencao { get; set; }//NOME_MANUTENCAO
        public string Status { get; set; }
        public DateTime UltimaAlteracao { get; set; }
        public string Usuario { get; set; }
    }
}