﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    public class IntegracaoAppaDto
    {
        public decimal Id { get; set; }
        public decimal Lote { get; set; }
        public DateTime DataCriacao { get; set; }
        public string DataCriacaoString
        {
            get { return DataCriacao.ToString(); }
        }
        public DateTime? DataEnvio { get; set; }
        public string DataEnvioString
        {
            get
            {
                var dataEnvio = String.Empty;
                if (DataEnvio.HasValue)
                {
                    dataEnvio = DataEnvio.ToString();
                }

                return dataEnvio;
            }
        }
        public string SucessoEnvio { get; set; }
        public bool SucessoEnvioBool
        {
            get
            {
                var sucesso = false;
                if (!String.IsNullOrEmpty(SucessoEnvio))
                {
                    sucesso = SucessoEnvio == "S";
                }

                return sucesso;
            }
        }
        public string MensagemErro { get; set; }
        public string SucessoRetorno { get; set; }
        public bool SucessoRetornoBool
        {
            get
            {
                var sucesso = false;
                if (!String.IsNullOrEmpty(SucessoRetorno))
                {
                    sucesso = SucessoRetorno == "S";
                }

                return sucesso;
            }
        }
        public string MensagemRetorno { get; set; }
        public string AreaOperacionalEnvio { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string CodigoFluxoComercial { get; set; }
        public string PlacaVagao { get; set; }
        public DateTime DataPrevistaChegada { get; set; }
        public string DataPrevistaChegadaString
        {
            get { return DataPrevistaChegada.ToShortDateString(); }
        }
        public string PossuiXml { get; set; }
        public bool PossuiXmlBool
        {
            get 
            { 
                var possuiXml = false;
                if (!String.IsNullOrEmpty(PossuiXml))
                {
                    possuiXml = PossuiXml == "S";
                }

                return possuiXml;
            }
        }
        public string XmlEnviado { get; set; }
    }
}