namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using FluxosComerciais.Carregamentos;

	/// <summary>
	/// Classe de dto de carregamento
	/// </summary>
	public class CarregamentoDto
	{
		#region PREENCHIMENTO PELO CONTROLLER
		/// <summary>
		/// Gets or sets CodigoTransacao.
		/// </summary>
		public string CodigoTransacao { get; set; }
		#endregion

		#region PREENCHIMENTO PELA TELA
		/// <summary>
		/// Gets or sets IdFluxoComercial.
		/// </summary>
		public int IdFluxoComercial { get; set; }

		/// <summary>
		/// Gets or sets IdFluxoInternacional.
		/// </summary>
		public int? IdFluxoArgentina { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether MultiploDespacho.
		/// </summary>
		public bool DclPorVagao { get; set; }

		/// <summary>
		/// Gets or sets SerieIntercambio.
		/// </summary>
		public string SerieIntercambio { get; set; }

		/// <summary>
		/// Gets or sets NumeroIntercambio.
		/// </summary>
		public int? NumeroIntercambio { get; set; }

		/// <summary>
		/// Gets or sets IdFerroviaIntercambio.
		/// </summary>
		public int? IdFerroviaIntercambio { get; set; }

		/// <summary>
		/// Gets or sets RegimeIntercambio.
		/// </summary>
		public string RegimeIntercambio { get; set; }

		/// <summary>
		/// Gets or sets IdLinhaIntercambio.
		/// </summary>
		public int? IdLinhaIntercambio { get; set; }

		/// <summary>
		/// Gets or sets DataIntercambioString.
		/// </summary>
		public string DataIntercambioString { get; set; }

		/// <summary>
		/// Gets or sets DataCarregamentoString.
		/// </summary>
		public string DataCarregamentoString { get; set; }
		
		/// <summary>
		/// Log do tempo de carregamento
		/// </summary>
		public LogTempoCarregamento LogDespacho { get; set; }

		#endregion

		/// <summary>
		/// Gets DataIntercambio.
		/// </summary>
		public DateTime? DataIntercambio
		{
			get
			{
				return !string.IsNullOrEmpty(DataIntercambioString)
						   ? (DateTime?)DateTime.ParseExact(DataIntercambioString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)
						   : null;
			}
		}

		/// <summary>
		/// Gets DataDespacho.
		/// </summary>
		public DateTime? DataDespacho
		{
			get
			{
				return !string.IsNullOrEmpty(DataCarregamentoString)
						   ? (DateTime?)DateTime.ParseExact(DataCarregamentoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)
						   : null;
			}
		}

		/// <summary>
		/// Gets DataCarregamento.
		/// </summary>
		public DateTime? DataCarregamento
		{
			get
			{
				return !string.IsNullOrEmpty(DataCarregamentoString)
						   ? (DateTime?)DateTime.ParseExact(DataCarregamentoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)
						   : null;
			}
		}

		/// <summary>
		/// Gets or sets ListaNotasFiscais.
		/// </summary>
		public IList<NotaFiscalVagaoCarregamentoDto> ListaNotasFiscais { get; set; }

		#region CAMPOS DE PREENCHIMENTO AUTOM�TICO NO SERVI�O

		/// <summary>
		/// Gets or sets IdMercadoria.
		/// </summary>
		public int IdMercadoria { get; set; }

		/// <summary>
		/// Indica se a esta��o de origem interna � RPL.
		/// </summary>
		public bool IndRpl { get; set; }

		/// <summary>
		/// Gets or sets IdAreaOperacionalSede.
		/// </summary>
		public int IdAreaOperacionalSede { get; set; }

		/// <summary>
		/// Gets or sets IdAreaOperacionalDestino.
		/// </summary>
		public int IdAreaOperacionalDestino { get; set; }

		/// <summary>
		/// Id da empresa
		/// </summary>
		public int? IdEmpresa { get; set; }

		/// <summary>
		/// Id do grupo de fluxo
		/// </summary>
		/// <remarks> S� pode ser nulo quando o fluxo comercial for 99999</remarks>
		public int? IdGrupoFluxo { get; set; }
		#endregion
	}
}