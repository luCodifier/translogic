﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class PainelExpedicaoVisualizacaoDto
    {

        public int TempoRefreshMilisegundos { get; set; }

        public string Origem { get; set; }
        public string OrigemDescricao { get; set; }
        public string OrigemUf { get; set; }
        public decimal Faturado { get; set; }
        public decimal FaturadoManual { get; set; }
        public decimal PendenteFaturado { get; set; }
        public decimal CteAutorizado { get; set; }
        public decimal CtePendente { get; set; }
        public decimal PendenteConfirmacao { get; set; }
        public decimal TotalRecusadoEstacao { get; set; }

        public PainelExpedicaoVisualizacaoColunaDto PendenteConfirmacaoValorCor { get; set; }
        public PainelExpedicaoVisualizacaoColunaDto TotalRecusadoEstacaoValorCor { get; set; }
        public PainelExpedicaoVisualizacaoColunaDto FaturadoValorCor { get; set; }
        public PainelExpedicaoVisualizacaoColunaDto FaturadoManualValorCor { get; set; }
        public PainelExpedicaoVisualizacaoColunaDto CteAutorizadoValorCor { get; set; }

        public PainelExpedicaoVisualizacaoColunaDto PendenteFaturadoValorCor { get; set; }
        public PainelExpedicaoVisualizacaoColunaDto CtePendenteValorCor { get; set; }

        public PainelExpedicaoVisualizacaoDto()
        {
            this.PendenteConfirmacaoValorCor = new PainelExpedicaoVisualizacaoColunaDto();
            this.TotalRecusadoEstacaoValorCor = new PainelExpedicaoVisualizacaoColunaDto();
            this.FaturadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();
            this.FaturadoManualValorCor = new PainelExpedicaoVisualizacaoColunaDto();
            this.CteAutorizadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();

            this.PendenteFaturadoValorCor = new PainelExpedicaoVisualizacaoColunaDto();
            this.CtePendenteValorCor = new PainelExpedicaoVisualizacaoColunaDto();
        }

    }
}