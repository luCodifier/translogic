namespace Translogic.Modules.Core.Domain.Model.Dto
{
    /// <summary>
    /// Dados do vag�o que n�o est� em situa��o de carregamento ou fora do patio
    /// </summary>
    public class DadosVagaoForaCarregamentoDto
    {
        /// <summary>
        /// Gets or sets Situacao.
        /// </summary>
        public string Situacao { get; set; }

        /// <summary>
        /// Gets or sets PrefixoTrem.
        /// </summary>
        public string PrefixoTrem { get; set; }

        /// <summary>
        /// Gets or sets Patio.
        /// </summary>
        public string Patio { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Intercambio.
        /// </summary>
        public bool Intercambio { get; set; }
    }
}