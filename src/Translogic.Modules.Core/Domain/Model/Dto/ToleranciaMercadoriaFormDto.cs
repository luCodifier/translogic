﻿using System;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class ToleranciaMercadoriaFormDto
    {
        public decimal Id { get; set; }
        public decimal IdMercadoria { get; set; }
        public decimal Tolerancia { get; set; }
        public string Evento { get; set; }
    }
}