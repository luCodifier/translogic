﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class EmpresaResumidaDto
    {
        public string CodigoEmpresa { get; set; }
        public string DescricaoEmpresa { get; set; }
    }
}