namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using FluxosComerciais.ConhecimentoTransporteEletronico;

    /// <summary>
    /// Dto de armazenamento das Temps
    /// </summary>
    public class ManutencaoCteTempDto
    {
        /// <summary>
        /// Gets or sets CteTemp.
        /// </summary>
        public CteTemp CteTemp { get; set; }

        /// <summary>
        /// Gets or sets NovaChaveCte
        /// </summary>
        public string NovaChaveCte { get; set; }

        /// <summary>
        /// Gets or sets ListaDetalhes.
        /// </summary>
        public IList<CteTempDetalheViewModel> ListaDetalhes { get; set; }
    }

    /// <summary>
    /// Detalhe de CteTemp herdando de CteTempDetalhe
    /// </summary>
    public class CteTempDetalheViewModel : CteTempDetalhe
    {
        private string _dataNotaFiscalString;

        /// <summary>
        /// Data da nota Fiscal
        /// </summary>
        public string DataNotaFiscalString
        {
            get
            {
                return _dataNotaFiscalString;
            }

            set
            {
                _dataNotaFiscalString = value;
                DataNotaFiscal = DateTime.ParseExact(_dataNotaFiscalString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }
    }
}