﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class CausaSeguroDto 
    {
        public decimal IdCausa { get; set; }        
        public string Descricao { get; set; }
    }
}