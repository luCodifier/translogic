﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    /// <summary>
    /// Dto de Faturamentos de Uma Nfe
    /// </summary>
    public class FaturamentosNfeDto
    {
        /// <summary>
        /// Faturamentos Cte
        /// </summary>
        public decimal FaturamentosCte { get; set; }
        
        /// <summary>
        /// Faturamentos Fiscais
        /// </summary>
        public decimal FaturamentosFiscais { get; set; }
    }
}