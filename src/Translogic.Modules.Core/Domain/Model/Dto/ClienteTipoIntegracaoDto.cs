﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao.Enums;
using Translogic.Modules.Core.Util;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class ClienteTipoIntegracaoDto
    {
        public int Id { get; set; }
        public decimal IdCliente { get; set; } 
        public string EstacaoCodigo { get; set; }
        public string Estacao { get; set; }
        public string Cliente { get; set; }
        public TipoIntegracaoEnum TipoIntegracaoId { get; set; }
        public string TipoIntegracao
        {
            get
            {
                return Tools.Desc(this.TipoIntegracaoId);
            }
        }
    }
}