﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using Diversos.Cte;

    /// <summary>
    /// Classe de DTO para Cte
    /// </summary>
    public class CteDto
    {
        /// <summary>
        ///  Data Complemento
        /// </summary>
        private string _dataComp;

        /// <summary>
        /// Gets or sets CteId
        /// </summary>
        public int CteId { get; set; }

        /// <summary>
        /// Gets or sets CteId
        /// </summary>
        public decimal IdCte { get; set; }

        /// <summary>
        /// Gets or sets Fluxo
        /// </summary>
        public string Fluxo { get; set; }

        /// <summary>
        /// Gets or sets Origem
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Gets or sets Destino
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Gets or sets Mercadoria
        /// </summary>
        public string Mercadoria { get; set; }

        /// <summary>
        /// Gets or sets ClienteFatura
        /// </summary>
        public string ClienteFatura { get; set; }

        /// <summary>
        /// Gets or sets Cte
        /// </summary>
        public string Cte { get; set; }

        /// <summary>
        /// Serie do despacho
        /// </summary>
        public string SerieDespacho { get; set; }

        /// <summary>
        /// Numero do despacho
        /// </summary>
        public decimal NumeroDespacho { get; set; }

        /// <summary>
        /// Id do despacho
        /// </summary>
        public decimal IdDespacho { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp5
        /// </summary>
        public string SerieDesp5 { get; set; }

        /// <summary>
        /// Gets or sets NumDesp5
        /// </summary>
        public int NumDesp5 { get; set; }

        /// <summary>
        /// Gets or sets SerieDesp6
        /// </summary>
        public int SerieDesp6 { get; set; }

        /// <summary>
        /// Gets or sets NumDesp6
        /// </summary>
        public int NumDesp6 { get; set; }

        /// <summary>
        /// Gets or sets CodigoControle
        /// </summary>
        public string CodigoControle { get; set; }

        /// <summary>
        /// Numero do Cte
        /// </summary>
        public virtual string Numero { get; set; }

        /// <summary>
        /// Chave do Cte
        /// </summary>
        public virtual string Chave { get; set; }

        /// <summary>
        /// Gets or sets DataEmissao
        /// </summary>
        public DateTime DataEmissao { get; set; }

        /// <summary>
        /// Gets or Sets isComplementado
        /// </summary>
        public TipoCteEnum TipoCte { get; set; }

        /// <summary>
        /// Gets or sets Valor do CTE
        /// </summary>
        public double ValorCte { get; set; }

        /// <summary>
        /// Gets or sets Valor Diferenca Complemento
        /// </summary>
        public double ValorDiferencaComplemento { get; set; }

        /// <summary>
        /// Gets or sets Peso Diferenca Complemento
        /// </summary>
        public double PesoDiferencaComplemento { get; set; }

        /// <summary>
        /// Gets or sets Peso Vagão
        /// </summary>
        public double PesoVagao { get; set; }

        /// <summary>
        /// Gets or sets Peso Vagão
        /// </summary>
        public decimal PesoVagaoDec { get; set; }

        /// <summary>
        /// Gets or sets DataComplemento
        /// </summary>
        public DateTime? DataComplemento { get; set; }

        /// <summary>
        /// Gets or sets DataComplemento
        /// </summary>
        public string DataComp
        {
            get
            {
                return _dataComp;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    DataComplemento = DateTime.Parse(value);
                }

                _dataComp = value;
            }
        }

        /// <summary>
        /// Gets or sets CodigoUsuario
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Gets or sets IsImpresso
        /// </summary>
        public bool Impresso { get; set; }

        /// <summary>
        /// Gets or sets Ferrovia origem
        /// </summary>
        public string FerroviaOrigem { get; set; }

        /// <summary>
        /// Gets or sets Ferrovia destino
        /// </summary>
        public string FerroviaDestino { get; set; }

        /// <summary>
        /// Gets or sets UF Origem
        /// </summary>
        public string UfOrigem { get; set; }

        /// <summary>
        /// Gets or sets UF Destino
        /// </summary>
        public string UfDestino { get; set; }

        /// <summary>
        /// Gets or sets Situação Cte Enum
        /// </summary>
        public SituacaoCteEnum SituacaoCte { get; set; }

        /// <summary>
        /// Situacao da CTe em string
        /// </summary>
        public string SituacaoCteStr { get; set; }

        /// <summary>
        /// Gets or sets Código do erro Cte
        /// </summary>
        public string CodigoErro { get; set; }

        /// <summary>
        /// Gets or sets Status Cte
        /// </summary>
        public int StatusCte { get; set; }

        /// <summary>
        /// Gets or sets CteComAgrupamentoNaoAutorizado
        /// </summary>
        public bool CteComAgrupamentoNaoAutorizado { get; set; }

        /// <summary>
        /// Variavel auxiliar para setar o CteComAgrupamentoNaoAutorizado como bool
        /// </summary>
        public string CteAgrupamentoNaoAutorizado
        {
            get { return CteComAgrupamentoNaoAutorizado ? "S" : "N"; }
            set { CteComAgrupamentoNaoAutorizado = value == "N" ? false : true; }
        }

        /// <summary>
        /// Gets or sets Valor Total
        /// </summary>
        public double ValorTotal { get; set; }

        /// <summary>
        /// Gets or sets Fluxo Referencia
        /// </summary>
        public string FluxoReferencia { get; set; }

        /// <summary>
        /// Indicador se o arquivo pdf foi gerado
        /// </summary>
        public virtual bool ArquivoPdfGerado { get; set; }

        /// <summary>
        /// Indicador se o cte já está pago
        /// </summary>
        public virtual bool CtePago { get; set; }

        /// <summary>
        /// Variavel auxiliar para setar o CtePago como bool
        /// </summary>
        public string Pago
        {
            get { return CtePago ? "S" : "N"; }
            set { CtePago = value == "S" ? true : false; }
        }

        /// <summary>
        /// Variavel auxiliar para setar o ForaDoTempoCancelamento como bool
        /// </summary>
        public string ForaDoTempoCancelado
        {
            get { return ForaDoTempoCancelamento ? "S" : "N"; }
            set { ForaDoTempoCancelamento = value == "S" ? true : false; }
        }

        /// <summary>
        /// Indicador se o cte está fora da data de cancelamento (7 dias após autorização)
        /// </summary>
        public virtual bool ForaDoTempoCancelamento { get; set; }

        /// <summary>
        /// Gets or sets Ação a ser tomada
        /// </summary>
        public string AcaoSerTomada { get; set; }

        /// <summary>
        /// Gets or sets Se é um Cte Raiz
        /// </summary>
        public bool CteRaiz { get; set; }

        /// <summary>
        /// Gets or sets do Vagao
        /// </summary>
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets ReemitirCancelamento
        /// </summary>
        public long ReemitirCancelamento { get; set; }

        /// <summary>
        /// Gets or sets QtdeAutorizacoes
        /// </summary>
        public long QtdeAutorizacoes { get; set; }

        /// <summary>
        /// Gets or sets NFe Chave
        /// </summary>
        public string Nfe { get; set; }

        /// <summary>
        /// Gets or sets NFe Peso
        /// </summary>
        public double? PesoNfe { get; set; }

        /// <summary>
        /// Gets or sets NFe Peso Utilizadp
        /// </summary>
        public double? PesoUtilizadoNfe { get; set; }

        /// <summary>
        /// Gets or sets SerieCte
        /// </summary>
        public string SerieCte { get; set; }

        /// <summary>
        /// Aliquota do ICMS do CTe
        /// </summary>
        public virtual double? AliquotaIcmsCte { get; set; }

        /// <summary>
        /// Aliquota do ICMS do contrato atual
        /// </summary>
        public virtual double? AliquotaIcmsContratoAtual { get; set; }

        /// <summary>
        /// Id do Trem em uma composicao quando houver composicao do Cte
        /// </summary>
        public decimal IdTrem { get; set; }

        /// <summary>
        /// Id do Mdfe em uma composicao quando houver trem
        /// </summary>
        public decimal IdMdfe { get; set; }

        /// <summary>
        /// Numero da OS quando houver trem
        /// </summary>
        public decimal NumeroOs { get; set; }

        /// <summary>
        /// Data do carregamento quando houver
        /// </summary>
        public DateTime DataCarregamento { get; set; }

        /// <summary>
        /// Indica se houve baldeio para o vagão
        /// </summary>
        public string HouveBaldeio { get; set; }

        /// <summary>
        /// Variavel auxiliar para setar o ForaDoTempoCancelamento como bool
        /// </summary>
        public string Acima168horas
        {
            get { return Acima168hs ? "S" : "N"; }
            set { Acima168hs = value == "S" ? true : false; }
        }

        /// <summary>
        /// Indicador se o cte está fora da data de cancelamento (7 dias após autorização)
        /// </summary>
        public virtual bool Acima168hs { get; set; }

        /// <summary>
        /// Indicador auxiliar para setar o ForaDoTempoCancelamento como bool
        /// </summary>
        public string Descarregados
        {
            get { return Descarregado ? "S" : "N"; }
            set { Descarregado = value == "S" ? true : false; }
        }

        /// <summary>
        /// Indicada se o vagão está descarregado
        /// </summary>
        public virtual bool Descarregado { get; set; }

        /// <summary>
        /// Indicador auxiliar para o Identificar envio para o sefaz.
        /// </summary>
        public virtual bool? PermiteCancRejeitado { get; set; }

        /// <summary>
        ///    Data de envio da anulacao para o sistema da SEFAZ
        /// </summary>    
        public virtual string DataAnulacao { get; set; }

        /// <summary>
        /// Indicador se o cte é cte com fluxo de rateio.
        /// </summary>
        public virtual bool RateioCte { get; set; }

        /// <summary>
        /// Indicador se o cte é de múltiplo despacho.
        /// </summary>
        public virtual string IndMultiploDespacho { get; set; }

        /// <summary>
        /// Indicativo do tomador do CTE
        /// </summary>
        public virtual int? IndIeToma { get; set; }

        /// <summary>
        /// Qtde do Cte
        /// </summary>
        public virtual double? Qtde { get; set; }

        /// <summary>
        /// Descricao do tomador do CTE
        /// </summary>
        public virtual string PapelTomador
        {
            get
            {
                if (IndIeToma == null)
                {
                    return "Não definido";
                }
                if (IndIeToma == 1)
                {
                    return "1-Contribuinte ICMS";
                }
                if (IndIeToma == 2)
                {
                    return "Contribuinte isento de inscrição";
                }
                if (IndIeToma == 9)
                {
                    return "9-Não Contribuinte";
                }
                return "Inválido";
            }
        }

        /// <summary>
        /// Status do CTE anulação
        /// </summary>
        public virtual SituacaoCteEnum SituacaoCteAnulacao { get; set; }
    }
}
