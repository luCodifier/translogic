﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class AreaOpDto
    {
        public decimal IdTerminal { get; set; }
        public string CodTerminal { get; set; }
    }
}