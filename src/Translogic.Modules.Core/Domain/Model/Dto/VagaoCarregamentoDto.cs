namespace Translogic.Modules.Core.Domain.Model.Dto
{
	/// <summary>
	/// Dto do vag�o para carregamento
	/// </summary>
	public class VagaoCarregamentoDto
	{
		/// <summary>
		/// Gets or sets IdVagao.
		/// </summary>
		public int IdVagao { get; set; }

		/// <summary>
		/// Gets or sets SerieVagao.
		/// </summary>
		public string SerieVagao { get; set; }

		/// <summary>
		/// Gets or sets CodigoVagao.
		/// </summary>
		public string CodigoVagao { get; set; }

		/// <summary>
		/// Gets or sets Linha.
		/// </summary>
		public string Linha { get; set; }

		/// <summary>
		/// Gets or sets Sequencia.
		/// </summary>
		public int? Sequencia { get; set; }

		/// <summary>
		/// Gets a value indicating whether IndCarregadoMesmoFluxo.
		/// </summary>
		public bool IndCarregadoMesmoFluxo
		{
			get { return QuantidadeCarregadoMesmoFluxo > 0; }
		}

		/// <summary>
		/// Gets a value indicating whether IndCarregadoFluxoNaoBrado.
		/// </summary>
		public bool IndCarregadoFluxoNaoBrado
		{
			get { return (QuantidadeCarregado - QuantidadeCarregadoFluxoBrado) > 0; }
		}

		/// <summary>
		/// Gets or sets QuantidadeCarregadoMesmoFluxo.
		/// </summary>
		public long QuantidadeCarregadoMesmoFluxo { get; set; }

		/// <summary>
		/// Gets or sets QuantidadeCarregado.
		/// </summary>
		public long QuantidadeCarregado { get; set; }

		/// <summary>
		/// Gets or sets QuantidadeCarregadoFluxoBrado.
		/// </summary>
		public long QuantidadeCarregadoFluxoBrado { get; set; }
	}
}