﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class RelatorioFichaRecomendacaoDto
    {
        /// <summary>
        /// Numero OS
        /// </summary>
        public decimal NumOs { get; set; }
        /// <summary>
        /// Prefixo
        /// </summary>
        public string Prefixo { get; set; }
        /// <summary>
        /// Origem
        /// </summary>
        public string Origem { get; set; }
        /// <summary>
        /// Destino
        /// </summary>
        public string Destino { get; set; }
        /// <summary>
        /// Local
        /// </summary>
        public string Local { get; set; }
        /// <summary>
        /// Data Chegada Prevista
        /// </summary>
        public DateTime DtChegadaPrevista { get; set; }
        /// <summary>
        /// Quantidade Avarivas
        /// </summary>
        public decimal QtdAvaria { get; set; }
        /// <summary>
        /// Quantidade Vagões
        /// </summary>
        public decimal QtdVagao { get; set; }
    }
}
