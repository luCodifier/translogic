﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using FluxosComerciais.Nfes;

	/// <summary>
	/// Dto de status da nfe
	/// </summary>
	public class StatusNfeDto
	{
		/// <summary>
		/// Peso da Nota fiscal
		/// </summary>
		public double? Peso { get; set; }

		/// <summary>
		/// Peso utilizado
		/// </summary>
		public double? PesoUtilizado { get; set; }

		/// <summary>
		/// Volume da nota fiscal
		/// </summary>
		public double? Volume { get; set; }

		/// <summary>
		/// Volume utilizado
		/// </summary>
		public double? VolumeUtilizado { get; set; }

		/// <summary>
		/// Código do status de retorno
		/// </summary>
		public string CodigoStatusRetorno { get; set; }

		/// <summary>
		/// Indica se libera a digitação
		/// </summary>
		public bool IndLiberaDigitacao { get; set; }

		/// <summary>
		/// Indica se permite reenvio
		/// </summary>
		public bool IndPermiteReenvio { get; set; }

		/// <summary>
		/// Stack trace
		/// </summary>
		public string StackTrace { get; set; }

		/// <summary>
		/// Mensagem de Erro
		/// </summary>
		public string MensagemErro { get; set; }

		/// <summary>
		/// Nota fiscal eletronica
		/// </summary>
		public INotaFiscalEletronica NotaFiscalEletronica { get; set; }

		/// <summary>
		/// Indica se foi obtida com sucesso
		/// </summary>
		public bool ObtidaComSucesso { get; set; }

        /// <summary>
        /// Peso Bruto da Nota
        /// </summary>
        public virtual double? PesoBruto { get; set; }
	}
}