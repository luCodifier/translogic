﻿
namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;

    [Serializable()]
    public class AnexoLaudoDto
    {
        public string NumProcesso { get; set; }
        public string Status { get; set; }
    }
}