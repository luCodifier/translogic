﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class MotivoSituacaoVagaoMotivoDto {

        public decimal IdSituacao { get; set; }
        public decimal IdMotivo { get; set; }
        public string DescricaoMotivo { get; set; }              
    }
}