namespace Translogic.Modules.Core.Domain.Model.Dto
{
	using System;
	using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
	
	/// <summary>
	/// Classe Dto para o Mdfe
	/// </summary>
	public class MdfeDto
	{
		/// <summary>
		/// Gets or sets Id
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets Chave
		/// </summary>
		public string Chave { get; set; }

        /// <summary>
        /// Gets or sets Serie
        /// </summary>
        public string Serie { get; set; }

		/// <summary>
        /// Gets or sets Numero
		/// </summary>
        public string Numero { get; set; }

		/// <summary>
		/// Gets or sets SituacaoAtual
		/// </summary>
		public SituacaoMdfeEnum SituacaoAtual { get; set; }

		/// <summary>
		/// Gets or sets DataHora
		/// </summary>
		public DateTime DataHora { get; set; }

		/// <summary>
		/// Gets or sets PdfGerado
		/// </summary>
		public string PdfGerado { get; set; }

        /// <summary>
        /// Id do trem da Mdfe
        /// </summary>
        public int TremId { get; set; }

        /// <summary>
        /// 1 = data de recebimento menor que 24h
        /// 0 = data de recebimento Maior que 24h, somente grupo fiscal pode cancelar 
        /// </summary>
        public int IndAnulacao { get; set; }
	}
}