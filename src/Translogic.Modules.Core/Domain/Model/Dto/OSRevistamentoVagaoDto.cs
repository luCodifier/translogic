﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Dto
{
    public class OSRevistamentoVagaoDto
    {
        #region Parâmetros de Enrada
        public decimal IdOs { get; set; } //ID_OS
        public decimal IdOsParcial { get; set; } //ID_OS_PARCIAL
        public DateTime Data { get; set; } //DT_HR
        public DateTime? HoraInicio { get; set; } //DT_HR_INICIO
        public DateTime? HoraTermino { get; set; } //DT_HR_TERMINO
        public DateTime? HoraEntrega { get; set; } //DT_HR_ENTREGA
        public string Linha { get; set; } //LINHA
        public string Convocacao { get; set; } //LINHA
        public decimal QtdeVagoes { get; set; } //QTDE_VAGOES
        public decimal QtdeVagoesVedados { get; set; } //QTDE_VAGOES_VEDADOS
        public decimal QtdeVagoesGambitados { get; set; } //QTDE_VAGOES_GAMBITADOS
        public decimal QtdeVagoesVazios { get; set; } //QTDE_ENTRE_VAZIOS
        public decimal IdFornecedor { get; set; } //ID_FORNECEDOR
        public decimal idLocal { get; set; }//ID_LOCAL
        public decimal IdTipo { get; set; }//ID_TIPO_SERVICO
        public decimal IdStatus { get; set; }//ID_STATUS
        public string NomeManutencao { get; set; }//NOME_MANUTENCAO
        public string MatriculaManutencao { get; set; }//MATRICULA_MANUTENCAO
        public string NomeEstacao { get; set; }//NOME_ESTACAO
        public string MatriculaEstacao { get; set; }//MATRICULA_ESTACAO
        public string JustificativaCancelamento { get; set; }//JUST_CANCE
        public DateTime UltimaAlteracao { get; set; }//DT_ULT_ALT
        public string Usuario { get; set; }//USUARIO
        
        #endregion

        #region Parâmetros de Retorno

        public string Fornecedor { get; set; }
        public string LocalServico { get; set; }
        public string Tipo { get; set; }
        public string Status { get; set; }
        public string VagaoRetirado { get; set; }
        public string ProblemaVedacao { get; set; }
        public string CarregadoEntreVazios { get; set; }        
      
        #endregion

    }
}