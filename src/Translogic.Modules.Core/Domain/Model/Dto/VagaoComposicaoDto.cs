﻿namespace Translogic.Modules.Core.Domain.Model.Dto
{
    using System;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.Diversos.Mdfe;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

    /// <summary>
    /// Classe Dto para os dados do vagao faturado
    /// </summary>
    public class DadosVagaoComposicaoDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Id { get; set; }

        /// <summary>
        /// Gets or sets Id do item de despacho
        /// </summary>
        public virtual decimal IdItemDespacho { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Sequencia { get; set; }
        
        /// <summary>
        /// Gets or sets SerieVagao
        /// </summary>
        public virtual string SerieVagao { get; set; }

        /// <summary>
        /// Gets or sets CodigoVagao
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets Origem
        /// </summary>
        public virtual string Origem { get; set; }

        /// <summary>
        /// Gets or sets Destino
        /// </summary>
        public virtual string Destino { get; set; }

        /// <summary>
        /// Gets or sets Mercadoria
        /// </summary>
        public virtual string Mercadoria { get; set; }

        /// <summary>
        /// Gets or sets TU
        /// </summary>
        public virtual decimal TU { get; set; }

        /// <summary>
        /// Gets or sets Tara
        /// </summary>
        public virtual decimal Tara { get; set; }

        /// <summary>
        /// Gets or sets Correntista
        /// </summary>
        public virtual string Correntista { get; set; }

        /// <summary>
        /// Gets or sets Destinatario
        /// </summary>
        public virtual string Destinatario { get; set; }

        /// <summary>
        /// Gets or sets DataCarregamento
        /// </summary>
        public virtual DateTime? DataCarregamento { get; set; }

        /// <summary>
        /// Gets or sets DataDescarregamento
        /// </summary>
        public virtual DateTime? DataDescarregamento { get; set; }

        /// <summary>
        /// Gets or sets SerieNf
        /// </summary>
        public virtual string SerieNf { get; set; }

        /// <summary>
        /// Gets or sets NumeroNf
        /// </summary>
        public virtual string NumeroNf { get; set; }

        /// <summary>
        /// Gets or sets DataNf
        /// </summary>
        public virtual DateTime DataNf { get; set; }

        /// <summary>
        /// Gets or sets PesoNf
        /// </summary>
        public virtual decimal PesoNf { get; set; }

        /// <summary>
        /// Gets or sets PesoRateioNf
        /// </summary>
        public virtual decimal PesoRateioNf { get; set; }

        /// <summary>
        /// Gets or sets ValorNf
        /// </summary>
        public virtual decimal ValorNf { get; set; }

        /// <summary>
        /// Gets or sets ChaveNfe
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Gets or sets Linha
        /// </summary>
        public virtual string Linha { get; set; }

        /// <summary>
        /// Gets or sets PossuiTicket
        /// </summary>
        public virtual decimal PossuiTicket { get; set; }
    }

    /// <summary>
    /// Classe Dto para o vagao faturado
    /// </summary>
    public class VagaoComposicaoDto
    {
        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Id { get; set; }

        /// <summary>
        /// Gets or sets Id do Item de Despacho
        /// </summary>
        public virtual decimal IdItemDespacho { get; set; }

        /// <summary>
        /// Gets or sets Id
        /// </summary>
        public virtual decimal Sequencia { get; set; }

        /// <summary>
        /// Gets or sets SerieVagao
        /// </summary>
        public virtual string SerieVagao { get; set; }

        /// <summary>
        /// Gets or sets CodigoVagao
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Gets or sets Origem
        /// </summary>
        public virtual string Origem { get; set; }

        /// <summary>
        /// Gets or sets Destino
        /// </summary>
        public virtual string Destino { get; set; }

        /// <summary>
        /// Gets or sets Mercadoria
        /// </summary>
        public virtual string Mercadoria { get; set; }

        /// <summary>
        /// Gets or sets TU
        /// </summary>
        public virtual decimal TU { get; set; }

        /// <summary>
        /// Gets or sets Tara
        /// </summary>
        public virtual decimal Tara { get; set; }

        /// <summary>
        /// Gets or sets Correntista
        /// </summary>
        public virtual string Correntista { get; set; }

        /// <summary>
        /// Gets or sets Destinatario
        /// </summary>
        public virtual string Destinatario { get; set; }

        /// <summary>
        /// Gets or sets DataCarregamento
        /// </summary>
        public virtual DateTime? DataCarregamento { get; set; }

        /// <summary>
        /// Gets or sets DataDescarregamento
        /// </summary>
        public virtual DateTime? DataDescarregamento { get; set; }

        /// <summary>
        /// Gets or sets Notas
        /// </summary>
        public virtual IList<NotaVagaoDto> Notas { get; set; }

        /// <summary>
        /// Gets or sets Linha
        /// </summary>
        public virtual string Linha { get; set; }

        /// <summary>
        /// Gets or sets PossuiTicket
        /// </summary>
        public virtual bool PossuiTicket { get; set; }
    }
    
    /// <summary>
    /// Classe Dto para as notas do vagao
    /// </summary>
    public class NotaVagaoDto
    {
        /// <summary>
        /// Gets or sets SerieNf
        /// </summary>
        public virtual string SerieNf { get; set; }

        /// <summary>
        /// Gets or sets NumeroNf
        /// </summary>
        public virtual string NumeroNf { get; set; }

        /// <summary>
        /// Gets or sets DataNf
        /// </summary>
        public virtual DateTime DataNf { get; set; }

        /// <summary>
        /// Gets or sets PesoNf
        /// </summary>
        public virtual decimal PesoNf { get; set; }

        /// <summary>
        /// Gets or sets PesoRateioNf
        /// </summary>
        public virtual decimal PesoRateioNf { get; set; }

        /// <summary>
        /// Gets or sets ValorNf
        /// </summary>
        public virtual decimal ValorNf { get; set; }

        /// <summary>
        /// Gets or sets ChaveNfe
        /// </summary>
        public virtual string ChaveNfe { get; set; }
    }
}