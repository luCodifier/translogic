﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.Dominio;

    public class HistoricoEnvioDocumentacao : EntidadeBase<int>
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        public HistoricoEnvioDocumentacao()
        {
        }

        /// <summary>
        /// Construtor com valores definidos
        /// </summary>
        /// <param name="documentacao">Dados da documentação</param>
        /// <param name="osId">ID da OS</param>
        /// <param name="composicaoId">ID da composição</param>
        /// <param name="recebedorId">ID do recebedor</param>
        /// <param name="recomposicao">Indicador se o e-mail é de recomposição</param>
        /// <param name="manual">Indicador se o e-mail foi ativado manualmente ou pelo serviço de e-mails</param>
        /// <param name="usuarioId">ID do usuário que solicitou o reenvio do e-mail</param>
        /// <param name="chegada">Indicador se o e-mail foi ativado pela chegada do trem na estação</param>
        /// <param name="saida">Indicador se o e-mail foi ativado pela saída do trem na estação</param>
        /// <param name="dataChegada">Indicador da hora de chegada do trem na estação</param>
        /// <param name="emails">Lista de e-mails a serem enviados os e-mails</param>
        public HistoricoEnvioDocumentacao(GerenciamentoDocumentacao documentacao,
                                          decimal osId,
                                          decimal composicaoId,
                                          decimal recebedorId,
                                          string recomposicao,
                                          string manual,
                                          decimal? usuarioId,
                                          string chegada,
                                          string saida,
                                          DateTime? dataChegada,
                                          IEnumerable<string> emails)
        {
            this.Documentacao = documentacao;
            this.OsId = osId;
            this.ComposicaoId = composicaoId;
            this.RecebedorId = recebedorId;
            this.Recomposicao = string.IsNullOrEmpty(recomposicao) ? "N" : recomposicao;

            var historicoEmailsEnvioDocumentacao = new List<HistoricoEmailEnvioDocumentacao>();
            emails.ToList().ForEach(email =>
            {
                var e = email.Replace("ç", "c");
                var historicoEmailEnvioDocumentacao = new HistoricoEmailEnvioDocumentacao(e);
                historicoEmailEnvioDocumentacao.HistoricoEnvioDocumentacao = this;
                historicoEmailsEnvioDocumentacao.Add(historicoEmailEnvioDocumentacao);
            });

            this.Emails = historicoEmailsEnvioDocumentacao;
            this.Enviado = "N";
            this.Manual = string.IsNullOrEmpty(manual) ? "N" : manual;
            this.UsuarioReenvioId = usuarioId;

            this.Chegada = string.IsNullOrEmpty(chegada) ? "N" : chegada;
            this.Saida = string.IsNullOrEmpty(saida) ? "S" : saida;
            this.DataChegada = dataChegada;

            this.CriadoEm = DateTime.Now;
        }

        /// <summary>
        /// Documentação que o e-mail representa
        /// </summary>
        public virtual GerenciamentoDocumentacao Documentacao { get; set; }

        /// <summary>
        /// Lista de emails enviados
        /// </summary>
        public virtual ICollection<HistoricoEmailEnvioDocumentacao> Emails { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal OsId { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal ComposicaoId { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal RecebedorId { get; set; }

        /// <summary>
        /// Indicador se o e-mail é de recomposicao ou não
        /// </summary>
        public virtual string Recomposicao { get; set; }

        /// <summary>
        /// Se o registro foi processado pelo serviço
        /// </summary>
        public virtual string Enviado { get; set; }

        /// <summary>
        /// Indicador se o e-mail foi disparado de forma manual
        /// </summary>
        public virtual string Manual { get; set; }

        /// <summary>
        /// Id do usuário que clicou no botão de reenviar a documentação
        /// </summary>
        public virtual decimal? UsuarioReenvioId { get; set; }

        /// <summary>
        /// Indicador se o registro é de chegada na estação parametrizada
        /// </summary>
        public virtual string Chegada { get; set; }

        /// <summary>
        /// Indicador se o registro é de saída da estação parametrizada
        /// </summary>
        public virtual string Saida { get; set; }
        
        /// <summary>
        /// Caso for chegada, conterá a data de chegada do trem na estação parametrizada
        /// </summary>
        public virtual DateTime? DataChegada { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}