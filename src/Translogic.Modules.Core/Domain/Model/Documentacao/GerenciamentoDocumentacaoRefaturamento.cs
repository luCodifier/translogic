﻿using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;

    public class GerenciamentoDocumentacaoRefaturamento : EntidadeBase<int>
    {
        /// <summary>
        /// Informações da documentação
        /// </summary>
        public virtual GerenciamentoDocumentacao Documentacao { get; set; }

        /// <summary>
        /// Motivo de refaturamento
        /// </summary>
        public virtual DocumentacaoMotivoRefaturamento MotivoRefaturamento { get; set; }

        /// <summary>
        /// Vagões que estão sendo solicitados a nova documentação
        /// </summary>
        public virtual ICollection<GerenciamentoDocumentacaoRefaturamentoVagao> Vagoes { get; set; }

        /// <summary>
        /// ID da OS
        /// </summary>
        public virtual decimal OsId { get; set; }

        /// <summary>
        /// ID do Recebedor da Mercadoria
        /// </summary>
        public virtual decimal RecebedorId { get; set; }

        /// <summary>
        /// ID da Composição
        /// </summary>
        public virtual decimal ComposicaoId { get; set; }

        /// <summary>
        /// Número da OS
        /// </summary>
        public virtual decimal NumeroOs { get; set; }

        /// <summary>
        /// Prefixo do Trem
        /// </summary>
        public virtual string PrefixoTrem { get; set; }

        /// <summary>
        /// Indicador se o registro já foi processado
        /// </summary>
        public virtual string Processado { get; set; }

        /// <summary>
        /// Data de geração do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}