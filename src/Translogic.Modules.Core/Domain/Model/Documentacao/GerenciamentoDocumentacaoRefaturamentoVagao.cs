﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    public class GerenciamentoDocumentacaoRefaturamentoVagao : EntidadeBase<int>
    {
        /// <summary>
        /// Motivo de refaturamento
        /// </summary>
        public virtual GerenciamentoDocumentacaoRefaturamento GerenciamentoDocumentacaoRefaturamento { get; set; }

        /// <summary>
        /// Código do Vagão
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Indicador se está ativo ou não
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Data de geração do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}