﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;

    public class EmailEnvioDocumentacao : EntidadeBase<int>
    {
        /// <summary>
        /// Configuração de envio
        /// </summary>
        public virtual ConfiguracaoEnvioDocumentacao ConfiguracaoEnvio { get; set; }

        /// <summary>
        /// Email a ser enviado a documentação
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Se o e-mail está ativo para ser enviado
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}