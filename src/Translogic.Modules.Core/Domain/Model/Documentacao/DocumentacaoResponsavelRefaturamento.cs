﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;
    using System.Collections.Generic;

    public class DocumentacaoResponsavelRefaturamento : EntidadeBase<int>
    {
        /// <summary>
        /// Nome do área responsável(culpado) pelo refaturamento
        /// </summary>
        public virtual string Responsavel { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }

        /// <summary>
        /// Lista de motivos vinculados à área
        /// </summary>
        public virtual ICollection<DocumentacaoMotivoRefaturamento> Motivos { get; set; }
    }
}