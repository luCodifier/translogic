﻿using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;

    public class GerenciamentoDocumentacao : EntidadeBase<int>
    {
        /// <summary>
        /// ID do despacho
        /// </summary>
        public virtual decimal DespachoId { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal ComposicaoId { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal OsId { get; set; }

        /// <summary>
        /// Gerenciamento Documentacao de Refaturamento
        /// </summary>
        public virtual GerenciamentoDocumentacaoRefaturamento GerenciamentoDocumentacaoRefaturamento { get; set; }

        /// <summary>
        /// Vagões inseridos na documentação
        /// </summary>
        public virtual ICollection<GerenciamentoDocumentacaoCompVagao> GerenciamentoDocumentacaoCompVagao { get; set; }

        /// <summary>
        /// Número da OS
        /// </summary>
        public virtual decimal? OsNumero { get; set; }

        /// <summary>
        /// Prefixo do trem
        /// </summary>
        public virtual string PrefixoTrem { get; set; }

        /// <summary>
        /// ID do Recebedor da Mercadoria
        /// </summary>
        public virtual decimal? RecebedorId { get; set; }

        /// <summary>
        /// Chave de acesso criptografada
        /// </summary>
        public virtual string ChaveAcesso { get; set; }

        /// <summary>
        /// URL para acesso da documentação
        /// </summary>
        public virtual string UrlAcesso { get; set; }

        /// <summary>
        /// Duração de vigência da documentação
        /// </summary>
        public virtual decimal DiasValidade { get; set; }

        /// <summary>
        /// Número de tentativas para gerar a documentação
        /// </summary>
        public virtual decimal Tentativas { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime? CriadoEm { get; set; }

        /// <summary>
        /// Data de exclusão da documentação na URL
        /// </summary>
        public virtual DateTime? ExcluidoEm { get; set; }

        /// <summary>
        /// Data do erro
        /// </summary>
        public virtual DateTime? ErroEm { get; set; }

        /// <summary>
        /// Mensagem do erro
        /// </summary>
        public virtual string MensagemErro { get; set; }

        /// <summary>
        /// Indicador se deve-se gerar o relatório de laudo de mercadorias
        /// </summary>
        public virtual string GerarRelatorioLaudoMercadoria { get; set; }

        /// <summary>
        /// Indicador se todas as NF-e de todos os vagões do link foram recebidas e geradas
        /// </summary>
        public virtual string NfeCompleto { get; set; }

        /// <summary>
        /// Indicador se todos os Tickets de Pesagem de todos os vagões do link foram recebidas e geradas
        /// </summary>
        public virtual string TicketCompleto { get; set; }

        /// <summary>
        /// Indicador se todos os Laudos de Mercadorias de todos os vagões do link foram recebidas e geradas
        /// </summary>
        public virtual string LaudoCompleto { get; set; }
    }
}