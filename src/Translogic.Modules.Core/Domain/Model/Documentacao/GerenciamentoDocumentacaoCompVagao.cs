﻿using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    public class GerenciamentoDocumentacaoCompVagao : EntidadeBase<int>
    {
        /// <summary>
        /// Gerenciamento Documentacao de Refaturamento
        /// </summary>
        public virtual GerenciamentoDocumentacao GerenciamentoDocumentacao { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal OsId { get; set; }

        /// <summary>
        /// ID da composição
        /// </summary>
        public virtual decimal ComposicaoId { get; set; }

        /// <summary>
        /// ID do Recebedor da Mercadoria
        /// </summary>
        public virtual decimal RecebedorId { get; set; }

        /// <summary>
        /// ID do Vagão
        /// </summary>
        public virtual decimal VagaoId { get; set; }

        /// <summary>
        /// Código do vagão
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}