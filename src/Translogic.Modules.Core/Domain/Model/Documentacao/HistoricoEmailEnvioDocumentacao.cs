﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;

    public class HistoricoEmailEnvioDocumentacao : EntidadeBase<int>
    {
        public HistoricoEmailEnvioDocumentacao()
        {
        }

        public HistoricoEmailEnvioDocumentacao(string email)
        {
            this.Email = email;
            this.Enviado = "N";
        }

        /// <summary>
        /// Registro de histórico do envio
        /// </summary>
        public virtual HistoricoEnvioDocumentacao HistoricoEnvioDocumentacao { get; set; }

        /// <summary>
        /// Informação do email enviado
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Se o e-mail foi enviado
        /// </summary>
        public virtual string Enviado { get; set; }

        /// <summary>
        /// A data de quando o e-mail foi enviado
        /// </summary>
        public virtual DateTime? EnviadoEm { get; set; }
    }
}