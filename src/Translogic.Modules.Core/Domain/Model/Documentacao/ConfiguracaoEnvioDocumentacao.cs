﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Via;

    public class ConfiguracaoEnvioDocumentacao : EntidadeBase<int>
    {
        public virtual EmpresaCliente EmpresaDestino { get; set; }

        public virtual IAreaOperacional AreaOperacionalEnvio { get; set; }

        public virtual ICollection<EmailEnvioDocumentacao> Emails { get; set; }

        public virtual decimal? DiasValidadeDocumentacao { get; set; }

        public virtual string GerarRelatorioLaudoMercadoria { get; set; }

        public virtual string EnviarChegada { get; set; }

        public virtual string EnviarSaida { get; set; }

        public virtual string Ativo { get; set; }

        public virtual DateTime CriadoEm { get; set; }
    }
}