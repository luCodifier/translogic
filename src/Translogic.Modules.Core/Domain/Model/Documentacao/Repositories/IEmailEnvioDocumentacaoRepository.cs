﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;

    /// <summary>
    /// Contrato do Repositório de <see cref="EmailEnvioDocumentacao"/>
    /// </summary>
    public interface IEmailEnvioDocumentacaoRepository : IRepository<EmailEnvioDocumentacao, int>
    {
        /// <summary>
        /// Obtem todos os e-mails pelo id da configuração
        /// </summary>
        /// <param name="idConfiguracao">ID da configuração</param>
        /// <returns>Todos os e-mails da configuração</returns>
        ICollection<EmailEnvioDocumentacao> ObterEmailsPorConfiguracao(int idConfiguracao);

        /// <summary>
        /// Obtem todos os e-mails pelo id da configuração
        /// </summary>
        /// <param name="paginacao">Grid da tela</param>
        /// <param name="idConfiguracao">ID da configuração</param>
        /// <returns>Todos os e-mails da configuração</returns>
        ResultadoPaginado<EmailEnvioDocumentacao> ObterEmailsPorConfiguracaoGrid(DetalhesPaginacao paginacao, int idConfiguracao);
    }
}