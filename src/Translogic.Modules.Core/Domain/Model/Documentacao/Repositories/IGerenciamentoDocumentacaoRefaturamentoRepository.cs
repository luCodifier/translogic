﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    public interface IGerenciamentoDocumentacaoRefaturamentoRepository : IRepository<GerenciamentoDocumentacaoRefaturamento, int>
    {
        GerenciamentoDocumentacaoRefaturamento ObterPorOsRecebedor(decimal osId, decimal recebedorId);

        /// <summary>
        /// Retorna todos as solicitações de documentações de refaturamento que estão pendentes de criação inicial
        /// </summary>
        /// <param name="recebedorId">ID do terminal de destino</param>
        /// <returns>Todos os registros cuja documentação ainda não foi criada</returns>
        ICollection<GerenciamentoDocumentacaoRefaturamento> ObterPendentesGeracao(decimal recebedorId);

        /// <summary>
        /// Retorna todos as documentações de refaturamento que estão pendentes de atualização
        /// </summary>
        /// <returns>Todos os registros cuja documentação está pendente de atualização</returns>
        ICollection<GerenciamentoDocumentacaoRefaturamento> ObterPendentesAtualizacao();
    }
}