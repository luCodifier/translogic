﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;

    public interface IDocumentacaoMotivoRefaturamentoRepository : IRepository<DocumentacaoMotivoRefaturamento, int>
    {
        ICollection<DocumentacaoMotivoRefaturamento> ObterAtivos();
    }
}