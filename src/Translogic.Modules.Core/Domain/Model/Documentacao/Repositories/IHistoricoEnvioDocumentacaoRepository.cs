﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="HistoricoEnvioDocumentacao"/>
    /// </summary>
    public interface IHistoricoEnvioDocumentacaoRepository : IRepository<HistoricoEnvioDocumentacao, int>
    {
        /// <summary>
        /// Obtem todos os registros pendentes a serem enviados
        /// </summary>
        /// <returns></returns>
        IList<HistoricoEnvioDocumentacao> ObterPendentes();
    }
}