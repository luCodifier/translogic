﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IGerenciamentoDocumentacaoRepository : IRepository<GerenciamentoDocumentacao, int>
    {
        /// <summary>
        /// Obtem registros que estão pendentes para serem excluídos
        /// </summary>
        /// <returns>Documentação que deve ser excluída</returns>
        ICollection<GerenciamentoDocumentacao> ObterNaoVigentes();

        /// <summary>
        /// Obtem os regitros que estão pendentes de geração inicial
        /// </summary>
        /// <returns>Documentações pendentes de criação no FileShare</returns>
        ICollection<GerenciamentoDocumentacao> ObterNaoGeradosInicialmente();

        /// <summary>
        /// Obtem o registro cuja chave de acesso é a informada
        /// </summary>
        /// <param name="chaveAcesso">Chave de acesso</param>
        /// <returns>Documentação cuja chave de acesso seja a passada na parametrização</returns>
        GerenciamentoDocumentacao ObterPorChaveAcesso(string chaveAcesso);

        /// <summary>
        /// Obtem o registro cujo ID da OS é a informada
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        /// <param name="refaturamento">Indicador se é para procurar registros de refaturamento</param>
        /// <returns>Documentação cujo ID da OS seja a passada na parametrização</returns>
        GerenciamentoDocumentacao ObterPorOs(decimal osId, decimal recebedorId, bool refaturamento);

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumento(decimal osId, decimal recebedorId);

        /// <summary>
        /// Obtem os dados do documento levando em consideração o histórico
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoHistorico(decimal osId, decimal recebedorId);

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor</param>
        ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoRefaturamento(decimal osId, decimal recebedorId);

        /// <summary>
        /// Obtem os dados do documento levando em consideração o ID da composição e o recebedor
        /// </summary>
        /// <param name="composicaoId">ID da composição</param>
        /// <param name="recebedorId">ID do recebedor</param>
        GerenciamentoDocumentacao ObterPorComposicao(decimal composicaoId, decimal recebedorId);

        /// <summary>
        /// Obtem os dados do documento em uma consulta apenas
        /// </summary>
        /// <param name="itensDespachosIds">Lista de IDs dos itens despachos</param>
        ICollection<GerenciamentoDocumentacaoDto> ObterDadosDocumentoPorItemDespachos(List<decimal> itensDespachosIds);

        /// <summary>
        /// Obtém as documentações pelo número da OS e pelo recebedor, se for informado.
        /// </summary>
        /// <param name="numeroOs">ID da OS</param>
        /// <param name="recebedorId">ID do recebedor. Opcional.</param>
        /// <returns>Documentação cujo número da OS seja a passada no parâmetro do método</returns>
        ICollection<GerenciamentoDocumentacao> ObterPorNumeroOs(string numeroOs, int? recebedorId);
    }
}