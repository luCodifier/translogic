﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using ALL.Core.AcessoDados;

    public interface IDocumentacaoResponsavelRefaturamentoRepository : IRepository<DocumentacaoResponsavelRefaturamento, int>
    {
    }
}