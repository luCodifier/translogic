﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    public interface IGerenciamentoDocumentacaoCompVagaoRepository : IRepository<GerenciamentoDocumentacaoCompVagao, int>
    {
        /// <summary>
        /// Obtem registros que condizem com os parâmetros
        /// </summary>
        ICollection<GerenciamentoDocumentacaoCompVagao> ObterPorComposicao(decimal osId, decimal composicaoId,
                                                                           decimal? recebedorId);

        /// <summary>
        /// Obtem registros que que antecedem a composição passada no parâmetro
        /// </summary>
        ICollection<GerenciamentoDocumentacaoCompVagao> ObterPorUltimoEnvioComposicao(decimal osId, decimal composicaoId,
                                                                                      decimal? recebedorId);
    }
}