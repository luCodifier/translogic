﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;

    public interface IGerenciamentoDocumentacaoRefaturamentoVagaoRepository : IRepository<GerenciamentoDocumentacaoRefaturamentoVagao, int>
    {
        void RemoverPorDocumentoFaturamentoId(decimal id);
    }
}