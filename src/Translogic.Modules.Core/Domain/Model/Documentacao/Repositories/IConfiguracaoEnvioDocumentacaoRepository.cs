﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Contrato do Repositório de <see cref="ConfiguracaoEnvioDocumentacao"/>
    /// </summary>
    public interface IConfiguracaoEnvioDocumentacaoRepository : IRepository<ConfiguracaoEnvioDocumentacao, int>
    {
        /// <summary>
        /// Obtem a configuração por parâmetros
        /// </summary>
        /// <param name="areaOperacional">Código da área operacional</param>
        /// <param name="terminalId">Id do terminal</param>
        /// <returns>A configuração encontrada</returns>
        ConfiguracaoEnvioDocumentacao ObterPorParametros(string areaOperacional, int terminalId);

        /// <summary>
        /// Obtem os resultados da grid da tela de envio de documentação de refaturamento
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="malha">Número da Ordem de Serviço</param>
        /// <param name="destino">Número da Ordem de Serviço</param>
        /// <param name="recebedorId">ID do terminal recebedor da mercadoria</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="prefixo">Número da Ordem de Serviço</param>
        /// <param name="dataInicio">Número da Ordem de Serviço</param>
        /// <param name="dataFim">Número da Ordem de Serviço</param>
        /// <param name="tremEncerr">Número da Ordem de Serviço</param>
        /// <param name="situacaoEnvioStatus">Número da Ordem de Serviço</param>
        /// <param name="docCompl">Número da Ordem de Serviço</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        ResultadoPaginado<MonitoramentoEnvioDocumentacaoDto> ObterMonitoramentoGrid(DetalhesPaginacao pagination,
                                                                                           string malha,
                                                                                           string destino,
                                                                                           decimal? recebedorId,
                                                                                           string numeroOs,
                                                                                           string prefixo,
                                                                                           string dataInicio,
                                                                                           string dataFim,
                                                                                           string tremEncerr,
                                                                                           int situacaoEnvioStatus,
                                                                                           string docCompl);

        /// <summary>
        /// Obtem os resultados da grid da tela de envio de documentação de refaturamento
        /// </summary>
        /// <param name="pagination">Paginação da Grid</param>
        /// <param name="numeroOs">Número da Ordem de Serviço</param>
        /// <param name="recebedorId">ID do terminal recebedor da mercadoria</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        ResultadoPaginado<DocumentacaoRefaturamentoDto> ObterRefaturamentosGrid(DetalhesPaginacao pagination,
                                                                                string numeroOs,
                                                                                decimal? recebedorId);

        /// <summary>
        /// Obtem os resultados da grid da tela de parametrização
        /// </summary>
        /// <param name="paginacao">Paginação da Grid</param>
        /// <param name="areaOperacional">Código da Área Operacional de envio</param>
        /// <param name="terminal">Terminal para ser enviado</param>
        /// <param name="ativo">Bool indicando se é para filtrar por registros ativos ou não. Se independe, envie null</param>
        /// <returns>Retorna o resultado paginado da busca</returns>
        ResultadoPaginado<ConfiguracaoEnvioDocumentacao> ObterConfiguracoesGrid(DetalhesPaginacao paginacao,
                                                                                string areaOperacional,
                                                                                string terminal,
                                                                                bool? ativo);

        /// <summary>
        /// Obtem a configuração por cnpj da empresa
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns></returns>
        ConfiguracaoEnvioDocumentacao ObterPorCnpjEmpresa(string cnpj);

        /// <summary>
        /// Obtem todos as configurações ativas que possuem vagões destinados a ele. Omitindo o número da OS, verifica-se diretamente na tabela de configurações.
        /// </summary>
        /// <param name="numeroOs">Número da OS</param>
        /// <returns>Retorna todas as empresas cuja configuraação esteja ativa</returns>
        ICollection<ConfiguracaoEnvioDocumentacaoDto> ObterTerminaisPorOs(string numeroOs);
    }
}