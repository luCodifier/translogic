﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="HistoricoEmailEnvioDocumentacao"/>
    /// </summary>
    public interface IHistoricoEmailEnvioDocumentacaoRepository : IRepository<HistoricoEmailEnvioDocumentacao, int>
    {
    }
}