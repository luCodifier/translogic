﻿namespace Translogic.Modules.Core.Domain.Model.Documentacao
{
    using System;
    using ALL.Core.Dominio;

    public class DocumentacaoMotivoRefaturamento : EntidadeBase<int>
    {
        /// <summary>
        /// Referência da área responsável pelo refaturamento
        /// </summary>
        public virtual DocumentacaoResponsavelRefaturamento Responsavel { get; set; }

        /// <summary>
        /// Motivo do refaturamento
        /// </summary>
        public virtual string Motivo { get; set; }

        /// <summary>
        /// Observação do Refaturamento
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Indicador se o registro está ativo
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Data de criação do registro
        /// </summary>
        public virtual DateTime CriadoEm { get; set; }
    }
}