﻿namespace Translogic.Modules.Core.Domain.Model.Thermometer
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Termometros
    /// </summary>
    public class Thermometer : EntidadeBase<int?>
    {
        /// <summary>
        /// Id da Rodovia
        /// </summary>
        public virtual int Rodovia { get; set; }

        /// <summary>
        /// Termometro alternativo
        /// </summary>
        public virtual Thermometer TermometroAlternativo { get; set; }

        /// <summary>
        /// Id do Usuario
        /// </summary>
        public virtual int Usuario { get; set; }

        /// <summary>
        /// Id da Station
        /// </summary>
        public virtual int Station { get; set; }

        /// <summary>
        /// Codigo do Termometro
        /// </summary>
        public virtual string CodigoTermometro { get; set; }

        /// <summary>
        /// Nome do Termometro
        /// </summary>
        public virtual string NomeTermometro { get; set; }

        /// <summary>
        /// Data da ultima leitura
        /// </summary>
        public virtual DateTime UltimaLeitura { get; set; }

        /// <summary>
        /// Valor da ultima temperatura 
        /// </summary>
        public virtual double ValorUltimaTemperatura { get; set; }

        /// <summary>
        /// Valor da leitura manual
        /// </summary>
        public virtual double ValorLeituraManual { get; set; }

        /// <summary>
        /// Variação de tempo
        /// </summary>
        public virtual double VariacaoTempo { get; set; }

        /// <summary>
        /// Variação permitida
        /// </summary>
        public virtual double VariacaoPermitida { get; set; }

        /// <summary>
        /// Nome do indicador
        /// </summary>
        public virtual string Indicador { get; set; }

        /// <summary>
        /// Indicador da tabela
        /// </summary>
        public virtual string IndicadorTabela { get; set; }

        /// <summary>
        /// Temperatura fora da faixa
        /// </summary>
        public virtual string TemperaturaForaFaixa { get; set; }

        /// <summary>
        /// Data da atualização da tabela
        /// </summary>
        public virtual DateTime DataAtualizacao { get; set; }

        /// <summary>
        /// Indice de pluviosidade
        /// </summary>
        public virtual int Pluviosidade { get; set; }

        /// <summary>
        /// indice de pluviosidade consolidado
        /// </summary>
        public virtual int PluviosidadeConsolidado { get; set; }

        /// <summary>
        /// Medições do termômetro
        /// </summary>
        public virtual ICollection<ReadThermometer> ListaMedicoes { get; set; }

        /// <summary>
        /// Medições do termômetro
        /// </summary>
        public virtual ICollection<RegionThermometer> ListaRegion { get; set; }
    }
}