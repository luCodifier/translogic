﻿namespace Translogic.Modules.Core.Domain.Model.Thermometer
{
    using ALL.Core.Dominio;
    using Diversos;

    /// <summary>
    /// Classe representa as máximas temperaturas registradas em uma determinada região
    /// </summary>
    public class MaximeRegion : EntidadeBase<int?>
    {
        /// <summary>
        /// Classe Termometro
        /// </summary>
        public virtual RegionThermometer RegionThermometer { get; set; }

        /// <summary>
        /// Classe Termometro
        /// </summary>
        public virtual RestricaoEnum TipoRestricao { get; set; }

        /// <summary>
        /// Temperatura do Termômetro
        /// </summary>
        public virtual int? Temperatura { get; set; }
    }
}