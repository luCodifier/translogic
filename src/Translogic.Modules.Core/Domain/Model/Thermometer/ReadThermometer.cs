﻿namespace Translogic.Modules.Core.Domain.Model.Thermometer
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
	/// Classe Leitura de Termometros
	/// </summary>
    public class ReadThermometer : EntidadeBase<int?>
    {
		/// <summary>
		/// Classe Termometro
		/// </summary>
        public virtual Thermometer Termometro { get; set; }

		/// <summary>
		/// Temperatura do termometro
		/// </summary>
        public virtual int TemperaturaTermometro { get; set; }

		/// <summary>
		/// Data da leitura
		/// </summary>
        public virtual DateTime DataLeitura { get; set; }

		/// <summary>
		/// Indicador do Termometro
		/// </summary>
        public virtual string IndicadorTermometro { get; set; }

        /// <summary>
        /// Data da atualização da tabela
        /// </summary>
        public virtual DateTime DataAtualizacao { get; set; }

        /// <summary>
        /// Medida de pluviosidade
        /// </summary>
        public virtual int MedidaPluviosidade { get; set; }
	}
}