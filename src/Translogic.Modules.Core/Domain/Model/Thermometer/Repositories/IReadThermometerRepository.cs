﻿namespace Translogic.Modules.Core.Domain.Model.Thermometer.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Model.Thermometer;
    
    /// <summary>
    /// Interface de repositório de termômetro
    /// </summary>
    public interface IReadThermometerRepository : IRepository<ReadThermometer, int?>
    {
        /// <summary>
        /// Obtém as medições de um determinado termômetro
        /// </summary>
        /// <param name="idTermometro">Identificador do termômetro</param>
        /// <param name="dataInicial">Data Inicial de pesquisa</param>
        /// <param name="dataFinal">Data Final de pesquisa</param>
        /// <returns>Retorna as medições</returns>
        List<ReadThermometer> ObterMedicoesTermometros(int idTermometro, DateTime dataInicial, DateTime dataFinal);
    }
}
