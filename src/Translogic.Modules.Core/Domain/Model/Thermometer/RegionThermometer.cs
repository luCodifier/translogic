﻿namespace Translogic.Modules.Core.Domain.Model.Thermometer
{
    using System.Collections.Generic;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe representa a região de um termômetro
    /// </summary>
    public class RegionThermometer : EntidadeBase<int?>
    {
        /// <summary>
        /// Classe Termometro
        /// </summary>
        public virtual Thermometer Termometro { get; set; }

        /// <summary>
        /// Kilômetro Inicial
        /// </summary>
        public virtual int KmInicial { get; set; }

        /// <summary>
        /// Kilômetro Final
        /// </summary>
        public virtual int KmFinal { get; set; }

        /// <summary>
        /// Kilômetro Final
        /// </summary>
        public virtual bool TermometroAtivo { get; set; }

        /// <summary>
        /// Mínimas Temperaturas de uma determinada região
        /// </summary>
        public virtual ICollection<MinimumRegion> ListaMinimasTemperaturas { get; set; }

        /// <summary>
        /// Máximas Temperaturas de uma determinada região
        /// </summary>
        public virtual ICollection<MaximeRegion> ListaMaximasTemperaturas { get; set; }
    }
}