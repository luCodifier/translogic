﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;
    using Estrutura;

    /// <summary>
    /// Classe Trecho Tkb Diesel
    /// </summary>
    public class TrechoTkb : EntidadeBase<int?>
    {
        /// <summary>
        /// TKB Trecho
        /// </summary>
        public virtual string Trecho { get; set; }

        /// <summary>
        /// Trecho Diesel
        /// </summary>
        public virtual string TrechoDiesel { get; set; }

        /// <summary>
        /// Corredor do Trecho
        /// </summary>
        public virtual string Corredor { get; set; }

        /// <summary>
        /// Sentido do Trecho
        /// </summary>
        public virtual string Sentido { get; set; }

        /// <summary>
        /// Up do Trecho
        /// </summary>
        public virtual string Up { get; set; }

        /// <summary>
        /// Unidade de Produoção
        /// </summary>
        public virtual UnidadeProducao UnidadeProducao { get; set; }
    }
}
