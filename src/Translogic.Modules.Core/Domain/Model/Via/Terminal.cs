﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	/// <summary>
	/// Representa um terminal da Área Operacional
	/// </summary>
	/// <remarks>
	/// Uma área operacional pode possuir vários terminais
	/// </remarks>
	public class Terminal : AreaOperacional<Terminal>
	{
	}
}