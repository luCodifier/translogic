﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	using Acesso;
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa um elemento de via, normalmente chamado pelo termo "Linha"
	/// </summary>
	public class ElementoVia : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Estação que o elemento de via pertence
		/// </summary>
		public virtual IAreaOperacional AreaOperacional { get; set; }

		/// <summary>
		/// Bitola do Elemento de Via
		/// </summary>
		public virtual Bitola Bitola { get; set; }

		/// <summary>
		/// Capacidade de Ocupação no elemento de via
		/// </summary>
		public virtual int? CapacidadeOcupacao { get; set; }

		/// <summary>
		/// Código do elemento de Via
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do elemento de via
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica se o elemento de Via é de Circulação
		/// </summary>
		public virtual bool? IndCirculacao { get; set; }

		/// <summary>
		/// Indica se o Elemento de Via é de Integração
		/// </summary>
		public virtual bool? IndIntegracao { get; set; }

		/// <summary>
		/// Placa do Km final
		/// </summary>
		public virtual double? PlacaFinal { get; set; }

		/// <summary>
		/// Placa do Km inicial
		/// </summary>
		public virtual double? PlacaInicial { get; set; }

		/// <summary>
		/// Número da sequencia do elemento de via
		/// </summary>
		public virtual int? Sequencia { get; set; }

		/// <summary>
		/// Comprimento da linha
		/// </summary>
		public virtual double? Tamanho { get; set; }

		/// <summary>
		/// Usuário que cadastrou o elemento de via
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}