﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	using System.Collections.Generic;
	using Diversos;

	/// <summary>
	/// Representa uma estação mãe
	/// </summary>
	public class EstacaoMae : AreaOperacional<EstacaoMae>
	{
		#region CONSTRUTORES

		/// <summary>
		/// Inicializa a classe sem passagem de parametros
		/// </summary>
		public EstacaoMae()
		{
			ListaDuracaoAtividade = new List<DuracaoAtividadeEstacao>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Lista de Duração das atividades da Estaçao
		/// </summary>
		public virtual IList<DuracaoAtividadeEstacao> ListaDuracaoAtividade { get; set; }

		#endregion
	}
}