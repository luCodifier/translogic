﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para mapear 
    /// </summary>
    public class MargemTrecho : EntidadeBase<int>
    {
        /// <summary>
        /// Margem direita do trecho
        /// </summary>
        public virtual EstacaoMae MargemDireita { get; set; }

        /// <summary>
        /// Margem esquerda do trecho
        /// </summary>
        public virtual EstacaoMae MargemEsquerda { get; set; }
    }
}