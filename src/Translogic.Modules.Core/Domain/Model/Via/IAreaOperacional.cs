namespace Translogic.Modules.Core.Domain.Model.Via
{
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Interface de Areas Operacionais
	/// </summary>
	public interface IAreaOperacional : IIdentificavel<int?>, IVersionavel, IEntidadeBase
	{
		#region PROPRIEDADES

		/// <summary>
		/// Bitola que a �rea operacional possui
		/// </summary>
		Bitola Bitola { get; set; }

		/// <summary>
		/// C�digo da �rea operacional
		/// </summary>
		string Codigo { get; set; }

		/// <summary>
		/// Descri��o da �rea operacional
		/// </summary>
		string Descricao { get; set; }

		/// <summary>
		/// Empresa concession�ria
		/// </summary>
		IEmpresa EmpresaConcessionaria { get; set; }

		/// <summary>
		/// Empresa Operadora
		/// </summary>
		IEmpresa EmpresaOperadora { get; set; }

		/// <summary>
		/// Esta��o M�e
		/// </summary>
		EstacaoMae EstacaoMae { get; set; }

		/// <summary>
		/// Tipo do abastecimento, se a area operacional possuir
		/// </summary>
		AbastecimentoEnum IndAbastecimento { get; set; }

		/// <summary>
		/// Indica se � de influencia
		/// </summary>
		bool? IndInfluencia { get; set; }

		/// <summary>
		/// Indica se possui intercambio
		/// </summary>
		bool? IndIntercambio { get; set; }

		/// <summary>
		/// Municipio - <see cref="Municipio"/>
		/// </summary>
		Municipio Municipio { get; set; }

		/// <summary>
		/// Unidade de Producao
		/// </summary>
		UnidadeProducao UnidadeProducao { get; set; }

		#endregion
	}
}