﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Super-Classe Genérica que representa a Area Operacional
	/// </summary>
	public class AreaOperacionalDto : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da área operacional
		/// </summary>
		public virtual int Codigo { get; set; }

		/// <summary>
		/// Código SAP
		/// </summary>
		public virtual string CodigoSAP { get; set; }

		#endregion
	}
}