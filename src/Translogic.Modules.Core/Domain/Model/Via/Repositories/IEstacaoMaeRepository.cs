namespace Translogic.Modules.Core.Domain.Model.Via.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Interfaces.Trem.OrdemServico;

	/// <summary>
	/// Interface de reposit�rio para <see cref="EstacaoMae"/>
	/// </summary>
	public interface IEstacaoMaeRepository : IRepository<EstacaoMae, int?>
	{
        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        EstacaoMae ObterEstMaePorCodigo(string codigo);

        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        IAreaOperacional ObterPorCodigo(string codigo);

		/// <summary>
		/// Obter a area operacional com o c�digo, descricao, municipio ou estado informado
		/// </summary>
		/// <param name="codigo">C�digo da area operacional</param>
		/// <param name="descricao"> Descri��o da area operacional</param>
		/// <param name="idMunicio"> Cidade da area operacional</param>
		/// <param name="idEstado"> Estado da area operacional</param>
		/// <returns>Retorna as areas operacionais que tenha o filtro informado.</returns>
		IList<EstacaoMae> ObterEstacao(string codigo, string descricao, int? idMunicio, int? idEstado);

		/// <summary>
		/// Obter a area operacional com o c�digo, descricao, municipio ou estado informado
		/// </summary>
		/// <param name="codigo">C�digo da area operacional</param>
		/// <returns>Retorna as areas operacionais que tenha o filtro informado.</returns>
		IList<EstacaoMae> ObterPorUnidadeProducao(int codigo);

        /// <summary>
        /// Retorna o Id EstacaoMae, pelo codigo do local
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        EstacaoMaeDto ObterIdEstacaoMaePorCodigoDoLocal(string codigo);
	}
}