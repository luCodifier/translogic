
namespace Translogic.Modules.Core.Domain.Model.Via.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Dto;

    /// <summary>
    /// Interface de reposit�rio para <see cref="IAreaOperacional"/>
    /// </summary>
    public interface IAreaOperacionalRepository : IRepository<IAreaOperacional, int?>
    {
        /// <summary>
        /// Obter a area operacional com o c�digo informado
        /// </summary>
        /// <param name="codigo">C�digo da area operacional</param>
        /// <returns>Retorna a area operacional que tenha o c�digo informado</returns>
        IAreaOperacional ObterPorCodigo(string codigo);

        /// <summary>
        /// Metodo que retorna os terminais
        /// </summary>
        /// <returns>Retorna lista de Terminais</returns>
        List<TerminalSeguroDto> ObterTerminais();

        /// <summary>
        /// Metodo que retorna os terminais de acordo com o despacho e serie
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>Retorna lista de Terminais</returns>
        List<TerminalSeguroDto> ObterTerminaisPorDespacho(int despacho, string serie);

        /// <summary>
        /// Metodo que retorna os terminais de acordo com o despacho e serie
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>Retorna lista de Terminais</returns>
        List<TerminalSeguroDto> ObterTerminaisPorDespachoIntercambio(int despacho, string serie);

        /// <summary>
        /// Metodo que retorna os terminais de acordo com o despacho e serie
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>Retorna lista de Terminais</returns>
        List<AreaOpDto> ObterPorCliente(string codigoEmpresa);

        IList<AreaOpDto> ObterTodasComFluxoComercial();

        List<TerminalSiglaDto> ObterListaTerminalSigla();
    }
}