﻿namespace Translogic.Modules.Core.Domain.Model.Via.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Estrutura;

    /// <summary>
    /// Interface de repositório para <see cref="TrechoTkb"/>
    /// </summary>
    public interface ITrechoTkbRepository : IRepository<TrechoTkb, int?>
    {
        /// <summary>
        /// Método para obter os trechos TKBs
        /// </summary>
        /// <param name="trechoTkb"> Parametro trecho Tkb. </param>
        /// <param name="trechoDiesel"> Parametro trecho Diesel. </param>
        /// <param name="corredor"> Parametro corredor. </param>
        /// <param name="up"> Parametro Up </param>
        /// <param name="idUp"> Id da Up (classe) </param>
        /// <returns> Retorna lista de Trechos </returns>
        IList<TrechoTkb> ObterTodosPorParametro(string trechoTkb, string trechoDiesel, string corredor, string up, int? idUp);

    	/// <summary>
    	/// Obtém o trecho Tkb por trecho diesel
    	/// </summary>
    	/// <param name="trechoDiesel"> The trecho diesel. </param>
    	/// <returns> Objeto do tipo <see cref="TrechoTkb"/> </returns>
    	TrechoTkb ObterPorTrechoDiesel(string trechoDiesel);
    }
}