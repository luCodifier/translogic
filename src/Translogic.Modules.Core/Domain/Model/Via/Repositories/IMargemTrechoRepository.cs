﻿namespace Translogic.Modules.Core.Domain.Model.Via.Repositories
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface de repositório para <see cref="MargemTrecho"/>
    /// </summary>
    public interface IMargemTrechoRepository
    {
    	/// <summary>
    	/// Obtém as margens referentes ao fluxo
    	/// </summary>
        /// <param name="fluxo"> Fluxo Comercial para filtro. </param>
    	/// <returns> Lista de margens referentes as este fluxo </returns>
    	IEnumerable<MargemTrecho> ObterPorDestinoFluxo(FluxosComerciais.FluxoComercial fluxo);
    }
}