﻿namespace Translogic.Modules.Core.Domain.Model.Via.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Estrutura;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface de repositório para <see cref="ElementoVia"/>
    /// </summary>
    public interface IElementoViaRepository : IRepository<ElementoVia, int>
    {
        /// <summary>
    	/// Obtém as linhas da areaOperacional
    	/// </summary>
        /// <param name="idAreaOperacional"> Id da Area Operacional da linha. </param>
    	/// <returns> Lista de Objetos do tipo <see cref="ElementoVia"/> </returns>
    	IList<ElementoVia> ObterPorIdEstacaoMae(int idAreaOperacional);

        /// <summary>
        /// Obter as linhas do pátio
        /// </summary>
        /// <param name="patio"></param>
        /// <returns></returns>
        IList<ElementoViaDto> ObterPorPatio(string patio);
    }
}