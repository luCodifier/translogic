﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	/// <summary>
	/// Area Operacional Trecho
	/// </summary>
	/// <remarks>
	/// Não confundir AreaOperacionalTrecho com Trecho
	/// </remarks>
	public class AreaOperacionalTrecho : AreaOperacional<AreaOperacionalTrecho>
	{
	}
}