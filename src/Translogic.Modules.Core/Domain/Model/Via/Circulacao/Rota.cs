﻿namespace Translogic.Modules.Core.Domain.Model.Via.Circulacao
{
    using System.Collections.Generic;
    using ALL.Core.Dominio;
	using Interfaces.Trem.OrdemServico;

	/// <summary>
	/// Representa as rotas que o <see cref="Translogic.Modules.Core.Domain.Model.Trem" /> pode passar
	/// </summary>
	public class Rota : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Indica se a Rota está ativa - <see cref="bool"/>
		/// </summary>
		public virtual bool Ativa { get; set; }

		/// <summary>
		/// Código da Rota
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Rota
		/// </summary>
		public virtual string Descricao { get; set; }

        /// <summary>
        /// Extensão da rota
        /// </summary>
	    public virtual float Extensao { get; set; }

		/// <summary>
		/// <see cref="PatioCirculacao"/> de Destino
		/// </summary>
		public virtual PatioCirculacao Destino { get; set; }

		/// <summary>
		/// <see cref="PatioCirculacao"/> de Origem
		/// </summary>
		public virtual PatioCirculacao Origem { get; set; }

		/// <summary>
		/// Sentido da Rota - <see cref="RotaSentido"/>
		/// </summary>
		public virtual RotaSentido? Sentido { get; set; }

        /// <summary>
        /// Lista de <see cref="IAreaOperacional"/> que formam esta rota
        /// </summary>
        public virtual IList<AreaOperacionalRota> Detalhe { get; set; }

		#endregion
	}
}