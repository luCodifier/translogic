namespace Translogic.Modules.Core.Domain.Model.Via.Circulacao.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Interfaces.Trem.OrdemServico;

	/// <summary>
	/// Contrato para o servi�o de Rota
	/// </summary>
	public interface IRotaRepository : IRepository<Rota, int>
	{
		#region M�TODOS

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="origem">Origem da rota</param>
		/// <param name="destino">Destino da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		Rota ObterPorOrigemDestino(PatioCirculacao origem, PatioCirculacao destino);

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="idRota"> Id da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		IList<EmpresasEnvolvidasDto> ObterFerroviasEnvolvidasPorIdRota(int idRota);

        /// <summary>
        /// Obt�m a rota de menor dist�ncia dado o trecho 
        /// </summary>
        /// <param name="codigoOrigem"> Codigo de origem. </param>
        /// <param name="codigoDestino"> Codigo de destino. </param>
        /// <returns> Objeto Rota </returns>
        Rota ObterPorTrechoMenorDistancia(string codigoOrigem, string codigoDestino);

		/// <summary>
		/// Obtem a rota pela Origem e Destino
		/// </summary>
		/// <param name="origem">Origem da rota</param>
		/// <param name="destino">Destino da rota</param>
		/// <returns>Rota de Origem e Destino</returns>
		Rota ObterPorOrigemDestino(EstacaoMae origem, EstacaoMae destino);

		#endregion
	}
}