﻿namespace Translogic.Modules.Core.Domain.Model.Via.Circulacao
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a sequencia da <see cref="AreaOperacionalTrecho"/>/<see cref="PatioCirculacao"/> na <see cref="Rota"/>
	/// </summary>
	public class AreaOperacionalRota : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Area Operacional que pode ser <see cref="AreaOperacionalTrecho"/> ou <see cref="PatioCirculacao"/>
		/// </summary>
		public virtual IAreaOperacional AreaOperacional { get; set; }

		/// <summary>
		/// Rota - <see cref="Rota"/>
		/// </summary>
		public virtual Rota Rota { get; set; }

		/// <summary>
		/// Sequencia da Area Operacional na Rota
		/// </summary>
		public virtual int Sequencia { get; set; }

		/// <summary>
		/// Trecho - <see cref="Trecho"/>
		/// </summary>
		public virtual Trecho Trecho { get; set; }

		#endregion
	}
}