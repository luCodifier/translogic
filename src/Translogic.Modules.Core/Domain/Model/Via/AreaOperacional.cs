﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Super-Classe Genérica que representa a Area Operacional
	/// </summary>
	/// <typeparam name="T">Classe de Área Operacional que está herdando</typeparam>
	public abstract class AreaOperacional<T> : EntidadeBase<int?>, IAreaOperacional
	{
		#region CONSTRUTORES

		/// <summary>
		/// Construtor inicializando o indicador de abastecimento
		/// </summary>
		protected AreaOperacional()
		{
			IndAbastecimento = AbastecimentoEnum.Nenhum;
		}

		#endregion

		#region IAreaOperacional MEMBROS

		/// <summary>
		/// Bitola - <see cref="Diversos.Bitola"/>
		/// </summary>
		public virtual Bitola Bitola { get; set; }

		/// <summary>
		/// Código da área operacional
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da área operacional
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Empresa Concessionária - <see cref="IEmpresa"/>
		/// </summary>
		public virtual IEmpresa EmpresaConcessionaria { get; set; }

		/// <summary>
		/// Empresa Operadora - <see cref="IEmpresa"/>
		/// </summary>
		public virtual IEmpresa EmpresaOperadora { get; set; }

		/// <summary>
		/// Estação mãe - <see cref="Translogic.Modules.Core.Domain.Model.Via.EstacaoMae"/>
		/// </summary>
		public virtual EstacaoMae EstacaoMae { get; set; }

		/// <summary>
		/// Indica qual o tipo do abastecimento de acordo com os valores do <see cref="AbastecimentoEnum"/>
		/// </summary>
		public virtual AbastecimentoEnum IndAbastecimento { get; set; }

		/// <summary>
		/// Indica se a área operacional é de influencia
		/// </summary>
		public virtual bool? IndInfluencia { get; set; }

		/// <summary>
		/// Indica se a área operacional é de intercambio
		/// </summary>
		public virtual bool? IndIntercambio { get; set; }

        /// <summary>
        /// Indica a area operacional é oficina
        /// </summary>
        public virtual bool? IndOficina { get; set; }

		/// <summary>
		/// Municipio - <see cref="Translogic.Modules.Core.Domain.Model.Diversos.Municipio"/>
		/// </summary>
		public virtual Municipio Municipio { get; set; }

		/// <summary>
		/// Unidade de produção - <see cref="Estrutura.UnidadeProducao"/>
		/// </summary>
		public virtual UnidadeProducao UnidadeProducao { get; set; }

		#endregion
	}
}