﻿namespace Translogic.Modules.Core.Domain.Model.Via
{
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Representa o Trecho
	/// </summary>
	public class Trecho : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Bitola do Trecho
		/// </summary>
		public virtual Bitola Bitola { get; set; }

		/// <summary>
		/// Descrição do Trecho
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Empresa Operadora no Trecho
		/// </summary>
		public virtual EmpresaFerrovia EmpresaOperadora { get; set; }

		/// <summary>
		/// <see cref="PatioCirculacao"/> de Término do Trecho
		/// </summary>
		public virtual PatioCirculacao EstacaoFim { get; set; }

		/// <summary>
		/// <see cref="PatioCirculacao"/> de Início do Trecho
		/// </summary>
		public virtual PatioCirculacao EstacaoInicio { get; set; }

		/// <summary>
		/// Extensão do Trecho
		/// </summary>
		public virtual double? Extensao { get; set; }

		/// <summary>
		/// Placa do Km final do Trecho
		/// </summary>
		public virtual double? PlacaKmFinal { get; set; }

		/// <summary>
		/// Placa do Km inicial do Trecho
		/// </summary>
		public virtual double? PlacaKmInicial { get; set; }

		#endregion
	}
}