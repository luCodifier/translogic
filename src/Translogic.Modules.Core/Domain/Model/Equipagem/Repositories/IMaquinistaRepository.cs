﻿namespace Translogic.Modules.Core.Domain.Model.Equipagem.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de repositorio de Maquinista
    /// </summary>
    public interface IMaquinistaRepository : IRepository<Maquinista, int?>
    {
    }
}