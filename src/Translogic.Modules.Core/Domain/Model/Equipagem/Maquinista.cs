﻿namespace Translogic.Modules.Core.Domain.Model.Equipagem
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa um maquinista
	/// </summary>
	public class Maquinista : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Categoria do Maquinista - <see cref="CategoriaMaquinista"/> 
		/// </summary>
		public virtual CategoriaMaquinista Categoria { get; set; }

		/// <summary>
		/// Data de Abertura da caderneta do maquinista
		/// </summary>
		public virtual DateTime? DataAberturaCaderneta { get; set; }

		/// <summary>
		/// Função do Maquinista - <see cref="FuncaoMaquinistaEnum"/>
		/// </summary>
		public virtual FuncaoMaquinistaEnum? Funcao { get; set; }

		/// <summary>
		/// Matrícula do maquinista
		/// </summary>
		public virtual string Matricula { get; set; }

		/// <summary>
		/// Nome do maquinista
		/// </summary>
		public virtual string Nome { get; set; }

		#endregion
	}
}