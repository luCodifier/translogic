﻿namespace Translogic.Modules.Core.Domain.Model.Equipagem
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa uma Categoria de Maquinistas
	/// </summary>
	public class CategoriaMaquinista : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descrição da Categoria
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}