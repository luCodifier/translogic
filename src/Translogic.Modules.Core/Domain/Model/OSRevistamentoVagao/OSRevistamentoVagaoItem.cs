﻿using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
using System;

namespace Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao
{
    public class OSRevistamentoVagaoItem : EntidadeBase<int>
    {
        #region Propriedades


        /// <summary>
        /// Referência da OS
        /// </summary>
        public virtual OSRevistamentoVagao OSRevistamentoVagao { get; set; }

        /// <summary>
        /// Referência ao vagão
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        /// Se o serviço foi concluído
        /// </summary>
        public virtual int? Concluido { get; set; }

        /// <summary>
        /// Retirar
        /// </summary>
        public virtual int? Retirar { get; set; }

        /// <summary>
        /// Comproblema
        /// </summary>
        public virtual int? Comproblema { get; set; }

        /// <summary>
        /// Observação
        /// </summary>
        public virtual string Observacao { get; set; }

        #endregion
    }
}