﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.OSLimpezaVagao;

namespace Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao
{
    public class OSRevistamentoVagao : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// ID da OS de Revistamento parcial
        /// </summary>
        public virtual OSRevistamentoVagao OsRevistamentoParcial { get; set; }

        /// <summary>
        /// Data e hora
        /// </summary>
        public virtual DateTime? Data { get; set; }

        /// <summary>
        /// Data Hora Inicio
        /// </summary>
        public virtual DateTime? DataHoraInicio { get; set; }

        /// <summary>
        /// Data Hora Termino
        /// </summary>
        public virtual DateTime? DataHoraTermino { get; set; }

        /// <summary>
        /// Data Hora Entrega
        /// </summary>
        public virtual DateTime? DataHoraEntrega { get; set; }

        /// <summary>
        /// Linha
        /// </summary>
        public virtual string Linha { get; set; }

        /// <summary>
        /// Convocacao
        /// </summary>
        public virtual string Convocacao { get; set; }

        /// <summary>
        /// Quantidade Vagões
        /// </summary>
        public virtual int QtdVagoes { get; set; }

        /// <summary>
        /// Quantidade Vagões Vedados
        /// </summary>
        public virtual int QtdVagoesVedados { get; set; }

        /// <summary>
        /// Quantidade Vagões Gambitados
        /// </summary>
        public virtual int QtdVagoesGambitados { get; set; }

        /// <summary>
        /// Quantidade Vazios
        /// </summary>
        public virtual int QtdEntreVazios { get; set; }

        /// Fornecedor
        /// </summary>
        public virtual FornecedorOs Fornecedor { get; set; }

        /// <summary>
        /// Local do Serviço
        /// </summary>
        public virtual IAreaOperacional Local { get; set; }

        /// <summary>
        /// TipoServico
        /// </summary>
        public virtual OSTipoServico TipoServico { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public virtual OSStatus Status { get; set; }

        /////// <summary>
        /////// Nome Manutenção
        /////// </summary>
        public virtual string NomeManutencao { get; set; }

        /////// <summary>
        /////// Matrícula Manutenção
        /////// </summary>
        public virtual string MatriculaManutencao { get; set; }

        /// <summary>
        /// Nome 
        /// </summary>
        public virtual string NomeEstacao { get; set; }

        /// <summary>
        /// Matrícula Estação
        /// </summary>
        public virtual string MatriculaEstacao { get; set; }

        /// <summary>
        /// Justificativa de cancelamento
        /// </summary>
        public virtual string JustificativaCancelamento { get; set; }

        /// <summary>
        /// Data da ultima alteração
        /// </summary>
        public virtual DateTime DataUltimaAlteracao { get; set; }

        /// <summary>
        /// Justificativa de cancelamento
        /// </summary>
        public virtual string Usuario { get; set; }

        #endregion

        #region Construtores
        public OSRevistamentoVagao()
        {
            VersionDate = DateTime.Now;
            DataUltimaAlteracao = DateTime.Now;
            Data = DateTime.Now;
        }
        #endregion
    }
}