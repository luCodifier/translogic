﻿
namespace Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using System;
    using ALL.Core.Dominio;
    using System.Collections.Generic;

    public interface IOSRevistamentoVagaoRepository : IRepository<OSRevistamentoVagao, int>
    {
        /// <summary>
        /// Retornar lista de OS de acordo com filtros e com paginação
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="vagaoRetirado"></param>
        /// <param name="problemaVedacao"></param>
        /// <param name="vagaoCarregado"></param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado);

        /// <summary>
        /// Retornar lista de OS com vagões sem paginação para exportação.
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="vagaoRetirado"></param>
        /// <param name="problemaVedacao"></param>
        /// <param name="vagaoCarregado"></param>
        /// <returns></returns>
        IList<OSRevistamentoVagaoExportaDto> ObterConsultaOsParaExportar(DateTime? dataInicial, DateTime? dataFinal,
        string local, string numOs, string idTipo, string idStatus, bool vagaoRetirado, bool problemaVedacao, bool vagaoCarregado);
        
    }
}
