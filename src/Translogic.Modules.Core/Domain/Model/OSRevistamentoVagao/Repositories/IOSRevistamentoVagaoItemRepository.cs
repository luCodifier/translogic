﻿
namespace Translogic.Modules.Core.Domain.Model.OSRevistamentoVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using System.Collections.Generic;

    public interface IOSRevistamentoVagaoItemRepository : IRepository<OSRevistamentoVagaoItem, int>
    {
        /// <summary>
        /// Buscar vagões para adicionar na OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">paginação e ordenação</param>
        /// <param name="numOs">Número da OS</param>
        /// <param name="listaDeVagoes">Vagões separados por ponto e vírgula</param>
        /// <param name="serie">série</param>
        /// <param name="local">local</param>
        /// <param name="linha">linha</param>
        /// <param name="idLotacao">id da lotação</param>
        /// <param name="sequenciaInicio">Sequencia inicial</param>
        /// <param name="sequenciaFim">Sequencia inicial</param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
              string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequenciaInicio, int sequenciaFim);

        /// <summary>
        /// Lista de lotações dos vagões da OS
        /// </summary>
        /// <returns></returns>
        IList<OSVagaoItemLotacaoDto> ObterLotacoes();

        /// <summary>
        /// Retornar os vagões de uma OS
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        ResultadoPaginado<OSRevistamentoVagaoItemDto> ObterVagoesDaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs);

        /// <summary>
        /// Criar uma nova OS e os itens da OS (vagões)
        /// </summary>
        /// <param name="osRevistamentoVagaoItens"></param>
        /// <returns></returns>
        void Inserir(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens);

        /// <summary>
        /// Atualizar os vagões da OS
        /// </summary>
        /// <param name="osRevistamentoVagaoItens"></param>
        void Atualizar(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens);

        /// <summary>
        /// Remover itens em lista
        /// </summary>
        /// <param name="osRevistamentoVagaoItens"></param>
        void Remover(IList<OSRevistamentoVagaoItem> osRevistamentoVagaoItens);

        /// <summary>
        /// Buscar vagões da OS
        /// </summary>
        /// <param name="idOs"></param>
        /// <returns></returns>
        IList<OSRevistamentoVagaoItem> ObterVagoesDaOs(int idOs);
    }
}
