﻿
namespace Translogic.Modules.Core.Domain.Model.ConsultaTrem
{
    using System;
    using ALL.Core.Dominio;
    public class PerguntaChecklist : EntidadeBase<int>
    {
        public virtual string Descricao { get; set; }
        public virtual Int32 Ordem { get; set; }
        public virtual string Tipo { get; set; }
        public virtual Int32 Agrupador { get; set; }
    }
}