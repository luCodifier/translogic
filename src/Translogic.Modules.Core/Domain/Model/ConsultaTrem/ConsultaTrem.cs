﻿namespace Translogic.Modules.Core.Domain.Model.ConsultaTrem
{
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa uma estação do cadatro único
	/// </summary>
    public class ConsultaTrem : EntidadeBase<int>
	{
		#region PROPRIEDADES

        /////// <summary>
        /////// Posicionamento: Altitude
        /////// </summary>
        ////public virtual int Altitude { get; set; }

        /////// <summary>
        /////// Código da estação
        /////// </summary>        
        ////public virtual string Codigo { get; set; }

        /////// <summary>
        /////// Posicionamento: Direcao
        /////// </summary>
        ////public virtual int Direcao { get; set; }

        /////// <summary>
        /////// Marcação de KM
        /////// </summary>
        ////public virtual int Km { get; set; }

        /////// <summary>
        /////// Posicionamento: Latitude
        /////// </summary>
        ////public virtual int Latitude { get; set; }

        /////// <summary>
        /////// Posicionamento: Longitude
        /////// </summary>
        ////public virtual int Longitude { get; set; }

        /////// <summary>
        /////// Pontos notáveis
        /////// </summary>
        ////public virtual int? PontosNotaveis { get; set; }

        /////// <summary>
        /////// Raio de curva
        /////// </summary>
        ////public virtual int Raio { get; set; }

        /////// <summary>
        /////// SB desviada para a direita
        /////// </summary>
        ////public virtual string SBDesviadaDireita { get; set; }

        /////// <summary>
        /////// SB desviada para a esquerda
        /////// </summary>
        ////public virtual string SBDesviadaEsquerda { get; set; }

        /////// <summary>
        /////// sub divisão da linha desviada
        /////// </summary>
        ////public virtual int SubdivisaoLinhaDesviada { get; set; }

        /////// <summary>
        /////// sub divisão da linha principal
        /////// </summary>
        ////public virtual int SubdivisaoLinhaPrincipal { get; set; }

        /////// <summary>
        /////// Tempo (em minutos) de descida
        /////// </summary>
        ////public virtual double TempoDescida { get; set; }

        /////// <summary>
        /////// Tempo (em minutos) de subida
        /////// </summary>
        ////public virtual double TempoSubida { get; set; }

        /////// <summary>
        /////// <see cref="Translogic.Modules.Core.Domain.Model.Acesso.Usuario"/> que gravou
        /////// </summary>
        ////public virtual Usuario Usuario { get; set; }

        /////// <summary>
        /////// Velocidade crescente
        /////// </summary>
        ////public virtual int VelocidadeCrescente { get; set; }

        /////// <summary>
        /////// Velocidade decrescente
        /////// </summary>
        ////public virtual int VelocidadeDecrescente { get; set; }

		#endregion
	}
}