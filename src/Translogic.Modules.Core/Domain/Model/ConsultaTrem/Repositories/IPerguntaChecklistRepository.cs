﻿namespace Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;

   public  interface IPerguntaChecklistRepository : IRepository<PerguntaChecklist, int>
    {
       /// <summary>
       /// Retorna as perguntas 
       /// </summary>
       /// <param name="detalhesPaginacaoWeb"></param>
       /// <param name="idAgrupador"></param>
       /// <param name="tipoPergunta"></param>
       /// <returns></returns>
       ResultadoPaginado<PerguntaCheckListDto> ObterConsultaPerguntasChecklist(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? idAgrupador, string tipoPergunta);

       /// <summary>
       /// Retorna lista de perguntas por tipo
       /// </summary>
       /// <param name="tipo"></param>
       /// <returns></returns>
       IList<PerguntaCheckListDto> ObterPerguntasChecklistPorTipo(string tipo);
    }
}
