namespace Translogic.Modules.Core.Domain.Model.ConsultaTrem.Repositories
{
	using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto.ConsultaTrem;
    using Translogic.Core.Infrastructure.Web;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Trem.OrdemServico;

	/// <summary>
	/// Interface de repositório de Consulta Trem
	/// </summary>
    public interface IConsultaTremRepository : IRepository
	{
        /// <summary>
        /// Pesquisar trens
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="dataInicial">Filtro data Inicial</param>
        /// <param name="dataFinal">Filtro data Final</param>
        /// <param name="numOs"> Filtro NUM OS</param>
        /// <returns>Lista de dados para GRID</returns>
        ResultadoPaginado<ConsultaTremDto> ObterConsultaTrem(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir);

        /// <summary>
        /// Exportar trens
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">Paginação</param>
        /// <param name="dataInicial">Filtro data Inicial</param>
        /// <param name="dataFinal">Filtro data Final</param>
        /// <param name="numOs"> Filtro NUM OS</param>
        /// <returns>Lista de dados para EXPORTAÇÃO EXCEL</returns>
        IEnumerable<ConsultaTremDto> ObterConsultaTremExportar(string prefixo, string os, string situacao, string origem, string destino, string tempoAPT, string prefixosExcluir);

        /// <summary>
        /// Verificar se existe mercadoria com codigo ONU na composição
        /// </summary>
        /// <param name="os">numero da OS</param>
        /// <returns>Retorna o total de mercadorias com numero ONU</returns>
        IEnumerable<ConsultaTremDto> ObterTotalMercadarioOnu(string os);



	}
}