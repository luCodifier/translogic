﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes;

namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Repositories
{
    public interface ILocalizacaoRepository : IRepository<Localizacao, int?>
    {

    }
}