﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a dimensão de localização do Quadro de Estados
	/// </summary>
	public class Localizacao : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// Código da Localizacao
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Localização
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}