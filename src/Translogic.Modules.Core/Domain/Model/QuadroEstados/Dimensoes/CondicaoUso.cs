namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa a dimens�o Condi��o de Uso do Quadro de Estados
	/// </summary>
	public class CondicaoUso : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region PROPRIEDADES

		/// <summary>
		/// Indica se a condi�ao de uso � aplicavel � locomotiva
		/// </summary>
		public virtual bool? IndAplicavelLocomotiva { get; set; }

		/// <summary>
		/// Indica se a condi��o de uso � aplicavel ao vagao
		/// </summary>
		public virtual bool? IndAplicavelVagao { get; set; }

		#endregion

		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// C�digo da Condi��o de uso
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descri��o da Condi��o de uso
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}