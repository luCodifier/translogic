namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a dimens�o de intercambio do Quadro de Estados
	/// </summary>
	public class Intercambio : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// C�digo do Interc�mbio
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descri��o do Interc�mbio
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}