namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	/// <summary>
	/// Interface que indica que o objeto � uma dimens�o do quadro de estados
	/// </summary>
	public interface IDimensaoQuadroEstado
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo da dimens�o do quadro de estados
		/// </summary>
		string Codigo { get; set; }

		/// <summary>
		/// Descri��o da dimens�o do quadro de estados
		/// </summary>
		string Descricao { get; set; }

		#endregion
	}
}