﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a dimensão de Lotação do Quadro de Estados
	/// </summary>
	public class Lotacao : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// Código da Lotação
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Lotação
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}