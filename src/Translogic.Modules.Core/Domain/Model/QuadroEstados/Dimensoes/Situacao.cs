﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;
	using Diversos;

	/// <summary>
	/// Representa a Situação de um veículo em um determinado momento
	/// </summary>
	public class Situacao : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region PROPRIEDADES

		/// <summary>
		/// Indica se a Situação é aplicável à um Container
		/// </summary>
		public virtual bool? IndAplicavelContainer { get; set; }

		/// <summary>
		/// Indica se a Situação é aplicável à uma Locomotiva
		/// </summary>
		public virtual bool? IndAplicavelLocomotiva { get; set; }

		/// <summary>
		/// Indica se a Situação é aplicável à um Trem
		/// </summary>
		public virtual bool? IndAplicavelTrem { get; set; }

		/// <summary>
		/// Indica se a Situação é aplicável à um Vagao
		/// </summary>
		public virtual bool? IndAplicavelVagao { get; set; }

		#endregion

		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// Código da Situação
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da Situação
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}