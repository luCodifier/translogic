﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a dimensão do Responsável do Quadro de Estados 
	/// </summary>
	public class Responsavel : EntidadeBase<int?>, IDimensaoQuadroEstado
	{
		#region IDimensaoQuadroEstado MEMBROS

		/// <summary>
		/// Código do responsavel
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do responsável
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}