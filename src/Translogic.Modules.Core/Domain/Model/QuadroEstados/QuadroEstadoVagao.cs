﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados
{
	using ALL.Core.Dominio;
	using Dimensoes;

	/// <summary>
	/// Representa o Quadro de Estados do Vagão
	/// </summary>
	public class QuadroEstadoVagao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Condição de uso - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.CondicaoUso"/>
		/// </summary>
		public virtual CondicaoUso CondicaoUso { get; set; }

		/// <summary>
		/// Itercambio - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Intercambio"/>
		/// </summary>
		public virtual Intercambio Intercambio { get; set; }

		/// <summary>
		/// Localização - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Localizacao"/>
		/// </summary>
		public virtual Localizacao Localizacao { get; set; }

		/// <summary>
		/// Lotação - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Lotacao"/>
		/// </summary>
		public virtual Lotacao Lotacao { get; set; }

		/// <summary>
		/// Responsavel - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Responsavel"/>
		/// </summary>
		public virtual Responsavel Responsavel { get; set; }

		/// <summary>
		/// Situação - <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Situacao"/>
		/// </summary>
		public virtual Situacao Situacao { get; set; }

		#endregion
	}
}