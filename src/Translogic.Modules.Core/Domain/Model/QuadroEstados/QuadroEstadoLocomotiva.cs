﻿namespace Translogic.Modules.Core.Domain.Model.QuadroEstados
{
	using ALL.Core.Dominio;
	using Dimensoes;

	/// <summary>
	/// Representa o Quadro de Estado da Locomotiva
	/// </summary>
	public class QuadroEstadoLocomotiva : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Dimensão <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.CondicaoUso"/>
		/// </summary>
		public virtual CondicaoUso CondicaoUso { get; set; }

		/// <summary>
		/// Dimensão <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Intercambio"/>
		/// </summary>
		public virtual Intercambio Intercambio { get; set; }

		/// <summary>
		/// Dimensão <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Localizacao"/>
		/// </summary>
		public virtual Localizacao Localizacao { get; set; }

		/// <summary>
		/// Dimensão <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Responsavel"/>
		/// </summary>
		public virtual Responsavel Responsavel { get; set; }

		/// <summary>
		/// Dimensão <see cref="Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes.Situacao"/>
		/// </summary>
		public virtual Situacao Situacao { get; set; }

		#endregion
	}
}