﻿using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
using System;

namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao
{
    public class OSLimpezaVagaoItem : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Referência da OS
        /// </summary>
        public virtual OSLimpezaVagao OSLimpezaVagao { get; set; }

        /// <summary>
        /// Referência ao vagão
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        /// Observação
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Se houve retrabalho no vagão (item)
        /// </summary>
        public virtual int? Retrabalho { get; set; }

        /// <summary>
        /// Se o serviço foi concluído
        /// </summary>
        public virtual int? Concluido { get; set; }

        #endregion

        #region Construtores
        public OSLimpezaVagaoItem()
        {
            this.VersionDate = DateTime.Now;
        }

        #endregion
    }
}