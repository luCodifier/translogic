﻿namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao
{
    using System.ComponentModel;
    /// <summary>
    /// Tipo dps arquivos enviados por email
    /// </summary>
    public enum EnumTpArquRelOSLimpezaVagao
    {
        /// <summary>
        /// Arquivos do tipo PDF
        /// </summary>
        [Description("PDF")]
        Pdf,

        /// <summary>
        /// Arquivos do tipo XML
        /// </summary>
        [Description("XML")]
        Xml,

        /// <summary>
        /// Arquivos do tipo PDF e XML
        /// </summary>
        [Description("PEX")]
        PdfXml
    }
}