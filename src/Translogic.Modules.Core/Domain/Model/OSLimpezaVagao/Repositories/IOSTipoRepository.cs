﻿
namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IOSTipoRepository : IRepository<OSTipoServico, int>
    {
        /// <summary>
        /// Tipos OS limpeza
        /// </summary>
        /// <returns></returns>
        IList<OSTipoDto> ObterTiposOSLimpeza();

        /// <summary>
        /// Tipos OS revistamento
        /// </summary>
        /// <returns></returns>
        IList<OSTipoDto> ObterTiposOSRevistamento();
    }
}
