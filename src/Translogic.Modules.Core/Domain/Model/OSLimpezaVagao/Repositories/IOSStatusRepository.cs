﻿
namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IOSStatusRepository : IRepository<OSStatus, int>
    {
        IList<OSStatusDto> ObterStatus();
    }
}
