﻿
namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using System;
using ALL.Core.Dominio;

    public interface IOSLimpezaVagaoRepository : IRepository<OSLimpezaVagao, int>
    {
        /// <summary>
        /// Retornar lista de OS de acordo com filtros e com paginação
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="idLocal"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="retrabalho"></param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoDto> ObterConsultaListaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal, 
            string idLocal, string numOs, string idTipo, string idStatus, bool? retrabalho);

        /// <summary>
        /// Retornar listagem e OS de limpeza e seus vagões para exportação
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="numOs"></param>
        /// <param name="idTipo"></param>
        /// <param name="idStatus"></param>
        /// <param name="retrabalho"></param>
        /// <returns></returns>
        IList<OSLimpezaVagaoExportaDto> ObterConsultaOsParaExportar(DateTime? dataInicial, DateTime? dataFinal,
          string local, string numOs, string idTipo, string idStatus, bool? retrabalho);

    }
}
