﻿
namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using System.Collections.Generic;

    public interface IOSLimpezaVagaoItemRepository : IRepository<OSLimpezaVagaoItem, int>
    {
        /// <summary>
        /// Buscar vagões para adicionar na OS
        /// </summary>
        /// <param name="detalhesPaginacaoWeb">paginação e ordenação</param>
        /// <param name="numOs">Número da OS</param>
        /// <param name="listaDeVagoes">Vagões separados por ponto e vírgula</param>
        /// <param name="serie">série</param>
        /// <param name="local">local</param>
        /// <param name="linha">linha</param>
        /// <param name="idLotacao">id da lotação</param>
        /// <param name="sequencioInicio">Sequencia inicial</param>
        /// <param name="sequencioFim">Sequencia final</param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoItemDto> ObterConsultaDeVagoesParaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? numOs,
              string listaDeVagoes, string serie, string local, string linha, string idLotacao, int sequencioInicio, int sequencioFim);

        /// <summary>
        /// Lista de lotações dos vagões da OS
        /// </summary>
        /// <returns></returns>
        IList<OSVagaoItemLotacaoDto> ObterLotacoes();

        /// <summary>
        /// Retornar os vagões de uma OS
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        ResultadoPaginado<OSLimpezaVagaoItemDto> ObterVagoesDaOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idOs);

        /// <summary>
        /// Criar uma nova OS e os itens da OS (vagões)
        /// </summary>
        /// <param name="osLimpezaVagaoItens"></param>
        /// <returns></returns>
        void Inserir(IList<OSLimpezaVagaoItem> osLimpezaVagaoItens);

        /// <summary>
        /// Atualizar os vagões da OS
        /// </summary>
        /// <param name="osLimpezaVagaoItens"></param>
        void Atualizar(IList<OSLimpezaVagaoItem> osLimpezaVagaoItens);

        /// <summary>
        /// Buscar os vagoões da OS
        /// </summary>
        /// <param name="idOs">ID da OS</param>
        /// <returns></returns>
        IList<OSLimpezaVagaoItem> ObterVagoesDaOs(int idOs);

        /// <summary>
        /// Remover itens de uma lista
        /// </summary>
        /// <param name="osLimpezaVagaoItens"></param>
        void Remover(IList<OSLimpezaVagaoItem> osLimpezaVagaoItens);
    }
}
