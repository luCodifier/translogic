﻿using ALL.Core.AcessoDados;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao.Repositories
{
    /// <summary>
    /// Interface do repositório da OSLimpezaVagaoArquivo
    /// </summary>
    public interface IOSLimpezaVagaoArquivoRepository : IRepository<OSLimpezaVagaoArquivo, int?>
    {
        string ObterArqPorId(int? id);

        byte[] ObterArqPorId(string chaveOS);

        /// <summary>
        /// Insere um OSLimpezaVagao arquivo
        /// </summary>
        /// <param name="osLimpezaVagao">OSLimpezaVagao de referência</param>
        /// <param name="nomeArquivoPdf">Nome do arquivo pdf</param>
        /// <param name="nomeArquivoXml">Nome do arquivo xml</param>
        /// <returns>Retorna o objeto OSLimpezaVagaoArquivo</returns>
        OSLimpezaVagaoArquivo Inserir(OSLimpezaVagao osLimpezaVagao, string nomeArquivoPdf, string nomeArquivoXml);

        /// <summary>
        /// Insere um OSLimpezaVagao arquivo (PDF)
        /// </summary>
        /// <param name="osLimpezaVagao">OSLimpezaVagao de referência</param>
        /// <param name="nomeArquivo">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto OSLimpezaVagaoArquivo</returns>
        OSLimpezaVagaoArquivo InserirOuAtualizarPdf(OSLimpezaVagao osLimpezaVagao, string nomeArquivo);

        /// <summary>
        /// Insere um OSLimpezaVagao arquivo (XML)
        /// </summary>
        /// <param name="osLimpezaVagao">OSLimpezaVagao de referência</param>
        /// <param name="nomeArquivo">Nome do arquivo Xml</param>
        /// <returns>Retorna o objeto OSLimpezaVagaoArquivo</returns>
        OSLimpezaVagaoArquivo InserirOuAtualizarXml(OSLimpezaVagao osLimpezaVagao, string nomeArquivo);

        /// <summary>
        /// Atualiza o OSLimpezaVagao com a carta de correção
        /// </summary>
        /// <param name="osLimpezaVagao">OSLimpezaVagao de referência</param>
        /// <param name="nomeArquivo">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto OSLimpezaVagaoArquivo</returns>
        OSLimpezaVagaoArquivo AtualizarPdfCartaDeCorrecao(OSLimpezaVagao osLimpezaVagao, string nomeArquivo);

        /// <summary>
        /// Atualiza o arquivo da carta de correção
        /// </summary>
        /// <param name="osLimpezaVagao">OSLimpezaVagaode referência</param>
        /// <param name="nomeArquivo">Nome do arquivo Xml</param>
        /// <returns>Retorna o objeto OSLimpezaVagaoArquivo</returns>
        OSLimpezaVagaoArquivo AtualizarXmlCartaDeCorrecao(OSLimpezaVagao osLimpezaVagao, string nomeArquivo);

        /// <summary>
        /// Obtem uma lista de arquivos OSLimpezaVagao
        /// </summary>
        /// <param name="osLimpezaVagaoId">Codigos das OSLimpezaVagao a serem localizadas</param>
        /// <returns>Retorna os arquivos de OS de Limpeza Vagão</returns>
        IList<OSLimpezaVagaoArquivo> ObterPorIds(List<int> idsOSLimpezaVagao);
    }
}