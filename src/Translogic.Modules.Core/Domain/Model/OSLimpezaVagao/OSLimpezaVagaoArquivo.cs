﻿using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao
{
    /// <summary>
    /// Classe de armazenamento do arquivo pdf impresso
    /// </summary>
    public class OSLimpezaVagaoArquivo : EntidadeBase<int?>
    {
        /// <summary>
        /// Dados do documento (arquivo pdf)
        /// </summary>
        public virtual byte[] ArquivoPdf { get; set; }

        /// <summary>
        /// Tamanho do arquivo pdf
        /// </summary>
        public virtual int? TamanhoPdf { get; set; }

        /// <summary>
        /// Data e hora de cadastramento no pooling
        /// </summary>
        public virtual DateTime? DataHora { get; set; }

        /// <summary>
        /// Arquivo de XML 
        /// </summary>
        public virtual string ArquivoXml { get; set; }

        /// <summary>
        /// Tamanho do arquivo xml
        /// </summary>
        public virtual int? TamanhoXml { get; set; }

        /// <summary>
        /// Data e hora de cadastramento no pooling do XML
        /// </summary>
        public virtual DateTime? DataHoraXml { get; set; }

        /// <summary>
        /// Dados do documento (arquivo pdf) da carta de correção
        /// </summary>
        public virtual byte[] ArquivoPdfCartaDeCorrecao { get; set; }

        /// <summary>
        /// Arquivo de XML da carta de correção
        /// </summary>
        public virtual string ArquivoXmlCartaDeCorrecao { get; set; }
    }
}