﻿using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao
{
    public class OSTipoServico : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Descrição do Serviço
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}