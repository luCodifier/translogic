﻿using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Model.OSLimpezaVagao
{
    public class OSLimpezaVagao : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// ID da OS de limpeza parcial (pai)
        /// </summary>
        public virtual OSLimpezaVagao OsLimpezaParcial { get; set; }

        /// <summary>
        /// Data e hora
        /// </summary>
        public virtual DateTime? Data { get; set; }

        /// <summary>
        /// Fornecedor
        /// </summary>
        public virtual FornecedorOs Fornecedor { get; set; }

        /// <summary>
        /// Local do Serviço
        /// </summary>
        public virtual IAreaOperacional Local { get; set; }

        /// <summary>
        /// TipoServico
        /// </summary>
        public virtual OSTipoServico TipoServico { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public virtual OSStatus Status { get; set; }

        /// <summary>
        /// Data e hora da entrega da OS para o fornecedor
        /// </summary>
        public virtual DateTime DataHoraEntregaOSFornecedor { get; set; }

        /// <summary>
        /// Data e hora da devolução da OS para a Rumo
        /// </summary>
        public virtual DateTime DataHoraDevolucaoOS { get; set; }

        /// <summary>
        /// Nome do Aprovador na Rumo
        /// </summary>
        public virtual string NomeAprovadorRumo { get; set; }

        /// <summary>
        /// Matrícula do Aprovador na Rumo
        /// </summary>
        public virtual string MatriculaAprovadorRumo { get; set; }

        /// <summary>
        /// Nome do Aprovador no Fornecedor
        /// </summary>
        public virtual string NomeAprovadorFornecedor { get; set; }

        /// <summary>
        /// Matrícula do Aprovador no Fornecedor
        /// </summary>
        public virtual string MatriculaAprovadorFornecedor { get; set; }

        /// <summary>
        /// Justificativa de cancelamento
        /// </summary>
        public virtual string JustificativaCancelamento { get; set; }

        /// <summary>
        /// Data da ultima alteração
        /// </summary>
        public virtual DateTime DataUltimaAlteracao { get; set; }

        /// <summary>
        /// Usuário que criou
        /// </summary>
        public virtual string Usuario { get; set; }

        #endregion

        #region Construtores
        public OSLimpezaVagao()
        {
            VersionDate = DateTime.Now;
            DataUltimaAlteracao = DateTime.Now;
            Data = DateTime.Now;
        }
        #endregion
    }
}