﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa classe de Bolar bo CTE
	/// </summary>
	public class BolarCte : EntidadeBase<int>
	{
		#region PROPRIEDADES

        /// <summary>
        ///    CNPJ da empresa Emitente do Cte
        /// </summary>
        public virtual long CnpjEmit { get; set; }

        /// <summary>
        ///    UF da empresa Emitente do Cte
        /// </summary>
        public virtual string UfEmit { get; set; }

        /// <summary>
        ///    Tipo de Serviço do Cte
        /// </summary>
        public virtual string TipoServico { get; set; }

        /// <summary>
        ///    Chave do Cte
        /// </summary>
        public virtual string ChaveCte { get; set; }

        /// <summary>
        ///    CNPJ da Empresa Remetente do Cte
        /// </summary>
        public virtual long CnpjRem { get; set; }

        /// <summary>
        ///    CNPJ da Empresa Destinatária do Cte
        /// </summary>
        public virtual long CnpjDest { get; set; }

        /// <summary>
        ///    Peso do Cte
        /// </summary>
        public virtual double? PesoCte { get; set; }

		#endregion
	}
}