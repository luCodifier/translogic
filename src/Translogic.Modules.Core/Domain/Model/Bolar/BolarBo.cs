﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
	using System;
	using System.ComponentModel;
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Representa classe de Bolar bo Bolar
	/// </summary>
	public class BolarBo : EntidadeBase<int>
	{
		#region CONSTRUTORES

		/// <summary>
		/// Construtor vazio
		/// </summary>
		public BolarBo()
		{
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Indica o fluxo comarcial
		/// </summary>
		public virtual int FluxoComercial { get; set; }

		/// <summary>
		/// Fluxo Comercial Argentino
		/// </summary>
		public virtual string FluxoArg { get; set; }

		/// <summary>
		/// Area operacional do BO
		/// </summary>
		public virtual string AreaOperacional { get; set; }

		/// <summary>
		/// Nome do usuário 
		/// </summary>
		public virtual string NomeUsuario { get; set; }

		/// <summary>
		/// Nome do arquivo que originou
		/// </summary>
		public virtual string NomeArquivo { get; set; }

		/// <summary>
		/// Situação do bobolar
		/// </summary>
		public virtual string Situacao { get; set; }

		/// <summary>
		/// Situação do bobolar
		/// </summary>
		public virtual DateTime TimeStamp { get; set; }

		/// <summary>
		/// Vagões do registro
		/// </summary>
		public virtual string Vagoes { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string Notas { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual double Valor { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string SerieDespacho { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string NumeroDespacho { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string CartaPorte { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string DescricaoErr { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string DespachoPorVagao { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string IcManutencao { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string FluxoOriginal { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string Justificativa { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string FerrInter { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string RegimeInter { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string SerieDespachoInter { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual string NumeroDespachoInter { get; set; }

		/// <summary>
		/// Notas do registro
		/// </summary>
		public virtual DateTime? DataDespachoInter { get; set; }

		/// <summary>
		/// Fluxo de outra ferrovia
		/// </summary>
		public virtual string FluxoOutraFerrovia { get; set; }

		/// <summary>
		/// Chave do CT-e quando de outra ferrovia
		/// </summary>
		public virtual string ChaveCteOutraFerrovia { get; set; }

		#endregion
	}
}