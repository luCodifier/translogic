﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Bolar
{
    public class BoBolarConfCargaAut : EntidadeBase<int>
    {
        public string Cliente { get; set; }
        public string AreaOperacional { get; set; }
        public string IndCargaTerm { get; set; }
        public string IndCargaPatio { get; set; }
        public int LoIdtLco { get; set; }
        public string Segmento { get; set; }
        public DateTime Data { get; set; }
        public string LibManual { get; set; }
    }
}