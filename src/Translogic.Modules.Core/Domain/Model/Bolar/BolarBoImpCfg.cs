﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Entidade BolarBoImpCfg
	/// </summary>
	public class BolarBoImpCfg : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo do cliente de envio dos arquivos.
		/// </summary>
		/// <example> Bunge (BAL), ATT (LLD), Rhall (RHL) </example>
		public virtual string CodigoEmpresa { get; set; }

		/// <summary>
		/// Descrição da empresa
		/// </summary>
		public virtual string DescricaoEmpresa { get; set; }

		/// <summary>
		/// Diretorio de gravação do arquivo
		/// </summary>
		public virtual string LocalArquivo { get; set; }

		/// <summary>
		/// Diretorio de gravação do arquivo de log
		/// </summary>
		public virtual string LocalArquivoLog { get; set; }

		/// <summary>
		/// Indica se a configuração está ativa
		/// </summary>
		public virtual bool IndAtivo { get; set; }

		/// <summary>
		/// caminho de armazenamento dos arquivos recebidos pelo EDI com erro (nao processados)
		/// </summary>
		public virtual string LocalArquivoErro { get; set; }

		/// <summary>
		/// Indica se a configuração é de intercambio
		/// </summary>
		public virtual bool IndIntercambio { get; set; }

		/// <summary>
		/// CNPJ da empresa
		/// </summary>
		public virtual string Cnpj { get; set; }
	}
}