﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    public interface IBoBolarConfCargaAutRepository : IRepository<BoBolarConfCargaAut, int>
    {
    }
}