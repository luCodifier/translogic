﻿namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="BolarNf"/>
    /// </summary>
    public interface IBolarNfRepository : IRepository<BolarNf, int>
    {
        /// <summary>
        /// Retorna bolar nf por número da nota fiscal
        /// </summary>
        /// <param name="numeroNotaFiscal">Número nota fiscal</param>
        /// <returns>Bolar Bo Nf</returns>
        BolarNf ObterPorNumeroNota(string numeroNotaFiscal);

		/// <summary>
		/// Obter Todas as Nfe nao processadas na Vw_Nfe
		/// </summary>
		/// <param name="data">Data limite para pesquisa</param>
		/// <returns>Retorna lista de chave</returns>
		IList<string> ObterNfeNaoProcessadas(DateTime data);

    	/// <summary>
    	/// Obter Todas as Nfe do SISPAT nao processadas na Vw_Nfe
    	/// </summary>
    	/// <param name="data">Data limite para pesquisa</param>
    	/// <returns>Retorna lista de chave</returns>
    	IList<string> ObterNfeNaoProcessadasSispat(DateTime data);
    }
}