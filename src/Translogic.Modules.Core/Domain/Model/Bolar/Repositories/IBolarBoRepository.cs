﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Contrato do Repositório de <see cref="BolarBo"/>
    /// </summary>
    public interface IBolarBoImpCfgRepository : IRepository<BolarBoImpCfg, int>
    {
        /// <summary>
        /// Obter por cnpj
        /// </summary>
		/// <param name="cnpj">CNPJ da empresa</param>
        /// <returns>Objeto BolarBoImpCfg</returns>
        BolarBoImpCfg ObterPorCnpjAtivo(string cnpj);

        /// <summary>
        /// Lista de expedidores
        /// </summary>
        /// <param name="query"> Filtro para os clientes</param>
        /// <returns>Lista de clientes</returns>
        List<EmpresaPesquisaPorNome> ObterListaNomesExpedidoresDescResumida(string query);

        IList<EmpresaResumidaDto> ObterTodasAtivas();

        IList<EmpresaResumidaDto> ObterTodasComFaturamentoAutomatico();
    }
}