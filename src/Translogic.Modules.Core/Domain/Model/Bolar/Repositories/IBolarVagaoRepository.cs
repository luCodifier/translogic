﻿namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="BolarVagao"/>
    /// </summary>
    public interface IBolarVagaoRepository : IRepository<BolarVagao, int>
    {
        /// <summary>
        /// Retorna bolar vagao pelo código vagão
        /// </summary>
        /// <param name="codigovagao">Código do vagão</param>
        /// <returns>Bolar Vagao objeto</returns>
        BolarVagao ObterPorCodigoVagao(string codigovagao);
    }
}