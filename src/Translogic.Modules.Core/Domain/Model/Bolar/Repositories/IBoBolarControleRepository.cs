﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    public interface IBoBolarControleRepository : IRepository<BoBolarControle, int>
    {
        IList<BoBolarControle> ObterTodos(int[] boBolarIds);
    }
}
