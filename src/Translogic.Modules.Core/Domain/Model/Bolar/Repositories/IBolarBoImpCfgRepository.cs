﻿using System;
using System.Collections.Generic;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    using System;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Contrato do Repositório de <see cref="BolarBo"/>
    /// </summary>
    public interface IBolarBoRepository : IRepository<BolarBo, int>
    {
        /// <summary>
        /// Obter por código
        /// </summary>
        /// <param name="codigo">Código Bolar Bo</param>
        /// <returns>Objeto Bolar Bo</returns>
        BolarBo ObterPorCodigo(string codigo);

        ResultadoPaginado<ProcessamentoFaturamentoAutomaticoDto> Pesquisar(DetalhesPaginacao detalhesPaginacao, DateTime dtInicial, DateTime dtFinal,
                                                                           string vagao, string processado,
                                                                           string cliente, string local, string fluxo);

        IList<ProcessamentoFaturamentoAutomaticoDto> Exportar(DateTime dtInicial, DateTime dtFinal, string vagao,
                                                              string processado, string cliente, string local,
                                                              string fluxo);

        IList<EmpresaBloqueadaFaturamentoManualDto> ObterEmpresasBloqueadasFaturamentoManual();

        bool EmpresaBloqueadaFaturamentoManual(string fluxo);

        ResultadoPaginado<RecebimentoArquivoEdiDto> PesquisarRecebimentoArquivoEdi(DetalhesPaginacao detalhesPaginacao, DateTime dtInicial, DateTime dtFinal, string fluxoComercial, string origem, string destino, string situacao, string lstVagoes, string cliente363);
    }
}