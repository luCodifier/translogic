﻿namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="BolarNf"/>
    /// </summary>
    public interface IBolarCteRepository : IRepository<BolarCte, int>
    {
        /// <summary>
        /// Retorna bolar ctes por Chave do CTE
        /// </summary>
        /// <param name="chave">Chave do CTE</param>
        /// <returns>Bolar Bo Cte</returns>
        BolarCte ObterPorChave(string chave);
    }
}