﻿namespace Translogic.Modules.Core.Domain.Model.Bolar.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;

    public interface IBolarBoBolarCteRepository : IRepository<BolarBoBolarCte, int>
    {
        IList<BolarBoBolarCte> ObterPorIdCte(int id);

        IList<BolarBoBolarCte> ObterPorIdBolar(int id);
    }
}