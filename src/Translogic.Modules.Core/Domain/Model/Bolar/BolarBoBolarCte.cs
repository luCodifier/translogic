﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;

    public class BolarBoBolarCte : EntidadeBase<int>
    {
        /// <summary>
        /// Bolar bo  Bolar
        /// </summary>
        public virtual BolarBo BolarBo { get; set; }

        /// <summary>
        /// Ctes bo  Bolar
        /// </summary>
        public virtual BolarCte BolarCte { get; set; }    
    }
}