﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
    using System;
    using System.ComponentModel;
    using ALL.Core.Dominio;
    using Trem.Veiculo.Vagao;

    /// <summary>
    /// Representa classe de Bolar Nota Fiscal
    /// </summary>
    public class BolarNf : EntidadeBase<int>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor vazio
        /// </summary>
        public BolarNf()
        {
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Número nota fiscal
        /// </summary>
        public virtual int NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Serie Nota Fiscal
        /// </summary>
        public virtual string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Tif nota fiscal
        /// </summary>
        public virtual string TifNotaFiscal { get; set; }

        /// <summary>
        /// Modelo nota fiscal
        /// </summary>
        public virtual int ModeloNotaFiscal { get; set; }

        /// <summary>
        /// Valor nota fiscal
        /// </summary>
        public virtual double ValorNotaFiscal { get; set; }

        /// <summary>
        /// Peso nota fiscal
        /// </summary>
        public virtual double PesoNotaFiscal { get; set; }

        /// <summary>
        /// Data emissão nota
        /// </summary>
        public virtual DateTime? DataEmissao { get; set; }

        /// <summary>
        /// Uf do remetente da nota fiscal
        /// </summary>
        public virtual string UfRemetente { get; set; }

        /// <summary>
        /// Cnpj do remetente da nota fiscal
        /// </summary>
        public virtual long CnpjRemetente { get; set; }

        /// <summary>
        /// Inscrição estadual remetente da nota fiscal
        /// </summary>
        public virtual string InscricaoEstadualRemetente { get; set; }

        /// <summary>
        /// Uf destino da nota fiscal
        /// </summary>
        public virtual string UfDestino { get; set; }

        /// <summary>
        /// Cnpj destino da nota fiscal
        /// </summary>
        public virtual long CnpjDestino { get; set; }

        /// <summary>
        /// Inscrição estadual destino da nota fiscal
        /// </summary>
        public virtual string IncricaoEstadualDestino { get; set; }

        /// <summary>
        /// Numero do Container 
        /// </summary>
        public virtual string NumeroContainer { get; set; }

        /// <summary>
        /// Bo Bolar Vagão
        /// </summary>
        public virtual BolarVagao VagaoBolar { get; set; }

        /// <summary>
        /// Contador da Nota fiscal
        /// </summary>
        public virtual int Contador { get; set; }

        /// <summary>
        /// Contador da Nota fiscal
        /// </summary>
        public virtual string FilialEmissora { get; set; }
        
        /// <summary>
        /// Nota fiscal Eletrônica
        /// </summary>
        public virtual int? Nfe { get; set; }

        /// <summary>
        /// Chave Nota fiscal Eletrônica
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Placa tração Nota 
        /// </summary>
        public virtual string PlacaTracao { get; set; }

        /// <summary>
        /// Placa reboque Nota
        /// </summary>
        public virtual string PlacaReboque { get; set; }

        /// <summary>
        /// Peso total nota fiscal
        /// </summary>
        public virtual double PesoTotalNf { get; set; }

        #endregion
    }
}