﻿namespace Translogic.Modules.Core.Domain.Model.Bolar
{
    using System;
    using System.ComponentModel;
    using ALL.Core.Dominio;

    /// <summary>
    /// Representa classe de Bolar Vagão
    /// </summary>
    public class BolarVagao : EntidadeBase<int>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor vazio
        /// </summary>
        public BolarVagao()
        {
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Indica o vagão 
        /// </summary>
        public virtual string Vagao { get; set; }

        /// <summary>
        /// Número despacho
        /// </summary>
        public virtual int? NumeroDespacho { get; set; }

        /// <summary>
        /// Peso Real das notas
        /// </summary>
        public virtual double PesoReal { get; set; }

        /// <summary>
        /// Peso total das notas
        /// </summary>
        public virtual double PesoTotal { get; set; }

        /// <summary>
        /// Valor Real das notas
        /// </summary>
        public virtual double ValorReal { get; set; }

        /// <summary>
        /// Md bolar vagão
        /// </summary>
        public virtual string Md { get; set; }

        /// <summary>
        /// Bolar bo  Bolar
        /// </summary>
        public virtual BolarBo BolarBo { get; set; }

        /// <summary>
        /// Contador bolar bo
        /// </summary>
        public virtual int Contador { get; set; }

        /// <summary>
        /// Consolid Id
        /// </summary>
        public virtual string ConsolidId { get; set; }

        /// <summary>
        /// Despacho id 
        /// </summary>
        public virtual int? Despacho { get; set; }

        /// <summary>
        /// S K Srd das notas
        /// </summary>
        public virtual int? SkSrd { get; set; }

        /// <summary>
        /// S kNum Srd
        /// </summary>
        public virtual string SkNumSrd { get; set; }

        /// <summary>
        /// Serie Despacho Interc
        /// </summary>
        public virtual string SerieDespachoInterc { get; set; }

        /// <summary>
        /// Notas do registro
        /// </summary>
        public virtual string DescricaoErr { get; set; }

        /// <summary>
        /// Numero Despacho Interc
        /// </summary>
        public virtual int? NumeroDespachoInterc { get; set; }

        /// <summary>
        /// Data Carregamento 
        /// </summary>
        public virtual DateTime DataCarregamento { get; set; }

        /// <summary>
        /// Notas do registro
        /// </summary>
        public virtual double Tara { get; set; }
        
        #endregion
    }
}