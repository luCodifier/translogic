﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Bolar
{
    public class BoBolarControle: EntidadeBase<int>
    {
        public virtual int BoIdBo { get; set; }
        public virtual int NumTentativas { get; set; }
        public virtual string BcIndLibTela { get; set; }
        public virtual DateTime Data { get; set; }

        public virtual void AlterarPermissao()
        {
            switch (BcIndLibTela.ToUpper())
            {
                case "S":
                    BcIndLibTela = "N";
                    break;
                case "N":
                    BcIndLibTela = "S";
                    break;
            }
        }
    }
}