namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
	/// Representa um Grupo de Fluxo comercial, agrupa <see cref="FluxoComercial">Fluxos Comerciais</see> com caracter�sticas comuns 
	/// </summary>
	/// <remarks>
	/// TODO: ATRIBUTOS QUE NAO FORAM COLOCADOS POR FALTA DE DOCUMENTACAO NA TABELA
	///  GF_DAT_UIN
	///  GF_ID_GPC
	///  TV_COD_TV
	///  AO_ORIPOS_RT
	///  AO_DSTPOS_RT
	///  CD_ID_CD_ORG
	///  CD_ID_CD_DST
	///  CD_ORIPOS_RT
	///  CD_DSTPOS_RT
	///  MR_ID_MR_ORG
	///  MR_ID_MR_DST
	///  GF_MIX_FROTA
	///  GF_MIX_AGREG
	///  GF_MIX_TERCE
	///  UNR_COD_UNR
	/// </remarks>
	public class GrupoFluxoComercial : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Classe do Vag�o do Grupo de Fluxo - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.ClasseVagao"/>
		/// </summary>
		public virtual ClasseVagao ClasseVagao { get; set; }

		/// <summary>
		/// C�digo do Grupo de Fluxo
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Data de ativa��o do Grupo de fluxo
		/// </summary>
		public virtual DateTime? DataAtivacao { get; set; }

		/// <summary>
		/// Data de Cria��o do Grupo de Fluxo
		/// </summary>
		public virtual DateTime DataCriacao { get; set; }

		/// <summary>
		/// Descri��o do Grupo de Fluxo
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Esta��o de Destino do Grupo de Fluxo
		/// </summary>
		public virtual EstacaoMae Destino { get; set; }

		/// <summary>
		/// Empresa do Grupo de Fluxo
		/// </summary>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Indica se o Grupo de Fluxo est� Ativo
		/// </summary>
		public virtual bool? IndAtivo { get; set; }

		/// <summary>
		/// Indica se pode haver descarregamento no final de semana na esta��o
		/// </summary>
		public virtual bool? IndDescarregarFinalSemana { get; set; }

		/// <summary>
		/// Mercadoria do Grupo de Fluxo
		/// </summary>
		public virtual Mercadoria Mercadoria { get; set; }

		/// <summary>
		/// Esta��o de Origem do Grupo de Fluxo
		/// </summary>
		public virtual EstacaoMae Origem { get; set; }

		/// <summary>
		/// Produto do Grupo de Fluxo
		/// </summary>
		public virtual Produto Produto { get; set; }

		/// <summary>
		/// Quantidade de horas que leva para executar o carregamento
		/// </summary>
		public virtual int? QuantidadeHorasCarregamento { get; set; }

		/// <summary>
		/// Quantidade de horas que leva para executar o descarregamento
		/// </summary>
		public virtual int? QuantidadeHorasDescarregamento { get; set; }

		/// <summary>
		/// Quantidade de horas de transit time do fluxo
		/// </summary>
		public virtual int? QuantidadeHorasTransitTime { get; set; }

		/// <summary>
		/// Quantidade de horas de vig�ncia do fluxo
		/// </summary>
		public virtual int? QuantidadeHorasVigencia { get; set; }

		/// <summary>
		/// Tipo do grupo de fluxo - <see cref="TipoGrupoFluxoEnum"/>
		/// </summary>
		public virtual TipoGrupoFluxoEnum Tipo { get; set; }

		/// <summary>
		/// Unidade de Neg�cio do Grupo de Fluxo
		/// </summary>
		public virtual UnidadeNegocio UnidadeNegocio { get; set; }

		/// <summary>
		/// Usu�rio que ativou o grupo de fluxo
		/// </summary>
		public virtual string UsuarioAtivacao { get; set; }

		/// <summary>
		/// Usu�rio que criou o Grupo de Fluxo
		/// </summary>
		public virtual string UsuarioCriacao { get; set; }

		/// <summary>
		/// �ltimo Usu�rio que alterou o Grupo de Fluxo
		/// </summary>
		public virtual string UsuarioUltimaAlteracao { get; set; }

		#endregion
	}
}