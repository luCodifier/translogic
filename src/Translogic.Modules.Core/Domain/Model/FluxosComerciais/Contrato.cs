﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

	/// <summary>
	/// Representa o Contrato de um pedido
	/// </summary>
	/// <remarks>
	/// Apesar de no banco existir o campo do Fluxo o correto é pegar o Contrato que está no fluxo e nao o contrário
	/// </remarks>
	public class Contrato : EntidadeBase<int?>, IContrato
	{
		#region PROPRIEDADES

		/// <summary>
		/// Condição de frete - <see cref="CondicaoFrete"/>
		/// </summary>
		public virtual CondicaoFrete CondicaoFrete { get; set; }

		/// <summary>
		/// Empresa do contrato
		/// </summary>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Empresa de cobrança do contrato
		/// </summary>
		public virtual EmpresaCliente EmpresaCobranca { get; set; }

		/// <summary>
		/// Empresa destinatária
		/// </summary>
		public virtual EmpresaCliente EmpresaDestinataria { get; set; }

		/// <summary>
		/// Empresa de faturamento do contrato
		/// </summary>
		public virtual EmpresaCliente EmpresaFaturamento { get; set; }

		/// <summary>
		/// Empresa pagadora
		/// </summary>
		public virtual IEmpresa EmpresaPagadora { get; set; }

		/// <summary>
		/// Empresa remetente
		/// </summary>
		public virtual EmpresaCliente EmpresaRemetente { get; set; }

		/// <summary>
		/// Ferrovia que fatura o carregamento
		/// </summary>
		/// <remarks>
		/// Foi feito o Enumeration, pois os valores que estão aqui vem do SAP e são diferentes da sigla que está na tabela "Empresa"
		/// </remarks>
		public virtual FerroviaFaturamentoEnum? FerroviaFaturamento { get; set; }

		/// <summary>
		/// Indica se o fluxo é de container
		/// </summary>
		public virtual bool? IndFluxoContainer { get; set; }

		/// <summary>
		/// Data de Início de Vigência
		/// </summary>
		public virtual DateTime InicioVigencia { get; set; }

		/// <summary>
		/// Número do contrato
		/// </summary>
		public virtual string NumeroContrato { get; set; }

		/// <summary>
		/// Prazo do contrato - em horas
		/// </summary>
		public virtual int? Prazo { get; set; }

		/// <summary>
		/// Prazo de cancelamento - em horas
		/// </summary>
		public virtual int? PrazoCancelamento { get; set; }

		/// <summary>
		/// Tempo de trânsito - em horas
		/// </summary>
		public virtual int? TempoTransito { get; set; }

		/// <summary>
		/// Data de Término de Vigência
		/// </summary>
		public virtual DateTime? TerminoVigencia { get; set; }

		/// <summary>
		/// Volume contratado
		/// </summary>
		public virtual double? VolumeContratado { get; set; }

		/// <summary>
		/// Código da unidade de medida
		/// </summary>
		public virtual string CodigoUnidadeMedida { get; set; }

		/// <summary>
		/// Alíquota do ICMS
		/// </summary>
		public virtual double? ValorAliquotaIcmsProduto { get; set; }

		/// <summary>
		/// Base de cálculo
		/// </summary>
		public virtual double? ValorBaseCalculo { get; set; }

		/// <summary>
		/// Sigla do CFOP
		/// </summary>
		public virtual string Cfop { get; set; }

		/// <summary>
		/// Descrição do CFOP
		/// </summary>
		public virtual string DescricaoCfop { get; set; }

		/// <summary>
		/// Valor da base de calculo de substituicao do produto
		/// </summary>
		public virtual double? ValorBaseCalculoSubstituicaoProduto { get; set; }

		/// <summary>
		/// CFOP do produto
		/// </summary>
		public virtual string CfopProduto { get; set; }

		/// <summary>
		/// Aliquota do icms de substituição do produto
		/// </summary>
		public virtual double? AliquotaIcmsSubstProduto { get; set; }

		/// <summary>
		/// Mensagem Fiscal
		/// </summary>
		public virtual string MensagemFiscal { get; set; }

	    /// <summary>
	    /// Mensagem Fiscal
	    /// </summary>
        public virtual string MensagemFiscalExp { get; set; }

		/// <summary>
		/// Tipo de tributação
		/// </summary>
		public virtual int? TipoTributacao { get; set; }

		/// <summary>
		/// Desconto por peso
		/// </summary>
		public virtual double? DescontoPorPeso { get; set; }

		/// <summary>
		/// Data e hora de cadastro / atualização do registro
		/// </summary>
		public virtual DateTime DataHoraAtualizacao { get; set; }

		/// <summary>
		/// Percentual de aliquota do Icms
		/// </summary>
		public virtual double? PercentualAliquotaIcms { get; set; }

		/// <summary>
		/// Percentual de desconto
		/// </summary>
		public virtual double? PercentualDesconto { get; set; }

		/// <summary>
		/// Empresa remetente fiscal
		/// </summary>
		public virtual EmpresaCliente EmpresaRemetenteFiscal { get; set; }

		/// <summary>
		/// Empresa destinataria fiscal
		/// </summary>
		public virtual EmpresaCliente EmpresaDestinatariaFiscal { get; set; }

		/// <summary>
		/// Percentual de Seguro
		/// </summary>
		public virtual double? PercentualSeguro { get; set; }

		/// <summary>
		/// Unidade Monetária do Contrato
		/// </summary>
		public virtual string UnidadeMonetaria { get; set; }

		/// <summary>
		/// Codigo do fluxo comercial 
		/// </summary>
		public virtual string CodigoFluxoComercial { get; set; }

		/// <summary>
		/// Percentual da aliquota de cofins
		/// </summary>
        public virtual double? PercentualCofins { get; set; }

		/// <summary>
		/// Percentual da aliquota de Pis
		/// </summary>
        public virtual double? PercentualPis { get; set; }

        /// <summary>
        /// Percentual da Cprb
        /// </summary>
        public virtual double? PercentualCprb { get; set; }

        /// <summary>
        /// Alíquota do PIS
        /// </summary>
        public virtual double? ValorAliquotaPisProduto { get; set; }

        /// <summary>
        /// Alíquota do COFINS
        /// </summary>
        public virtual double? ValorAliquotaCofinsProduto { get; set; }

        /// <summary>
        /// TarifaTotal do contrato
        /// </summary>
        public virtual double? TarifaTotal { get; set; }

        /// <summary>
        /// IndIeToma do contrato
        /// </summary>
        public virtual int? IndIeToma { get; set; }

	    /// <summary>
        /// Cfop Anulacao
	    /// </summary>
        public virtual string CfopAnulacao { get; set; }

	    /// <summary>
        /// Desc Cfop Anulacao
	    /// </summary>
        public virtual string DescCfopAnulacao { get; set; }

        #endregion
	}
}
