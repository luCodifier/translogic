namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
    /// Interface de repositório de Grupo de Fluxo Comercial
    /// </summary>
    public interface IGrupoFluxoComercialRepository : IRepository<GrupoFluxoComercial, int>
    {
    }
}