namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de historico de contrato
	/// </summary>
	public interface IContratoHistoricoRepository : IRepository<ContratoHistorico, int?>
	{
		/// <summary>
		/// Obt�m a lista de hist�ricos de contrato pelo contrato
		/// </summary>
		/// <param name="contrato">Contrato a ser pesquisado no hist�rico</param>
		/// <returns>Retorna a lista historica dos contratos</returns>
		IList<ContratoHistorico> ObterPorContrato(Contrato contrato);
	}
}