namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
	using System;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de contrato
	/// </summary>
	public interface IContratoRepository : IRepository<Contrato, int?>
	{
		/// <summary>
		/// Obtem o contrato pelo fluxo e pela data
		/// </summary>
		/// <param name="codigoFluxoComercial">C�digo do fluxo comercial</param>
		/// <param name="dataVigencia">Data de vig�ncia</param>
		/// <returns>Objeto contrato</returns>
		Contrato ObterPorFluxoData(string codigoFluxoComercial, DateTime dataVigencia);
	}
}