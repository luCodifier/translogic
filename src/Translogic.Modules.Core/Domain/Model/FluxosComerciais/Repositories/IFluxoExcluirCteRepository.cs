﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de fluxo excluir CT-e
	/// </summary>
	public interface IFluxoExcluirCteRepository : IRepository<FluxoExcluirCte, string>
	{
	}
}