namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio de agrupamento de CFOP
	/// </summary>
	public interface ICfopAgrupamentoRepository : IRepository<CfopAgrupamento, int>
	{
		/// <summary>
		/// Obt�m o CfopAgrupamento pelo exemplo de CFOP
		/// </summary>
		/// <param name="cfopProduto">CFOP do produto de exemplo</param>
		/// <returns>Objeto CfopAgrupamento</returns>
		CfopAgrupamento ObterPorCfopExemplo(string cfopProduto);
	}
}