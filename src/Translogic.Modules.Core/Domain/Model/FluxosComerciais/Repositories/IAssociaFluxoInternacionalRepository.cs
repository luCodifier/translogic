namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
	/// Interface de reposit�rio de Associa Fluxo
	/// </summary>
	public interface IAssociaFluxoInternacionalRepository : IRepository<AssociaFluxoInternacional, int?>
	{
        /// <summary>
        /// Obt�m a associa��o do fluxo comercial com o fluxo internacional
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Objeto AssociaFluxoInternacional </returns>
        AssociaFluxoInternacional ObterPorFluxoComercial(FluxoComercial fluxo);
	}
}