﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;

    /// <summary>
    /// Interface do repository responsável pela persistência de Logs de impressão
    /// </summary>
    public interface ILogImpressaoReciboRepository : IRepository<LogImpressaoRecibo, int?>
    {
        /// <summary>
        /// Realiza a persistência do Log quando o usuário imprimir
        /// </summary>
        /// <param name="idTrem">Id do trem selecionado para impressão</param>
        /// <param name="idVagao">Id do vagão selecionado para impressão</param>
        /// <param name="idEmpresa">Id da empresa selecionada para impressão</param>
        /// <param name="usuarioId">Id do usuário logado que está realizando a impressão</param>
        void GravarLogImpressao(int? idTrem, int? idVagao, int? idEmpresa, int usuarioId);
    }
}