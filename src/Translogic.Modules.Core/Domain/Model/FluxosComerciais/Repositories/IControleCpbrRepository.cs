namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
	/// Interface de repositório de Associa Fluxo
	/// </summary>
    public interface IControleCpbrRepository : IRepository<ControleCpbr, int?>
	{
       
	}
}