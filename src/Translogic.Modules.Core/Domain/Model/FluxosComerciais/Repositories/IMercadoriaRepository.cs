namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de reposit�rio de mercadoria
    /// </summary>
    public interface IMercadoriaRepository : IRepository<Mercadoria, int>
    {
        /// <summary>
        /// Obt�m a mercadoria pelo c�digo
        /// </summary>
        /// <param name="codigoMercadoria">C�digo da mercadoria</param>
        /// <returns>Objeto <see cref="Mercadoria"/></returns>
        Mercadoria ObterPorCodigo(string codigoMercadoria);

        /// <summary>
        /// Lista de mercadorias
        /// </summary>
        /// <param name="query"> Filtro para os mercadorias</param>
        /// <returns>Lista de Mercadorias</returns>
        List<Mercadoria> ObterLista(string query);

        IList<Mercadoria> ObterTodasMercadorias();
    }
}