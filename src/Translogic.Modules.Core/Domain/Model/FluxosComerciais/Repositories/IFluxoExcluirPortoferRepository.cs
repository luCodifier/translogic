namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de repositório de fluxo excluir portofer
    /// </summary>
    public interface IFluxoExcluirPortoferRepository : IRepository<FluxoExcluirPortofer, string>
    {
    }
}