namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
	/// Interface de reposit�rio de Associa Fluxo
	/// </summary>
	public interface IAssociaFluxoRepository : IRepository<AssociaFluxo, int?>
	{
        /// <summary>
        /// Obt�m a associa��o do fluxo comercial com o fluxo internacional ativo
        /// </summary>
        /// <param name="fluxo"> Objeto FluxoComercial. </param>
        /// <returns> Objeto AssociaFluxo </returns>
        AssociaFluxo ObterPorFluxoAtivo(FluxoComercial fluxo);
	}
}