namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de repositório de densidade da mercadoria
    /// </summary>
    public interface IDensidadeMercadoriaRepository : IRepository<DensidadeMercadoria, string>
    {
    }
}