namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using ALL.Core.AcessoDados;
    using FluxosComerciais;

    /// <summary>
	/// Interface de reposit�rio de Fluxo Internacional
	/// </summary>
	public interface IFluxoInternacionalRepository : IRepository<FluxoInternacional, int?>
	{
        /// <summary>
        /// Obt�m o fluxo internacional pelo c�digo do fluxo informado
        /// </summary>
        /// <param name="codigoFluxo"> The codigo fluxo. </param>
        /// <returns> Objeto FluxoInternacional </returns>
        FluxoInternacional ObterPorCodigo(string codigoFluxo);
	}
}