namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    /// <summary>
    /// Interface de reposit�rio de Fluxo Comercial
    /// </summary>
    public interface IFluxoComercialRepository : IRepository<FluxoComercial, int>
    {
        /// <summary>
        /// Obter fluxo comercial por c�digo
        /// </summary>
        /// <param name="codigofluxo">C�digo do fluxo comercial</param>
        /// <returns>Fluxo Comercial</returns>
        FluxoComercial ObterPorCodigo(string codigofluxo);

        /// <summary>
        /// Obter fluxo comercial por c�digo
        /// </summary>
        /// <param name="codigofluxo">C�digo do fluxo comercial</param>
        /// <returns>Fluxo Comercial</returns>
        FluxoComercial ObterPorCodigoSimplificado(string codigofluxo);

        /// <summary>
        /// Obt�m os fluxos para o Rateio
        /// </summary>
        /// <param name="empresaRemetente"> C�digo da empresa remetente. </param>
        /// <param name="terminalDestino"> C�digo da empresa do terminal de destino. </param>
        /// <param name="terminalOrigem"> C�digo da empresa do terminal de origem. </param>
        /// <param name="ncm"> C�digo NCM da mercadoria. </param>
        /// <returns> Retorna a lista de fluxos </returns>
        IList<FluxoComercial> ObterFluxoParaRateio(int empresaRemetente, string terminalDestino, string terminalOrigem, string ncm);

        /// <summary>
        /// Obt�m os fluxos pelo id do Despacho Translogic
        /// </summary>
        /// <param name="despacho"> id do Despacho. </param>
        /// <param name="codfluxo"> C�digo do Fluxo Comercial. </param>
        /// <returns> Retorna Fluxo Comercial </returns>
        FluxoComercial ObterFluxoPorDespachoFluxo(int despacho, string codfluxo);

        /// <summary>
        /// Obter fluxo comercial ferroviario por c�digo
        /// </summary>
        /// <param name="codigoFluxo">C�digo do fluxo comercial</param>
        /// <returns>Fluxo Comercial</returns>
        FluxoComercial ObterFerroviarioPorCodigo(string codigoFluxo);

        /// <summary>
        /// Obter fluxos comercial pelo cliente
        /// </summary>
        /// <param name="clienteDescResumida">Descri��o resumida do cliente</param>
        /// <returns>Resumos dos fluxos</returns>
        IEnumerable<ResumoFluxosDto> ObterFluxosResumidosPorDescResumida(string clienteDescResumida);

        /// <summary>
        /// Obt�m os segmentos diferentes do sistema
        /// </summary>
        IList<string> ObterSegmentos();

        /// <summary>
        /// Obt�m a densidade da mercadoria do fluxo
        /// </summary>
        decimal ObterDensidadeLiquido(string codigoFluxo);

        /// <summary>
        /// Obtem os fluxo comerciais por empresa correntista
        /// </summary>
        /// <param name="cnpjCorrentista">C�digo da empresa correntista dos Fluxos Comerciais</param>
        /// <param name="cnpjOrigem">C�digo da empresa de origem dos Fluxos Comerciais (opcional ainda)</param>
        /// <param name="cnpjDestino">C�digo da empresa de destino dos Fluxos Comerciais (opcional ainda)</param>
        /// <param name="somenteVigentes">Filtro de Fluxos Comerciais Vigentes apenas</param>
        /// <returns>Fluxo Comercial</returns>
        IList<FluxoComercial> ObterFluxoPorEmpresaCorrentista(string cnpjCorrentista, string cnpjOrigem, string cnpjDestino, bool somenteVigentes);
    }
}