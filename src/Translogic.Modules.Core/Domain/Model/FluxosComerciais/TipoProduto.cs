namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa o Tipo de um determinado Produto
	/// </summary>
	public class TipoProduto : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo do Tipo do Produto
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descri��o do Tipo do Produto
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}