﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Model Fluxo Internacional
	/// </summary>
	public class FluxoInternacional : EntidadeBase<int?>
    {
        #region PROPRIEDADES

		/// <summary>
		///  Gets or sets Codigo Fluxo Internacional
		/// </summary>
		public virtual string CodigoFluxoInternacional { get; set; }

      #endregion
    }
}
