namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe que representa um pooling de Nfe
	/// </summary>
	public class NfePooling : EntidadeBase<int>
	{
		#region PROPRIEDADES
		/// <summary>
		/// Chave da Nfe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Data da Nfe
		/// </summary>
		public virtual DateTime DataNfe { get; set; }

		/// <summary>
		/// Permite reenvio
		/// </summary>
		public virtual bool? PermiteReenvio { get; set; }

		/// <summary>
		/// Data do pr�ximo processamento
		/// </summary>
		public virtual DateTime? DataProxProcessamento { get; set; }

		/// <summary>
		/// Num tentativas
		/// </summary>
		public virtual int NumTentativas { get; set; }
		
		/// <summary>
		/// Base de origem da Nfe
		/// </summary>
		public virtual string OrigemNfe { get; set; }

		/// <summary>
		/// Indica se est� em processamento
		/// </summary>
		public virtual bool IndEmProcessamento { get; set; }
		#endregion
	}
}