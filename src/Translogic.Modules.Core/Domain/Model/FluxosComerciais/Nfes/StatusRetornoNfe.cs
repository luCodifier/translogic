namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Status de retorno do simconsultas
	/// </summary>
	public class StatusRetornoNfe : EntidadeBase<int>
	{
		/// <summary>
		/// C�digo do retorno
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Mensagem de retorno
		/// </summary>
		public virtual string Mensagem { get; set; }

		/// <summary>
		/// Indica se � permitido o reenvido pelo BO
		/// </summary>
		public virtual bool IndPermiteReenvioBo { get; set; }

		/// <summary>
		/// Indica se � permitido o reenvido pela tela
		/// </summary>
		public virtual bool IndPermiteReenvioTela { get; set; }

		/// <summary>
		/// Indica se � para liberar a digita��o em caso de reenvio de tela
		/// </summary>
		public virtual bool IndLiberarDigitacao { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}