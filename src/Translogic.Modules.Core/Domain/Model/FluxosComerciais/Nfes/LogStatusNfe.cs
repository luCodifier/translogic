namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Log do Status da NFe
	/// </summary>
	public class LogStatusNfe : EntidadeBase<int>
	{
		/// <summary>
		/// Status de retorno da nfe
		/// </summary>
		public virtual StatusRetornoNfe StatusRetornoNfe { get; set; }

		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}