namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Acesso;

	/// <summary>
	/// Status da NFe de Anulacao
	/// </summary>
    public class StatusNfeLog : EntidadeBase<int>
	{
		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double PesoUtilizadoAntigo { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double VolumeUtilizadoAntigo { get; set; }

		/// <summary>
		/// Origem da NFe
		/// </summary>
		public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Status da Nfe
        /// </summary>
        public virtual StatusNfe StatusNfe { get; set; }
	}
}