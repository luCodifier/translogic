namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using Diversos.Simconsultas;

    /// <summary>
    /// Interface de nota fiscal eletronica
    /// </summary>
    public interface INotaFiscalEletronicaDistribuicao
    {
        /// <summary>
        /// Tipo da NFe - SCO qdo simConsultas ou EDI.
        /// </summary>
        string Tipo { get; set; }

        /// <summary>
        /// Chave NFE do Simconsultas
        /// </summary>
        string ChaveNfe { get; set; }

        /// <summary>
        /// CNPJ Emitente
        /// </summary>
        string CnpjEmitente { get; set; }

        /// <summary>
        /// CNPJ Destinatario
        /// </summary>
        string CnpjDestinatario { get; set; }

        /// <summary>
        /// Data emiss�o da nota
        /// </summary>
        DateTime DataEmissao { get; set; }

        /// <summary>
        /// Identificador do cliente
        /// </summary>
        int IdCliente { get; set; }

        /// <summary>
        /// Inscri��o estadual do emitente
        /// </summary>
        string InscricaoEstadualEmitente { get; set; }

        /// <summary>
        /// Raz�o social do emitente
        /// </summary>
        string RazaoSocialEmitente { get; set; }

        /// <summary>
        /// Nome fantasia do emitente
        /// </summary>
        string NomeFantasiaEmitente { get; set; }

        /// <summary>
        /// Telefone do emitente
        /// </summary>
        string TelefoneEmitente { get; set; }

        /// <summary>
        /// Logradouro do emitente
        /// </summary>
        string LogradouroEmitente { get; set; }

        /// <summary>
        /// Complemento do emitente
        /// </summary>
        string ComplementoEmitente { get; set; }

        /// <summary>
        /// Numero do emitente
        /// </summary>
        string NumeroEmitente { get; set; }

        /// <summary>
        /// Bairro do emitente
        /// </summary>
        string BairroEmitente { get; set; }

        /// <summary>
        /// C�digo do municipio do emitente
        /// </summary>
        string CodigoMunicipioEmitente { get; set; }

        /// <summary>
        /// Municipio do emitente
        /// </summary>
        string MunicipioEmitente { get; set; }

        /// <summary>
        /// Cep do emitente
        /// </summary>
        string CepEmitente { get; set; }

        /// <summary>
        /// Unidade Federativa do emitente
        /// </summary>
        string UfEmitente { get; set; }

        /// <summary>
        /// C�digo do pais do emitente
        /// </summary>
        string CodigoPaisEmitente { get; set; }

        /// <summary>
        /// Pais do emitente
        /// </summary>
        string PaisEmitente { get; set; }

        /// <summary>
        /// Inscri��o estadual do destinatario
        /// </summary>
        string InscricaoEstadualDestinatario { get; set; }

        /// <summary>
        /// Raz�o social do destinatario
        /// </summary>
        string RazaoSocialDestinatario { get; set; }

        /// <summary>
        /// Nome fantasia destinatario
        /// </summary>
        string NomeFantasiaDestinatario { get; set; }

        /// <summary>
        /// Telefone destinatario
        /// </summary>
        string TelefoneDestinatario { get; set; }

        /// <summary>
        /// Logradouro destinatario
        /// </summary>
        string LogradouroDestinatario { get; set; }

        /// <summary>
        /// Numero do destinatario
        /// </summary>
        string NumeroDestinatario { get; set; }

        /// <summary>
        /// Complemento do destinatario
        /// </summary>
        string ComplementoDestinatario { get; set; }

        /// <summary>
        /// Bairro do destinatario
        /// </summary>
        string BairroDestinatario { get; set; }

        /// <summary>
        /// C�digo do municipio do destinatario
        /// </summary>
        string CodigoMunicipioDestinatario { get; set; }

        /// <summary>
        /// Municipio do destinatario
        /// </summary>
        string MunicipioDestinatario { get; set; }

        /// <summary>
        /// Unidade federativa do destinatario
        /// </summary>
        string UfDestinatario { get; set; }

        /// <summary>
        /// Cep do distinatario
        /// </summary>
        string CepDestinatario { get; set; }

        /// <summary>
        /// C�digo do pais destinatario
        /// </summary>
        string CodigoPaisDestinatario { get; set; }

        /// <summary>
        /// Nome do pais destinatario
        /// </summary>
        string PaisDestinatario { get; set; }

        /// <summary>
        /// Numero da nita fiscal
        /// </summary>
        int NumeroNotaFiscal { get; set; }

        /// <summary>
        /// S�rie da nota fiscal
        /// </summary>
        string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Placa do veiculo
        /// </summary>
        string Placa { get; set; }

        /// <summary>
        /// Peso total da nota fiscal
        /// </summary>
        double Peso { get; set; }

        /// <summary>
        /// Valor totaldo frete
        /// </summary>
        double ValorTotalFrete { get; set; }

        /// <summary>
        /// Valor total da nota fiscal
        /// </summary>
        double Valor { get; set; }

        /// <summary>
        /// C�digo do IBGE
        /// </summary>
        int CodigoUfIbge { get; set; }

        /// <summary>
        /// C�digo da chave de acesso
        /// </summary>
        int CodigoChaveAcesso { get; set; }

        /// <summary>
        /// Natureza de opera��o da nota fiscal
        /// </summary>
        string NaturezaOperacao { get; set; }

        /// <summary>
        ///  Forma de pagamento da nota fiscal
        /// </summary>
        FormaPagamentoEnum? FormaPagamento { get; set; }

        /// <summary>
        /// Modelo da nota fiscal
        /// </summary>
        string ModeloNota { get; set; }

        /// <summary>
        /// Data de sa�da da nota fiscal
        /// </summary>
        DateTime? DataSaida { get; set; }

        /// <summary>
        /// Tipo da nota fiscal
        /// </summary>
        TipoNotaFiscalEnum? TipoNotaFiscal { get; set; }

        /// <summary>
        /// C�digo do IBGE
        /// </summary>
        int CodigoIbge { get; set; }

        /// <summary>
        /// Formato de impress�o da nota fiscal
        /// </summary>
        FormatoImpressaoEnum? FormatoImpressao { get; set; }

        /// <summary>
        /// Tipo de emiss�o da nota fiscal
        /// </summary>
        TipoEmissaoEnum? TipoEmissao { get; set; }

        /// <summary>
        /// Digito verificador da chave
        /// </summary>
        int DigitoVerificadorChave { get; set; }

        /// <summary>
        /// Tipo de ambiente da nota fiscal
        /// </summary>
        TipoAmbienteEnum? TipoAmbiente { get; set; }

        /// <summary>
        /// Finalidade da emiss�o da nota fiscal
        /// </summary>
        FinalidadeEmissaoEnum? FinalidadeEmissao { get; set; }

        /// <summary>
        /// Processo de emiss�o da nota fiscal
        /// </summary>
        ProcessoEmissaoEnum? ProcessoEmissao { get; set; }

        /// <summary>
        /// Vers�o do processo
        /// </summary>
        string VersaoProcesso { get; set; }

        /// <summary>
        /// Valor da base de c�lculo do ICMS
        /// </summary>
        double ValorBaseCalculoIcms { get; set; }

        /// <summary>
        /// Valor do ICMS da nota fiscal
        /// </summary>
        double ValorIcms { get; set; }

        /// <summary>
        /// Valor da base de c�lculo da sunstitui��o tribut�ria
        /// </summary>
        double ValorBaseCalculoSubTributaria { get; set; }

        /// <summary>
        /// Valor da substitui��o tribut�ria
        /// </summary>
        double ValorSubTributaria { get; set; }

        /// <summary>
        /// Valor total do produto
        /// </summary>
        double ValorProduto { get; set; }

        /// <summary>
        /// Valor total do frete
        /// </summary>
        double ValorFrete { get; set; }

        /// <summary>
        /// Valor do seguro
        /// </summary>
        double ValorSeguro { get; set; }

        /// <summary>
        /// Valor do desconto da nota fiscal
        /// </summary>
        double ValorDesconto { get; set; }

        /// <summary>
        /// Valor do imposto de importa��o
        /// </summary>
        double ValorImpostoImportacao { get; set; }

        /// <summary>
        /// Valor do IPI
        /// </summary>
        double ValorIpi { get; set; }

        /// <summary>
        /// Valor do PIS
        /// </summary>
        double ValorPis { get; set; }

        /// <summary>
        /// Valor do Cofins
        /// </summary>
        double ValorCofins { get; set; }

        /// <summary>
        /// Outros valores
        /// </summary>
        double ValorOutro { get; set; }

        /// <summary>
        /// CNPJ da transportadora
        /// </summary>
        string CnpjTransportadora { get; set; }

        /// <summary>
        /// Nome da transportadora
        /// </summary>
        string NomeTransportadora { get; set; }

        /// <summary>
        /// Modelo do frete
        /// </summary>
        ModeloFreteEnum? ModeloFrete { get; set; }

        /// <summary>
        /// Endere�o da transportadora
        /// </summary>
        string EnderecoTransportadora { get; set; }

        /// <summary>
        /// Unidade federativa da transportadora
        /// </summary>
        string UfTransportadora { get; set; }

        /// <summary>
        /// Placa do reboque
        /// </summary>
        string PlacaReboque { get; set; }

        /// <summary>
        /// Peso bruto da nota fiscal
        /// </summary>
        double PesoBruto { get; set; }

        /// <summary>
        /// Municipio da transportadora
        /// </summary>
        string MunicipioTransportadora { get; set; }

        /// <summary>
        /// Cancelado pelo emitente
        /// </summary>
        bool CanceladoEmitente { get; set; }

        /// <summary>
        /// Data e hora da grava��o
        /// </summary>
        DateTime DataHoraGravacao { get; set; }

        /// <summary>
        /// Id da nota fiscal eletronica.
        /// </summary>
        int? Id { get; set; }

        /// <summary>
        /// Volume da Nfe
        /// </summary>
        double? Volume { get; set; }
    }
}