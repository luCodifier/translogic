namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos;

	/// <summary>
	/// Status da NFe
	/// </summary>
    public class HistoricoNfe : EntidadeBase<int>
	{
		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Peso antigo da Nota
		/// </summary>
		public virtual double? PesoAntigo { get; set; }

		/// <summary>
		/// Volume antigo da Nota
		/// </summary>
		public virtual double? VolumeAntigo { get; set; }

		/// <summary>
		/// Peso atual da Nota
		/// </summary>
		public virtual double PesoAtual { get; set; }

		/// <summary>
		/// Peso atual da Nota
		/// </summary>
		public virtual double VolumeAtual { get; set; }

		/// <summary>
		/// Data de Altera��o dos valores
		/// </summary>
		public virtual DateTime DataAlteracao { get; set; }

		/// <summary>
		/// Origem da NFe
		/// </summary>
		public virtual string Origem { get; set; }

        /// <summary>
        /// Usu�rio que alterou a NFe
        /// </summary>
        public virtual string MatriculaUsuario { get; set; }

        /// <summary>
        /// Altera��o conferida
        /// </summary>
        public virtual bool AlteracaoConferida { get; set; }

        /// <summary>
        /// Status de Envio de e-mail
        /// </summary>
        public virtual StatusEnvioEmailEnum StatusEnvioEmail { get; set; }

        /// <summary>
        /// Natureza da Opera��o
        /// </summary>
        public virtual NaturezaDaOperacaoEnum NaturezaDaOperacao { get; set; }
        
        /// <summary>
        /// Gets or Sets CnpjOrigemAntigo
        /// </summary>
        public virtual string CnpjOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets IeOrigemAntigo
        /// </summary>
        public virtual string IeOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets RazaoSocialOrigemAntigo
        /// </summary>
        public virtual string RazaoSocialOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets NomeFantasiaOrigemAntigo
        /// </summary>
        public virtual string NomeFantasiaOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets FoneOrigemAntigo
        /// </summary>
        public virtual string FoneOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets LogradouroOrigemAntigo
        /// </summary>
        public virtual string LogradouroOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets NumeroOrigemAntigo
        /// </summary>
        public virtual string NumeroOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets ComplementoOrigemAntigo
        /// </summary>
        public virtual string ComplementoOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets BairroOrigemAntigo
        /// </summary>
        public virtual string BairroOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CodMunicipioOrigemAntigo
        /// </summary>
        public virtual string CodMunicipioOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets MunicipioOrigemAntigo
        /// </summary>
        public virtual string MunicipioOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CepOrigemAntigo
        /// </summary>
        public virtual string CepOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CodPaisOrigemAntigo
        /// </summary>
        public virtual string CodPaisOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets PaisOrigemAntigo
        /// </summary>
        public virtual string PaisOrigemAntigo { get; set; }

        /// <summary>
        /// Gets or Sets UfOrigemAntigo
        /// </summary>
        public virtual string UfOrigemAntigo { get; set; }
        
        /// <summary>
        /// Gets or Sets CnpjOrigemNovo
        /// </summary>
        public virtual string CnpjOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets IeOrigemNovo
        /// </summary>
        public virtual string IeOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets RazaoSocialOrigemNovo
        /// </summary>
        public virtual string RazaoSocialOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets NomeFantasiaOrigemNovo
        /// </summary>
        public virtual string NomeFantasiaOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets FoneOrigemNovo
        /// </summary>
        public virtual string FoneOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets LogradouroOrigemNovo
        /// </summary>
        public virtual string LogradouroOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets NumeroOrigemNovo
        /// </summary>
        public virtual string NumeroOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets ComplementoOrigemNovo
        /// </summary>
        public virtual string ComplementoOrigemNovo { get; set; }
        
        /// <summary>
        /// Gets or Sets BairroOrigemNovo
        /// </summary>
        public virtual string BairroOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets CodMunicipioOrigemNovo
        /// </summary>
        public virtual string CodMunicipioOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets MunicipioOrigemNovo
        /// </summary>
        public virtual string MunicipioOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets CepOrigemNovo
        /// </summary>
        public virtual string CepOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets CodPaisOrigemNovo
        /// </summary>
        public virtual string CodPaisOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets PaisOrigemNovo
        /// </summary>
        public virtual string PaisOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets UfOrigemNovo
        /// </summary>
        public virtual string UfOrigemNovo { get; set; }

        /// <summary>
        /// Gets or Sets CnpjDestinoAntigo
        /// </summary>
        public virtual string CnpjDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets IeDestinoAntigo
        /// </summary>
        public virtual string IeDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets RazaoSocialDestinoAntigo
        /// </summary>
        public virtual string RazaoSocialDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets NomeFantasiaDestinoAntigo
        /// </summary>
        public virtual string NomeFantasiaDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets FoneDestinoAntigo
        /// </summary>
        public virtual string FoneDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets LogradouroDestinoAntigo
        /// </summary>
        public virtual string LogradouroDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets NumeroDestinoAntigo
        /// </summary>
        public virtual string NumeroDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets ComplementoDestinoAntigo
        /// </summary>
        public virtual string ComplementoDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets BairroDestinoAntigo
        /// </summary>
        public virtual string BairroDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CodMunicipioDestinoAntigo
        /// </summary>
        public virtual string CodMunicipioDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets MunicipioDestinoAntigo
        /// </summary>
        public virtual string MunicipioDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CepDestinoAntigo
        /// </summary>
        public virtual string CepDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CodPaisDestinoAntigo
        /// </summary>
        public virtual string CodPaisDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets PaisDestinoAntigo
        /// </summary>
        public virtual string PaisDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets UfDestinoAntigo
        /// </summary>
        public virtual string UfDestinoAntigo { get; set; }

        /// <summary>
        /// Gets or Sets CnpjDestinoNovo
        /// </summary>
        public virtual string CnpjDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets IeDestinoNovo
        /// </summary>
        public virtual string IeDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets RazaoSocialDestinoNovo
        /// </summary>
        public virtual string RazaoSocialDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets NomeFantasiaDestinoNovo
        /// </summary>
        public virtual string NomeFantasiaDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets FoneDestinoNovo
        /// </summary>
        public virtual string FoneDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets LogradouroDestinoNovo
        /// </summary>
        public virtual string LogradouroDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets NumeroDestinoNovo
        /// </summary>
        public virtual string NumeroDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets ComplementoDestinoNovo
        /// </summary>
        public virtual string ComplementoDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets BairroDestinoNovo
        /// </summary>
        public virtual string BairroDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets CodMunicipioDestinoNovo
        /// </summary>
        public virtual string CodMunicipioDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets MunicipioDestinoNovo
        /// </summary>
        public virtual string MunicipioDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets CepDestinoNovo
        /// </summary>
        public virtual string CepDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets CodPaisDestinoNovo
        /// </summary>
        public virtual string CodPaisDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets PaisDestinoNovo
        /// </summary>
        public virtual string PaisDestinoNovo { get; set; }

        /// <summary>
        /// Gets or Sets UfDestinoNovo
        /// </summary>
        public virtual string UfDestinoNovo { get; set; }
	}
}