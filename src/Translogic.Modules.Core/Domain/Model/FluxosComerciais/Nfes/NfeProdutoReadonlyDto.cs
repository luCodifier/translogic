﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using Translogic.Modules.Core.Domain.Model.Diversos.Simconsultas;

    public class NfeProdutoReadonlyDto
    {
        /// <summary>
        /// Tipo da NFe - SCO qdo simConsultas ou EDI.
        /// </summary>
        public virtual string Tipo { get; set; }

        /// <summary>
        /// ID da NFE readonly
        /// </summary>
        public virtual decimal? IdNfe { get; set; }

        /// <summary>
        /// Codigo do Produto
        /// </summary>
        public virtual string CodigoProduto { get; set; }

        /// <summary>
        /// Descrição do Produto
        /// </summary>
        public virtual string DescricaoProduto { get; set; }

        /// <summary>
        /// Código NCM
        /// </summary>
        public virtual string CodigoNcm { get; set; }

        /// <summary>
        /// Genero do produto
        /// </summary>
        public virtual string Genero { get; set; }

        /// <summary>
        /// Código Cfop
        /// </summary>
        public virtual string Cfop { get; set; }

        /// <summary>
        /// Unidade comercial do produto
        /// </summary>
        public virtual string UnidadeComercial { get; set; }

        /// <summary>
        /// Quantidade comercial do produto
        /// </summary>
        public virtual decimal QuantidadeComercial { get; set; }

        /// <summary>
        /// Valor unitario da comercialização
        /// </summary>
        public virtual decimal ValorUnitarioComercializacao { get; set; }

        /// <summary>
        /// Valor do produto
        /// </summary>
        public virtual decimal ValorProduto { get; set; }

        /// <summary>
        /// Unidade tributavel do produto
        /// </summary>
        public virtual string UnidadeTributavel { get; set; }

        /// <summary>
        /// Quantidade tributavel do produto
        /// </summary>
        public virtual decimal QuantidadeTributavel { get; set; }

        /// <summary>
        /// Valor unitario da tributação
        /// </summary>
        public virtual decimal ValorUnitarioTributacao { get; set; }

        /// <summary>
        /// Icms emitente
        /// </summary>
        public virtual string IcmsEmitente { get; set; }

        /// <summary>
        /// Icms cst NfeProdutoSimconsultas
        /// </summary>
        public virtual string IcmsCst { get; set; }

        /// <summary>
        /// Pis cst do produto
        /// </summary>
        public virtual string PisCst { get; set; }

        /// <summary>
        /// Cofins cst do produto
        /// </summary>
        public virtual string CofinsCst { get; set; }

        /// <summary>
        /// Valor da base calculo do icms
        /// </summary>
        public virtual decimal ValorBaseCalculoIcms { get; set; }

        /// <summary>
        /// Aliquota do icms
        /// </summary>
        public virtual decimal AliquotaIcms { get; set; }

        /// <summary>
        /// Valor do Icms
        /// </summary>
        public virtual decimal ValorIcms { get; set; }

        /// <summary>
        /// Valor da base de calculo do Ipi
        /// </summary>
        public virtual decimal ValorBaseCalculoIpi { get; set; }

        /// <summary>
        /// Percentual do Ipi
        /// </summary>
        public virtual decimal PercentualIpi { get; set; }

        /// <summary>
        /// Valor do Ipi
        /// </summary>
        public virtual decimal ValorIpi { get; set; }

        /// <summary>
        /// Valor da despesa adianeira
        /// </summary>
        public virtual decimal ValorDespesaAduaneira { get; set; }

        /// <summary>
        /// Valor do Iof
        /// </summary>
        public virtual decimal ValorIof { get; set; }

        /// <summary>
        /// Quantidade vendida
        /// </summary>
        public virtual decimal QuantidadeVendida { get; set; }

        /// <summary>
        /// Aliquota do pis
        /// </summary>
        public virtual decimal AliquotaPis { get; set; }

        /// <summary>
        /// Valor do Pis
        /// </summary>
        public virtual decimal ValorPis { get; set; }

        /// <summary>
        /// Quantidade vendida cofins
        /// </summary>
        public virtual decimal QuantidadeVendidaCofins { get; set; }

        /// <summary>
        /// Aliquota cofins
        /// </summary>
        public virtual decimal AliquotaCofins { get; set; }

        /// <summary>
        /// Valor cofins
        /// </summary>
        public virtual decimal ValorCofins { get; set; }

        /// <summary>
        /// Valor do ipi cst
        /// </summary>
        public virtual string IpiCst { get; set; }

        /// <summary>
        /// Valor do imposto importação
        /// </summary>
        public virtual decimal ValorImpostoImportacao { get; set; }

        /// <summary>
        /// Data e hora da gravação
        /// </summary>
        public virtual DateTime DataHoraGravacao { get; set; }
    }
}