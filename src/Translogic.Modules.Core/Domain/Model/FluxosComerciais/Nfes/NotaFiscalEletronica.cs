﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.Dominio;
    using Diversos.Simconsultas;

    /// <summary>
    /// Classe que representa a Nota Fiscal eletrônica
    /// </summary>
    public class NotaFiscalEletronica : EntidadeBase<int?>, INotaFiscalEletronica
    {
        /// <summary>
        /// Tipo da NFe
        /// </summary>
        public virtual string Tipo { get; set; }

        /// <summary>
        /// Chave NFE do Simconsultas
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// CNPJ Emitente
        /// </summary>
        public virtual string CnpjEmitente { get; set; }

        /// <summary>
        /// CNPJ Destinatario
        /// </summary>
        public virtual string CnpjDestinatario { get; set; }

        /// <summary>
        /// Data emissão da nota
        /// </summary>
        public virtual DateTime DataEmissao { get; set; }

        /// <summary>
        /// Identificador do cliente
        /// </summary>
        public virtual int IdCliente { get; set; }

        /// <summary>
        /// Inscrição estadual do emitente
        /// </summary>
        public virtual string InscricaoEstadualEmitente { get; set; }

        /// <summary>
        /// Razão social do emitente
        /// </summary>
        public virtual string RazaoSocialEmitente { get; set; }

        /// <summary>
        /// Nome fantasia do emitente
        /// </summary>
        public virtual string NomeFantasiaEmitente { get; set; }

        /// <summary>
        /// Telefone do emitente
        /// </summary>
        public virtual string TelefoneEmitente { get; set; }

        /// <summary>
        /// Logradouro do emitente
        /// </summary>
        public virtual string LogradouroEmitente { get; set; }

        /// <summary>
        /// Complemento do emitente
        /// </summary>
        public virtual string ComplementoEmitente { get; set; }

        /// <summary>
        /// Numero do emitente
        /// </summary>
        public virtual string NumeroEmitente { get; set; }

        /// <summary>
        /// Bairro do emitente
        /// </summary>
        public virtual string BairroEmitente { get; set; }

        /// <summary>
        /// Código do municipio do emitente
        /// </summary>
        public virtual string CodigoMunicipioEmitente { get; set; }

        /// <summary>
        /// Municipio do emitente
        /// </summary>
        public virtual string MunicipioEmitente { get; set; }

        /// <summary>
        /// Cep do emitente
        /// </summary>
        public virtual string CepEmitente { get; set; }

        /// <summary>
        /// Unidade Federativa do emitente
        /// </summary>
        public virtual string UfEmitente { get; set; }

        /// <summary>
        /// Código do pais do emitente
        /// </summary>
        public virtual string CodigoPaisEmitente { get; set; }

        /// <summary>
        /// Pais do emitente
        /// </summary>
        public virtual string PaisEmitente { get; set; }

        /// <summary>
        /// Inscrição estadual do destinatario
        /// </summary>
        public virtual string InscricaoEstadualDestinatario { get; set; }

        /// <summary>
        /// Razão social do destinatario
        /// </summary>
        public virtual string RazaoSocialDestinatario { get; set; }

        /// <summary>
        /// Nome fantasia destinatario
        /// </summary>
        public virtual string NomeFantasiaDestinatario { get; set; }

        /// <summary>
        /// Telefone destinatario
        /// </summary>
        public virtual string TelefoneDestinatario { get; set; }

        /// <summary>
        /// Logradouro destinatario
        /// </summary>
        public virtual string LogradouroDestinatario { get; set; }

        /// <summary>
        /// Numero do destinatario
        /// </summary>
        public virtual string NumeroDestinatario { get; set; }

        /// <summary>
        /// Complemento do destinatario
        /// </summary>
        public virtual string ComplementoDestinatario { get; set; }

        /// <summary>
        /// Bairro do destinatario
        /// </summary>
        public virtual string BairroDestinatario { get; set; }

        /// <summary>
        /// Código do municipio do destinatario
        /// </summary>
        public virtual string CodigoMunicipioDestinatario { get; set; }

        /// <summary>
        /// Municipio do destinatario
        /// </summary>
        public virtual string MunicipioDestinatario { get; set; }

        /// <summary>
        /// Unidade federativa do destinatario
        /// </summary>
        public virtual string UfDestinatario { get; set; }

        /// <summary>
        /// Cep do distinatario
        /// </summary>
        public virtual string CepDestinatario { get; set; }

        /// <summary>
        /// Código do pais destinatario
        /// </summary>
        public virtual string CodigoPaisDestinatario { get; set; }

        /// <summary>
        /// Nome do pais destinatario
        /// </summary>
        public virtual string PaisDestinatario { get; set; }

        /// <summary>
        /// Numero da nita fiscal
        /// </summary>
        public virtual int NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Série da nota fiscal
        /// </summary>
        public virtual string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Placa do veiculo
        /// </summary>
        public virtual string Placa { get; set; }

        /// <summary>
        /// Peso total da nota fiscal
        /// </summary>
        public virtual double Peso { get; set; }

        /// <summary>
        /// Valor totaldo frete
        /// </summary>
        public virtual double ValorTotalFrete { get; set; }

        /// <summary>
        /// Valor total da nota fiscal
        /// </summary>
        public virtual double Valor { get; set; }

        /// <summary>
        /// Código do IBGE
        /// </summary>
        public virtual int CodigoUfIbge { get; set; }

        /// <summary>
        /// Código da chave de acesso
        /// </summary>
        public virtual int CodigoChaveAcesso { get; set; }

        /// <summary>
        /// Natureza de operação da nota fiscal
        /// </summary>
        public virtual string NaturezaOperacao { get; set; }

        /// <summary>
        ///  Forma de pagamento da nota fiscal
        /// </summary>
        public virtual FormaPagamentoEnum? FormaPagamento { get; set; }

        /// <summary>
        /// Modelo da nota fiscal
        /// </summary>
        public virtual string ModeloNota { get; set; }

        /// <summary>
        /// Data de saída da nota fiscal
        /// </summary>
        public virtual DateTime? DataSaida { get; set; }

        /// <summary>
        /// Tipo da nota fiscal
        /// </summary>
        public virtual TipoNotaFiscalEnum? TipoNotaFiscal { get; set; }

        /// <summary>
        /// Código do IBGE
        /// </summary>
        public virtual int CodigoIbge { get; set; }

        /// <summary>
        /// Formato de impressão da nota fiscal
        /// </summary>
        public virtual FormatoImpressaoEnum? FormatoImpressao { get; set; }

        /// <summary>
        /// Tipo de emissão da nota fiscal
        /// </summary>
        public virtual TipoEmissaoEnum? TipoEmissao { get; set; }

        /// <summary>
        /// Digito verificador da chave
        /// </summary>
        public virtual int DigitoVerificadorChave { get; set; }

        /// <summary>
        /// Tipo de ambiente da nota fiscal
        /// </summary>
        public virtual TipoAmbienteEnum? TipoAmbiente { get; set; }

        /// <summary>
        /// Finalidade da emissão da nota fiscal
        /// </summary>
        public virtual FinalidadeEmissaoEnum? FinalidadeEmissao { get; set; }

        /// <summary>
        /// Processo de emissão da nota fiscal
        /// </summary>
        public virtual ProcessoEmissaoEnum? ProcessoEmissao { get; set; }

        /// <summary>
        /// Versão do processo
        /// </summary>
        public virtual string VersaoProcesso { get; set; }

        /// <summary>
        /// Valor da base de cálculo do ICMS
        /// </summary>
        public virtual double ValorBaseCalculoIcms { get; set; }

        /// <summary>
        /// Valor do ICMS da nota fiscal
        /// </summary>
        public virtual double ValorIcms { get; set; }

        /// <summary>
        /// Valor da base de cálculo da sunstituição tributária
        /// </summary>
        public virtual double ValorBaseCalculoSubTributaria { get; set; }

        /// <summary>
        /// Valor da substituição tributária
        /// </summary>
        public virtual double ValorSubTributaria { get; set; }

        /// <summary>
        /// Valor total do produto
        /// </summary>
        public virtual double ValorProduto { get; set; }

        /// <summary>
        /// Valor total do frete
        /// </summary>
        public virtual double ValorFrete { get; set; }

        /// <summary>
        /// Valor do seguro
        /// </summary>
        public virtual double ValorSeguro { get; set; }

        /// <summary>
        /// Valor do desconto da nota fiscal
        /// </summary>
        public virtual double ValorDesconto { get; set; }

        /// <summary>
        /// Valor do imposto de importação
        /// </summary>
        public virtual double ValorImpostoImportacao { get; set; }

        /// <summary>
        /// Valor do IPI
        /// </summary>
        public virtual double ValorIpi { get; set; }

        /// <summary>
        /// Valor do PIS
        /// </summary>
        public virtual double ValorPis { get; set; }

        /// <summary>
        /// Valor do Cofins
        /// </summary>
        public virtual double ValorCofins { get; set; }

        /// <summary>
        /// Outros valores
        /// </summary>
        public virtual double ValorOutro { get; set; }

        /// <summary>
        /// CNPJ da transportadora
        /// </summary>
        public virtual string CnpjTransportadora { get; set; }

        /// <summary>
        /// Nome da transportadora
        /// </summary>
        public virtual string NomeTransportadora { get; set; }

        /// <summary>
        /// Modelo do frete
        /// </summary>
        public virtual ModeloFreteEnum? ModeloFrete { get; set; }

        /// <summary>
        /// Endereço da transportadora
        /// </summary>
        public virtual string EnderecoTransportadora { get; set; }

        /// <summary>
        /// Unidade federativa da transportadora
        /// </summary>
        public virtual string UfTransportadora { get; set; }

        /// <summary>
        /// Placa do reboque
        /// </summary>
        public virtual string PlacaReboque { get; set; }

        /// <summary>
        /// Peso bruto da nota fiscal
        /// </summary>
        public virtual double PesoBruto { get; set; }

        /// <summary>
        /// Municipio da transportadora
        /// </summary>
        public virtual string MunicipioTransportadora { get; set; }

        /// <summary>
        /// Cancelado pelo emitente
        /// </summary>
        public virtual bool CanceladoEmitente { get; set; }

        /// <summary>
        /// Data e hora da gravação
        /// </summary>
        public virtual DateTime DataHoraGravacao { get; set; }

        /// <summary>
        /// Xml da Nfe.
        /// </summary>
        public virtual string Xml { get; set; }

        /// <summary>
        /// Volume da NF-e
        /// </summary>
        public virtual double? Volume { get; set; }

        /// <summary>
        /// Lista de produtos da NFe
        /// </summary>
        public virtual List<NotaFiscalEletronicaProduto> ListaProdutos { get; set; }

        /// <summary>
        /// CNPJ do local de retirada do produto
        /// </summary>
        public virtual string CnpjRetirada { get; set; }

        /// <summary>
        /// CPF do local de retirada do produto
        /// </summary>
        public virtual string CpfRetirada { get; set; }

        /// <summary>
        /// Logradouro do local de retirada do produto
        /// </summary>
        public virtual string LogradouroRetirada { get; set; }

        /// <summary>
        /// Número do local de retirada do produto
        /// </summary>
        public virtual string NumeroRetirada { get; set; }

        /// <summary>
        /// Complemento do local de retirada do produto
        /// </summary>
        public virtual string ComplementoRetirada { get; set; }

        /// <summary>
        /// Bairro do local de retirada do produto
        /// </summary>
        public virtual string BairroRetirada { get; set; }

        /// <summary>
        /// Código do município do local de retirada do produto
        /// </summary>
        public virtual string CodigoMunicipioRetirada { get; set; }

        /// <summary>
        /// Município do local de retirada do produto
        /// </summary>
        public virtual string MunicipioRetirada { get; set; }

        /// <summary>
        /// UF do local de retirada do produto
        /// </summary>
        public virtual string UfRetirada { get; set; }


        /// <summary>
        /// CNPJ do local de entrega do produto
        /// </summary>
        public virtual string CnpjEntrega { get; set; }

        /// <summary>
        /// CPF do local de entrega do produto
        /// </summary>
        public virtual string CpfEntrega { get; set; }

        /// <summary>
        /// Logradouro do local de retirada do produto
        /// </summary>
        public virtual string LogradouroEntrega { get; set; }

        /// <summary>
        /// Número do local de retirada do produto
        /// </summary>
        public virtual string NumeroEntrega { get; set; }

        /// <summary>
        /// Complemento do local de retirada do produto
        /// </summary>
        public virtual string ComplementoEntrega { get; set; }

        /// <summary>
        /// Bairro do local de retirada do produto
        /// </summary>
        public virtual string BairroEntrega { get; set; }

        /// <summary>
        /// Código do município do local de retirada do produto
        /// </summary>
        public virtual string CodigoMunicipioEntrega { get; set; }

        /// <summary>
        /// Município do local de retirada do produto
        /// </summary>
        public virtual string MunicipioEntrega { get; set; }

        /// <summary>
        /// UF do local de retirada do produto
        /// </summary>
        public virtual string UfEntrega { get; set; }
    }
}