namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Associa uma Nota Fiscal ao Status da NFe
	/// </summary>
	public class AssociaNotaFiscalStatusNfe : EntidadeBase<int>
	{
		/// <summary>
		/// Id do Status da nfe
		/// </summary>
		public virtual int IdStatusNfe { get; set; }

		/// <summary>
		/// Id da Nota Fiscal
		/// </summary>
		public virtual int IdNotaFiscal { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}