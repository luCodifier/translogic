namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Acesso;

	/// <summary>
	/// Status da NFe
	/// </summary>
	public class StatusNfe : EntidadeBase<int>
	{
		/// <summary>
		/// Status de retorno da nfe
		/// </summary>
		public virtual StatusRetornoNfe StatusRetornoNfe { get; set; }

		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Peso da Nota
		/// </summary>
		public virtual double Peso { get; set; }

		/// <summary>
		/// Volume da Nota
		/// </summary>
		public virtual double Volume { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double PesoUtilizado { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double VolumeUtilizado { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Origem da NFe
		/// </summary>
		public virtual string Origem { get; set; }

		/// <summary>
		/// Origem da NFe
		/// </summary>
		public virtual Usuario UsuarioCadastro { get; set; }

        /// <summary>
        /// Peso Bruto da Nota
        /// </summary>
        public virtual double? PesoBruto { get; set; }

	}
}