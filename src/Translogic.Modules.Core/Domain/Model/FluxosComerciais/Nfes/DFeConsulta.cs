﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Registro para controle das ultimas consultas de nf-e 
    /// </summary>
    public class DFeConsulta : EntidadeBase<int?>
    {
        /// <summary>
        /// Data da ultima consulta 
        /// </summary>
        public virtual DateTime TimeStamp { get; set; }

        /// <summary>
        /// Id do ultimo registro baixado 
        /// </summary>
        public virtual string UltimoRegistro { get; set; }

        /// <summary>
        /// Cnpj para consulta
        /// </summary>
        public virtual string Cnpj { get; set; }

        /// <summary>
        /// Estado para consulta
        /// </summary>
        public virtual string Uf { get; set; }
    }
}