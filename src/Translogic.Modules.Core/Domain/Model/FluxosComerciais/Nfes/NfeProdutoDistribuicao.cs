﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System.Reflection;
    using Nfes;

    /// <summary>
	/// Entidade NFE Produto SimConsultas
	/// </summary>
	public class NfeProdutoDistribuicao : NotaFiscalEletronicaProduto
	{
        /// <summary>
        /// Inicializa passando o produto lido na nfe
        /// </summary>
        /// <param name="nfep">Nota Fiscal Eletrônica Produto</param>
        public NfeProdutoDistribuicao(NotaFiscalEletronicaProduto nfep)
        {
            foreach (FieldInfo field in nfep.GetType().GetFields())
            {
                GetType().GetField(field.Name).SetValue(this, field.GetValue(nfep));
            }

            foreach (PropertyInfo prop in nfep.GetType().GetProperties())
            {
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(nfep, null), null);
            }
        }

        /// <summary>
        /// Inicializa sem preencher valores
        /// </summary>
        public NfeProdutoDistribuicao()
        {
        }

		/// <summary>
		/// NFE Simconsultas
		/// </summary>
        public virtual NfeDistribuicaoReadonly NfeDistribuicaoReadonly { get; set; }
	}
}