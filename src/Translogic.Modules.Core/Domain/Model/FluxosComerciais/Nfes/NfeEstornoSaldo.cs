﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;    

    /// <summary>
    /// Informação de estorno de saldo da NFe
    /// </summary>
    public class NfeEstornoSaldo : EntidadeBase<int>
    {
        /// <summary>
        /// Justificativa do estorno
        /// </summary>
        public virtual string Justificativa { get; set; }

        /// <summary>
        /// Chave da NFe.
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Nome da pessoa que aprovou o estorno
        /// </summary>
        public virtual string Aprovador { get; set; }

        /// <summary>
        /// Peso utilizado da Nota
        /// </summary>
        public virtual double PesoUtilizado { get; set; }

        /// <summary>
        /// Peso estornado da Nota
        /// </summary>
        public virtual double PesoEstornado { get; set; }

        /// <summary>
        /// Volume utilizado da Nota
        /// </summary>
        public virtual double VolumeUtilizado { get; set; }

        /// <summary>
        /// Volume estornado da Nota
        /// </summary>
        public virtual double VolumeEstornado { get; set; }

        /// <summary>
        /// Usuário que realizou o estorno
        /// </summary>        
        public virtual Usuario UsuarioEstorno { get; set; }

        /// <summary>
        /// Data de estorno
        /// </summary>
        public virtual DateTime DataEstorno { get; set; }

        /// <summary>
        /// Nome da transação de tela que realizou o estorno
        /// </summary>
        public virtual string TransacaoEstorno { get; set; }
    }
}