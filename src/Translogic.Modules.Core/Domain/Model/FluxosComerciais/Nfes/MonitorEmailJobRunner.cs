namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe que representa as configura��es do pooling de Nfe
	/// </summary>
    public class MonitorEmailJobRunner : EntidadeBase<int>
	{
		#region PROPRIEDADES
		/// <summary>
		/// Estado
		/// </summary>
        public virtual string Processo { get; set; }

		/// <summary>
		/// Quantidade M�xima
		/// </summary>
        public virtual double ContEmail { get; set; }

        /// <summary>
        /// C�digo inicio da Nota
        /// </summary>
        public virtual DateTime TimestampAtz { get; set; }
        
		#endregion
	}
}