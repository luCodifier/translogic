﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
    using System;
    using Diversos.Simconsultas;

    /// <summary>
    /// Interface de produto de nota fiscal eletronica
    /// </summary>
    public interface INotaFiscalEletronicaProduto
    {
        /// <summary>
        /// Tipo da NFe - SCO qdo simConsultas ou EDI.
        /// </summary>
        string Tipo { get; set; }

        /// <summary>
        /// NFE readonly
        /// </summary>
        int? IdNfe { get; set; }

        /// <summary>
        /// Codigo do Produto
        /// </summary>
        string CodigoProduto { get; set; }

        /// <summary>
        /// Descrição do Produto
        /// </summary>
        string DescricaoProduto { get; set; }

        /// <summary>
        /// Código NCM
        /// </summary>
        string CodigoNcm { get; set; }

        /// <summary>
        /// Genero do produto
        /// </summary>
        string Genero { get; set; }

        /// <summary>
        /// Código Cfop
        /// </summary>
        string Cfop { get; set; }

        /// <summary>
        /// Unidade comercial do produto
        /// </summary>
        string UnidadeComercial { get; set; }

        /// <summary>
        /// Quantidade comercial do produto
        /// </summary>
        double QuantidadeComercial { get; set; }

        /// <summary>
        /// Valor unitario da comercialização
        /// </summary>
        double ValorUnitarioComercializacao { get; set; }

        /// <summary>
        /// Valor do produto
        /// </summary>
        double ValorProduto { get; set; }

        /// <summary>
        /// Unidade tributavel do produto
        /// </summary>
        string UnidadeTributavel { get; set; }

        /// <summary>
        /// Quantidade tributavel do produto
        /// </summary>
        double QuantidadeTributavel { get; set; }

        /// <summary>
        /// Valor unitario da tributação
        /// </summary>
        double ValorUnitarioTributacao { get; set; }

        /// <summary>
        /// Icms emitente
        /// </summary>
        string IcmsEmitente { get; set; }

        /// <summary>
        /// Icms cst NfeProdutoSimconsultas
        /// </summary>
        string IcmsCst { get; set; }

        /// <summary>
        /// Pis cst do produto
        /// </summary>
        string PisCst { get; set; }

        /// <summary>
        /// Cofins cst do produto
        /// </summary>
        string CofinsCst { get; set; }

        /// <summary>
        /// Modalidade da determinação da base de cálculo do icms
        /// </summary>
        ModalidadeDeterminacaoBaseCalculoIcmsEnum? ModalidadeDeterminacaoBaseCalculoIcms { get; set; }

        /// <summary>
        /// Valor da base calculo do icms
        /// </summary>
        double ValorBaseCalculoIcms { get; set; }

        /// <summary>
        /// Aliquota do icms
        /// </summary>
        double AliquotaIcms { get; set; }

        /// <summary>
        /// Valor do Icms
        /// </summary>
        double ValorIcms { get; set; }

        /// <summary>
        /// Valor da base de calculo do Ipi
        /// </summary>
        double ValorBaseCalculoIpi { get; set; }

        /// <summary>
        /// Percentual do Ipi
        /// </summary>
        double PercentualIpi { get; set; }

        /// <summary>
        /// Valor do Ipi
        /// </summary>
        double ValorIpi { get; set; }

        /// <summary>
        /// Valor da base de cálculo do imposto de importação
        /// </summary>
        double ValorBaseCalculoImpostoImportacao { get; set; }

        /// <summary>
        /// Valor da despesa adianeira
        /// </summary>
        double ValorDespesaAduaneira { get; set; }

        /// <summary>
        /// Valor do Iof
        /// </summary>
        double ValorIof { get; set; }

        /// <summary>
        /// Quantidade vendida
        /// </summary>
        double QuantidadeVendida { get; set; }

        /// <summary>
        /// Aliquota do pis
        /// </summary>
        double AliquotaPis { get; set; }

        /// <summary>
        /// Valor do Pis
        /// </summary>
        double ValorPis { get; set; }

        /// <summary>
        /// Quantidade vendida cofins
        /// </summary>
        double QuantidadeVendidaCofins { get; set; }

        /// <summary>
        /// Aliquota cofins
        /// </summary>
        double AliquotaCofins { get; set; }

        /// <summary>
        /// Valor cofins
        /// </summary>
        double ValorCofins { get; set; }

        /// <summary>
        /// Valor do ipi cst
        /// </summary>
        double IpiCst { get; set; }

        /// <summary>
        /// Valor do imposto importação
        /// </summary>
        double ValorImpostoImportacao { get; set; }

        /// <summary>
        /// Data e hora da gravação
        /// </summary>
        DateTime DataHoraGravacao { get; set; }
        
        /// <summary>
        /// Id da nota fiscal eletronica
        /// </summary>
        int? Id { get; set; }
    }
}