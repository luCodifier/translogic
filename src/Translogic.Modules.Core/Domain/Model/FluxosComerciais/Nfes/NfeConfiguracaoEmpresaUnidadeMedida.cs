namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Configuração de empresa x unidade de medida
	/// </summary>
	public class NfeConfiguracaoEmpresaUnidadeMedida : EntidadeBase<int>
	{
		/// <summary>
		/// Cnpj da empresa.
		/// </summary>
		public virtual string Cnpj { get; set; }

		/// <summary>
		/// Unidade de Medida.
		/// </summary>
		public virtual string UnidadeMedida { get; set; }

		/// <summary>
		/// Data de Cadastro.
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}