namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Log nfe Saldo
	/// </summary>
	public class LogNfeSaldo : EntidadeBase<int>
	{
		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Peso da Nota
		/// </summary>
		public virtual double Peso { get; set; }

		/// <summary>
		/// Volume da Nota
		/// </summary>
		public virtual double Volume { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double PesoUtilizado { get; set; }

		/// <summary>
		/// Peso utilizado da Nota
		/// </summary>
		public virtual double VolumeUtilizado { get; set; }

		/// <summary>
		/// Margem Percentual
		/// </summary>
		public virtual double MargemPercentual { get; set; }

		/// <summary>
		/// Saldo de Peso Disponivel
		/// </summary>
		public virtual double SaldoPeso { get; set; }

		/// <summary>
		/// Saldo Volume disponivel
		/// </summary>
		public virtual double SaldoVolume { get; set; }

		/// <summary>
		/// Peso Utilizado no Despacho
		/// </summary>
		public virtual double PesoDespacho { get; set; }

		/// <summary>
		/// Volume utilizado no Despacho
		/// </summary>
		public virtual double VolumeDespacho { get; set; }
		
		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}