﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do repositorio das consultas ao service
    /// </summary>
    public interface IDFeConsultaRepository : IRepository<DFeConsulta, int?>
    {
        /// <summary>
        /// Obtem data e registro da ultima consulta
        /// </summary>
        /// <returns>Retorna data e registro da ultima consulta, registro é utilzado para realizar a próxima consulta</returns>
        DFeConsulta ObterUltimoRegistro(string cnpj, string uf);
    }
}