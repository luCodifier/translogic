namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

	/// <summary>
	/// Interface de repositório de Monitoramento de Emails do JobRunner
	/// </summary>
    public interface IMonitorEmailJobRunnerRepository : IRepository<MonitorEmailJobRunner, int>
	{
	}
}