namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;
	using Simconsultas;

	/// <summary>
	/// Interface de reposit�rio do status de retorno da nfe
	/// </summary>
	public interface IStatusRetornoNfeRepository : IRepository<StatusRetornoNfe, int>
	{
		/// <summary>
		/// Obt�m o status pelo c�digo
		/// </summary>
		/// <param name="codigo">C�digo do status de retorno</param>
		/// <returns>Objeto StatusRetornoNfe</returns>
		StatusRetornoNfe ObterPorCodigo(string codigo);
	}
}