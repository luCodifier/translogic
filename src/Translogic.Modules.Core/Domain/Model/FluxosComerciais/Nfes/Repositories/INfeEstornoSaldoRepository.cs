﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface de repositório do estorno de saldo de nfe
    /// </summary>
    public interface INfeEstornoSaldoRepository : IRepository<NfeEstornoSaldo, int>
    {
    }
}
