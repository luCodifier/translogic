namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio do status da nfe
	/// </summary>
	public interface ILogStatusNfeRepository : IRepository<LogStatusNfe, int>
	{
		/// <summary>
		/// Obt�m todos os status pela chave
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Lista de objetos LogStatusNfe</returns>
		IList<LogStatusNfe> ObterPorChaveNfe(string chaveNfe);
	}
}