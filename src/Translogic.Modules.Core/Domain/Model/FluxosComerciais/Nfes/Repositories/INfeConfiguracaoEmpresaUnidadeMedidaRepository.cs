namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio do status da nfe
	/// </summary>
	public interface INfeConfiguracaoEmpresaUnidadeMedidaRepository : IRepository<NfeConfiguracaoEmpresaUnidadeMedida, int>
	{
		/// <summary>
		/// Obt�m a configura��o pelo cnpj
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa</param>
		/// <returns>Objeto NfeConfiguracaoEmpresaUnidadeMedida</returns>
		NfeConfiguracaoEmpresaUnidadeMedida ObterPorCnpj(string cnpj);
	}
}