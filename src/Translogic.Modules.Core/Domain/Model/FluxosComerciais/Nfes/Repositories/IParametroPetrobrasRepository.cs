﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using ALL.Core.AcessoDados;
    using NfePetrobras;

    /// <summary>
    /// Interface dos parametros da petrobras
    /// </summary>
    public interface IParametrosPetrobrasRepository : IRepository<ParametrosPetrobras, int>
    {
    }
}