namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da classe NFE produto Simconsultas
	/// </summary>
	public interface INfeProdutoDistribuicaoRepository : IRepository<NfeProdutoDistribuicao, int?>
	{
        /// <summary>
        /// Obter todos os produtos pelo Id da NFe
        /// </summary>
        /// <param name="nfeSimConsultasId">Id da NfeSimConsultas</param>
        /// <returns>Lista de produtos da Nfe</returns>
        IList<NfeProdutoDistribuicao> ObterPorNfeId(int nfeSimConsultasId);
	}
}