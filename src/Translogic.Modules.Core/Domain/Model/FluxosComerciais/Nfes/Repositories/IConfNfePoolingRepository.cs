namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

	/// <summary>
	/// Interface de repositório de Configuração de Pooling Nfe
	/// </summary>
    public interface IConfNfePoolingRepository : IRepository<ConfNfePooling, int>
	{
	}
}