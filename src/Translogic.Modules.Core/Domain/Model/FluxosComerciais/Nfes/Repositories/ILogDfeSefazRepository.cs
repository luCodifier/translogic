﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface consulta ao Repositorio Dfe
    /// </summary>
    public interface ILogDfeSefazRepository : IRepository<LogDfeSefaz, int>
    {
    }
}