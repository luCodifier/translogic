﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
    using ALL.Core.Dominio;
    using Translogic.Modules.EDI.Domain.Models.Dto;

	/// <summary>
	/// Interface do repositorio do NFE Simconsultas
	/// </summary>
	public interface INfeReadonlyRepository : IRepository<NfeReadonly, int?>
	{
		/// <summary>
		/// Obtém os dados da NFe pela chave
		/// </summary>
		/// <param name="chaveNfe"> The chave nfe. </param>
		/// <returns> Objeto NfeReadonly </returns>
		NfeReadonly ObterPorChaveNfe(string chaveNfe);

	    /// <summary>
	    /// Obtém os dados das NFes pelas chaves
	    /// </summary>
	    /// <param name="chavesNfe"> As chaves de nfes. </param>
	    /// <returns> Lista de NfeReadonly </returns>
	    IList<NfeReadonly> ObterPorChavesNfe(List<string> chavesNfe);

         /// <summary>
        /// Obtém os dados das NFes pelas chaves sem estado de sessão
        /// </summary>
        /// <param name="chavesNfe"> As chaves de nfes separadas por ','. </param>
        /// <returns> Lista de NfeReadonly </returns>
        IList<NfeReadonly> ObterPorChavesNfeSemEstado(List<string> chavesNfe);

		/// <summary>
		/// Atualiza os pesos da Nfe no EDI ou no Simconsultas
		/// </summary>
		/// <param name="nfe">Objeto da NF-e</param>
		void AtualizarPesosNfe(NfeReadonly nfe);

		/// <summary>
		/// Retorna as Nfe fora do Edi
		/// </summary>
		/// <returns>Lista de Nfe</returns>
		IList<string> ObterNfeForaEdi();

	    /// <summary>
	    /// Obtém os dados da NFe pela chave
	    /// </summary>
        /// <param name="chavesNfe"> The chave nfe. </param>
	    /// <returns> Objeto NfeReadonly </returns>
        IList<NotaTicketDto> ObterPorChaveNfe(IList<string> chavesNfe);

	    /// <summary>
	    /// Obtém os dados da Nota Fiscal pela chave da Nfe informada
	    /// </summary>
        /// <param name="idDespacho">Id do despacho da composição</param>
	    /// <returns>Nota Fiscal</returns>
        IList<NotaFiscalDto> ObterNotaFiscalPorDespachoId(int idDespacho);

        /// <summary>
        /// Obtém uma Nota Fiscal dado um ID
        /// </summary>
        /// <param name="idNotaFiscal">Id da Nota para se obter os dados</param>
        /// <returns>Nota Fiscal</returns>
	    NotaFiscalDto ObterNotaFiscalPorIdNota(int idNotaFiscal);

        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        IList<AcompanhamentoFatDto> ObterAcompanhamentos();

	    /// <summary>
	    /// Obtem todos os dados para a planilha de acompanhamento do faturamento
	    /// </summary>
	    /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
	    IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupados();

	    /// <summary>
	    /// Obtem todos os dados para a planilha de acompanhamento do faturamento
	    /// </summary>
	    /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
	    IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupadosMalhaSul();

        /// <summary>
        /// Obtem todos os dados para a planilha de acompanhamento do faturamento
        /// </summary>
        /// <returns>Retorna lista de Objetos do tipo AcompanhamentoFatDto</returns>
        IList<AcompanhamentoFatDto> ObterAcompanhamentosAgrupadosMalhaNorte();

        /// <summary>
        /// Obtém os fluxos associados aos dados da nota fiscal (remetente e destinatario fiscal)
        /// </summary>
        /// <param name="chavesNfe"> chaves nfe </param>
        /// <returns> Retorna lista de Objetos do tipo NotaFiscalFluxoAssociacaoDto </returns>
        IList<NotaFiscalFluxoAssociacaoDto> ObterFluxosAssociadosChaveNfe(DetalhesPaginacao detalhesPaginacao,
                                                                          List<string> chavesNfe,
                                                                          bool somenteVigentes);


         /// <summary>
        /// Obtem o registro dos PDFs EDI através da chave da NFe
        /// </summary>
        /// <param name="chavesNfe">Lista de chaves da NFe</param>
        /// <returns>Retorna os registros dos PDFs</returns>
        IList<NfePdfEdiDto> ObterPdfEdiPorChavesNfe(List<string> chavesNfe);
    }
}
