﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
	/// Interface do repositorio do NFE Simconsultas
	/// </summary>
	public interface INfeProdutoReadonlyRepository : IRepository<NfeProdutoReadonly, int?>
	{
        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        IList<NfeProdutoReadonly> ObterPorTipoId(int idNfe, string tipo);

        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        IList<NfeProdutoReadonlyDto> ObterPorTipoIdHql(int idNfe, string tipo);

        /// <summary>
        /// Obtém os produtos dado o id da nfe e o tipo se é edi ou simconsultas (Dettached)
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se é edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        IList<NfeProdutoReadonly> ObterPorTipoIdSemEstado(int idNfe, string tipo);
	}
}
