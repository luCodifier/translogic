namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;

    /// <summary>
	/// Interface de reposit�rio do status da nfe
	/// </summary>
	public interface INfeConfiguracaoEmpresaRepository : IRepository<NfeConfiguracaoEmpresa, int>
	{
		/// <summary>
		/// Obt�m a configura��o pelo cnpj
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa</param>
        /// <returns>Objeto NfeConfiguracaoEmpresa</returns>
        NfeConfiguracaoEmpresa ObterPorCnpj(string cnpj);

	    /// <summary>
	    /// ListarPorTrechoDe Cnpj
	    /// </summary>
	    /// <param name="pagination">detalhes da paginacao</param>
	    /// <param name="value">valor procurado no cnpj</param>
	    /// <returns>Resultado Paginado</returns>
	    ResultadoPaginado<NfeConfiguracaoEmpresa> ListarPorTrechoDeCnpj(DetalhesPaginacao pagination, string value);

        /// <summary>
        /// verifica se j� existe o cnpj
        /// </summary>
        /// <param name="cnpj">cnpj da entidade</param>
        ///  <param name="id">id da entidade</param>
        /// <returns>resultado da verificacao</returns>
        bool CnpjJaExiste(string cnpj, int id);
	}
}