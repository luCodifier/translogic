namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio do status da nfe
	/// </summary>
	public interface IAssociaNotaFiscalStatusNfeRepository : IRepository<AssociaNotaFiscalStatusNfe, int>
	{
		/// <summary>
		/// Obt�m a associa��o pelo id do status da nfe
		/// </summary>
		/// <param name="idStatusNfe">Id do status da nfe</param>
		/// <returns>Objeto AssociaNotaFiscalStatusNfe</returns>
		AssociaNotaFiscalStatusNfe ObterPorIdStatusNfe(int idStatusNfe);

		/// <summary>
		/// Obt�m a associa��o pelo id da Nota fiscal
		/// </summary>
		/// <param name="idNotaFiscal">Id da NotaFiscal</param>
		/// <returns>Objeto AssociaNotaFiscalStatusNfe</returns>
		AssociaNotaFiscalStatusNfe ObterPorIdNotaFiscal(int idNotaFiscal);
	}
}