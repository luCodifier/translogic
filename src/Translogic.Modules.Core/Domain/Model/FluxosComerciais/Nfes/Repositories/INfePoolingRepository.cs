namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;

	/// <summary>
	/// Interface de reposit�rio de Nfe Pooling
	/// </summary>
	public interface INfePoolingRepository : IRepository<NfePooling, int>
	{
		/// <summary>
		/// Obtem todas as notas no pooling que permitam reenvio
		/// </summary>
		/// <param name="numTentativas">Numero de tentarivas</param>
		/// <param name="codigoUfIbge">Codigo da UF Ibge</param>
		/// <returns>Retorna as notas do pooling</returns>
		IList<NfePooling> ObterNotasPooling(int numTentativas, string codigoUfIbge);

		/// <summary>
		/// Remove as Notas Antigas que n�o permite reenvio
		/// </summary>
		/// <param name="date"> Data de filtro</param>
		/// <param name="numMaxTentativas">Numero m�ximo de tentativas</param>
		/// <param name="codigoUfIbge">Codigo da Uf Ibge</param>
		/// <remarks>Quando ficar muito antiga ou exceder o m�ximo de tentativas deve ser removido</remarks>
		void RemoverNotasAntigasPooling(DateTime date, int numMaxTentativas, string codigoUfIbge);

		/// <summary>
		/// Remove as Notas j� processadas que est�o no pooling
		/// </summary>
		/// <param name="codigoUfIbge">Codigo da UF Ibge</param>
		void RemoverNotasExistentesPooling(string codigoUfIbge);

        /// <summary>
        /// Verifica se j� atingiu o limite de Notas que est�o processando no pooling por estado
        /// </summary>
        /// <param name="codigoUfIbge">Codigo da UF Ibge</param>
	    bool AtingiuLimiteProcessamento(string codigoUfIbge);
	}
}