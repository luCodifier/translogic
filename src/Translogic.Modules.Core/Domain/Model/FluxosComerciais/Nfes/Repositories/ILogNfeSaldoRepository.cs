namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de log de Saldo de Nfe
	/// </summary>
	public interface ILogNfeSaldoRepository : IRepository<LogNfeSaldo, int>
	{
	}
}