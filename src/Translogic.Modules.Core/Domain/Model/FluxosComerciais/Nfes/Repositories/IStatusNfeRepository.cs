namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
	/// Interface de reposit�rio do status da nfe
	/// </summary>
	public interface IStatusNfeRepository : IRepository<StatusNfe, int>
	{
		/// <summary>
		/// Obt�m o status pela chave nfe
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Objeto StatusNfe</returns>
		StatusNfe ObterPorChaveNfe(string chaveNfe);

		/// <summary>
		/// Obt�m os despachos da Nfe por Chave 
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Lista DespachosNfe</returns>
		IList<DespachosNfeDto> ObterDespachosPorChaveNfe(string chaveNfe);

        /// <summary>
        /// Apagar Registros NFe
        /// </summary>
        /// <param name="chave">chave da nfe</param>
        void ApagarRegistrosNFe(string chave);

        /// <summary>
        /// Verificar Quantidade Faturamento de uma NFe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>dto dos faturamentos da nfe</returns>
        FaturamentosNfeDto VerificarQuantidadeFaturamento(string chaveNfe);
	}
}