namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
	/// Interface de reposit�rio do status da nfe Anulacao
	/// </summary>
	public interface IStatusNfeAnulacaoRepository : IRepository<StatusNfeAnulacao, int>
	{
		/// <summary>
		/// Obt�m o status pela chave
		/// </summary>
		/// <param name="chaveNfe">Chave da Nfe</param>
		/// <returns>Objeto StatusNfe</returns>
		StatusNfeAnulacao ObterPorChaveNfe(string chaveNfe);
	}
}