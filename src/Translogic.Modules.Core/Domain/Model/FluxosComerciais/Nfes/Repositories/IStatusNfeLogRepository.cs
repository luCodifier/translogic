namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Dto;

	/// <summary>
	/// Interface de repositório do status da nfe
	/// </summary>
	public interface IStatusNfeLogRepository : IRepository<StatusNfeLog, int>
	{
	}
}