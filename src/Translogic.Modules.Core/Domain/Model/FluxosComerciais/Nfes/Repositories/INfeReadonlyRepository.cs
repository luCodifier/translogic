﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using System.Collections;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
    /// Interface do repositorio do HistoricoNfeRepository
	/// </summary>
    public interface IHistoricoNfeRepository : IRepository<HistoricoNfe, int>
	{
        /// <summary>
        /// Recupera uma lista de endereços de e-mail, para os quais devem ser enviados mensagem, quando uma nfe for alterada.
        /// </summary>
        /// <returns>lista de endereços de e-mail</returns>
	    List<string> ObterListaDeEmailsQuandoHaAlteracao();

        /// <summary>
        /// obtem a origem da nfe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>nome da tabela de origem da nfe</returns>
	    string ObterTabelaOrigemNfe(string chaveNfe);

        /// <summary>
        /// Lista de alteracoes nao aprovadas
        /// </summary>
        /// <returns>alteracoes nao aprovadas da nfe</returns>
	    List<HistoricoNfe> ListarHistoricoNfe();
	}
}
