﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
    /// Interface do repositorio do Nfe Distribuicao Repository
	/// </summary>
    public interface INfeDistribuicaoReadonlyRepository : IRepository<NfeDistribuicaoReadonly, int?>
	{
        /// <summary>
        /// Obtém os dados da NFe pela chave
        /// </summary>
        /// <param name="chaveNfe"> The chave nfe. </param>
        /// <returns> Objeto NfeReadonly </returns>
        NfeDistribuicaoReadonly ObterPorChaveNfe(string chaveNfe);
	}
}
