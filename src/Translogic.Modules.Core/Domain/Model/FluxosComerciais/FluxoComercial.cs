namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
    /// Classe que representa um fluxo comercial
    /// </summary>
    public class FluxoComercial : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// C�digo do fluxo comercial
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Contrato do fluxo - <see cref="FluxosComerciais.Contrato"/>
        /// </summary>
        public virtual Contrato Contrato { get; set; }

        /// <summary>
        /// Valor de custo do MAC
        /// </summary>
        public virtual double? CustoMAC { get; set; }

        /// <summary>
        /// Data de Vig�ncia do �ltimo contrato
        /// </summary>
        public virtual DateTime? DataVigencia { get; set; }

        /// <summary>
        /// Descri��o do fluxo comercial
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Esta��o de destino do fluxo
        /// </summary>
        public virtual EstacaoMae Destino { get; set; }

        /// <summary>
        /// Esta��o de destino de intercambio
        /// </summary>
        public virtual EstacaoMae DestinoIntercambio { get; set; }

        /// <summary>
        /// Distancia em Km da ALL
        /// </summary>
        public virtual int? DistanciaKmALL { get; set; }

        /// <summary>
        /// Distancia em Km da EFVM
        /// </summary>
        public virtual int? DistanciaKmEFVM { get; set; }

        /// <summary>
        /// Distancia em Km da FCA
        /// </summary>
        public virtual int? DistanciaKmFCA { get; set; }

        /// <summary>
        /// Distancia em Km da Ferroban
        /// </summary>
        public virtual int? DistanciaKmFERROBAN { get; set; }

        /// <summary>
        /// Distancia em Km da Ferropar
        /// </summary>
        public virtual int? DistanciaKmFERROPAR { get; set; }

        /// <summary>
        /// Distancia em Km da MESO
        /// </summary>
        public virtual int? DistanciaKmMESO { get; set; }

        /// <summary>
        /// Distancia em Km da MRS
        /// </summary>
        public virtual int? DistanciaKmMRS { get; set; }

        /// <summary>
        /// Distancia em Km da Novoeste
        /// </summary>
        public virtual int? DistanciaKmNOVOESTE { get; set; }

        /// <summary>
        /// Empresa destinat�ria - <see cref="EmpresaCliente"/>
        /// </summary>
        public virtual EmpresaCliente EmpresaDestinataria { get; set; }

        /// <summary>
        /// Empresa pagadora - <see cref="EmpresaCliente"/>
        /// </summary>
        public virtual EmpresaCliente EmpresaPagadora { get; set; }

        /// <summary>
        /// Empresa remetente - <see cref="EmpresaCliente"/>
        /// </summary>
        public virtual EmpresaCliente EmpresaRemetente { get; set; }

        /// <summary>
        /// Indica se o fluxo est� travado
        /// </summary>
        public virtual bool? FluxoTravado { get; set; }

        /// <summary>
        /// Indica se o fluxo est� ativo
        /// </summary>
        public virtual bool? IndAtivo { get; set; }

        /// <summary>
        /// Mercadoria do fluxo - <see cref="Mercadoria"/>
        /// </summary>
        public virtual Mercadoria Mercadoria { get; set; }

        /// <summary>
        /// Esta�ao de origem do fluxo
        /// </summary>
        public virtual EstacaoMae Origem { get; set; }

        /// <summary>
        /// Esta��o de Origem de Intercambio
        /// </summary>
        public virtual EstacaoMae OrigemIntercambio { get; set; }

        /// <summary>
        /// Quantidade m�nima de vag�es no trem
        /// </summary>
        public virtual int? QuantidadeMinimoVagoesTrem { get; set; }

        /// <summary>
        /// Terminal de destino do fluxo
        /// </summary>
        public virtual IAreaOperacional TerminalDestino { get; set; }

        /// <summary>
        /// Terminal de origem do fluxo
        /// </summary>
        public virtual IAreaOperacional TerminalOrigem { get; set; }

        /// <summary>
        /// Especifica o tipo do fluxo - ferroviario ou intermodal
        /// </summary>
        public virtual TipoFluxoEnum? Tipo { get; set; }

        /// <summary>
        /// Unidade de medida - <see cref="Translogic.Modules.Core.Domain.Model.Estrutura.UnidadeMedida"/>
        /// </summary>
        public virtual UnidadeMedida UnidadeMedida { get; set; }

        /// <summary>
        /// Unidade de negocio - <see cref="Estrutura.UnidadeNegocio"/>
        /// </summary>
        public virtual UnidadeNegocio UnidadeNegocio { get; set; }

        /// <summary>
        /// Unidade de negocio receptora - <see cref="UnidadeNegocio"/>
        /// </summary>
        public virtual UnidadeNegocio UnidadeNegocioReceptora { get; set; }

        /// <summary>
        /// Modelo da nota fiscal.
        /// </summary>
        public virtual string ModeloNotaFiscal { get; set; }

        /// <summary>
        /// C�digo Ncm da mercadoria.
        /// </summary>
        public virtual string MercadoriaNcm { get; set; }

        /// <summary>
        /// CNPJ do remetente fiscal.
        /// </summary>
        public virtual string CnpjRemetenteFiscal { get; set; }

        /// <summary>
        /// CNPJ do destinatario fiscal.
        /// </summary>
        public virtual string CnpjDestinatarioFiscal { get; set; }

        /// <summary>
        /// Indica se o fluxo � de Transbordo.
        /// </summary>
        public virtual bool? IndTransbordo { get; set; }

        /// <summary>
        /// Indica se o fluxo � de container vazio.
        /// </summary>
        public virtual string ContainerVazio { get; set; }

        /// <summary>
        /// Tipo de trafego do fluxo
        /// </summary>
        public virtual TipoTrafegoFluxoEnum? TipoTrafegoFluxo { get; set; }

        /// <summary>
        /// Indica se � longstack
        /// </summary>
        public virtual bool? IndLongStack { get; set; }

        /// <summary>
        /// Flag que indica se o fluxo � um fluxo de substitui��o tribut�ria
        /// </summary>
        public virtual bool? SubstituicaoTributaria { get; set; }

        public virtual string Segmento { get; set; }

        /// <summary>
        /// Indica se � fluxo que rateia cte.
        /// </summary>
        public virtual bool? IndRateioCte { get; set; }

        /// <summary>
        /// Tipo de servi�o do Cte
        /// </summary>
        public virtual int? TipoServicoCte { get; set; }

        /// <summary>
        /// Indica se o fluxo � um fluxo operacional, utilizado pelo faturamento 2.0
        /// </summary>
        public virtual bool FluxoOperacional { get; set; }

        /// <summary>
        /// C�digo do fluxo comercial
        /// </summary>
        public virtual string CodigoAgrupador { get; set; }
        
        #endregion

        public virtual int? ObterTipoTomador()
        {
            int? valor = null;
            if (Contrato != null)
                valor = Contrato.IndIeToma;

            return valor;
        }

        public static string ObterDescricaoTipoServicoCte(int? tipoServicoCte)
        {
            var valor = string.Empty;

            if (tipoServicoCte.HasValue)
            {
                switch (tipoServicoCte.Value)
                {
                    case 0:
                        valor = "NORMAL";
                        break;
                    case 1:
                        valor = "SUBCONTRATA��O";
                        break;
                    case 2:
                        valor = "REDESPACHO";
                        break;
                    case 3:
                        valor = "REDESPACHO INTERMEDIARIO";
                        break;
                    case 4:
                        valor = "VINCULADO A MULTIMODAL";
                        break;
                }
            }

            return valor;
        }
    }
}