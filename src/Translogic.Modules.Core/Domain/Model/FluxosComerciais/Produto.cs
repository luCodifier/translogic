﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa um Produto que pode ser transportado
	/// </summary>
	public class Produto : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código do Produto
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição Detalhada do Produto
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição Resumida do Produto
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// Indicador de Ativo aceita valores do enumeration <see cref="bool">bool</see>
		/// </summary>
		public virtual bool? IndAtivo { get; set; }

		/// <summary>
		/// Tipo do Produto <see cref="TipoProduto"/>
		/// </summary>
		public virtual TipoProduto Tipo { get; set; }

		#endregion
	}
}