﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using System;
    using ALL.Core.AcessoDados;
    using NHibernate;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Repositories;

    /// <summary>
    /// Repository responsável pela persistência de Logs de impressão
    /// </summary>
    public class LogImpressaoReciboRepository : NHRepository<LogImpressaoRecibo, int?>, ILogImpressaoReciboRepository
    {
        /// <summary>
        /// Fabrica de sessões injetadas
        /// </summary>
        public ISessionFactory SessionFactory { get; set; }

        /// <summary>
        /// Realiza a persistência do Log quando o usuário imprimir
        /// </summary>
        /// <param name="idTrem">Id do trem selecionado para impressão</param>
        /// <param name="idVagao">Id do vagão selecionado para impressão</param>
        /// <param name="idEmpresa">Id da empresa selecionada para impressão</param>
        /// <param name="usuarioId">Id do usuário logado que está realizando a impressão</param>
        public void GravarLogImpressao(int? idTrem, int? idVagao, int? idEmpresa, int usuarioId)
        {
            var logImpressao = new LogImpressaoRecibo()
                                   {
                                       DataHora = DateTime.Now,
                                       TremId = idTrem,
                                       VagaoId = idVagao,
                                       EmpresaId = idEmpresa,
                                       UsuarioId = usuarioId
                                   };
            Inserir(logImpressao);
        }
    }
}