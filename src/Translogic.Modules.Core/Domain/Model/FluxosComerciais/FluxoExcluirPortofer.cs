namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe que representa um fluxo da portofer que deve ser exluido 
	/// </summary>
	/// <remarks> 
	/// O ID � a parte inteira do c�digo do fluxo por exemplo:
	/// Se o c�digo do fluxo � GN41036 estar� gravado na tabela o id 41036</remarks>
	public class FluxoExcluirPortofer : EntidadeBase<string>
	{
		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Motivo da exclus�o
		/// </summary>
		public virtual string MotivoExclusao { get; set; }
	}
}