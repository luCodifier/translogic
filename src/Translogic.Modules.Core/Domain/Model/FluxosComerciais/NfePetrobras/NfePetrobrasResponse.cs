﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras
{
    using System.Runtime.Serialization;
    using Outputs;

    /// <summary>
    /// Request da mensagem de envio
    /// </summary>
    [DataContract]
    public class NfePetrobrasResponse
    {
        /// <summary>
        /// construtor vazio
        /// </summary>
        public NfePetrobrasResponse()
        {
            DadosNfe = new DadosNfe();
        }

        /// <summary>
        /// construtor recebendo objeto
        /// </summary>
        /// <param name="dadosNfe">Objeto do tipo DadosNfe</param>
        public NfePetrobrasResponse(DadosNfe dadosNfe)
        {
            DadosNfe = dadosNfe;
        }

        /// <summary>
        /// Declaracao da variavel
        /// </summary>
        [DataMember]
        public DadosNfe DadosNfe { get; set; }
    }
}