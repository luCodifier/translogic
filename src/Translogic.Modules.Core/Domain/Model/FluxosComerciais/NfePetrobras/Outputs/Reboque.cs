﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Reboque
    {
        [DataMember]
        public virtual string Placa { get; set; }
    }
}