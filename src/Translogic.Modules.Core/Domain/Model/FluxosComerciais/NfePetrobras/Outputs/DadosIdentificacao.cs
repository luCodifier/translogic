﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class DadosIdentificacao
    {
        [XmlElement]
        public virtual string DirecaoMovimentoMercadorias { get; set; }

        [XmlElement]
        public virtual Etapa Etapa { get; set; }
    }
}