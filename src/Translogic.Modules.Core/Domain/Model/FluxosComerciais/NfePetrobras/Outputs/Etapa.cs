﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Etapa
    {
        [XmlElement]
        public virtual string Valor { get; set; }

        [XmlElement]
        public virtual string NumeroTransporte { get; set; }

        [XmlElement]
        public virtual string NumeroEtapa { get; set; }

        [XmlElement]
        public virtual DadosEtapa DadosEtapa { get; set; }
    }

    [XmlRoot("Etapa")]
    [Serializable]
    public class DadosEtapa
    {
        [XmlElement]
        public virtual string IdentificadorNotaFiscal { get; set; }

        [XmlElement]
        public virtual string NumeroNFe { get; set; }

        [XmlElement]
        public virtual string Serie { get; set; }

        [XmlElement]
        public virtual DateTime DataEmissao { get; set; }
    }
}