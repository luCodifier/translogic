﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Carga
    {
        [DataMember]
        public virtual string ValorTotal { get; set; }

        [DataMember]
        public virtual string VolumeTotal { get; set; }

        [DataMember]
        public virtual string UnidadeVolume { get; set; }

        [DataMember]
        public virtual string PesoTotal { get; set; }

        [DataMember]
        public virtual string UnidadePeso { get; set; }
    }
}