﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class DadosFrete
    {
        [XmlElement]
        public virtual string Valor { get; set; }

        [XmlElement]
        public virtual string ValorBaseICMS { get; set; }

        [XmlElement]
        public virtual string AliquotaICMS { get; set; }

        [XmlElement]
        public virtual string ValorICMS { get; set; }
    }
}