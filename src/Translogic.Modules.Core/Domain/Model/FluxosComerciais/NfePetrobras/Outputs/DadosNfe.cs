﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class DadosNfe
    {
        [XmlElement]
        public virtual string IdentificadorNotaFiscal { get; set; }

        [XmlElement]
        public virtual DadosIdentificacao DadosIdentificacao { get; set; }

        [XmlElement]
        public virtual DadosTransporte DadosTransporte { get; set; }

        [XmlElement]
        public virtual DadosEmissor DadosEmissor { get; set; }

        [XmlElement]
        public virtual DadosDestinatario DadosDestinatario { get; set; }

        [XmlElement]
        public virtual DadosEntrega DadosEntrega { get; set; }

        [XmlElement]
        public virtual DadosFrete DadosFrete { get; set; }
    }

    [Serializable]
    [XmlRoot("ConsultaNFResultado")]
    public class NFesBr
    {
        [XmlArrayItem("DadosNFe", typeof(DadosNfe))]
        public DadosNfe[] NFes { get; set; }
    }
}