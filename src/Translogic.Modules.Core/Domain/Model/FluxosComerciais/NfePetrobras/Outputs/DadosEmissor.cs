﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class DadosEmissor
    {
        [XmlElement]
        public virtual string CNPJ { get; set; }

        [XmlElement]
        public virtual string NomeEmpresa { get; set; }

        [XmlElement]
        public virtual string Logradouro { get; set; }

        [XmlElement]
        public virtual string Numero { get; set; }

        [XmlElement]
        public virtual string Complemento { get; set; }

        [XmlElement]
        public virtual string Bairro { get; set; }

        [XmlElement]
        public virtual string CodigoCidade { get; set; }

        [XmlElement]
        public virtual string NomeCidade { get; set; }

        [XmlElement]
        public virtual string UF { get; set; }

        [XmlElement]
        public virtual string CEP { get; set; }

        [XmlElement]
        public virtual string ChavePais { get; set; }

        [XmlElement]
        public virtual string NomePais { get; set; }
    }
}