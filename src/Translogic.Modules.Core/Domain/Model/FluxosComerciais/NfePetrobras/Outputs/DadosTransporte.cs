﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class DadosTransporte
    {
        [XmlElement]
        public virtual Motorista Motorista { get; set; }

        [XmlElement]
        public virtual string CustosFrete { get; set; }

        [XmlElement]
        public virtual string Vagao { get; set; }

        [XmlElement]
        public virtual string Balsa { get; set; }

        [XmlElement]
        public virtual string Placa { get; set; }

        [XmlElement]
        public virtual Reboque Reboque { get; set; }

        [XmlElement]
        public virtual Transportador Transportador{ get; set; }

        [XmlElement]
        public virtual Carga Carga { get; set; }
    }
}