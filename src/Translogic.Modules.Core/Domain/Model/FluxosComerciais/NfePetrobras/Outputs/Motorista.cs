﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Motorista
    {
        [XmlElement]
        public virtual string Nome { get; set; }

        [XmlElement]
        public virtual string CPF { get; set; }
    }
}