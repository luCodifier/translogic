﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras.Outputs
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Transportador
    {
        [XmlElement]
        public virtual string CNPJ { get; set; }

        [XmlElement]
        public virtual string InscricaoEstadual { get; set; }

        [XmlElement]
        public virtual string Nome { get; set; }

        [XmlElement]
        public virtual string EnderecoCompleto { get; set; }

        [XmlElement]
        public virtual string Cidade { get; set; }

        [XmlElement]
        public virtual string UF { get; set; }
    }
}