﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.NfePetrobras
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Parametros para a consulta ao serviço da petrobras
    /// </summary>
    public class ParametrosPetrobras : EntidadeBase<int>
    {
        /// <summary>
        /// Cnpj Raiz
        /// </summary>
        public virtual string CnpjRaiz { get; set; }

        /// <summary>
        /// Cnpj cadastrado na base da petrobras para a ALL
        /// </summary>
        public virtual string CnpjBusca { get; set; }

        /// <summary>
        /// Token gerado e cadastrado na base da petrobras para a ALL (canal de negocios)
        /// </summary>
        public virtual string Token { get; set; }

        /// <summary>
        /// Url do servico
        /// </summary>
        public virtual string Url { get; set; }
    }
}