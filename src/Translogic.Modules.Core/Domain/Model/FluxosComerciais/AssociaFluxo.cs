﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Model Associa Fluxo
	/// </summary>
	public class AssociaFluxo : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Fluxo Comercial
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		/// Grupo de Fluxo Comercial
		/// </summary>
		public virtual GrupoFluxoComercial GrupoFluxoComercial { get; set; }

		/// <summary>
		/// Situação do Fluxo
		/// </summary>
		public virtual bool IndAtivo { get; set; }

		/// <summary>
		/// Data Associação do Fluxo
		/// </summary>
		public virtual DateTime DataAssociacao { get; set; }

		/// <summary>
		/// Código do usuario que Associou o Fluxo
		/// </summary>
		public virtual string CodigoUsuario { get; set; }

		/// <summary>
		/// Data Cadastro da Associação de Fluxo
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Codigo do usuario que desvinculou o fluxo
		/// </summary>
		public virtual string CodigoUsuarioDesvinculou { get; set; }

		/// <summary>
		/// Data da desvinculação do Fluxo
		/// </summary>
		public virtual DateTime DataDesvinculacao { get; set; }

		#endregion
	}
}
