namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de componente de presta��o do CTe baixado pelo simconsultas
	/// </summary>
	public class CteComponentePrestacaoSimconsultas : EntidadeBase<int?>
	{
		/// <summary>
		/// CTe do simconsultas
		/// </summary>
		public virtual CteSimconsultas CteSimconsultas { get; set; }

		/// <summary>
		/// Nome do componente
		/// </summary>
		public virtual string Xnome { get; set; }

		/// <summary>
		/// Valor do componente
		/// </summary>
		public virtual double? Vcomp { get; set; }
	}
}