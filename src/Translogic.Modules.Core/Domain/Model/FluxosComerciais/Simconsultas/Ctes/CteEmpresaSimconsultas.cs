namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes
{
	using System.Collections.Generic;

	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de empresa do CTe baixado pelo simconsultas
	/// </summary>
	public class CteEmpresaSimconsultas : EntidadeBase<int?>
	{
		/// <summary>
		/// Tipo da pessoa
		/// </summary>
		/// <example>PF - pessoa fisica / PJ - pessoa juridica</example>
		public virtual string TipoPessoa { get; set; }

		/// <summary>
		/// Cnpj ou cpf
		/// </summary>
		public virtual string CnpjCpf { get; set; }

		/// <summary>
		/// Inscricao Estadual quando PJ
		/// </summary>
		public virtual string InscricaoEstadual { get; set; }

		/// <summary>
		/// Razao Social da empresa ou nome da pessoa
		/// </summary>
		public virtual string Xnome { get; set; }

		/// <summary>
		/// Nome fantasia da empresa
		/// </summary>
		public virtual string Xfant { get; set; }

		/// <summary>
		/// N�mero do telefone
		/// </summary>
		public virtual string Fone { get; set; }

		/// <summary>
		/// Logradouro da empresa
		/// </summary>
		public virtual string Xlgr { get; set; }

		/// <summary>
		/// Numero do endere�o
		/// </summary>
		public virtual string Nro { get; set; }

		/// <summary>
		/// Complemento do endereco
		/// </summary>
		public virtual string Xcpl { get; set; }

		/// <summary>
		/// Bairro do endereco
		/// </summary>
		public virtual string Xbairro { get; set; }

		/// <summary>
		/// Codigo do municipio da tabela IBGE
		/// </summary>
		public virtual string Cmun { get; set; }

		/// <summary>
		/// Nome do municipio
		/// </summary>
		public virtual string Xmun { get; set; }

		/// <summary>
		/// Cep do endereo
		/// </summary>
		public virtual string Cep { get; set; }

		/// <summary>
		/// Sigla da UF
		/// </summary>
		public virtual string Uf { get; set; }

		/// <summary>
		/// Codigo do pais da tabela IBGE
		/// </summary>
		public virtual string Cpais { get; set; }

		/// <summary>
		/// Nome do pais
		/// </summary>
		public virtual string Xpais { get; set; }

		/// <summary>
		/// Email da empresa
		/// </summary>
		public virtual string Email { get; set; }

		/// <summary>
		/// Inscri�ao Suframa
		/// </summary>
		public virtual string Isuf { get; set; }

		/*/// <summary>
		/// Lista de documentos fiscais do remetente
		/// </summary>
		public virtual IList<CteDocumentoRemetenteSimconsultas> ListaDocumentos { get; set; }*/
	}
}