﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe cte de configuração de empresas que recebem email somente com XML sem corpo de email
    /// O Id desta classe é o Id da empresa que está sendo configurada
    /// </summary>
    public class CteConfigEmail : EntidadeBase<int?>
    {
        /// <summary>
        /// Assunto do email que poderá ser opcional
        /// </summary>
        public virtual string AssuntoEmail { get; set; }
    }
}