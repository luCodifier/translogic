namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes
{
	using System;

	using ALL.Core.Dominio;

	/// <summary>
	/// Documento do remetente do CTe baixado pelo simconsultas
	/// </summary>
	public class CteDocumentoRemetenteSimconsultas : EntidadeBase<int?>
	{
		/// <summary>
		/// Empresa remetente do CTe
		/// </summary>
		public virtual CteEmpresaSimconsultas CteEmpresaSimconsultas { get; set; }

		/// <summary>
		/// Tipo do documento
		/// </summary>
		/// <example>NF - NOTA FISCAL
		/// NFE - NOTA FISCAL ELETRONICA
		/// OUT - OUTROS DOCUMENTOS
		/// </example>
		public virtual string TipoDocumento { get; set; }

		/// <summary>
		/// Numero do romaneio da NF
		/// </summary>
		public virtual string NfNroma { get; set; }

		/// <summary>
		/// Numero do pedido da nf
		/// </summary>
		public virtual string NfNped { get; set; }

		/// <summary>
		/// Serie da nota fiscal
		/// </summary>
		public virtual string NfSerie { get; set; }

		/// <summary>
		/// N�mero da nota fiscal
		/// </summary>
		public virtual string NfNdoc { get; set; }

		/// <summary>
		/// Data de emiss�o da nota fiscal
		/// </summary>
		public virtual DateTime? NfDemi { get; set; }

		/// <summary>
		/// Valor da base de c�lculo do ICMS
		/// </summary>
		public virtual double? NfVbc { get; set; }

		/// <summary>
		/// Valor total do ICMS
		/// </summary>
		public virtual double? NfVicms { get; set; }

		/// <summary>
		/// Valor da base de c�lculo do ICMS substituto
		/// </summary>
		public virtual double? NfVbcst { get; set; }

		/// <summary>
		/// Valor total do ICMS substituto
		/// </summary>
		public virtual double? NfVst { get; set; }

		/// <summary>
		/// Valor total dos produtos
		/// </summary>
		public virtual double? NfVprod { get; set; }

		/// <summary>
		/// Valor total da NF
		/// </summary>
		public virtual double? NfVnf { get; set; }

		/// <summary>
		/// CFOP predominante
		/// </summary>
		public virtual string NfNCfop { get; set; }

		/// <summary>
		/// Peso total em KG
		/// </summary>
		public virtual double? NfNpeso { get; set; }

		/// <summary>
		/// PIN SUFRAMA
		/// </summary>
		public virtual string Pin { get; set; }

		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string NfeChave { get; set; }

		/// <summary>
		/// Tipo de documento origin�rio
		/// </summary>
		/// <example>
		/// 00 - Declara��o
		/// 10 - Dutovi�rio
		/// 99 - Outros
		/// </example>
		public virtual string OutroTpDoc { get; set; }

		/// <summary>
		/// Descricao quando for 99
		/// </summary>
		public virtual string OutroDescOutros { get; set; }

		/// <summary>
		/// Numero do documento
		/// </summary>
		public virtual string OutroNdoc { get; set; }

		/// <summary>
		/// Data de emiss�o
		/// </summary>
		public virtual DateTime? OutroDemi { get; set; }

		/// <summary>
		/// Valor do documento
		/// </summary>
		public virtual double OutroVdocFisc { get; set; }
	}
}