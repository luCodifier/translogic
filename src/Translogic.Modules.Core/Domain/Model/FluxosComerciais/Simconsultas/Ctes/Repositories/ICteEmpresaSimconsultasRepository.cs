﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositorio <see cref="CteEmpresaSimconsultasRepository"/>
	/// </summary>
	public interface ICteEmpresaSimconsultasRepository : IRepository<CteEmpresaSimconsultas, int?>
	{
	}
}