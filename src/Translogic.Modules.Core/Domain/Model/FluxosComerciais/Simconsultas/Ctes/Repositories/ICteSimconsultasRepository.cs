﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio do CteSimconsultas
	/// </summary>
	public interface ICteSimconsultasRepository : IRepository<CteSimconsultas, int?>
	{
		/// <summary>
		/// Obtém o cte pela chave
		/// </summary>
		/// <param name="chaveCte">Chave do Cte</param>
		/// <returns>Objeto CteSimconsultas</returns>
		CteSimconsultas ObterPorChaveCte(string chaveCte);

		/// <summary>
		/// Obtém os que não foram baixados
		/// </summary>
		/// <returns>Lista de chaves que não foram baixados</returns>
		IList<string> ObterNaoBaixados();

		/// <summary>
		/// Obtém os que não foram processados
		/// </summary>
		/// <returns>Lista de chaves que não foram processados</returns>
		IList<CteSimconsultas> ObterNaoProcessados();
	}
}