﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositorio <see cref="CteDocumentoRemetenteSimconsultasRepository"/>
	/// </summary>
	public interface ICteDocumentoRemetenteSimconsultasRepository : IRepository<CteDocumentoRemetenteSimconsultas, int?>
	{
	}
}