﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories
{
	using ALL.Core.AcessoDados;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes;

	/// <summary>
	/// Interface do repositorio <see cref="CteComponentePrestacaoSimconsultasRepository"/>
	/// </summary>
	public interface ICteComponentePrestacaoSimconsultasRepository : IRepository<CteComponentePrestacaoSimconsultas, int?>
	{
	}
}