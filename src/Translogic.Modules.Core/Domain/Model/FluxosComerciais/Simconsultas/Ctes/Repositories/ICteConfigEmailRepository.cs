﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Simconsultas.Ctes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

    /// <summary>
    /// Interface do repositório <see cref="CteConfigEmailRepository"/>
    /// </summary>
    public interface ICteConfigEmailRepository : IRepository<CteConfigEmail, int?>
    {
        /// <summary>
        /// Obter configuração por cte
        /// </summary>
        /// <param name="cte">Cte que possui a empresa tomadora</param>
        /// <returns>Configuração indicando se a empresa recebe corpo no email</returns>
        CteConfigEmail ObterPorCte(Cte cte);
    }
}
