﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes
{
	using System;
	using System.Collections.Generic;

	using ALL.Core.Dominio;
	
	/// <summary>
	/// Classe de cte baixado pelo simconsultas
	/// </summary>
	public class CteSimconsultas : EntidadeBase<int?>
	{
		/// <summary>
		/// Chave do Cte
		/// </summary>
		public virtual string ChaveCte { get; set; }

		/// <summary>
		/// Versao do CTe
		/// </summary>
		public virtual string Versao { get; set; }

		/// <summary>
		/// Codigo da uf do emitente
		/// </summary>
		public virtual string Cuf { get; set; }

		/// <summary>
		/// CFOP do CTe
		/// </summary>
		public virtual string Cfop { get; set; }

		/// <summary>
		/// Natureza da operação
		/// </summary>
		public virtual string NatOp { get; set; }

		/// <summary>
		/// Forma de pagamento
		/// </summary>
		public virtual string ForPag { get; set; }

		/// <summary>
		/// Modelo do documento fiscal
		/// </summary>
		public virtual string Mod { get; set; }

		/// <summary>
		/// Serie do CT-e
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		/// Numero do CT-e
		/// </summary>
		public virtual string Numero { get; set; }

		/// <summary>
		/// Data/Hora de emissão
		/// </summary>
		public virtual DateTime DhEmi { get; set; }

		/// <summary>
		/// Formato de impressão do DACTE
		/// </summary>
		public virtual string TpImp { get; set; }

		/// <summary>
		/// Forma de emissão
		/// </summary>
		public virtual string TpEmis { get; set; }

		/// <summary>
		/// Digito verificador
		/// </summary>
		public virtual string Cdv { get; set; }

		/// <summary>
		/// Tipo de ambiente SEFAZ
		/// </summary>
		public virtual string TpAmb { get; set; }

		/// <summary>
		/// Tipo do CT-e
		/// </summary>
		/// <example>
		/// 0 - CT-e Normal, 
		/// 1 - CT-e de Complemento de Valores,
		/// 2 - CT-e de Anulação de Valores,
		/// 3 - CT-e Substituto
	    /// </example>
		public virtual string TpCte { get; set; }

		/// <summary>
		/// Identificador do processo de emissão
		/// </summary>
		public virtual string ProcEmi { get; set; }

		/// <summary>
		/// Versão do processo de emissão
		/// </summary>
		public virtual string VerProc { get; set; }

		/// <summary>
		/// CTe referenciado
		/// </summary>
		public virtual string RefCte { get; set; }

		/// <summary>
		/// Codigo municipio que o CTe está sendo emitido
		/// </summary>
		public virtual string CmunEmi { get; set; }

		/// <summary>
		/// Nome do municipio que o CTe está sendo emitido
		/// </summary>
		public virtual string XmunEmi { get; set; }

		/// <summary>
		/// Codigo estado que o CTe está sendo emitido
		/// </summary>
		public virtual string UfEmi { get; set; }

		/// <summary>
		/// Modal do CTe
		/// </summary>
		public virtual string Modal { get; set; }

		/// <summary>
		/// Tipo do serviço
		/// </summary>
		public virtual string TpServ { get; set; }

		/// <summary>
		/// Codigo de Municipio de inicio da prestação
		/// </summary>
		public virtual string CmunIni { get; set; }

		/// <summary>
		/// Nome do municipio de inicio da prestação
		/// </summary>
		public virtual string XmunIni { get; set; }

		/// <summary>
		/// Uf do inicio da prestação
		/// </summary>
		public virtual string UfIni { get; set; }

		/// <summary>
		/// Codigo do municipio de termino da prestação
		/// </summary>
		public virtual string CmunFim { get; set; }

		/// <summary>
		/// Nome do municipio de termino da prestação
		/// </summary>
		public virtual string XmunFim { get; set; }

		/// <summary>
		/// UF do término da prestação
		/// </summary>
		public virtual string UfFim { get; set; }

		/// <summary>
		/// Indicador se o recebedor retira no Aeroporto, filial, porto ou estação de destino
		/// </summary>
		public virtual string Retira { get; set; }

		/// <summary>
		/// Detalhes do retira
		/// </summary>
		public virtual string XdetRetira { get; set; }

		/// <summary>
		/// Indica o tipo do tomador 
		/// </summary>
		/// <example>TOMA03 ou TOMA4</example>
		public virtual string TipoTomador { get; set; }

		/// <summary>
		/// Tomador do serviço
		/// </summary>
		public virtual string Toma { get; set; }

		/// <summary>
		/// Data/Hora de contigencia
		/// </summary>
		public virtual DateTime? DhCont { get; set; }

		/// <summary>
		/// Justificatva de contingencia
		/// </summary>
		public virtual string Xjust { get; set; }

		/// <summary>
		/// Valor total da prestação de serviço
		/// </summary>
		public virtual double? VprestVtPrest { get; set; }

		/// <summary>
		/// Valor a receber
		/// </summary>
		public virtual double? VprestVrec { get; set; }

		/// <summary>
		/// Peso declarado no CTe
		/// </summary>
		public virtual double? Peso { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Xml do cte
		/// </summary>
		public virtual string CteXml { get; set; }

		/// <summary>
		/// Indica se já foi processado
		/// </summary>
		public virtual bool IndProcessado { get; set; }

		/// <summary>
		/// Codigo do erro
		/// </summary>
		public virtual string CodigoErro { get; set; }

		/// <summary>
		/// Descricao do erro
		/// </summary>
		public virtual string DescricaoErro { get; set; }

		/// <summary>
		/// Empresa tomador 4 apenas populado quando for
		/// </summary>
		public virtual CteEmpresaSimconsultas Toma4 { get; set; }

		/// <summary>
		/// Empresa emitente
		/// </summary>
		public virtual CteEmpresaSimconsultas Emitente { get; set; }

		/// <summary>
		/// Empresa remetente
		/// </summary>
		public virtual CteEmpresaSimconsultas Remetente { get; set; }

		/// <summary>
		/// Empresa expedidora
		/// </summary>
		public virtual CteEmpresaSimconsultas Expedidor { get; set; }

		/// <summary>
		/// Empresa recebedora
		/// </summary>
		public virtual CteEmpresaSimconsultas Recebedor { get; set; }

		/// <summary>
		/// Empresa destinataria
		/// </summary>
		public virtual CteEmpresaSimconsultas Destinatario { get; set; }

		/*/// <summary>
		/// Lista de componentes
		/// </summary>
		public virtual IList<CteComponentePrestacaoSimconsultas> ListaComponentes { get; set; }*/
	}
}