﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas
{
    using System.Reflection;
    using Nfes;

    /// <summary>
	/// Entidade NFE Produto SimConsultas
	/// </summary>
	public class NfeProdutoSimconsultas : NotaFiscalEletronicaProduto
	{
        /// <summary>
        /// Inicializa passando o produto lido na nfe
        /// </summary>
        /// <param name="nfep">Nota Fiscal Eletrônica Produto</param>
        public NfeProdutoSimconsultas(NotaFiscalEletronicaProduto nfep)
        {
            foreach (FieldInfo field in nfep.GetType().GetFields())
            {
                GetType().GetField(field.Name).SetValue(this, field.GetValue(nfep));
            }

            foreach (PropertyInfo prop in nfep.GetType().GetProperties())
            {
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(nfep, null), null);
            }
        }

        /// <summary>
        /// Inicializa sem preencher valores
        /// </summary>
        public NfeProdutoSimconsultas()
        {
        }

		/// <summary>
		/// NFE Simconsultas
		/// </summary>
		public virtual NfeSimconsultas NfeSimconsultas { get; set; }
	}
}