﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Simconsultas;

	/// <summary>
	/// Entidade NFE Produto
	/// </summary>
	public class NotaFiscalAnulacaoProduto : EntidadeBase<int?>
	{
		/// <summary>
		/// NFE Simconsultas
		/// </summary>
		public virtual NotaFiscalAnulacao NotaFiscalAnulacao { get; set; }

		/// <summary>
		/// Codigo do Produto
		/// </summary>
		public virtual string CodigoProduto { get; set; }

		/// <summary>
		/// Descrição do Produto
		/// </summary>
		public virtual string DescricaoProduto { get; set; }

		/// <summary>
		/// Código NCM
		/// </summary>
		public virtual string CodigoNcm { get; set; }

		/// <summary>
		/// Genero do produto
		/// </summary>
		public virtual string Genero { get; set; }

		/// <summary>
		/// Código Cfop
		/// </summary>
		public virtual string Cfop { get; set; }

		/// <summary>
		/// Unidade comercial do produto
		/// </summary>
		public virtual string UnidadeComercial { get; set; }

		/// <summary>
		/// Quantidade comercial do produto
		/// </summary>
		public virtual double QuantidadeComercial { get; set; }

		/// <summary>
		/// Valor unitario da comercialização
		/// </summary>
		public virtual double ValorUnitarioComercializacao { get; set; }

		/// <summary>
		/// Valor do produto
		/// </summary>
		public virtual double ValorProduto { get; set; }

		/// <summary>
		/// Unidade tributavel do produto
		/// </summary>
		public virtual string UnidadeTributavel { get; set; }

		/// <summary>
		/// Quantidade tributavel do produto
		/// </summary>
		public virtual double QuantidadeTributavel { get; set; }

		/// <summary>
		/// Valor unitario da tributação
		/// </summary>
		public virtual double ValorUnitarioTributacao { get; set; }

		/// <summary>
		/// Icms emitente
		/// </summary>
		public virtual string IcmsEmitente { get; set; }

		/// <summary>
		/// Icms cst NfeProdutoSimconsultas
		/// </summary>
		public virtual string IcmsCst { get; set; }

		/// <summary>
		/// Pis cst do produto
		/// </summary>
		public virtual string PisCst { get; set; }

		/// <summary>
		/// Cofins cst do produto
		/// </summary>
		public virtual string CofinsCst { get; set; }

		/// <summary>
		/// Modalidade da determinação da base de cálculo do icms
		/// </summary>
		public virtual ModalidadeDeterminacaoBaseCalculoIcmsEnum? ModalidadeDeterminacaoBaseCalculoIcms { get; set; }

		/// <summary>
		/// Valor da base calculo do icms
		/// </summary>
		public virtual double ValorBaseCalculoIcms { get; set; }

		/// <summary>
		/// Aliquota do icms
		/// </summary>
		public virtual double AliquotaIcms { get; set; }

		/// <summary>
		/// Valor do Icms
		/// </summary>
		public virtual double ValorIcms { get; set; }

		/// <summary>
		/// Valor da base de calculo do Ipi
		/// </summary>
		public virtual double ValorBaseCalculoIpi { get; set; }

		/// <summary>
		/// Percentual do Ipi
		/// </summary>
		public virtual double PercentualIpi { get; set; }

		/// <summary>
		/// Valor do Ipi
		/// </summary>
		public virtual double ValorIpi { get; set; }

		/// <summary>
		/// Valor da base de cálculo do imposto de importação
		/// </summary>
		public virtual double ValorBaseCalculoImpostoImportacao { get; set; }

		/// <summary>
		/// Valor da despesa adianeira
		/// </summary>
		public virtual double ValorDespesaAduaneira { get; set; }

		/// <summary>
		/// Valor do Iof
		/// </summary>
		public virtual double ValorIof { get; set; }

		/// <summary>
		/// Quantidade vendida
		/// </summary>
		public virtual double QuantidadeVendida { get; set; }

		/// <summary>
		/// Aliquota do pis
		/// </summary>
		public virtual double AliquotaPis { get; set; }

		/// <summary>
		/// Valor do Pis
		/// </summary>
		public virtual double ValorPis { get; set; }

		/// <summary>
		/// Quantidade vendida cofins
		/// </summary>
		public virtual double QuantidadeVendidaCofins { get; set; }

		/// <summary>
		/// Aliquota cofins
		/// </summary>
		public virtual double AliquotaCofins { get; set; }

		/// <summary>
		/// Valor cofins
		/// </summary>
		public virtual double ValorCofins { get; set; }

		/// <summary>
		/// Valor do ipi cst
		/// </summary>
		public virtual double IpiCst { get; set; }

		/// <summary>
		/// Valor do imposto importação
		/// </summary>
		public virtual double ValorImpostoImportacao { get; set; }

		/// <summary>
		/// Data e hora da gravação
		/// </summary>
		public virtual DateTime DataHoraGravacao { get; set; }
	}
}