﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

	/// <summary>
	/// Interface do repositorio do NFE Simconsultas
	/// </summary>
	public interface INfeSimconsultasRepository : IRepository<NfeSimconsultas, int?>
	{
		/// <summary>
		/// Obtem pela chave nfe
		/// </summary>
		/// <param name="chaveNfe">campo Chave nfe</param>
		/// <returns>Retorna a nfe do simConsultas</returns>
		NfeSimconsultas ObterPorChaveNfe(string chaveNfe);

        /// <summary>
        /// Verifica se existe chave nfe
        /// </summary>
        /// <param name="chaveNfe">campo Chave nfe</param>
        /// <returns>True/False</returns>
        bool ExisteChaveNfe(string chaveNfe);

        /// <summary>
        /// recupera o tipo de operacao (entrada ou saída) da nfe
        /// </summary>
        /// <param name="chaveNfe">chave da nfe</param>
        /// <returns>tipo de operacao (entrada ou saída)</returns>
        object ObterTipoNotaFiscal(string chaveNfe);

        /// <summary>
        /// Obtem pela lista de chave nfe
        /// </summary>
        /// <param name="chavesNfe">lista com as chaves da nfe</param>
        /// <returns>Lista de NfeSimConsulta</returns>
	    IList<NfeSimconsultas> ObterPorChavesNfe(IList<string> chavesNfe);

	    string ObterTagXml(string chaveNfe, string tag);
	}
}
