namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da classe NFE produto Simconsultas
	/// </summary>
	public interface INfeAnulacaoProdutoSimconsultasRepository : IRepository<NotaFiscalAnulacaoProduto, int?>
	{
	}
}