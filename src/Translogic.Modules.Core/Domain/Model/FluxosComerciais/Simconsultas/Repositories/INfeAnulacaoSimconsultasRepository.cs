﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositorio do NFE Simconsultas
	/// </summary>
	public interface INfeAnulacaoSimconsultasRepository : IRepository<NotaFiscalAnulacao, int?>
	{
		/// <summary>
		/// Obtem pela chave nfe
		/// </summary>
		/// <param name="chaveNfe">campo Chave nfe</param>
		/// <returns>Retorna a nfe do simConsultas</returns>
		NotaFiscalAnulacao ObterPorChaveNfe(string chaveNfe);
	}
}
