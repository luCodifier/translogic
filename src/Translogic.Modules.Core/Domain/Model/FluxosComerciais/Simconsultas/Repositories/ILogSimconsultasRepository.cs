﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositorio do Log Simconsultas
	/// </summary>
	public interface ILogSimconsultasRepository : IRepository<LogSimconsultas, int>
	{
	}
}
