namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Log do Simconsultas
	/// </summary>
	public class LogSimconsultas : EntidadeBase<int>
	{
		/// <summary>
		/// Chave da NFe
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Indica se deu erro ao baixa os dados da NF-e
		/// </summary>
		public virtual bool Erro { get; set; }

		/// <summary>
		/// Mensagem de erro quando retornardo o erro
		/// </summary>
		public virtual string MensagemErro { get; set; }

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de T�rmino
		/// </summary>
		public virtual DateTime DataTermino { get; set; }

		/// <summary>
		/// Data/Hora de grava��o
		/// </summary>
		public virtual DateTime DataHoraGravacao { get; set; }

		/// <summary>
		/// Retorno do simconsultas
		/// </summary>
		public virtual string Retorno { get; set; }
	}		
}