﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Nfes;

    /// <summary>
	/// Entidade da NFE Simconsultas
	/// </summary>
	public class NfeSimconsultas : NotaFiscalEletronica
	{
        /// <summary>
        /// Inicialização com população de dados da herança
        /// </summary>
        /// <param name="nfe">Nota Fiscal Eletronica lida do XML</param>
        public NfeSimconsultas(NotaFiscalEletronica nfe)
        {
            foreach (FieldInfo field in nfe.GetType().GetFields())
            {
                if (field.Name != "Tipo" && field.Name != "ListaProdutos")
                {
                    GetType().GetField(field.Name).SetValue(this, field.GetValue(nfe));    
                }
            }
                
            foreach (PropertyInfo prop in nfe.GetType().GetProperties())
            {
                if (prop.Name != "Tipo" && prop.Name != "ListaProdutos")
                {
                    GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(nfe, null), null);
                } 
            }
        }

        /// <summary>
        /// Construtor sem parâmetro
        /// </summary>
        public NfeSimconsultas()
        {
        }

        /// <summary>
        /// Tipo da NFE SCO quando SimConsultas
        /// </summary>
        public override string Tipo
        {
            get
            {
                return "SCO";
            }

            set
            {
                throw new NotImplementedException();
            }
        }

		/// <summary>
		/// Lista de produtos da NFe
		/// </summary>
		public virtual IList<NfeProdutoSimconsultas> ListaProdutos { get; set; }
	}
}