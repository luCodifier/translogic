namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes dos documentos (NF, Nfe e Outros)
	/// </summary>
	public class Cte102 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
        public virtual int IdIcnInfOutros { get; set; }

        /// <summary>
        /// Tipo de documento origin�rio
        /// </summary>
        public virtual int? InfOutrosTpDoc { get; set; }

        /// <summary>
        /// Descri��o outros
        /// </summary>
        public virtual string InfOutrosDescOutros { get; set; }

        /// <summary>
        /// N�mero do documento
        /// </summary>
        public virtual string InfOutrosNdoc { get; set; }

        /// <summary>
        /// Data da emiss�o
        /// </summary>
        public virtual DateTime? InfOutrosDemi { get; set; }

        /// <summary>
        /// Valor do documento
        /// </summary>
        public virtual double? InfOutrosVdocFisc { get; set; }
	}
}