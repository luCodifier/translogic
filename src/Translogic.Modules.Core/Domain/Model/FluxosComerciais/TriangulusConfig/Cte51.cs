namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// N�o utilizada para dados, informa�oes de dados de envio dos Documentos para tomador (0-n)
    /// </summary>
    public class Cte51 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int IdFilial { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int Numero { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string Serie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdDest { get; set; }

        /// <summary>
        /// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
        /// </summary>
        public virtual string CodCliFil { get; set; }

        /// <summary>
        /// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
        /// </summary>
        public virtual string CodCliEmp { get; set; }

        /// <summary>
        /// Usuario do FTP
        /// </summary>
        public virtual string FtpUsuario { get; set; }

        /// <summary>
        /// Senha do FTP
        /// </summary>
        public virtual string FtpSenha { get; set; }

        /// <summary>
        /// Nome do Servidor
        /// </summary>
        public virtual string FtpHost { get; set; }

        /// <summary>
        /// Pasta destinada a receber o Arquivo
        /// </summary>
        public virtual string FtpPath { get; set; }

        /// <summary>
        /// Lista de usu�rios que ser� encaminhado o Arquivo XML
        /// </summary>
        public virtual string EmailPara { get; set; }

        /// <summary>
        /// Lista de usu�rios que receberam como copia o XML
        /// </summary>
        public virtual string EmailCC { get; set; }

        /// <summary>
        /// Lista de usu�rios que receberam como oculto  o XML
        /// </summary>
        public virtual string EmailCO { get; set; }

        /// <summary>
        /// Tipo de envio do Arquivo  (0-N�o envia, 1 � Email , 2 � FTP) Atualiza o cadastro
        /// </summary>
        public virtual int TpEnvio { get; set; }

        /// <summary>
        /// Tipo do Arquivo
        /// </summary>
        public virtual int TpArquivo { get; set; }

        /// <summary>
        /// Nome do Anexo
        /// </summary>
        public virtual string NomeAnexo { get; set; }
    }
}