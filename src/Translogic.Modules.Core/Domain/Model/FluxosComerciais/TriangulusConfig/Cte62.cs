namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Informa��es das Ferrovias Envolvidas (0 - n)
	/// </summary>
	public class Cte62 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdFerroEnv { get; set; }

		/// <summary>
		/// N�mero do CNPJ
		/// </summary>
		public virtual string FerroEnvCnpj { get; set; }

		/// <summary>
		/// C�digo interno da Ferrovia Substituta
		/// </summary>
		public virtual string FerroEnvCint { get; set; }

		/// <summary>
		/// Inscri��o Estadual
		/// </summary>
		public virtual long? FerroEnvIe { get; set; }

		/// <summary>
		/// Raz�o Social ou Nome
		/// </summary>
		public virtual string FerroEnvXnome { get; set; }

		/// <summary>
		/// Logradouro das ferrovias envolvidas
		/// </summary>
		public virtual string EnderFerroXlgr { get; set; }

		/// <summary>
		/// N�mero do endere�o das ferrovias envolvidas
		/// </summary>
		public virtual string EnderFerroNro { get; set; }

		/// <summary>
		/// Complemento do endere�o das ferrovias envolvidas
		/// </summary>
		public virtual string EnderFerroXcpl { get; set; }

		/// <summary>
		/// Bairro das ferrovias envolvidas
		/// </summary>
		public virtual string EnderFerroXbairro { get; set; }

		/// <summary>
		/// C�digo do munic�pio
		/// </summary>
		public virtual int? EnderFerroCMun { get; set; }

		/// <summary>
		/// Nome do munic�pio
		/// </summary>
		public virtual string EnderFerroXmun { get; set; }

		/// <summary>
		/// CEP das ferrovias envolvidas
		/// </summary>
		public virtual int? EnderFerroCep { get; set; }

		/// <summary>
		/// Sigla da UF
		/// </summary>
		public virtual string EnderFerroUf { get; set; }
	}
}