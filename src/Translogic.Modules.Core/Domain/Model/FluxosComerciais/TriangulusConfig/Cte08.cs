namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de valores da prestacao do servico e impostos (1-1)
	/// </summary>
	public class Cte08 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Valor total da presta��o do servi�o
		/// </summary>
		public virtual double? VprestVtprest { get; set; }

		/// <summary>
		/// Valor a Receber
		/// </summary>
		public virtual double? VprestVrec { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst00Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst00Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst00Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst00Vicms { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst20Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst20Predbc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst20Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst20Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst20Vicms { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst45Cst { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst80Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst80Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst80Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst80Vicms { get; set; }

		/// <summary>
		/// Valor do Cr�dito outorgado/presumido
		/// </summary>
		public virtual double? Cst80Vcred { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst81Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst81Predbc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst81Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst81Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst81Vicms { get; set; }

		/// <summary>
		/// C�digo de Situa��o Tribut�ria
		/// </summary>
		public virtual int? Cst90Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst90Predbc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst90Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst90Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst90Vicms { get; set; }

		/// <summary>
		/// Valor do Cr�dito outorgado/presumido
		/// </summary>
		public virtual double? Cst90Vcred { get; set; }

		/// <summary>
		/// Informa��es adicionais de interesse do Fisco
		/// </summary>
		public virtual string ImpInfadfisco { get; set; }

		/// <summary>
		/// classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? Icms00Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms00Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms00Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms00Vicms { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do servi�o
		/// </summary>
		public virtual int? Icms20Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? Icms20Predbc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms20Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms20Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms20Vicms { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? Icms45Cst { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? Icms60Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS ST retido
		/// </summary>
		public virtual double? Icms60Vbcstret { get; set; }

		/// <summary>
		/// Valor do ICMS ST retido
		/// </summary>
		public virtual double? Icms60Vicmsstret { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms60Picmsstret { get; set; }

		/// <summary>
		/// Valor do Cr�dito outorgado/Presumido
		/// </summary>
		public virtual double? Icms60Vcred { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? Icms90Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? Icms90Predbc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms90Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms90Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms90Vicms { get; set; }

		/// <summary>
		/// Valor do Cr�dito Outorgado/Presumido
		/// </summary>
		public virtual double? Icms90Vcred { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? IcmsOutraufCst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? IcmsOutraUfPredbcOutraUf { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? IcmsOutraUfVbcOutraUf { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? IcmsOutraUfPicmsOutraUf { get; set; }

		/// <summary>
		/// Valor do ICMS devido outra UF
		/// </summary>
		public virtual double? IcmsOutraUfVicmsOutraUf { get; set; }

		/// <summary>
		/// Indica se o contribuinte � Simples Nacional 1=Sim
		/// </summary>
		public virtual int? IcmsSnIndsn { get; set; }
	}
}