namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do Cte04
	/// </summary>
	public interface ICte04Repository : IRepository<Cte04, int>
	{
		/// <summary>
		///  Remove pelo os campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		void Remover(int idFilial, int numero, int serie);

		/// <summary>
		/// Obter todos teste
		/// </summary>
		/// <returns>Retorna uma lista desses objetos</returns>
		IList<Cte04> ObterTeste();
	}
}