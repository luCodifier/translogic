namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do reposit�rio do INfe03FilialRepository
    /// </summary>
    public interface INfe03FilialRepository : IRepository<Nfe03Filial, int>
    {
        /// <summary>
        /// Obt�m a filial pelo CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ a ser consultado</param>
        /// <returns>Objeto EmpresaFerrovia</returns>
        Nfe03Filial ObterFilialPorCnpj(long cnpj);
    }
}