using System;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do reposit�rio do Cte01
    /// </summary>
    public interface ICte01Repository : IRepository<Cte01, int>
    {
		/// <summary>
		///  Remove pelo os campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		void Remover(int idFilial, int numero, int serie);

        /// <summary>
        ///   Atualiza a data de emiss�o do CTE na config
        /// </summary>
        /// <param name="idFilial"> Numero da filial</param>
        /// <param name="numero"> Numero do cte</param>
        /// <param name="serie"> Serie do cte</param>
        void AtualizaDataEmissao(int idFilial, int numero, int serie, DateTime dataEmissao);
    }
}