namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do repositório do Nfe19ErroClassRepository
    /// </summary>
    public interface INfe19ErroClassRepository : IRepository<Nfe19ErroClass, int>
    {
    }
}