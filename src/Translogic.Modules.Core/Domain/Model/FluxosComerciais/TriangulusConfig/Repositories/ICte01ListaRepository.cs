namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositorio da lista de processamento da config
	/// </summary>
	public interface ICte01ListaRepository : IRepository<Cte01Lista, int>
	{
		/// <summary>
		/// Obt�m o item da fila de processamento por filial, numero cte e serie cte
		/// </summary>
		/// <param name="idFilial">Identificador da filial</param>
		/// <param name="numeroCte">Numero do Cte</param>
		/// <param name="serieCte">Serie do Cte</param>
		/// <param name="chaveAcesso">Chave de acesso do Cte</param>
		/// <param name="tipo">Tipo da acao na lista</param>
		/// <param name="ascendente">Tipo da classifica��o ascendente</param>
		/// <returns>Retorna o objeto da fila de processamento</returns>
		Cte01Lista ObterListaFilaProcessamento(int idFilial, int numeroCte, int serieCte, string chaveAcesso, int tipo, bool ascendente);
	}
}