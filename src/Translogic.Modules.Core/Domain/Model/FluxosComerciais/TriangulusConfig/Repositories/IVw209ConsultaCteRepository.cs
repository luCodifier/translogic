namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do reposit�rio do Nfe19ErroClassRepository
    /// </summary>
    public interface IVw209ConsultaCteRepository : IRepository<Vw209ConsultaCte, int>
    {
        /// <summary>
        /// Obtem o retorno do processamento pelo id da lista de fila de processamento
        /// </summary>
        /// <param name="idLista">Identificador da lista de processamento do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterRetornoPorIdLista(int idLista);

        /// <summary>
        /// Obtem o retorno do processamento pelo protocolo
        /// </summary>
        /// <param name="protocolo">Identificador da lista de processamento do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterRetornoPorProtocolo(long protocolo);

        /// <summary>
        /// Obtem o retorno do processamento pelo id da lista de fila de processamento
        /// </summary>
        /// <param name="idFilial"> Numero da filial</param>
        /// <param name="numero"> Numero do cte</param>
        /// <param name="serie"> Serie do cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterRetornoPorSerieNumeroUnidade(int idFilial, int numero, int serie);

        /// <summary>
        /// Obtem o retorno do processamento pela chave
        /// </summary>
        /// <param name="chave">Identificador da chave do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterRetornoPorChave(string chave);

        /// <summary>
        /// Obtem o retorno do processamento pela chave
        /// </summary>
        /// <param name="chave">Identificador da chave do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterCteJaAutorizado(string chave);

        /// <summary>
        /// Obt�m o retorno do processamento 
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o CteProcessado</returns>
        Vw209ConsultaCte ObterCteJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie);
    }
}