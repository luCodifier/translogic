namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do INfe20ErroLogRepository
	/// </summary>
	public interface INfe20ErroLogRepository : IRepository<Nfe20ErroLog, int>
	{
		/// <summary>
		///  Obtem o Log de Erro pelos campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		/// <returns>Retorna o log de erro</returns>
		Nfe20ErroLog ObterErro(int idFilial, int numero, int serie);
	}
}
