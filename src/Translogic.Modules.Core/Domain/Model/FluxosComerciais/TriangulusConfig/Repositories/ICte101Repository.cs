namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do Cte06
	/// </summary>
	public interface ICte101Repository : IRepository<Cte101, int>
	{
		/// <summary>
		///  Remove pelo os campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do cte</param>
		/// <param name="serie"> Serie do cte</param>
		void Remover(int idFilial, int numero, int serie);
	}
}