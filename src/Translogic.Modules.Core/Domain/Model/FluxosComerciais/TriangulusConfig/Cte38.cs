﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela ICTE38_ICS
	/// </summary>
	public class Cte38 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary> 
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Chave de Acesso da CT-e emitida pelo Tomador
		/// </summary>
		public virtual string InfCteSubChCte { get; set; }

		/// <summary>
		/// Chave de Acesso da NF-e emitida pelo Tomador
		/// </summary>
		public virtual string TomaIcmsRefNfe { get; set; }

		/// <summary>
		/// Informar CNPJ do emitente do Documento Fiscal
		/// </summary>
		public virtual string RefNfCnpj { get; set; }

		/// <summary>
		/// Código do Modelo Documento Fiscal
		/// </summary>
		public virtual string RefNfMod { get; set; }

		/// <summary>
		/// Série do Documento Fiscal
		/// </summary>
		public virtual int? RefNfSerie { get; set; }

		/// <summary>
		/// Sub-Série do Documento Fiscal
		/// </summary>
		public virtual int? RefNfSubSerie { get; set; }

		/// <summary>
		/// número do documento fiscal
		/// </summary>
		public virtual int? RefNfNro { get; set; }

		/// <summary>
		/// valor do documento fiscal
		/// </summary>
		public virtual double? RefNfValor { get; set; }

		/// <summary>
		/// data de emissão do documento fiscal
		/// </summary>
		public virtual DateTime? RefNfDemi { get; set; }

		/// <summary>
		/// Chave de acesso do CT-e emitido pelo Tomador
		/// </summary>
		public virtual string TomaIcmsRefCte { get; set; }

		/// <summary>
		/// Chave de acesso do CT-e de Anulação
		/// </summary>
		public virtual string TomaINaoCmsRefCteAnu { get; set; }

        /// <summary>
        /// Indicador de CT-e Alteração de Tomador
        /// </summary>
        public virtual int? InfCteSubIndAlteraToma { get; set; }
	}
}