namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Fila de Processamento
	/// </summary>
	public class Cte01Lista : EntidadeBase<int>
	{
		/*/// <summary>
		/// Campo Sequencial Único - Automático
		/// </summary>
		public virtual int IdLista { get; set; }*/

		/// <summary>
		/// Identificação da Filial
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// UF da Sefaz de Origem
		/// </summary>
		public virtual int Uf { get; set; }

		/// <summary>
		/// Número do CT-e do ERP
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		/// Série da CT-e do ERP
		/// </summary>
		public virtual int Serie { get; set; }

		/// <summary>
		/// Número do CT-e final - só para inutilização
		/// </summary>
		public virtual int? NumeroFim { get; set; }

		/// <summary>
		/// Ano de inutilização - só para inutilização (AA)
		/// </summary>
		public virtual string AnoInutilizacao { get; set; }

		/// <summary>
		/// Data e Hora de Emissão do documento
		/// </summary>
		public virtual DateTime DataHoraEmissao { get; set; }

		/// <summary>
		/// Tipo do Registro -> 0. CT-e Emitido, 1. CT-e Emitido em Contingência, 2. CT-e Cancelado, 3 CT-e Inutilizado, 4 Consulta CT-e
		/// </summary>
		public virtual int? Tipo { get; set; }

		/// <summary>
		/// CNPJ do Emitente
		/// </summary>
		public virtual long? CnpjEmitente { get; set; }

		/// <summary>
		/// CNPJ do Destinatário
		/// </summary>
		public virtual long? CnpjDestinatario { get; set; }

		/// <summary>
		/// Tipo de ambiente 1- produção  2- homologação
		/// </summary>
		public virtual int? TipoAmbiente { get; set; }

		/// <summary>
		/// Versão do Schema da Sefaz
		/// </summary>
		public virtual double? VersaoSefaz { get; set; }

		/// <summary>
		/// Justificativa da Inutilização / Cancelamento
		/// </summary>
		public virtual string Justificativa { get; set; }

		/// <summary>
		/// Modelo do documento fiscal
		/// </summary>
		public virtual string ModeloDocumentoFiscal { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero de agrupamento para impressão por lote
		/// </summary>
		public virtual int Agrupamento { get; set; }

		/// <summary>
		/// Flag do Registro -> 0. Incluida, 1. XML OK, 2. PDF , 3. Assinatura, 4. Lote, 5. Sefaz, 6 Autorizada.
		/// </summary>
		public virtual int Flag { get; set; }

		/// <summary>
		/// 0. Sem Erros , 1. Erro Detectado
		/// </summary>
		public virtual int FlagErro { get; set; }

		/// <summary>
		/// Data e Hora de Entrada do Registro na Lista
		/// </summary>
		public virtual DateTime DataHoraEntradaLista { get; set; }

		/// <summary>
		/// Identificação do Lote de CT-e
		/// </summary>
		public virtual int? IdLote { get; set; }

		/// <summary>
		/// Chave de Acesso do CT-e
		/// </summary>
		public virtual string ChaveAcesso { get; set; }

		/// <summary>
		/// Protocolo Retornado pela SEFAZ - Autorização de Uso da CT-e
		/// </summary>
		public virtual long? Protocolo { get; set; }

		/// <summary>
		/// Data e Hora de Recebimento do Protocolo
		/// </summary>
		public virtual DateTime? DataHoraRecibimento { get; set; }

		/// <summary>
		/// XML de Retorno do SEFAZ  - ( Protocolo + Dt. Recibo )
		/// </summary>
		public virtual string XmlRetorno { get; set; }

		/// <summary>
		/// Status da CT-e retornado pelo SEFAZ
		/// </summary>
		public virtual int? Status { get; set; }

		/// <summary>
		/// 1. Nfe enviada p/ processamento Central,  0. Nfe ainda não enviada p/ process. Central
		/// </summary>
		public virtual int? EnvioCentral { get; set; }

		/// <summary>
		/// Tipo de impressão do documento 1-Retrato(default) ou 2-Paisagem
		/// </summary>
		public virtual int TipoImpressao { get; set; }

		/// <summary>
		/// Tamanho da CT-e em bytes
		/// </summary>
		public virtual int? Tamanho { get; set; }

		/// <summary>
		/// XML de Envio
		/// </summary>
		public virtual string XmlEnvio { get; set; }

		/// <summary>
		/// Data e Hora final do processo de emissão da Nfe
		/// </summary>
		public virtual DateTime? DataHoraFinal { get; set; }

		/// <summary>
		/// Controle interno da Aplicação
		/// </summary>
		public virtual int LockId { get; set; }

		/// <summary>
		/// Forma de emissão do CT-e
		/// </summary>
		public virtual int? TipoEmissao { get; set; }

		/// <summary>
		/// Data e Hora do inicio do processo
		/// </summary>
		public virtual DateTime? DataHoraInicioProcesso { get; set; }

		/// <summary>
		/// Data e Hora das alterações
		/// </summary>
		public virtual DateTime? DataHoraAlteracao { get; set; }
	}
}

