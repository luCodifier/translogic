namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabelas de informacoes de ve�culos novos transportados
	/// </summary>
	public class Cte34 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdVeicnNovos { get; set; }

		/// <summary>
		/// Chassi do Ve�culo
		/// </summary>
		public virtual string VeicNovosChassi { get; set; }

		/// <summary>
		/// cor do ve�culo
		/// </summary>
		public virtual string VeicNovosCcor { get; set; }

		/// <summary>
		/// Cor dos veiculos novos
		/// </summary>
		public virtual string VeicNovosXcor { get; set; }

		/// <summary>
		/// Codigo e marca do veiculo modelo
		/// </summary>
		public virtual string VeicNovosCmod { get; set; }

		/// <summary>
		/// Valor Unit�rio do Ve�culo
		/// </summary>
		public virtual double? VeicNovosVunit { get; set; }

		/// <summary>
		/// Frete Unit�rio
		/// </summary>
		public virtual double? VeicNovosVfrete { get; set; }
	}
}