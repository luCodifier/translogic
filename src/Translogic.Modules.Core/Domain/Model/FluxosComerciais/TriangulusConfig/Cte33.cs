namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabelas de informacoes de produtos perigosos
	/// </summary>
	public class Cte33 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnPeri { get; set; }

		/// <summary>
		/// N�mero ONU
		/// </summary>
		public virtual int? PeriNonu { get; set; }

		/// <summary>
		/// Nome apropriado para embarque do produto
		/// </summary>
		public virtual string PeriXnomeae { get; set; }

		/// <summary>
		/// Classe ou subclasse, e risco subsidi�rio
		/// </summary>
		public virtual string PeriXclaRisco { get; set; }

		/// <summary>
		/// Grupo de Embalagem
		/// </summary>
		public virtual string PeriGrEmb { get; set; }

		/// <summary>
		/// Quantidade e tipo de volumes
		/// </summary>
		public virtual string PeriQtotProd { get; set; }

		/// <summary>
		/// Quantidade e tipo de volumes
		/// </summary>
		public virtual string PeriQvolTipo { get; set; }

		/// <summary>
		/// Ponto de fulgor
		/// </summary>
		public virtual string PeriPontoFulgor { get; set; }
	}
}