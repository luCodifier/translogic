﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Informações do Multimodal
    /// </summary>
    public class Cte90 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary> 
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Número do Certificado do Operador de Transporte Multimodal
        /// </summary>
        public virtual string CertifOperMultimodal { get; set; }

        /// <summary>
        /// Indicador Negociável - Preencher com: 0 - Não Negociável; 1 - Negociável
        /// </summary>
        public virtual string IndicadorNegociavel { get; set; }
    }
}