namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Informa��es dos containeres contidos no vag�o (0 - n)
	/// </summary>
	public class Cte69 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDetVag { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdContVag { get; set; }

		/// <summary>
		/// Identifica��o do Container
		/// </summary>
		public virtual string ContVagNcont { get; set; }

		/// <summary>
		/// Data prevista da entrega
		/// </summary>
		public virtual DateTime? ContVagDprev { get; set; }
	}
}