namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes do Expedidor / Recebedor / Destinatario (1-1)
	/// </summary>
	public class Cte07 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// CNPJ (Informacoes do Expedidor)
		/// </summary>
		public virtual long? ExpedCnpj { get; set; }

		/// <summary>
		/// CPF (Informacoes do Expedidor)
		/// </summary>
		public virtual long? ExpedCpf { get; set; }

		/// <summary>
		/// IE (Informacoes do Expedidor)
		/// </summary>
		public virtual string ExpedIe { get; set; }

		/// <summary>
		/// Nome / raz�o social
		/// </summary>
		public virtual string ExpedXNome { get; set; }

		/// <summary>
		/// Telefone (Informacoes do Expedidor)
		/// </summary>
		public virtual string ExpedFone { get; set; }

		/// <summary>
		/// Logradouro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderExpedXlgr { get; set; }

		/// <summary>
		/// N�mero (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderExpedNro { get; set; }

		/// <summary>
		/// Complemento (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderExpedXcpl { get; set; }

		/// <summary>
		/// Bairro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderExpedXBairro { get; set; }

		/// <summary>
		/// C�digo munic�pio
		/// </summary>
		public virtual int? EnderExpedCMun { get; set; }

		/// <summary>
		/// Nome Munic�pio
		/// </summary>
		public virtual string EnderExpedXMun { get; set; }

		/// <summary>
		/// CEP (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderExpedCep { get; set; }

		/// <summary>
		/// UF (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderExpedUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s
		/// </summary>
		public virtual int? EnderExpedCpais { get; set; }

		/// <summary>
		/// Nome do Pa�s
		/// </summary>
		public virtual string EnderExpedXPais { get; set; }

		/// <summary>
		/// CNPJ (Informacoes do Expedidor)
		/// </summary>
		public virtual long? RecebCnpj { get; set; }

		/// <summary>
		/// CPF (Informacoes do Expedidor)
		/// </summary>
		public virtual long? RecebCpf { get; set; }

		/// <summary>
		/// IE (Informacoes do Expedidor)
		/// </summary>
		public virtual string RecebIe { get; set; }

		/// <summary>
		/// Nome / raz�o social
		/// </summary>
		public virtual string RecebXNome { get; set; }

		/// <summary>
		/// Telefone (Informacoes do Expedidor)
		/// </summary>
		public virtual string RecebFone { get; set; }

		/// <summary>
		/// Logradouro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebXlgr { get; set; }

		/// <summary>
		/// N�mero (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebNro { get; set; }

		/// <summary>
		/// Complemento (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebXcpl { get; set; }

		/// <summary>
		/// Bairro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebXbairro { get; set; }

		/// <summary>
		/// C�digo munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderRecebCMun { get; set; }

		/// <summary>
		/// Nome Munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebXMun { get; set; }

		/// <summary>
		/// CEP (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderRecebCep { get; set; }

		/// <summary>
		/// UF (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderRecebCpais { get; set; }

		/// <summary>
		/// Nome do Pa�s (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderRecebXpais { get; set; }

		/// <summary>
		/// CNPJ (Informacoes do Expedidor)
		/// </summary>
		public virtual long? DestCnpj { get; set; }

		/// <summary>
		/// CPF (Informacoes do Expedidor)
		/// </summary>
		public virtual long? DestCpf { get; set; }

		/// <summary>
		/// IE (Informacoes do Expedidor)
		/// </summary>
		public virtual string DestIe { get; set; }

		/// <summary>
		/// Nome/raz�o social
		/// </summary>
		public virtual string DestXNome { get; set; }

		/// <summary>
		/// Telefone (Informacoes do Expedidor)
		/// </summary>
		public virtual string DestFone { get; set; }

		/// <summary>
		/// Inscri��o na SUFRAMA
		/// </summary>
		public virtual string DestIsUf { get; set; }

		/// <summary>
		/// Logradouro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestXlgr { get; set; }

		/// <summary>
		/// N�mero (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestNro { get; set; }

		/// <summary>
		/// Complemento (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestXCpl { get; set; }

		/// <summary>
		/// Bairro (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestXBairro { get; set; }

		/// <summary>
		/// C�digo munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderDestCMun { get; set; }

		/// <summary>
		/// Nome Munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestXMun { get; set; }

		/// <summary>
		/// CEP (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderDestCep { get; set; }

		/// <summary> 
		/// UF (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s (Informacoes do Expedidor)
		/// </summary>
		public virtual int? EnderDestCpais { get; set; }

		/// <summary>
		/// Nome do Pa�s (Informacoes do Expedidor)
		/// </summary>
		public virtual string EnderDestXpais { get; set; }

		/// <summary>
		/// CNPJ (Informacoes do Expedidor)
		/// </summary>
		public virtual long? LocEntCnpj { get; set; }

		/// <summary>
		/// CPF (Informacoes do Expedidor)
		/// </summary>
		public virtual long? LocEntCpf { get; set; }

		/// <summary>
		/// Raz�o social (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntXNome { get; set; }

		/// <summary>
		/// Logradouro (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntXlgr { get; set; }

		/// <summary>
		/// N�mero (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntNro { get; set; }

		/// <summary>
		/// Complemento (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntXcpl { get; set; }

		/// <summary>
		/// Bairro (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntXbairro { get; set; }

		/// <summary>
		/// C�digo Munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual int? LocEntCmun { get; set; }

		/// <summary>
		/// Nome Munic�pio (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntXmun { get; set; }

		/// <summary>
		/// UF (Informacoes do Expedidor)
		/// </summary>
		public virtual string LocEntUf { get; set; }
	}
}