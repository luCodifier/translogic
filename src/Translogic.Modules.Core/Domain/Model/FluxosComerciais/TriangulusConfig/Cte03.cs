﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Tabela de campo uso de uso livre do contribuinte (0-10)
    /// </summary>
    public class Cte03 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdObsCont { get; set; }

        /// <summary>
        /// Identificação do campo
        /// </summary>
        public virtual string ObsContxCampo { get; set; }

        /// <summary>
        /// conteúdo do campo
        /// </summary>
        public virtual string ObsContxTexto { get; set; }
    }
}