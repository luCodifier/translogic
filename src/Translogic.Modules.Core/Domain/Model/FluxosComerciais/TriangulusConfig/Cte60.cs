namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Informações do modal Ferroviário (1 - 1)
	/// </summary>
	public class Cte60 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Tipo de Tráfego
		/// </summary>
		public virtual int? FerrovTpTraf { get; set; }

		/// <summary>
		/// Responsável pelo Faturamento
		/// </summary>
		public virtual int? TrafMutRespFat { get; set; }

		/// <summary>
		/// Ferrovia Emitente do CTe
		/// </summary>
		public virtual int? TrafMutFerrEmi { get; set; }

		/// <summary>
		/// Fluxo Ferroviário
		/// </summary>
		public virtual string FerrovFluxo { get; set; }

		/// <summary>
		/// Identificação do trem.
		/// </summary>
		public virtual string FerrovIdTrem { get; set; }

		/// <summary>
		/// Valor do Frete
		/// </summary>
		public virtual double? FerrovValFrete { get; set; }
	}
}