﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces;

    /// <summary>
    /// Tabela de informacoes dos documentos (NF, Nfe e Outros)
    /// </summary>
    public class Cte103 : EntidadeBase<int>, IInfUnidTransp
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Relacionamento com a Cte100(ICTE100_ICN_INFNF (FK))
        /// </summary>
        public virtual int IdIcnInf { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdInfUnidTransp { get; set; }

        /// <summary>
        /// Tipo da Unidade de Transporte
        /// </summary>
        public virtual int TpUnidTransp { get; set; }

        /// <summary>
        /// Identificação da Unidade de Transporte
        /// </summary>
        public virtual int IdUnidTransp { get; set; }

        /// <summary>
        /// Quantidade rateada (Peso, Volume)
        /// </summary>
        public virtual double QtdRat { get; set; }
    }
}