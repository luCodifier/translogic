namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Tabela de c�digo da filial
    /// </summary>
    public class Nfe03Filial : EntidadeBase<int>
    {
        /// <summary>
        ///  Campo ID_FILIAL
        /// </summary>
        public virtual long IdFilial { get; set; }

        /// <summary>
        ///  Campo PATH_REC
        /// </summary>
        public virtual string PathRec { get; set; }

        /// <summary>
        ///  Campo DDD da tabela de c�digo da filial
        /// </summary>
        public virtual int? Ddd { get; set; }

        /// <summary>
        ///  Campo TELEFONE
        /// </summary>
        public virtual int? Telefone { get; set; }

        /// <summary>
        ///  Campo COD_MUN
        /// </summary>
        public virtual int? CodMun { get; set; }

        /// <summary>
        ///  Campo IM da tabela de c�digo da filial
        /// </summary>
        public virtual string Im { get; set; }

        /// <summary>
        ///  Campo INCENT_CULTURAL
        /// </summary>
        public virtual int? IncentCultural { get; set; }

        /// <summary>
        ///  Campo OPT_SIMPLES_NAC
        /// </summary>
        public virtual int? OptSimplesNac { get; set; }

        /// <summary>
        ///  Campo REG_ESP_TRIB
        /// </summary>
        public virtual int? RegEspTrib { get; set; }

        /// <summary>
        ///  Campo FILIAL_RECEBIMENTO
        /// </summary>
        public virtual int FilialRecebimento { get; set; }

        /// <summary>
        ///  Campo UF da tabela de c�digo da filial
        /// </summary>
        public virtual int Uf { get; set; }

        /// <summary>
        ///  Campo ID_EMPRESA
        /// </summary>
        public virtual int IdEmpresa { get; set; }

        /// <summary>
        ///  Campo CNPJ da tabela de c�digo da filial
        /// </summary>
        public virtual long Cnpj { get; set; }

        /// <summary>
        ///  Campo IE da tabela de c�digo da filial
        /// </summary>
        public virtual string Ie { get; set; }

        /// <summary>
        ///  Campo LICENCA
        /// </summary>
        public virtual string Licenca { get; set; }

        /// <summary>
        ///  Campo RAZ_SOC
        /// </summary>
        public virtual string RazSoc { get; set; }

        /// <summary>
        ///  Campo NOME_REDUZIDO
        /// </summary>
        public virtual string NomeReduzido { get; set; }

        /// <summary>
        ///  Campo ENDERECO
        /// </summary>
        public virtual string Endereco { get; set; }

        /// <summary>
        ///  Campo NR_LOGR
        /// </summary>
        public virtual int? NrLogr { get; set; }

        /// <summary>
        ///  Campo COMPL
        /// </summary>
        public virtual string Compl { get; set; }

        /// <summary>
        ///  Campo BAIRRO
        /// </summary>
        public virtual string Bairro { get; set; }

        /// <summary>
        ///  Campo CIDADE
        /// </summary>
        public virtual string Cidade { get; set; }

        /// <summary>
        ///  Campo CEP da tabela de c�digo da filial
        /// </summary>
        public virtual int? Cep { get; set; }

        /// <summary>
        ///  Campo DANFE_PRE_FORMATADO
        /// </summary>
        public virtual int? DanfePreFormatado { get; set; }

        /// <summary>
        ///  Campo DANFE_ORIENTACAO
        /// </summary>
        public virtual int? DanfeOrientacao { get; set; }

        /// <summary>
        ///  Campo ID_CFG_PROXY
        /// </summary>
        public virtual int? IdCfgProxy { get; set; }

        /// <summary>
        ///  Campo ID_CFG_EMAIL
        /// </summary>
        public virtual int? IdCfgEmail { get; set; }

        /// <summary>
        ///  Campo HTML_ENVIO_NFE
        /// </summary>
        public virtual string HtmlEnvioNfe { get; set; }
        
        /// <summary>
        ///  Campo COD_CLI_FIL
        /// </summary>
        public virtual string CodCliFil { get; set; }

        /// <summary>
        ///  Campo ID_FILIAL_CONT
        /// </summary>
        public virtual long? IdFilialCont { get; set; }

        /// <summary>
        ///  Campo TARIFADOR
        /// </summary>
        public virtual int Tarifador { get; set; }

        /// <summary>
        ///  Campo QTDE_MINUTOS_ENVIO
        /// </summary>
        public virtual int? QtdeMinutosEnvio { get; set; }
    }
}