namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Validators
{
    using NHibernate.Validator.Cfg.Loquacious;
    using Util;

    /// <summary>
    /// Valida��es do Cte01
    /// </summary>
    public class Cte01Validation : ValidationDef<Cte01>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor com as defini��es de valida��o
        /// </summary>
        public Cte01Validation()
        {
            /*
            Define(p => p.InfCteId).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER44)).
                WithMessage("O campo InfCteId da tabela ICte01 n�o est� no formato correto!");

            Define(p => p.IdeDhEmi.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER34)).
                 WithMessage("O campo IdeDhEmi da tabela ICte01 n�o est� no formato correto!");

            Define(p => p.IdeCdv.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER35)).
                  WithMessage("O campo IdeCdv da tabela ICte01 n�o est� no formato correto!");

            Define(p => p.IdeCdv.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER35)).
                  WithMessage("O campo IdeCdv da tabela ICte01 n�o est� no formato correto!");
            */

            Define(p => p.IdeVerProc).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                  WithMessage("O campo IdeVerProc da tabela ICte01 n�o est� no formato correto!");

            /*
            Define(p => p.IdeCMunEmi.Value.ToString()).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER2)).
                        WithMessage("O campo IdeCMunEmi da tabela ICte01 n�o est� no formato correto!");
             */

            Define(p => p.IdeXMunEmi).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo IdeXMunEmi da tabela ICte01 n�o est� no formato correto!");

            /*
           Define(p => p.IdeCMunIni.Value.ToString()).NotNullableAndNotEmpty().And.
                       MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER2)).
                       WithMessage("O campo IdeCMunIni da tabela ICte01 n�o est� no formato correto!");
            */

            Define(p => p.IdeXMunIni).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo IdeXMunIni da tabela ICte01 n�o est� no formato correto!");

            /*
           Define(p => p.IdeCMunFim.Value.ToString()).NotNullableAndNotEmpty().And.
                       MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER2)).
                       WithMessage("O campo IdeCMunFim da tabela ICte01 n�o est� no formato correto!");
            */

            Define(p => p.IdeXMunFim).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo IdeXMunFim da tabela ICte01 n�o est� no formato correto!");

            Define(p => p.ComplOrigCalc).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo ComplOrigCalc da tabela ICte01 n�o est� no formato correto!");

            Define(p => p.ComplDestCalc).NotNullableAndNotEmpty().And.
                         MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                         WithMessage("O campo ComplDestCalc da tabela ICte01 n�o est� no formato correto!");
        }

        #endregion
    }
}