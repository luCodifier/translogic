namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Validators
{
    using NHibernate.Validator.Cfg.Loquacious;
    using Util;

    /// <summary>
    /// Valida��es do Cte62
    /// </summary>
    public class Cte62Validation : ValidationDef<Cte62>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor com as defini��es de valida��o
        /// </summary>
        public Cte62Validation()
        {
            Define(p => p.FerroEnvCint).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                WithMessage("O campo FerroEnvCint da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.FerroEnvCnpj).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER4)).
                WithMessage("O campo FerroEnvCnpj da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.FerroEnvXnome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                WithMessage("O campo FerroEnvXnome da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.EnderFerroXlgr).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
              WithMessage("O campo EnderFerroXlgr da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.EnderFerroXbairro).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
              WithMessage("O campo EnderFerroXbairro da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.EnderFerroXmun).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
              WithMessage("O campo EnderFerroXmun da tabela ICte62 n�o est� no formato correto!");

            Define(p => p.EnderFerroUf).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
              WithMessage("O campo EnderFerroXmun da tabela ICte62 n�o est� no formato correto!");
        }

        #endregion
    }
}