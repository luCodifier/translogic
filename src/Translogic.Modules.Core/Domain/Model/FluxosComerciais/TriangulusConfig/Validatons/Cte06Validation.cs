namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Validators
{
    using NHibernate.Validator.Cfg.Loquacious;
    using Util;

    /// <summary>
    /// Valida��es do Cte01
    /// </summary>
    public class Cte06Validation : ValidationDef<Cte06>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor com as defini��es de valida��o
        /// </summary>
        public Cte06Validation()
        {
            Define(p => p.LocRetXnome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo LocRetXnome da tabela ICte06 n�o est� no formato correto!");

            Define(p => p.LocRetXmun).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo LocRetXmun da tabela ICte06 n�o est� no formato correto!");
        }

        #endregion
    }
}