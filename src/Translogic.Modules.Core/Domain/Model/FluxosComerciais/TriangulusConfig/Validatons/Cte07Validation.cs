namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Validators
{
    using NHibernate.Validator.Cfg.Loquacious;
    using Util;

    /// <summary>
    /// Valida��es do Cte07
    /// </summary>
    public class Cte07Validation : ValidationDef<Cte07>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor com as defini��es de valida��o
        /// </summary>
        public Cte07Validation()
        {
            Define(p => p.ExpedIe).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER26)).
                 WithMessage("O campo ExpedIe da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.ExpedXNome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo ExpedXNome da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.ExpedFone).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36)).
                 WithMessage("O campo ExpedFone da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderExpedXBairro).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderExpedXBairro da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderExpedXMun).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderExpedXMun da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderExpedXPais).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderExpedXPais da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.RecebIe).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER26)).
                 WithMessage("O campo RecebIe da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.RecebXNome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo RecebXNome da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.RecebFone).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36)).
                 WithMessage("O campo RecebFone da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderRecebXbairro).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderRecebXbairro da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderRecebXMun).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderRecebXMun da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderRecebXpais).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderRecebXpais da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.DestIe).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER26)).
                 WithMessage("O campo DestIe da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.DestXNome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo DestXNome da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.DestFone).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36)).
                 WithMessage("O campo DestFone da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderDestXBairro).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderDestXBairro da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.EnderDestXpais).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo EnderDestXpais da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.LocEntXNome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo LocEntXNome da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.LocEntXbairro).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo LocEntXbairro da tabela ICte07 n�o est� no formato correto!");

            Define(p => p.LocEntXmun).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                 WithMessage("O campo LocEntXmun da tabela ICte07 n�o est� no formato correto!");
        }

        #endregion
    }
}