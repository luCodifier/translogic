namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Validators
{
    using NHibernate.Validator.Cfg.Loquacious;
    using Util;

    /// <summary>
    /// Valida��es do Cte01
    /// </summary>
    public class Cte05Validation : ValidationDef<Cte05>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor com as defini��es de valida��o
        /// </summary>
        public Cte05Validation()
        {
            /*
           Define(p => p.EmitCnpj.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER6)).
               WithMessage("O campo EmitCnpj da tabela ICte05 n�o est� no formato correto!");
           */

            Define(p => p.EmitIe).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER26)).
                  WithMessage("O campo EmitIe da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.EmitXnome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                  WithMessage("O campo EmitXnome da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.EmitXfant).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                  WithMessage("O campo EmitXfant da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.EnderEmitXbairro).NotNullableAndNotEmpty().And.
                         MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                  WithMessage("O campo EnderEmitXbairro da tabela ICte05 n�o pode ser vazio!");

            /*
            Define(p => p.EnderEmitCmun.Value.ToString()).NotNullableAndNotEmpty().And.
                       MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER2)).
                       WithMessage("O campo EnderEmitCmun da tabela ICte05 n�o est� no formato correto!");
            */

            Define(p => p.EnderEmitXmun).NotNullableAndNotEmpty().And.
                        MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo EnderEmitXmun da tabela ICte05 n�o est� no formato correto!");
            
            /*
           Define(p => p.EnderEmitCep.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER33)).
                      WithMessage("O campo EnderEmitCep da tabela ICte05 n�o est� no formato correto!");
           */

            Define(p => p.EnderEmitFone).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36)).
                       WithMessage("O campo EnderEmitFone da tabela ICte05 n�o est� no formato correto!");

            /*
           Define(p => p.RemCnpj.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER6)).
                      WithMessage("O campo RemCnpj da tabela ICte05 n�o est� no formato correto!");

           Define(p => p.RemCpf.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER7)).
                      WithMessage("O campo RemCpf da tabela ICte05 n�o est� no formato correto!");
           */

            Define(p => p.RemIe).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER26)).
                       WithMessage("O campo RemIe da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.RemXnome).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                      WithMessage("O campo RemXnome da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.RemXfant).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                      WithMessage("O campo RemXfant da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.EnderEmitFone).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36)).
                      WithMessage("O campo EnderEmitFone da tabela ICte05 n�o est� no formato correto!");

            Define(p => p.EnderRemeXbairro).NotNullableAndNotEmpty().And.
                         MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                        WithMessage("O campo EnderRemeXbairro da tabela ICte05 n�o pode ser vazio!");

            /*
           Define(p => p.EnderRemeCmun.Value.ToString()).NotNullableAndNotEmpty().And.
                       MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER2)).
                      WithMessage("O campo EnderRemeCmun da tabela ICte05 n�o pode ser vazio!");
           */

            Define(p => p.EnderRemeXmun).NotNullableAndNotEmpty().And.
                      MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER32)).
                     WithMessage("O campo EnderRemeXmun da tabela ICte05 n�o pode ser vazio!");
            /*
           Define(p => p.EnderRemeCep.Value.ToString()).MatchWith(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER33)).
                    WithMessage("O campo EnderRemeCep da tabela ICte05 n�o pode ser vazio!");
             */
        }

        #endregion
    }
}