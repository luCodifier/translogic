namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes dos componestes de valor da prestacao do complementado
	/// </summary>
	public class Cte36 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdIcc { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIccCompComp { get; set; }

		/// <summary>
		/// Nome do componente
		/// </summary>
		public virtual string CompCompXnome { get; set; }

		/// <summary>
		/// Valor do componente
		/// </summary>
		public virtual double? CompCompVcomp { get; set; }
	}
}