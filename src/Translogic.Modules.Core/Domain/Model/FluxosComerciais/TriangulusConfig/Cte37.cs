namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela ICTE37_ICA
	/// </summary>
	public class Cte37 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Chave de acesso do CT-e original a ser anulado e substituido
		/// </summary>
		public virtual string InfCteAnuEntChCTe { get; set; }

		/// <summary>
		/// Data de emiss�o da declara��o do tomador n�o contribuinte do ICMS
		/// </summary>
		public virtual DateTime? InfCteAnuEntDemi { get; set; }
	}
}