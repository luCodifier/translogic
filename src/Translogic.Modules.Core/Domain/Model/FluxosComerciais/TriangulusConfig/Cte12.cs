namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes dos cointainers da qtde da carga (1-N)
	/// </summary>
	public class Cte12 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnCont { get; set; }

		/// <summary>
		/// N�mero do container
		/// </summary>
		public virtual string ContQtNcont { get; set; }

		/// <summary>
		/// Data prevista da entrega
		/// </summary>
		public virtual DateTime? ContQtDprev { get; set; }
	}
}