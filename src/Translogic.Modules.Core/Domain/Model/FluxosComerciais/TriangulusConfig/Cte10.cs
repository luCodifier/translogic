namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes do CT-e Normal ou Anulacao (0-1)
	/// </summary>
	public class Cte10 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Valor total da mercadoria
		/// </summary>
		public virtual double? InfCargaVmerc { get; set; }

		/// <summary>
		/// Produto predominante
		/// </summary>
		public virtual string InfCargaPropred { get; set; }

		/// <summary>
		/// Outras caracterÝsticas da carga
		/// </summary>
		public virtual string InfCargaXoutCat { get; set; }
	}
}