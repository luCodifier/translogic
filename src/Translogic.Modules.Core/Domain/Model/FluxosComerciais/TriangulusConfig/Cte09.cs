namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de componentes do valor da prestacao (0-N)
	/// </summary>
	public class Cte09 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdComp { get; set; }

		/// <summary>
		/// Nome do componente
		/// </summary>
		public virtual string CompXnome { get; set; }

		/// <summary>
		/// Valor do componente
		/// </summary>
		public virtual double? CompVcomp { get; set; }
	}
}