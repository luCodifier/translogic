namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela ICTE24_ICN_NATCARGA_INFMANU 
	/// </summary>
	public class Cte24 : EntidadeBase<int>
	{
		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdNatCarga { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdNatCargaInfManu { get; set; }

		/// <summary>
		/// Informações do Manuseio
		/// </summary>
		public virtual int? NatCargaCInfManu { get; set; }

		/// <summary>
		/// Carga Especial
		/// </summary>
		public virtual int? NatCargaCImp { get; set; }
	}
}