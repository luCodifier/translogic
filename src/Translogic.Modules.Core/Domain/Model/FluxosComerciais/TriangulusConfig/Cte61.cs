namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Informa��es da Ferrovia Substitu�da (0 - 1)
	/// </summary>
	public class Cte61 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// N�mero do CNPJ
		/// </summary>
		public virtual int? FerroSubCnpj { get; set; }

		/// <summary>
		/// C�digo interno da Ferrovia Substituta
		/// </summary>
		public virtual string FerroSubCint { get; set; }

		/// <summary>
		/// Inscri��o Estadual
		/// </summary>
		public virtual string FerroSubIe { get; set; }

		/// <summary>
		/// Raz�o Social ou Nome
		/// </summary>
		public virtual string FerroSubXnome { get; set; }

		/// <summary>
		/// Logradouro da ferrovia
		/// </summary>
		public virtual string EnderFerroXlgr { get; set; }

		/// <summary>
		/// N�mero do endere�o da ferrovia
		/// </summary>
		public virtual string EnderFerroNro { get; set; }

		/// <summary>
		/// Complemento do endere�o da ferrovia
		/// </summary>
		public virtual string EnderFerroXcpl { get; set; }

		/// <summary>
		/// Bairro da ferrovia
		/// </summary>
		public virtual string EnderFerroXbairro { get; set; }

		/// <summary>
		/// C�digo do munic�pio
		/// </summary>
		public virtual int? EnderFerroCmun { get; set; }

		/// <summary>
		/// Nome do munic�pio
		/// </summary>
		public virtual string EnderFerroXmun { get; set; }

		/// <summary>
		/// CEP da ferrovia
		/// </summary>
		public virtual int? EnderFerroCep { get; set; }

		/// <summary>
		/// Sigla da UF
		/// </summary>
		public virtual string EnderFerroUf { get; set; }
	}
}