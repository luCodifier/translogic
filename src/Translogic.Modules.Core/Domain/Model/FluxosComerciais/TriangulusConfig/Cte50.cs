namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// N�o utilizada para dados, utilizada somente para saber qual impressora que ser� impresso o(s) Documentos
	/// </summary>
	public class Cte50 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdImp { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodCliFil { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodCliEmp { get; set; }

		/// <summary>
		/// Identifica��o do Local de Impress�o . Cada Local de Impress�o (Server ou Client) ter� um nome definido no Sistema, que ser� utilizado para controlar as emiss�es.
		/// </summary>
		public virtual string LocalPrt { get; set; }

		/// <summary>
		/// Identifica��o da Impressora a ser utilizada pelo Sistema para emiss�o do documento
		/// </summary>
		public virtual string IdentPrt { get; set; }

		/// <summary>
		/// Numero de copias a serem impressas
		/// </summary>
		public virtual int NrCop { get; set; }

		/// <summary>
		/// String para redirecionar a captura do papel para a impress�o do documento na bandeja da impressora
		/// </summary>
		public virtual string BandejaEntrada { get; set; }

		/// <summary>
		/// Imprimi frente verso 0 - N�o / 1 - Sim
		/// </summary>
		public virtual int ImpFrenteVerso { get; set; }

		/// <summary>
		/// 0- original, P.S. ou sulfite 1-copia, sulfite
		/// </summary>
		public virtual int Copia { get; set; }
	}
}