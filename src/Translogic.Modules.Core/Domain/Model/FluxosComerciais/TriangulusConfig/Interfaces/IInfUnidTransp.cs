﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces
{
    /// <summary>
    /// Interface da unidade de tranporte
    /// </summary>
    public interface IInfUnidTransp
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        string CfgSerie { get; set; }

        /// <summary>
        /// Relacionamento com a tabela
        /// </summary>
        int IdIcnInf { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        int IdInfUnidTransp { get; set; }

        /// <summary>
        /// Tipo da Unidade de Transporte
        /// </summary>
        int TpUnidTransp { get; set; }

        /// <summary>
        /// Identificação da Unidade de Transporte
        /// </summary>
        int IdUnidTransp { get; set; }

        /// <summary>
        /// Quantidade rateada (Peso, Volume)
        /// </summary>
        double QtdRat { get; set; }
    }
}