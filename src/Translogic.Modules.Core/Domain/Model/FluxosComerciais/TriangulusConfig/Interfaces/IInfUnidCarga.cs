﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces
{
    /// <summary>
    /// Interface de informações da carga
    /// </summary>
    public interface IInfUnidCarga
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        string CfgSerie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        int IdInfUnidCarga { get; set; }
        
        /// <summary>
        /// Relacionamento com a tabela
        /// </summary>
        int IdIcnInf { get; set; }

        /// <summary>
        /// IIdentificação da Unidade de Carga
        /// </summary>
        string IdUnidCarga { get; set; }

        /// <summary>
        /// Tipo da Unidade de Carga
        /// </summary>
        int TpUnidCarga { get; set; }

        /// <summary>
        /// Quantidade rateada (Peso, Volume)
        /// </summary>
        double QtdRat { get; set; } 
    }
}