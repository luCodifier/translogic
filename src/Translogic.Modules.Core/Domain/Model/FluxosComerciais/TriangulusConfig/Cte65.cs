namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Lacres dos vag�es do DCL (0 - n)
	/// </summary>
	public class Cte65 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDcl { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDetVag { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdLacDetVag { get; set; }

		/// <summary>
		/// n�mero do lacre
		/// </summary>
		public virtual string LacDetVagNlacre { get; set; }
	}
}