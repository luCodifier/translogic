namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de campo uso de uso livre do fisco (0-10)
	/// </summary>
	public class Cte04 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		///  Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdObsFisco { get; set; }

		/// <summary>
		/// Identifica��o do campo
		/// </summary>
		public virtual string ObsFiscoXcampo { get; set; }

		/// <summary>
		/// conte�do do campo
		/// </summary>
		public virtual string ObsFiscoXtexto { get; set; }
	}
}