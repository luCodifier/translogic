﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;
    
    /// <summary>
    /// Tabelas de informacoes de vinculado multimodal
    /// </summary>
    public class Cte32 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdInfServVinc { get; set; }

        /// <summary>
        /// Chave de acesso do CT-e Multimodal
        /// </summary>
        public virtual string InfCteMultiModalChcte { get; set; }
    }
}