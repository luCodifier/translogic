namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Informa��es da DCL (0 - n)
	/// </summary>
	public class Cte63 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDcl { get; set; }

		/// <summary>
		/// s�rie da DCL
		/// </summary>
		public virtual string DclSerie { get; set; }

		/// <summary>
		/// n�mero da DCL
		/// </summary>
		public virtual int? DclNdcl { get; set; }

		/// <summary>
		/// Data de emiss�o
		/// </summary>
		public virtual DateTime? DclDemi { get; set; }

		/// <summary>
		/// Quantidade de Vag�es
		/// </summary>
		public virtual int? DclQvag { get; set; }

		/// <summary>
		/// peso para c�lculo em Toneladas
		/// </summary>
		public virtual double? DclPcalc { get; set; }

		/// <summary>
		/// Valor da Tarifa
		/// </summary>
		public virtual double? DclVtar { get; set; }

		/// <summary>
		/// Valor do Frete
		/// </summary>
		public virtual double? DclVfrete { get; set; }

		/// <summary>
		/// Valor dos Servi�os Acess�rios
		/// </summary>
		public virtual double? DclVsacess { get; set; }

		/// <summary>
		/// Valor Total do Servi�o
		/// </summary>
		public virtual double? DclVtServ { get; set; }

		/// <summary>
		/// Identifica��o do trem
		/// </summary>
		public virtual string DclIdTrem { get; set; }
	}
}