namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes do seguro da carga (0-N)
	/// </summary>
	public class Cte16 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdSeg { get; set; }

		/// <summary>
		/// Respons�vel pelo Seguro
		/// </summary>
		public virtual string SegRespSeg { get; set; }

		/// <summary>
		/// Nome da Seguradora
		/// </summary>
		public virtual string SegXseg { get; set; }

		/// <summary>
		/// N�mero da Ap�lice
		/// </summary>
		public virtual string SegNapol { get; set; }

		/// <summary>
		/// N�mero da Averba��o
		/// </summary>
		public virtual string SegNaver { get; set; }

		/// <summary>
		/// Valor da mercadoria para efeito de averba��o
		/// </summary>
		public virtual double? SegVmerc { get; set; }
	}
}