namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes basicas dois doc de transportes anteriores (1,N)
	/// </summary>
	public class Cte15 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnDoc { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnDocId { get; set; }

		/// <summary>
		/// Tipo de documento de transporte anterior
		/// </summary>
		public virtual int? IdDocAntPapTpdoc { get; set; }

		/// <summary>
		/// S�rie (transportes anteriores)
		/// </summary>
		public virtual string IdDocAntPapSerie { get; set; }

		/// <summary>
		/// Sub-s�rie (transportes anteriores)
		/// </summary>
		public virtual string IdDocAntPapSubser { get; set; }

		/// <summary>
		/// N�mero (transportes anteriores)
		/// </summary>
		public virtual string IdDocAntPapNDoc { get; set; }

		/// <summary>
		/// Data da emiss�o (transportes anteriores)
		/// </summary>
		public virtual DateTime? IdDocAntPapDemi { get; set; }

		/// <summary>
		/// Chave de acesso do CT-e
		/// </summary>
		public virtual string IdDocAntEleChave { get; set; }
	}
}