namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Informa��es de detalhes dos Vag�es (1 - n)
	/// </summary>
	public class Cte64 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDcl { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDetVag { get; set; }

		/// <summary>
		/// N�mero de Identifica��o do vag�o
		/// </summary>
		public virtual int? DclDetVagNvag { get; set; }

		/// <summary>
		/// Capacidade em Toneladas
		/// </summary>
		public virtual double? DclDetVagCap { get; set; }

		/// <summary>
		/// Tipo de Vag�o
		/// </summary>
		public virtual string DclDetVagTpVag { get; set; }

		/// <summary>
		/// Peso Real em Toneladas
		/// </summary>
		public virtual double? DclDetVagPesoReal { get; set; }

		/// <summary>
		/// Peso Base de C�lculo de Frete em Toneladas
		/// </summary>
		public virtual double? DclDetVagPesoBc { get; set; }
	}
}