﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Interfaces;

    /// <summary>
    /// Tabela de Informações da Unidade de Caga (Containeres/ULD/Outros)
    /// </summary>
    public class Cte113 : EntidadeBase<int>, IInfUnidCarga
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Numero sequecial da tabela (PK)
        /// </summary>
        public virtual int IdInfUnidCarga { get; set; }

        /// <summary>
        /// Relacionamento com a tabela ICTE111_INFOUTROS_INFUNIDTRANSP (FK)
        /// </summary>
        public virtual int IdInfUnidTransp { get; set; }

        /// <summary>
        /// Relacionamento com a tabela ICTE111_INFOUTROS_INFUNIDTRANSP (FK)
        /// </summary>
        public virtual int IdIcnInf { get; set; }

        /// <summary>
        /// Tipo da Unidade de Carga
        /// </summary>
        public virtual int TpUnidCarga { get; set; }

        /// <summary>
        /// IIdentificação da Unidade de Carga
        /// </summary>
        public virtual string IdUnidCarga { get; set; }

        /// <summary>
        /// Quantidade rateada (Peso, Volume)
        /// </summary>
        public virtual double QtdRat { get; set; }
    }
}