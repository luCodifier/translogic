namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes do Log de erro
	/// </summary>
	public class Nfe20ErroLog : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo do ErroLog (PK)
		/// </summary>
		public virtual int IdErroLog { get; set; }

		/// <summary>
		/// Codigo da Filial do cte
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// Numero do cte
		/// </summary>
		public virtual int NumeroCte { get; set; }

		/// <summary>
		/// SerieCte do cte
		/// </summary>
		public virtual int SerieCte { get; set; }

		/// <summary>
		/// Data do erro
		/// </summary>
		public virtual DateTime Data { get; set; }

		/// <summary>
		/// Descricao Completa do erro
		/// </summary>
		public virtual string DescricaoCompleta { get; set; }
	}
}