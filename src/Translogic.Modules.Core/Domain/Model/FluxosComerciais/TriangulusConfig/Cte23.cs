namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Natureza da Carga
    /// </summary>
    public class Cte23 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Dimens�o (Natureza da Carga)
        /// </summary>
        public virtual string NatCargaXdime { get; set; }
    }
}