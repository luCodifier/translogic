namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes dos documentos (NF, Nfe e Outros)
	/// </summary>
	public class Cte06 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdEmitInf { get; set; }

		/// <summary>
		/// N�mero do Romaneio da NF
		/// </summary>
		public virtual string InfNfNroma { get; set; }

		/// <summary>
		/// N�mero do Pedido da NF
		/// </summary>
		public virtual string InfNfNped { get; set; }

		/// <summary>
		/// S�rie do documento
		/// </summary>
		public virtual string InfNfSerie { get; set; }

		/// <summary>
		/// N�mero do documento
		/// </summary>
		public virtual string InfNfNdoc { get; set; }

		/// <summary>
		/// Data da emiss�o
		/// </summary>
		public virtual DateTime? InfNfDemi { get; set; }

		/// <summary>
		/// Base de C�lculo do ICMS
		/// </summary>
		public virtual double? InfNfVbc { get; set; }

		/// <summary>
		/// Valor total do ICMS
		/// </summary>
		public virtual double? InfNfVicms { get; set; }

		/// <summary>
		/// Base de c�lculo do ICMS ST
		/// </summary>
		public virtual double? InfNfVbcst { get; set; }

		/// <summary>
		/// Valor total do ICMS ST
		/// </summary>
		public virtual double? InfNfVst { get; set; }

		/// <summary>
		/// Valor total dos produtos
		/// </summary>
		public virtual double? InfNfVprod { get; set; }

		/// <summary>
		/// Valor total da NF
		/// </summary>
		public virtual double? InfNfVnf { get; set; }

		/// <summary>
		/// CFOP predominante
		/// </summary>
		public virtual int? InfNfNcfop { get; set; }

		/// <summary>
		/// Peso do documento
		/// </summary>
		public virtual double? InfNfNpeso { get; set; }

		/// <summary>
		/// PIN SUFRAMA
		/// </summary>
		public virtual int? InfNfPin { get; set; }

		/// <summary>
		/// CNPJ do documento
		/// </summary>
		public virtual long? LocRetCnpj { get; set; }

		/// <summary>
		/// CPF do documento
		/// </summary>
		public virtual long? LocRetCpf { get; set; }

		/// <summary>
		/// Raz�o social
		/// </summary>
		public virtual string LocRetXnome { get; set; }

		/// <summary>
		/// Logradouro do documento
		/// </summary>
		public virtual string LocRetXlgr { get; set; }

		/// <summary>
		/// N�mero do documento
		/// </summary>
		public virtual string LocRetNro { get; set; }

		/// <summary>
		/// Complemento do documento
		/// </summary>
		public virtual string LocRetXcpl { get; set; }

		/// <summary>
		/// Bairro retirada
		/// </summary>
		public virtual string LocRetXbairro { get; set; }

		/// <summary>
		/// C�digo Munic�pio
		/// </summary>
		public virtual int? LocRetCmun { get; set; }

		/// <summary>
		/// Nome Munic�pio
		/// </summary>
		public virtual string LocRetXmun { get; set; }

		/// <summary>
		/// UF do documento
		/// </summary>
		public virtual string LocRetUf { get; set; }

		/// <summary>
		/// Chave de acesso da NF-e
		/// </summary>
		public virtual string InfNfeChave { get; set; }

		/// <summary>
		/// PIN SUFRAMA
		/// </summary>
		public virtual int? InfNfePin { get; set; }

		/// <summary>
		/// Tipo de documento origin�rio
		/// </summary>
		public virtual int? InfOutrosTpDoc { get; set; }

		/// <summary>
		/// Descri��o outros
		/// </summary>
		public virtual string InfOutrosDescOutros { get; set; }

		/// <summary>
		/// N�mero do documento
		/// </summary>
		public virtual string InfOutrosNdoc { get; set; }

		/// <summary>
		/// Data da emiss�o
		/// </summary>
		public virtual DateTime? InfOutrosDemi { get; set; }

		/// <summary>
		/// Valor do documento
		/// </summary>
		public virtual double? InfOutrosVdocFisc { get; set; }

		/// <summary>
		/// Modelo da Nota Fiscal
		/// </summary>
		public virtual int? InfNfMod { get; set; }
	}
}