namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Dados das Duplicatas
	/// </summary>
	public class Cte17 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdDup { get; set; }

		/// <summary>
		/// N�mero da duplicata
		/// </summary>
		public virtual double? DupNdup { get; set; }

		/// <summary>
		/// Data de vencimento da duplicata
		/// </summary>
		public virtual DateTime? DupDvenc { get; set; }

		/// <summary>
		/// Valor da duplicata
		/// </summary>
		public virtual double? DupVdup { get; set; }
	}
}