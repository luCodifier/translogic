namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacao do emitente do CT-e
	/// </summary>
	public class Cte05 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// CNPJ de informa��o do CTE
		/// </summary>
		public virtual long? EmitCnpj { get; set; }

		/// <summary>
		/// Inscri��o Estadual
		/// </summary>
		public virtual string EmitIe { get; set; }

		/// <summary>
		/// Raz�o social
		/// </summary>
		public virtual string EmitXnome { get; set; }

		/// <summary>
		/// Nome fantasia
		/// </summary>
		public virtual string EmitXfant { get; set; }

		/// <summary>
		/// Logradouro do CTE
		/// </summary>
		public virtual string EnderEmitXlgr { get; set; }

		/// <summary>
		/// N�mero do CTE
		/// </summary>
		public virtual string EnderEmitNro { get; set; }

		/// <summary>
		/// Complemento do endere�o
		/// </summary>
		public virtual string EnderEmitXcpl { get; set; }

		/// <summary>
		/// Bairro do Cte
		/// </summary>
		public virtual string EnderEmitXbairro { get; set; }

		/// <summary>
		/// C�digo Munic�pio
		/// </summary>
		public virtual int? EnderEmitCmun { get; set; }

		/// <summary>
		/// Nome Munic�pio
		/// </summary>
		public virtual string EnderEmitXmun { get; set; }

		/// <summary>
		/// CEP do CTE
		/// </summary>
		public virtual int? EnderEmitCep { get; set; }

		/// <summary>
		/// UF do emitente do CTE
		/// </summary>
		public virtual string EnderEmitUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s
		/// </summary>
		public virtual int? EnderEmitCpais { get; set; }

		/// <summary>
		/// Nome do Pa�s
		/// </summary>
		public virtual string EnderEmitXpais { get; set; }

		/// <summary>
		/// telefone do CTE
		/// </summary>
		public virtual string EnderEmitFone { get; set; }

		/// <summary>
		/// CNPJ do CTE
		/// </summary>
		public virtual long? RemCnpj { get; set; }

		/// <summary>
		/// CPF do remetente
		/// </summary>
		public virtual long? RemCpf { get; set; }

		/// <summary>
		/// Inscri��o Estadual
		/// </summary>
		public virtual string RemIe { get; set; }

		/// <summary>
		/// Raz�o social
		/// </summary>
		public virtual string RemXnome { get; set; }

		/// <summary>
		/// Nome fantasia
		/// </summary>
		public virtual string RemXfant { get; set; }

		/// <summary>
		/// Telefone do remetente
		/// </summary>
		public virtual string RemFone { get; set; }

		/// <summary>
		/// Logradouro do CTE
		/// </summary>
		public virtual string EnderRemeXlgr { get; set; }

		/// <summary>
		/// N�mero do CTE
		/// </summary>
		public virtual string EnderRemeNro { get; set; }

		/// <summary>
		/// Complemento do CTE
		/// </summary>
		public virtual string EnderRemeXcpl { get; set; }

		/// <summary>
		/// Bairro do CTE
		/// </summary>
		public virtual string EnderRemeXbairro { get; set; }

		/// <summary>
		/// C�digo Munic�pio
		/// </summary>
		public virtual int? EnderRemeCmun { get; set; }

		/// <summary>
		/// Nome Munic�pio
		/// </summary>
		public virtual string EnderRemeXmun { get; set; }

		/// <summary>
		/// CEP do CTE
		/// </summary>
		public virtual int? EnderRemeCep { get; set; }

		/// <summary>
		/// UF do remetente do CTE
		/// </summary>
		public virtual string EnderRemeUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s
		/// </summary>
		public virtual int? EnderRemeCpais { get; set; }

		/// <summary>
		/// N�mero do CNPJ
		/// </summary>
		public virtual string EnderRemeXpais { get; set; }
        
		/// <summary>
		/// N�mero do Cnpj
		/// </summary>
		public virtual long LocColetaCnpj { get; set; }

		/// <summary>
		/// N�mero do CPF
		/// </summary>
        public virtual string LocColetaCpf { get; set; }
        
        /// <summary>
		/// Raz�o Social ou Nome
		/// </summary>
		public virtual string LocColetaxNome { get; set; }
        
        /// <summary>
        /// Logradouro do local de coleta
		/// </summary>
		public virtual string LocColetaxLgr { get; set; }
        
        /// <summary>
        /// N�mero do local de coleta
		/// </summary>
		public virtual string LocColetaNro { get; set; }
        
        /// <summary>
        /// Complemento do local de coleta
		/// </summary>
		public virtual string LocColetaxCpl { get; set; }
        
        /// <summary>
        /// Bairro do local de coleta
		/// </summary>
		public virtual string LocColetaxBairro { get; set; }
        
        /// <summary>
        /// C�digo do munic�pio (utilizar a tabela do IBGE) do local de coleta
		/// </summary>
        public virtual int LocColetacMun { get; set; }
        
        /// <summary>
        /// Nome do munic�pio do local de coleta
		/// </summary>
		public virtual string LocColetaxMun { get; set; }
        
        /// <summary>
        /// Sigla da UF do local de coleta 
		/// </summary>
		public virtual string LocColetaUf { get; set; }
	}
}