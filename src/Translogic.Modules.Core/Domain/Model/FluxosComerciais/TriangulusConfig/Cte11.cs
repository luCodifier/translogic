namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes da qtde da carga (1-N)
	/// </summary>
	public class Cte11 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnInfq { get; set; }

		/// <summary>
		/// C�digo da Unidade de Medida
		/// </summary>
		public virtual int? InfqCunid { get; set; }

		/// <summary>
		/// Tipo da Medida
		/// </summary>
		public virtual string InfqTpMed { get; set; }

		/// <summary>
		/// Quantidade da Carga
		/// </summary>
		public virtual double? InfqQcarga { get; set; }
	}
}