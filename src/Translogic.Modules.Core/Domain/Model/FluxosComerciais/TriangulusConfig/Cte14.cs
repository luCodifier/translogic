namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes de emitente dos doc anteriores (0-N)
	/// </summary>
	public class Cte14 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcnDoc { get; set; }

		/// <summary>
		/// CNPJ (informacoes de emitente dos doc anteriores)
		/// </summary>
		public virtual long? EmiDocAntCnpj { get; set; }

		/// <summary>
		/// CPF (informacoes de emitente dos doc anteriores)
		/// </summary>
		public virtual long? EmiDocAntCpf { get; set; }

		/// <summary>
		/// IEa (informacoes de emitente dos doc anteriores)
		/// </summary>
		public virtual string EmiDocAntIe { get; set; }

		/// <summary>
		/// UF (informacoes de emitente dos doc anteriores)
		/// </summary>
		public virtual string EmiDocAntUf { get; set; }

		/// <summary>
		/// Nome / raz�o social
		/// </summary>
		public virtual string EmiDocAntXnome { get; set; }
	}
}