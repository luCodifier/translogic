﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Tabela de Informações da Unidade de Caga (Containeres/ULD/Outros)
    /// </summary>
    public class Cte121 : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo interno da filial (PK)
        /// </summary>
        public virtual int CfgUn { get; set; }

        /// <summary>
        /// Numero do documento CT-e (PK)
        /// </summary>
        public virtual int CfgDoc { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual string CfgSerie { get; set; }

        /// <summary>
        /// Serie do documento CT-e (PK)
        /// </summary>
        public virtual int IdCceInfCorrecao { get; set; }

        /// <summary>
        /// Grupo de informações que pertence o campoAlterado
        /// </summary>
        public virtual string GrupoAlterado { get; set; }

        /// <summary>
        /// Nome do campo modificado do CT-e Original.
        /// </summary>
        public virtual string CampoAlterado { get; set; }

        /// <summary>
        /// Valor correspondente à alteração
        /// </summary>
        public virtual string ValorAlterado { get; set; }

        /// <summary>
        /// Indice do item alterado caso a alteração ocorra em uma lista
        /// </summary>
        public virtual string NroItemAlterado { get; set; }
    }
}