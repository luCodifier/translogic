namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela utilizada para dados principais do CT-e (1-1)
	/// </summary>
	public class Cte01 : EntidadeBase<int>
	{
		/// <summary> 
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Versao do CT-e
		/// </summary>
		public virtual double? InfCteVersao { get; set; }

		/// <summary>
		/// Identificador da TAG a ser assinada
		/// </summary>
		public virtual string InfCteId { get; set; }

		/// <summary>
		/// C�digo de UF emitente do Documento Fiscal
		/// </summary>
		public virtual int? IdeCuf { get; set; }

		/// <summary>
		/// C�digo num�rico que comp�e a Chave de acesso
		/// </summary>
		public virtual int? IdeCct { get; set; }

		/// <summary>
		/// CFOP do CTE
		/// </summary>
		public virtual int? IdeCfOp { get; set; }

		/// <summary>
		/// Descri��o da Natureza da Opera��o
		/// </summary>
		public virtual string IdeNatOp { get; set; }

		/// <summary>
		/// Forma de pagamento do servi�o
		/// </summary>
		public virtual string IdeForPag { get; set; }

		/// <summary>
		/// Modelo do Documento Fiscal
		/// </summary>
		public virtual string IdeMod { get; set; }

		/// <summary>
		/// S�rie do CTE
		/// </summary>
		public virtual int? IdeSerie { get; set; }

		/// <summary>
		/// N�mero do conhecimento
		/// </summary>
		public virtual int? IdeNct { get; set; }

		/// <summary>
		/// Data e Hora de emiss�o
		/// </summary>
		public virtual DateTime? IdeDhEmi { get; set; }

		/// <summary>
		/// Formato de Impress�o do DACTE
		/// </summary>
		public virtual int? IdeTpImp { get; set; }

		/// <summary>
		/// Forma de emiss�o do CT-e
		/// </summary>
		public virtual int? IdeTpEmis { get; set; }

		/// <summary>
		/// D�gito Verificador da chave de acesso do CT-e
		/// </summary>
		public virtual int? IdeCdv { get; set; }

		/// <summary>
		/// Identifica��o do ambiente
		/// </summary>
		public virtual int? IdeTpAmb { get; set; }

		/// <summary>
		/// tipo do Conhecimento
		/// </summary>
		public virtual int? IdeTpCte { get; set; }

		/// <summary>
		/// Processo de emiss�o do CT-e
		/// </summary>
		public virtual int? IdeProcEmi { get; set; }

		/// <summary>
		/// Vers�o do Processo de emiss�o do CT-e
		/// </summary>
		public virtual string IdeVerProc { get; set; }

		/// <summary>
		/// Chave de acesso do CT-e referenciado
		/// </summary>
		public virtual string IdeRefcte { get; set; }

		/// <summary>
		/// C�digo do Munic�pio onde o CT-e est� sendo emitido
		/// </summary>
		public virtual int? IdeCMunEmi { get; set; }

		/// <summary>
		/// Nome do Munic�pio onde o CT-e est� sendo emitido
		/// </summary>
		public virtual string IdeXMunEmi { get; set; }

		/// <summary>
		/// Sigla da UF do local onde o CT-e est� sendo emitido
		/// </summary>
		public virtual string IdeUfEmi { get; set; }

		/// <summary>
		/// Modal do CTE
		/// </summary>
		public virtual int? IdeModal { get; set; }

		/// <summary>
		/// Tipo do Servi�o
		/// </summary>
		public virtual int? IdeTpServ { get; set; }

		/// <summary>
		/// C�digo do Munic�pio do in�cio da presta��o
		/// </summary>
		public virtual int? IdeCMunIni { get; set; }

		/// <summary>
		/// Nome do Munic�pio do in�cio da presta��o
		/// </summary>
		public virtual string IdeXMunIni { get; set; }

		/// <summary>
		/// UF do in�cio da presta��o
		/// </summary>
		public virtual string IdeUfIni { get; set; }

		/// <summary>
		/// C�digo do Munic�pio do t�rmino da presta��o
		/// </summary>
		public virtual int? IdeCMunFim { get; set; }

		/// <summary>
		/// Nome do Munic�pio do t�rmino da presta��o
		/// </summary>
		public virtual string IdeXMunFim { get; set; }

		/// <summary>
		/// UF do t�rmino da presta��o
		/// </summary>
		public virtual string IdeUfFim { get; set; }

		/// <summary>
		/// Recebedor retira no Aeroporto, Filial, Porto ou Esta��o de Destino?
		/// </summary>
		public virtual int? IdeRetira { get; set; }

		/// <summary>
		/// Detalhes do retira
		/// </summary>
		public virtual string IdeXDetRetira { get; set; }

		/// <summary>
		/// tomador do servi�o
		/// </summary>
		public virtual int? Toma03Toma { get; set; }

		/// <summary>
		/// tomador do servi�o
		/// </summary>
		public virtual int? Toma4Toma { get; set; }

		/// <summary>
		/// CNPJ  do CTE
		/// </summary>
		public virtual long? Toma4Cnpj { get; set; }

		/// <summary>
		/// CPF  do CTE
		/// </summary>
		public virtual long? Toma4Cpf { get; set; }

		/// <summary>
		/// Inscri��o Estadual
		/// </summary>
		public virtual string Toma4Ie { get; set; }

		/// <summary>
		/// Raz�o social
		/// </summary>
		public virtual string Toma4xNome { get; set; }

		/// <summary>
		/// Nome fantasia
		/// </summary>
		public virtual string Toma4xFant { get; set; }

		/// <summary>
		/// Telefone  do CTE
		/// </summary>
		public virtual string Toma4Fone { get; set; }

		/// <summary>
		/// Logradouro  do CTE
		/// </summary>
		public virtual string EnderTomaxLgr { get; set; }

		/// <summary>
		/// N�mero  do CTE
		/// </summary>
		public virtual string EnderTomaNro { get; set; }

		/// <summary>
		/// Complemento  do CTE
		/// </summary>
		public virtual string EnderTomaXCpl { get; set; }

		/// <summary>
		/// Bairro  do CTE
		/// </summary>
		public virtual string EnderTomaXBairro { get; set; }

		/// <summary>
		/// C�digo Munic�pio
		/// </summary>
		public virtual int? EnderTomaCMun { get; set; }

		/// <summary>
		/// Nome Munic�pio
		/// </summary>
		public virtual string EnderTomaXMun { get; set; }

		/// <summary>
		/// CEP (Dados do CTE)
		/// </summary>
		public virtual int? EnderTomaCep { get; set; }

		/// <summary>
		/// UF  (Dados do CTE)
		/// </summary>
		public virtual string EnderTomaUf { get; set; }

		/// <summary>
		/// C�digo do Pa�s
		/// </summary>
		public virtual int? EnderTomaCPais { get; set; }

		/// <summary>
		/// Nome do Pa�s
		/// </summary>
		public virtual string EnderTomaXPais { get; set; }

		/// <summary>
		/// Caracter�stica adicional do transporte
		/// </summary>
		public virtual string ComplXCaracAd { get; set; }

		/// <summary>
		/// Caracter�stica adicional do servi�o
		/// </summary>
		public virtual string ComplXCaracSer { get; set; }

		/// <summary>
		/// Funcion�rio emissor do CTe
		/// </summary>
		public virtual string ComplXEmi { get; set; }

		/// <summary>
		/// Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Origem
		/// </summary>
		public virtual string FluxoXOrig { get; set; }

		/// <summary>
		/// Sigla ou c�digo interno da Filial/Porto/Esta��o/ Aeroporto de Destino
		/// </summary>
		public virtual string FluxoXDest { get; set; }

		/// <summary>
		/// C�digo da Rota de Entrega
		/// </summary>
		public virtual string FluxoXRota { get; set; }

		/// <summary>
		/// Tipo de data/per�odo programado para a entrega
		/// </summary>
		public virtual string SemDataTpPer { get; set; }

		/// <summary>
		/// Tipo de data/per�odo programado para a entrega
		/// </summary>
		public virtual string ComDataTpPer { get; set; }

		/// <summary>
		/// Data programada
		/// </summary>
		public virtual DateTime? ComDataDProg { get; set; }

		/// <summary>
		/// Tipo de data/per�odo programado para a entrega
		/// </summary>
		public virtual string NoPeriodoTpPer { get; set; }

		/// <summary>
		/// Data inicial
		/// </summary>
		public virtual DateTime? NoPeriodoDIni { get; set; }

		/// <summary>
		/// Data final
		/// </summary>
		public virtual DateTime? NoPeriodoDFim { get; set; }

		/// <summary>
		/// Tipo de hora/per�odo programado para a entrega
		/// </summary>
		public virtual string SemHoraTpHor { get; set; }

		/// <summary>
		/// Tipo de data/per�odo programado para a entrega
		/// </summary>
		public virtual string ComHoraTpHor { get; set; }

		/// <summary>
		/// Hora programada (HH:MM:SS)
		/// </summary>
		public virtual string ComHoraHProg { get; set; }

		/// <summary>
		/// Tipo de data/per�odo programado para a entrega
		/// </summary>
		public virtual string NoInterTpHor { get; set; }

		/// <summary>
		/// Hora inicial (HH:MM:SS)
		/// </summary>
		public virtual string NoInterHIni { get; set; }

		/// <summary>
		/// Hora Final (HH:MM:SS)
		/// </summary>
		public virtual string NoInterHFim { get; set; }

		/// <summary>
		/// munic�pio origem para efeito de c�lculo do frete
		/// </summary>
		public virtual string ComplOrigCalc { get; set; }

		/// <summary>
		/// munic�pio destino para efeito de c�lculo do frete
		/// </summary>
		public virtual string ComplDestCalc { get; set; }

		/// <summary>
		/// Observa��es Gerais
		/// </summary>
		public virtual string ComplxObs { get; set; }

		/// <summary>
		/// N�mero da fatura
		/// </summary>
		public virtual string FatNFat { get; set; }

		/// <summary>
		/// Valor original da fatura
		/// </summary>
		public virtual double? FatVOrig { get; set; }

		/// <summary>
		/// Valor do desconto da fatura
		/// </summary>
		public virtual double? FatVDesc { get; set; }

		/// <summary>
		/// Valor l�quido da fatura
		/// </summary>
		public virtual double? FatVLiq { get; set; }

        /// <summary>
        /// Campo IdeDhCont
		/// </summary>
		public virtual DateTime? IdeDhCont { get; set; }

        /// <summary>
        /// Campo IdeXJust
		/// </summary>
		public virtual string IdeXJust { get; set; }

        /// <summary>
        /// Campo Toma4Email  
		/// </summary>
		public virtual string Toma4Email { get; set; }

        /// <summary>
        /// Campo IdeIndGlobalizado
		/// </summary>
		public virtual int? IdeIndGlobalizado { get; set; }

        /// <summary>
        /// Campo IdeIndIeToma
		/// </summary>
		public virtual int? IdeIndIeToma { get; set; }
	}
}