namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabelas de informacoes de CT-e complementado
	/// </summary>
	public class Cte35 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
		public virtual int IdIcc { get; set; }

		/// <summary>
		/// Chave do CT-e complementado
		/// </summary>
		public virtual string InfCteChave { get; set; }

		/// <summary>
		/// Valor total da presta��o complementado
		/// </summary>
		public virtual double? VpresVtPrest { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst00Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst00Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst00Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst00Vicms { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst20Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst20PredBc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst20Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst20Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst20Vicms { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst45Cst { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst80Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst80Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst80Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst80Vicms { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst81Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst81PredBc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst81Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst81Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst81Vicms { get; set; }

		/// <summary>
		/// Tributa��o do Servi�o
		/// </summary>
        public virtual string Cst90Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC do ICMS
		/// </summary>
		public virtual double? Cst90PredBc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Cst90Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Cst90Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Cst90Vicms { get; set; }

		/// <summary>
		/// Valor do Cr�dito outorgado/presumido
		/// </summary>
		public virtual double? Cst90Vcred { get; set; }

		/// <summary>
		/// Informa��es adicionais de interesse do Fisco
		/// </summary>
		public virtual string ImpCompInfAdFisco { get; set; }

		/// <summary>
		/// classifica��o Tribut�ria do Servi�o
		/// </summary>
        public virtual string Icms00Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms00Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms00Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms00Vicms { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do servi�o
		/// </summary>
        public virtual string Icms20Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? Icms20PredBc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms20Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms20Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms20Vicms { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
        public virtual string Icms45Cst { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
        public virtual string Icms60Cst { get; set; }

		/// <summary>
		/// Valor da BC do ICMS ST retido
		/// </summary>
		public virtual double? Icms60VbcStRet { get; set; }

		/// <summary>
		/// Valor do ICMS ST retido
		/// </summary>
		public virtual double? Icms60VicmsStRet { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms60PicmsStRet { get; set; }

		/// <summary>
		/// Valor do Cr�dito outorgado/Presumido
		/// </summary>
		public virtual double? Icms60Vcred { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual string Icms90Cst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? Icms90PredBc { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? Icms90Vbc { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? Icms90Picms { get; set; }

		/// <summary>
		/// Valor do ICMS
		/// </summary>
		public virtual double? Icms90Vicms { get; set; }

		/// <summary>
		/// Valor do Cr�dito Outorgado/Presumido
		/// </summary>
		public virtual double? Icms90Vcred { get; set; }

		/// <summary>
		/// Classifica��o Tribut�ria do Servi�o
		/// </summary>
		public virtual int? IcmsOutraUfCst { get; set; }

		/// <summary>
		/// Percentual de redu��o da BC
		/// </summary>
		public virtual double? IcmsOutraUfPredBcOutraUf { get; set; }

		/// <summary>
		/// Valor da BC do ICMS
		/// </summary>
		public virtual double? IcmsOutraUfVbcOutraUf { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? IcmsOutraUfPicmsOutrauf { get; set; }

		/// <summary>
		/// Valor do ICMS devido outra UF
		/// </summary>
		public virtual double? IcmsOutraUfVicmsOutraUf { get; set; }

		/// <summary>
		/// Indica se o contribuinte � Simples Nacional 1=Sim
		/// </summary>
		public virtual int? IcmsSnIndSn { get; set; }
	}
}