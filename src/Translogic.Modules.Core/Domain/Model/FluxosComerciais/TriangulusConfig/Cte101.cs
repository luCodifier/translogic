namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela de informacoes dos documentos (NF, Nfe e Outros)
	/// </summary>
	public class Cte101 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequecial da tabela (PK)
		/// </summary>
        public virtual int IdIcnInfNfe { get; set; }

		/// <summary>
		/// Chave de acesso da NF-e
		/// </summary>
		public virtual string InfNfeChave { get; set; }

		/// <summary>
		/// PIN SUFRAMA
		/// </summary>
		public virtual int? InfNfePin { get; set; }
	}
}