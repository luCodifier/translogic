namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Rateio dos vag�es (0 - n)
	/// </summary>
	public class Cte70 : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdDetVag { get; set; }

		/// <summary>
		/// Numero sequencial da tabela (PK)
		/// </summary>
		public virtual int IdRatVag { get; set; }

		/// <summary>
		/// S�rie do vag�o
		/// </summary>
		public virtual string RatNfSerie { get; set; }

		/// <summary>
		/// N�mero do ratio de vag�es
		/// </summary>
		public virtual string RatNfNdoc { get; set; }

		/// <summary>
		/// Peso rateado
		/// </summary>
		public virtual double? RatNfPesoRat { get; set; }

		/// <summary>
		/// Chave de acesso da NF-e
		/// </summary>
		public virtual string RatNfeChave { get; set; }

		/// <summary>
		/// Peso rateado
		/// </summary>
		public virtual double? RatNfePesoRat { get; set; }
	}
}