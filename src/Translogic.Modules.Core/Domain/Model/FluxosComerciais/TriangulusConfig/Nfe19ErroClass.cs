namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Tabela de informacoes de Erro da Classe
    /// </summary>
    public class Nfe19ErroClass : EntidadeBase<int>
    {
        /// <summary>
        /// Codigo do ErroClass (PK)
        /// </summary>
        public virtual int IdErroClass { get; set; }

        /// <summary>
        /// Descri��o da classe (PK)
        /// </summary>
        public virtual string DescClass { get; set; }

        /// <summary>
        /// Prioridade da classe (PK)
        /// </summary>
        public virtual int Prioridade { get; set; }

        /// <summary>
        /// Flag de erro 
        /// </summary>
        public virtual int? FlagErro { get; set; }

        /// <summary>
        /// Tipo do Suporte
        /// </summary>
        public virtual int? TpSuporte { get; set; }
    }
}