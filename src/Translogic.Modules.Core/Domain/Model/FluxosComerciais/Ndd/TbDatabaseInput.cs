namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Ndd
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Entidade de envio de dados para a NDD
	/// </summary>
	public class TbDatabaseInput : EntidadeBase<int?>
	{
		/// <summary>
		/// Dados do documento (arquivo texto)
		/// </summary>
		public virtual byte[] DocumentData { get; set; }

		/// <summary>
		/// Status da tabela
		/// </summary>
		public virtual int Status { get; set; }

		/// <summary>
		/// Nome da tarefa
		/// </summary>
		public virtual string Job { get; set; }

		/// <summary>
		/// N�mero do documento do usu�rio
		/// </summary>
		public virtual string DocumentUser { get; set; }

		/// <summary>
		/// Tipo de envio
		/// </summary>
		public virtual int Kind { get; set; }

		/// <summary>
		/// Data e hora de envio
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// N�mero de documento de envio
		/// </summary>
		public virtual string NumeroDocumento { get; set; }
	}
}