namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Ndd.Repositories
{
	using System.Text;
	using Acesso;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Reposit�rio do TbDatabaseInput
	/// </summary>
	public interface ITbDatabaseInputRepository : IRepository<TbDatabaseInput, int?>
	{
		/// <summary>
		/// Insere os dados na tabela utilizada pela NDD
		/// </summary>
		/// <param name="numeroDocumento">N�mero do documento a ser inserido</param>
		/// <param name="usuario">Usuario responsavel pela inser��o</param>
		/// <param name="arquivo">Dados do arquivo</param>
		void Inserir(string numeroDocumento, Usuario usuario, StringBuilder arquivo);
	}
}