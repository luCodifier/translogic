namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Estrutura;

    /// <summary>
	/// Representa uma mercadoria, associa com o produto e produto do porto
	/// </summary>
	/// <remarks>
	/// TODO: campos abaixo devem ser revisados
	/// MC_CRG_PRG - Carga perigosa bool
	/// MC_IND_SRV - Indicador de Servico bool
	/// </remarks>
	public class Mercadoria : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Apelido da Mercadoria
		/// </summary>
		public virtual string Apelido { get; set; }

		/// <summary>
		/// C�digo da Mercadoria
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// C�digo da mercadoria no SAP, referente a empresa 201 (Intermodal).
		/// </summary>
		public virtual string CodigoMercadoriaSAP { get; set; }

		/// <summary>
		/// Densidade da Mercadoria
		/// </summary>
		public virtual double? Densidade { get; set; }

		/// <summary>
		/// Descri��o Detalhada da Mercadoria
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descri��o Resumida da Mercadoria
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// Empresa da Mercadoria
		/// </summary>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Modal de Origem da Mercadoria - <see cref="TipoGrupoFluxoEnum" />
		/// </summary>
		public virtual TipoGrupoFluxoEnum? ModalOrigemInformacao { get; set; }

		/// <summary>
		/// Produto da Mercadoria
		/// </summary>
		public virtual Produto Produto { get; set; }

		/// <summary>
		/// Produto do Porto da Mercadoria
		/// </summary>
		public virtual ProdutoPorto ProdutoPorto { get; set; }

		/// <summary>
		/// Unidade de medida - <see cref="UnidadeMedida"/>
		/// </summary>
		public virtual UnidadeMedida UnidadeMedida { get; set; }

		#endregion
	}
}