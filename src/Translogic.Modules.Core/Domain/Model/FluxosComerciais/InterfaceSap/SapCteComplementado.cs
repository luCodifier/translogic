namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP CTE Complementado
    /// </summary>
    public class SapCteComplementado : EntidadeBase<int>
    {
        /// <summary>
        ///  Id da tabela SAP_DESP_CTE. � o ID do CTE complementar.
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  Chave do CTE complementado
        /// </summary>
        public virtual string ChaveCteComplementado { get; set; }

        /// <summary>
        ///  Valor do complemento
        /// </summary>
        public virtual double Valor { get; set; }

        /// <summary>
        ///  Aliquota de ICMS do CTE complementado.
        /// </summary>
        public virtual double? AliquotaIcms { get; set; }

        /// <summary>
        ///  Valor do ICMS do CTE complementado.
        /// </summary>
        public virtual double ValorIcms { get; set; }

        /// <summary>
        ///  Campo Timestamp
        /// </summary>
        public virtual DateTime? Timestamp { get; set; }
    }
}

