namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Documento outras Ferrovias CTE
    /// </summary>
    public class SapDocsoutrasferCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  Tipo de documento emitido pela outra ferrovia - TIF,DCL,CP(carta porte),CTE ou V2.
        /// </summary>
        public virtual string TipoDoc { get; set; }

        /// <summary>
        ///  N�mero do documento. Composi��o da s�rie concatenado com h�fen e o nro do documento, quando for DCL.
        /// </summary>
        public virtual string Nro { get; set; }
    }
}

