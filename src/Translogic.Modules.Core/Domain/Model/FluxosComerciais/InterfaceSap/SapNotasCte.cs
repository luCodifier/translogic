namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Nota CTE
    /// </summary>
    public class SapNotasCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  S�rie da nota
        /// </summary>
        public virtual string SerieNota { get; set; }

        /// <summary>
        ///  N�mero da nota
        /// </summary>
        public virtual string NroNota { get; set; }

        /// <summary>
        ///  Modelo da nota
        /// </summary>
        public virtual string Modelo { get; set; }

        /// <summary>
        /// Valor da nota.
        /// </summary>
        public virtual double Valor { get; set; }

        /// <summary>
        ///  Data e hora de emiss�o da nota
        /// </summary>
        public virtual DateTime? DthRemissao { get; set; }

        /// <summary>
        ///  UF do remetente
        /// </summary>
        public virtual string UfRemetente { get; set; }

        /// <summary>
        ///  CNPJ do remetente
        /// </summary>
        public virtual string CnpjRemetente { get; set; }

        /// <summary>
        ///  Inscri��o estatual do remetente
        /// </summary>
        public virtual string InsRemetente { get; set; }

        /// <summary>
        ///  UF do destinat�rio
        /// </summary>
        public virtual string UfDestinatario { get; set; }

        /// <summary>
        ///  CNPJ do destinat�rio
        /// </summary>
        public virtual string CnpjDestinatario { get; set; }

        /// <summary>
        ///  Inscri��o estatual do destinat�rio
        /// </summary>
        public virtual string InsDestinatario { get; set; }

        /// <summary>
        ///  Chave Nota Fiscal Eletronica
        /// </summary>
        public virtual string Nfe { get; set; }

        /// <summary>
        ///  Peso total da nota.
        /// </summary>
        public virtual double PesoTotal { get; set; }

        /// <summary>
        ///  Peso reateado da nota
        /// </summary>
        public virtual double PesoRateio { get; set; }

        /// <summary>
        ///  Campo da BUNGE que envia na nota fiscal
        /// </summary>
        public virtual string ComplementoBunge { get; set; }

		/// <summary>
		/// Numero do protocolo de autoriza��o na SEFAZ
		/// </summary>
		public virtual string Protocolo { get; set; }
    }
}

