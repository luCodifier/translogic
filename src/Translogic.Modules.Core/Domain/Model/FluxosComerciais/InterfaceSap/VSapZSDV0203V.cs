namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Tabela do SAP Para identificar Pagamento CTE
    /// </summary>
    public class VSapZSDV0203V : EntidadeBase<int>
    {
        /// <summary>
        /// Campo Vbeln
        /// </summary>
        public virtual string Vbeln { get; set; }

        /// <summary>
        /// Campo Tipo
        /// </summary>
        public virtual string Tipo { get; set; }

        /// <summary>
        /// Campo Bstkd (Serie e numero despacho de 5 digitos)
        /// </summary>
        public virtual string Bstkd { get; set; }

        /// <summary>
        /// Campo Belnr
        /// </summary>
        public virtual string Belnr { get; set; }

        /// <summary>
        /// Campo Fatura
        /// </summary>
        public virtual string Fatura { get; set; }

        /// <summary>
        /// Campo Compensacao
        /// </summary>
        public virtual string Compensacao { get; set; }

        /// <summary>
        /// Campo Fatura Compensada
        /// </summary>
		public virtual string FaturaCompensada { get; set; }

        /// <summary>
        /// Campo Chave do CTe
        /// </summary>
		public virtual string ChaveCte { get; set; }
    }
}
