namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
	using System;
	using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

	/// <summary>
	/// Interface SAP 
	/// </summary>
	public class SapDespCte : EntidadeBase<int>
	{
		/// <summary>
		///  S�rie do despacho  de 5 d�gitos
		/// </summary>
		public virtual string SerDesp5 { get; set; }

		/// <summary>
		///  N�mero do despacho de 5 d�gitos
		/// </summary>
		public virtual int? NroDesp5 { get; set; }

		/// <summary>
		///  S�rie do despacho de 6 d�gitos
		/// </summary>
		public virtual string SerDesp6 { get; set; }

		/// <summary>
		///  N�mero do despacho de 6 d�gitos
		/// </summary>
		public virtual int? NroDesp6 { get; set; }

		/// <summary>
		///  C�digo ferrovia+UF usada no DCL de 6 d�gitos
		/// </summary>
		public virtual string FerroviaUf { get; set; }

		/// <summary>
		///  Data do CTE
		/// </summary>
		public virtual DateTime? DthRemissao { get; set; }

		/// <summary>
		///  C�digo do fluxo
		/// </summary>
		public virtual string CodFluxo { get; set; }

		/// <summary>
		///  N�mero do contrato
		/// </summary>
		public virtual string NroContrato { get; set; }

		/// <summary>
		///  Peso para c�lculo do grupo de vag�es
		/// </summary>
		public virtual double? PesoCalculo { get; set; }

		/// <summary>
		///  Tonelada �til do grupo de vag�es.
		/// </summary>
		public virtual double? ToneladaUtil { get; set; }

		/// <summary>
		/// Vers�o do XML do CTE
		/// </summary>
		public virtual double VersaoXml { get; set; }

		/// <summary>
		///  Tipo de opera��o
		/// </summary>
		public virtual string TipoOperacao { get; set; }

		/// <summary>
		///  Data e hora de processamento da informa��o pelo SAP
		/// </summary>
		public virtual DateTime? DthrProc { get; set; }

		/// <summary>
		///  Campo Timestamp
		/// </summary>
		public virtual DateTime? Timestamp { get; set; }

		/// <summary>
		///  Valor do desconto por percentual
		/// </summary>
		public virtual double VlrDescontoPerc { get; set; }

		/// <summary>
		///  Valor do desconto
		/// </summary>
		public virtual double VlrDescontoTon { get; set; }

		/// <summary>
		///  Valor do seguro
		/// </summary>
		public virtual double VlrSeguro { get; set; }

		/// <summary>
		///  Valor da mercadoria (Somat�ria das notas do vag�o)
		/// </summary>
		public virtual double VrlMercadoria { get; set; }

		/// <summary>
		/// Cota��o do d�lar. Sempre a cota��o de D-1.
		/// </summary>
		public virtual double? CotacaoDolar { get; set; }

		/// <summary>
		/// Data de carga.
		/// </summary>
		public virtual DateTime? DataCargaOriginal { get; set; }

		/// <summary>
		///  C�digo do fluxo da outra ferrovia.
		/// </summary>
		public virtual string FlxOutraFerrovia { get; set; }

		/// <summary>
		///  CTE Substitu�do
		/// </summary>
		public virtual string CteSubstituido { get; set; }

		/// <summary>
		///  S�rie 5 CTE Substitu�do
		/// </summary>
		public virtual string Serie5CteSubstituido { get; set; }

		/// <summary>
		/// DCL 5 CTE Substitu�do
		/// </summary>
		public virtual string Dcl5CteSubstituido { get; set; }

		/// <summary>
		/// EDI/MANUAL usado para identificar complemento da BUNGE. E = EDI e M = Manual.
		/// </summary>
		public virtual string Transacao { get; set; }

		/// <summary>
		///  Flag S/N para indicar o processamento.
		/// </summary>
		public virtual string IndProc { get; set; }

		/// <summary>
		/// Ambiente da Sefaz
		/// </summary>
		/// <remarks> 1 - Produ��o / 2 - Homologa��o </remarks>
		public virtual int Ambiente { get; set; }

		/// <summary>
		/// S�rie do Despacho gerado pelo SAP - Utilizado no Cte Complementar
		/// OBS: Esses valores s�o gerados pelo SAP. No entanto quando for um cancelamento de CTE complementar
		/// a s�rie dever� ser preenchida
		/// </summary>
		public virtual string SerieDespachoSap { get; set; }

		/// <summary>
		/// Numero do Despacho gerado pelo SAP - Utilizado no Cte Complementar
		/// OBS: Esses valores s�o gerados pelo SAP. No entanto quando for um cancelamento de CTE complementar
		/// o numero dever� ser preenchido
		/// </summary>
		public virtual int NumeroDespachoSap { get; set; }

		/// <summary>
		/// C�digo do motivo de cancelamento
		/// </summary>
		public virtual int? CodigoMotivoCancelamento { get; set; }

		/// <summary>
		/// Data de vencimento do Cte
		/// </summary>
		public virtual DateTime? Vencimento { get; set; }

        /// <summary>
        /// Tipo de servico do CTE
        /// </summary>
        public virtual int? TipoServico { get; set; }

        /// <summary>
        /// Campo usado pelo SAP nos relat�rios de concilia��o 
        /// </summary>
		public virtual DateTime? DataDescargaNfe { get; set; }

    }
}