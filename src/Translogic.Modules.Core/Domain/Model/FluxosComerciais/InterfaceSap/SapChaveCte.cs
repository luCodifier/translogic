namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Chave CTE
    /// </summary>
    public class SapChaveCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  Letra da ferrovia
        /// </summary>
        public virtual string Ferrovia { get; set; }

        /// <summary>
        /// Chave do CTE
        /// </summary>
        public virtual string ChaveCte { get; set; }

        /// <summary>
        /// Protocolo de autoriza��o ou inutiliza��o do CTE junto ao SEFAZ
        /// </summary>
        public virtual string ProtocoloSefaz { get; set; }

        /// <summary>
        /// Data da autoriza��o do CTE
        /// </summary>
        public virtual DateTime? DthrAutorizacao { get; set; }

        /// <summary>
        ///  Status do CTE. Utilizar o retorno do SEFAZ
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        ///  S�rie do CTE
        /// </summary>
        public virtual string SerieCte { get; set; }

        /// <summary>
        ///  Nro do CTE
        /// </summary>
        public virtual string NroCte { get; set; }

        /// <summary>
        ///  Valor total (mercadoria+impostos+seguro). vBC
        /// </summary>
        public virtual double VlrTotal { get; set; }

        /// <summary>
        ///  Cnpj das filiais
        /// </summary>
        public virtual string Cnpj { get; set; }

        /// <summary>
        /// C�digo SAP da empresa cliente
        /// </summary>
        public virtual string ClienteSap { get; set; }

        /// <summary>
        /// Al�quota do ICMS
        /// </summary>
        public virtual double? AliquotaIcms { get; set; }

        /// <summary>
        /// Valor do ICMS
        /// </summary>
        public virtual double? ValorIcms { get; set; }

        /// <summary>
        /// Observa��o do CTE
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Sequencia do Cte no agrupamento
        /// </summary>
        public virtual int Sequencia { get; set; }

        /// <summary>
        /// C�digo Fiscal de opera��es e presta��es
        /// </summary>
        public virtual string Cfop { get; set; }

        /// <summary>
        /// Chave do CTE origem
        /// </summary>
        public virtual string ChaveCteOrigem { get; set; }
    }
}
