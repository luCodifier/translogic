namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Vagao CTE
    /// </summary>
    public class SapVagaoCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  C�digo do vag�o
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        /// Proprieade do vag�o
        /// </summary>
        public virtual string PropVagao { get; set; }

        /// <summary>
        ///  S�rie do vag�o
        /// </summary>
        public virtual string SerieVagao { get; set; }

        /// <summary>
        ///  Peso para c�lculo do vag�o
        /// </summary>
        public virtual double? PesoCalculo { get; set; }

        /// <summary>
        ///  TU para o vag�o.
        /// </summary>
        public virtual double? ToneladaUtil { get; set; }
    }
}
