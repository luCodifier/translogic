namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Composição Frete CTE
    /// </summary>
    public class SapComposicaoFreteCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  Condição da Tarfia
        /// </summary>
        public virtual string CondicaoTarifa { get; set; }

        /// <summary>
        ///  Codigo da Ferrovia
        /// </summary>
        public virtual string Ferrovia { get; set; }

        /// <summary>
        ///  Ton * VALORCOMICMS da COMP_FRETE_CONTRATO
        /// </summary>
        public virtual double? ValorComIcms { get; set; }

        /// <summary>
        ///  Valor Sem Icms
        /// </summary>
        public virtual double? ValorSemIcms { get; set; }
    }
}

