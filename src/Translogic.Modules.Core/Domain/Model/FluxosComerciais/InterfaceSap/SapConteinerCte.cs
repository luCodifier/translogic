namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Interface SAP Conteiner CTE
    /// </summary>
    public class SapConteinerCte : EntidadeBase<int>
    {
        /// <summary>
        ///  Sap Desp Cte
        /// </summary>
        public virtual SapDespCte SapDespCte { get; set; }

        /// <summary>
        ///  C�digo do conteiner
        /// </summary>
        public virtual string CodConteiner { get; set; }
        
        /// <summary>
        ///  C�digo do agrupamento do conteiner com ctes no mesmo carregamento
        /// </summary>
        public virtual long Agrupamento { get; set; }
    }
}

