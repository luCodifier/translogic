namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de envio para o sap dos Ctes anulados
	/// </summary>
	public class SapCteAnulado : EntidadeBase<int>
	{
		/// <summary>
		///  Sap Desp Cte
		/// </summary>
		public virtual SapDespCte SapDespCte { get; set; }

		/// <summary>
		/// Chave do CTE
		/// </summary>
		public virtual string ChaveCte { get; set; }
	}
}