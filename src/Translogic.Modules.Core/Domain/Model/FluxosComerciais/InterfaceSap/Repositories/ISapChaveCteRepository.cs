namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Reposit�rio de SapChaveCte
    /// </summary>
    public interface ISapChaveCteRepository : IRepository<SapChaveCte, int>
    {
        /// <summary>
        /// Obt�m pela chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave de Cte para ser pesquisada</param>
        /// <returns>Retorna a inst�ncia do objeto</returns>
        SapChaveCte ObterPorChaveCte(string chaveCte);

        /// <summary>
        /// Obt�m pela chave do cte
        /// </summary>
        /// <param name="chaveCte">Chave de Cte para ser pesquisada</param>
        /// <returns>Retorna a inst�ncia do objeto</returns>
        IList<SapChaveCte> ObterPorChavesCte(string chaveCte);
    }
}