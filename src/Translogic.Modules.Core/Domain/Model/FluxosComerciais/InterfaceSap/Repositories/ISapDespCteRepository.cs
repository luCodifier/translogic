namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Reposit�rio de SapDespCte
	/// </summary>
	public interface ISapDespCteRepository : IRepository<SapDespCte, int>
	{
        /// <summary>
        ///    Insere na SapDespCte  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade da SapDespCte</param>
        /// <returns>Retorna a entidade inserida </returns>
	    SapDespCte InserirSemSessao(SapDespCte entity);
	}
}