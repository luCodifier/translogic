namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Repositório de SapCteAnulado
	/// </summary>
	public interface ISapCteAnuladoRepository : IRepository<SapCteAnulado, int>
	{
	}
}