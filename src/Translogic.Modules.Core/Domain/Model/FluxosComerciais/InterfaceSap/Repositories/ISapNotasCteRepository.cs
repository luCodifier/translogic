namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Repositório de SapNotasCte
    /// </summary>
    public interface ISapNotasCteRepository : IRepository<SapNotasCte, int>
    {
    }
}