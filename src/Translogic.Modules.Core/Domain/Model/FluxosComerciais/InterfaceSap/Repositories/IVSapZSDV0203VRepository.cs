namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Reposit�rio de VSapZSDV0203V
    /// </summary>
    public interface IVSapZSDV0203VRepository : IRepository<VSapZSDV0203V, int>
    {
        /// <summary>
        /// Obt�m VSapZSDV0203V pela Serie, Despacho de 5 digitos e o Tipo
        /// </summary>
        /// <param name="serie"> Numero da Serie </param>
        /// <param name="despacho"> N�mero do Despacho de 5 digitos </param>
        /// <param name="tipo"> tipo da opera��o </param>
        /// <returns> Objeto VSapZSDV0203V </returns>
        VSapZSDV0203V ObterPorSerieDespachoTipo(int serie, int despacho, string tipo);
    }
}