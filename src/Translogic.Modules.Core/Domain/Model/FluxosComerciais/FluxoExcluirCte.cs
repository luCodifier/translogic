﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe que representa um fluxo do CT-e que deve ser exluido 
	/// </summary>
	/// <remarks> 
	/// O ID é a parte inteira do código do fluxo por exemplo:
	/// Se o código do fluxo é GN41036 estará gravado na tabela o id 41036</remarks>
	public class FluxoExcluirCte : EntidadeBase<string>
	{
		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Motivo da exclusão
		/// </summary>
		public virtual string MotivoExclusao { get; set; }
	}
}