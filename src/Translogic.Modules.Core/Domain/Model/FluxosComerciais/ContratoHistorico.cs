namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;
	using Pedidos.Despachos;

	/// <summary>
	/// Representa o hist�rico de contratos Contrato de um pedido
	/// </summary>
	public class ContratoHistorico : EntidadeBase<int?>, IContrato
	{
		/// <summary>
		/// Contrato - <see cref="Contrato"/>
		/// </summary>
		public virtual Contrato Contrato { get; set; }

		/// <summary>
		/// Condi��o de frete - <see cref="CondicaoFrete"/>
		/// </summary>
		public virtual CondicaoFrete CondicaoFrete { get; set; }

		/// <summary>
		/// Empresa do contrato
		/// </summary>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Empresa de cobran�a do contrato
		/// </summary>
		public virtual EmpresaCliente EmpresaCobranca { get; set; }

		/// <summary>
		/// Empresa destinat�ria
		/// </summary>
		public virtual EmpresaCliente EmpresaDestinataria { get; set; }

		/// <summary>
		/// Empresa de faturamento do contrato
		/// </summary>
		public virtual EmpresaCliente EmpresaFaturamento { get; set; }

		/// <summary>
		/// Empresa pagadora
		/// </summary>
		public virtual IEmpresa EmpresaPagadora { get; set; }

		/// <summary>
		/// Empresa remetente
		/// </summary>
		public virtual EmpresaCliente EmpresaRemetente { get; set; }

		/// <summary>
		/// Ferrovia que fatura o carregamento
		/// </summary>
		/// <remarks>
		/// Foi feito o Enumeration, pois os valores que est�o aqui vem do SAP e s�o diferentes da sigla que est� na tabela "Empresa"
		/// </remarks>
		public virtual FerroviaFaturamentoEnum? FerroviaFaturamento { get; set; }

		/// <summary>
		/// Indica se o fluxo � de container
		/// </summary>
		public virtual bool? IndFluxoContainer { get; set; }

		/// <summary>
		/// Data de In�cio de Vig�ncia
		/// </summary>
		public virtual DateTime InicioVigencia { get; set; }

		/// <summary>
		/// N�mero do contrato
		/// </summary>
		public virtual string NumeroContrato { get; set; }

		/// <summary>
		/// Prazo do contrato - em horas
		/// </summary>
		public virtual int? Prazo { get; set; }

		/// <summary>
		/// Prazo de cancelamento - em horas
		/// </summary>
		public virtual int? PrazoCancelamento { get; set; }

		/// <summary>
		/// Tempo de tr�nsito - em horas
		/// </summary>
		public virtual int? TempoTransito { get; set; }

		/// <summary>
		/// Data de T�rmino de Vig�ncia
		/// </summary>
		public virtual DateTime? TerminoVigencia { get; set; }

		/// <summary>
		/// Volume contratado
		/// </summary>
		public virtual double? VolumeContratado { get; set; }

		/// <summary>
		/// C�digo da unidade de medida
		/// </summary>
		public virtual string CodigoUnidadeMedida { get; set; }

		/// <summary>
		/// Al�quota do ICMS
		/// </summary>
		public virtual double? ValorAliquotaIcmsProduto { get; set; }

		/// <summary>
		/// Base de c�lculo
		/// </summary>
		public virtual double? ValorBaseCalculo { get; set; }

		/// <summary>
		/// Sigla do CFOP
		/// </summary>
		public virtual string Cfop { get; set; }

		/// <summary>
		/// Descri��o do CFOP
		/// </summary>
		public virtual string DescricaoCfop { get; set; }

		/// <summary>
		/// Valor da base de calculo de substituicao do produto
		/// </summary>
		public virtual double? ValorBaseCalculoSubstituicaoProduto { get; set; }

		/// <summary>
		/// CFOP do produto
		/// </summary>
		public virtual string CfopProduto { get; set; }

		/// <summary>
		/// Aliquota do icms de substitui��o do produto
		/// </summary>
		public virtual double? AliquotaIcmsSubstProduto { get; set; }

		/// <summary>
		/// Tipo de tributa��o
		/// </summary>
		public virtual int? TipoTributacao { get; set; }

		/// <summary>
		/// Desconto por peso
		/// </summary>
		public virtual double? DescontoPorPeso { get; set; }

		/// <summary>
		/// Data e hora de cadastro / atualiza��o do registro
		/// </summary>
		public virtual DateTime DataHoraAtualizacao { get; set; }

		/// <summary>
		/// Contribui��o Sujeito Tributa��o
		/// </summary>
		public virtual string Cst { get; set; }

		/// <summary>
		/// Percentual de aliquota do Icms
		/// </summary>
		public virtual double? PercentualAliquotaIcms { get; set; }

		/// <summary>
		/// Percentual de desconto
		/// </summary>
		public virtual double? PercentualDesconto { get; set; }

		/// <summary>
		/// Empresa remetente fiscal
		/// </summary>
		public virtual EmpresaCliente EmpresaRemetenteFiscal { get; set; }

		/// <summary>
		/// Empresa destinataria fiscal
		/// </summary>
		public virtual EmpresaCliente EmpresaDestinatariaFiscal { get; set; }

		/// <summary>
		/// Percentual de Seguro
		/// </summary>
		public virtual double? PercentualSeguro { get; set; }

		/// <summary>
		/// Descri��o da Tributa��o
		/// </summary>
		public virtual string DescricaoTributacao { get; set; }

		/// <summary>
		/// C�digo da unidade monet�ria
		/// </summary>
		public virtual string UnidadeMonetaria { get; set; }

		/// <summary>
		/// Mensagem fiscal
		/// </summary>
		public virtual string MensagemFiscal { get; set; }
        
	    /// <summary>
	    /// Mensagem Fiscal
	    /// </summary>
	    public virtual string MensagemFiscalExp { get; set; }

		/// <summary>
		/// Codigo do fluxo comercial 
		/// </summary>
        public virtual string CodigoFluxoComercial { get; set; }

        /// <summary>
        /// Percentual da aliquota de cofins
        /// </summary>
        public virtual double? PercentualCofins { get; set; }

        /// <summary>
        /// Percentual da aliquota de Pis
        /// </summary>
        public virtual double? PercentualPis { get; set; }

        /// <summary>
        /// Percentual de Cprb
        /// </summary>
        public virtual double? PercentualCprb { get; set; }

        /// <summary>
        /// Al�quota do PIS
        /// </summary>
        public virtual double? ValorAliquotaPisProduto { get; set; }

        /// <summary>
        /// Al�quota do COFINS
        /// </summary>
        public virtual double? ValorAliquotaCofinsProduto { get; set; }

	    /// <summary>
	    /// Cfop Anulacao
	    /// </summary>
	    public virtual string CfopAnulacao { get; set; }

	    /// <summary>
	    /// Desc Cfop Anulacao
	    /// </summary>
	    public virtual string DescCfopAnulacao { get; set; }
	}
}