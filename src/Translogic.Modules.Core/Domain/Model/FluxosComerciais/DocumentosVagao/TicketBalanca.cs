﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
    /// Possui os dados para geração do ticket da balanca
    /// </summary>
    public class TicketBalanca : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Empresa de destino
        /// </summary>
        public virtual IEmpresa Destino { get; set; }

        /// <summary>
        /// Empresa de origem 
        /// </summary>
        public virtual IEmpresa Origem { get; set; }

        /// <summary>
        /// Data de saída do caminhão
        /// </summary>
        public virtual DateTime DataPesagem { get; set; }

        /// <summary>
        /// Vistor da balança
        /// </summary>
        public virtual string Responsavel { get; set; }

        /// <summary>
        /// Vistor da balança
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Número do Ticket
        /// </summary>
        public virtual string NumeroTicket { get; set; }

        /// <summary>
        /// Número da Balança
        /// </summary>
        public virtual string NumeroBalanca { get; set; }
        #endregion
    }
}