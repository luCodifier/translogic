﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Controle de recebimentos dos tickets de balança
    /// </summary>
    public class WsTicketControleRecebimento : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Indicador de erro
        /// </summary>
        public virtual bool IndErro { get; set; }

        /// <summary>
        /// Protocolo de recebimento
        /// </summary>
        public virtual string Protocolo { get; set; }

        /// <summary>
        /// Mensagem de retorno
        /// </summary>
        public virtual string LogRetorno { get; set; }

        /// <summary>
        /// Mensagem serializada recebida
        /// </summary>
        public virtual string MensagemRecebida { get; set; }

        /// <summary>
        /// Data que a mensagem foi recebida
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }
        
        /// <summary>
        /// Ticket Recebido
        /// </summary>
        public virtual TicketBalanca Ticket { get; set; }

        #endregion
    }
}