﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Controle de recebimentos dos tickets de balança
    /// </summary>
    public class WsTicketLogRecebimento : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Mensagem de retorno
        /// </summary>
        public virtual string LogMensagem { get; set; }

        /// <summary>
        /// Mensagem de retorno
        /// </summary>
        public virtual string LogMensagemRetorno { get; set; }

        /// <summary>
        /// Data que a mensagem foi recebida
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        #endregion
    }
}
