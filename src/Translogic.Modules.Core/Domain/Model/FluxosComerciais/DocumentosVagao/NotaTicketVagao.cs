﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Notas do Caminhão do ticket da balança
    /// </summary>
    public class NotaTicketVagao : EntidadeBase<int>
    {
        /// <summary>
        /// Caminhão da nfe
        /// </summary>
        public virtual VagaoTicket Vagao { get; set; }

        /// <summary>
        /// Chave da nota fiscal
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Peso rateio da nota fiscal
        /// </summary>
        public virtual double PesoRateio { get; set; }

        /// <summary>
        /// Cliente que enviou o ticket
        /// </summary>
        public virtual string ClienteTicket { get; set; }

        /// <summary>
        /// Peso rateio formatado
        /// </summary>
        public virtual string PesoRateioFormatado
        {
            get
            {
                return String.Format("{0:0.000}", PesoRateio / 1000);
            }
        }
    }
}