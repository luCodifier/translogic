﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;
using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;

    /// <summary>
    ///  Interface de repositório de Tickets da balanca
    /// </summary>
    public interface ITicketBalancaRepository : IRepository<TicketBalanca, int?>
    {
        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="idComposicao">id da composição</param>
        /// <param name="vagoes">vagoes a serem filtrados </param>
        /// <returns>Lista de tickets</returns>
        IList<decimal> ObterNotasTicketsPorVagao(int idComposicao, IList<int> vagoes);

        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="itensDespacho">itens de despacho a serem filtrados </param>
        /// <returns>Lista de tickets</returns>
        IList<NotaTicketVagao> ObterNotasTicketItensDespacho(IList<int> itensDespacho);

        /// <summary>
        /// Obtém os tickets (e o seu respectivo cliente) para os vagões
        /// </summary>
        /// <param name="itensDespacho">itens de despacho a serem filtrados </param>
        /// <param name="historico">informa se a pesquisa esta sendo feita pela tela de histórico devido o Fat2.0</param>
        /// <returns>Lista de tickets</returns>
        IList<NotaTicketVagaoClienteDto> ObterNotasTicketVagaoClienteItensDespacho(IList<int> itensDespacho, bool historico);

        /// <summary>
        /// Obtém os tickets para os vagões
        /// </summary>
        /// <param name="itensDespacho">itens de despacho a serem filtrados </param>
        /// <param name="historico">informa se a pesquisa esta sendo feita pela tela de histórico devido o Fat2.0</param>
        /// <returns>Lista de tickets</returns>
        IList<ItemDespachoNotaTicketVagaoDto> ObterNotasTicketItensDespachoDto(IList<int> itensDespacho, bool historico);
    }
}