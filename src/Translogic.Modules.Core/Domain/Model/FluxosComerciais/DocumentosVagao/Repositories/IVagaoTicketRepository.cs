﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    ///  Interface de repositório de Caminhao dos Tickets da balanca
    /// </summary>
    public interface IVagaoTicketRepository : IRepository<VagaoTicket, int>
    {
        VagaoTicketDto GetTaraByIdVagao(string codigoVagao);
    }
}