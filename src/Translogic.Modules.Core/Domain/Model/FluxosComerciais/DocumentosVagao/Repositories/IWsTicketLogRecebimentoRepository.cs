﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    ///  Interface de repositório  de log de recebimentos dos tickets de balança
    /// </summary>
    public interface IWsTicketLogRecebimentoRepository : IRepository<WsTicketLogRecebimento, int>
    {
    }
}