﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    ///  Interface de repositório de Notas do Caminhão dos Tickets da balanca
    /// </summary>
    public interface INotaTicketVagaoRepository : IRepository<NotaTicketVagao, int>
    {
        /// <summary>
        /// Retorna as notas do ticket vagao
        /// </summary>
        /// <param name="listaIds">Lista de Ids para filtro</param>
        /// <returns>Objeto NotaTicketVagao</returns>
        List<NotaTicketVagao> ObterTodos(IList<decimal> listaIds);
    }
}