﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    ///  Interface de repositório  de Controle de recebimentos dos tickets de balança
    /// </summary>
    public interface IWsTicketControleRecebimentoRepository : IRepository<WsTicketControleRecebimento, int>
    {
    }
}