﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Possui os dados do caminhão para geração do ticket da balança
    /// </summary>
    public class VagaoTicket : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual double PesoBruto { get; set; }

        /// <summary>
        /// Série do Vagão
        /// </summary>
        public virtual string SerieVagao { get; set; }

        /// <summary>
        /// Código do Vagão
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual double PesoTara { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual double PesoLiquido { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public virtual TicketBalanca Ticket { get; set; }

        /// <summary>
        /// Data de cadastro
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Ajuste Trigger
        /// </summary>
        public virtual bool AjusteTrg { get; set; }

        /// <summary>
        /// Peso bruto formatado
        /// </summary>
        public virtual string PesoBrutoFormatado
        {
            get
            {
                // Está dividindo por 100 pois a tabela do banco de dados possui uma trigger que já divide por 10
                // Caso der problemas, verificar a trigger TRG_AJUSTA_TICKET
                ////if ((PesoLiquido + PesoTara) <= PesoBruto)
                if (AjusteTrg) // mesmo if da trigger
                    return String.Format("{0:0.000}", PesoBruto/100);
                else
                    return String.Format("{0:0.000}", PesoBruto/1000);
            }
        }

        /// <summary>
        /// Peso liquido formatado
        /// </summary>
        public virtual string PesoLiquidoFormatado
        {
            get
            {
                // Está dividindo por 100 pois a tabela do banco de dados possui uma trigger que já divide por 10
                // Caso der problemas, verificar a trigger TRG_AJUSTA_TICKET
                ////if ((PesoBruto - PesoTara) <= PesoLiquido)
                if (AjusteTrg) // mesmo if da trigger
                    return String.Format("{0:0.000}", PesoLiquido/100);
                else
                    return String.Format("{0:0.000}", PesoLiquido/1000);
            }
        }

        /// <summary>
        /// Peso tara formatado
        /// </summary>
        public virtual string PesoTaraFormatado
        {
            get
            {
                // Está dividindo por 100 pois a tabela do banco de dados possui uma trigger que já divide por 10
                // Caso der problemas, verificar a trigger TRG_AJUSTA_TICKET
                if (AjusteTrg)
                    return String.Format("{0:0.000}", PesoTara / 100);
                else
                    return String.Format("{0:0.000}", PesoTara / 1000);
            }
        }
        #endregion 
    }
}