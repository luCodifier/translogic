﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Model Associa Fluxo
	/// </summary>
    public class ControleCpbr : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Identificador da Malha 
		/// </summary>
		public virtual string Malha { get; set; }

        /// <summary>
        /// Identificador do Estado
        /// </summary>
        public virtual string CnpjFerrovia { get; set; }

        /// <summary>
        /// Valor percentual do tributo
        /// </summary>
        public virtual decimal PercentualTributo { get; set; }

		/// <summary>
		/// Data do inicio da vigencia da regra
		/// </summary>
		public virtual DateTime InicioVigencia { get; set; }

        /// <summary>
        /// Indicador de trava ativa
        /// </summary>
        public virtual string FlagAtivo { get; set; }

		#endregion
	}
}
