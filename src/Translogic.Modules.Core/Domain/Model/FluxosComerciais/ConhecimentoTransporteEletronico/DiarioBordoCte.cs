﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;

    /// <summary>
    /// Classe de diário de bordo de Cte
    /// </summary>
    public class DiarioBordoCte : EntidadeBase<int?>
    {
        /// <summary>
        /// Cte selecionada para o diário de bordo
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Usuário que realizou o cadastro no diário de bordo
        /// </summary>
        public virtual Usuario UsuarioCadastro { get; set; }

        /// <summary>
        /// Data e hora da gravação do diário de bordo
        /// </summary>
        public virtual DateTime DataHora { get; set; }

        /// <summary>
        /// Ação ou mensagem informada pelo usuário
        /// </summary>
        public virtual string Acao { get; set; }
    }
}