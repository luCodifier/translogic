namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe com os c�digos de retorno do CTE - (c�digos de retorno da receita)
	/// </summary>
	public class CteStatusRetorno : EntidadeBase<int?>
	{
		/// <summary>
		/// Descri��o dos c�digos de retorno
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica se o c�digo foi informado pela Receita.
		/// </summary>
		public virtual bool InformadoPelaReceita { get; set; }

		/// <summary>
		/// Indicador se esse c�digo de erro permite reenvio
		/// </summary>
		public virtual bool PermiteReenvio { get; set; }

		/// <summary>
		/// Indicador se remove da fila da interface
		/// </summary>
		public virtual bool RemoveFilaInterface { get; set; }

		/// <summary>
		/// Dados da a a��o a ser tomada
		/// </summary>
		public virtual string AcaoSerTomada { get; set; }
	}
}