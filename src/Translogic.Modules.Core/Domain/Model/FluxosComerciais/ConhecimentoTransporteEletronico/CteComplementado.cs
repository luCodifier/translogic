namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

	/// <summary>
	/// Classe de Cte Complementado
	/// </summary>
	public class CteComplementado : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto da classe Cte
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Diferenca do valor no cte
		/// </summary>
		public virtual double ValorDiferenca { get; set; }

        /// <summary>
        /// Diferenca do peso no cte
        /// </summary>
        public virtual double PesoDiferenca { get; set; }

		/// <summary>
		/// Data e hora do Complemento
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// O usu�rio de complemento do Cte
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Status do Processamento
		/// </summary>
		public virtual bool Processado { get; set; }

		/// <summary>
		/// N�mero do Lote do Processamento
		/// </summary>
		public virtual int NumeroLote { get; set; }

		/// <summary>
		/// Cte Complementar
		/// </summary> 
		public virtual Cte CteComplementar { get; set; }
        
        /// <summary>
        /// Tipo do complemento - (CV - Complemento de valor) / (CI - Complemento de Icms)
        /// </summary> 
        public virtual TipoOperacaoCteEnum TipoComplemento { get; set; }
	}
}