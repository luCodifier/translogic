namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using NHibernate;

	/// <summary>
	/// Inteface do repost�rio da classe Cte recebimento pooling
	/// </summary>
	public interface ICteRecebimentoPoolingRepository : IRepository<CteRecebimentoPooling, int?>
	{
		/// <summary>
		/// Obt�m os itens do pooling para serem processado o retorno
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que est�o no pooling</returns>
		IList<CteRecebimentoPooling> ObterListaRecebimentoPorHost(string hostName);

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		void RemoverPorLista(IList<CteRecebimentoPooling> lista);

		/// <summary>
		/// Limpa a sessao
		/// </summary>
		void ClearSession();
	}
}