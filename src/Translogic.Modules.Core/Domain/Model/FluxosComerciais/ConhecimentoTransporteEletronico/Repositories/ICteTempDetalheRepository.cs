namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório de Detalhe de CTE temp
	/// </summary>
	public interface ICteTempDetalheRepository : IRepository<CteTempDetalhe, string>
	{
	}
}