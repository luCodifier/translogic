namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de vers�o do CTE
	/// </summary>
	public interface ICteVersaoRepository : IRepository<CteVersao, int>
	{
	    /// <summary>
	    /// Vers�o vigente do CTe
	    /// </summary>
	    /// <returns>Objeto CteVersao </returns>
	    CteVersao ObterVigente();
	}
}