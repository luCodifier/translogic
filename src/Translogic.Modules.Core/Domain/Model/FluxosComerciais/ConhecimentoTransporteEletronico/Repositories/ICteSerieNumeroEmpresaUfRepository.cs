namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;
	using Estrutura;

    /// <summary>
	/// Interface do reposit�rio de Serie e Numero do CTe pela empresa e estado
	/// </summary>
	public interface ICteSerieNumeroEmpresaUfRepository : IRepository<CteSerieNumeroEmpresaUf, int>
	{
        /// <summary>
        /// Obt�m por empresa e estado
        /// </summary>
        /// <param name="empresa">Objeto da empresa</param>
        /// <param name="uf">Estado da empresa</param>
        /// <returns>Objeto <see cref="CteSerieNumeroEmpresaUf"/></returns>
        CteSerieNumeroEmpresaUf ObterPorEmpresaUf(IEmpresa empresa, string uf);

		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="CteSerieNumeroEmpresaUf"/></returns>
		CteSerieNumeroEmpresaUf ObterPorUf(string uf);
	}
}