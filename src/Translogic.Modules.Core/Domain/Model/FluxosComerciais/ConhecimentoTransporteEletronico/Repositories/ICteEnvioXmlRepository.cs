namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Dto;

	/// <summary>
	/// Reposit�rio da classe de envio de email
	/// </summary>
	public interface ICteEnvioXmlRepository : IRepository<CteEnvioXml, int?>
	{
		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		IList<CteEnvioXml> ObterPorCte(Cte cte);

		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		IList<CteEnvioXml> ObterTodosPorCte(Cte cte);

		/// <summary>
		/// Obt�m uma lista de envio pelo Cte
		/// </summary>
		/// <param name="idCte">Id Cte a ser procurado</param>
		/// <returns>Retorna uma lista de email para envio</returns>
		IList<ReenvioEmailDto> ObterPorCte(int idCte);

		/// <summary>
		/// Retorna os CteEnvioXml
		/// </summary>
		/// <param name="idCtes">id dos Cte</param>
		/// <returns> Retorna a lista de Email</returns>
		IList<CteEnvioXml> ObterTodosPorCte(int[] idCtes);

		/// <summary>
		/// Remove a lista de envio por Cte
		/// </summary>
		/// <param name="cte">Cte a ser procurado</param>
		void RemoverPorCte(Cte cte);

		/// <summary>
		/// Obt�m por cte e por email
		/// </summary>
		/// <param name="cte">Cte de refer�ncia</param>
		/// <param name="email">Email para ser procurado</param>
		/// <returns>Retorna a instancia do objeto</returns>
		CteEnvioXml ObterPorCte(Cte cte, string email);
	}
}