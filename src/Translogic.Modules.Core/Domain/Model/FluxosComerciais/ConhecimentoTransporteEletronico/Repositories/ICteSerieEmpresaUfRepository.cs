namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface Reposit�rio da s�rie empresa UF
	/// </summary>
	public interface ICteSerieEmpresaUfRepository : IRepository<CteSerieEmpresaUf, int>
	{
	}
}