namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Text;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Inteface do repost�rio da classe CteRunnerLog
	/// </summary>
	public interface ICteRunnerLogRepository : IRepository<CteRunnerLog, int?>
	{
		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(string hostName, string nomeServico, string mensagem = null);

		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="builder">String builder do log</param>
		void InserirLogInfo(string hostName, string nomeServico, StringBuilder builder);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		void InserirLogErro(string hostName, string nomeServico, string mensagem, Exception ex = null);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do servi�o</param>
		/// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
		/// <param name="builder">String builderdo log</param>
		void InserirLogErro(string hostName, string nomeServico, StringBuilder builder);
	}
}