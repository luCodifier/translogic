﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;

    public interface ICteOrigemRepository : IRepository<CteOrigem, int?>
    {
        /// <summary>
        /// Obtém as informações dos CTES de origem a partir do CTE informado.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista de CTES de origem</returns>
        IList<CteOrigem> ObterPorCte(Cte cte);
    }
}