namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Reposit�rio do agrupamento do Cte
	/// </summary>
	public interface ICteAgrupamentoRepository : IRepository<CteAgrupamento, int?>
	{
        /// <summary>
        /// Obt�m CTE Raiz pelo Filho
        /// </summary>
        /// <param name="cteFilho">Cte filho do agrupamento</param>
        /// <returns>Cte Agrupamento Raiz</returns>
	    CteAgrupamento ObterAgrupamentoRaizPorCteFilho(Cte cteFilho);

        /// <summary>
        /// Obt�m CTE Raiz pelo Filho
        /// </summary>
        /// <param name="cteFilho">Cte filho do agrupamento</param>
        /// <returns>Cte raiz no Agrupamento</returns>
	    Cte ObterCteRaizPorCteFilho(Cte cteFilho);

		/// <summary>
		/// Obt�m lista do agrupamento que o CTE filho faz parte
		/// </summary>
		/// <param name="cteFilho">Cte filho do agrupamento</param>
		/// <returns>Retorna a lista de agrupamento que o filho faz parte</returns>
		IList<CteAgrupamento> ObterAgrupamentoPorCteFilho(Cte cteFilho);
	}
}