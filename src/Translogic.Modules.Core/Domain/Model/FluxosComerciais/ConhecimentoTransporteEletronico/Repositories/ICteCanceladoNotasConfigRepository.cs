namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    ///     Inteface do repostório da classe Cte Cancelado Notas Config
    /// </summary>
    public interface ICteCanceladoNotasConfigRepository : IRepository<CteCanceladoNotasConfig, int?>
    {
    }
}