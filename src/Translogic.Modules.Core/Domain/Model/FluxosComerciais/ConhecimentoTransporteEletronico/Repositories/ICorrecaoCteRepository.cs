﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do repositório da correção de Cte
    /// </summary>
    public interface ICorrecaoCteRepository : IRepository<CorrecaoCte, int?>
    {
        /// <summary>
        /// Obtém as informações de correção para o Cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Informações da Carta de Correção</returns>
        CorrecaoCte ObterPorCte(Cte cte);
    }
}