﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao;
    using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Outputs;

    /// <summary>
    /// Serviço de informações dos ctes e notas.
    /// </summary>
    public interface ICteConclusaoExportacaoRepository
    {
        /// <summary>
        /// Retorna informacoes dos Ctes
        /// </summary>
        /// <param name="request">Objeto com os dados de entrada para a busca</param>
        /// <returns>Retorna objeto com os dados das ctes pesquisadas</returns>
        List<ElectronicBillofLadingExportDtoData> InformationCteConclusaoExportacao(EletronicBillofLadingExportCompletedRailRequest request);
    }
}