using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;
using System.Collections.Generic;

	/// <summary>
	/// Interface do repositório de envio de email histórico
	/// </summary>
	public interface ICteEnvioEmailHistoricoRepository : IRepository<CteEnvioEmailHistorico, int?>
	{
	    IList<HistoricoEnvioEmailDto> ObterPor(IList<int> idCtes);
	}
}