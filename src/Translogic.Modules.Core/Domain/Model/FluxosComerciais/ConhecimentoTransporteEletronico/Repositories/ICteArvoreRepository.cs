namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio do Cte Arvore
	/// </summary>
	public interface ICteArvoreRepository : IRepository<CteArvore, int?>
	{
		/// <summary>
		/// Obt�m lista da arvore pela raiz
		/// </summary>
		/// <param name="cteRaiz">Cte raiz da arvore</param>
		/// <returns>Lista dos elementos da arvore</returns>
		IList<CteArvore> ObterPorCteRaiz(Cte cteRaiz);

		/// <summary> 
		/// Obt�m a arvore de Cte pelo cte filho
		/// </summary>
		/// <param name="cteFilho">Cte filho que est� contido na arvore</param>
		/// <returns>Retorna a lista com os elementos da arvore</returns>
		IList<CteArvore> ObterArvorePorCteFilho(Cte cteFilho);

		/// <summary>
		/// Obt�m o cte Folha da arvore
		/// </summary>
		/// <param name="cteFilho">Cte filho que est� contido na arvore</param>
		/// <returns>Retorna o Cte Folha da arvore</returns>
		CteArvore ObterCteFolhaDaArvore(Cte cteFilho);

		/// <summary>
		/// Obt�m o cte arvore pelo cte filho (obt�m o n� da arvore)
		/// </summary>
		/// <param name="cteFilho">Cte filho do n� da arvore</param>
		/// <returns>Retorna o n� do cte arvore</returns>
		CteArvore ObterCteArvorePorCteFilho(Cte cteFilho);
	}
}