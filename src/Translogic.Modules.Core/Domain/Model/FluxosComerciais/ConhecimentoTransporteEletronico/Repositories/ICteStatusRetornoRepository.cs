namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;
    using System.Collections.Generic;

	/// <summary>
	/// Interface do reposit�rio da classe CteStatusRetorno
	/// </summary>
	public interface ICteStatusRetornoRepository : IRepository<CteStatusRetorno, int?>
	{
        /// <summary>
        /// Obt�m uma lista de Erros da Status Retorno
        /// </summary>
        /// <param name="statusRetornoId">Lista de Ids da CteStatusRetorno para se obter os status detalhados</param>
        /// <returns>Lista CteStatusRetorno</returns>
	    IList<CteStatusRetorno> ObterCteStatusRetornoPorId(string statusRetornoId);
	}
}