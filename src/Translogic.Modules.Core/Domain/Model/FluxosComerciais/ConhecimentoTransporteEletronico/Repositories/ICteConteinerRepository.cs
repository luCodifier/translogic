﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;

    public interface ICteConteinerRepository : IRepository<CteConteiner, int>
    {
        /// <summary>
        /// Obtém a lista com os detalhes do Cte
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <param name="idConteiner">Cte a ser pesquisado os detalhes</param>
        /// <returns>Object CteConteiner</returns>
        CteConteiner ObterPorCteConteiner(int idCte, int idConteiner);

        /// <summary>
        /// Obtém a lista com os agrupamentos de carga para este CTE
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        IList<CteConteiner> ObterPorCteAgrupCarga(int idCte);

        /// <summary>
        /// Obtém a lista com os agrupamentos
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        IList<CteConteiner> ObterPorCteAgrupamento(int idCte);

        /// <summary>
        /// Obtém a lista com os detalhes do Cte em stateless
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <param name="idConteiner">Cte a ser pesquisado os detalhes</param>
        /// <returns>Object CteConteiner</returns>
        CteConteiner ObterPorCteConteinerSemEstado(int idCte, int idConteiner);

        /// <summary>
        /// Obtém a lista com os detalhes do Cte
        /// </summary>
        /// <param name="idCte">Cte a ser pesquisado os detalhes</param>
        /// <returns>List CteConteiner</returns>
        IList<CteConteiner> ObterPorCte(int idCte);
        
        /// <summary>
        /// Obtém a lista com os detalhes do Cte
        /// </summary>
        /// <param name="chaveCte">Chave do Cte a ter o indicativo de carga cancelada atualizado na tabela CTE_CONTEINER</param>
        void CancelarCarregPorChaveCte(string chaveCte);
    }
}