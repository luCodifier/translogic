﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do repositório da correção de Cte
    /// </summary>
    public interface ICorrecaoConteinerCteRepository : IRepository<CorrecaoConteinerCte, int?>
    {
        /// <summary>
        /// Obtém as informações de correção para o Cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Informações da Carta de Correção</returns>
        IList<CorrecaoConteinerCte> ObterPorCte(Cte cte);
    }
}
