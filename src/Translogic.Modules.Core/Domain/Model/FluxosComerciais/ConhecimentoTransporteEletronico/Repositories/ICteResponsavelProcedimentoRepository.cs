﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="CteResponsavelProcedimento"/>
    /// </summary>
    public interface ICteResponsavelProcedimentoRepository : IRepository<CteResponsavelProcedimento, int?>
    {
        /// <summary>
        /// Obtém os procedimentos e responsáveis
        /// </summary>
        /// <param name="cteStatusRetornoId">Id CteStatusRetorno para se obter os procedimentos</param>
        /// <returns>Lista de procedimentos com responsáveis para a Cte</returns>
        IList<CteResponsavelProcedimento> ObterPorCteStatusRetornoId(int cteStatusRetornoId);
    }
}
