﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using ALL.Core.AcessoDados;
    using Services.Ctes;

    /// <summary>
    ///  Inteface do repostório da classe Cte envio seguradora histórico
    /// </summary>
    public interface ICteEnvioSeguradoraHistRepository : IRepository<CteEnvioSeguradoraHist, int>
    {
    }
}
