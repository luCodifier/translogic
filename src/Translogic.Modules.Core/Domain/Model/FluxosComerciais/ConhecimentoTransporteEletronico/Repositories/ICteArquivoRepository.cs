namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;
using System.Collections.Generic;

	/// <summary>
	/// Interface do repositório do CteArquivo
	/// </summary>
	public interface ICteArquivoRepository : IRepository<CteArquivo, int?>
	{
	    string ObterArqPorId(int? id);

	    byte[] ObterArqPorId(string chaveCte);

		/// <summary>
		/// Insere um Cte arquivo
		/// </summary>
		/// <param name="cte">Cte de referência</param>
		/// <param name="nomeArquivoPdf">Nome do arquivo pdf</param>
		/// <param name="nomeArquivoXml">Nome do arquivo xml</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		CteArquivo Inserir(Cte cte, string nomeArquivoPdf, string nomeArquivoXml);

		/// <summary>
		/// Insere um Cte arquivo (PDF)
		/// </summary>
		/// <param name="cte">Cte de referência</param>
		/// <param name="nomeArquivo">Nome do arquivo pdf</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		CteArquivo InserirOuAtualizarPdf(Cte cte, string nomeArquivo);

		/// <summary>
		/// Insere um Cte arquivo (XML)
		/// </summary>
		/// <param name="cte">Cte de referência</param>
		/// <param name="nomeArquivo">Nome do arquivo Xml</param>
		/// <returns>Retorna o objeto CteArquivo</returns>
		CteArquivo InserirOuAtualizarXml(Cte cte, string nomeArquivo);

	    /// <summary>
	    /// Atualiza o cte com a carta de correção
	    /// </summary>
	    /// <param name="cte">Cte de referência</param>
	    /// <param name="nomeArquivo">Nome do arquivo pdf</param>
	    /// <returns>Retorna o objeto CteArquivo</returns>
	    CteArquivo AtualizarPdfCartaDeCorrecao(Cte cte, string nomeArquivo);

	    /// <summary>
	    /// Atualiza o arquivo da carta de correção
	    /// </summary>
	    /// <param name="cte">Cte de referência</param>
	    /// <param name="nomeArquivo">Nome do arquivo Xml</param>
	    /// <returns>Retorna o objeto CteArquivo</returns>
	    CteArquivo AtualizarXmlCartaDeCorrecao(Cte cte, string nomeArquivo);

        /// <summary>
        /// Obtem uma lista de arquivos cte
        /// </summary>
        /// <param name="cteId">Codigos dos Ctes a serem localizados</param>
        /// <returns>Retorna os arquivos cte</returns>
        IList<CteArquivo> ObterPorIds(List<int> idsCte);


        /// <summary>
        /// Obtem uma lista de arquivos cte de acordo com o ID de Despacho informado
        /// </summary>
        /// <param name="idDespacho">Id do Despacho dos Ctes a serem localizados</param>
        /// <returns>Retorna os arquivos cte</returns>
        IList<CteArquivo> ObterPorIdDespacho(int idDespacho);

	}
}