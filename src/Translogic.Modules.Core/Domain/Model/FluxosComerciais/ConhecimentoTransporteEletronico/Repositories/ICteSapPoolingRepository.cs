namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Inteface do repost�rio da classe Cte sap pooling
	/// </summary>
	public interface ICteSapPoolingRepository : IRepository<CteSapPooling, int?>
	{
		/// <summary>
		/// Obt�m os itens do pooling para serem enviados para o SAP
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que est�o no pooling</returns>
		IList<CteSapPooling> ObterListaRecebimentoPorHost(string hostName);

		/// <summary>
		/// Limpa a sess�o
		/// </summary>
		void ClearSession();

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		void RemoverPorLista(IList<CteSapPooling> lista);
	}
}