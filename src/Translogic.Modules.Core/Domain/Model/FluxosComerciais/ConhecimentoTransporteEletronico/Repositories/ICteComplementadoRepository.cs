namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da classe Cte Complementado
	/// </summary> 
	public interface ICteComplementadoRepository : IRepository<CteComplementado, int?>
	{
		/// <summary>
		/// Obt�m a lista de cte complementado por cte complementar
		/// </summary>
		/// <param name="cteComplementar">Cte complementar</param>
		/// <returns>Retorna uma lista com os ctes complementados</returns>
		IList<CteComplementado> ObterCteComplementadoPorCteComplementar(Cte cteComplementar);
	}
}