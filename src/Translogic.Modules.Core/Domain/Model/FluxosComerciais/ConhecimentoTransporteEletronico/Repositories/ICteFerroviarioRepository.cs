﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario;
    using WebServices.CteFerroviario.Output;

    /// <summary>
    /// Serviço de informações do cte.
    /// </summary>
    public interface ICteFerroviarioRepository
    {
        /// <summary>
        /// Retorna informações dos ctes
        /// </summary>
        /// <param name="request">Dados do request - periodo de filtro</param>
        /// <returns>Retorna dados do cte referente ao periodo</returns>
        InformationElectronicNotificationResponse InformationElectronicCtes(InformationElectronicNotificationRequest request);

        /// <summary>
        /// Retorna informações dos ctes da malha norte com MDFEs
        /// </summary>
        /// <param name="request">Dados do request - periodo de filtro</param>
        /// <returns>Retorna dados do cte referente ao periodo</returns>
        InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMdfes(InformationElectronicNotificationRequest request);

        /// <summary>
        /// Retorna informações dos ctes da malha norte com MDFEs
        /// Foi adicionado em 09 de Agosto de 2019 os MDF-es para CT-es com multiplo despacho que nao estavam sendo considerados
        /// </summary>
        /// <param name="request">Dados do request - periodo de filtro</param>
        /// <returns>Retorna dados do cte referente ao periodo</returns>
        InformationElectronicNotificationResponse InformationElectronicCtesMalhaNorteComMdfesNew(InformationElectronicNotificationRequest request);

        /// <summary>
        /// Retorna informações dos ctes da malha sul com MDFEs
        /// Foi adicionado em 09 de Agosto de 2019 os MDF-es para CT-es com multiplo despacho que nao estavam sendo considerados
        /// </summary>
        /// <param name="request">Dados do request - periodo de filtro</param>
        /// <returns>Retorna dados do cte referente ao periodo</returns>
        InformationElectronicNotificationResponse InformationElectronicCtesMalhaSulComMdfes(InformationElectronicNotificationRequest request);
    }
}