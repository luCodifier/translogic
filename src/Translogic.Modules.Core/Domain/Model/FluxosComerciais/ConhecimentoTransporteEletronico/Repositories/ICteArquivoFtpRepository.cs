namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Reposit�rio do Arquivo do FTP do Cte
	/// </summary>
	public interface ICteArquivoFtpRepository : IRepository<CteArquivoFtp, string>
	{
	    /// <summary>
	    /// Limpa a sess�o quando houver um erro
	    /// </summary>
	    void ClearSession();

        /// <summary>
        /// Atualiza o registro com o Id do cte quando existir a chave
        /// </summary>
	    void AtualizarQuandoAcharCte();

        /// <summary>
        /// Limpa os arquivos do FTP antigos e que n�o foram encontrados no translogic
        /// </summary>
        /// <param name="diasRetroativos">Quantidade de dias retroativos</param>
	    void LimparAntigos(int diasRetroativos);

        /// <summary>
        /// Obt�m a lista de ctes que est�o no pool de recebimento
        /// </summary>
        /// <param name="hostName">Host do Processamento</param>
        /// <returns>Lista de CT-e</returns>
	    IList<CteArquivoFtp> ObterListaRecebimentoPorHost(string hostName);

        /// <summary>
        /// Remove os arquivos da tabela de FTP
        /// </summary>
        /// <param name="listaProcessamento">Lista de arquivos a serem removidos</param>
	    void RemoverPorLista(IList<CteArquivoFtp> listaProcessamento);
	}
}