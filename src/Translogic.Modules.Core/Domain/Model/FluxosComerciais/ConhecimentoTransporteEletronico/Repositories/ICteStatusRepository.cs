namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using Acesso;
	using ALL.Core.AcessoDados;
    using TriangulusConfig;

	/// <summary>
	/// Interface do reposit�rio da classe CteStatus
	/// </summary>
	public interface ICteStatusRepository : IRepository<CteStatus, int?>
	{
		/// <summary>
		/// Insere o status do Cte
		/// </summary>
		/// <param name="cte">Cte de referencia</param>
		/// <param name="usuario">usuario de inser��o</param>
		/// <param name="cteStatusRetorno">Status de retorno do cte</param>
		void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno);

		/// <summary>
		/// Insere o status do Cte
		/// </summary>
		/// <param name="cte">Cte de referencia</param>
		/// <param name="usuario">usuario de inser��o</param>
		/// <param name="cteStatusRetorno">Status de retorno do cte</param>
		/// <param name="xmlRetorno">Xml de Retorno</param>
		void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno, string xmlRetorno);

        /// <summary>
        /// Insere o status do Cte
        /// </summary>
        /// <param name="cte">Cte de referencia</param>
        /// <param name="usuario">usuario de inser��o</param>
        /// <param name="cteStatusRetorno">Status de retorno do cte</param>
        /// <param name="consultaCte">Objeto de consulta do Cte</param>
        /// <param name="xmlRetorno">Xml de Retorno</param>
        void InserirCteStatus(Cte cte, Usuario usuario, CteStatusRetorno cteStatusRetorno, Vw209ConsultaCte consultaCte, string xmlRetorno);

		/// <summary>
		/// Obt�m os status do Cte pelo Cte
		/// </summary>
		/// <param name="cte">Cte para obter o status</param>
		/// <returns>Retorna uma lista com os status do CTE</returns>
		IList<CteStatus> ObterPorCte(Cte cte);

	    /// <summary>
	    /// Verifica se h� Status do CTE igual a "autorizado"
	    /// </summary>
	    /// <param name="cte">Cte para obter o status</param>
	    /// <returns>Retorna a verifica��o</returns>
	    bool TemStatusAutorizado(Cte cte);

	    /// <summary>
	    /// Obt�m o CteStatus pela situacao do cte
	    /// </summary>
	    /// <param name="cte">Cte para obter o status</param>
	    /// <param name="situacaoCte">Situa��o do Cte</param>
	    /// <returns>Retorna a verifica��o</returns>
	    CteStatus ObterCteStatusPorSituacaoCte(Cte cte, int situacaoCte);
	}
}