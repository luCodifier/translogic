﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;

    /// <summary>
    /// Contrato do Repositório de <see cref="DiarioBordoCte"/>
    /// </summary>
    public interface IDiarioBordoCteRepository : IRepository<DiarioBordoCte, int?>
    {
        /// <summary>
        /// Obtém o diário de bordo da Cte por id de Cte
        /// </summary>
        /// <param name="pagination">Detalhes da paginação</param>
        /// <param name="idCte">Id da Cte para se obter o diário de bordo</param>
        /// <returns>Lista de diário de bordo para a Cte</returns>
        ResultadoPaginado<DiarioBordoCte> ObterDiarioBordoCte(DetalhesPaginacao pagination, int idCte);

        /// <summary>
        /// Grava um novo registro no diário de bordo para a Cte selecionada
        /// </summary>
        /// <param name="acao">Ação ou mensagem informada</param>
        /// <param name="cte">Cte selecionada</param>
        /// <param name="usuario">Usuario que está inserindo o registro no diário</param>
        /// <returns>Resultado com o sucesso ou não da operação</returns>
        bool GravarDiarioBordoCte(string acao, Cte cte, Usuario usuario);
    }
}
