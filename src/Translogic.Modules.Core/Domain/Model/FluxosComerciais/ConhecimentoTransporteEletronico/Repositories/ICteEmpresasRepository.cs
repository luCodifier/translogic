namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Inteface do repost�rio da classe Cte Empresas
    /// </summary> 
    public interface ICteEmpresasRepository : IRepository<CteEmpresas, int?>
    {
        CteEmpresas InserirSemSessao(CteEmpresas entity);

        /// <summary>
        /// Obt�m lista de empresas utilizadas para o cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista dos elementos da arvore</returns>
        CteEmpresas ObterPorCte(Cte cte);

        /// <summary>
        /// Obt�m lista de empresas utilizadas para o cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        /// <returns>Lista dos elementos da arvore</returns>
        IList<CteEmpresas> ObterPorListaCte(decimal[] cte);

        /// <summary>
        /// apaga a relacao de empresas utilizadas para o cte.
        /// </summary>
        /// <param name="cte">Cte a ser pesquisado</param>
        void RemoverPorCte(Cte cte);

        /// <summary>
        /// apaga a relacao de empresas pelo Id
        /// </summary>
        /// <param name="id">Id a ser pesquisado</param>
        void RemoverPorId(int? id);    
    }
}