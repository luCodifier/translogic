namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface do reposit�rio de detalhe de Cte
    /// </summary>
    public interface ICteDetalheRepository : IRepository<CteDetalhe, int?>
	{
		/// <summary>
		/// Obt�m a lista com os detalhes do Cte
		/// </summary>
		/// <param name="cte">Cte a ser pesquisado os detalhes</param>
		/// <returns>Lista com os detalhes</returns>
		IList<CteDetalhe> ObterPorCte(Cte cte);
        
        MalhaEmitenteDto GetMalhaByEstacao(string estacao);
    }
}