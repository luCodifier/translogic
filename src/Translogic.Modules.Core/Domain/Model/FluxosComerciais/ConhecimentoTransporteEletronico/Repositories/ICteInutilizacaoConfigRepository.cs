namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;

    using ALL.Core.AcessoDados;

    /// <summary>
    ///     Inteface do repost�rio da classe Cte Inutiliza��o Config
    /// </summary>
    public interface ICteInutilizacaoConfigRepository : IRepository<CteInutilizacaoConfig, int?>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obtem o cte inutilizado pelo id da lista config
        /// </summary>
        /// <returns>Cte Inutilizado</returns>
        CteInutilizacaoConfig ObterCteinutilizadosBaseConsolidadaConfigPorIdLista(int idListaConfig);

        /// <summary>
        ///     Obtem a lista de ctes inutilizados processados ou n�o processados
        /// </summary>
        /// <returns>Lista de Ctes Inutilizados</returns>
        IList<CteInutilizacaoConfig> ObterTodosProcessados(bool processado);

        #endregion
    }
}