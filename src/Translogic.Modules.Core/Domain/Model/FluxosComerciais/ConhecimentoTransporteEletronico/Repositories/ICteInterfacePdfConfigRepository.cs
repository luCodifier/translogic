namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da interface da importa��o do arquivo pdf
	/// </summary>
	public interface ICteInterfacePdfConfigRepository : IRepository<CteInterfacePdfConfig, int?>
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		IList<CteInterfacePdfConfig> ObterListaInterfacePorHost(string hostName);
	}
}