namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Collections.Generic;
    using Acesso;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Diversos.Cte;
    using Dto;
    using Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Translogic.Modules.Core.Interfaces.DadosFiscais;

    /// <summary>
    /// Inteface do repost�rio da classe Cte
    /// </summary> 
    public interface ICteRepository : IRepository<Cte, int?>
    {
        /// <summary>
        ///    Insere o CTE  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade do CTE</param>
        /// <returns>Retorna a entidade inserida </returns>
        Cte InserirSemSessao(Cte entity);

        /// <summary>
        ///    Atualizar o CTE  sem sess�o
        /// </summary>
        /// <param name="entity">Entidade do CTE</param>
        /// <returns>Retorna a entidade atualizada </returns>
        Cte AtualizarSemSessao(Cte entity);

        /// <summary>
        /// Obtem os ctes pela nfe de anula��o.
        /// </summary>
        /// <param name="nfeAnulacao">Nota de Anual��o</param>
        /// <returns>Retorna Ctes vinculados a nfe de anula��o</returns>
        IList<Cte> ObterPorChaveAnulacao(NotaFiscalAnulacao nfeAnulacao);

        /// <summary>
        /// Obtem os ctes pelo despacho.
        /// </summary>
        /// <param name="despachoId">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e </returns>
        IList<Cte> ObterCtesPorDespacho(int despachoId);

        /// <summary>
        /// Obtem os ctes pelos despachos.
        /// </summary>
        /// <param name="despachosId">Lista de id dos despachos</param>
        /// <returns>retorna a lista de Ct-es </returns>
        IList<Cte> ObterCtesPorDespachos(List<int> despachosId);

        /// <summary>
        /// Obtem os ctes por uma lista de IDs.
        /// </summary>
        /// <param name="ids">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e</returns>
        IList<Cte> ObterPorIds(List<int> ids);

        /// <summary>
        /// Obtem os ctes por uma lista de IDs.
        /// </summary>
        /// <param name="ids">Id do Despacho</param>
        /// <returns>retorna a lista de Ct-e por hql</returns>
        IList<Cte> ObterPorIdsHql(List<int> ids);

        /// <summary>
        /// Verifica se existe o status de retorno informado para o cte
        /// </summary>
        /// <param name="cteId">Identificador do Cte</param>
        /// <param name="codigoStatusRetorno">C�digo de Status do Retorno</param>
        /// <returns>Retorna a lista de Ctes</returns>
        bool ExisteStatusRetornoCte(int cteId, int codigoStatusRetorno);

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <returns>lista de Cte</returns>
        IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int despacho);

        /// <summary>
        /// Retorna todos os Cte Pagos
        /// </summary>
        /// <param name="listaCte">Lista de Cte pago</param>
        /// <returns>lista de Ctes pago</returns>
        IList<CteDto> ObterCtesPago(List<int> listaCte);

        /// <summary>
        /// Retorna a lista de Ctes que n�o constam na tabela Cte_Empresas
        /// </summary> 
        /// <returns>Retorna a lista dos ctes com a flag de Pago preenchida</returns>
        IList<CteDto> RetornaCtesNaoConstamCteEmpresas();

        /// <summary>
        /// Retorna os ctes por IdCte
        /// </summary>
        /// <param name="ids">id dos cte</param>
        /// <returns>lista de Cte</returns>
        IList<Cte> ObterTodosPorId(int[] ids);

        /// <summary>
        /// Chama a package para verificar a gera��o de cte.
        /// </summary>
        /// <param name="fluxo"> C�digo do fluxo</param>
        /// <returns>Retorna se deve gerar cte.</returns>
        bool VerificarFLuxoGerarCte(string fluxo);

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="codigoEstacaoOrigem">C�digo da Esta��o de Origem</param>
        /// <param name="numeroCte">N�mero do Cte</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="impresso">Status de impress�o Cte</param>
        /// <returns>lista de Cte</returns>
        IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int despacho, string chaveCte, string codigoEstacaoOrigem, int numeroCte, string codFluxo, bool impresso);

        /// <summary>
        /// Retorna todos os Cte para Inutilizacao
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="diasPrazoInutilizacao">Dias para cancelamento</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteInutilizacao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, int diasPrazoInutilizacao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param> 
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteManutencaoPendente(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para anula��o
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="horasCancelamento"> horas de cancelamento</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteAnulacao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string origem, string destino, string numVagao, int horasCancelamento);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteGerarArquivoLote(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string origem, string destino, string[] numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="chaveCte">Chave do cte</param>
        /// <returns>lista de Cte</returns>
        IList<Cte> ObterCtesAgrupamentoConteiner(string chaveCte);

        /// <summary>
        /// Salvar todos os CteEnvioXml
        /// </summary>
        /// <param name="email"> e-mail a ser alterado</param>
        /// <param name="novoEmail"> Novo e-mail a ser alterado</param>
        /// <param name="fluxo"> Fluxo informado</param>
        void SalvarEmailCteReenvioArquivo(string email, string novoEmail, string fluxo);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data Inicial</param>
        /// <param name="dataFinal">Data Final</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="email">Chave do Cte informada</param>
        /// <param name="erro"> Emails com Erro</param>
        /// <param name="serie">Serie do despacho</param>
        /// <param name="numDespacho">Numero despacho</param>
        /// <param name="destino">Destino do despacho</param>
        /// <param name="origem">origem do despacho</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo Uf do despacho</param>
        /// <param name="numeroCte"> Numero do cte</param>
        /// <param name="chaveCte"> Array de chaves do cte</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteReenvioEmail(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string email, bool erro, string serie, int numDespacho, string destino, string origem, string numVagao, string codigoUfDcl, string numeroCte, string[] chaveCte);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteRelatorioErro(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string origem, string destino);

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <returns>Retorna o CTE</returns>
        IList<ImportacaoEmailDto> ObterCteEnvioXml();

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoDcl">Codigo Dcl</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteTakeOrPay(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo UF de Despacho</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteComplemento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl"> Codigo UF de Despacho</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteComplementoIcms(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteImpressao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, int serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao);

        /// <summary>
        /// Retorna todos os Cte para cancelamento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="diasPrazoCancelamento">Dias prazo para cancelamento</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="horasCancelamento">Horas prazo para cancelamento</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteCancelamento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string[] chaveCte, string erro, string origem, string destino, string[] numVagao, int diasPrazoCancelamento, string codigoUfDcl, int horasCancelamento);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="impresso"> Indicador se cte impresso</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="incluirNfe">incluir nfe na listagem</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string[] numVagao, bool? impresso, string codigoUfDcl, bool incluirNfe);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CorrecaoCteDto> ObterListaCteParaCorrecao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao"> Numero do Vagao</param>
        /// <param name="impresso"> Indicador se cte impresso</param>
        /// <param name="codigoUfDcl">Codigo controle do despacho</param>
        /// <param name="incluirNfe">ir� fazer join com nfe</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<CteDto> ObterCteMonitoramento(DetalhesPaginacao pagination, DateTime dataInicial,
                                                               DateTime dataFinal, string serie, int despacho,
                                                               string codFluxo, string chaveCte, string erro,
                                                               string origem, string destino, string numVagao,
                                                               bool? impresso, string codigoUfDcl, bool incluirNfe);

        /// <summary>
        /// Retorna todos os Cte para monitoramento
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<ManutencaoCteDto> ObterCteManutencao(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte para virtual
        /// </summary>
        /// <param name="pagination">Detalhes da Pagina��o</param>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="despacho">N�mero do Despacho</param>
        /// <param name="codFluxo">C�digo do fluxo comercial</param>
        /// <param name="chaveCte">Chave do Cte informada</param>
        /// <param name="erro">Status de erro Cte</param>
        /// <param name="origem"> Origem do Cte</param>
        /// <param name="destino"> Destino do Cte</param>
        /// <param name="numVagao">Numero do Vagao</param>
        /// <param name="codigoUfDcl">Codigo do estado Dcl</param>
        /// <returns>lista de Cte</returns>
        ResultadoPaginado<ManutencaoCteDto> ObterCteVirtual(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string serie, int despacho, string codFluxo, string chaveCte, string erro, string origem, string destino, string numVagao, string codigoUfDcl);

        /// <summary>
        /// Retorna todos os Cte
        /// </summary>
        /// <param name="dataInicial">Data inicial de pesquisa</param>
        /// <param name="dataFinal">Data final de pesquisa</param>
        /// <param name="serie">N�mero da s�rie</param>
        /// <param name="numDespacho">N�mero do Despacho</param>
        /// <param name="codVagao">C�digo do Vag�o</param>
        /// <param name="chave">Chave do CTE</param>
        /// <param name="serieCte">S�rie do CTE</param>
        /// <param name="nroCte">N�mero do CTE</param>
        /// <returns>lista de Cte</returns>
        IList<Cte> Obter(DateTime dataInicial, DateTime dataFinal, int serie, int numDespacho, string codVagao, string chave, int serieCte, int nroCte);

        /// <summary>
        /// Retorna Cte pelo Id
        /// </summary>
        /// <param name="idCte">Id do Cte Selecionado</param>
        /// <returns>Cte Selecionado</returns>
        Cte ObterPorIdCte(int idCte);

        /// <summary>
        /// Retorna Cte pelo Id com fetch para o EDI acessar despacho, fluxo, origem e destino
        /// </summary>
        /// <param name="idCte">Id do Cte selecionado</param>
        /// <returns>Cte Selecionado</returns>
        Cte ObterPorIdEdi(int idCte);

        /// <summary>
        /// Obt�m os CTe's que est�o com status de processamento
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual o ambiente Sefaz</param>
        /// <returns>Lista de CTe's</returns>
        IList<Cte> ObterStatusProcessamentoEnvio(int indAmbienteSefaz);

        /// <summary>
        /// Obt�m a lista de Ctes complementado que est�o no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <returns>Lista de CT-e</returns>
        IList<Cte> ObterListaEnvioPorHost(string hostName);

        /// <summary>
        /// Obt�m a lista de Ctes que est�o no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <returns>Lista de CT-e</returns>
        IList<Cte> ObterListaEnvioComplementarPorHost(string hostName);

        /// <summary>
        /// Obt�m os CTe's que est�o com o status de processamento para recebimento
        /// </summary>
        /// <returns>Lista de Ctes</returns>
        IList<Cte> ObterStatusProcessamentoRecebimento();

        /// <summary>
        /// Chama a package para Gerar o Cte Complementar
        /// </summary>
        /// <param name="usuario"> Usu�rio Logado </param>
        /// <param name="idCteComplementar"> Id do Cte Complementar retornado pela Package </param>
        void GerarCteComplementar(Usuario usuario, out int idCteComplementar);

        /// <summary>
        /// Chama a package para Gerar o Cte Complementar
        /// </summary>
        /// <param name="usuario"> Usu�rio Logado </param>
        /// <param name="idCteAnulacao"> Id do Cte Anulacao retornado pela Package </param>
        void GerarCteAnulacao(Usuario usuario, out int idCteAnulacao);


        /// <summary>
        /// Atualiza o motivo do cancelamento
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        void AtualizarMotivoCancelamento(Cte cte);

        /// <summary>
        /// Atualiza o valor do CTE
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        /// <param name="valorCte">Valor do cte para ser atualizado</param>
        void AtualizarValorCte(Cte cte, double valorCte);

        /// <summary>
        /// Atualiza os Flags de arquivo gerado do CTE de acordo com o tipo de arquivo
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        /// <param name="tipoArquivoEnvioEnum">Tipo de arquivo que ser� atualizado o flag (PDF ou XML)</param>
        void AtualizarArquivoGeradoCte(Cte cte, TipoArquivoEnvioEnum tipoArquivoEnvioEnum);

        /// <summary>
        /// Obt�m cte utilizando por HQL
        /// </summary>
        /// <param name="cte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        Cte ObterCtePorHql(Cte cte);

        /// <summary>
        /// Obtm cte utilizando por HQL
        /// </summary>
        /// <param name="idCte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        Cte ObterCtePorIdHql(int idCte);

        /// <summary>
        /// Obt�m cte Complementar utilizando por HQL
        /// </summary>
        /// <param name="cte">Cte a ser buscado</param>
        /// <returns>Retorna o CTE</returns>
        Cte ObterCteComplementarPorHql(Cte cte);

        /// <summary>
        /// Obt�m o Cte pela chave
        /// </summary>
        /// <param name="chave">Chave do Cte</param>
        /// <returns>Objeto cte</returns>
        Cte ObterPorChave(string chave);

        /// <summary>
        /// Atualiza a situacao do Cte por Stateless
        /// </summary>
        /// <param name="cte">Cte para ser atualizado</param>
        void AtualizarSituacao(Cte cte);

        /// <summary>
        /// Obter o CT-e pelo despacho do Translogic
        /// </summary>
        /// <param name="despachoTranslogic">Despacho do translogic</param>
        /// <param name="idVagao : VG_ID_VG para buscar o CTE correto para aquele vagao">Despacho do translogic</param>
        /// <param name="idCte : esse campo somente tem valor qdo ws_despacho.id_cte � n�o nulo. isto vale para ctes virtuais">Id do Cte</param>
        /// <returns>Retorna a inst�ncia do CT-e</returns>
        Cte ObterPorDespacho(DespachoTranslogic despachoTranslogic, int? idVagao = null, int? idCte = null);

        /// <summary>
        /// Obt�m cte anterior referente ao vag�o
        /// </summary>
        /// <param name="cte">Cte corrente, ser� retornado cte anterior ao mesmo</param>
        /// <returns>Retorna o CTE</returns>
        Cte ObterCteAnteriorVagao(Cte cte);

        /// <summary>
        /// Obter os Refaturamentos
        /// </summary>
        /// <param name="ordemServico"> Ordem de Servi�o</param>
        /// <param name="prefixoTrem"> Prefixo do Trem</param>
        /// <returns>Retorna a inst�ncia do RefaturamentoDto</returns>
        IList<RefaturamentoDto> ObterParaRefaturamento(string ordemServico, string prefixoTrem);

        /// <summary>
        /// Salvar os Refaturamentos
        /// </summary>
        /// <param name="listaVagoes"> Lista de Vagoes</param>
        void SalvarRefaturamento(RefatVagoesDto listaVagoes);

        /// <summary>
        /// Recupera o saldo disponivel para o contrato.
        /// </summary>
        /// <param name="contrato">n�mero do contrato</param>
        /// <returns>Saldo dispon�vel.</returns>
        decimal ObterSaldoContratoDisponivel(string contrato);

        /// <summary>
        ///    Obt�m o somat�rio dos pesos das notas dos ctes contidos no vagao vigente do cte informado pela chave
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        decimal ObterSomaPesoNotasCtesPorVagaoPedidoVigente(string chaveCte);

        /// <summary>
        ///    Obt�m o somat�rio dos pesos das notas dos ctes contidos no vagao vigente e fluxo do cte informado pela chave
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        decimal ObterSomaPesoNotasCtesPorVagaoPedidoVigenteFluxo(string chaveCte);

        /// <summary>
        /// Obt�m ctes cancelados por periodo sigla ferrovia.
        /// </summary>
        /// <param name="dataInicio"> Data inicio. </param>
        /// <param name="dataTermino"> Data termino. </param>
        /// <param name="siglaEstado"> Sigla do estado. </param>
        /// <returns> Lista de ctes cancelados. </returns>
        IList<CteFerroviarioCancelado> ObterCtesCanceladosPorPeriodoSiglaFerrovia(DateTime dataInicio, DateTime dataTermino, string siglaEstado);

        /// <summary>
        /// Obt�m ctes por periodo sigla ferrovia.
        /// </summary>
        /// <param name="dataInicio"> Data inicio. </param>
        /// <param name="dataTermino"> Data termino. </param>
        /// <param name="siglaEstado"> Sigla do Estado. </param>
        /// <returns> Lista de ctes gerados </returns>
        IList<CteFerroviarioDescarregado> ObterCtesDescarregaosPorPeriodoSiglaFerrovia(DateTime dataInicio, DateTime dataTermino, string siglaEstado);

        /// <summary>
        /// Obt�m os ctes da arvore gerados apos o cancelamento
        /// </summary>
        /// <param name="idCte">Id do Cte...</param>
        /// <returns>Lista de novos ctes</returns>
        IList<CteFerroviarioCanceladoNovoCte> ObterCtesGeradosAposCancelamentoPorIdCte(int idCte);

        /// <summary>
        /// Lista as Ctes com erro para mostrar no semaforo
        /// </summary>
        /// <returns>Ctes com erro</returns>
        IList<SemaforoCteDto> ObterCtesErro();

        /// <summary>
        /// Obter os vag�es de acordo com os filtros informados na tela
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="dataInicial">Data inicial do Cte</param>
        /// <param name="dataFinal">Data final do Cte</param>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <param name="fluxo">O fluxo do cte</param>
        /// <param name="siglaUfFerrovia">a UF da ferrovia do Cte</param>
        /// <param name="origem">A Origem do cte</param>
        /// <param name="destino">O Destino do cte</param>
        /// <param name="serieDesp">O n�mero de s�rie do despacho</param>
        /// <param name="numeroDesp">O n�mero do despacho</param>
        /// <param name="chaveCte">O n�mero da chave do cte</param>
        /// <param name="codVagao">O c�digo do vag�o</param>
        /// <param name="tipoCte">Tipo de Cte para serem filtradas</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        ResultadoPaginado<CteDto> ObterComposicaoPorFiltros(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string chaveNfe, string fluxo, string siglaUfFerrovia, string origem, string destino, int serieDesp, int numeroDesp, string[] chaveCte, string codVagao, string tipoCte);

        /// <summary>
        /// Obt�m os vag�es de acordo como o Trem informado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        ResultadoPaginado<CteDto> ObterComposicaoPorTrem(DetalhesPaginacao pagination, int idTrem);

        /// <summary>
        /// Obt�m os vag�es de acordo com o Mdfe selecionado
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="idMdfe">Id da Mdfe selecionada</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        ResultadoPaginado<CteDto> ObterComposicaoPorMdfe(DetalhesPaginacao pagination, int idMdfe);

        /// <summary>
        ///    Obt�m os ctes de acordo com o id do vag�o vigente 
        /// </summary>
        /// <param name="idVagao">Detalhes de pagina��o de resultados</param>
        IList<Cte> ObterCtesPorVagaoPedidoVigente(int? idVagao);

        /// <summary>
        ///    Obt�m os ctes de acordo com o id do vag�o vigente do CTE informado pelo chave.
        /// </summary>
        /// <param name="chaveCte">Chave do CTE</param>
        /// <param name="conteiner">C�digo do conteiner</param>
        IList<CteDto> ObterCtesPorVagaoPedidoVigenteConteiner(string chaveCte, string conteiner);

        /// <summary>
        /// Obt�m os vag�es de acordo com o sem�foro
        /// </summary>
        /// <param name="pagination">Detalhes de pagina��o de resultados</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no sem�foro</param>
        /// <param name="periodoHorasErroSemaforo">O per�odo de tempo selecionado no sem�foro</param>
        /// <returns>Lista de vag�es de acordo com os filtros informados</returns>
        ResultadoPaginado<CteDto> ObterComposicaoPorSemaforo(DetalhesPaginacao pagination, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo);

        /// <summary>
        /// Obt�m os status de uma Cte
        /// </summary>
        /// <param name="idCte">Id da Cte que se deseja obter os status</param>
        /// <returns>Lista de Status da Cte</returns>
        IList<CteStatus> ObterStatusCtePorIdCte(int idCte);

        /// <summary>
        /// Retorna os tipos de CTE que existe no banco de dados
        /// </summary>
        /// <returns>Lista de Tipos de Cte</returns>
        IList<string> ObterTiposCte();

        /// <summary>
        ///    Obt�m os ctes de acordo com os id passadps em uma lista de int
        /// </summary>
        /// <param name="ctes">Id do vag�o</param>
        List<CteDto> ObterCtesPorListaIds(IList<int> ctes);

        /// <summary>
        /// Verifica se uma Cte pode ser inutilizada
        /// </summary>
        /// <param name="idCte">Id da Cte para inutilizar</param>
        /// <returns>Verdadeiro se pode inutilizar, falso se n�o pode</returns>
        bool PodeInutilizar(int idCte);

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="idCte">Id da Cte para verificar</param>
        /// <returns>Verdadeiro se a Cte pode ser cancelada, sen�o falso</returns>
        bool PodeCancelar(int idCte);

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="idVagao">Id do vagao vigente</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        bool ExistemCtesRateioVagaoVigente(int idVagao);

        /// <summary>
        ///    Verifica se existe cte que fecha multiplo despacho no vagao vigente
        /// </summary>
        /// <param name="idVagao">Id do vagao vigente</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        bool ExisteCteFinalizaMultiploDespachoVagaoVigente(int idVagao);

        /// <summary>
        /// Verifica se a Cte pode ser cancelada
        /// </summary>
        /// <param name="chaveCte">chave do cte</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        bool ExistemCtesRateioVagaoVigente(string chaveCte);

        /// <summary>
        ///    Verifica se existe cte que fecha multiplo despacho no vagao vigente
        /// </summary>
        /// <param name="chaveCte">chave do cte</param>
        /// <returns>Verdadeiro se existe, senao falso</returns>
        bool ExisteCteFinalizaMultiploDespachoVagaoVigente(string chaveCte);

        Cte ObterCteAutorizadoByDespacho(int id);

        Cte ObterCtePaiIdCteComplementar(int id);

        /// <summary>
        /// Pega as informa��es do cte Virtual para a apresenta��o em tela e a exporta��o do excel quando o parametro isExportarExcel == true
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="codFluxo"></param>
        /// <param name="chaveCte"></param>
        /// <param name="numVagao"></param>
        /// <param name="isExportarExcel"></param>
        /// <returns></returns>
        ResultadoPaginado<CteVirtuaisSemVinculoMdfeDto> CteVirtuaisSemVinculoMdfeDto(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, string codFluxo, string[] chaveCte, string[] numVagao, bool isExportarExcel);

        IList<CteVirtuaisSemVinculoMdfeDto> CteVirtuaisMdfePai(string[] numDespacho);


        IList<MonitoramentoPlanejamentoDescargaDto> MonitoramentoPlanejamentoDescarga(DateTime dataInicial, DateTime dataFinal, string prefixo, string os, string terminal, string vagao);
    }
}
