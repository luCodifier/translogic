namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da classe de interface de recebimento do config
	/// </summary>
	public interface ICteInterfaceRecebimentoConfigRepository : IRepository<CteInterfaceRecebimentoConfig, int?>
	{
		/// <summary>
		/// Obtem os itens da interface que nao foram processados (que n�o est�o no pooling de recebimento)
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-producao / 2-homologa��o</param>
		/// <returns>Retorna uma lista dos itens que n�o est�o no pooling</returns>
		IList<CteInterfaceRecebimentoConfig> ObterNaoProcessados(int indAmbienteSefaz);

		/// <summary>
		/// Remove os cte da interface que ja foi processados
		/// </summary>
		/// <param name="cte">cte para ser removido da lista</param>
		void RemoverPorCte(Cte cte);
	}
}