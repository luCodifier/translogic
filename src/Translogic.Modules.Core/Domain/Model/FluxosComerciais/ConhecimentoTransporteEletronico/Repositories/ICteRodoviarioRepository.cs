﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using CteRodoviario;
    using CteRodoviario.Outputs;

    /// <summary>
    /// Interface para acessoa aos dados do repositorio de ctes rodoviarios
    /// </summary>
    public interface ICteRodoviarioRepository
    {
        /// <summary>
        /// Metodo que retorna os dados dos ctes rodoviarios para o ws
        /// </summary>
        /// <param name="request">Objeto de input do servico</param>
        /// <returns>Retorna output do servico</returns>
        List<ElectronicBillofLadingDataDto> InformationCtesRodoviarios(InformationElectronicBillofLadingHighwayRequest request);
    }
}