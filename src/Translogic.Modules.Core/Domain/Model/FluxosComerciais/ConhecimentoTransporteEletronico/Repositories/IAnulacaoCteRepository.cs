﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.AcessoDados;

    public interface IAnulacaoCteRepository : IRepository<AnulacaoCte, int?>
    {
        
    }
}