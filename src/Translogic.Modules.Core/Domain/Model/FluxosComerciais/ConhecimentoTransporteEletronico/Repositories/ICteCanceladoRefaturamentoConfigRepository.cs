namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Collections.Generic;

    using ALL.Core.AcessoDados;

    /// <summary>
    ///     Inteface do repost�rio da classe Cte Cancelado Refaturamento
    /// </summary>
    public interface ICteCanceladoRefaturamentoConfigRepository : IRepository<CteCanceladoRefaturamentoConfig, int?>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Obtem os ctes cancelados para refaturamento da base consolidada config
        /// </summary>
        /// <param name="data">Data do Cte</param>
        /// <returns>Lista de Ctes Cancelados</returns>
        IList<CteCanceladoRefaturamentoConfig> ObterCtesCanceladosRefaturamentoBaseConsolidadaConfigPorData(DateTime data);

        /// <summary>
        ///     Obtem o �ltimo cte cancelado para refaturamento da base consolidada config
        /// </summary>
        /// <returns>�ltimo Cte Cancelado</returns>
        CteCanceladoRefaturamentoConfig ObterUltimoCteCanceladoRefaturamentoBaseConsolidadaConfig();

        #endregion
    }
}