namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using Acesso;
    using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de CTE temp
	/// </summary>
	public interface ICteTempRepository : IRepository<CteTemp, int>
	{
        /// <summary>
        /// Gera o cte de manuten��o pelas tabelas tempor�rias
        /// </summary>
        /// <param name="usuario">Usu�rio que est� realizando a opera��o</param>
	    void GerarCteManutencaoPorTemporarias(Usuario usuario);

	    /// <summary>
	    /// Adicionar um novo <see cref="cte"/> ao <see cref="cte"/>
	    /// </summary>
	    /// <param name="cte"><see cref="cte"/> onde o </param>
	    void SalvarCteTemp(CteTemp cte);
	}
}