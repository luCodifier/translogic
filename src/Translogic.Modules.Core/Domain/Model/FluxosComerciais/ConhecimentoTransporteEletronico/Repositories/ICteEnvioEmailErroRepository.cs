namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório de envio de email com erro
	/// </summary>
	public interface ICteEnvioEmailErroRepository : IRepository<CteEnvioEmailErro, int?>
	{
	    /// <summary>
	    /// Retorna os CteEnvioEmail com Erro
	    /// </summary>
	    /// <param name="idCtes">id dos Cte</param>
        void RemoverTodosPorCte(int[] idCtes);
	}
}