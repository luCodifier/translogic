namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de motivo de cancelamento do CTE
	/// </summary>
	public interface ICteMotivoCancelamentoRepository : IRepository<CteMotivoCancelamento, int>
	{
		/// <summary>
		/// Obt�m os motivos ativos
		/// </summary>
		/// <returns>Lista de motivos de cancelamentos ativos</returns>
		IList<CteMotivoCancelamento> ObterAtivos();
	}
}