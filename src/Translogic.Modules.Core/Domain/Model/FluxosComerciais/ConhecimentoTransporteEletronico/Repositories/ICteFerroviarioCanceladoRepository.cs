﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;

    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    ///     Serviço de informações dos ctes e notas.
    /// </summary>
    public interface ICteFerroviarioCanceladoRepository
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Retorna informações dos ctes cancelados e notas
        /// </summary>
        /// <param name="request"> Objeto EletronicBillofLadingCancelledRailRequest</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Objeto EletronicBillofLadingCancelledRailResponse</returns>
        IList<EletronicBillofLadingCancelledData> ObterCtesCanceladosSemDespachoCancelado(EletronicBillofLadingCancelledRailRequest request, out string message);

        /// <summary>
        ///     Obtem os despachos cancelados e os próximos despachos gerados
        /// </summary>
        /// <param name="request">Objeto EletronicBillofLadingCancelledRailRequest com as datas para busca</param>
        /// <param name="message">Mensagem de retorno</param>
        /// <returns>Lista de despachos cancelados</returns>
        IList<DespachoCanceladoDto> ObterDespachosCancelados(EletronicBillofLadingCancelledRailRequest request, out string message);

        #endregion
    }
}