namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do CteComunicado
	/// </summary>
	public interface ICteComunicadoRepository : IRepository<CteComunicado, int?>
	{
		/// <summary>
		/// Obtem o Comunicado ativo para o Estado
		/// </summary>
		/// <param name="estado">Estado a ser pesquisado</param>
		/// <returns>Lista de CteComunicado</returns>
		CteComunicado ObterPorSiglaUf(string estado);

		/// <summary>
		/// Obtem os Comunicado ativos
		/// </summary>
		/// <returns>Lista de CteComunicado</returns>
		IList<CteComunicado> ObterTodosAtivos();
	}
}
