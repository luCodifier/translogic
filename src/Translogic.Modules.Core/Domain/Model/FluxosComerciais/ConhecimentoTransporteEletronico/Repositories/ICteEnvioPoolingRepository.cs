namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

	/// <summary>
	/// Inteface do repost�rio da classe Cte envio pooling
	/// </summary>
	public interface ICteEnvioPoolingRepository : IRepository<CteEnvioPooling, int?>
	{
	    /// <summary>
	    /// Remove os CT-e's do pool
	    /// </summary>
	    /// <param name="lista"> Lista de CTes a serem removidos. </param>
	    void RemoverPorLista(IList<Cte> lista);

		/// <summary>
		/// Limpa a sess�o
		/// </summary>
		void ClearSession();

		/// <summary>
		/// Limpa o objeto da sess�o
		/// </summary>
		/// <param name="pooling">Cte pooling sendo utilizado na sess�o</param>
		void ClearSession(CteEnvioPooling pooling);
	}
}
