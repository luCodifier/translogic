namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    ///     Inteface do repostório da classe Cte Cancelado Notas
    /// </summary>
    public interface ICteCanceladoNotasRepository : IRepository<CteCanceladoNotas, int?>
    {
    }
}