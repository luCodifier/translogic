namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da Classe do sap histórico
	/// </summary>
	public interface ICteSapHistoricoRepository : IRepository<CteSapHistorico, int?>
	{
	}
}