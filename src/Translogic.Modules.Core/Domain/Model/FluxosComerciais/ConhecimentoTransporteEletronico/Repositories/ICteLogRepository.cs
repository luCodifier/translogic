namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
    using System;
    using System.Text;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Inteface do repost�rio da classe CteLog
	/// </summary>
	public interface ICteLogRepository : IRepository<CteLog, int?>
	{
		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(Cte cte, string mensagem);

		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="nomeServico">Nome do servi�o ou m�todo que est� sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(Cte cte, string nomeServico, string mensagem);

		/// <summary>
		/// Insere o log de informa��o
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="builder">String builder do log</param>
		void InserirLogInfo(Cte cte, StringBuilder builder);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		/// <param name="ex">Exception do erro</param>
		void InserirLogErro(Cte cte, string mensagem, Exception ex = null);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		/// <param name="ex">Exception do erro</param>
		void InserirLogErro(Cte cte, string nomeServico, string mensagem, Exception ex = null);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="cte">Cte a ser logado</param>
		/// <param name="builder">String builderdo log</param>
		void InserirLogErro(Cte cte, StringBuilder builder);
	}
}