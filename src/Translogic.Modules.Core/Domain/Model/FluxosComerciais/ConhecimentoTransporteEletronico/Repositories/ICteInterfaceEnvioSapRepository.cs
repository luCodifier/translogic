namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da Classe de interface de envio para Sap
	/// </summary>
	public interface ICteInterfaceEnvioSapRepository : IRepository<CteInterfaceEnvioSap, int?>
	{
		/// <summary>
		/// Obtem os itens da interface que nao foram processados
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna uma lista dos itens que nao foram processados</returns>
		IList<CteInterfaceEnvioSap> ObterNaoProcessadosPorHost(string hostName);

		/// <summary>
		/// Atualiza os dados de envio do SAP
		/// </summary>
		/// <param name="cte">Cte a ser atualizado para envio</param>
		void AtualizarEnvioSap(Cte cte);

		/// <summary>
		/// Remove os dados de envio do SAP da interface
		/// </summary>
		/// <param name="cte">Cte a ser removido para envio</param>
		void RemoverCteInterfaceEnvioSap(Cte cte);

		/// <summary>
		/// Obt�m os CT-e's dado um agrupamento
		/// </summary>
		/// <param name="listaAgrupamento">Lista de agrupamentos</param>
		/// <returns>Lista de CteInterfaceEnvioSap</returns>
		IList<CteInterfaceEnvioSap> ObterFilhosPorAgrupamento(IList<CteAgrupamento> listaAgrupamento);

		/// <summary>
		/// Grava o nome do host na table de envio para o SAP
		/// </summary>
		/// <param name="hostName">Nome do host</param>
		/// <param name="cteInterfaceEnvioSap">Cte Interface envio sap</param>
		void GravarHostNaoProcessados(string hostName, CteInterfaceEnvioSap cteInterfaceEnvioSap);

		/// <summary>
		/// Obt�m os itens n�o processados que est�o sem host
		/// </summary>
		/// <returns>Lista de CteInterfaceEnvioSap</returns>
		IList<CteInterfaceEnvioSap> ObterNaoProcessadosSemHost();

		/// <summary>
		/// Obt�m um registro com os dados para interface de envio Sap - Cte
		/// </summary>
		/// <param name="cteId">id do Cte a ser obtido</param>
		/// <returns>Retorna um registro de interface Sap - Cte</returns>
		CteInterfaceEnvioSap ObterPorCte(int cteId);

		/// <summary>
		/// Obt�m os ctes que n�o foram processados o envio para o SAP
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ</param>
		/// <returns>Retorna uma lista com os ctes a serem enviados para o SAP</returns>
		IList<CteInterfaceEnvioSap> ObterNaoProcessados(int indAmbienteSefaz);
	}
}