namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da interface da importa��o do arquivo pdf
	/// </summary>
	public interface ICteInterfaceEnvioEmailRepository : IRepository<CteInterfaceEnvioEmail, int?>
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		IList<CteInterfaceEnvioEmail> ObterListaInterfacePorHost(string hostName);

		/// <summary>
		/// Obt�m o item da interface pelo Cte
		/// </summary>
		/// <param name="idCte">Id do Cte a ser retornado</param>
		/// <returns>Retorna o item da interface pelo Cte</returns>
		CteInterfaceEnvioEmail ObterInterfacePorCte(int idCte);
	}
}