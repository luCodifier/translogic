namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio de vers�o do CTE
	/// </summary>
	public interface ITempCompRepository : IRepository<TempComp, int>
	{
		/// <summary>
		/// Obter por primaria
		/// </summary>
		/// <param name="idPrimaria">Identificador do primaria</param>
		/// <returns>Retorna uma de elementos do primaria</returns>
		IList<TempComp> ObterPorPrimaria(int idPrimaria);
	}
}