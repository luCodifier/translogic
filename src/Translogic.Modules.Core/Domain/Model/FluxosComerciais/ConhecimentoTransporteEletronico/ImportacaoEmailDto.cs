namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    /// <summary>
    /// classe ImportacaoEmailHelper
    /// </summary>
    public class ImportacaoEmailDto
    {
        /// <summary>
        /// Propriedade do Cte
        /// </summary>
        public virtual Cte Cte { get; set; }
       
        /// <summary>
        /// Propriedade do Email
        /// </summary>
        public virtual string EmailNfe { get; set; }

        /// <summary>
        /// Propriedade do Email Xml
        /// </summary>
        public virtual string EmailXml { get; set; }
    }
}