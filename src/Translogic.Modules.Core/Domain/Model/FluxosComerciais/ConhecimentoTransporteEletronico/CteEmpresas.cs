namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Classe Cte empresa
	/// </summary>
	public class CteEmpresas : EntidadeBase<int?>
	{
		/// <summary>
		/// Propriedade Cte
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Propriedade da Empresa Remetente
		/// </summary>
		public virtual IEmpresa EmpresaRemetente { get; set; }

		/// <summary>
		/// Propriedade da empresa Expedidor
		/// </summary>
		public virtual IEmpresa EmpresaExpedidor { get; set; }

		/// <summary>
		/// Propriedade da empresa Recebedor
		/// </summary>
		public virtual IEmpresa EmpresaRecebedor { get; set; }

		/// <summary>
		/// Propriedade da empresa Destinatario
		/// </summary>
		public virtual IEmpresa EmpresaDestinatario { get; set; }

		/// <summary>
		/// Propriedade da empresa Tomadora
		/// </summary>
		public virtual IEmpresa EmpresaTomadora { get; set; }

		/// <summary>
		/// Propriedade da codigo do Tomador
		/// </summary>
		public virtual int? CodigoTomador { get; set; }
	}
}