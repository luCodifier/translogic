namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.Core.Dominio;
    using Diversos.Cte;
    using Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Trem.Veiculo.Vagao;

    /// <summary>
    /// Classe de CTE
    /// </summary>
    public class Cte : EntidadeBase<int?>
    {
        /// <summary>
        /// Propriedade do despacho do translogic
        /// </summary>
        public virtual DespachoTranslogic Despacho { get; set; }

        /// <summary>
        /// Propriedade do fluxo comercial do translogic
        /// </summary>
        public virtual FluxoComercial FluxoComercial { get; set; }

        /// <summary>
        /// Propriedade do contrato do translogic (mesmo do fluxo comercial)
        /// </summary>
        public virtual ContratoHistorico ContratoHistorico { get; set; }

        /// <summary>
        /// Propriedade vag�o 
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        /// Propriedade da vers�o do CTE
        /// </summary>
        public virtual CteVersao Versao { get; set; }

        /// <summary>
        /// Serie do Cte
        /// </summary>
        public virtual string Serie { get; set; }

        /// <summary>
        /// Numero do Cte
        /// </summary>
        public virtual string Numero { get; set; }

        /// <summary>
        /// Chave do Cte
        /// </summary>
        public virtual string Chave { get; set; }

        /// <summary>
        /// N�mero do protocolo de autoriza��o
        /// </summary>
        public virtual string NumeroProtocolo { get; set; }

        /// <summary>
        /// Data e hora de gera��o do Cte
        /// </summary>
        public virtual DateTime DataHora { get; set; }

        /// <summary>
        /// XML de autoriza��o do Cte
        /// </summary>
        public virtual string XmlAutorizacao { get; set; }

        /// <summary>
        /// XML de cancelamento do Cte
        /// </summary>
        public virtual string XmlCancelamento { get; set; }

        /// <summary>
        /// XML de complemento do Cte
        /// </summary>
        public virtual string XmlComplemento { get; set; }

        /// <summary>
        /// XML de inutiliza��o do Cte
        /// </summary>
        public virtual string XmlInutilizacao { get; set; }

        /// <summary>
        /// Lista com os status do Cte
        /// </summary>
        public virtual ICollection<CteStatus> ListaStatus { get; set; }

        /// <summary>
        /// Lista com os complementos do Cte
        /// </summary>
        public virtual ICollection<CteComplementado> ListaComplemento { get; set; }

        //		/// <summary>
        //		/// Complemento do Cte
        //		/// </summary>
        //		public virtual CteComplementado Complemento { get; set; }

        /// <summary>
        /// Status de Impress�o de Cte
        /// </summary>
        public virtual bool Impresso { get; set; }

        /// <summary>
        /// Indicador se o arquivo pdf foi gerado
        /// </summary>
        public virtual bool ArquivoPdfGerado { get; set; }

        /// <summary>
        /// Indicador se o arquivo xml foi gerado
        /// </summary>
        public virtual bool ArquivoXmlGerado { get; set; }

        /// <summary>
        /// Indica se o Cte � Complementar
        /// </summary>
        public virtual TipoCteEnum TipoCte { get; set; }

        /// <summary>
        /// Indicativo se o Cte sofreu manute��o
        /// </summary>
        public virtual bool Manutencao { get; set; }

        /// <summary>
        /// Situa��o atual do Cte
        /// </summary>
        public virtual SituacaoCteEnum SituacaoAtual { get; set; }

        /// <summary>
        /// Cnpj da Ferrovia
        /// </summary>
        public virtual string CnpjFerrovia { get; set; }

        /// <summary>
        /// Sigla Uf da Ferrovia
        /// </summary>
        public virtual string SiglaUfFerrovia { get; set; }

        /// <summary>
        /// Lista com os detalhes do Cte
        /// </summary>
        public virtual ICollection<CteDetalhe> ListaDetalhes { get; set; }

        /// <summary>
        /// Peso total do vag�o
        /// </summary>
        public virtual double? PesoVagao { get; set; }

        /// <summary>
        /// Valor do Cte
        /// </summary>
        public virtual double ValorCte { get; set; }

        /// <summary>
        /// Volume do vag�o
        /// </summary>
        public virtual double VolumeVagao { get; set; }

        /// <summary>
        /// Tara do Vagao.
        /// </summary>
        public virtual double TaraVagao { get; set; }

        /// <summary>
        /// Peso utilizado para calculo
        /// </summary>
        public virtual double? PesoParaCalculo { get; set; }

        /// <summary>
        /// Base de calculo do ICMS
        /// </summary>
        public virtual double? BaseCalculoIcms { get; set; }

        /// <summary>
        /// Valor do ICMS
        /// </summary>
        public virtual double? ValorIcms { get; set; }

        /// <summary>
        /// Aliquota do ICMS
        /// </summary>
        public virtual double? PercentualAliquotaIcms { get; set; }

        /// <summary>
        /// Valor do desconto por peso
        /// </summary>
        public virtual double? DescontoPorPeso { get; set; }

        /// <summary>
        /// Valor do desconto por percentual
        /// </summary>
        public virtual double? DescontoPorPercentual { get; set; }

        /// <summary>
        /// Valor a receber do CTE
        /// </summary>
        public virtual double? ValorReceber { get; set; }

        /// <summary>
        /// Valor total de mercadoria
        /// </summary>
        public virtual double? ValorTotalMercadoria { get; set; }

        /// <summary>
        /// Valor do frete 
        /// </summary>
        public virtual double? ValorFrete { get; set; }

        /// <summary>
        /// Valor total das notas fiscais
        /// </summary>
        public virtual double? ValorTotalNf { get; set; }

        /// <summary>
        /// Peso total da Nota fiscal
        /// </summary>
        public virtual double? PesoTotalNf { get; set; }

        /// <summary>
        /// Valor do desconto por tonelada
        /// </summary>
        public virtual double? ValorDescontoTonelada { get; set; }

        /// <summary>
        /// Valor do seguro do CTE
        /// </summary>
        public virtual double? ValorSeguro { get; set; }

        /// <summary>
        /// Cfop utilizado no CTE
        /// </summary>
        public virtual string Cfop { get; set; }

        /// <summary>
        /// Tipo de ambiente da Sefaz. 
        /// 1 - Produ��o  
        /// 2 - Homologa��o
        /// </summary>
        public virtual int AmbienteSefaz { get; set; }

        /// <summary>
        /// Data de vencimento do Ct-e
        /// </summary>
        public virtual DateTime? Vencimento { get; set; }

        /// <summary>
        /// Status Vigente (Ultimo status) da sefaz
        /// </summary>
        public virtual CteStatus StatusVigente
        {
            get
            {
                return ListaStatus.Where(g => g.CteStatusRetorno.InformadoPelaReceita).OrderByDescending(c => c.DataHora).First();
            }
        }

        /// <summary>
        /// Motivo de cancelamento do CTe
        /// </summary>
        public virtual CteMotivoCancelamento CteMotivoCancelamento { get; set; }

        /// <summary>
        /// Nota de Anulacao vinculada ao cte
        /// </summary>
        public virtual NotaFiscalAnulacao NfeAnulacao { get; set; }

        /// <summary>
        /// Observa��o do Cte
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Indicativo de multiplo despacho.
        /// </summary>
        public virtual bool IndMultiploDespacho { get; set; }

        /// <summary>
        /// Tipo de servi�o do Cte
        /// </summary>
        public virtual int? TipoServico { get; set; }

        /// <summary>
        /// Data de anulacao do CTE.
        /// </summary>
        public virtual DateTime? DataAnulacao { get; set; }

        /// <summary>
        /// Campo IndAltToma
        /// </summary>
        public virtual int? IndAltToma { get; set; }

        /// <summary>
        /// Segurado
        /// </summary>
        public virtual string Segurado { get; set; }

        /// <summary>
        /// N�mero de tentativas de envio feitas
        /// </summary>
        public virtual int? NumTentSeg { get; set; }

        public virtual string ObterCodigoVagao()
        {
            var valor = string.Empty;
            if (Vagao != null)
                valor = Vagao.Codigo;
            return valor;
        }

        public virtual string ObterSerieVagao()
        {
            var valor = string.Empty;
            if (Vagao != null)
                valor = Vagao.ObterSerie();
            return valor;
        }

        public virtual int? ObterTipoTomador()
        {
            int? valor = null;
            if (FluxoComercial != null)
                valor = FluxoComercial.ObterTipoTomador(); 

            return valor;
        }

        public virtual int? ObterTipoServicoCte()
        {
            int? valor = null;

            if (FluxoComercial != null)
                valor = FluxoComercial.TipoServicoCte;

            return valor;
        }
    }
}
