namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Tabela temporaria de teste de chave composta
	/// </summary>
	public class TempComp : EntidadeBase<int>
	{
		/// <summary>
		/// Dados do Id Principal
		/// </summary>
		public virtual int IdPrincipal { get; set; }

		/// <summary>
		/// Dados do Id Secundario
		/// </summary>
		public virtual int IdSecundario { get; set; }

		/// <summary>
		/// Informações do dados
		/// </summary>
		public virtual string Dados { get; set; }
	}
}