namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe hist�rica de envio de email do Cte com erro
	/// </summary>
	public class CteEnvioEmailErro : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte do envio de email
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Endere�os utilizados para envio de email
		/// </summary>
		public virtual string EmailPara { get; set; }

		/// <summary>
		/// Endere�os utilizados para envio de email em c�pia
		/// </summary>
		public virtual string EmailCopia { get; set; }

		/// <summary>
		/// Endere�os utilizados para envio de email em c�pia oculta
		/// </summary>
		public virtual string EmailCopiaOculta { get; set; }

		/// <summary>
		/// Descri��o do erro ocasionado no envio do email
		/// </summary>
		public virtual string DescricaoErro { get; set; }

		/// <summary>
		/// Nome do Servidor
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Tipo do arquivo enviado por email
		/// </summary>
		public virtual TipoArquivoEnvioEnum? TipoArquivoEnvio { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado no historico
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
        ///  Smtp que tentou enviar o email
		/// </summary>
        public virtual string Smtp { get; set; }
        
        /// <summary>
        /// Remetente que tentou enviar o email
        /// </summary>
        public virtual string Remetente { get; set; }
	}
}
