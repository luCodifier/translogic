﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Correção do Cte
    /// </summary>
    public class CorrecaoConteinerCte : EntidadeBase<int?>
    {
        /// <summary>
        /// objeto Cte
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Propriedade ConteinerOriginalNotaFiscal 
        /// </summary>
        public virtual string ConteinerNotaFiscal { get; set; }

        /// <summary>
        /// Propriedade ConteinerCorrigidoNotaFiscal 
        /// </summary>
        public virtual string ConteinerCorrigidoNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Chave Nota Fiscal Eletronica
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        ///  Gets or sets Chave Nota Fiscal Eletronica
        /// </summary>
        public virtual DateTime DataCorrecao { get; set; }

        /// <summary>
        /// O usuário do Cte
        /// </summary>
        public virtual Usuario Usuario { get; set; }
    }
}