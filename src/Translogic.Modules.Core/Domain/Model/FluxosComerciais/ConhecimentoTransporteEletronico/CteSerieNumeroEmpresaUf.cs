namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Estrutura;

	/// <summary>
	/// Serie e numero da empresa e uf
	/// </summary>
	public class CteSerieNumeroEmpresaUf : EntidadeBase<int>
	{
		/// <summary>
		/// Data de in�cio da vig�ncia da trava da data do despacho
		/// </summary>
		public virtual DateTime DataInicioVigenciaTravaDataDespacho { get; set; }

		/// <summary>
		/// Gets or sets Uf.
		/// </summary>
		public virtual string Uf { get; set; }

		/// <summary>
		/// Gets or sets Empresa.
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		/// Ultima S�rie gerada pelo SAP
		/// </summary>
		public virtual string UltimaSerieSap { get; set; }

		/// <summary>
		/// Ultimo N�mero gerado pelo SAP
		/// </summary>
		public virtual string UltimoNumeroSap { get; set; }

		/// <summary>
		/// Serie atual do CTE
		/// </summary>
		public virtual string SerieAtual { get; set; }

		/// <summary>
		/// Indica se vai ser enviado para producao da sefaz
		/// </summary>
		public virtual bool IndConsultaSimConsulta { get; set; }

		/// <summary>
		/// N�mero atual do CT-e
		/// </summary>
		public virtual string NumeroAtual { get; set; }

		/// <summary>
		/// Data de Inicio de vig�ncia
		/// </summary>
		public virtual DateTime DataInicioVigencia { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
		
		/// <summary>
		/// Indica se vai ser enviado para producao da sefaz
		/// </summary>
		public virtual bool ProducaoSefaz { get; set; }

        /// <summary>
        /// Propriedade da vers�o do CTE
        /// </summary>
        public virtual CteVersao Versao { get; set; }

        /// <summary>
        /// Propriedade da impress�o DCL
        /// </summary>
        public virtual bool IndLiberarImpressaoDcl { get; set; }
	}
}