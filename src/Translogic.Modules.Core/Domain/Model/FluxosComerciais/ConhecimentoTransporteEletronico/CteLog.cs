namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe de log do Cte
	/// </summary>
	public class CteLog : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte no qual o log se refere
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Tipo de log 
		/// </summary>
		public virtual TipoLogCteEnum TipoLog { get; set; }

		/// <summary>
		/// Nome do servi�o ou m�todo que est� sendo logado
		/// </summary>
		public virtual string NomeServico { get; set; }

		/// <summary>
		/// Descri��o do log
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Data e hora de grava��o do log
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Nome da m�quina que est� sendo executado o robo
		/// </summary>
		public virtual string HostName { get; set; }
	}
}