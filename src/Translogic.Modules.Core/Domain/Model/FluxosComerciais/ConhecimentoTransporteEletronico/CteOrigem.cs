﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ALL.Core.Dominio;

    public class CteOrigem : EntidadeBase<int?>
    {
        /// <summary>
        /// Cte
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Chave do Cte
        /// </summary>
        public virtual string ChaveCte { get; set; }
    }
}