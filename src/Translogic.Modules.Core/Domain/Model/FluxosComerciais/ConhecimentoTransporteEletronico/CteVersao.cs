namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de vers�o do CTE
	/// </summary>
	public class CteVersao : EntidadeBase<int>
	{
		/// <summary>
		/// C�digo da vers�o do CTE
		/// </summary>
		public virtual string CodigoVersao { get; set; }

		/// <summary>
		/// Indicador de a vers�o est� ativa
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Data e hora do cadastro da versao
		/// </summary>
		public virtual DateTime DataHora { get; set; }
	}
}