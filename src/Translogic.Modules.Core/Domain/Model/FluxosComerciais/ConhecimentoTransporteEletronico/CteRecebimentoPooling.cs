namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de pooling de recebimento de CTE
	/// </summary>
	public class CteRecebimentoPooling : EntidadeBase<int?>
	{
		/// <summary>
		/// Data e hora de cadastramento no pooling
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// HostName do servidor que ser� processado
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Identificador da lista da config
		/// </summary>
		public virtual int IdListaConfig { get; set; }
	}
}