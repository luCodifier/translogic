﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

    /// <summary>
    /// Classe de Cte Anulado
    /// </summary>
    public class CteAnulado : EntidadeBase<int?>
    {
        /// <summary>
        /// Objeto da classe Cte
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Data e hora do Complemento
        /// </summary>
        public virtual DateTime DataHora { get; set; }

        /// <summary>
        /// O usuário de complemento do Cte
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Cte Complementar
        /// </summary> 
        public virtual Cte CteAnulacao { get; set; }

        /// <summary>
        /// Tipo da anulacao - (AN - Anulacao n contribuinte) / (AC - Anulacao contribuinte)
        /// </summary> 
        public virtual string TipoAnulacao { get; set; }

    }
}