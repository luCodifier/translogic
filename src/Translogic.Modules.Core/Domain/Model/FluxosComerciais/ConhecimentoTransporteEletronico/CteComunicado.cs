namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de CteComunicado
	/// </summary>
	public class CteComunicado : EntidadeBase<int?>
	{
		/// <summary>
		/// Sigla UF do Comunicado
		/// </summary>
		public virtual string SiglaUf { get; set; }

		/// <summary>
		/// Data e hora de cadastramento do comunicado
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		/// <summary>
		/// Dados do documento (arquivo pdf) 
		/// </summary>
		public virtual byte[] ComunicadoPdf { get; set; }

		/// <summary>
		/// Indicador de Ativo
		/// </summary>
		public virtual bool? Ativo { get; set; }
	}
}