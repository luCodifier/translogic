namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe de envio do XML do CTE
	/// </summary>
	public class CteEnvioXml : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto da classe Cte
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Email de envio para
		/// </summary>
		public virtual string EnvioPara { get; set; }

		/// <summary>
		/// Tipo de envio do Cte
		/// </summary>
		public virtual TipoEnvioCteEnum TipoEnvioCte { get; set; }

		/// <summary>
		/// Tipo do arquivo enviado por email
		/// </summary>
		public virtual TipoArquivoEnvioEnum? TipoArquivoEnvio { get; set; }

		/// <summary>
		/// Indica se o email para esse Cte est� ativo
		/// </summary>
		public virtual bool Ativo { get; set; }
	}
}