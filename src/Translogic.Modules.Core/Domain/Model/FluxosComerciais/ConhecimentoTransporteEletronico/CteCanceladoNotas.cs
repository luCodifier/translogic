namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.Dominio;

    /// <summary>
    ///     Classe Cte Cancelado Notas
    /// </summary>
    public class CteCanceladoNotas : EntidadeBase<int?>
    {
        #region Public Properties

        public virtual CteCanceladoRefaturamento CteCanceladoRefaturamento { get; set; }

        public virtual string NotaCanceladaChave { get; set; }

        public virtual decimal? NotaCanceladaId { get; set; }

        public virtual string NotaChave { get; set; }

        public virtual decimal? NotaId { get; set; }

        #endregion
    }
}