namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Model.Acesso;

	/// <summary>
	/// Classe de status do Cte
	/// </summary>
	public class CteStatus : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto da classe Cte
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// O usu�rio de status do Cte
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Status de retorno do Cte
		/// </summary>
		public virtual CteStatusRetorno CteStatusRetorno { get; set; }

		/// <summary>
		/// Data e hora do status
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// XML de retorno do status do Cte
		/// </summary>
		public virtual string XmlRetorno { get; set; }

        /// <summary>
        /// Descricao de retorno do status do Cte
        /// </summary>
        public virtual string DescricaoRetorno { get; set; }

        /// <summary>
        ///  Protocolo do Cte Lista
        /// </summary>
        public virtual string Protocolo { get; set; }

        /// <summary>
        ///  Data hora do Protocolo do Cte Lista
        /// </summary>
        public virtual DateTime? DhProtocolo { get; set; }
	}
}