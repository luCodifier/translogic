namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe de log do Cte Runner
	/// </summary>
	public class CteRunnerLog : EntidadeBase<int?>
	{
		/// <summary>
		/// Tipo de log 
		/// </summary>
		public virtual TipoLogCteEnum TipoLog { get; set; }

		/// <summary>
		/// Nome da m�quina que est� sendo executado o robo
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Nome do servi�o que est� sendo feito o log
		/// </summary>
		public virtual string NomeServico { get; set; }

		/// <summary>
		/// Descri��o do log
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Data e hora de grava��o do log
		/// </summary>
		public virtual DateTime DataHora { get; set; }
	}
}