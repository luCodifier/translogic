namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de arquivo do FTP
    /// </summary>
    public class CteArquivoFtp : EntidadeBase<string>
    {
        /// <summary>
        /// Chave do Cte
        /// </summary>
        public virtual string ChaveCte { get; set; }

        /// <summary>
        /// Data de cadastro pelo Robo
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Objeto Cte
        /// </summary>
        public virtual Cte Cte { get; set; }
    }
}