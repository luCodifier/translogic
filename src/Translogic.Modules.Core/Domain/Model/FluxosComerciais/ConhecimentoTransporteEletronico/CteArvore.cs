namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de arvore do CTE
	/// </summary>
	public class CteArvore : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto Cte Raiz
		/// </summary>
		public virtual Cte CteRaiz { get; set; } 
		
		/// <summary>
		/// Objeto Cte Pai
		/// </summary>
		public virtual Cte CtePai { get; set; } 

		/// <summary>
		/// Objeto Cte
		/// </summary>
		public virtual Cte CteFilho { get; set; }

		/// <summary>
		/// Sequencia dos elementos da arvore
		/// </summary>
		public virtual int Sequencia { get; set; }
	}
}