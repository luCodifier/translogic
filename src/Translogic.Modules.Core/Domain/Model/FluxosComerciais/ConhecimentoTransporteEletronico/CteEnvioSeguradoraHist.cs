﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.Dominio;
    using Diversos.Cte;

    /// <summary>
    /// Classe cte envio seguradora hist
    /// </summary>
    public class CteEnvioSeguradoraHist : EntidadeBase<int>
    {
        /// <summary>
        /// Identificação do cte
        /// </summary>
        public Cte Cte { get; set; }

        /// <summary>
        /// Tipo de envio(email, web service)
        /// </summary>
        public string TipoEnvio { get; set; }

        /// <summary>
        /// email do remetente
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// seguradora enviada
        /// </summary>
        public string Seguradora { get; set; }

        /// <summary>
        /// retorno do envio
        /// </summary>
        public string Retorno { get; set; }

        /// <summary>
        /// status do envio
        /// </summary>
        public string StatusEnvio { get; set; }
    }
}