﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Trem.Veiculo.Vagao;

    /// <summary>
	/// Classe de Correção do Cte
	/// </summary>
	public class CorrecaoCte : EntidadeBase<int?>
    {
        /// <summary>
        /// Propriedade do fluxo comercial do translogic
        /// </summary>
        public virtual FluxoComercial FluxoComercial { get; set; }

        /// <summary>
        /// Propriedade vagão 
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        /// objeto Cte
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Observação do Cte
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// O usuário de status do Cte
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        /// Cfop utilizado no CTE
        /// </summary>
        public virtual string Cfop { get; set; }

        /// <summary>
        ///  Gets or sets Chave Nota Fiscal Eletronica
        /// </summary>
        public virtual DateTime DataCorrecao { get; set; }
    }
}