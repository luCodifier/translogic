namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;

    using ALL.Core.Dominio;

    /// <summary>
    ///     Classe Cte Cancelado Refaturamento
    /// </summary>
    public class CteCanceladoRefaturamento : EntidadeBase<int?>
    {
        #region Public Properties

        public virtual string CteCanceladoChave { get; set; }

        public virtual decimal? CteCanceladoId { get; set; }

        public virtual string CteNovoChave { get; set; }

        public virtual decimal? CteNovoId { get; set; }

        public virtual DateTime Data { get; set; }

        public virtual bool Processado { get; set; }

        public virtual ICollection<CteCanceladoNotas> ListaNotas { get; set; }

        #endregion
    }
}