namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

    /// <summary>
    /// Classe de dados tempor�rios do CT-e
    /// </summary>
    public class CteTemp : EntidadeBase<int>
    {
        /// <summary>
        /// C�digo do vag�o
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Id do cte de origem
        /// </summary>
        public virtual int? IdCteOrigem { get; set; }

        /// <summary>
        /// Id do despacho
        /// </summary>
        public virtual int? IdDespacho { get; set; }

        /// <summary>
        /// Id do fluxo comercial
        /// </summary>
        public virtual int IdFluxoComercial { get; set; }

        /// <summary>
        /// Peso total do vag�o
        /// </summary>
        public virtual double PesoVagao { get; set; }

        /// <summary>
        /// Volume do vag�o
        /// </summary>
        public virtual double? VolumeVagao { get; set; }

        /// <summary>
        /// Tarae do vag�o
        /// </summary>
        public virtual double? TaraVagao { get; set; }

        /// <summary>
        /// Tipo do cte
        /// </summary>
		public virtual TipoCteEnum TipoCte { get; set; }

        /// <summary>
        /// Campo IndMultDp
        /// </summary>
        public virtual string IndMultDp { get; set; }

        /// <summary>
        /// Campo IndAltToma
        /// </summary>
        public virtual int? IndAltToma { get; set; }
    }
}