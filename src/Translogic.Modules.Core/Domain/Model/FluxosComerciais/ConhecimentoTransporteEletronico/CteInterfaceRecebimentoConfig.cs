namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
    using Diversos.Cte;

	/// <summary>
	/// Classe de interface de recebimento da config
	/// </summary>
	public class CteInterfaceRecebimentoConfig : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte da interface de recebimento
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Identificador da lista da config
		/// </summary>
		public virtual int IdListaConfig { get; set; }
		
		/// <summary> 
		/// Codigo interno da filial (PK)
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Numero do documento CT-e (PK)
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Serie do documento CT-e (PK)
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado na fila de recebimento
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado que foi realizado a ultima leitura
		/// </summary>
		public virtual DateTime DataUltimaLeitura { get; set; }
	}
}