namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de motivo de cancelamento de CTE
	/// </summary>
	public class CteMotivoCancelamento : EntidadeBase<int>
	{
		/// <summary>
		/// Descricao de motivo de cancelamento
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indicador se est� ativa
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Data e hora do cadastro
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Indicador se cte cancelado pelo motivo, deve ser exibido na tela.
		/// </summary>
		public virtual bool ExibirTelaManutencao { get; set; }

        /// <summary>
        /// Indicador que determina envio para sefaz
        /// </summary>
        public virtual bool EnviaSefaz { get; set; }
	}
}