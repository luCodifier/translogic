﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Acesso;

    /// <summary>
    /// Classe de Procedimentos Operacionais e Responsáveis pela execução para o Cte
    /// </summary>
    public class CteResponsavelProcedimento : EntidadeBase<int?>
    {
        /// <summary>
        /// Onde está o Código de Status Retorno da Cte
        /// </summary>
        public virtual CteStatusRetorno CteStatusRetorno { get; set; }

        /// <summary>
        /// Descrição do Procedimento a ser executado
        /// </summary>
        public virtual string DescricaoProcedimento { get; set; }

        /// <summary>
        /// Nome do Procedimento a ser exeecutado
        /// </summary>
        public virtual string NomeProcedimento { get; set; }

        /// <summary>
        /// Link para o documento do procedimento a ser executado
        /// </summary>
        public virtual string LinkProcedimento { get; set; }

        /// <summary>
        /// Nome do responsável pelo execução do procedimento
        /// </summary>
        public virtual string ResponsavelProcedimento { get; set; }

        /// <summary>
        /// Usuário que cadastrou este vinculo entre procedimento, responsável e ctestatusretorno
        /// </summary>
        public virtual Usuario UsuarioCadastro { get; set; }

        /// <summary>
        /// Data e hora de cadastro
        /// </summary>
        public virtual DateTime DataHoraCadastro { get; set; }

        /// <summary>
        /// Indica se este Procedimento com este responsável para este ctestatusretorno está ativo ou não
        /// </summary>
        public char Ativo { get; set; }
    }
}