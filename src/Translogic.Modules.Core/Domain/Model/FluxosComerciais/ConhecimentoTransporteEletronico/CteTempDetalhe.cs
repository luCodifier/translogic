namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de dados tempor�rios de detalhes do CT-e - NF_NUM_NFL
    /// </summary>
    /// <remarks> O Id � a concatena��o do n�mero da nota com a s�rie </remarks>
    public class CteTempDetalhe : EntidadeBase<string>
    {
        /// <summary>
        /// Valor da nota fiscal - NF_VAL_NFL
        /// </summary>
        public virtual double? ValorNotaFiscal { get; set; }

        /// <summary>
        /// Data de cadastro do detalhe - NF_TIMESTAMP
        /// </summary>
        public virtual DateTime? DataCadastro { get; set; }

        /// <summary>
        /// Tipo da moeda - NF_TPO_MOD
        /// </summary>
        /// <remarks> Valor padr�o - BRL </remarks>
        public virtual string TipoMoeda { get; set; }

        /// <summary>
        /// N�mero do TIF quando houver - TIF_NUM_TIF
        /// </summary>
        public virtual string NumeroTif { get; set; }

        /// <summary>
        /// N�mero da nota fiscal - NF_NRO_NF
        /// </summary>
        public virtual string NumeroNotaFiscal { get; set; }

        /// <summary>
        ///  Serie da Nota fiscal - NF_SER_NF
        /// </summary>
        public virtual string SerieNotaFiscal { get; set; }

        /// <summary>
        /// C�digo da empresa remetente - EP_COD_REM
        /// </summary>
        public virtual string SiglaRemetente { get; set; }

        /// <summary>
        /// C�digo da empresa destinat�ria - EP_COD_DES
        /// </summary>
        public virtual string SiglaDestinatario { get; set; }

        /// <summary>
        /// Data da nota fiscal - NF_DAT_NF
        /// </summary>
        public virtual DateTime DataNotaFiscal { get; set; }

        /// <summary>
        /// Peso da nota fiscal - NF_PESO_NF
        /// </summary>
        public virtual double? PesoNotaFiscal { get; set; }

        /// <summary>
        /// Id da empresa remetente - EP_ID_REM
        /// </summary>
        public virtual int? IdEmpresaRemetente { get; set; }
        
        /// <summary>
        /// UF do remetente - EP_UF_REM
        /// </summary>
        public virtual string UfRemetente { get; set; }

        /// <summary>
        /// CNPJ do Remetente - EP_CGC_REM
        /// </summary>
        public virtual string CnpjRemetente { get; set; }

        /// <summary>
        /// Inscri��o estadual do Remetente - EP_IES_REM
        /// </summary>
        public virtual string InscricaoEstadualRemetente { get; set; }

        /// <summary>
        /// Id da empresa destinat�ria - EP_ID_DES
        /// </summary>
        public virtual int? IdEmpresaDestinatario { get; set; }

        /// <summary>
        /// UF da empresa Destinataria - EP_UF_DES
        /// </summary>
        public virtual string UfDestinatario { get; set; }

        /// <summary>
        /// CNPJ da Empresa Destinataria - EP_CGC_DES
        /// </summary>
        public virtual string CnpjDestinatario { get; set; }

        /// <summary>
        /// Inscri��o estadual da Empresa destinataria - EP_IES_DES
        /// </summary>
        public virtual string InscricaoEstadualDestinatario { get; set; }

        /// <summary>
        /// Chave da nota fiscal Eletr�nica - NFE_CHAVE_NFE
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Peso total da nota fiscal - NF_PESO_TOT_NF
        /// </summary>
        public virtual double PesoTotalNotaFiscal { get; set; }

        /// <summary>
        /// Id do vag�o - VG_ID_VG
        /// </summary>
        public virtual int IdVagao { get; set; }

        /// <summary>
        ///  Gets or sets Volume Nota Fiscal
        /// </summary>
        public virtual double? VolumeNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Valor Total Nota Fiscal
        /// </summary>
        public virtual double ValorTotalNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Conteiner Nota Fiscal
        /// </summary>
        public virtual string ConteinerNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Filial Emissora
		/// </summary>
		public virtual string FilialEmissora { get; set; }

		/// <summary>
		/// C�digo Nfe (s�rio e n�mero nfe concatenados)
		/// </summary>
		public virtual string CodigoNfe { get; set; }

        /// <summary>
        /// Chave Cte
        /// </summary>
        public virtual string ChaveCte { get; set; }
    }
}