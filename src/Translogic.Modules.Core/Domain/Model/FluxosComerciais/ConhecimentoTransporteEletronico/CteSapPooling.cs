namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe de pooling do envio para o sap de CTE
	/// </summary>
	public class CteSapPooling : EntidadeBase<int?>
	{
		/// <summary>
		/// Data e hora de cadastramento no pooling
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// HostName do servidor que ser� processado
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Tipo Operacao
		/// </summary>
		public virtual TipoOperacaoCteEnum? TipoOperacao { get; set; }
	}
}