namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
    using Diversos.Cte;

	/// <summary>
	/// Classe de envio para interface de recebimento da config dos arquivos de XML
	/// </summary>
	public class CteInterfaceXmlConfig : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte da interface de recebimento
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Chave do Cte
		/// </summary>
		public virtual string Chave { get; set; }

		/// <summary>
		/// Nome do Servidor
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado na fila de recebimento
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Data e hora da ultima leitura dos dados
		/// </summary>
		public virtual DateTime DataUltimaLeitura { get; set; }

        /// <summary>
        /// Tipo Operacao
        /// </summary>
        public virtual TipoOperacaoCteEnum? TipoOperacao { get; set; }
	}
}