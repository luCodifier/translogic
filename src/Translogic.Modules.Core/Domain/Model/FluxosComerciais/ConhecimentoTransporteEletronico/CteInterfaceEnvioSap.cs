namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Cte;

	/// <summary>
	/// Classe de interface de envio para Sap
	/// </summary>
	public class CteInterfaceEnvioSap : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte da interface de recebimento
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Data e hora que o cte foi Gravado com o retorno da Sefaz
		/// </summary>
		public virtual DateTime? DataGravacaoRetorno { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado na fila de envio
		/// </summary>
		public virtual DateTime? DataEnvioSap { get; set; }

		/// <summary>
		/// Nome do Servidor
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Tipo Operacao
		/// </summary>
		public virtual TipoOperacaoCteEnum? TipoOperacao { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado que foi realizado a ultima leitura
		/// </summary>
		public virtual DateTime? DataUltimaLeitura { get; set; }
	}
}