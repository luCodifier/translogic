﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.Dominio;
    using Estrutura;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Conteiner;

    /// <summary>
    /// Classe de agrupamento de Cte e conteiner
    /// </summary>
    public class CteConteiner : EntidadeBase<int>
    {
        /// <summary>
        /// Conhecimento de transporte eletônico Cte raiz
        /// </summary>
        public virtual Cte Cte { get; set; }

        /// <summary>
        /// Conhecimento de transporte eletônico Cte raiz
        /// </summary>
        public virtual Conteiner Conteiner { get; set; }

        /// <summary>
        /// Sequencia do Cte
        /// </summary>
        public virtual long Agrupamento { get; set; }

        /// <summary>
        /// Id do agrupamento da carga
        /// </summary>
        public virtual long AgrupamentoCarga { get; set; }
        
        /// <summary>
        /// Indicativo do carregamento cancelado
        /// </summary>       
        public virtual string IndCarregamentoCancelado { get; set; }
        
        /// <summary>
        /// Indicativo do carregamento finalizado
        /// </summary>       
        public virtual string IndCarregamentoFinalizado { get; set; }
    }
}