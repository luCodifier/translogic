namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	///  Classe de detalhe do Cte
	/// </summary>
	public class CteDetalhe : EntidadeBase<int?>
	{
		/// <summary>
		/// objeto Cte
		/// </summary>
		public virtual Cte Cte { get; set; }

        /// <summary> 
		///  Gets or sets Num Nota Serie 
		/// </summary>
		public virtual string NumeroNotaSerie { get; set; }

		/// <summary>
		/// Valor Total da Nota Fiscal
		/// </summary>
		public virtual double ValorNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		///  Gets or sets Modelo Nota Fiscal
		/// </summary>
		public virtual string ModeloNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets NumeroTif
		/// </summary>
		public virtual string NumeroTif { get; set; }

		/// <summary>
		///  Gets or sets Num Nota
		/// </summary>
		public virtual string NumeroNota { get; set; }

		/// <summary>
		///  Gets or sets Serie Nota
		/// </summary>
		public virtual string SerieNota { get; set; }

		/// <summary>
		///  Gets or sets Cod Remetente
		/// </summary>
		public virtual string CodigoRemetente { get; set; }

		/// <summary>
		///  Gets or sets Cod Destinatario
		/// </summary>
		public virtual string CodigoDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Dt Nota Fiscal
		/// </summary>
		public virtual DateTime DataNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Peso Nota Fiscal
		/// </summary>
		public virtual double PesoNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Peso Total
        /// </summary>
        public virtual double PesoTotal { get; set; }

		/// <summary>
		///  Gets or sets Id Rementente
		/// </summary>
		public virtual int IdRementente { get; set; }

		/// <summary>
		///  Gets or sets Uf Remetente
		/// </summary>
		public virtual string UfRemetente { get; set; }

		/// <summary>
		///  Gets or sets Cgc Remetente
		/// </summary>
		public virtual string CgcRemetente { get; set; }

		/// <summary>
		///  Gets or sets Ins Estadual Remetente
		/// </summary>
		public virtual string InsEstadualRemetente { get; set; }

		/// <summary>
		///  Gets or sets Id Destinatario
		/// </summary>
		public virtual int IdDestinatario { get; set; }

		/// <summary>
		///  Gets or sets UfDestinatario
		/// </summary>
		public virtual string UfDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Cgc Destinatario
		/// </summary>
		public virtual string CgcDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Ins Estadual Destinatario
		/// </summary>
		public virtual string InsEstadualDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Tipo Registro
		/// </summary>
		public virtual string TipoRegistro { get; set; }

		/// <summary>
		///  Gets or sets Id Nfe
		/// </summary>
		public virtual int IdNfe { get; set; }

		/// <summary>
		///  Gets or sets Chave Nota Fiscal Eletronica
		/// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        ///  Gets or sets Volume Nota Fiscal
        /// </summary>
        public virtual double VolumeNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Valor Total Nota Fiscal
        /// </summary>
        public virtual double ValorTotalNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Conteiner Nota Fiscal
        /// </summary>
        public virtual string ConteinerNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Filial Emissora
		/// </summary>
		public virtual string FilialEmissora { get; set; }
	}
}