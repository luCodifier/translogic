namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;

    using ALL.Core.Dominio;

    /// <summary>
    ///     Classe Cte Inutilização Config
    /// </summary>
    public class CteInutilizacaoConfig : EntidadeBase<int?>
    {
        #region Public Properties

        public virtual string Numero { get; set; }

        public virtual string Serie { get; set; }

        public virtual string Cnpj { get; set; }

        public virtual int IdListaConfig { get; set; }

        public virtual int? StatusCte { get; set; }

        public virtual long? Protocolo { get; set; }

        public virtual bool Processado { get; set; }

        #endregion
    }
}