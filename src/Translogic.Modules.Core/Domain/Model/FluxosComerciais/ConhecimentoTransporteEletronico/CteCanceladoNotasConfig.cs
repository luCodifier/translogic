namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using ALL.Core.Dominio;

    /// <summary>
    ///     Classe Cte Cancelado Notas Config
    /// </summary>
    public class CteCanceladoNotasConfig : EntidadeBase<int?>
    {
        #region Public Properties

        public virtual CteCanceladoRefaturamentoConfig CteCanceladoRefaturamentoConfig { get; set; }

        public virtual string NotaCanceladaChave { get; set; }

        public virtual decimal? NotaCanceladaId { get; set; }

        public virtual string NotaChave { get; set; }

        public virtual decimal? NotaId { get; set; }

        #endregion
    }
}