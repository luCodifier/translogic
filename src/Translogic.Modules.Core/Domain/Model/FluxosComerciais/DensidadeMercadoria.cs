namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Defini��o da densidade da mercadoria
    /// </summary>
    public class DensidadeMercadoria : EntidadeBase<string>
    {
        /// <summary>
        /// Densidade da mercadoria.
        /// </summary>
        public virtual double Densidade { get; set; }
    }
}