﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using System;
    using System.ComponentModel;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
    /// Interface do contrato
    /// </summary>
    public interface IContrato
    {
        /// <summary>
        /// Condição de frete - <see cref="CondicaoFrete"/>
        /// </summary>
        CondicaoFrete CondicaoFrete { get; set; }

        /// <summary>
        /// Empresa do contrato
        /// </summary>
        EmpresaFerrovia Empresa { get; set; }

        /// <summary>
        /// Empresa de cobrança do contrato
        /// </summary>
        EmpresaCliente EmpresaCobranca { get; set; }

        /// <summary>
        /// Empresa destinatária
        /// </summary>
        EmpresaCliente EmpresaDestinataria { get; set; }

        /// <summary>
        /// Empresa de faturamento do contrato
        /// </summary>
        EmpresaCliente EmpresaFaturamento { get; set; }

        /// <summary>
        /// Empresa pagadora
        /// </summary>
        IEmpresa EmpresaPagadora { get; set; }

        /// <summary>
        /// Empresa remetente
        /// </summary>
        EmpresaCliente EmpresaRemetente { get; set; }

        /// <summary>
        /// Ferrovia que fatura o carregamento
        /// </summary>
        /// <remarks>
        /// Foi feito o Enumeration, pois os valores que estão aqui vem do SAP e são diferentes da sigla que está na tabela "Empresa"
        /// </remarks>
        FerroviaFaturamentoEnum? FerroviaFaturamento { get; set; }

        /// <summary>
        /// Indica se o fluxo é de container
        /// </summary>
        bool? IndFluxoContainer { get; set; }

        /// <summary>
        /// Data de Início de Vigência
        /// </summary>
        DateTime InicioVigencia { get; set; }

        /// <summary>
        /// Número do contrato
        /// </summary>
        string NumeroContrato { get; set; }

        /// <summary>
        /// Prazo do contrato - em horas
        /// </summary>
        int? Prazo { get; set; }

        /// <summary>
        /// Prazo de cancelamento - em horas
        /// </summary>
        int? PrazoCancelamento { get; set; }

        /// <summary>
        /// Tempo de trânsito - em horas
        /// </summary>
        int? TempoTransito { get; set; }

        /// <summary>
        /// Data de Término de Vigência
        /// </summary>
        DateTime? TerminoVigencia { get; set; }

        /// <summary>
        /// Volume contratado
        /// </summary>
        double? VolumeContratado { get; set; }

        /// <summary>
        /// Código da unidade de medida
        /// </summary>
        string CodigoUnidadeMedida { get; set; }

        /// <summary>
        /// Alíquota do ICMS
        /// </summary>
        double? ValorAliquotaIcmsProduto { get; set; }

        /// <summary>
        /// Base de cálculo
        /// </summary>
        double? ValorBaseCalculo { get; set; }

        /// <summary>
        /// Sigla do CFOP
        /// </summary>
        string Cfop { get; set; }

        /// <summary>
        /// Descrição do CFOP
        /// </summary>
        string DescricaoCfop { get; set; }

        /// <summary>
        /// Valor da base de calculo de substituicao do produto
        /// </summary>
        double? ValorBaseCalculoSubstituicaoProduto { get; set; }

        /// <summary>
        /// CFOP do produto
        /// </summary>
        string CfopProduto { get; set; }

        /// <summary>
        /// Aliquota do icms de substituição do produto
        /// </summary>
        double? AliquotaIcmsSubstProduto { get; set; }

        /// <summary>
        /// Mensagem Fiscal
        /// </summary>
        string MensagemFiscal { get; set; }
        
        /// <summary>
        /// Mensagem Fiscal
        /// </summary>
        string MensagemFiscalExp { get; set; }

        /// <summary>
        /// Tipo de tributação
        /// </summary>
        int? TipoTributacao { get; set; }

        /// <summary>
        /// Desconto por peso
        /// </summary>
        double? DescontoPorPeso { get; set; }

        /// <summary>
        /// Data e hora de cadastro / atualização do registro
        /// </summary>
        DateTime DataHoraAtualizacao { get; set; }

        /// <summary>
        /// Percentual de aliquota do Icms
        /// </summary>
        double? PercentualAliquotaIcms { get; set; }

        /// <summary>
        /// Percentual de desconto
        /// </summary>
        double? PercentualDesconto { get; set; }

        /// <summary>
        /// Empresa remetente fiscal
        /// </summary>
        EmpresaCliente EmpresaRemetenteFiscal { get; set; }

        /// <summary>
        /// Empresa destinataria fiscal
        /// </summary>
        EmpresaCliente EmpresaDestinatariaFiscal { get; set; }

        /// <summary>
        /// Percentual de Seguro
        /// </summary>
        double? PercentualSeguro { get; set; }

        /// <summary>
        /// Unidade Monetária do Contrato
        /// </summary>
        string UnidadeMonetaria { get; set; }

        /// <summary>
        /// Codigo do fluxo comercial 
        /// </summary>
        string CodigoFluxoComercial { get; set; }

        /// <summary>
        /// Percentual da aliquota de cofins
        /// </summary>
        double? PercentualCofins { get; set; }

        /// <summary>
        /// Percentual da aliquota de Pis
        /// </summary>
        double? PercentualPis { get; set; }

        /// <summary>
        /// Cfop Anulacao
        /// </summary>
        string CfopAnulacao { get; set; }

        /// <summary>
        /// Desc Cfop Anulacao
        /// </summary>
        string DescCfopAnulacao { get; set; }
    }
}