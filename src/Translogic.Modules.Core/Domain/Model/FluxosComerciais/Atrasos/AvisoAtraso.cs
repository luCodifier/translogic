﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Atrasos
{
    using System;
    using System.Collections.Generic;
    using Acesso;
    using ALL.Core.Dominio;
    using Estrutura;
    using Trem.Veiculo.Vagao;
    using Via;

    public class AvisoAtraso : EntidadeBase<int>
    {
        public AvisoAtraso()
        {
            Vagoes = new List<Vagao>();
        }

        public virtual DateTime Data { get; set; }

        public virtual decimal HorasAtraso { get; set; }

        public virtual decimal? TU { get; set; }

        public virtual int QtdeVagoes { get; set; }

        public virtual String VagoesDigitados { get; set; }

        public virtual IEmpresa Cliente { get; set; }

        public virtual string DescResumida { get; set; }

        public virtual EstacaoMae Estacao { get; set; }

        public virtual TipoAtraso Tipo { get; set; }

        public virtual MotivoAtraso Motivo { get; set; }

        public virtual ResponsavelAtraso Responsavel { get; set; }

        public virtual IList<Vagao> Vagoes { get; set; }

        public virtual string Observacao { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual Mercadoria Mercadoria { get; set; }
    }
}