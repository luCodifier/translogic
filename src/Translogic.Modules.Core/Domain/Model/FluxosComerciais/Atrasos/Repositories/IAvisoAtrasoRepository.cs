﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Atrasos.Repositories
{
    using ALL.Core.Dominio;

    public interface IAvisoAtrasoRepository
    {
        ResultadoPaginado<AvisoAtraso> ObterAtrasos(DetalhesPaginacao pagination, DateTime dataInicial, DateTime dataFinal, TipoAtraso tipo, ResponsavelAtraso responsavel);

        AvisoAtraso Salvar(AvisoAtraso avisoAtraso);

        AvisoAtraso ObterPorId(int idAvisoAtraso);

        IEnumerable<MotivoAtraso> ObterMotivos();

        IEnumerable<TipoAtraso> ObterTipos();

        IEnumerable<ResponsavelAtraso> ObterResponsaveis();
    }
}