﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Atrasos
{
    using System;
    using ALL.Core.Dominio;

    public class TipoAtraso : EntidadeBase<string>
    {
        public virtual string Nome { get; set; }
        public virtual string Descricao { get; set; }
        public virtual bool Ativo { get; set; }
    }
}