namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de Destino que permite Refaturamento
	/// </summary>
	public interface IRefaturamentoDestinoPermRepository : IRepository<RefaturamentoDestinoPerm, int>
	{
		/// <summary>
		/// Retorna todos os Destino Permitidos para Refaturamento
		/// </summary>
		/// <returns>lista de Destino Permitidos para Refaturamento.</returns>
		IList<RefaturamentoDestinoPerm> ObterTodosAtivos();
	}
}