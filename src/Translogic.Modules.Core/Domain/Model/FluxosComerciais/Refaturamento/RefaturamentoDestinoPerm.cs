namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Refaturamento
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de Destino Permitido para Refaturamento 
	/// </summary>
	public class RefaturamentoDestinoPerm : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Fluxo Comercial
		/// </summary>
		public virtual string CodigoAreaOperacional { get; set; }

		/// <summary>
		/// Fluxo Comercial
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Fluxo Comercial
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		#endregion
	}
}