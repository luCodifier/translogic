﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Intercambios;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
	using Translogic.Modules.Core.Domain.Model.Via;

	/// <summary>
	/// Modelo Carregamento
	/// </summary>
	public class Carregamento : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets Area operacional
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacional { get; set; }

		/// <summary>
		/// Gets or sets Descarregamento
		/// </summary>
		public virtual Descarregamento Descarregamento { get; set; }

		/// <summary>
		/// Gets or sets FluxoComercial
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		/// Gets or sets Baldeio
		/// </summary>
		public virtual Baldeio Baldeio { get; set; }

		/// <summary>
		/// Gets or sets IdPedido
		/// </summary>
		public virtual int IdPedido { get; set; }

		/// <summary>
		/// Gets or sets DataCarregamento
		/// </summary>
		public virtual DateTime DataCarregamento { get; set; }

		/// <summary>
		/// Gets or sets QtdCarregada
		/// </summary>
		public virtual int QtdCarregada { get; set; }

		/// <summary>
		/// Gets or sets DtCarregamento
		/// </summary>
		public virtual DateTime DtCarregamento { get; set; }

		/// <summary>
		/// Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		/// Gets or sets DtIniCarregamento
		/// </summary>
		public virtual DateTime DtIniCarregamento { get; set; }

		/// <summary>
		/// Gets or sets NumVagCarregados
		/// </summary>
		public virtual int? NumeroVagoesCarregados { get; set; } 

		/// <summary>
		/// Gets or sets RegistroIntercambio
		/// </summary>
		public virtual RegistroIntercambio RegistroIntercambio { get; set; }

		/// <summary>
		/// Gets or sets PedidoDerivado
		/// </summary>
		public virtual PedidoDerivado PedidoDerivado { get; set; }

		/// <summary>
		/// Gets or sets FluxoComercial
		/// </summary>
		public virtual FluxoComercial FluxoComercialCarregamento { get; set; }

		#endregion
	}
}
