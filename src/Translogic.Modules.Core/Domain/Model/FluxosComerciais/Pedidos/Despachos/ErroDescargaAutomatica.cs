﻿
namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe model para erros na descarga automatica
    /// </summary>
    public class ErroDescargaAutomatica : EntidadeBase<int>
    {
        /// <summary>
        /// Descrição do erro
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// XML retornado na execução da dercarga
        /// </summary>
        public virtual string XMLRetorno { get; set; }

        /// <summary>
        /// Data do erro
        /// </summary>
        public virtual DateTime Data { get; set; }

        /// <summary>
        /// Dados do vagão/pedido
        /// </summary>
        public virtual VagaoPedido VagaoPedido { get; set; }

        /// <summary>
        /// Usuário que tentou descarregar o vagão
        /// </summary>
        public virtual Usuario Usuario { get; set; }
    }
}