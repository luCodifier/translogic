﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Intercambios;
	using Translogic.Modules.Core.Domain.Model.Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;

	/// <summary>
	/// Model Despacho Translogic
	/// </summary>
	public class DespachoTranslogic : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets IdDespachoRedeSp
		/// </summary>
		public virtual int? IdDespachoRedeSp { get; set; }

		/// <summary>
		///  Gets or sets GrupoDespacho
		/// </summary>
		public virtual GrupoDespacho GrupoDespacho { get; set; }

		/// <summary>
		///  Gets or sets DtDespacho
		/// </summary>
		public virtual DateTime DataDespacho { get; set; }

		/// <summary>
		///  Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets NumeroDespacho
		/// </summary>
		public virtual int? NumeroDespacho { get; set; }

		/// <summary>
		///  Gets or sets RegistroIntercambio
		/// </summary>
		public virtual RegistroIntercambio RegistroIntercambio { get; set; }

		/// <summary>
		///  Número de série do despacho
		/// </summary>
		public virtual int? NumeroSerieDespacho { get; set; }

		/// <summary>
		///  Gets or sets IndImpressao
		/// </summary>
		public virtual bool IndImpressao { get; set; }

		/// <summary>
		///  Gets or sets DtCancelamento CNG
		/// </summary>
		public virtual DateTime? DataCng { get; set; }

		/// <summary>
		///  Gets or sets Observacao
		/// </summary>
		public virtual string Observacao { get; set; }

		/// <summary>
		///  Gets or sets data
		/// </summary>
		public virtual int IdPedido { get; set; }

		/// <summary>
		///  Gets or sets SerieDespacho
		/// </summary>
		public virtual SerieDespacho SerieDespacho { get; set; }

		/// <summary>
		///  Gets or sets Empresa
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		///  Gets or sets IndFechamento
		/// </summary>
		public virtual bool IndFechamento { get; set; }

		/// <summary>
		///  Gets or sets IndCancelamento
		/// </summary>
		public virtual bool IndCancelamento { get; set; }

		/// <summary>
		///  Gets or sets DtCancelamento CLD
		/// </summary>
		public virtual DateTime? DataCancelamento { get; set; }

		/// <summary>
		///  Gets or sets PesoLiqCarregado
		/// </summary>
		public virtual double? PesoLiqCarregado { get; set; }

		/// <summary>
		///  Gets or sets PesoTotCarregado
		/// </summary>
		public virtual double? PesoTotCarregado { get; set; }

		/// <summary>
		///  Gets or sets SerieDespacho SDI
		/// </summary>
		public virtual string SerieDespachoSdi { get; set; }

		/// <summary>
		///  Gets or sets NumDespIntercambio
		/// </summary>
		public virtual int? NumDespIntercambio { get; set; }

		/// <summary>
		///  Gets or sets DtDespIntercambio
		/// </summary>
		public virtual DateTime? DtDespIntercambio { get; set; }

		/// <summary>
		///  Gets or sets PedidoDerivado
		/// </summary>
		public virtual PedidoDerivado PedidoDerivado { get; set; }

        /// <summary>
        ///  Gets or sets IdPedidoTransporte
        /// </summary>
        ////public virtual int? IdPedidoTransporte { get; set; } comentado devido ao problema de invalid index 28 parameter

		/// <summary>
		///  Gets or sets IdFluxoIntercambio
		/// </summary>
		public virtual int? IdFluxoIntercambio { get; set; }

		/// <summary>
		///  Gets or sets NumeroConteiners
		/// </summary>
		public virtual int? NumeroConteiners { get; set; }

		/// <summary>
		///  Gets or sets DespachoImpresso
		/// </summary>
		public virtual bool DespachoImpresso { get; set; }

		/// <summary>
		///  Gets or sets NumeroDespachoUf
		/// </summary>
		public virtual int? NumeroDespachoUf { get; set; }

		/// <summary>
		///  Gets or sets Serie despacho UF
		/// </summary>
		public virtual SerieDespachoUf SerieDespachoUf { get; set; }

		#endregion
	}
}
