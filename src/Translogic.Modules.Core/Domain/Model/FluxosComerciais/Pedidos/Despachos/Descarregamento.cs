﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
	/// Modelo Descarregamento
	/// </summary>
	public class Descarregamento : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
		/// Gets or sets Carregamento
        /// </summary>
		public virtual Carregamento Carregamento { get; set; }

		/// <summary>
		/// Gets or sets Area operacional
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacional { get; set; }

		/// <summary>
		/// Gets or sets DtDescarga
		/// </summary>
		public virtual DateTime DtDescarga { get; set; }

		/// <summary>
		/// Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		/// Gets or sets IdPedidoDerivado
		/// </summary>
		public virtual int IdPedidoDerivado { get; set; }

		/// <summary>
		/// Gets or sets DtIniDescarga
		/// </summary>
		public virtual DateTime DtIniDescarga { get; set; }

		/// <summary>
		/// Gets or sets Baldeio
		/// </summary>
		public virtual Baldeio Baldeio { get; set; }

		/// <summary>
		/// Gets or sets NumVagDescarregados
		/// </summary>
		public virtual int NumVagDescarregados { get; set; }

		/// <summary>
		/// Gets or sets Area operacional
		/// </summary>
		public virtual string UsuRespCad { get; set; }

		/// <summary>
		/// Gets or sets PedidoDerivado
		/// </summary>
		public virtual PedidoDerivado PedidoDerivado { get; set; }

        #endregion
    }
}
