﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
    /// Model DetalheDescarregamento
	/// </summary>
    public class DetalheDescarregamento : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets Descarregamento
		/// </summary>
        public virtual Descarregamento Descarregamento { get; set; }

        /// <summary>
        /// Gets or sets DetalheCarregamento Aloca
        /// </summary>
        public virtual DetalheCarregamento DetalheCarregamento { get; set; }

		/// <summary>
		/// Gets or sets Vagao Indicado
		/// </summary>
		public virtual Vagao VagaoIndicado { get; set; }

		/// <summary>
		/// Gets or sets Peso após o descarregamento
		/// </summary>
        public virtual double? PesoDescarregado { get; set; }

        /// <summary>
        /// Gets or sets Volume após o descarregamento
        /// </summary>
        public virtual double? VolumeDescarregado { get; set; }

		/// <summary>
		/// Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime? DtCadastro { get; set; }

		/// <summary>
		/// Gets or sets Tonelada util
		/// </summary>
        public virtual double? PesoAntes { get; set; }

		/// <summary>
		/// Gets or sets Volume
		/// </summary>
        public virtual double? PesoDepois { get; set; }

		#endregion
	}
}
