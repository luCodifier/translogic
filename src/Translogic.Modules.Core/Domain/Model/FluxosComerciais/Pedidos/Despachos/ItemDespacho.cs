﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Intercambios;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Model ItemDespacho
	/// </summary>
	public class ItemDespacho : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets Detalhe Carregamento
		/// </summary>
		public virtual DetalheCarregamento DetalheCarregamento { get; set; }

		/// <summary>
		///  Gets or sets Despacho
		/// </summary> 
		public virtual DespachoTranslogic DespachoTranslogic { get; set; }

		/// <summary>
		///  Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets Detalhe Intercambio
		/// </summary>
		public virtual DetalheIntercambio DetalheIntercambio { get; set; }

		/// <summary>
		///  Gets or sets Vagao
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		///  Gets or sets Id Nota Fiscal
		/// </summary>
		public virtual int? IdNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Peso Vag Liquido
		/// </summary>
		public virtual double? PesoLiquido { get; set; }

		/// <summary>
		///  Gets or sets Peso de Cálculo
		/// </summary>
        public virtual double? PesoCalculo { get; set; }

		/// <summary>
		///  Gets or sets TU
		/// </summary>
		public virtual double? PesoToneladaUtil { get; set; }

        /// <summary>
        /// Peso real do vagão
        /// </summary>
        public virtual double? PesoReal
	    {
	        get { return PesoLiquido.Equals(0) ? PesoToneladaUtil : PesoLiquido; }
	    }

		/// <summary>
		///  Indica se o vagão já foi descarregado
		/// </summary>
		public virtual bool? IndVagaoDescarregado { get; set; }

		/// <summary>
		///  Indica se o vagão está fora de uso
		/// </summary>
		public virtual bool? IndForaDeUso { get; set; }

        /// <summary>
		///  Indica se o Baldeio
		/// </summary>
		public virtual string IndBaldeio { get; set; }

        /// <summary>
        ///  Gets or sets Volume
        /// </summary>
        public virtual double? Volume { get; set; }

		/// <summary>
		///  Gets or sets Peso Total Vg
		/// </summary>
		public virtual double? PesoTotalVagao { get; set; }

		/// <summary>
		///  Gets or sets Ind Pre Faturamento
		/// </summary>
		public virtual bool? IndPreFaturamento { get; set; }

		/// <summary>
		///  Gets or sets Ind Manutencao
		/// </summary>
		public virtual bool? IndManutencao { get; set; }

		/// <summary>
		///  Gets or sets Numero Conteiner
		/// </summary>
		public virtual int? NumeroConteiner { get; set; }

        /// <summary>
        ///  Gets or sets Detalhe Carregamento Operacional
        /// </summary>
        public virtual int? DetalheCarregamentoOperacional { get; set; }

		#endregion
	}
}
