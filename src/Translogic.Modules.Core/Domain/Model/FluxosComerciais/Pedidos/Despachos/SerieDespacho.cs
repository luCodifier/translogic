﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Model Serie Despacho
	/// </summary>
	public class SerieDespacho : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets Empresa
		/// </summary>
		public virtual Empresa Empresa { get; set; }

		/// <summary>
		///  Gets or sets Serie Despacho Num
		/// </summary>
		public virtual string SerieDespachoNum { get; set; }

		/// <summary>
		///  Gets or sets Ind Ativo
		/// </summary>
		public virtual bool IndAtivo { get; set; }

		/// <summary>
		///  Gets or sets Dt Criacao Serie
		/// </summary>
		public virtual DateTime DtCriacaoSerie { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		#endregion
	}
}
