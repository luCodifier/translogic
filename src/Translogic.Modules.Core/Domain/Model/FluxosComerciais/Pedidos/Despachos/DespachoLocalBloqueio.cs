﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Acesso;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    public class DespachoLocalBloqueio : EntidadeBase<int>
    {

        #region Propriedades

        public virtual IAreaOperacional Estacao { get; set; }

        public virtual string Segmento { get; set; }

        public virtual string CnpjRaiz { get; set; }

        public virtual Usuario Usuario { get; set; }

        public virtual DateTime DataCadastro { get; set; }

        #endregion
    }
}