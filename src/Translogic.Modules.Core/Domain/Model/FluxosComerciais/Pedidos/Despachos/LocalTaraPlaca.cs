namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    using ALL.Core.Dominio;
    using Via;

    /// <summary>
    /// Classe que identifica o local em que deve ser preenchido a tara no despacho
    /// </summary>
    public class LocalTaraPlaca : EntidadeBase<int?>
    {
        /// <summary>
        /// Local em que deve ser preenchido a tara
        /// </summary>
        public virtual IAreaOperacional Local { get; set; }
    }
}