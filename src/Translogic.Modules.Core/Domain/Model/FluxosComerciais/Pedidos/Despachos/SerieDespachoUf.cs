﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Model Serie Despacho Uf
	/// </summary>
	public class SerieDespachoUf : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets Empresa Ferrovia Despacho
		/// </summary>
		public virtual IEmpresa EmpresaFerroviaDespacho { get; set; }

		/// <summary>
		///  Gets or sets Num Serie Despacho
		/// </summary>
		public virtual int NumeroSerieDespacho { get; set; }

		/// <summary>
		///  Gets or sets Cod Serie Despacho
		/// </summary>
		public virtual string CodigoSerieDespacho { get; set; }

		/// <summary>
		///  Gets or sets Indicador Serie
		/// </summary>
		public virtual bool IndicadorSerie { get; set; }

		/// <summary>
		///  Gets or sets Data Inicio Serie
		/// </summary>
		public virtual DateTime DataInicioSerie { get; set; }

		/// <summary>
		///  Gets or sets Cod Unidade Federativa Ferrovia
		/// </summary>
		public virtual string CodUnidadeFederativaFerrovia { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		///  Gets or sets Sd Cod Ctrl
		/// </summary>
		public virtual string CodigoControle { get; set; }

		/// <summary>
		///  Gets or sets Cnpj Ferrovia
		/// </summary>
		public virtual string CnpjFerrovia { get; set; }

		/// <summary>
		///  Gets or sets Cnpj Holding
		/// </summary>
		public virtual string CnpjHolding { get; set; }

		/// <summary>
		/// Letra utilizada como identificação da ferrovia
		/// </summary>
		public virtual string LetraFerrovia { get; set; }
		#endregion PROPRIEDADES
	}
}
