﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Trem.Veiculo.Vagao;

	/// <summary>
	/// Model Nota Nota Fiscal Translogic
	/// </summary>
	public class NotaFiscalTranslogic : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary> 
		///  Gets or sets Num Nota Serie 
		/// </summary>
		public virtual string NumNotaSerie { get; set; }

		/// <summary>
		/// Valor Total da Nota Fiscal
		/// </summary>
		public virtual double ValorNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets Despacho
		/// </summary>
		public virtual DespachoTranslogic DespachoTranslogic { get; set; }

		/// <summary>
		///  Gets or sets Modelo Nota Fiscal
		/// </summary>
		public virtual string ModeloNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets NumeroTif
		/// </summary>
		public virtual string NumeroTif { get; set; }

		/// <summary>
		///  Gets or sets do vagao
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		///  Gets or sets Num Nota
		/// </summary>
		public virtual string NumNota { get; set; }

		/// <summary>
		///  Gets or sets Serie Nota
		/// </summary>
		public virtual string SerieNota { get; set; }

		/// <summary>
		///  Gets or sets Cod Remetente
		/// </summary>
		public virtual string CodRemetente { get; set; }

		/// <summary>
		///  Gets or sets Cod Destinatario
		/// </summary>
		public virtual string CodDestinatario { get; set; }

		/// <summary>
		///  Gets or sets OsSol
		/// </summary>
		public virtual int? OsSol { get; set; }

		/// <summary>
		///  Gets or sets Nota Fiscal Sol
		/// </summary>
		public virtual int? NotaFiscalSol { get; set; }

		/// <summary>
		///  Gets or sets Dt Nota Fiscal
		/// </summary>
		public virtual DateTime DtNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Peso Nota Fiscal
		/// </summary>
		public virtual double? PesoNotaFiscal { get; set; }

		/// <summary>
		///  Gets or sets Id Rementente
		/// </summary>
		public virtual int? IdRementente { get; set; }

		/// <summary>
		///  Gets or sets Uf Remetente
		/// </summary>
		public virtual string UfRemetente { get; set; }

		/// <summary>
		///  Gets or sets Cgc Remetente
		/// </summary>
		public virtual string CgcRemetente { get; set; }

		/// <summary>
		///  Gets or sets Ins Estadual Remetente
		/// </summary>
		public virtual string InsEstadualRemetente { get; set; }

		/// <summary>
		///  Gets or sets Id Destinatario
		/// </summary>
		public virtual int? IdDestinatario { get; set; }

		/// <summary>
		///  Gets or sets UfDestinatario
		/// </summary>
		public virtual string UfDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Cgc Destinatario
		/// </summary>
		public virtual string CgcDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Ins Estadual Destinatario
		/// </summary>
		public virtual string InsEstadualDestinatario { get; set; }

		/// <summary>
		///  Gets or sets Id Usu Insert Nota
		/// </summary>
		public virtual int IdUsuInsertNota { get; set; }

		/// <summary>
		///  Gets or sets Filial Emissora
		/// </summary>
		public virtual string FilialEmissora { get; set; }

		/// <summary>
		///  Gets or sets Tipo Registro
		/// </summary>
		public virtual string TipoRegistro { get; set; }

		/*/// <summary>
		///  Gets or sets Cod Nfe
		/// </summary>
		// public virtual string CodNfe { get; set; }

		/// <summary>
		///  Gets or sets Base Calculo
		/// </summary>
		// public virtual double BaseCalculo { get; set; }

		/// <summary>
		///  Gets or sets Valor Icms
		/// </summary>
		// public virtual double ValorIcms { get; set; }

		/// <summary>
		///  Gets or sets Aliquota
		/// </summary>
		// public virtual double Aliquota { get; set; }

		/// <summary>
		///  Gets or sets Base Calculo Icms Subst
		/// </summary>
		// public virtual double BaseCalculoIcmsSubst { get; set; }

		/// <summary>
		///  Gets or sets Valor Icms Subst
		/// </summary>
		// public virtual double ValorIcmsSubst { get; set; }*/

		/// <summary>
		///  Gets or sets Id Nfe
		/// </summary>
		public virtual int? IdNfe { get; set; }

		/// <summary>
		///  Gets or sets Chave Nota Fiscal Eletronica
		/// </summary>
		public virtual string ChaveNotaFiscalEletronica { get; set; }

        /// <summary>
        ///  Gets or sets Volume Nota Fiscal
        /// </summary>
        public virtual double? VolumeNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Valor Total Nota Fiscal
        /// </summary>
        public virtual double ValorTotalNotaFiscal { get; set; }

        /// <summary>
        ///  Gets or sets Conteiner Nota Fiscal
        /// </summary>
        public virtual string ConteinerNotaFiscal { get; set; }

		#endregion
	}
}
