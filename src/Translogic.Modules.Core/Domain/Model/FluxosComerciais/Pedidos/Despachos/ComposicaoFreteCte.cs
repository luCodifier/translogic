namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using ConhecimentoTransporteEletronico;

	/// <summary>
	/// Classe de composi��o frete CTE
	/// </summary>
	public class ComposicaoFreteCte : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte utilizado na composi��o do frete
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Ferrovia de utiliza��o
		/// </summary>
		public virtual string Ferrovia { get; set; }

		/// <summary>
		/// Valor com o ICMS
		/// </summary>
		public virtual double? ValorComIcms { get; set; }

		/// <summary>
		/// Valor da tonelada com o ICMS
		/// </summary>
		public virtual double? ValorToneladaComIcms { get; set; }

		/// <summary>
		/// Valor sem o ICMS
		/// </summary>
		public virtual double? ValorSemIcms { get; set; }

		/// <summary>
		/// Valor da tonelada sem o ICMS
		/// </summary>
		public virtual double? ValorToneladaSemIcms { get; set; }

		/// <summary>
		/// Valor da tonelada sem o ICMS
		/// </summary>
		public virtual double? Coeficiente { get; set; }

		/// <summary>
		/// Condi��o da tarifa
		/// </summary>
		public virtual string CondicaoTarifa { get; set; }

		/// <summary>
		/// Data de envio para o SAP
		/// </summary>
		public virtual DateTime? DataEnvio { get; set; }
	}
}