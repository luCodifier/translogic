﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Model DetalheCarregamento
	/// </summary>
	public class DetalheCarregamento : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets Carregamento Aloca
		/// </summary>
		public virtual Carregamento Carregamento { get; set; }

		/// <summary>
		/// Gets or sets Vagao Indicado
		/// </summary>
		public virtual Vagao VagaoIndicado { get; set; }

		/// <summary>
		/// Gets or sets Peso Carregado
		/// </summary>
		public virtual double? PesoCarregado { get; set; }

		/// <summary>
		/// Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime? DtCadastro { get; set; }

		/// <summary>
		/// Gets or sets Tonelada util
		/// </summary>
		public virtual double? ToneladaUtil { get; set; }

		/// <summary>
		/// Gets or sets Volume
		/// </summary>
		public virtual double? Volume { get; set; }

		#endregion
	}
}
