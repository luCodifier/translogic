﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Dtos
{
    /// <summary>
    /// Dto de notas fiscal de LDP
    /// </summary>
    public class NotaFiscalLdpDto
    {
        /// <summary>
        /// Id da nota fiscal
        /// </summary>
        public decimal IdNota { get; set; }

        /// <summary>
        /// Chave da nota fiscal eletronica
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Peso total
        /// </summary>
        public double? PesoTotal { get; set; }

        /// <summary>
        /// Placa Cavalo.
        /// </summary>
        public string PlacaCavalo { get; set; }

        /// <summary>
        /// Mensagem de erro ao consultar o simconsultas
        /// </summary>
        public string MensagemErro { get; set; }

        /// <summary>
        /// Indica que foi processado
        /// </summary>
        public bool? Processado { get; set; }
    }
}