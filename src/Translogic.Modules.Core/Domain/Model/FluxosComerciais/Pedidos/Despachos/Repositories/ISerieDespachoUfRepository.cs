namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
	using ALL.Core.AcessoDados;
	using Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

	/// <summary>
	/// Interface de reposit�rio de Serie Despacho Uf
	/// </summary>
	public interface ISerieDespachoUfRepository : IRepository<SerieDespachoUf, int?>
	{
		/// <summary>
		/// Obt�m a empresa da serie despacho Uf pelo id da empresa
		/// </summary>
		/// <param name="empresa">Objeto de Despacho do Translogic</param>
		/// <returns>Retorna a inst�ncia da serie despacho UF</returns>
		SerieDespachoUf ObterPorEmpresa(int empresa);

		/// <summary>
		/// Obt�m a serie despacho uf pela empresa e pela sigla
		/// </summary>
		/// <param name="empresaConcessionaria"> The empresa concessionaria. </param>
		/// <param name="siglaUf"> The sigla do estado. </param>
		/// <returns> Objeto SerieDespachoUf </returns>
		SerieDespachoUf ObterPorEmpresaUf(IEmpresa empresaConcessionaria, string siglaUf);

		/// <summary>
		/// Obt�m a empresa de ferrovia pelo CNPJ definido pelo grupo
		/// </summary>
		/// <param name="cnpj">CNPJ a ser consultado</param>
		/// <returns>Objeto EmpresaFerrovia</returns>
		IEmpresa ObterEmpresaPorCnpjDoGrupo(string cnpj);

		/// <summary>
		/// Obt�m a serie do despacho uf pelo cnpj e pelo uf
		/// </summary>
		/// <param name="cnpj">Cnpj da empresa emitente</param>
		/// <param name="uf">Unidade federativa da empresa</param>
		/// <returns>Retorna a ins�ncia do objeto</returns>
		SerieDespachoUf ObterPorCnpjUf(string cnpj, string uf);
	}
}