namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using ALL.Core.AcessoDados;
    using Via;

    /// <summary>
    /// Interface de Repositorio de LocalTaraPlaca
    /// </summary>
    public interface ILocalTaraPlacaRepository : IRepository<LocalTaraPlaca, int?>
    {
        /// <summary>
        /// Verifica se o local passado est� cadastrado
        /// </summary>
        /// <param name="local">Area operacional</param>
        /// <returns>Valor booleano</returns>
        bool VerificarLocalExiste(IAreaOperacional local);
    }
}