namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ConhecimentoTransporteEletronico;

	/// <summary>
	/// Interface do reposit�rio da composi��o frete cte
	/// </summary>
	public interface IComposicaoFreteCteRepository : IRepository<ComposicaoFreteCte, int?>
	{
	    /// <summary>
	    ///    Insere a ComposicaoFreteCte  sem sess�o
	    /// </summary>
	    /// <param name="entity">Entidade da ComposicaoFreteCte</param>
	    /// <returns>Retorna a entidade inserida </returns>
	    ComposicaoFreteCte InserirSemSessao(ComposicaoFreteCte entity);

	    /// <summary>
	    ///    Atualizar ComposicaoFreteCte  sem sess�o
	    /// </summary>
	    /// <param name="entity">Entidade ComposicaoFreteCte</param>
	    /// <returns>Retorna a entidade atualizada </returns>
	    ComposicaoFreteCte AtualizarSemSessao(ComposicaoFreteCte entity);

	    /// <summary>
	    /// Obt�m a Composi��o Frete Cte pelo Id do CTE
	    /// </summary>
	    /// <param name="cte">Cte da composi��o frete</param>
	    /// <returns>Retorna a lista com as composi��es Frete CTE</returns>
	    IList<ComposicaoFreteCte> ObterComposicaoFreteCtePorCte(Cte cte);
	}
}