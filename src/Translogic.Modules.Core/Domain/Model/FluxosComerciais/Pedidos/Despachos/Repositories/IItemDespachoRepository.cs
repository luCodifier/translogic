namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Dto;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Documentos.Request;
    using Trem.Veiculo.Vagao;

    /// <summary>
	/// Interface de reposit�rio de ItemDespacho
	/// </summary>
	public interface IItemDespachoRepository : IRepository<ItemDespacho, int?>
    {
        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="itensDespachosIds">ids dos itens despachos</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        IList<ItemDespacho> ObterPorIds(List<int> itensDespachosIds);

        /// <summary>
        ///   Obter ItemDespacho pelo ID do Despacho
        /// </summary>
        /// <returns>Item Despacho</returns>
        ItemDespacho ObterPorIdSemEstado(int idDespacho);

        /// <summary>
        ///   Obter ItemDespacho pelo ID do Despacho sem estado
        /// </summary>
        /// <returns>ItemDespacho</returns>
        ItemDespacho ObterItemDespachoPorIdDespachoSemEstado(int idDespacho);

		/// <summary>
		/// Obt�m o item de despacho pelo despacho
		/// </summary>
		/// <param name="despacho">Objeto de Despacho do Translogic</param>
		/// <returns>Retorna a insta�ncia do item de despacho</returns>
    	ItemDespacho ObterItemDespachoPorDespacho(DespachoTranslogic despacho);

        /// <summary>
        /// Obt�m os Itens de Despacho pelo vag�o que possua pedido Vigente
        /// </summary>
        /// <param name="vagao"> Objeto vagao. </param>
        /// <returns>Lista de itens de despacho </returns>
        IList<ItemDespacho> ObterPorVagaoComPedidoVigente(Vagao vagao);

        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="idsItemDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        IList<DespachoTranslogic> ObterDespachosPorItemDespacho(int[] idsItemDespacho);

        /// <summary>
        /// Obt�m o despacho dos itens de despacho
        /// </summary>
        /// <param name="idsItemDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos despachos</returns>
        IList<ItemDespachoDespachoDto> ObterDespachosPorItemDespachoDto(int[] idsItemDespacho);

        /// <summary>
        /// Obt�m o vag�o e o cte dos itens de despacho
        /// </summary>
        /// <param name="itensDespacho">id dos itens despacho</param>
        /// <returns>Retorna a inst�ncia dos dados do ItemDespacho</returns>
        IList<ImpressaoDocumentosVagaoItemDespacho> ObterVagoesCtesItensDespacho(IList<int> itensDespacho);

        /// <summary>
        /// Obt�m o vag�o e o seu item despacho para obter suas documenta��es
        /// </summary>
        /// <param name="composicaoId">Id da composi��o</param>
        /// <returns>Retorna a inst�ncia dos dados do ItemDespacho</returns>
        IList<ImpressaoDocumentosVagaoItemDespacho> ObterVagoesItensDespachoPorComposicao(decimal composicaoId);

        /// <summary>
        /// Obt�m o item de despacho pelo N�mero do Despacho e N�mero do Vag�o
        /// </summary>
        /// <param name="numeroDespacho">N�mero do Despacho</param>
        /// <param name="numeroVagao">N�mero do Vag�o</param>
        /// <returns>Retorna a inst�ncia do item de despacho pelo N�mero do Despacho e N�mero do Vag�o</returns>
        ItemDespacho ObterItemDespachoPorNumeroDespachoNumeroVagao(int numeroDespacho, string numeroVagao);

        /// <summary>
        /// Obt�m o ID do item de despacho pelo ID do Despacho
        /// </summary>
        /// <param name="idDespacho">ID do Despacho</param>
        /// <returns>Retorna o ID do item de despacho pelo ID do Despacho</returns>
        int? ObterIDItemDespachoPorIdDespacho(int idDespacho);
       
    }
}