namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da composi��o frete contrato
	/// </summary>
	public interface IComposicaoFreteContratoRepository : IRepository<ComposicaoFreteContrato, int?>
	{
		/// <summary>
		/// Obt�m pelo n�mero do contrato e pela condi��o tarifa
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da tarifa do contrato</param>
		/// <returns>Retorna a composi��o frete contrato</returns>
		ComposicaoFreteContrato ObterPorNumeroContratoCondicaoTarifa(string numeroContrato, string condicaoTarifa);
		
		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, string letraFerrovia, bool? utilizadoParaCalculo);

		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <param name="condicaoTarifa">Condi��o de tarifa</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, DateTime dataReferencia, string letraFerrovia, bool? utilizadoParaCalculo, string condicaoTarifa);
		
		/// <summary>
		/// Obt�m pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">C�digo do n�mero do contrato</param>
		/// <param name="dataReferencia">Data de referencia</param>
		/// <param name="letraFerrovia">Letra de indica��o da ferrovia</param>
		/// <param name="utilizadoParaCalculo">Flag informando se � utilizado para calculo</param>
		/// <returns>Retorna uma lista com os objetos</returns>
		IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, DateTime dataReferencia, string letraFerrovia, bool? utilizadoParaCalculo);

		/// <summary>
		/// Obt�m a composi��o pelo n�mero de contrato
		/// </summary>
		/// <param name="numeroContrato">N�mero do contrato</param>
		/// <returns>Retorna a lista com as composi��o</returns>
		IList<ComposicaoFreteContrato> ObterComposicaoPorNumeroContrato(string numeroContrato);

		/// <summary>
		/// Obt�m a composi��o frete contrato das ferrovias do grupo pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		IList<ComposicaoFreteContrato> ObterComposicaoFerroviasGrupoPorNumeroContrato(string numeroContrato);

		/// <summary>
		/// Obt�m a composi��o frete contrato das ferrovias do grupo pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da Tarifa</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		IList<ComposicaoFreteContrato> ObterComposicaoFerroviasGrupoPorNumeroContrato(string numeroContrato, string condicaoTarifa);

		/// <summary>
		/// Obtem a composicao frete contrato pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato);

		/// <summary>
		/// Obtem a composicao frete contrato pelo n�mero do contrato
		/// </summary>
		/// <param name="numeroContrato">Numero do contrato</param>
		/// <param name="condicaoTarifa">Condi��o da tarifa</param>
		/// <returns>Retorna uma lista de composi��o frete contrato</returns>
		IList<ComposicaoFreteContrato> ObterPorNumeroContrato(string numeroContrato, string condicaoTarifa);
	}
}