namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Despachos;
    using Dtos;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Trem.Veiculo.Vagao;

    /// <summary>
    /// Interface de reposit�rio de Nota Fiscal Translogic
    /// </summary>
    public interface INotaFiscalTranslogicRepository : IRepository<NotaFiscalTranslogic, int?>
    {
        /// <summary>
        /// Obt�m a nota fiscal do translogic pelo despacho
        /// </summary>
        /// <param name="despachoTranslogic">Despacho do translogic</param>
        /// <returns>Retorna uma lista com as nota fiscal</returns>
        IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespacho(DespachoTranslogic despachoTranslogic);

        /// <summary>
        /// Obt�m as notas fiscais do translogic pelos despachos
        /// </summary>
        /// <param name="despachoIds">Lista de ids de despachos</param>
        /// <returns>Retorna as notas fiscais</returns>
        IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespacho(List<int> despachoIds);

        /// <summary>
        /// Obt�m a nota fiscal do translogic pelo carregamento
        /// </summary>
        /// <param name="carregamento">Carregamento do translogic</param>
        /// <returns>Retorna a nota fiscal</returns>
        IList<NotaFiscalTranslogic> ObterNotaFiscalPorCarregamento(Carregamento carregamento);

        /// <summary>
        /// Obt�m as notas fiscais que devem ser atualizadas as informa��es
        /// </summary>
        /// <returns> Lista de notas fiscais de Dom Pedro </returns>
        IList<NotaFiscalLdpDto> ObterNotasCorrecaoDomPedro();

        /// <summary>
        /// Atualiza a placa e o peso de uma nota fiscal
        /// </summary>
        /// <param name="notaFiscalLdpDto">Id da nota fiscal</param>
        void AtualizarPlacaPeso(NotaFiscalLdpDto notaFiscalLdpDto);

        /// <summary>
        /// Atualiza a lista de notas em lote
        /// </summary>
        /// <param name="listaNotas"> The lista notas. </param>
        void AtualizarEmLotePlacaPeso(List<NotaFiscalLdpDto> listaNotas);

        /// <summary>
        /// Obt�m as notas fiscais do translogic que utilizaram a chave nfe
        /// </summary>
        /// <param name="chaveNfe">Chave da Nfe</param>
        /// <returns>Lista de NotaFiscalTranslogic</returns>
        IList<NotaFiscalTranslogic> ObterPorChaveNfeSemDespachosCancelados(string chaveNfe);

        /// <summary>
        /// Obt�m as notas fiscais pelo despacho e pelo vag�o
        /// </summary>
        /// <param name="despachoTranslogic">Despacho do Translogic</param>
        /// <param name="vagao">Vag�o com as notas fiscais</param>
        /// <returns>Retorna uma lista com as notas fiscais do Translogic</returns>
        IList<NotaFiscalTranslogic> ObterNotaFiscalPorDespachoVagao(DespachoTranslogic despachoTranslogic, Vagao vagao);

        /// <summary>
        /// Obt�m as notas fiscais pelo despacho e pelo vag�o dos CT-es passados como par�metro
        /// </summary>
        /// <param name="ctes">Lista de CT-es</param>
        /// <returns>Retorna uma lista com as notas fiscais do Translogic</returns>
        IList<NotaFiscalTranslogic> ObterNotaFiscalPorCtesDespachoVagao(IList<Cte> ctes);
    }
}
