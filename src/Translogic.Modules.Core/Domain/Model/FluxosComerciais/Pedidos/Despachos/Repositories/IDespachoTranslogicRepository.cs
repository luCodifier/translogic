using System;
using System.Collections.Generic;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Dto;
using Translogic.Modules.Core.Domain.Model.Dto.Despacho;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
	/// <summary>
	///   Interface de reposit�rio de Despacho
	/// </summary>
	public interface IDespachoTranslogicRepository : IRepository<DespachoTranslogic, int?>
	{
		/// <summary>
		///   Obter Dados Despacho
		/// </summary>
		/// <returns>Lista de dto Manutencao Despacho</returns>
		IList<ManutencaoDespachoDto> ObterDadosDespacho();

		/// <summary>
		///   Obter Despacho pelo N�mero do Despacho e S�rie
		/// </summary>
		/// <returns>Lista de Despacho</returns>
        IList<DespachoTranslogic> ObterDespachoPeloNumeroDespachoSerie(int numDespacho, string serie);

	    /// <summary>
	    ///   Obter Despacho pelo N�mero do Despacho e S�rie
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    IList<DespachoTranslogic> ObterDespachoPeloNumeroDespachoSerieIntercambio(int numDespacho, string serie);

	    /// <summary>
	    ///   Obter Despacho pelo ID do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    DespachoTranslogic ObterPorIdSemEstado(int idDespacho);

	    /// <summary>
	    ///   Obter data do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
        DateTime? ObterDataDespachoPorId(int numDespacho);

	    /// <summary>
	    ///   Obter NumeroDespacho do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    decimal ObterNumeroDespachoPorId(int numDespacho);

	    /// <summary>
	    ///   Obter NumDespIntercambio do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterNumDespIntercambioPorId(int numDespacho);

	    /// <summary>
	    ///   Obter NumeroSerieDespacho do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterNumeroSerieDespachoPorId(int numDespacho);

	    /// <summary>
	    ///   Obter SerieDespacho do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterSerieDespachoPorId(int numDespacho);

	    /// <summary>
	    ///   Obter SerieDespachoSdi do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    string ObterSerieDespachoSdiPorId(int numDespacho);

	    /// <summary>
	    ///   Obter NumeroDespachoUf do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterNumeroDespachoUfPorId(int numDespacho);

	    /// <summary>
	    ///   Obter SerieDespachoUf do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterSerieDespachoUfPorId(int numDespacho);

	    /// <summary>
	    ///   Obter SerieDespachoNum do despacho pelo N�mero do Despacho
	    /// </summary>
	    /// <returns>Lista de Despacho</returns>
	    int? ObterSerieDespachoNumPorId(int numDespacho);

        /// <summary>
        /// Obt�m Dto contendo informa��es b�sicas do Despacho para manipula��o e consulta na gera��o do Processo de Seguro
        /// </summary>
        /// <param name="numeroDespacho">N�mero do Despacho</param>
        /// <param name="serieDespacho">S�rie do Despacho</param>
        /// <param name="numeroVagao">N�mero do Vag�o</param>
        /// <returns>Retorna Dto contendo informa��es b�sicas do Despacho para manipula��o e consulta na gera��o do Processo de Seguro</returns>
        DespachoProcessoSeguroDto ObterDespachoProcessoSeguroDtoPorNumeroDespachoNumeroVagao(int numeroDespacho, string serieDespacho, string numeroVagao);


        /// <summary>
        ///   Obter Peso do Carregamento vinculado ao despacho e s�rie
        /// </summary>
        /// <returns>Peso do Carregamento vinculado ao despacho e s�rie</returns>
        Double ObterPesoCarregamento(int numDespacho, string serie);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="idCte"></param>
		/// <returns></returns>
		DateTime? ObterDataDescargaNfe(int? idCte);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="numDespacho"></param>
		/// <param name="serie"></param>
		/// <returns></returns>
		DateTime? ObterDataDescargaNfe(int? numDespacho, string serieDespacho);

		/// <summary>
		/// Verifica se pertence � precifica��o por origem
		/// </summary>
		/// <param name="_idContratoHistorico"></param>
		/// <returns></returns>
		bool VerificaPrecificacaoOrigem(int? _idContratoHistorico);

        string ObterMensagemDataDespacho(int? idCte);
        string ObterMensagemDataDespacho(int? numDespacho, string serieDespacho);

    }
}