namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
	/// Interface de repositório de GrupoDespacho
	/// </summary>
	public interface IGrupoDespachoRepository : IRepository<GrupoDespacho, int?>
	{
	}
}