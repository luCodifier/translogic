namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

    /// <summary>
	/// Interface de repositório de Carregamento
	/// </summary>
	public interface ICarregamentoRepository : IRepository<Carregamento, int?>
    {
        /// <summary>
        /// Retorna id da estacao mae 
        /// </summary>
        /// <param name="despacho">Numero do despacho</param>
        /// <param name="serie">Serie do despacho</param>
        /// <returns>retorna id da estacao mae</returns>
        string ObterEstacaoMaePorDespacho(int despacho, string serie);
    }
}