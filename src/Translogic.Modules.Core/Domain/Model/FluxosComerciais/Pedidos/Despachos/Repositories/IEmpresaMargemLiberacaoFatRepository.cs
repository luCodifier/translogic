namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Interface de reposit�rio de EmpresaMargemLiberacaoFat
	/// </summary>
	public interface IEmpresaMargemLiberacaoFatRepository : IRepository<EmpresaMargemLiberacaoFat, int?>
	{
		/// <summary>
		/// Pesquisa pela emprsa
		/// </summary>
		/// <param name="empresaCliente">Filtro Empresa Cliente</param>
		/// <returns>Retorna a empresa, ou null caso n�o encontre</returns>
		EmpresaMargemLiberacaoFat ObterPorEmpresa(EmpresaCliente empresaCliente);
	}
}