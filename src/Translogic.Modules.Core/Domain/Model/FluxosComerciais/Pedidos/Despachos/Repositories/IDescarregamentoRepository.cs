namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
    using System;
    using System.Collections.Generic;
    using Acesso;
    using Dto;
    using Estrutura;

    /// <summary>
	/// Interface de reposit�rio de Descarregamento
	/// </summary>
	public interface IDescarregamentoRepository
    {
        /// <summary>
        /// Recupera a lista de vag�es a serem descarregados
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do Cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        IList<VagaoDescarregamentoDto> ObterVagoesParaDescarregamento(
            string codAO, string codLinha, string codVagao,
            string codMerc, string codFluxo, string codPedido,
            string identCliente, DateTime dataInicio, DateTime dataFim);

        /// <summary>
        /// Recupera a lista de vag�es a serem descarregados
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do Cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        IList<VagaoDescarregamentoDto> ObterVagoesDescarregados(
            string codAO, string codLinha, string codVagao,
            string codMerc, string codFluxo, string codPedido,
            string identCliente, DateTime dataInicio, DateTime dataFim);

        /// <summary>
        /// Recupera a lista de vag�es n�o descarregados por erros
        /// </summary>
        /// <param name="codAO">C�digo da esta��o para filtro</param>
        /// <param name="codLinha">C�digo da linha</param>
        /// <param name="codVagao">C�digo do vag�o</param>
        /// <param name="codMerc">C�digo da mercadoria</param>
        /// <param name="codFluxo">C�digo do fluxo</param>
        /// <param name="codPedido">C�digo do Pedido</param>
        /// <param name="identCliente">Id do cliente</param>
        /// <param name="dataInicio">Data de in�cio da pesquisa</param>
        /// <param name="dataFim">Data de fim da pesquisa</param>
        /// <returns>Lista de vag�es para descarregamento</returns>
        IList<VagaoDescarregamentoDto> ObterVagoesComErro(
            string codAO, string codLinha, string codVagao, string codMerc,
            string codFluxo, string codPedido, string identCliente,
            DateTime dataInicio, DateTime dataFim);
        
        /// <summary>
        /// Efetuar descarregamento de vag�es com base nas TUs da Tabela de PESAGEM_ESTATICA_FERRO
        /// </summary>
        /// <param name="usuAcao">Usu�rio que efetuou a a��o</param>
        /// <param name="identAO">Identificador da �rea Operacional</param>
        /// <param name="vagaoEmCarreg">Indicador se � para mudar o vag�o para "EM CARREGAMENTO"</param>
        /// <param name="vagoes">Lista de vag�es</param>
        void DescarregarVagoes(Usuario usuAcao, int identAO, bool vagaoEmCarreg, List<VagaoDescarregamentoDto> vagoes);

        /// <summary>
        /// Obter clientes com vag�es em condi��o de descarregamento
        /// </summary>
        /// <param name="idAO">Identificador a Area Operacional</param>
        /// <returns>Lista de empresas com vag�es para descarregamento</returns>
        IList<string> ObterClientesComDescarregamentos(int idAO);
    }
}
