using System.Collections.Generic;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories
{
	/// <summary>
	///   Interface de repositório de Bloqueio de Despacho
	/// </summary>
	public interface IDespachoLocalBloqueioRepository : IRepository<DespachoLocalBloqueio, int>
	{
        IList<DespachoLocalBloqueioDto> ObterTodosBloqueios(DetalhesPaginacao detalhesPaginacao);
	}
}