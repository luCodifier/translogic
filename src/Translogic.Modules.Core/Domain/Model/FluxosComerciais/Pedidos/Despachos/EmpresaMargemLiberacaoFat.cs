namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Model EmpresaMargemLiberacaoFat
	/// </summary>
	public class EmpresaMargemLiberacaoFat : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets 
		/// </summary>
		public virtual EmpresaCliente Empresa { get; set; }

		/// <summary>
		/// Gets or sets 
		/// </summary>
		public virtual double MargemPercentual { get; set; }

		#endregion
	}
}