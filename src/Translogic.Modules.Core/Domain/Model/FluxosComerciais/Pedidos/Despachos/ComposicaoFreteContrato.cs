namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de composi��o frete contrato
	/// </summary>
	public class ComposicaoFreteContrato : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo do N�mero do Contrato
		/// </summary>
		public virtual string NumeroCodigoContrato { get; set; }

		/// <summary>
		/// Descri��o da composi��o frete contrato
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Ferrovia de utiliza��o
		/// </summary>
		public virtual string Ferrovia { get; set; }

		/// <summary>
		/// Valor com o ICMS
		/// </summary>
		public virtual double? ValorComIcms { get; set; }

		/// <summary>
		/// Valor sem o ICMS
		/// </summary>
		public virtual double? ValorSemIcms { get; set; }

		/// <summary>
		/// Flag de utilizado para calculo
		/// </summary>
		public virtual bool UtilizadoParaCalculo { get; set; }

		/// <summary>
		/// Condi��o da tarifa
		/// </summary>
		public virtual string CondicaoTarifa { get; set; }

		/// <summary>
		/// Data de inicio da vigencia da composi��o frete contrato
		/// </summary>
		public virtual DateTime DataInicioVigencia { get; set; }

		/// <summary>
		/// Data de fim de vigencia da composicao frete contrato
		/// </summary>
		public virtual DateTime? DataFimVigencia { get; set; }
	}
}