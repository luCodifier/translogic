﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para Pesagens
    /// </summary>
    public class PesagemEstaticaFerro : EntidadeBase<int>
    {
        /// <summary>
        /// Código da Balança
        /// </summary>
        public virtual string CodigoBalanca { get; set; }

        /// <summary>
        /// Tara do vagão no momento da pesagem
        /// </summary>
        public virtual decimal TaraDoVagao { get; set; }

        /// <summary>
        /// Peso bruto na pesagem
        /// </summary>
        public virtual decimal PesoBruto { get; set; }

        /// <summary>
        /// Código do Vagão
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Data efetiva da pesagem
        /// </summary>
        public virtual DateTime? DataPesagem { get; set; }

        /// <summary>
        /// Data de cadastro da pesagem
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }
    }
}