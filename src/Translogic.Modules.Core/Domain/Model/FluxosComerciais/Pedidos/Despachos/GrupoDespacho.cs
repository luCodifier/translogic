﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Model GrupoDespacho
	/// </summary>
	public class GrupoDespacho : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets DtDespacho
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		#endregion
	}
}
