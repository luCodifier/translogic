﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa uma Condição de frete
	/// </summary>
	public class CondicaoFrete : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da condição de frete
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da condição de frete
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}