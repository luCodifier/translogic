namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa o Tipo de um determinado Pedido
	/// </summary>
	public class TipoPedido : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo do Tipo do Pedido
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descri��o do Tipo do Pedido
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}