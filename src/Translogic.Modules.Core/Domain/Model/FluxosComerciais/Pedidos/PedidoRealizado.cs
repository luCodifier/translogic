﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
	/// Model Pedido Realizado
	/// </summary>
	public class PedidoRealizado : EntidadeBase<int?>
    {
        #region PROPRIEDADES

		/// <summary>
		///  Gets or sets QuantidadeVagaoVazio
		/// </summary>
		public virtual int QuantidadeVagaoVazio { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoCarregadoVcr
		/// </summary>
		public virtual int QuantidadeVagaoCarregadoVcr { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoDespachado
		/// </summary>
		public virtual int QuantidadeVagaoDespachado { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoCarregadoVcg
		/// </summary>
		public virtual int QuantidadeVagaoCarregadoVcg { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeDespachos
		/// </summary>
		public virtual int QuantidadeDespachos { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagoesDescarregados
		/// </summary>
		public virtual decimal QuantidadeVagoesDescarregados { get; set; }

		/// <summary>
		///  Gets or sets Pedido
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		///  Gets or sets DtCadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

      #endregion
    }
}
