namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Classe base de vag�o pedido
	/// </summary>
	public abstract class BaseVagaoPedido : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		///  Gets or sets Vagao
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		///  Gets or sets Pedido
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		///  Gets or sets Dt Carregamento
		/// </summary>
		public virtual DateTime DtCarregamento { get; set; }

		/// <summary>
		///  Gets or sets Dt Descarregamento
		/// </summary>
		public virtual DateTime? DtDescarregamento { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets Indicador Documento
		/// </summary>
		public virtual bool IndicadorDocumento { get; set; }

		/// <summary>
		///  Gets or sets Grupo Fluxo Comercial
		/// </summary>
		public virtual GrupoFluxoComercial GrupoFluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets Fluxo Comercial
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets Carregamento
		/// </summary>
		public virtual Carregamento Carregamento { get; set; }

		/// <summary>
		///  Gets or sets PedidoDerivado
		/// </summary>
		public virtual PedidoDerivado PedidoDerivado { get; set; }

		/// <summary>
		///  Gets or sets Descarregamento
		/// </summary>
		public virtual Descarregamento Descarregamento { get; set; }

		/// <summary>
		///  Gets or sets Desassocia Evento Vagao
		/// </summary>
		public virtual EventoVagao DesassociaEventoVagao { get; set; }

		/// <summary>
		///  Gets or sets Associa Evento Vagao
		/// </summary>
		public virtual EventoVagao AssociaEventoVagao { get; set; }

		/// <summary>
		///  Gets or sets Matricula Insert Pedido
		/// </summary>
		public virtual string MatriculaInsertPedido { get; set; }

		/// <summary>
		///  Gets or sets Matricula Close Pedido
		/// </summary>
		public virtual string MatriculaClosePedido { get; set; }

		/// <summary>
		///  Gets or sets Cod Transacao
		/// </summary>
		public virtual string CodTransacao { get; set; }

		/// <summary>
		///  Gets or sets Cod Trans Descarga
		/// </summary>
		public virtual string CodTransDescarga { get; set; }

        /// <summary>
		///  Gets or sets Vagao Pedido Operacional Fat2.0
		/// </summary>
        public virtual int? VagaoPedidoOperacional { get; set; }

		#endregion
	}
}