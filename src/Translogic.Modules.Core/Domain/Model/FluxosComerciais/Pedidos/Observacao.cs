namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa uma Observação do Pedido
	/// </summary>
	public class Observacao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descrição da Observação
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Resumo da Observação
		/// </summary>
		public virtual string Resumo { get; set; }

		#endregion
	}
}