﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
	/// Model Pedido Transporte
	/// </summary>
	public class PedidoTransporte : EntidadeBase<int?>
    {
        #region PROPRIEDADES

		/// <summary>
		///  Gets or sets IdTipoPedido
		/// </summary>
		public virtual int IdTipoPedido { get; set; }

		/// <summary>
		///  Gets or sets IdGrupoFluxoComercial
		/// </summary>
		public virtual int IdGrupoFluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets IdFluxoComercial
		/// </summary>
		public virtual int IdFluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets IdEmpresaRealiza
		/// </summary>
		public virtual int IdEmpresaRealiza { get; set; }

		/// <summary>
		///  Gets or sets IdEstacaoOrigem
		/// </summary>
		public virtual int IdEstacaoOrigem { get; set; }

		/// <summary>
		///  Gets or sets AreaOperacionalDestFerr
		/// </summary>
		public virtual int AreaOperacionalDestFerr { get; set; }

		/// <summary>
		///  Gets or sets AreaOperacionalOrigext
		/// </summary>
		public virtual int AreaOperacionalOrigext { get; set; }

		/// <summary>
		///  Gets or sets AreaOperacionalDestext
		/// </summary>
		public virtual int AreaOperacionalDestext { get; set; }

		/// <summary>
		///  Gets or sets AreaOperacionalOrigemIntercambio
		/// </summary>
		public virtual int AreaOperacionalOrigemIntercambio { get; set; }

		/// <summary>
		///  Gets or sets AreaOperacionalDestinoIntercambio
		/// </summary>
		public virtual int AreaOperacionalDestinoIntercambio { get; set; }

		/// <summary>
		///  Gets or sets IdUnidadeNegocio
		/// </summary>
		public virtual int IdUnidadeNegocio { get; set; }

		/// <summary>
		///  Gets or sets IdMercadoria
		/// </summary>
		public virtual int IdMercadoria { get; set; }

		/// <summary>
		///  Gets or sets RedefinePedido
		/// </summary>
		public virtual char RedefinePedido { get; set; }

		/// <summary>
		///  Gets or sets IdEmpresaSolicita
		/// </summary>
		public virtual int IdEmpresaSolicita { get; set; }

		/// <summary>
		///  Gets or sets IdEmpresaRemete
		/// </summary>
		public virtual int IdEmpresaRemete { get; set; }

		/// <summary>
		///  Gets or sets IdEmpresaDestina
		/// </summary>
		public virtual int IdEmpresaDestina { get; set; }

		/// <summary>
		///  Gets or sets DataCriacao
		/// </summary>
		public virtual DateTime DataCriacao { get; set; }

		/// <summary>
		///  Gets or sets PesoPedido
		/// </summary>
		public virtual double PesoPedido { get; set; }

		/// <summary>
		///  Gets or sets DataFimDescarregamento
		/// </summary>
		public virtual DateTime DataFimDescarregamento { get; set; }

		/// <summary>
		///  Gets or sets TipoSituacaoOrdemServico
		/// </summary>
		public virtual string TipoSituacaoOrdemServico { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagao
		/// </summary>
		public virtual int QuantidadeVagao { get; set; }

		/// <summary>
		///  Gets or sets DataInicioCarregamento
		/// </summary>
		public virtual DateTime DataInicioCarregamento { get; set; }

		// public virtual double PesoPedido { get; set; }

		/// <summary>
		///  Gets or sets TempoCarregamento
		/// </summary>
		public virtual int TempoCarregamento { get; set; }

		/// <summary>
		///  Gets or sets CodigoPedido
		/// </summary>
		public virtual string CodigoPedido { get; set; }

		/// <summary>
		///  Gets or sets ClasseVagao
		/// </summary>
		public virtual int ClasseVagao { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeMinimaVagao
		/// </summary>
		public virtual int QuantidadeMinimaVagao { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoVazio
		/// </summary>
		public virtual int QuantidadeVagaoVazio { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoCarregadoVcr
		/// </summary>
		public virtual int QuantidadeVagaoCarregadoVcr { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoDespachado
		/// </summary>
		public virtual int QuantidadeVagaoDespachado { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagaoCarregadoVcg
		/// </summary>
		public virtual int QuantidadeVagaoCarregadoVcg { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeDespachos
		/// </summary>
		public virtual int QuantidadeDespachos { get; set; }

		/// <summary>
		///  Gets or sets QuantidadeVagoesDescarregados
		/// </summary>
        public virtual decimal QuantidadeVagoesDescarregados { get; set; }

		/// <summary>
		///  Gets or sets DtCadastroPedidoRealizado
		/// </summary>
		public virtual DateTime DtCadastroPedidoRealizado { get; set; }

		/// <summary>
		///  Gets or sets IndPesagem (Pesagem Origem ou Destino)
		/// </summary>
		public virtual char IndPesagem { get; set; }

		/// <summary>
		///  Gets or sets IndPago (Pago na Origem ou Destino)
		/// </summary>
		public virtual char IndPago { get; set; }

		/// <summary>
		///  Gets or sets NumFluxo
		/// </summary>
		public virtual string NumFluxo { get; set; }
		
		/// <summary>
		///  Gets or sets NumContrato
		/// </summary>
		public virtual string NumContrato { get; set; }

		/// <summary>
		///  Gets or sets CodigoCondicaoFrete
		/// </summary>
		public virtual string CodigoCondicaoFrete { get; set; }

		/// <summary>
		///  Gets or sets DescricaoCondicaoFrete
		/// </summary>
		public virtual string DescricaoCondicaoFrete { get; set; }

		/// <summary>
		///  Gets or sets DistanciaKmAll
		/// </summary>
		public virtual int DistanciaKmAll { get; set; }

		// public virtual DateTime DataFimDescarregamento { get; set; }

		/// <summary>
		///  Gets or sets UnidadeMedida
		/// </summary>
		public virtual string UnidadeMedida { get; set; }

		/// <summary>
		///  Gets or sets FrotaVagao
		/// </summary>
		public virtual int FrotaVagao { get; set; }

		/// <summary>
		///  Gets or sets SituacaoPedido
		/// </summary>
		public virtual int SituacaoPedido { get; set; }

		/// <summary>
		///  Gets or sets PrioridadePedido
		/// </summary>
		public virtual int PrioridadePedido { get; set; }

		/// <summary>
		///  Gets or sets DataInicioVigencia
		/// </summary>
		public virtual DateTime DataInicioVigencia { get; set; }

		/// <summary>
		///  Gets or sets DataFimVigencia
		/// </summary>
		public virtual DateTime DataFimVigencia { get; set; }

		/// <summary>
		///  Gets or sets SequenciaPedidoEmergencial
		/// </summary>
		public virtual int SequenciaPedidoEmergencial { get; set; }

		/// <summary>
		///  Gets or sets TempoDescarregamento
		/// </summary>
		public virtual int TempoDescarregamento { get; set; }

		/// <summary>
		///  Gets or sets TransitTimeGrupoFluxo
		/// </summary>
		public virtual int TransitTimeGrupoFluxo { get; set; }

		/// <summary>
		///  Gets or sets IndPedidoEmergencia
		/// </summary>
		public virtual char IndPedidoEmergencia { get; set; }

      #endregion
    }
}
