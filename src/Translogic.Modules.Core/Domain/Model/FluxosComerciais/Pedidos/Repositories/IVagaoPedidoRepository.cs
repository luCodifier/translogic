namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	/// <summary>
	/// Interface de reposit�rio de VagaoPedido
	/// </summary>
	public interface IVagaoPedidoRepository : IRepository<VagaoPedido, int?>
    {
        /// <summary>
        /// Obt�m Vagao Pedido por id do Pedido
        /// </summary>
        /// <param name="pedidoId">Id do Pedido</param>
        /// <returns>Retorna Vagao Pedido</returns>
        VagaoPedido ObterVagaoPedidoPorPedido(int pedidoId);

        /// <summary>
        /// Obt�m Vagao Pedido por id do Pedido Derivado
        /// </summary>
        /// <param name="pedidoDerivadoId">Id do Pedido Derivado</param>
        /// <returns>Retorna Vagao Pedido</returns>
        VagaoPedido ObterVagaoPedidoPorPedidoDerivado(int pedidoDerivadoId);

    	/// <summary>
    	/// Obt�m Vagao Pedido por id do Vafao
    	/// </summary>
    	/// <param name="idVagao">Id vagao do translogic</param>
    	/// <returns>Retorna Vagao Pedido</returns>
    	VagaoPedido ObterVagaoPedidoPorVagao(int? idVagao);

		/// <summary>
		/// Obt�m lista de Vagao Pedido por ComposicaoVagao
		/// </summary>
		/// <param name="composicaoVagao">Objeto composicaoVagao</param>
		/// <returns>Retorna Vagao Pedido</returns>
		IList<VagaoPedido> ObterVagoesPedidosPorComposicaoVagao(ComposicaoVagao composicaoVagao);
    }
}