namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;

    /// <summary>
	/// Interface de repositório de PedidoDerivado
	/// </summary>
	public interface IPedidoDerivadoRepository : IRepository<PedidoDerivado, int?>
    {
        /// <summary>
        /// Obtem os pedidos por ids
        /// </summary>
        /// <param name="ids">Ids dos pedidos derivados</param>
        /// <returns>Retorna a lista de pedidos derivados</returns>
        IList<PedidoDerivado> ObterPorIds(List<int> ids);
    }
}