namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;

    /// <summary>
	/// Interface de repositório de Pedido Transporte
	/// </summary>
	public interface IPedidoTransporteRepository : IRepository<PedidoTransporte, int?>
	{
	}
}