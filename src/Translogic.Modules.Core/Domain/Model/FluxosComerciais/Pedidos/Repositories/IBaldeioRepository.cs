namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto.Baldeio;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
    using System.Collections.Generic;

    /// <summary>
    /// Interface de repositório de Baldeio
    /// </summary>
    public interface IBaldeioRepository : IRepository<Baldeio, int?>
    {
        /// <summary>
        /// Retorna lista de registro  com carta de baldeio anexo conforme filtro informado pelo usuario
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="vagaoCedente"></param>
        /// <param name="vagaoRecebedor"></param>
        /// <returns></returns>
        ResultadoPaginado<BaldeioDto> ObterConsultaBaldeio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string dataInicial, string dataFinal, string local, string vagaoCedente, string vagaoRecebedor);
        
        /// <summary>
        /// Retorna lista baldeio para exportação
        /// </summary>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <param name="local"></param>
        /// <param name="vagaoCedente"></param>
        /// <param name="vagaoRecebedor"></param>
        /// <returns> Lista Baleio</returns>
        IEnumerable<BaldeioDto> ObterConsultaBaldeioExportar(string dataInicial, string dataFinal, string local, string vagaoCedente, string vagaoRecebedor);
    
    
    }
}