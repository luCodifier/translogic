namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Repositories
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

    /// <summary>
	/// Interface de reposit�rio de VagaoPedido
	/// </summary>
	public interface IVagaoPedidoVigenteRepository : IRepository<VagaoPedidoVigente, int?>
    {
    	/// <summary>
    	/// Obt�m lista de pedidos do Vagao por id
    	/// </summary>
    	/// <param name="idVagao">Id vagao do translogic</param>
    	/// <returns>Retorna lista de Vagao Pedido</returns>
		IList<VagaoPedidoVigente> ObterVagaoPedidoPorVagao(int? idVagao);
    }
}