﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
	/// Model Pedido Derivado
	/// </summary>
	public class PedidoDerivado : EntidadeBase<int?>
    {
        #region PROPRIEDADES

		/// <summary>
		///  Gets or sets Pedido
		/// </summary>
		public virtual Pedido Pedido { get; set; }

		/// <summary>
		///  Gets or sets Fluxo Comercial
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets Grupo Fluxo Comercial
		/// </summary>
		public virtual GrupoFluxoComercial GrupoFluxoComercial { get; set; }

		/// <summary>
		///  Gets or sets Dsc Pedido
		/// </summary>
		public virtual string DscPedido { get; set; }

		/// <summary>
		///  Gets or sets Data Prevista Carga
		/// </summary>
		public virtual DateTime DataPrevistaCarga { get; set; }

		/// <summary>
		///  Gets or sets Qtd Vagoes Carregados VGC
		/// </summary>
		public virtual int QtdVagoesCarregadosVgc { get; set; }

		/// <summary>
		///  Gets or sets Qtd Vagoes Carregados VCG
		/// </summary>
		public virtual int QtdVagoesCarregadosVcg { get; set; }

		/// <summary>
		///  Gets or sets Dt Cadastro
		/// </summary>
		public virtual DateTime DtCadastro { get; set; }

		/// <summary>
		///  Gets or sets Associa Fluxo
		/// </summary>
		public virtual AssociaFluxo AssociaFluxo { get; set; }

		/// <summary>
		///  Gets or sets Tipo Pedido
		/// </summary>
		public virtual TipoPedido TipoPedido { get; set; }

		/// <summary>
		///  Gets or sets Empresa Realiza
		/// </summary>
		public virtual Empresa EmpresaRealiza { get; set; }

		/// <summary>
		///  Gets or sets Empresa Solicita
		/// </summary>
		public virtual Empresa EmpresaSolicita { get; set; }

		/// <summary>
		///  Gets or sets Empresa Remete
		/// </summary>
		public virtual Empresa EmpresaRemete { get; set; }

		/// <summary>
		///  Gets or sets Empresa Destina
		/// </summary>
		public virtual Empresa EmpresaDestina { get; set; }

		/// <summary>
		///  Gets or sets Area Operacional Destext
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacionalDestext { get; set; }

		/// <summary>
		///  Gets or sets Area Operacional Origext
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacionalOrigext { get; set; }

		/// <summary>
		///  Gets or sets Area Operacional Destferr
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacionalDestferr { get; set; }

		/// <summary>
		///  Gets or sets Area Operacional Origferr
		/// </summary>
		public virtual AreaOperacional<EstacaoMae> AreaOperacionalOrigferr { get; set; }

		/// <summary>
		///  Gets or sets Mercadoria
		/// </summary>
		public virtual Mercadoria Mercadoria { get; set; }

		/// <summary>
		///  Gets or sets Classe Vagao
		/// </summary>
		public virtual ClasseVagao ClasseVagao { get; set; }

		/// <summary>
		///  Gets or sets Unidade Negocio
		/// </summary>
		public virtual UnidadeNegocio UnidadeNegocio { get; set; }

		/// <summary>
		///  Gets or sets Data Criacao Pedido
		/// </summary>
		public virtual DateTime DataCriacaoPedido { get; set; }

		/// <summary>
		///  Gets or sets Sit Pedido
		/// </summary>
		public virtual string SitPedido { get; set; }

		/// <summary>
		///  Gets or sets Qtd Vagoes
		/// </summary>
		public virtual int QtdVagoes { get; set; }

		/// <summary>
		///  Gets or sets Qtd Tonelada
		/// </summary>
		public virtual double QtdTonelada { get; set; }

		/// <summary>
		///  Gets or sets Qtd Despacho
		/// </summary>
		public virtual int QtdDespacho { get; set; }

		/// <summary>
		///  Gets or sets Qtd Descarga
		/// </summary>
		public virtual decimal QtdDescarga { get; set; }

		/// <summary>
		///  Gets or sets Numero Fluxo
		/// </summary>
		public virtual string NumeroFluxo { get; set; }

		/// <summary>
		///  Gets or sets Numero Contato
		/// </summary>
		public virtual string NumeroContato { get; set; }

		/// <summary>
		///  Gets or sets Cod Unid Negocio 
		/// </summary>
		public virtual string CodUnidNegocio { get; set; }

		/// <summary>
		///  Gets or sets Ind Frota
		/// </summary>
		public virtual int IndFrota { get; set; }

      #endregion
    }
}
