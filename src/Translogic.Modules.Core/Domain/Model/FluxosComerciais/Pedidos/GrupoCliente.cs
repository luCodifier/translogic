﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa um grupo de clientes
	/// </summary>
	public class GrupoCliente : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código do Grupo
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do Grupo
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}