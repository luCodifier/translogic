namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Diversos;

    /// <summary>
	/// Representa o controle de altera��o do pedido
	/// </summary>
	public class ControleAlteracao : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Data de altera��o
		/// </summary>
		public virtual DateTime DataAlteracao { get; set; }

		/// <summary>
		/// Motivo da altera��o
		/// </summary>
		public virtual IMotivoALL Motivo { get; set; }

		/// <summary>
		/// Usuario que efetuou a altera��o
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}