﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using Acesso;
    using ALL.Core.Dominio;
    using Diversos;
    using Estrutura;
    using FluxosComerciais;
    using Trem.OrdemServico;
    using Trem.Veiculo.Vagao;
    using Via;

    /// <summary>
	/// Representa um pedido
	/// </summary>
	public class Pedido : EntidadeBase<int?>
	{
		#region PROPRIEDADES

        /// <summary>
        /// Código do pedido
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Número do pedido
        /// </summary>
        public virtual int? Numero { get; set; }

        /// <summary>
        /// Data do Pedido
        /// </summary>
        public virtual DateTime DataPedido { get; set; }

        /// <summary>
        /// Situação em que a OS do pedido se encontra
        /// </summary>
        public virtual SituacaoOrdemServico Situacao { get; set; }

        /// <summary>
        /// Tipo do Pedido - <see cref="TipoPedido"/>
        /// </summary>
        public virtual TipoPedido Tipo { get; set; }

        /// <summary>
        /// Empresa solicitante do pedido
        /// </summary>
        public virtual EmpresaFerrovia Empresa { get; set; }

        /// <summary>
        /// Classe de vagão - <see cref="Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao.ClasseVagao"/>
        /// </summary>
        public virtual ClasseVagao ClasseVagao { get; set; }

        /// <summary>
        /// Fluxo comercial do pedido - <see cref="FluxoComercial"/>
        /// </summary>
        public virtual FluxoComercial Fluxo { get; set; }

        /// <summary>
        /// Estação de Origem do Pedido
        /// </summary>
        public virtual EstacaoMae Origem { get; set; }

        /// <summary>
        /// Estação de Destino do Pedido
        /// </summary>
        public virtual EstacaoMae Destino { get; set; }

        /// <summary>
        /// Mercadoria do Pedido - <see cref="FluxosComerciais.Mercadoria"/>
        /// </summary>
        public virtual Mercadoria Mercadoria { get; set; }

        /// <summary>
        /// Grupo de Fluxo do Pedido - <see cref="GrupoFluxoComercial"/>
        /// </summary>
        public virtual GrupoFluxoComercial GrupoFluxo { get; set; }

        /// <summary>
        /// Frota dos vagões do pedido - <see cref="FrotaVagao"/>
        /// </summary>
        public virtual FrotaVagao FrotaVagao { get; set; }

        /// <summary>
        /// Quantidade alterada
        /// </summary>
        public virtual int QuantidadeAlterada { get; set; }

        /// <summary>
        /// Saldo de vagões
        /// </summary>
        public virtual int SaldoVagao { get; set; }

        /// <summary>
        /// Observação do pedido - <see cref="FluxosComerciais.Pedidos.Observacao"/>
        /// </summary>
        public virtual Observacao Observacao { get; set; }

        /// <summary>
        /// Usuário responsável pelo pedido - <see cref="Usuario"/>
        /// </summary>
        public virtual Usuario UsuarioResponsavel { get; set; }

        /// <summary>
        /// Controle de Alteração - <see cref="ControleAlteracao"/>
        /// </summary>
        public virtual ControleAlteracao ControleAlteracao { get; set; }

        /// <summary>
        /// Quantidade de vagões carregados no pedido
        /// </summary>
        public virtual int QuantidadeVagoesCarregados { get; set; }

        /// <summary>
        /// Quantidade de vagões vazios no pedido
        /// </summary>
        public virtual int QuantidadeVagoesVazios { get; set; }

        /// <summary>
        /// Empresa ferrovia - <see cref="EmpresaFerrovia"/>
        /// </summary>
        public virtual EmpresaFerrovia EmpresaFerroviaria { get; set; }

         /// <summary>
         /// Estação de Origem interno
         /// </summary>
         public virtual EstacaoMae OrigemInterno { get; set; }

         /// <summary>
         /// Estação de Destino Interno
         /// </summary>
         public virtual EstacaoMae DestinoInterno { get; set; }

         /// <summary>
         /// Data de início de tração
         /// </summary>
         public virtual DateTime DataInicioTracao { get; set; }

         /// <summary>
         /// Data de término de tração
         /// </summary>
         public virtual DateTime DataFimTracao { get; set; }

         /// <summary>
         /// Quantidade de Maquinistas
         /// </summary>
         public virtual int QuantidadeMaquinistas { get; set; }

         /// <summary>
         /// Código MCT
         /// </summary>
         public virtual string CodigoMCT { get; set; }

         /// <summary>
         /// Código EOT
         /// </summary>
         public virtual string CodigoEOT { get; set; }

         /// <summary>
         /// Motivo de cancelamento
         /// </summary>
         public virtual IMotivoALL MotivoCancelamento { get; set; }

         /// <summary>
         /// Data de criação do pedido
         /// </summary>
         public virtual DateTime DataCriacao { get; set; }

         /// <summary>
         /// Peso do pedido
         /// </summary>
         public virtual double PesoPedido { get; set; }

         /// <summary>
         /// Terminal de origem
         /// </summary>
         public virtual EstacaoMae TerminalOrigem { get; set; }

         /// <summary>
         /// Terminal de Destino
         /// </summary>
         public virtual EstacaoMae TerminalDestino { get; set; }

         /// <summary>
         /// Motivo de criação do pedido
         /// </summary>
         public virtual IMotivoALL MotivoCriacao { get; set; }

         /// <summary>
          /// Data de início de carregamento
          /// </summary>
          public virtual DateTime DataInicioCarregamento { get; set; }

          /// <summary>
          /// Data de término de carregamento
          /// </summary>
          public virtual DateTime DataFimCarregamento { get; set; }

          /// <summary>
          /// Data de início do descarregamento
          /// </summary>
          public virtual DateTime DataInicioDescarregamento { get; set; }

          /// <summary>
          /// Data de término do descarregamento
          /// </summary>
          public virtual DateTime DataFimDescarregamento { get; set; }

          /// <summary>
          /// Quantidade de mercadoria
          /// </summary>
          public virtual int QuantidadeMercadoria { get; set; }

          /// <summary>
          /// Utilizado na redestinação do vagão
          /// </summary>
          public virtual Pedido PedidoRedestinacao { get; set; }

          /// <summary>
          /// Utilizado em pedidos derivados
          /// </summary>
          public virtual Pedido PedidoOrigem { get; set; }

          /// <summary>
          /// Número do pedido do cliente
          /// </summary>
          public virtual string NumeroPedidoCliente { get; set; }

        /// <summary>
        /// Contrato do pedido - <see cref="Contrato"/>
        /// </summary>
        public virtual Contrato Contrato { get; set; }

        /// <summary>
        /// Prioridade do pedido
        /// </summary>
        public virtual int PrioridadePedido { get; set; }

        /// <summary>
        /// Data de início de vigência do pedido
        /// </summary>
        public virtual DateTime DataInicioVigencia { get; set; }

        /// <summary>
        /// Data de término da vigencia do pedido
        /// </summary>
        public virtual DateTime DataFimVigencia { get; set; }

        /// <summary>
        /// Unidade de negocio do pedido - <see cref="UnidadeNegocio"/>
        /// </summary>
        public virtual UnidadeNegocio UnidadeNegocio { get; set; }

        /// <summary>
        /// Sequencia do pedido
        /// </summary>
        /// <remarks>
        /// apenas para pedidos emergenciais
        /// </remarks>
        public virtual int SequenciaPedidoEmergencial { get; set; }

        /// <summary>
        /// Tempo de carregamento
        /// </summary>
        /// <remarks>
        /// Não se aplica a pedidos derivados
        /// </remarks>
        public virtual double TempoCarregamento { get; set; }

        /// <summary>
        /// Tempo de descarregamento
        /// </summary>
        /// <remarks>
        /// Não se aplica a pedidos derivados
        /// </remarks>
        public virtual double TempoDescarregamento { get; set; }

         /// <summary>
        /// Transit time do grupo de fluxo
        /// </summary>
        public virtual double TransitTimeGrupoFluxo { get; set; }

         /// <summary>
         /// Indica se o pedido é normal ou de emergência
         /// </summary>
         public virtual PedidoEmergenciaEnum IndPedidoEmergencia { get; set; }

        /// <summary>
        /// Indica se o pedido é derivado
        /// </summary>
        public virtual bool IndDerivado { get; set; }

        #endregion
    }
}