﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos
{
    using System;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Via;

    /// <summary>
	/// Model Baldeio
	/// </summary>
	public class Baldeio : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Data do Baldeio
        /// </summary>
        public virtual DateTime DtBaldeio { get; set; }
        
        /// <summary>
        /// Quantidade Baldeada
        /// </summary>
        public virtual double QtdBaldeio { get; set; }

        /// <summary>
        /// Data de Cadastro do Baldeio
        /// </summary>
        public virtual DateTime DtCadastro { get; set; }

        /// <summary>
        /// Id do Pedido
        /// </summary>
        public virtual int IdPedido { get; set; }

        /// <summary>
        /// Area Operacional
        /// </summary>
        public virtual AreaOperacional<EstacaoMae> AreaOperacionalAbriga { get; set; }

        /// <summary>
        /// Quantidade Restante para Baldeio
        /// </summary>
        public virtual double QtdRestBaldeio { get; set; }

        /// <summary>
        /// Observação do Baldeio
        /// </summary>
        public virtual string Observacao { get; set; }

        /// <summary>
        /// Volume baldeado
        /// </summary>
        public virtual double VolumeBaldeado { get; set; }

        /// <summary>
        /// Pedido Derivado 
        /// </summary>
        public virtual PedidoDerivado PedidoDerivadoRegistra { get; set; }

      #endregion
    }
}
