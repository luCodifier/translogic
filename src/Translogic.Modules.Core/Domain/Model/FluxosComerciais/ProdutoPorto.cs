﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
    using ALL.Core.Dominio;

    /// <summary>
	/// Representa um Produto que pode ser transportado
	/// </summary>
	public class ProdutoPorto : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código do ProdutoPorto
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do ProdutoPorto
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}