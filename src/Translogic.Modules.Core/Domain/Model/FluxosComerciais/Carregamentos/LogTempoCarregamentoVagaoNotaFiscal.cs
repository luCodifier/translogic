﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos
{
	using System;
	using System.Globalization;
	using ALL.Core.Dominio;

	/// <summary>
	/// Log do tempo de carregamento da nota fiscal do vagão 
	/// </summary>
	public class LogTempoCarregamentoVagaoNotaFiscal : EntidadeBase<int>
	{
		private string _dataInicioString;
		private string _dataTerminoString;

		/// <summary>
		/// Log do tempo de carregamento do vagao
		/// </summary>
		public virtual LogTempoCarregamentoVagao LogTempoCarregamentoVagao { get; set; }

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de termino
		/// </summary>
		public virtual DateTime DataTermino { get; set; }

		/// <summary>
		/// Gets or sets ChaveNfe.
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Gets or sets Erro.
		/// </summary>
		public virtual bool Erro { get; set; }

		/// <summary>
		/// Gets or sets MensagemExibida.
		/// </summary>
		public virtual string MensagemExibida { get; set; }

		/// <summary>
		/// Gets or sets ProblemaEncontrado.
		/// </summary>
		public virtual string ProblemaEncontrado { get; set; }

		/// <summary>
		/// Data de Cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Gets or sets DataInicioString.
		/// </summary>
		public virtual string DataInicioString
		{
			get
			{
				return _dataInicioString;
			}

			set
			{
				_dataInicioString = value;
				DataInicio = DateTime.ParseExact(_dataInicioString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Gets or sets DataTerminoString.
		/// </summary>
		public virtual string DataTerminoString
		{
			get
			{
				return _dataTerminoString;
			}

			set
			{
				_dataTerminoString = value;
				DataTermino = DateTime.ParseExact(_dataTerminoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}
	}
}