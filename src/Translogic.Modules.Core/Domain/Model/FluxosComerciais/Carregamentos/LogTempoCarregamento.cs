﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using ALL.Core.Dominio;
	using Dto;

	/// <summary>
	/// Log do tempo de carregamento
	/// </summary>
	public class LogTempoCarregamento : EntidadeBase<int>
	{
		private string _dataInicioString;
		private string _dataTerminoString;

		/// <summary>
		/// Indica se foi despachado com sucesso
		/// </summary>
		public virtual bool IndDespachadoSucesso { get; set; }
		
		/// <summary>
		/// Indica se foi despachado com sucesso
		/// </summary>
		public virtual string MensagemErroDespacho { get; set; }

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime DataInicioPersistenciaDespacho { get; set; }

		/// <summary>
		/// Data de termino
		/// </summary>
		public virtual DateTime DataTerminoPersistenciaDespacho { get; set; }

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de termino
		/// </summary>
		public virtual DateTime DataTermino { get; set; }

		/// <summary>
		/// Gets or sets CodigoFluxo.
		/// </summary>
		public virtual string CodigoFluxo { get; set; }

		/// <summary>
		/// Gets or sets CodigoUsuario.
		/// </summary>
		public virtual string CodigoUsuario { get; set; }

		/// <summary>
		/// Id da origem do fluxo
		/// </summary>
		public virtual int IdOrigem { get; set; }

		/// <summary>
		/// Id do destino do fluxo
		/// </summary>
		public virtual int IdDestino { get; set; }

		/// <summary>
		/// Id da mercadoria
		/// </summary>
		public virtual int IdMercadoria { get; set; }

		/// <summary>
		/// Lista de vagões do despacho
		/// </summary>
		public virtual IList<LogTempoCarregamentoVagao> ListaVagoes { get; set; }

		/// <summary>
		/// Data de Cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Gets or sets DataInicioString.
		/// </summary>
		public virtual string DataInicioString
		{
			get
			{
				return _dataInicioString;
			}

			set
			{
				_dataInicioString = value;
				DataInicio = DateTime.ParseExact(_dataInicioString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Gets or sets DataTerminoString.
		/// </summary>
		public virtual string DataTerminoString
		{
			get
			{
				return _dataTerminoString;
			}

			set
			{
				_dataTerminoString = value;
				DataTermino = DateTime.ParseExact(_dataTerminoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}
	}
}