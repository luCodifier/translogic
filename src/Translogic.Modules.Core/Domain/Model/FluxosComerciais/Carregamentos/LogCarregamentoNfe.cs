﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos
{
	using System;
	using System.Globalization;
	using ALL.Core.Dominio;

	/// <summary>
	/// Log do carregamento quando busca a NF-e
	/// </summary>
	public class LogCarregamentoNfe : EntidadeBase<int>
	{
		private string _dataInicioString;
		private string _dataTerminoString;

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Codigo do Usuario
		/// </summary>
		public virtual string CodigoUsuario { get; set; }

		/// <summary>
		/// Mensagem Exibida na tela
		/// </summary>
		public virtual string MensagemExibidaTela { get; set; }

		/// <summary>
		/// Chave NF-e
		/// </summary>
		public virtual string ChaveNfe { get; set; }

		/// <summary>
		/// Data de inicio da pesquisa
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de término da pesquisa
		/// </summary>
		public virtual DateTime DataTermino { get; set; }

		/// <summary>
		/// Problema encontrado
		/// </summary>
		public virtual string ProblemaEncontrado { get; set; }

		/// <summary>
		/// Código do fluxo comercial
		/// </summary>
		public virtual string CodigoFluxo { get; set; }

		/// <summary>
		/// Gets or sets DataInicioString.
		/// </summary>
		public virtual string DataInicioString
		{
			get
			{
				return _dataInicioString;
			}

			set
			{
				_dataInicioString = value;
				DataInicio = DateTime.ParseExact(_dataInicioString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Gets or sets DataTerminoString.
		/// </summary>
		public virtual string DataTerminoString
		{
			get
			{
				return _dataTerminoString;
			}

			set
			{
				_dataTerminoString = value;
				DataTermino = DateTime.ParseExact(_dataTerminoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}
	}
}