﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using ALL.Core.Dominio;
	using Dto;

	/// <summary>
	/// Log do tempo de carregamento do vagão
	/// </summary>
	public class LogTempoCarregamentoVagao : EntidadeBase<int>
	{
		private string _dataInicioString;
		private string _dataTerminoString;

		/// <summary>
		/// Log do tempo de carregamento
		/// </summary>
		public virtual LogTempoCarregamento LogTempoCarregamento { get; set; }

		/// <summary>
		/// Data de inicio
		/// </summary>
		public virtual DateTime DataInicio { get; set; }

		/// <summary>
		/// Data de termino
		/// </summary>
		public virtual DateTime DataTermino { get; set; }

		/// <summary>
		/// Gets or sets CodigoFluxo.
		/// </summary>
		public virtual string CodigoVagao { get; set; }

		/// <summary>
		/// Data de Cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Lista de Notas fiscais do vagão
		/// </summary>
		public virtual IList<LogTempoCarregamentoVagaoNotaFiscal> ListaNotas { get; set; }

		/// <summary>
		/// Gets or sets DataInicioString.
		/// </summary>
		public virtual string DataInicioString
		{
			get
			{
				return _dataInicioString;
			}

			set
			{
				_dataInicioString = value;
				DataInicio = DateTime.ParseExact(_dataInicioString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}

		/// <summary>
		/// Gets or sets DataTerminoString.
		/// </summary>
		public virtual string DataTerminoString
		{
			get
			{
				return _dataTerminoString;
			}

			set
			{
				_dataTerminoString = value;
				DataTermino = DateTime.ParseExact(_dataTerminoString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
			}
		}
	}
}