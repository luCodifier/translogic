namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório <see cref="LogTempoCarregamento"/>
	/// </summary>
	public interface ILogTempoCarregamentoRepository : IRepository<LogTempoCarregamento, int>
	{	
	}
}