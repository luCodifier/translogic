namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório <see cref="LogTempoCarregamentoVagaoNotaFiscal"/>
	/// </summary>
	public interface ILogTempoCarregamentoVagaoNotaFiscalRepository : IRepository<LogTempoCarregamentoVagaoNotaFiscal, int>
	{	
	}
}