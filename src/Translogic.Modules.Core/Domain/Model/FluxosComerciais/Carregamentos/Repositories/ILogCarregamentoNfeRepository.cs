namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório <see cref="LogCarregamentoNfe"/>
	/// </summary>
	public interface ILogCarregamentoNfeRepository : IRepository<LogCarregamentoNfe, int>
	{	
	}
}