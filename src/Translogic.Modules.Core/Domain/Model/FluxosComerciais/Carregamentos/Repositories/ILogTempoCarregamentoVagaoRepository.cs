namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório <see cref="LogTempoCarregamentoVagao"/>
	/// </summary>
	public interface ILogTempoCarregamentoVagaoRepository : IRepository<LogTempoCarregamentoVagao, int>
	{	
	}
}