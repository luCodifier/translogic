﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de arvore do MDF-e
	/// </summary>
	public class MdfeArvore : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto Cte Raiz
		/// </summary>
		public virtual Mdfe MdfeRaiz { get; set; }

		/// <summary>
		/// Objeto Cte Pai
		/// </summary>
		public virtual Mdfe MdfePai { get; set; }

		/// <summary>
		/// Objeto Cte
		/// </summary>
		public virtual Mdfe MdfeFilho { get; set; }

		/// <summary>
		/// Sequencia dos elementos da arvore
		/// </summary>
		public virtual int Sequencia { get; set; } 
	}
}