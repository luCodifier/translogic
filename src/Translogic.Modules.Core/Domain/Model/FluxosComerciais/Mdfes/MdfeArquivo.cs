namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de armazenamento do arquivo pdf impresso
	/// </summary>
	public class MdfeArquivo : EntidadeBase<int?>
	{
		/// <summary>
		/// Dados do documento (arquivo pdf)
		/// </summary>
		public virtual byte[] ArquivoPdf { get; set; }

		/// <summary>
		/// Tamanho do arquivo pdf
		/// </summary>
		public virtual int? TamanhoPdf { get; set; }

		/// <summary>
		/// Data e hora de cadastramento no pooling
		/// </summary>
		public virtual DateTime? DataHora { get; set; }
	}
}