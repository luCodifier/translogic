﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de pooling de envio do MDF-e
	/// </summary>
	public class MdfeEnvioPooling : EntidadeBase<int?>
	{
		/// <summary>
		/// Data e hora de cadastramento no pooling
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Nome do servidor
		/// </summary>
		public virtual string HostName { get; set; }
	}
}