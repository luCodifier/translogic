﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de recebimento do retorno da Config
	/// </summary>
	public class MdfeRecebimentoPooling : EntidadeBase<int?>
	{
		/// <summary>
		/// Data e hora de cadastramento no pooling
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// HostName do servidor que será processado
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Identificador da lista da config
		/// </summary>
		public virtual int IdListaConfig { get; set; }
	}
}