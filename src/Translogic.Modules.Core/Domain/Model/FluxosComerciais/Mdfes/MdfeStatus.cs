﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de status do MDF-e
	/// </summary>
	public class MdfeStatus : EntidadeBase<int?>
	{
		/// <summary>
		/// Objeto da classe MDF-e
		/// </summary>
		public virtual Mdfe Mdfe { get; set; }

		/// <summary>
		/// O usuário de status do Cte
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Status de retorno do Cte
		/// </summary>
		public virtual MdfeStatusRetorno MdfeStatusRetorno { get; set; }

		/// <summary>
		/// Data e hora do status
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// XML de retorno do status do Cte
		/// </summary>
		public virtual string XmlRetorno { get; set; }
	}
}