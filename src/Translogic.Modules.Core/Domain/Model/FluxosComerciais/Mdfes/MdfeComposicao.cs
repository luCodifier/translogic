﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
    using ALL.Core.Dominio;
    using ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Trem.Veiculo.Vagao;

	/// <summary>
	/// Composição do MDF-e
	/// </summary>
	public class MdfeComposicao : EntidadeBase<int?>
	{
		/// <summary>
		/// MDF-e de referencia
		/// </summary>
		public virtual Mdfe Mdfe { get; set; }

		/// <summary>
		/// Cte integrante da composição
		/// </summary>
		public virtual Cte Cte { get; set; }

		/// <summary>
		/// Vagão integrante da composição do MDF-e
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		/// Código do vagão
		/// </summary>
		public virtual string CodigoVagao { get; set; }

		/// <summary>
		/// Sequencia da composição
		/// </summary>
		public virtual int Sequencia { get; set; }

		/// <summary>
		/// Situação atual do Ct-e
		/// </summary>
		public virtual SituacaoCteEnum SituacaoCte { get; set; }

        /// <summary>
        /// Despacho da composição
        /// </summary>
        public virtual DespachoTranslogic Despacho { get; set; }
	}
}