﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Realiza o Log dos usuários que realizam impressão
    /// </summary>
    public class LogImpressaoRecibo : EntidadeBase<int?>
    {
        /// <summary>
        /// Id do usuário que realizou a impressão
        /// </summary>
        public int UsuarioId { get; set; }

        /// <summary>
        /// Vagão que o usuário selecionou para impressão
        /// </summary>
        public int? VagaoId { get; set; }

        /// <summary>
        /// Trem que o usuário selecionou para impressão
        /// </summary>
        public int? TremId { get; set; }

        /// <summary>
        /// Empresa selecionada na impressão
        /// </summary>
        public int? EmpresaId { get; set; }

        /// <summary>
        /// Data e Hora da Gravação do Log
        /// </summary>
        public DateTime DataHora { get; set; }
    }
}