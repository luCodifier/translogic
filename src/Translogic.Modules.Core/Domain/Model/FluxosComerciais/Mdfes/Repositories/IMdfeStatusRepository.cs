﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using Acesso;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do status de retorno
	/// </summary>
	public interface IMdfeStatusRepository : IRepository<MdfeStatus, int?>
	{
		/// <summary>
		/// Insere o status do Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe de referencia</param>
		/// <param name="usuario">usuario de inserção</param>
		/// <param name="mdfeStatusRetorno">Status de retorno do mdfe</param>
		void InserirStatus(Mdfe mdfe, Usuario usuario, MdfeStatusRetorno mdfeStatusRetorno);

		/// <summary>
		/// Insere o status do Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe de referencia</param>
		/// <param name="usuario">usuario de inserção</param>
		/// <param name="mdfeStatusRetorno">Status de retorno do mdfe</param>
		/// <param name="xmlRetorno">Xml de Retorno</param>
		void InserirStatus(Mdfe mdfe, Usuario usuario, MdfeStatusRetorno mdfeStatusRetorno, string xmlRetorno);

		/// <summary>
		/// Obtém os status do Mdfe pelo Mdfe
		/// </summary>
		/// <param name="mdfe">Mdfe para obter o status</param>
		/// <returns>Retorna uma lista com os status do CTE</returns>
		IList<MdfeStatus> ObterPorMdfe(Mdfe mdfe);
	}
}