﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repósitório de recebimento do MDF-e da Config
	/// </summary>
	public interface IMdfeInterfaceRecebimentoConfigRepository : IRepository<MdfeInterfaceRecebimentoConfig, int?>
	{
		/// <summary>
		/// Obtem os itens da interface que nao foram processados (que não estão no pooling de recebimento)
		/// </summary>
		/// <param name="indAmbienteSefaz">Indica qual ambiente da SEFAZ 1-producao / 2-homologação</param>
		/// <returns>Retorna uma lista dos itens que não estão no pooling (interface de recebimento)</returns>
		IList<MdfeInterfaceRecebimentoConfig> ObterNaoProcessados(int indAmbienteSefaz);

		/// <summary>
		/// Remove os cte da interface que ja foi processados
		/// </summary>
		/// <param name="mdfe">MDF-es para serem removidos da lista</param>
		void RemoverPorMdfe(Mdfe mdfe);
	}
}