﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da classe MDF-e arvore
	/// </summary>
	public interface IMdfeArvoreRepository : IRepository<MdfeArvore, int?>
	{
		/// <summary>
		/// Obtém a arvore de MDfe pelo MDfe filho
		/// </summary>
		/// <param name="mdfeFilho">MDfe filho que está contido na arvore</param>
		/// <returns>Retorna a lista com os elementos da arvore</returns>
		IList<MdfeArvore> ObterArvorePorMdfeFilho(Mdfe mdfeFilho);
	}
}