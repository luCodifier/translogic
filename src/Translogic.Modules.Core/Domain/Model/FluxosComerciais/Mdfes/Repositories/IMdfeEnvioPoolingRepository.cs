﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do pooling de envio 
	/// </summary>
	public interface IMdfeEnvioPoolingRepository : IRepository<MdfeEnvioPooling, int?>
	{
		/// <summary>
		/// Limpa a sessão
		/// </summary>
		void ClearSession();

		/// <summary>
		/// Remove os MDF-es da lista
		/// </summary>
		/// <param name="listaMdfe">Lista com os MDF-es a serem removidos</param>
		void RemoverPorLista(IList<Mdfe> listaMdfe);
	}
}