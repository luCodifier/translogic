﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Diversos;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface do repositório do MDF-e
    /// </summary>
    public interface IMdfeRepository : IRepository<Mdfe, int?>
    {
        /// <summary>
        /// Obtém MDF-e por status de processamento
        /// </summary>
        /// <param name="indAmbienteSefaz">Indica qual o ambiente Sefaz</param>
        /// <returns>Retorna uma lista com os MDF-es</returns>
        IList<Mdfe> ObterStatusProcessamentoEnvio(int indAmbienteSefaz);

        /// <summary>
        /// Atualiza a situacao do MDF-e por Stateless
        /// </summary>
        /// <param name="mdfe">Mdfe para ser atualizado</param>
        void AtualizarSituacao(Mdfe mdfe);

        /// <summary>
        /// Retorna o ultimo mdfe vigente para o trem
        /// </summary>
        /// <param name="idTrem">Id do trem</param>
        /// <returns>Retorna o id do mdfe</returns>
        int ObterUltimoMdfePorTrem(int idTrem);

        /// <summary>
        /// Obtém a lista dos MDF-es que estão no pooling de envio
        /// </summary>
        /// <param name="hostName">Host do processamento</param>
        /// <param name="segundos">Timer em segundos</param>
        /// <returns>Lista dos MDF-e</returns>
        IList<Mdfe> ObterListaEnvioPorHost(string hostName, int segundos);

        /// <summary>
        /// Lista as Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        ResultadoPaginado<TremMdfeDto> Listar(DetalhesPaginacao detalhesPaginacao, string chaveMdfe, string numero, string serie, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, int idTrem);

        /// <summary>
        /// Busca para exportar
        /// </summary>
        /// <param name="chave">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixo">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="situacao">Situação do trem</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        IList<TremMdfeDto> ExportarExcel(string chave, string numero, string serie, string prefixo, int? os,
            DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao);

        /// <summary>
        /// Lista as Mdfes e os trens sem Mdfe
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="chaveMdfe">chave da mdfe</param>
        /// <param name="numero">numero da mdfe</param>
        /// <param name="serie">serie da mdfe</param>
        /// <param name="prefixoTrem">prefixo do trem</param>
        /// <param name="os">os do trem</param>
        /// <param name="dataInicial">data inicial do intervalo</param>
        /// <param name="dataFinal">data final do intervalo</param>
        /// <param name="origem">origem do trem</param>
        /// <param name="destino">destino do trem</param>
        /// <param name="situacao">Situação do trem</param>
        /// <param name="idTrem">Id do trem selecionado</param>
        /// <returns>resultado paginado de trens com mdfe</returns>
        ResultadoPaginado<TremMdfeDto> ListarSemMdfe(DetalhesPaginacao detalhesPaginacao, string chaveMdfe, string numero, string serie, string prefixoTrem, int? os, DateTime? dataInicial, DateTime? dataFinal, string origem, string destino, string situacao, int idTrem);

        /// <summary>
        /// Lista Log de Mdfes
        /// </summary>
        /// <param name="detalhesPaginacao">detalhes da paginacao</param>
        /// <param name="tipoLog">Tipo de Log</param>
        /// <param name="nomeServico">Nome do Serviço</param>
        /// <returns>resultado paginado de logs de mdfe</returns>
        ResultadoPaginado<LogPkgMdfeDto> ListarLogPkg(DetalhesPaginacao detalhesPaginacao, string tipoLog, string nomeServico);

        /// <summary>
        /// Gera uma Mdfe forçada pela tela 1253
        /// </summary>
        /// <param name="os">Número da OS</param>
        bool GerarMdfeForcada(int os);

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado</returns>
        IList<MdfeDto> ListarDetalhesDaMdfe(int idTrem);

        /// <summary>
        /// Carrega os detalhes da MDFe
        /// </summary>
        /// <param name="paginacao">Detalhes da paginacao</param>
        /// <param name="idTrem">id do trem</param>
        /// <param name="semaforo">Semaforo que foi selecionado para o filtro</param>
        /// <param name="siglaUfSemaforo">Sigla da UF selecionada no semáforo</param>
        /// <param name="periodoHorasErroSemaforo">O período de tempo selecionado no semáforo</param>
        /// <returns>resultado paginado</returns>
        ResultadoPaginado<MdfeDto> ListarDetalhesDaMdfePaginado(DetalhesPaginacao paginacao, int idTrem, string semaforo, string siglaUfSemaforo, HorasErroEnum periodoHorasErroSemaforo);

        /// <summary>
        /// Listar mdfes por despacho
        /// </summary>
        /// <param name="paginacao">Detalhes da paginacao</param>
        /// <param name="idDespacho">Id do despacho selecionado</param>
        /// <returns>Lista de Mdfe do Despacho</returns>
        ResultadoPaginado<MdfeDto> ListarPorDespacho(DetalhesPaginacao paginacao, int idDespacho);

        /// <summary>
        /// Obtém os status de uma Mdfe
        /// </summary>
        /// <param name="idMdfe">Id da Mdfe que se deseja obter os status</param>
        /// <returns>Lista de Status da Mdfe</returns>
        IList<MdfeStatus> ObterStatusMdfePorIdMdfe(int idMdfe);

        /// <summary>
        /// Lista as Mdfes com erro para mostrar no semaforo
        /// </summary>
        /// <returns>Mdfes com erro</returns>
        IList<SemaforoMdfeDto> ObterMdfesErro();

        /// <summary>
        /// Lista o ID do  MDF-e pelo ID do despacho
        /// </summary>
        /// <param name="idDespacho">Id do Despacho</param>
        /// <returns>O ID do MDF-e</returns>
        int ObterIdMdfePorDespacho(int idDespacho);

        /// <summary>
        /// Gera uma Mdfe Virtual ser vinculo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        bool GerarMdfeVirtualSemVinculo(CteVirtuaisSemVinculoMdfeDto obj);

    }
}