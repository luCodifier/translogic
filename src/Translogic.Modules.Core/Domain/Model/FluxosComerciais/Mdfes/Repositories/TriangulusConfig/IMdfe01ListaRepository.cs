namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Interface do repositório da classe MDF-e Lista
	/// </summary>
	public interface IMdfe01ListaRepository : IRepository<Mdfe01Lista, int>
	{
		/// <summary>
		/// Obtém o item da fila de processamento por filial, numero cte e serie cte
		/// </summary>
		/// <param name="idFilial">Identificador da filial</param>
		/// <param name="numeroCte">Numero do Cte</param>
		/// <param name="serieCte">Serie do Cte</param>
		/// <param name="tipo">Tipo da acao na lista</param>
		/// <param name="ascendente">Tipo da classificação ascendente</param>
		/// <returns>Retorna o objeto da fila de processamento</returns>
		Mdfe01Lista ObterListaFilaProcessamento(int idFilial, int numeroCte, string serieCte, int tipo, bool ascendente);
	}
}