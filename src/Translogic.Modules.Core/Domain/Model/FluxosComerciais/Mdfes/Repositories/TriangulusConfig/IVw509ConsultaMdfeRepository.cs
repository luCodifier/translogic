namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Interface do reposit�rio do IVw509ConsultaMdfeRepository
	/// </summary>
	public interface IVw509ConsultaMdfeRepository : IRepository<Vw509ConsultaMdfe, int>
	{
		/// <summary>
		/// Obtem o retorno do processamento pelo id da lista de fila de processamento
		/// </summary>
		/// <param name="idLista">Identificador da lista de processamento do Cte</param>
		/// <returns>Retorna o CteProcessado</returns>
		Vw509ConsultaMdfe ObterRetornoPorIdLista(int idLista);

        /// <summary>
        /// Obt�m o retorno do processamento 
        /// </summary>
        /// <param name="numero">N�mero do Cte</param>
        /// <param name="idFilial">Filial do Cte</param>
        /// <param name="serie">Serie do Cte</param>
        /// <returns>Retorna o mdfe processado</returns>
	    Vw509ConsultaMdfe ObterMdfeJaAutorizadoComDiferencaNaChaveDeAcesso(int numero, int idFilial, int serie);
	}
}