namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Interface do reposit�rio - Tabela utilizada para informa��es dos Vag�es (1-n)
	/// </summary>
	public interface IMdfe10FerroVagRepository : IRepository<Mdfe10FerroVag, int?>
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do mdfe</param>
		/// <param name="serie"> Serie do mdfe</param>
		void Remover(int idFilial, int numero, string serie);
	}
}