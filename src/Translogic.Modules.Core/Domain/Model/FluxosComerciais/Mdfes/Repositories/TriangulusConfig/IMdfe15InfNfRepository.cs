namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories.TriangulusConfig
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	/// <summary>
	/// Interface do repositório - Tabela utilizada para informações de Nota Fiscal Papel (mod 1 e 1A) (0-2000)
	/// </summary>
	public interface IMdfe15InfNfRepository : IRepository<Mdfe15InfNf, int?>
	{
		/// <summary>
		///  Remove por campos chaves
		/// </summary>
		/// <param name="idFilial"> Numero da filial</param>
		/// <param name="numero"> Numero do mdfe</param>
		/// <param name="serie"> Serie do mdfe</param>
		void Remover(int idFilial, int numero, string serie);
	}
}