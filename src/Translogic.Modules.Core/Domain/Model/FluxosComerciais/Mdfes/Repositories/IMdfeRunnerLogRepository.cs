﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Text;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da classe MdfeRunnerLog
	/// </summary>
	public interface IMdfeRunnerLogRepository : IRepository<MdfeRunnerLog, int?>
	{
		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(string hostName, string nomeServico, string mensagem);

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="builder">String builder do log</param>
		void InserirLogInfo(string hostName, string nomeServico, StringBuilder builder);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		void InserirLogErro(string hostName, string nomeServico, string mensagem);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="hostName">Nome do host do serviço</param>
		/// <param name="nomeServico">Nome do serviço que está sendo logado</param>
		/// <param name="builder">String builderdo log</param>
		void InserirLogErro(string hostName, string nomeServico, StringBuilder builder);		 
	}
}