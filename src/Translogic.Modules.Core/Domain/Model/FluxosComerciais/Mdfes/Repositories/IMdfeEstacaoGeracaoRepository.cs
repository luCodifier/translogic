﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório da estação de geração do MDF-e
	/// </summary>
	public interface IMdfeEstacaoGeracaoRepository : IRepository<MdfeEstacaoGeracao, int?>
	{
	}
}