namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface do reposit�rio do CteArquivo
    /// </summary>
    public interface IMdfeArquivoRepository : IRepository<MdfeArquivo, int?>
    {
        /// <summary>
        /// Insere um mdfe arquivo
        /// </summary>
        /// <param name="mdfe">mdfe de refer�ncia</param>
        /// <param name="nomeArquivoPdf">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto mdfeArquivo</returns>
        MdfeArquivo Inserir(Mdfe mdfe, string nomeArquivoPdf);

        /// <summary>
        /// Insere um mdfe arquivo (PDF)
        /// </summary>
        /// <param name="mdfe">mdfe de refer�ncia</param>
        /// <param name="nomeArquivo">Nome do arquivo pdf</param>
        /// <returns>Retorna o objeto mdfeArquivo</returns>
        MdfeArquivo InserirOuAtualizarPdf(Mdfe mdfe, string nomeArquivo);

        /// <summary>
        /// Obtem as informa��es do MDF-e n�o aprovado pela Sefaz
        /// </summary>
        /// <param name="osId">ID da OS</param>
        /// <param name="recebedorId">ID do Terminal Recebedor</param>
        /// <param name="refat">Indicador se � refat ou normal. Se for refat, observa o hist�rico.</param>
        /// <returns>As informa��es contidas no MDF-e</returns>
        ICollection<ManifestoCargaDto> ObterManifestoCargaPorOsRecebedor(decimal osId, decimal recebedorId, bool refat);
    }
}
