﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Text;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório do MDF-e log
	/// </summary>
	public interface IMdfeLogRepository : IRepository<MdfeLog, int?>
	{
		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(Mdfe mdfe, string mensagem);

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="nomeServico">Nome do serviço ou método que está sendo logado</param>
		/// <param name="mensagem">Mensagem de log</param>
		void InserirLogInfo(Mdfe mdfe, string nomeServico, string mensagem);

		/// <summary>
		/// Insere o log de informação
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="builder">String builder do log</param>
		void InserirLogInfo(Mdfe mdfe, StringBuilder builder);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		void InserirLogErro(Mdfe mdfe, string mensagem);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="nomeServico">Nome do servico ou metodo que esta sendo logado</param>
		/// <param name="mensagem">Mensagem do log</param>
		void InserirLogErro(Mdfe mdfe, string nomeServico, string mensagem);

		/// <summary>
		/// Insere o log de erro
		/// </summary>
		/// <param name="mdfe">mdfe a ser logado</param>
		/// <param name="builder">String builderdo log</param>
		void InserirLogErro(Mdfe mdfe, StringBuilder builder);
	}
}