﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;

	/// <summary>
	/// Interface de repositório da classe de recebimento do retorno do MDF-e
	/// </summary>
	public interface IMdfeRecebimentoPoolingRepository : IRepository<MdfeRecebimentoPooling, int?>
	{
		/// <summary>
		/// Obtém os itens do pooling para serem processado o retorno
		/// </summary>
		/// <param name="hostName">Nome do host para pegar os dados</param>
		/// <returns>Retorna uma lista dos itens que estão no pooling</returns>
		IList<MdfeRecebimentoPooling> ObterListaRecebimentoPorHost(string hostName);

		/// <summary>
		/// Remove os itens da lista
		/// </summary>
		/// <param name="lista">Lista com os item para ser removido</param>
		void RemoverPorLista(IList<MdfeRecebimentoPooling> lista);

		/// <summary>
		/// Limpa a sessao
		/// </summary>
		void ClearSession();
	}
}