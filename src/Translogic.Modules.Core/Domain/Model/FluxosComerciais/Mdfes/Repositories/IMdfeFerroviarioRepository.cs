﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
    using System.Collections.Generic;
    using WebServices.MdfeFerroviario;
    using WebServices.MdfeFerroviario.Outputs;

    /// <summary>
    /// Interface de acesso aos dados de mdfe 
    /// </summary>
    public interface IMdfeFerroviarioRepository
    {
        /// <summary>
        /// Consulta Dados Mdfe para o ws
        /// </summary>
        /// <param name="request">Objeto de input EletronicManifestInformationRailRequest</param>
        /// <returns>Lista de detalhes dos mdfes</returns>
        List<EletronicManifestInformationRailOutputDetail> InformationMdfe(EletronicManifestInformationRailRequest request);
    }
}