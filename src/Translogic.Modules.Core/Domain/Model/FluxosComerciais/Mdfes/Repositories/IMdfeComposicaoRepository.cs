﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    /// <summary>
    /// Interface do repositório da composição do MDF-e
    /// </summary>
    public interface IMdfeComposicaoRepository : IRepository<MdfeComposicao, int?>
    {
        /// <summary>
        /// Obtem a composicao pelo mdfe
        /// </summary>
        /// <param name="mdfe">Objeto Mdfe</param>
        /// <returns>Retorna a composicao do mdfe</returns>
        IList<MdfeComposicao> ObterPorMdfe(Mdfe mdfe);

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idMdfe">id da Mdfe</param>
        /// <returns>resultado paginado</returns>
        IList<MdfeComposicaoDto> ListarDetalhesDaComposicao(int idMdfe);

        /// <summary>
        /// Carrega os detalhes da composicao por Mdfe para o Cte informado
        /// </summary>
        /// <param name="idCte">Id do Cte informado</param>
        /// <returns>resultado paginado</returns>
        int ObterIdMdfeDaComposicaoPorCte(int idCte);

        /// <summary>
        /// Carrega os detalhes da composicao por Mdfe para o Cte informado
        /// </summary>
        /// <param name="idCte">Id do Cte informado</param>
        /// <returns>resultado paginado</returns>
        int ObterIdMdfeAutDaComposicaoPorCte(int idCte);

        /// <summary>
        /// Carrega os fluxos da composicao
        /// </summary>
        /// <param name="idTrem">id do trem</param>
        /// <returns>resultado paginado</returns>
        IList<FluxosMdfeDto> ListarFluxosDaComposicao(int idTrem);

        /// <summary>
        /// Carrega os detalhes da última composição do trem
        /// </summary>
        /// <param name="idTrem">id do Trem</param>
        /// <param name="idEmpresa">Id da empresa</param>
        /// <param name="vagoes">Lista de vagões para filtro de impressão</param>
        /// <returns>resultado paginado</returns>
        IList<MdfeComposicaoDto> DetalhesUltComposicaoTrem(int idTrem, int idEmpresa, List<int> vagoes);
        
        /// <summary>
        /// Carrega os fluxos da composicao
        /// </summary>
        /// <param name="dataInicial">Data inicial para filtro</param>
        /// <param name="dataFinal">Data final para filtro</param>
        /// <param name="codigoEstacao">codigo da estação</param>
        /// <param name="linha">id do trem</param>
        /// <returns>resultado paginado</returns>
        IList<FluxosMdfeDto> ListarFluxosPorPatio(DateTime dataInicial, DateTime dataFinal, string codigoEstacao, int? linha);

        /// <summary>
        /// Carrega os detalhes da composicao
        /// </summary>
        /// <param name="idEmpresa">Id da empresa recebedora</param>
        /// <param name="dataInicial">Data inicial do período</param>
        /// <param name="dataFinal">Data final do período</param>
        /// <param name="estacao">Código da estação</param>
        /// <param name="idLinha">Id da Linha</param>
        /// <returns>resultado paginado</returns>
        IList<MdfeComposicaoDto> ListarDetalhesDaComposicaoPorPatio(int idEmpresa, DateTime dataInicial, DateTime dataFinal, string estacao, int? idLinha);

        /// <summary>
        /// Busca a UF dos CTES da composição do MDFE
        /// </summary>
        /// <param name="idMdfe">ID MDFE</param>
        /// <returns>UF retornada</returns>
        string BuscarUfDosCteComposicaoMdfe(int idMdfe, decimal idTrem);
    }
}