﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do respositório da classe da versão do MDF-e
	/// </summary>
	public interface IMdfeVersaoRepository : IRepository<MdfeVersao, int?>
	{
	}
}