namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio do MDF-e
	/// </summary>
	public interface IMdfeInterfacePdfConfigRepository : IRepository<MdfeInterfacePdfConfig, int?>
	{
		/// <summary>
		/// Obt�m lista dos itens da interface pelo Host
		/// </summary>
		/// <param name="hostName">Nome do Servidor</param>
		/// <returns>Retorna a lista dos itens da interface</returns>
		IList<MdfeInterfacePdfConfig> ObterListaInterfacePorHost(string hostName);
	}
}