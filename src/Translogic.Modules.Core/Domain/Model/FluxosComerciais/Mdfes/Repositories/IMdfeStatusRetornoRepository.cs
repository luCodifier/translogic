﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Repositório da classe de status de retorno do MDF-e
	/// </summary>
	public interface IMdfeStatusRetornoRepository : IRepository<MdfeStatusRetorno, int?>
	{
	}
}