namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories
{
	using ALL.Core.AcessoDados;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Interface da classe MdfeSerieEmpresaUfRepository 
	/// </summary>
	public interface IMdfeSerieEmpresaUfRepository : IRepository<MdfeSerieEmpresaUf, int>
	{
		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="empresa">Objeto da empresa</param>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="MdfeSerieEmpresaUf"/></returns>
		MdfeSerieEmpresaUf ObterPorEmpresaUf(IEmpresa empresa, string uf);

		/// <summary>
		/// Obt�m por empresa e estado
		/// </summary>
		/// <param name="uf">Estado da empresa</param>
		/// <returns>Objeto <see cref="MdfeSerieEmpresaUf"/></returns>
		MdfeSerieEmpresaUf ObterPorUf(string uf);
	}
}