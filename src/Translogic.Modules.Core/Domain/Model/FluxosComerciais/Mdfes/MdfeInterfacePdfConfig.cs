namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de interface de recebimento da config
	/// </summary>
	public class MdfeInterfacePdfConfig : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte da interface de recebimento
		/// </summary>
		public virtual Mdfe Mdfe { get; set; }

		/// <summary>
		/// Chave do Cte
		/// </summary>
		public virtual string Chave { get; set; }

		/// <summary>
		/// Nome do Servidor
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado na fila de recebimento
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Data e hora da ultima leitura dos dados
		/// </summary>
		public virtual DateTime DataUltimaLeitura { get; set; }
	}
}