namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;
	using Translogic.Modules.Core.Domain.Model.Estrutura;

	/// <summary>
	/// Classe MdfeSerieEmpresaUf
	/// </summary>
	public class MdfeSerieEmpresaUf : EntidadeBase<int>
	{
		/// <summary>
		/// Gets or sets Uf.
		/// </summary>
		public virtual string Uf { get; set; }

		/// <summary>
		/// Gets or sets Empresa.
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		/// Serie atual do MDFE
		/// </summary>
		public virtual string SerieAtual { get; set; }
		
		/// <summary>
		/// N�mero atual do MDF-e
		/// </summary>
		public virtual string NumeroAtual { get; set; }

		/// <summary>
		/// Data de Inicio de vig�ncia
		/// </summary>
		public virtual DateTime DataInicioVigencia { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		/// <summary>
		/// Indica se vai ser enviado para producao da sefaz
		/// </summary>
		public virtual bool ProducaoSefaz { get; set; }
	}
}