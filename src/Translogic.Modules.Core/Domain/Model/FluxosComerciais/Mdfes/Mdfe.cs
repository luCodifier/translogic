﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using Diversos.Mdfe;
	using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem;
	using Translogic.Modules.Core.Domain.Model.Via;
	using Trem;
	using Trem.OrdemServico;

	/// <summary>
	/// Classe do MDF-e
	/// </summary>
	public class Mdfe : EntidadeBase<int?>
	{
		/// <summary>
		/// Trem do Translogic no MDF-e
		/// </summary>
		public virtual Trem Trem { get; set; }

		/// <summary>
		/// Movimentação do TremTrem do Translogic no MDF-e
		/// </summary>
		public virtual MovimentacaoTrem MovimentacaoTrem { get; set; }
		
		/// <summary>
		/// Identificador da area operacional que esta gerando o MDF-e
		/// </summary>
		public virtual IAreaOperacional Origem { get; set; }

		/// <summary>
		/// Composição ativa do Trem
		/// </summary>
		public virtual Composicao Composicao { get; set; }

		/// <summary>
		/// Ordem de serviço do Trem do Translogic
		/// </summary>
		public virtual OrdemServico OrdemServico { get; set; }

		/// <summary>
		/// Prefixo do Trem do Translogic
		/// </summary>
		public virtual string PrefixoTrem { get; set; }

		/// <summary>
		/// Chave de acesso do MDF-e
		/// </summary>
		public virtual string Chave { get; set; }

		/// <summary>
		/// Numero do protocolo de autorização do MDF-e
		/// </summary>
		public virtual string NumeroProtocolo { get; set; }

		/// <summary>
		/// Data do protocolo de autorização do MDF-e
		/// </summary>
		public virtual DateTime? DataRecibo { get; set; }

		/// <summary>
		/// Numero do MDF-e
		/// </summary>
		public virtual string Numero { get; set; }

		/// <summary>
		/// Data e hora de criação do MDF-e
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Ambiente Sefaz 1 - Produção e 2 - Homologação
		/// </summary>
		public virtual int AmbienteSefaz { get; set; }
		
		/// <summary>
		/// Situação atual do MDF-e
		/// </summary>
		public virtual SituacaoMdfeEnum SituacaoAtual { get; set; }

		/// <summary>
		/// Versão do MDF-e (Sefaz)
		/// </summary>
		public virtual MdfeVersao Versao { get; set; }

		/// <summary>
		/// XML de autorização do MDF-e
		/// </summary>
		public virtual string XmlAutorizacao { get; set; }

		/// <summary>
		/// XML de cancelamento do MDF-e
		/// </summary>
		public virtual string XmlCancelamento { get; set; }

		/// <summary>
		/// Cnpj Ferrovia
		/// </summary>
		public virtual string CnpjFerrovia { get; set; }

		/// <summary>
		/// Sigla Uf Ferrovia
		/// </summary>
		public virtual string SiglaUfFerrovia { get; set; }

		/// <summary>
		/// Serie do Mdf-e
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		/// Lista com os dados da composicao
		/// </summary>
		public virtual ICollection<MdfeComposicao> ListaComposicao { get; set; }

        /// <summary>
        /// Lista com os status do Mdfe
        /// </summary>
        public virtual ICollection<MdfeStatus> ListaStatus { get; set; }
	}
}