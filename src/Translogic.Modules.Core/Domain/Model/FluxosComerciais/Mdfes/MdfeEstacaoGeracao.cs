﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Classe de estação de geração dos MDF-es
	/// </summary>
	public class MdfeEstacaoGeracao : EntidadeBase<int?>
	{
		/// <summary>
		/// Estação mãe de geração do MDF-e
		/// </summary>
		public virtual EstacaoMae EstacaoMae { get; set; }

		/// <summary>
		/// Codigo da estação mãe
		/// </summary>
		public virtual string CodigoEstacaoMae { get; set; }

		/// <summary>
		/// Indicador de ativo
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Data e hora de cadastro
		/// </summary>
		public virtual DateTime DataHora { get; set; }
	}
}