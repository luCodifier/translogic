﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe01Inf
	/// </summary>
	public class Mdfe01Inf : EntidadeBase<int?>
	{
		/// <summary>
		/// Código da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// Número do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// Série do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Vers�o do leiaute
		/// </summary>
		public virtual double InfMdfeVersao { get; set; }

		/// <summary>
		/// Identificador da tag a ser assinada
		/// </summary>
		public virtual string InfMdfeId { get; set; }

		/// <summary>
		/// Código da UF do Emitente do MDF-e
		/// </summary>
		public virtual int IdeCuf { get; set; }

		/// <summary>
		/// Tipo do Ambiente
		/// </summary>
		public virtual int IdeTpAmb { get; set; }

		/// <summary>
		/// Tipo do Emitente
		/// </summary>
		public virtual int IdeTpEmit { get; set; }

		/// <summary>
		/// Modelo do Manifesto Eletrônico
		/// </summary>
		public virtual int IdeMod { get; set; }

		/// <summary>
		/// Série do Manifesto
		/// </summary>
		public virtual int IdeSerie { get; set; }

		/// <summary>
		/// Número do Manifesto
		/// </summary>
		public virtual int IdeNmdf { get; set; }

		/// <summary>
		/// Código numérico que compoe a Chave de Acesso
		/// </summary>
		public virtual int IdeCmdf { get; set; }

		/// <summary>
		/// Dígito verificador da chave de acesso do Manifesto
		/// </summary>
		public virtual int? IdeCdv { get; set; }

		/// <summary>
		/// Modalidade de transporte
		/// </summary>
		public virtual int IdeModal { get; set; }

		/// <summary>
		/// Data e Hora de emiss�o do Manifesto
		/// </summary>
		public virtual DateTime IdeDhEmi { get; set; }

		/// <summary>
		/// Forma de emissão do Manifesto (Normal ou Contingência) 
		/// </summary>
		public virtual int IdeTpEmis { get; set; }

		/// <summary>
		/// Identificação do processo de emissão do Manifesto
		/// </summary>
		public virtual double IdeProcEmi { get; set; }

		/// <summary>
		/// Versão do processo de emissão
		/// </summary>
		public virtual string IdeVerProc { get; set; }

		/// <summary>
		/// Sigla da UF de Carregamento
		/// </summary>
		public virtual string IdeUfIni { get; set; }

		/// <summary>
		/// Sigla da UF de Descarregamento
		/// </summary>
		public virtual string IdeUfFim { get; set; }

		/// <summary>
		/// CNPJ do Emitente
		/// </summary>
		public virtual long EmitCnpj { get; set; }

		/// <summary>
		/// Inscrição Estadual do emitente
		/// </summary>
		public virtual long EmitIe { get; set; }

		/// <summary>
		/// Razão Social ou Nome do emitente
		/// </summary>
		public virtual string EmitXnome { get; set; }

		/// <summary>
		/// Nome Fantasia do emitente
		/// </summary>
		public virtual string EmitXfant { get; set; }

		/// <summary>
		/// Endereço do Logradouro
		/// </summary>
		public virtual string EnderEmitXlgr { get; set; }

		/// <summary>
		/// Número do logradouro
		/// </summary>
		public virtual string EnderEmitNro { get; set; }

		/// <summary>
		/// Complemento do logradouro
		/// </summary>
		public virtual string EnderEmitXcpl { get; set; }

		/// <summary>
		/// Bairro do logradouro
		/// </summary>
		public virtual string EnderEmitXbairro { get; set; }

		/// <summary>
		/// Codigo do Municipio
		/// </summary>
		public virtual int EnderEmitCmun { get; set; }

		/// <summary>
		/// Nome do Municipio
		/// </summary>
		public virtual string EnderEmitXmun { get; set; }

		/// <summary>
		/// CEP do logradouro
		/// </summary>
		public virtual int EnderEmitCep { get; set; }

		/// <summary>
		/// Sigla da UF
		/// </summary>
		public virtual string EnderEmitUf { get; set; }

		/// <summary>
		/// Telefone do logradouro
		/// </summary>
		public virtual int? EnderEmitFone { get; set; }

		/// <summary>
		/// Endereço de E-mail
		/// </summary>
		public virtual string EnderEmitEmail { get; set; }

		/// <summary>
		/// Versão do leiaute específico para o Modal
		/// </summary>
		public virtual double InfModalVersao { get; set; }

		/// <summary>
		/// Quantidade total de CT-e relacionados no Manifesto
		/// </summary>
		public virtual int? TotQcte { get; set; }

		/// <summary>
		/// Quantidade total de Conhecimentos Papel relacionados no Manifesto
		/// </summary>
		public virtual int? TotQct { get; set; }

		/// <summary>
		/// Quantidade total de NF-e relacionadas no Manifesto
		/// </summary>
		public virtual int? TotQnfe { get; set; }

		/// <summary>
		/// Quantidade total de Nota Fiscal mod 1/1A relacionadas no Manifesto
		/// </summary>
		public virtual int? TotQnf { get; set; }

		/// <summary>
		/// Valor total da mercadoria/carga transportada
		/// </summary>
		public virtual double? TotVcarga { get; set; }

		/// <summary>
		/// Código da unidade de medida do Peso Bruto da Carga/Mercadoria transportada
		/// </summary>
		public virtual int TotCunid { get; set; }

		/// <summary>
		/// Peso Bruto Total da Carga/Mercadoria Transportada
		/// </summary>
		public virtual double TotQcarga { get; set; }

		/// <summary>
		/// Informacões Adicionais de Interesse do Fisco
		/// </summary>
		public virtual string InfAdicInfAdfisco { get; set; }

		/// <summary>
		/// Informações Complementares de interesse do Contribuinte
		/// </summary>
		public virtual string InfAdicInfcpl { get; set; }
	}
}
