namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	///  Classe Mdfe01Lista
	/// </summary>
	public class Mdfe01Lista : EntidadeBase<int>
	{
		/// <summary>
		/// Identifica��o da Filial
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// UF da Sefaz de Origem. A mesma UF do Emitente da NF-e (Ver Tabela de C�digos de UF do IBGE)
		/// </summary>
		public virtual int UfIbge { get; set; }

		/// <summary>
		/// N�mero da MDF-e do ERP
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		/// S�rie da MDF-e do ERP
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		/// N�mero da MDF-e final - s� para inutiliza��o
		/// </summary>
		public virtual int NumeroFim { get; set; }

		/// <summary>
		/// Ano de inutiliza��o - s� para inutiliza��o (AA)
		/// </summary>
		public virtual string AnoInut { get; set; }

		/// <summary>
		/// Data e Hora de Emiss�o da MDF-e
		/// </summary>
		public virtual DateTime? DataEmissao { get; set; }

		/// <summary>
		/// Tipo do Registro - 0 - Emiss�o; 1 - Conting�ncia; 2 - Cancelamento; 8 - Encerramento; 9 - Registro de Passagem
		/// </summary>
		public virtual int Tipo { get; set; }

		/// <summary>
		/// CNPJ do Emitente
		/// </summary>
		public virtual long? CnpjEmitente { get; set; }

		/// <summary>
		/// CNPJ do Destinat�rio
		/// </summary>
		public virtual long? CnpjDestinatario { get; set; }

		/// <summary>
		/// Tipo de ambiente - 1 - Produ��o, 2 - Homologa��o
		/// </summary>
		public virtual int TipoAmbiente { get; set; }

		/// <summary>
		/// Vers�o do Schema da Sefaz
		/// </summary>
		public virtual double VersaoSefaz { get; set; }

		/// <summary>
		/// Justificativa do Cancelamento - Obrigat�rio somente para cancelamento
		/// </summary>
		public virtual string Justificativa { get; set; }

		/// <summary>
		/// Modelo do documento fiscal
		/// </summary>
		public virtual string Modelo { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero de agrupamento para impress�o por lote
		/// </summary>
		public virtual int NumeroAgrupamento { get; set; }

		/// <summary>
		/// Flag do Registro -> 0. Incluida, 1. XML OK, 2. PDF , 3. Assinatura, 4. Lote, 5. Sefaz, 6 Autorizada.
		/// </summary>
		public virtual int Flag { get; set; }

		/// <summary>
		/// Data e Hora de Entrada do Registro na Lista
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		/// <summary>
		/// Identifica��o do Lote de MDF-e
		/// </summary>
		public virtual int? IdLote { get; set; }

		/// <summary>
		/// 0 - Sem Erros , 1 - Erro Detectado
		/// </summary>
		public virtual int FlagErro { get; set; }

		/// <summary>
		/// Chave de Acesso do MDF-e
		/// </summary>
		public virtual string ChaveAcesso { get; set; }

		/// <summary>
		/// Protocolo Retornado pela SEFAZ - Autoriza��o de Uso da MDF-e
		/// </summary>
		public virtual long? Protocolo { get; set; }

		/// <summary>
		/// Data e Hora de Recebimento do Protocolo
		/// </summary>
		public virtual DateTime? DataRecibo { get; set; }

		/// <summary>
		/// XML de Retorno do SEFAZ  - ( Protocolo + Dt. Recibo )
		/// </summary>
		public virtual string XmlRetorno { get; set; }

		/// <summary>
		/// Status da MDF-e retornado pelo SEFAZ	
		/// </summary>
		public virtual int? Status { get; set; }

		/// <summary>
		/// Tipo de impress�o do documento 1-Retrato(default) ou 2-Paisagem
		/// </summary>
		public virtual int TipoImpressao { get; set; }

		/// <summary>
		/// Tamanho da MDF-e em bytes
		/// </summary>
		public virtual int? Tamanho { get; set; }

		/// <summary>
		/// XML de Envio
		/// </summary>
		public virtual string XmlEnvio { get; set; }

		/// <summary>
		/// Data e Hora final do processo de emiss�o da MDF-e
		/// </summary>
		public virtual DateTime? DataFinal { get; set; }

		/// <summary>
		/// Controle interno da Aplica��o - Valor inicial "0"
		/// </summary>
		public virtual int Lock { get; set; }

		/// <summary>
		/// Forma de emiss�o do CT-e - 1 - Normal; 5 - Conting�ncia
		/// </summary>
		public virtual int TipoEmissao { get; set; }

		/// <summary>
		/// Data e Hora do inicio do processo
		/// </summary>
		public virtual DateTime? DataInicioProc { get; set; }

		/// <summary>
		/// Data e Hora das altera��es
		/// </summary>
		public virtual DateTime? DataAlteracaoProc { get; set; }

		/// <summary>
		/// Data e Hora do Encerramento do MDF-e - Obrigat�rio somente para evento de Encerramento
		/// </summary>
		public virtual DateTime? DataEncerramentoProc { get; set; }

		/// <summary>
		/// N�mero da UF de Encerramento do MDF-e - Obrigat�rio somente para evento de Encerramento
		/// </summary>
		public virtual int? UfEncerramento { get; set; }

		/// <summary>
		/// C�digo do  Munic�pio de Encerramento do MDF-e - Obrigat�rio somente para evento de Encerramento
		/// </summary>
		public virtual int? CodigoMunicipioEncerramento { get; set; }
	}
}