namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe10FerroVag - Tabela utilizada para informa��es dos Vag�es (1-n)
	/// </summary>
	public class Mdfe10FerroVag : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// C�digo do Item
		/// </summary>
		public virtual int IdVag { get; set; }

		/// <summary>
		/// S�rie de Identifica��o do vag�o
		/// </summary>
		public virtual string VagSerie { get; set; }

		/// <summary>
		/// N�mero de Identifica��o do vag�o
		/// </summary>
		public virtual int VagNvag { get; set; }

		/// <summary>
		/// Sequ�ncia do vag�o na composi��o
		/// </summary>
		public virtual int VagNseq { get; set; }
		
		/// <summary>
		/// Tonelada �til
		/// </summary>
		public virtual double VagTu { get; set; }

		/// <summary>
        /// Peso Base de C�lculo de Frete em Toneladas
		/// </summary>
        public virtual double VagPesoBc { get; set; }

		/// <summary>
        /// Peso Real em Toneladas
		/// </summary>
        public virtual double VagPesoR { get; set; }
		
		/// <summary>
        /// Tipo de Vag�o (S�rie)
		/// </summary>
		public virtual string VagTpVag { get; set; }
	}
}