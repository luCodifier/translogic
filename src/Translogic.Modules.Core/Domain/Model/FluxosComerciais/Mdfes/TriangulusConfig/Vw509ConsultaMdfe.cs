namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Vw509ConsultaMdfe
	/// </summary>
	public class Vw509ConsultaMdfe : EntidadeBase<int>
	{
		/// <summary>
		/// Codigo da Lista 
		/// </summary>
		public virtual int IdLista { get; set; }

		/// <summary>
		/// Codigo interno da filial 
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// Numero do Cte Lista 
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		///  S�rie do Cte Lista
		/// </summary>
		public virtual int Serie { get; set; }

		/// <summary>
		///  Data do Cte Lista
		/// </summary>
		public virtual DateTime DtLista { get; set; }

		/// <summary>
		///  Tipo do Cte Lista
		/// </summary>
		public virtual int Tipo { get; set; }

		/// <summary>
		///  Descri��o do Tipo do Cte Lista
		/// </summary>
		public virtual string DescTipo { get; set; }

		/// <summary>
		///  Flag do Cte Lista
		/// </summary>
		public virtual int Flag { get; set; }

		/// <summary>
		///  Descri��o do Flag do Cte Lista
		/// </summary>
		public virtual string DescFlag { get; set; }

		/// <summary>
		/// Flag de Erro do Cte Lista
		/// </summary>
		public virtual int FlagErro { get; set; }

		/// <summary>
		/// Numero do cte Inicial
		/// </summary>
		public virtual int NumeroMdfeIni { get; set; }

		/// <summary>
		///  Campo NUMERO_CTE_FIM
		/// </summary>
		public virtual int? NumeroMdfeFim { get; set; }

		/// <summary>
		///  Protocolo do Cte Lista
		/// </summary>
		public virtual long? Protocolo { get; set; }

		/// <summary>
		///  Data hora do Protocolo do Cte Lista
		/// </summary>
		public virtual DateTime? DhProtocolo { get; set; }

		/// <summary>
		///  Status do Cte Lista
		/// </summary>
		public virtual int Stat { get; set; }

		/// <summary>
		/// Descri��o Status do Cte Lista
		/// </summary>
		public virtual string DescStat { get; set; }

		/// <summary>
		///  Campo UF do Cte Lista
		/// </summary>
		public virtual int Uf { get; set; }

		/// <summary>
		///  Numero Nrec do Cte Lista
		/// </summary>
		public virtual long? NRec { get; set; }

		/// <summary>
		///  Data hora Rec lote
		/// </summary>
		public virtual DateTime? DhRecLote { get; set; }

		/// <summary>
		///  Chave de acesso do Cte Lista
		/// </summary>
		public virtual string ChaveAcesso { get; set; }

		/// <summary>
		///  C�digo Cliente Emp do Cte Lista
		/// </summary>
		public virtual string CodCliEmp { get; set; }

		/// <summary>
		/// C�digo Cliente Filial do Cte Lista
		/// </summary>
		public virtual string CodCliFil { get; set; }

		/// <summary>
		/// Tipo de Emiss�o
		/// </summary>
		public virtual int TipoEmissao { get; set; }
	}
}