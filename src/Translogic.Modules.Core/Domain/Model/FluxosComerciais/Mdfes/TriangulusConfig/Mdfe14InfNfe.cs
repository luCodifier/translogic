namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe14InfNfe - Tabela utilizada para informa��es de Nota Fiscal Eletr�nica (0-2000)
	/// </summary>
	public class Mdfe14InfNfe : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfDoc { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfNfe { get; set; }

		/// <summary>
		/// Nota Fiscal Eletr�nica
		/// </summary>
		public virtual string InfNfeChNfe { get; set; }

		/// <summary>
		/// Segundo c�digo de barras
		/// </summary>
		public virtual string InfNfeSegCodBarra { get; set; }
	}
}