namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe02InfMunCarrega - Tabela utilizada para informa��es dos Munic�pios de Carregamento (1-50)
	/// </summary>
	public class Mdfe02InfMunCarrega : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfMunCarega { get; set; }

		/// <summary>
		/// C�digo do Munic�pio de Carregamento
		/// </summary>
		public virtual int InfNumCarregaCmunCarrega { get; set; }

		/// <summary>
		/// Nome do Munic�pio de Carregamento
		/// </summary>
		public virtual string InfNumCarregaXmumCarrega { get; set; }
	}
}