namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe15InfNf - Tabela utilizada para informa��es de Nota Fiscal Papel (mod 1 e 1A) (0-2000)
	/// </summary>
	public class Mdfe15InfNf : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfDoc { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfNf { get; set; }

		/// <summary>
		/// CNPJ do emitente
		/// </summary>
		public virtual int IdInfCnpj { get; set; }

		/// <summary>
		/// UF do Destinat�rio
		/// </summary>
		public virtual string IdInfUf { get; set; }

		/// <summary>
		/// N�mero da Nf
		/// </summary>
		public virtual string IdInfNnf { get; set; }

		/// <summary>
		/// S�rie da Nf
		/// </summary>
		public virtual int IdInfSerie { get; set; }

		/// <summary>
		/// Data de Emiss�o
		/// </summary>
		public virtual DateTime IdInfDemi { get; set; }

		/// <summary>
		/// Valor Total da NF
		/// </summary>
		public virtual double IdInfVnf { get; set; }

		/// <summary>
		/// PIN SUFRAMA
		/// </summary>
		public virtual int IdInfPin { get; set; }
	}
}