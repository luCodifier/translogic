namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Mdfe09Ferro - Tabela utilizada para informa��es do modal Ferrovi�rio (0-1)
	/// </summary>
	public class Mdfe09Ferro : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public virtual string TremXpref { get; set; }

		/// <summary>
		/// Data e Hora de libera��o do trem na origem
		/// </summary>
		public virtual DateTime TremDhTrem { get; set; }

		/// <summary>
		/// Origem do Trem
		/// </summary>
		public virtual string TremXori { get; set; }

		/// <summary>
		/// Destino do Trem
		/// </summary>
		public virtual string TremXdest { get; set; }

		/// <summary>
		/// Quantidade de vag�es carregados
		/// </summary>
		public virtual int TremQvag { get; set; }
	}
}