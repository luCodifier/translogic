namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe06AgrupImp - Tabela utilizada para informa��es dos Agrupamentos para Impress�o (0-n)
	/// </summary>
	public class Mdfe06AgrupImp : EntidadeBase<int?>
	{
		/// <summary>
		/// N�mero do Agrupamento
		/// </summary>
		public virtual int Agrupamento { get; set; }

		/// <summary>
		/// Flag para controle de j� imprimiu ou n�o
		/// </summary>
		public virtual int Impresso { get; set; }

		/// <summary>
		/// Quantidade de Documentos que far� parte do grupo
		/// </summary>
		public virtual int Qtde { get; set; }

		/// <summary>
		/// Data de Inclus�o
		/// </summary>
		public virtual DateTime DataInclusao { get; set; }

		/// <summary>
		/// Data de Altera��o
		/// </summary>
		public virtual DateTime DataAlteracao { get; set; }

		/// <summary>
		/// N�mero do Lock
		/// </summary>
		public virtual int Lock { get; set; }

		/// <summary>
		/// Descri��o do agrupamento
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// C�digo da Filial
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// Agrupamento liberado pelo usu�rio
		/// </summary>
		public virtual int QtdeUsr { get; set; }
	}
}