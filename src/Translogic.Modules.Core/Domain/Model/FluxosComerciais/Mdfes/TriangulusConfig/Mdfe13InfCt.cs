namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe13InfCt - Tabela utilizada para informa��es de Conhecimetos de Transporte (papel) (0-2000)
	/// </summary>
	public class Mdfe13InfCt : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfDoc { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfCt { get; set; }

		/// <summary>
		/// N�mero do CT
		/// </summary>
		public virtual string InfCtNct { get; set; }

		/// <summary>
		/// S�rie do CT
		/// </summary>
		public virtual int InfCtSerie { get; set; }

		/// <summary>
		/// Subs�rie do CT
		/// </summary>
		public virtual int InfCtSubSer { get; set; }

		/// <summary>
		/// Data de Emiss�o
		/// </summary>
		public virtual DateTime InfCtDemi { get; set; }

		/// <summary>
		/// Valor total da carga
		/// </summary>
		public virtual double InfCtVcarga { get; set; }
	}
}