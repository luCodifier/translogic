namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe03InfPercurso - Tabela utilizada para informa��es do percurso do MDF-e (0-25)
	/// </summary>
	public class Mdfe03InfPercurso : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfPercurso { get; set; }

		/// <summary>
		/// Sigla das Unidades da Federa��o do percurso do ve�culo - N�o � necess�rio repetir as UF de In�cio e Fim
		/// </summary>
		public virtual string InfPercursoUfPer { get; set; }
	}
}