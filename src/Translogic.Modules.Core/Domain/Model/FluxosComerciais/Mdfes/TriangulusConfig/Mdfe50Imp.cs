namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe Mdfe50Imp - Tabela utilizada para informa��es de Impressoras (0-n)
	/// </summary>
	public class Mdfe50Imp : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int IdFilial { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int Numero { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		/// N�mero Sequencial por Documento
		/// </summary>
		public virtual int IdImp { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }
		
		/// <summary>
		/// Local da Impressora
		/// </summary>
		public virtual string LocalImpressora { get; set; }

		/// <summary>
		/// Nome da Impressora
		/// </summary>
		public virtual string NomeImpressora { get; set; }

		/// <summary>
		/// N�mero de C�pias do Documento
		/// </summary>
		public virtual int NumCopiasDocumento { get; set; }

		/// <summary>
		/// N�mero da Bandeja
		/// </summary>
		public virtual string NumBandeja { get; set; }

		/// <summary>
		/// Documento ser� impresso frente e verso ou n�o
		/// </summary>
		public virtual int IndImpFrenteVerso { get; set; }

		/// <summary>
		/// Quantidade de c�pias adicionais do Documento
		/// </summary>
		public virtual int QtdeCopiasAdicionais { get; set; }
	}
}