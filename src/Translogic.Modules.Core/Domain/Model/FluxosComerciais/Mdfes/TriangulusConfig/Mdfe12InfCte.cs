namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe  Mdfe12InfCte - Tabela utilizada para informa��es dos Conhecimento de Transporte (0-2000)
	/// </summary>
	public class Mdfe12InfCte : EntidadeBase<int?>
	{
		/// <summary>
		/// C�digo da Unidade
		/// </summary>
		public virtual int CfgUn { get; set; }

		/// <summary>
		/// N�mero do Documento
		/// </summary>
		public virtual int CfgDoc { get; set; }

		/// <summary>
		/// S�rie do Documento
		/// </summary>
		public virtual string CfgSerie { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a Filial - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteFilial { get; set; }

		/// <summary>
		/// Codigo do Cliente destinado a empresa - utilizado para multi-empresa
		/// </summary>
		public virtual string CodigoClienteEmpresa { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfDoc { get; set; }

		/// <summary>
		/// Numero do Item
		/// </summary>
		public virtual int IdInfCte { get; set; }

		/// <summary>
		/// Conhecimento Eletr�nico - Chave de Acesso
		/// </summary>
		public virtual string InfCteChCte { get; set; }

		/// <summary>
		/// Segundo c�digo de barras
		/// </summary>
		public virtual string InfCteSegCodBarra { get; set; }
	}
}