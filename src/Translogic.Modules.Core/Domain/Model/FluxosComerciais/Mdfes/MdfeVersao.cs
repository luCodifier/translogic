﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe da versão do MDF-e
	/// </summary>
	public class MdfeVersao : EntidadeBase<int?>
	{
		/// <summary>
		/// Código da versão do CTE
		/// </summary>
		public virtual string CodigoVersao { get; set; }

		/// <summary>
		/// Indicador de a versão está ativa
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Data e hora do cadastro da versao
		/// </summary>
		public virtual DateTime DataHora { get; set; } 
	}
}