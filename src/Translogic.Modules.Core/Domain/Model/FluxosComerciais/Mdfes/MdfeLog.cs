﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Mdfe;

	/// <summary>
	/// Classe de log do MDF-e
	/// </summary>
	public class MdfeLog : EntidadeBase<int?>
	{
		/// <summary>
		/// Mdfe no qual o log se refere
		/// </summary>
		public virtual Mdfe Mdfe { get; set; }

		/// <summary>
		/// Tipo de log 
		/// </summary>
		public virtual TipoLogMdfeEnum TipoLog { get; set; }

		/// <summary>
		/// Nome do serviço ou método que está sendo logado
		/// </summary>
		public virtual string NomeServico { get; set; }

		/// <summary>
		/// Descrição do log
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Data e hora de gravação do log
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Nome da máquina que está sendo executado o robo
		/// </summary>
		public virtual string HostName { get; set; }		 
	}
}