﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;
	using Diversos.Mdfe;

	/// <summary>
	/// Classe de log do MDF-e Runner
	/// </summary>
	public class MdfeRunnerLog : EntidadeBase<int?>
	{
		/// <summary>
		/// Tipo de log 
		/// </summary>
		public virtual TipoLogMdfeEnum TipoLog { get; set; }

		/// <summary>
		/// Nome da máquina que está sendo executado o robo
		/// </summary>
		public virtual string HostName { get; set; }

		/// <summary>
		/// Nome do serviço que está sendo feito o log
		/// </summary>
		public virtual string NomeServico { get; set; }

		/// <summary>
		/// Descrição do log
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Data e hora de gravação do log
		/// </summary>
		public virtual DateTime DataHora { get; set; }
	}
}