﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de interface de recebimento do retorno do MDF-e da Config
	/// </summary>
	public class MdfeInterfaceRecebimentoConfig : EntidadeBase<int?>
	{
		/// <summary>
		/// Cte da interface de recebimento
		/// </summary>
		public virtual Mdfe Mdfe { get; set; }

		/// <summary>
		/// Identificador da lista da config
		/// </summary>
		public virtual int IdListaConfig { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado na fila de recebimento
		/// </summary>
		public virtual DateTime DataHora { get; set; }

		/// <summary>
		/// Data e hora que o cte foi colocado que foi realizado a ultima leitura
		/// </summary>
		public virtual DateTime DataUltimaLeitura { get; set; }
	}
}