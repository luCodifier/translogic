﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe com os códigos de retorno do MDF-e - (códigos de retorno da receita)
	/// </summary>
	public class MdfeStatusRetorno : EntidadeBase<int?>
	{
		/// <summary>
		/// Descrição dos códigos de retorno
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica se o código foi informado pela Receita.
		/// </summary>
		public virtual bool InformadoPelaReceita { get; set; }

		/// <summary>
		/// Indicador se esse código de erro permite reenvio
		/// </summary>
		public virtual bool PermiteReenvio { get; set; }

		/// <summary>
		/// Indicador se remove da fila da interface
		/// </summary>
		public virtual bool RemoveFilaInterface { get; set; }

		/// <summary>
		/// Dados da a ação a ser tomada
		/// </summary>
		public virtual string AcaoSerTomada { get; set; }		 
	}
}