﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Model Associa Fluxo Internacional
	/// </summary>
	public class AssociaFluxoInternacional : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Fluxo Comercial
		/// </summary>
		public virtual FluxoComercial FluxoComercial { get; set; }

		/// <summary>
		/// Fluxo Internacional
		/// </summary>
		public virtual FluxoInternacional FluxoInternacional { get; set; }

		#endregion
	}
}
