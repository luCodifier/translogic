﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    public class DclImpressaoDocumento
    {
        public virtual string CnpjEmitente { get; set; }

        public virtual string Numero { get; set; }

        public virtual string ChaveNfe { get; set; }

        public virtual double Peso { get; set; }

        public virtual double Volume { get; set; }

        public virtual string Conteiner { get; set; }
    }
}