﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.Via;

    public class DclImpressao
    {
        public virtual int IdDespacho { get; set; }

        public virtual string Numero { get; set; }

        public virtual string Serie { get; set; }

        public virtual IList<Spd.Data.Cte> Ctes { get; set; }

        public virtual Spd.Data.Empresa Emitente { get; set; }

        public virtual FluxoComercial FluxoComercial { get; set; }

        public virtual IAreaOperacional Origem { get; set; }

        public virtual IAreaOperacional Destino { get; set; }

        public virtual IEmpresa Remetente { get; set; }

        public virtual IEmpresa Destinatario { get; set; }

        public virtual IEmpresa Expedidor { get; set; }

        public virtual IEmpresa Recebedor { get; set; }

        public virtual string Mercadoria { get; set; }

        public virtual string SerieVagao { get; set; }

        public virtual string CodigoVagao { get; set; }

        public virtual double TaraVagao { get; set; }

        public virtual double PesoBrutoVagao { get; set; }

        public virtual double PesoLiquido { get; set; }

        public virtual ICollection<DclImpressaoDocumento> ListaDocs { get; set; }

        public virtual string Observacao { get; set; }

    }
}