namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para persistir o Dcl na tabela tempor�ria de vagao
    /// VG_ID_VG - � o id da entidade
    /// </summary>
    public class DclTempVagao : EntidadeBase<int?>
    {
        /// <summary>
        /// C�digo do vag�o
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Indica se � m�ltiplo despacho
        /// </summary>
        public virtual bool IndMultiploDespacho { get; set; }

        /// <summary>
        /// Peso do DCL
        /// </summary>
        public virtual double PesoDcl { get; set; }

        /// <summary>
        /// Peso total do vag�o
        /// </summary>
        public virtual double PesoTotal { get; set; }

        /// <summary>
        /// Volume do vag�o
        /// </summary>
        public virtual double Volume { get; set; }

        /// <summary>
        /// Quantidade de conteiners no vag�o
        /// </summary>
        public virtual int? QuantidadeConteiners { get; set; }

        /// <summary>
        /// Tara do vag�o
        /// </summary>
        public virtual double Tara { get; set; }
    }
}