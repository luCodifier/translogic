namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para persistir o Dcl na tabela tempor�ria
    /// ID_DESPACHO - � o id da entidade
    /// </summary>
    public class DclTemp : EntidadeBase<int?>
    {
        /// <summary>
        /// Usu�rio que gravar� o DCL - campo US_USR_IDU
        /// </summary>
        public virtual string CodigoUsuario { get; set; }

        /// <summary>
        /// C�digo da transa��o - campo TS_COD_TRS
        /// </summary>
        public virtual string CodigoTransacao { get; set; }

        /// <summary>
        /// Id do Fluxo comercial - FX_ID_FLX
        /// </summary>
        public virtual int IdFluxoComercial { get; set; }

        /// <summary>
        /// Id do fluxo internacional - FXI_ID_FXI
        /// </summary>
        public virtual int? IdFluxoInternacional { get; set; }

        /// <summary>
        /// Data do despacho - DP_DT
        /// </summary>
        public virtual DateTime DataDespacho { get; set; }

        /// <summary>
        /// Indica se o dcl � de vag�o - IND_DCL_VG
        /// </summary>
        public virtual bool DclPorVagao { get; set; }

        /// <summary>
        /// N�mero de s�rio de intercambio - DP_NUM_SDI
        /// </summary>
        public virtual string NumeroSerieIntercambio { get; set; }

        /// <summary>
        /// N�mero do despacho de intercambio - DP_NUM_DPI
        /// </summary>
        public virtual int? NumeroDespachoIntercambio { get; set; }

        /// <summary>
        /// Id da ferrovia de intercambio - ID_FERR_INT
        /// </summary>
        public virtual int? IdFerroviaIntercambio { get; set; }

        /// <summary>
        /// Regime de interc�mbio - REG_INT
        /// </summary>
        public virtual string RegimeIntercambio { get; set; }

        /// <summary>
        /// Id da linha de recebimento do intercambio - EV_ID_ELV
        /// </summary>
        public virtual int? IdLinhaIntercambio { get; set; }

        /// <summary>
        /// Id da empresa do despacho - EP_ID_EMP
        /// </summary>
        public virtual int IdEmpresaDespacho { get; set; }

        /// <summary>
        /// Id da area operacional da origem do fluxo - AO_ID_AO_SEDE
        /// </summary>
        public virtual int IdAreaOperacionalSede { get; set; }

        /// <summary>
        /// Id da area operacional de destino - AO_ID_AO_DEST
        /// </summary>
        public virtual int IdAreaOperacionalDestino { get; set; }

        /// <summary>
        /// Id da mercadoria - MC_ID_MRC
        /// </summary>
        public virtual int IdMercadoria { get; set; }

        /// <summary>
        /// Indica se a origem interna � RPL - IND_AO_RPL
        /// </summary>
        public virtual bool IndOrigemRpl { get; set; }

        /// <summary>
        /// Data do despacho de intecambio - DP_DAT_DIT
        /// </summary>
        public virtual DateTime? DataIntercambio { get; set; }

        /// <summary>
        /// Serie de manuten��o - DCL_SERIE_MAN
        /// </summary>
        public virtual string SerieManutencao { get; set; }

        /// <summary>
        /// N�mero do DCL de manuten��o - DCL_NUME_MAN
        /// </summary>
        public virtual int? NumeroDclManutencao { get; set; }

        /// <summary>
        /// Indica se � rebatimento - IND_REBATIME
        /// </summary>
        public virtual bool IndRebatimento { get; set; }

        /// <summary>
        /// Observa��o do DCL - DCL_OBS
        /// </summary>
        public virtual string Observacao { get; set; }

        /*/// <summary>
        /// Id do grupo de fluxo - GF_ID_GFX
        /// </summary>
        public virtual int? IdGrupoFluxo { get; set; }

        /// <summary>
        /// C�digo do grupo de fluxo - GF_COD_GFX
        /// </summary>
        public virtual string CodigoGrupoFluxo { get; set; }*/

        /// <summary>
        /// Indica se � pre-carregamento - IND_PRECARGA
        /// </summary>
        public virtual bool IndPrecarregamento { get; set; }

        /// <summary>
        /// S�rie do CTFC - DCL_SERIE_CTFC
        /// </summary>
        public virtual string SerieCtfc { get; set; }

        /// <summary>
        /// N�mero do CTFC - DCL_NUM_CTFC
        /// </summary>
        public virtual string NumeroCtfc { get; set; }

        /// <summary>
        /// Data de carregamento - DP_DAT_CNG
        /// </summary>
        public virtual DateTime DataCarregamento { get; set; }

        /// <summary>
        /// Prefixo do despacho - DCL_PREFIXO_DESP
        /// </summary>
        public virtual string PrefixoDespacho { get; set; }

        /*/// <summary>
        /// Id do motivo de manuten��o - ID_MOTIVO_MANUT
        /// </summary>
        public virtual int? IdMotivoManutencao { get; set; }*/
    }
}