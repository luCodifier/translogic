namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Repositório de DclTemp
    /// </summary>
    public interface IDclTempVagaoRepository : IRepository<DclTempVagao, int?>
    {
        /// <summary>
        /// Insere uma lista de DclTempVagao
        /// </summary>
        /// <param name="listaDclTempVagao"> The lista dcl temp vagao. </param>
        void Inserir(IList<DclTempVagao> listaDclTempVagao);
    }
}