namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Repositório de DclTempVagaoConteiner
    /// </summary>
    public interface IDclTempVagaoConteinerRepository : IRepository<DclTempVagaoConteiner, string>
    {
        /// <summary>
        /// Insere uma lista de DclTempVagaoConteiner
        /// </summary>
        /// <param name="listDclTempVagaoConteiner"> Lista de DclTempVagaoConteiner. </param>
        void Inserir(IList<DclTempVagaoConteiner> listDclTempVagaoConteiner);
    }
}