namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Reposit�rio de DclTemp
    /// </summary>
    public interface IDclTempRepository : IRepository<DclTemp, int?>
    {
        /// <summary>
        /// Chama a package de gerar despachos
        /// </summary>
        /// <param name="idControle">Id de controle</param>
        /// <param name="codigoTela">C�digo da Tela</param>
        void GerarDespachos(string idControle, string codigoTela);

        /// <summary>
        /// Obt�m o Id de controle da DclTemp
        /// </summary>
        /// <returns> Id de controle da dcltemp </returns>
        decimal ObterIdControle();

        /// <summary>
        /// Apaga os dados das tabelas tempor�rias de carregamento
        /// </summary>
        /// <param name="dclTemp">Dcl temp a ser apagada</param>
        /// <param name="dclTempVagaoConteiner">Dcl temp vagao conteiner</param>
        /// <param name="dclTempVagao">Dcl temp vagao</param>
        /// <param name="dclTempVagaoNotaFiscal">dcl temp vagao nota fiscal</param>
        /// <param name="idControleDclTemp">id controle dcl temp</param>
        void LimparDados(DclTemp dclTemp, IList<DclTempVagaoConteiner> dclTempVagaoConteiner, IList<DclTempVagao> dclTempVagao, IList<DclTempVagaoNotaFiscal> dclTempVagaoNotaFiscal, decimal idControleDclTemp);
    }
}