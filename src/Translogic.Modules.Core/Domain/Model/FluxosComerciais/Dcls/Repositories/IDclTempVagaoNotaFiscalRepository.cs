namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Repositório de DclTempVagaoNotaFiscal
    /// </summary>
    public interface IDclTempVagaoNotaFiscalRepository : IRepository<DclTempVagaoNotaFiscal, int>
    {
        /// <summary>
        /// Insere uma lista de DclTempVagaoNotaFiscal
        /// </summary>
        /// <param name="listaDclTempVagaoNotaFiscal"> Lista de DclTempVagaoNotaFiscal. </param>
        void Inserir(IList<DclTempVagaoNotaFiscal> listaDclTempVagaoNotaFiscal);
    }
}