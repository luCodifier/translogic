namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Reposit�rio de DclErroGravacao
    /// </summary>
    public interface IDclErroGravacaoRepository : IRepository<DclErroGravacao, DateTime>
    {
        /// <summary>
        /// Obt�m os erros de grava��o pelo id de controle
        /// </summary>
        /// <param name="idControle"> The id controle. </param>
        /// <returns> Lista de erros </returns>
        IList<DclErroGravacao> ObterPorIdControle(decimal idControle);
    }
}