namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para persistir o Dcl na tabela tempor�ria de nota fiscal
    /// NF_IDENTIF - � o id da entidade
    /// </summary>
    public class DclTempVagaoNotaFiscal : EntidadeBase<int>
    {
        /// <summary>
        /// Identifica��o da nota fiscal (concatena��o do Numero e Serie)
        /// </summary>
        public virtual string Identificacao { get; set; }

        /// <summary>
        /// C�digo do vag�o
        /// </summary>
        public virtual string CodigoVagao { get; set; }

        /// <summary>
        /// Codigo do conteiner
        /// </summary>
        public virtual string CodigoConteiner { get; set; }

        /// <summary>
        /// N�mero da nota fiscal
        /// </summary>
        public virtual string NumeroNotaFiscal { get; set; }

        /// <summary>
        /// Serie da nota fiscal
        /// </summary>
        public virtual string SerieNotaFiscal { get; set; }

        /// <summary>
        /// Valor da nota fiscal
        /// </summary>
        public virtual double Valor { get; set; }

        /// <summary>
        /// Valor Total da nota fiscal
        /// </summary>
        public virtual double ValorTotal { get; set; }

        /// <summary>
        /// Data da nota fiscal
        /// </summary>
        public virtual DateTime DataNotaFiscal { get; set; }

        /// <summary>
        /// Peso informado
        /// </summary>
        public virtual double? Peso { get; set; }

        /// <summary>
        /// Peso total informado
        /// </summary>
        public virtual double? PesoTotal { get; set; }

        /// <summary>
        /// Volume informado
        /// </summary>
        public virtual double? Volume { get; set; }

        /// <summary>
        /// N�mero do TIF
        /// </summary>
        public virtual string NumeroTif { get; set; }

        /// <summary>
        /// C�digo da empresa Remetente
        /// </summary>
        public virtual string CodigoEmpresaRemetente { get; set; }

        /// <summary>
        /// C�digo da empresa destinat�ria
        /// </summary>
        public virtual string CodigoEmpresaDestinataria { get; set; }

        /// <summary>
        /// Uf da Empresa Remetente
        /// </summary>
        public virtual string UfEmpresaRemetente { get; set; }

        /// <summary>
        /// Uf da empresa destinat�ria
        /// </summary>
        public virtual string UfEmpresaDestinataria { get; set; }

        /// <summary>
        /// Cnpj da empresa remetente
        /// </summary>
        public virtual string CnpjEmpresaRemetente { get; set; }

        /// <summary>
        /// Cnpj da empresa destinat�ria
        /// </summary>
        public virtual string CnpjEmpresaDestinataria { get; set; }

        /// <summary>
        /// Inscricao Estadual da empresa remetente
        /// </summary>
        public virtual string InscricaoEstadualEmpresaRemetente { get; set; }

        /// <summary>
        /// Inscri��o estadual da empresa destinat�ria
        /// </summary>
        public virtual string InscricaoEstadualEmpresaDestinataria { get; set; }

        /// <summary>
        /// Chave da NFe
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Chave do Cte
        /// </summary>
        public virtual string ChaveCte { get; set; }
    }
}
