namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de erro de grava��o de dcl
    /// </summary>
    public class DclErroGravacao : EntidadeBase<DateTime>
    {
        /// <summary>
        /// Mensagem de Erro
        /// </summary>
        public virtual string Mensagem { get; set; }

        /// <summary>
        /// Id do controle
        /// </summary>
        public virtual decimal IdControle { get; set; }
    }
}