namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais.Dcls
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe para persistir o Dcl na tabela tempor�ria conteiner no vagao
    /// VG_COD_VAG - � o id da entidade
    /// </summary>
    public class DclTempVagaoConteiner : EntidadeBase<string>
    {
        /// <summary>
        /// Codigo do vag�o
        /// </summary>
        public virtual string CodigoVagao { get; set; }
    }
}