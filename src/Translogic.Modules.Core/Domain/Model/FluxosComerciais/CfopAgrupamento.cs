﻿namespace Translogic.Modules.Core.Domain.Model.FluxosComerciais
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de Agrupamento de Cfop
	/// </summary>
	public class CfopAgrupamento : EntidadeBase<int>
	{
		/// <summary>
		/// Prefixo do Cfop
		/// </summary>
		public virtual string PrefixoCfop { get; set; }

		/// <summary>
		/// Inicio da variação
		/// </summary>
		public virtual int InicioIntervalo { get; set; }

		/// <summary>
		/// Fim da Variacao.
		/// </summary>
		public virtual int FimInteravalo { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}