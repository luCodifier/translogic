﻿using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using System;

namespace Translogic.Modules.Core.Domain.Model.Vagoes.Repositories
{
    public interface IExcessaoVagaoPesoRepository : IRepository<ExcessaoVagaoPeso, int>
    {
        ResultadoPaginado<ExcessaoVagaoPeso> Obter(DetalhesPaginacao paginacao, DateTime dtInicial, DateTime dtFim, string codigoVagao, decimal? peso);

        ExcessaoVagaoPeso ObterPorCodigoVagao(string codigoVagao);
    }
}
