﻿using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Vagoes
{
    public class ExcessaoVagaoPeso : EntidadeBase<int>
    {
        public ExcessaoVagaoPeso()
        {
            Data = DateTime.Now;
        }

        public virtual string CodigoVagao { get; set; }
        public virtual decimal Peso { get; set; }
        public virtual DateTime Data { get; set; }
    }
}