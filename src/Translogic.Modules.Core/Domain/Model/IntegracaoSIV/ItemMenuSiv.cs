﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Representa um item do siv.
    /// </summary>
    public class ItemMenuSiv : RootObjectSiv
    {
        #region PROPRIEDADES

        /// <summary>
        /// Indica se o no do item 
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Indica se o nome interno do item 
        /// </summary>
        public virtual string NomeInterno { get; set; }

        /// <summary>
        /// Indica a descricao do item 
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Indica se o item do menu esta Ativo
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Indica se o item esta Visivel 
        /// </summary>
        public virtual string Visivel { get; set; }

        /// <summary>
        /// Indica a url do item 
        /// </summary>
        public virtual string Url { get; set; }

        /// <summary>
        /// Indica se o item de menu esta restrito 
        /// </summary>
        public virtual string Restrito { get; set; }

        /// <summary>
        /// Indica o nome da transacao
        /// </summary>
        public virtual string NomeTransacao { get; set; }

        /// <summary>
        /// Indica se o submenu esta Ativo
        /// </summary>
        public virtual long Sequencia { get; set; }

        /// <summary>
        /// Indica a target do link 
        /// </summary>
        public virtual string Target { get; set; }

        /// <summary>
        /// Indica o SubMenu 
        /// </summary>
        public virtual SubMenuSiv Submenu { get; set; }

        #endregion
    }
}

