﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="ItemMenuSiv"/>
    /// </summary>
    public interface IItemMenuSivRepository : IRepository<ItemMenuSiv, int?>
    {
        /// <summary>
        /// Obtém todos os itens de menu que possuem autorização
        /// </summary>
        /// <param name="codigoUsuario">Código do usuario</param>
        /// <returns>Lista de itens de menu do siv</returns>
        IList<ItemMenuSiv> ObterTodosAutorizados(string codigoUsuario);

        /// <summary>
        /// Obtém o item de menu do siv pelo código e que esteja autorizado
        /// </summary>
        /// <param name="codigoTelaSiv">Codigo da tela do SIV</param>
        /// <param name="codigoUsuario">Codigo do Usuario</param>
        /// <returns>Item de menu do siv</returns>
        ItemMenuSiv ObterPorCodigoTelaAutorizado(int codigoTelaSiv, string codigoUsuario);
    }
}
