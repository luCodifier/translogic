﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Contrato do Repositório de <see cref="MenuSiv"/>
    /// </summary>
    public interface ISubMenuSivRepository : IRepository<SubMenuSiv, int?>
    {
    }
}
