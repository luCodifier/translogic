﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Contrato do Repositório de <see cref="DivisionSiv"/>
	/// </summary>
	public interface IDivisionSivRepository : IRepository<DivisionSiv, int?>
	{
	}
}