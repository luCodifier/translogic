﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Propriedades comuns ao objetos do SIV
    /// </summary>
    public abstract class RootObjectSiv : EntidadeBase<int?>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Indica o id do objeto
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Indica o Timestamp do objeto
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// Usuario ID.
        /// </summary>
        public virtual long UsuarioId { get; set; }

        #endregion
    }
}