﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV
{
	/// <summary>
	/// Representa uma division no sistema Siv.
	/// </summary>
    public class DivisionSiv : RootObjectSiv
	{
		#region PROPRIEDADES

		/// <summary>
		/// Codigo da sub divisao.
		/// </summary>
		public virtual string CodigoSubdivisao { get; set; }

		/// <summary>
		/// descricao sub divisao.
		/// </summary>
		public virtual string DescricaoDivisao { get; set; }

		/// <summary>
		/// KM inicial  sub divisao.
		/// </summary>
		public virtual float QuilometragemInicialDivisao { get; set; }

		/// <summary>
		/// KM final sub divisao.
		/// </summary>
		public virtual float QuilometragemFinalDivisao { get; set; }

		/// <summary>
		/// Indicador ativo.
		/// </summary>
		public virtual string IndicadorAtivo { get; set; }

		#endregion
	}
}
