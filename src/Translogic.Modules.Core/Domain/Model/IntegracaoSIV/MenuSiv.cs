﻿namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Representa um item do menu principal do SIV. 
    /// </summary>
    public class MenuSiv : RootObjectSiv
    {
        #region PROPRIEDADES

        /// <summary>
        /// Indica se o menu esta Ativo
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Nome do menu. 
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Nome interno do menu
        /// </summary>
        public virtual string NomeInterno { get; set; }

        /// <summary>
        /// Nome interno do menu suspenso
        /// </summary>
        public virtual string NomeInternoSuspenso { get; set; }

        /// <summary>
        /// Descricao do menu. 
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Sequencia do menu
        /// </summary>
        public virtual long Sequencia { get; set; }

        /// <summary>
        /// Endereco da imagem que representa o menu
        /// </summary>
        public virtual string ImagemSource { get; set; }

        #endregion
    }
}