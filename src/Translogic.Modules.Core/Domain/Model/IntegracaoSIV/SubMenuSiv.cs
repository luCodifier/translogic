﻿
namespace Translogic.Modules.Core.Domain.Model.IntegracaoSIV
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Representa um submenu do siv.
    /// </summary>
    public class SubMenuSiv : RootObjectSiv
    {
        #region PROPRIEDADES

        /// <summary>
        /// Indica se o submenu esta Ativo
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Nome do submenu
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Nome interno do submenu 
        /// </summary>
        public virtual string NomeInterno { get; set; }

        /// <summary>
        /// Nome interno do submenu suspenso
        /// </summary>
        public virtual string NomeInternoSuspenso { get; set; }

        /// <summary>
        /// Descricao do submenu
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// Sequencia do submenu
        /// </summary>
        public virtual long Sequencia { get; set; }

        /// <summary>
        /// Indica qual o menu que pertence
        /// </summary>
        public virtual MenuSiv Menu { get; set; }

        #endregion
    }
}