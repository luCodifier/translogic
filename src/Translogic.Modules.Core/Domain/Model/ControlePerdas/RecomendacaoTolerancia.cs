﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Domain.Model.ControlePerdas
{
    /// <summary>
    ///   Classe Recomendação de Tolerância - (Gestão de regras de recomendação)
    /// </summary>
    public class RecomendacaoTolerancia : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Código da Malha vinculado a Recomendação de Tolerância
        /// </summary>
        public virtual string CodMalha { get; set; }

        /// <summary>
        /// Valor da tolerância em toneladas para envio da notificação
        /// </summary>
        public virtual double Valor { get; set; }

        /// <summary>
        /// E-mail para notificação quando regra é válida
        /// </summary>
        public virtual string Email { get; set; }

        #endregion
    }
}