﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Domain.Model.ControlePerdas
{
    /// <summary>
    ///   Classe Recomendação de Vagão - (Gestão de regras de recomendação)
    /// </summary>
    public class RecomendacaoVagao : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Causa Seguro vinculado ao Recomendação do Vagão
        /// </summary>
        public virtual CausaSeguro CausaSeguro { get; set; }

        /// <summary>
        /// Identifica se deve ser considerada a Recomendação S/N (N - Default)
        /// </summary>
        public virtual bool Recomendacao { get; set; }

        /// <summary>
        /// Identifica se deve ser considerada a Tolerância S/N (N - Default)
        /// </summary>
        public virtual bool Tolerancia { get; set; }

        #endregion
    }
}