﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories
{
    /// <summary>
    /// Interface de repositório Recomendação de Tolerância - (Gestão de regras de recomendação)
    /// </summary>
    public interface IRecomendacaoToleranciaRepository : IRepository<RecomendacaoTolerancia, int>
    {
        /// <summary>
        /// Retorna o registro de RecomendacaoTolerancia, de acordo com o código de Malha
        /// </summary>
        /// <param name="codMalha">Código de Malha (LG, MS, MN)</param>
        /// <returns>Retorna o registro de RecomendacaoTolerancia, de acordo com o código de Malha</returns>
        RecomendacaoTolerancia ObterPodCodMalha(string codMalha);
    }
}