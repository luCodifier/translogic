﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.ControlePerdas.Repositories
{
    /// <summary>
    /// Interface de repositório Recomendação de Vagão - (Gestão de regras de recomendação)
    /// </summary>
    public interface IRecomendacaoVagaoRepository : IRepository<RecomendacaoVagao, int>
    {
        /// <summary>
        /// Retorna o registro de RecomendacaoVagao, de acordo com o ID da Causa de Dano 
        /// </summary>
        /// <param name="idCausaSeguro">ID da Causa de Dano</param>
        /// <returns>Retorna o registro de RecomendacaoVagao, de acordo com o ID da Causa de Dano</returns>
        RecomendacaoVagao ObterPorIdCausaSeguro(int idCausaSeguro);
    }
}