﻿namespace Translogic.Modules.Core.Domain.Model.Appa
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IIntegracaoAppaRepository : IRepository
    {
        /// <summary>
        /// Adquire o registro pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IntegracaoAppaDto ObterIntegracaoPorId(int id);

        /// <summary>
        /// Método para obter as integrações do WebService da APPA
        /// </summary>
        ResultadoPaginado<IntegracaoAppaDto> ObterIntegracoesConsulta(DetalhesPaginacao detalhesPaginacao
                                                                     , DateTime dataInicial
                                                                     , DateTime dataFinal
                                                                     , string origem
                                                                     , string destino
                                                                     , string[] vagoes
                                                                     , int situacaoEnvioStatus
                                                                     , int situacaoRecebimentoStatus
                                                                     , int? lote);

        IList<IntegracaoAppaDto> ObterIntegracoesExcel(
            DateTime dataInicial, 
            DateTime dataFinal, 
            string origem, 
            string destino, 
            string[] vagoes, 
            int situacaoEnvioStatus, 
            int situacaoRecebimentoStatus, 
            int? lote);

        /// <summary>
        /// Adquire o XML que foi enviada na integração
        /// </summary>
        /// <param name="id"></param>
        void ObterXmlEnvio(int id);

        /// <summary>
        /// Reprocessa uma integração com erro.
        /// </summary>
        /// <param name="id"></param>
        void ReprocessarRegistro(int id);
    }
}