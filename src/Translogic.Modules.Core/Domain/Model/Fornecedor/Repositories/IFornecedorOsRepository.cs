﻿

namespace Translogic.Modules.Core.Domain.Model.Fornecedor.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Util;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;

    public interface IFornecedorOsRepository :IRepository<FornecedorOs, int>
    {
        /// <summary>
        /// Busca fornecedorOs por parametros
        /// </summary>        
        /// <param name="nomeFornecedor"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoServico"></param>
        /// <returns></returns>        
        ResultadoPaginado<FornecedorOsDto> ObterFornecedorOs(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idTipoServico, int? ativo);
        
        /// <summary>
        /// Retornar os locais Fornecedor por ID
        /// </summary>
        /// <param name="idFornecedor">ID do fornecedor</param>
        /// <param name="idFornecedor">detalhes de paginação</param>
        /// <returns></returns>
        ResultadoPaginado<LocalTipoServicoFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idFornecedor);

        /// <summary>
        /// Retornar lista de FornecedorOS habilitados
        /// </summary>     
        /// <returns></returns>
        ResultadoPaginado<FornecedorOsDto> ObterFornecedoresOsHabilitados(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string nomeFornecedor, string local, int? idServico, string Ativo);

        /// <summary>
        /// Retorna nome fornecedor pelo ID
        /// </summary>
        /// <param name="id fornecedor"></param>
        /// <returns></returns>        
        string ObterNomePorId(int? idFornecedor);

        /// <summary>
        /// Verifica se nome fornecedor existe na base
        /// </summary>
        /// <param name="nome fornecedor"></param>
        /// <returns></returns>        
        bool ObterPorNome(string nomeFornecedor);

        /// <summary>
        /// Verifica se o fornecedor existe na base
        /// </summary>
        /// <param name="nomeFornecedor"></param>
        /// <param name="idFornecedor"></param>
        /// <returns></returns>
        bool ExisteFornecedorOsPorNomeId(string nomeFornecedor, int idFornecedor);

        /// <summary>
        /// Listar fornecedores
        /// </summary>
        /// <param name="local"></param>
        /// <param name="tipoFornecedor"></param>
        /// <param name="todos">Retornar ativos e inativos, caso contrário só ativos e filtrados por local</param>
        /// <returns></returns>
        IList<FornecedorOsDto> ObterFornecedor(string local, EnumTipoFornecedor tipoFornecedor, bool todos);

        /// <summary>
        /// Retornar lista de locais do tipo de serviço revistamento
        /// </summary>
        /// <returns></returns>
        IList<OSLocalDto> ObterLocaisParaTipoServico();
    }
}
