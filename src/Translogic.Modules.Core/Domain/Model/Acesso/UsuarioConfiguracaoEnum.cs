namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.ComponentModel;

	/// <summary>
	/// Enumeration de chave de configuracao usuario.
	/// </summary>
	public enum UsuarioConfiguracaoEnum
	{
		/// <summary> Tema do Translogic </summary>
		[Description("TEMA_TRANSLOGIC")]
		Tema
	}
}