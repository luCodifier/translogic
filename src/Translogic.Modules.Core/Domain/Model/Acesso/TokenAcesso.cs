﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Entidade de Token de acesso para integração com o legado
	/// </summary>
	public class TokenAcesso : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Idioma que é gravado
		/// </summary>
		public virtual string Idioma { get; set; }

		/// <summary>
		/// String do token
		/// </summary>
		public virtual string Token { get; set; }

		/// <summary>
		/// Usuário a ser gravado o token
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}