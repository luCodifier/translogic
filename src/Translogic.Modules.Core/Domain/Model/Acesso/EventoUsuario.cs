namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.ComponentModel;
	using ALL.Core.Dominio;

	/// <summary>
	/// Enumeration de C�digos de Evento de Usu�rio
	/// </summary>
	public enum EventoUsuarioCodigo
	{
		/// <summary> Evento de desativa��o de cotna </summary>
		[Description("CUD")] ContaDesativada,

		/// <summary> Evento de ativa��o de conta </summary>
		[Description("CUA")] ContaAtivada,

		/// <summary> Evento de cria��o de conta </summary>
		[Description("CUC")] ContaCriada,

		/// <summary> Evento de expira��o de senha </summary>
		[Description("SAE")] SenhaExpirou,

		/// <summary> Senha alterada pelo usu�rio </summary>
		[Description("SAA")] SenhaAlterada,

		/// <summary> Evento de erro de digita��o de senha </summary>
		[Description("UESA")] UsuarioErrouSenha,

		/// <summary> Evento de conta desbloqueada </summary>
		[Description("CD")] ContaDesbloqueada,

		/// <summary> Evento de login efetuado com sucesso </summary>
		[Description("LES")] LoginEfetuadoComSucesso,

		/// <summary> Evento de bloqueio de conta </summary>
		[Description("CUB")] ContaBloqueada,

		/// <summary> Usu�rio esqueceu a senha </summary>
		[Description("UES")] Usu�rioEsqueceuSenha
	}

	/// <summary>
	/// Entidade de Evento de Usu�rio
	/// </summary>
	public class EventoUsuario : EntidadeBase<int>
	{
		#region PROPRIEDADES
		/// <summary>
		/// C�digo do Evento de Usuario - <see cref="EventoUsuarioCodigo"/>
		/// </summary>
		public virtual EventoUsuarioCodigo Codigo { get; set; }

		/// <summary>
		/// Descri��o do evento
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}