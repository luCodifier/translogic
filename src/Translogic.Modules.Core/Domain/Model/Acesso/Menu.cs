namespace Translogic.Modules.Core.Domain.Model.Acesso
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using ALL.Core.Dominio;

    /// <summary>
    /// Entidade de internacionaliza��o de entidade base
    /// </summary>
    [Serializable]
    public class MenuI18N : EntidadeBase<int>
    {
        #region CONSTRUTORES

        /// <summary>
        /// Construtor setando a cultura default
        /// </summary>
        public MenuI18N()
        {
            Cultura = Thread.CurrentThread.CurrentCulture.Name;
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Cultura do Menu
        /// </summary>
        public virtual string Cultura { get; set; }

        /// <summary>
        /// Menu a ser internacionalizado
        /// </summary>
        public virtual Menu Menu { get; set; }

        /// <summary>
        /// Titulo do menu internacionalizado
        /// </summary>
        public virtual string Titulo { get; set; }

        #endregion
    }

    /// <summary>
    /// Entidade do menu
    /// </summary>
    [Serializable]
    public class Menu : EntidadeBase<int>
    {
        #region ATRIBUTOS

        private IList<Menu> _breadcrumbs;

        #endregion

        #region CONSTRUTORES

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        public Menu()
        {
            Filhos = new List<Menu>();
            I18N = new List<MenuI18N>();
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Indica se o menu est� Ativo.
        /// </summary>
        public virtual bool Ativo { get; set; }

        /// <summary>
        /// Gets Breadcrumbs.
        /// </summary>
        public virtual IList<Menu> Breadcrumbs
        {
            get
            {
                if (_breadcrumbs != null)
                {
                    return _breadcrumbs;
                }

                var caminho = new Stack<Menu>();

                caminho.Push(this);

                Menu pai = Pai;

                while (pai != null)
                {
                    caminho.Push(pai);

                    pai = pai.Pai;
                }

                _breadcrumbs = caminho.ToArray();

                return _breadcrumbs;
            }
        }

        /// <summary>
        /// Chave do menu.
        /// </summary>
        public virtual string Chave { get; set; }

        /// <summary>
        /// Codigo da Tela.
        /// </summary>
        public virtual int? CodigoTela { get; set; }

        /// <summary>
        /// Lista de menus filhos.
        /// </summary>
        public virtual IList<Menu> Filhos { get; set; }

        /// <summary>
        /// Lista de tradu��es.
        /// </summary>
        public virtual IList<MenuI18N> I18N { get; set; }

        /// <summary>
        /// Indica se o item de menu � tela do Legado.
        /// </summary>
        public virtual bool Legado
        {
            get { return !(Path ?? "").StartsWith("~"); }
        }

        /// <summary>
        /// Ordem do menu.
        /// </summary>
        public virtual int Ordem { get; set; }

        /// <summary>
        /// Menu Pai do menu.
        /// </summary>
        public virtual Menu Pai { get; set; }

        /// <summary>
        /// Path do item do menu
        /// </summary>
        public virtual string Path { get; set; }

        /// <summary>
        /// Titulo do menu.
        /// </summary>
        public virtual string Titulo
        {
            get { return I18N.Count == 0 ? Chave : I18N[0].Titulo; }
        }

        /// <summary>
        /// Transacao do menu.
        /// </summary>
        public virtual string Transacao { get; set; }

        #endregion
    }
}