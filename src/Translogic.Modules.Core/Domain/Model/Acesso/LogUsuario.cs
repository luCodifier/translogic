namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Entidade de Log de a��es de Usuario 
	/// </summary>
	public class LogUsuario : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Evento executado pelo Usu�rio.
		/// </summary>
		public virtual EventoUsuario Evento { get; set; }

		/// <summary>
		/// IP da m�quina do usuario.
		/// </summary>
		public virtual string IP { get; set; }

		/// <summary>
		/// Usuario a ser logado.
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Usuario do Active Directory.
		/// </summary>
		public virtual string UsuarioAD { get; set; }

		/// <summary>
		/// Gets or sets AconteceuEm.
		/// </summary>
		public virtual DateTime AconteceuEm { get; set; }

		#endregion
	}
}