﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System;
	using System.Collections.Generic;
	using System.Security.Principal;
	using ALL.Core.Dominio;
	using Estrutura;
	using Microsoft.Practices.ServiceLocation;
	using Repositories;

	/// <summary>
	/// Representa os Usuários que acessam o Sistema
	/// </summary>
	public class Usuario : EntidadeBase<int?>
	{
		#region CONSTRUTORES

		/// <summary>
		/// Inicializa a lista de permissões às transações
		/// </summary>
		public Usuario()
		{
			Permissoes = new List<Permissao>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Indica se o Usuário está ativo ou não
		/// </summary>
		public virtual bool? Ativo { get; set; }

		/// <summary>
		/// Código do Usuário.
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Data da criação do usuário.
		/// </summary>
		public virtual DateTime DataCriacao { get; set; }

		/// <summary>
		/// Email do usuário.
		/// </summary>
		public virtual string Email { get; set; }

		/// <summary>
		/// Empresa que o usuário pertence.
		/// </summary>
		public virtual EmpresaFerrovia Empresa { get; set; }

		/// <summary>
		/// Grupo que o usuário pertence.
		/// </summary>
		public virtual GrupoUsuario Grupo { get; set; }

		/// <summary>
		/// Nome completo do usuário.
		/// </summary>
		public virtual string Nome { get; set; }

		/// <summary>
		/// Lista de Permissões de transação do Usuário - <see cref="Permissao" />
		/// </summary>
		public virtual IList<Permissao> Permissoes { get; set; }

		/// <summary>
		/// Lista de itens do meu Translogic. <see cref="MeuTranslogic"/>
		/// </summary>
		public virtual IList<MeuTranslogic> MeuTranslogic { get; set; }

		/// <summary>
		/// Senha do usuário.
		/// </summary>
		public virtual string Senha { get; set; }

		/// <summary>
		/// Data da última alteração de senha.
		/// </summary>
		public virtual DateTime UltimaAlteracaoSenha { get; set; }

		/// <summary>
		/// Data do Último acesso deste usuário.
		/// </summary>
		public virtual DateTime? UltimoAcesso { get; set; }
        
        /// <summary>
        /// Retorna o Codigo do Usuario
        /// </summary>
        /// <returns>Codigo do Usuario</returns>
        public override string ToString()
        {
            return Codigo;
        }
		#endregion
	}

	/// <summary>
	/// Implementação de IIdentity para utilização no FormsAuthentication
	/// </summary>
	[Serializable]
	public class UsuarioIdentity : MarshalByRefObject, IIdentity
	{
		#region ATRIBUTOS READONLY & ESTÁTICOS

		private readonly string _authenticationType;
		private readonly bool _isAuthenticated;
		private static IUsuarioRepository _usuarioRepository;

		#endregion

		#region ATRIBUTOS

		private string _name;
		private Usuario _usuario;

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Construtor passando parâmetros
		/// </summary>
		/// <param name="name">Passando parâmetro nome</param>
		public UsuarioIdentity(string name) : this(name, true)
		{
		}

		/// <summary>
		/// Construtor passando parâmetros
		/// </summary>
		/// <param name="name">Nome da identidade</param>
		/// <param name="isAuthenticated">Está autenticado?</param>
		public UsuarioIdentity(string name, bool isAuthenticated)
		{
			_name = name;
			_isAuthenticated = isAuthenticated;
		}

		/// <summary>
		/// Construtor passando parametros
		/// </summary>
		/// <param name="name">Nome da identidade</param>
		/// <param name="authenticationType">Tipo da autenticação</param>
		/// <param name="isAuthenticated">Está autenticado?</param>
		public UsuarioIdentity(string name, string authenticationType, bool isAuthenticated)
		{
			_name = name;
			_authenticationType = authenticationType;
			_isAuthenticated = isAuthenticated;
		}

		#endregion

		#region PROPRIEDADES ESTÁTICAS

		/// <summary>
		/// Repositório de usuário a ser injetado pelo service locators.
		/// </summary>
		public static IUsuarioRepository UsuarioRepository
		{
			get
			{
				if (_usuarioRepository == null)
				{
					_usuarioRepository = ServiceLocator.Current.GetInstance<IUsuarioRepository>();
				}

				return _usuarioRepository;
			}

			set
			{
				_usuarioRepository = value;
			}
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Retorna o usuário
		/// </summary>
		public Usuario Usuario
		{
			get
			{
				if (_usuario == null)
				{

					_usuario = UsuarioRepository.ObterPorCodigo(_name);
				    
				}

				return _usuario;
			}
		}

		#endregion

		#region IIdentity MEMBROS

		/// <summary>
		/// Nome da identidade.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Tipo da autenticação
		/// </summary>
		public string AuthenticationType
		{
			get { return _authenticationType; }
		}

		/// <summary>
		/// Está autenticado?
		/// </summary>
		public bool IsAuthenticated
		{
			get { return _isAuthenticated; }
		}

		#endregion
	}
}