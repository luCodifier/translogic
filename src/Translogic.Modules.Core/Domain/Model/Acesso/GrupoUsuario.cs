﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using Diversos;
	using Estrutura;

	/// <summary>
	/// Representa o Grupo dos UsuáriosTranslogic.Modules.Core.Domain.Model
	/// </summary>
	public class GrupoUsuario : EntidadeBase<int>
	{
		#region CONSTRUTORES

		/// <summary>
		/// Construtor que inicializa a lista de Permissões
		/// </summary>
		public GrupoUsuario()
		{
			Permissoes = new List<Permissao>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Código do Grupo
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do Grupo
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Empresa a qual o grupo pertence
		/// </summary>
		public virtual IEmpresa Empresa { get; set; }

		/// <summary>
		/// Lista de Permissões de transação do Grupo - <see cref="Permissao" />
		/// </summary>
		public virtual IList<Permissao> Permissoes { get; set; }

		/// <summary>
		/// Indica se o grupo é Restrito - <see cref="bool"/>
		/// </summary>
		public virtual bool? Restrito { get; set; }

		#endregion
	}
}