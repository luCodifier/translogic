namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.Globalization;
	
	/// <summary>
	/// Culturas da aplica��o
	/// </summary>
	public class Cultura
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor inicializando com uma determinada cultura
		/// </summary>
		/// <param name="culture">Cultura a ser inicializada</param>
		public Cultura(string culture)
		{
			Culture = new CultureInfo(culture);

			if (Culture.Name.Equals("pt-BR"))
			{
				DisplayName = "Portugu�s";
			}
			else if (Culture.Name.Equals("es-AR"))
			{
				DisplayName = "Espa�ol";
			}
			else if (Culture.Name.Equals("en-US"))
			{
				DisplayName = "English";
			}
		}

		#endregion

		#region PROPRIEDADES EST�TICAS

		/// <summary>
		/// Cultura padr�o
		/// </summary>
		public static Cultura Default
		{
			get { return Todas[0]; }
		}

		/// <summary>
		/// Todas as culturas dispon�veis
		/// </summary>
		public static Cultura[] Todas
		{
			get
			{
				return new[]
				       	{
				       		new Cultura("pt-BR"),
				       		new Cultura("es-AR")
				       	};
			}
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Cultura a ser setada - <see cref="CultureInfo"/>.
		/// </summary>
		public CultureInfo Culture { get; set; }

		/// <summary>
		/// Nome a ser mostrado
		/// </summary>
		public string DisplayName { get; private set; }

		/// <summary>
		/// Nome da cultura.
		/// </summary>
		public string Name
		{
			get { return Culture.Name; }
		}

		/// <summary>
		/// LCID da cultura
		/// </summary>
		public int LCID
		{
			get { return Culture.LCID; }
		}

		#endregion
	}
}