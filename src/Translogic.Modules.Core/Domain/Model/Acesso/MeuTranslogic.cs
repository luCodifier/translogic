namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using IntegracaoSIV;

    /// <summary>
	/// Entidade MeuTranslogic
	/// </summary>
	public class MeuTranslogic : EntidadeBase<int>
	{
		/// <summary>
		/// Construtor Inicializando a Lista de itens do MeuTranslogic
		/// </summary>
		public MeuTranslogic()
		{
			Filhos = new List<MeuTranslogic>();
		}

		#region PROPRIEDADES

		/// <summary>
		/// Item de Menu do MeuTranslogic.
		/// </summary>
		public virtual Menu Menu { get; set; }

		/// <summary>
		/// Ordem do item do MeuTranslogic.
		/// </summary>
		public virtual int Ordem { get; set; }

		/// <summary>
		/// Item Pai do MeuTranslogic.
		/// </summary>
		public virtual MeuTranslogic Pai { get; set; }

		/// <summary>
		/// Titulo do MeuTranslogic.
		/// </summary>
		public virtual string Titulo { get; set; }

		/// <summary>
		/// Usuario que adicionou o Menu.
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Lista de Itens Filhos.
		/// </summary>
		public virtual IList<MeuTranslogic> Filhos { get; set; }

        /// <summary>
        /// Item do menu Siv.
        /// </summary>
        public virtual ItemMenuSiv ItemMenuSiv { get; set; }

		/// <summary>
		/// Indica se o item � Pasta.
		/// </summary>
		public virtual bool EhPasta
		{
			get
			{
				return Menu == null;
			}
		}

		#endregion
	}
}