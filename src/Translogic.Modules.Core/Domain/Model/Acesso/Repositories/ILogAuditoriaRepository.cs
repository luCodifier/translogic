namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório de log de auditoria
	/// </summary>
	public interface ILogAuditoriaRepository : IRepository<LogAuditoria, int>
	{
	}
}