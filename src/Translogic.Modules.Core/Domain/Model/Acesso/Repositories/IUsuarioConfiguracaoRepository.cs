﻿namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

	/// <summary>
	/// Inteface de repositorio de configuração de usuario
	/// </summary>
	public interface IUsuarioConfiguracaoRepository : IRepository<UsuarioConfiguracao, int>
	{
		/// <summary>
		/// Obtém configurações do usuario
		/// </summary>
		/// <param name="usuario">Usuario a ser obtida as configurações</param>
		/// <returns>Lista de configurações</returns>
		IList<UsuarioConfiguracao> ObterPorUsuario(Usuario usuario);
	}
}