namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio de menu
	/// </summary>
	public interface IMenuRepository : IRepository<Menu, int>
	{
		#region M�TODOS

		/// <summary>
		/// Obt�m os itens de menus ativos
		/// </summary>
		/// <returns>Lista de menus</returns>
		IList<Menu> ObterItensAtivos();

        /// <summary>
        /// Obt�m os itens de menus ativos
        /// </summary>
        /// <param name="codUsuario">C�digo do usuario</param>
        /// <returns>Lista de menus</returns>
        IList<Menu> ObterItensAtivos(string codUsuario);

		#endregion
	}
}