namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de resposit�rio de Token de Acesso
	/// </summary>
	public interface ITokenAcessoRepository : IRepository<TokenAcesso, int>
	{
		#region M�TODOS

		/// <summary>
		/// Obt�m Token por Usuario
		/// </summary>
		/// <param name="usuario"> Usuario gravado no token. </param>
		/// <returns>
		/// Token de acesso - <see cref="TokenAcesso"/>
		/// </returns>
		TokenAcesso ObterPorUsuario(Usuario usuario);

		#endregion
	}
}