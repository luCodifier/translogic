namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de reposit�rio de eventos do usu�rio
	/// </summary>
	public interface IEventoUsuarioRepository : IRepository<EventoUsuario, int>
	{
		#region M�TODOS
		/// <summary>
		/// Obt�m Evento pelo c�digo
		/// </summary>
		/// <param name="codigo">C�digo do Evento</param>
		/// <returns>Evento do Usuario - <see cref="EventoUsuario"/></returns>
		EventoUsuario ObterPorCodigo(EventoUsuarioCodigo codigo);

		#endregion
	}
}