namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using IntegracaoSIV;

    /// <summary>
	/// Interface de resposit�rio de MeuTranslogic
	/// </summary>
	public interface IMeuTranslogicRepository : IRepository<MeuTranslogic, int>
	{
		/// <summary>
		/// Obtem o Meu Translogic do usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio dono do Meu Translogic</param>
		/// <returns>Itens do Meu Translogic</returns>
		IList<MeuTranslogic> ObterPorUsuario(Usuario usuario);

		/// <summary>
		/// Verifica se j� existe o menu cadastrado para o usu�rio naquele n�vel
		/// </summary>
		/// <param name="usuario">Dono do Meu Translogic</param>
		/// <param name="menu">Menu a ser verificado</param>
		/// <param name="pai">Menu Pai - <see cref="Menu"/></param>
		/// <returns>Se j� existe o menu</returns>
		bool JaExiste(Usuario usuario, Menu menu, Menu pai);

		/// <summary>
		/// Verifica se j� existe o menu cadastrado para o usu�rio no root(sem n�vel)
		/// </summary>
		/// <param name="usuario">Dono do Meu Translogic</param>
		/// <param name="menu">Menu a ser verificado</param>
		/// <returns>Se j� existe o menu</returns>
		bool JaExiste(Usuario usuario, Menu menu);

        /// <summary>
        /// Verifica se j� existe o menu SIV cadastrado para o usu�rio no root(sem n�vel)
        /// </summary>
        /// <param name="usuario">Dono do Meu Translogic</param>
        /// <param name="itemMenuSiv">Menu SIV a ser verificado</param>
        /// <returns>Se j� existe o menu</returns>
        bool JaExiste(Usuario usuario, ItemMenuSiv itemMenuSiv);

		/// <summary>
		/// Obtem a �ltima ordem
		/// </summary>
		/// <param name="usuario"> Dono do MeuTranslogic </param>
		/// <param name="pai"> Qual o pai a ser considerado </param>
		/// <returns> �ltima ordem.</returns>
		int ObterUltimaOrdem(Usuario usuario, Menu pai);
	}
}