namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de Reposit�rio de Log de Usu�rio
	/// </summary>
	public interface ILogUsuarioRepository : IRepository<LogUsuario, int>
	{
		#region M�TODOS

		/// <summary>
		/// Obt�m Logs por usuario dado um per�odo
		/// </summary>
		/// <param name="usuario">Usu�rio a ser buscado os logs</param>
		/// <param name="inicial">Data Inicial</param>
		/// <param name="final">Data final</param>
		/// <returns>Lista de Logs do Usuario</returns>
		IList<LogUsuario> ObterLogsPorUsuarioNoPeriodo(Usuario usuario, DateTime inicial, DateTime final);

		/// <summary>
		/// Obt�m o �ltimo log do usu�rio
		/// </summary>
		/// <param name="usuario">Usu�rio a ser buscado o Log</param>
		/// <returns>�ltimo Log do Usu�rio</returns>
		LogUsuario ObterUltimoLogPorUsuario(Usuario usuario);

		#endregion
	}
}