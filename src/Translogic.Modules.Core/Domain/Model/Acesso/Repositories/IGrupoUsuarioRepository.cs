namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;

	/// <summary>
	/// Interface de resposit�rio de grupo de usuario.
	/// </summary>
	public interface IGrupoUsuarioRepository : IRepository<GrupoUsuario, int>
	{
		#region M�TODOS

		/// <summary>
		/// Retorna os grupos de usu�rios pelas iniciais do c�digo
		/// </summary>
		/// <param name="paginacao">Detalhes da pagina��o</param>
		/// <param name="codigo">Iniciais de pesquisa</param>
		/// <returns>Lista de Grupos Paginados - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		ResultadoPaginado<GrupoUsuario> ObterPaginadoComCodigoIniciando(DetalhesPaginacao paginacao, string codigo);

		/// <summary>
		/// Obt�m grupo com par�metro de c�digo
		/// </summary>
		/// <param name="codigo">C�digo do grupo</param>
		/// <returns>Resultado Paginado - <see cref="GrupoUsuario"/></returns>
		GrupoUsuario ObterPorCodigo(string codigo);

		#endregion
	}
}