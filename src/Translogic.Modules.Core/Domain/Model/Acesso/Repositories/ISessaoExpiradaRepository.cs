namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface com os metodos de repositório
	/// </summary>
	public interface ISessaoExpiradaRepository : IRepository<SessaoExpirada, int>
	{
	}
}