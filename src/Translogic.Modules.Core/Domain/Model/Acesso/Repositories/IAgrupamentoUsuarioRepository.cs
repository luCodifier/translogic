namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using ALL.Core.Dominio;

	/// <summary>
	/// Interface de resposit�rio de agrupamento de usuario.
	/// </summary>
	public interface IAgrupamentoUsuarioRepository : IRepository<AgrupamentoUsuario, int>
	{
		#region M�TODOS

		/// <summary>
		/// Retorna os grupos do usu�rio pelo c�digo do usu�rio.
		/// </summary>
		/// <param name="usuario">Usu�rio do grupo</param>
		/// <returns>Lista de Grupos Paginados - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		IList<GrupoUsuario> ObterGruposPorUsuario(Usuario usuario);

		/// <summary>
		/// Retorna os grupos do usu�rio pelo c�digo do usu�rio.
		/// </summary>
		/// <param name="usuario">Usu�rio do grupo</param>
		/// <param name="grupoUsuario">Grupo do usu�rio.</param>
		/// <returns>Lista de Grupos Paginados - <see cref="ResultadoPaginado{GrupoUsuario}"/></returns>
		bool VerificarUsuarioGrupo(Usuario usuario, GrupoUsuario grupoUsuario);

		#endregion
	}
}