using System;

namespace Translogic.Modules.Core.Domain.Model.Acesso.Repositories
{
	using System.Collections;
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;
	using Interfaces.Acesso;

    /// <summary>
	/// Interface de reposit�rio de usu�rio
	/// </summary>
	public interface IUsuarioRepository : IRepository<Usuario, int?>
	{
		#region M�TODOS

		/// <summary>
		/// Obt�m o usu�rio pelo c�digo
		/// </summary>
		/// <param name="codigo">C�digo do usu�rio</param>
		/// <returns>Objeto <see cref="Usuario"/></returns>
		Usuario ObterPorCodigo(string codigo);

		#endregion

		/// <summary>
		/// Obt�m a lista de permiss�es da transa��o
		/// </summary>
		/// <param name="transacao"> Transa��o a ser verificada a permiss�o. </param>
		/// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
		/// <returns> Lista de permiss�es </returns>
		IList ObterPermissoes(string transacao, Usuario usuario);

		/// <summary>
		/// Obt�m a lista de permiss�es da transa��o
		/// </summary>
		/// <param name="transacao"> Transa��o que a a��o pertence. </param>
		/// <param name="acao"> A��o a ser verificada a permissao. </param>
		/// <param name="usuario">Usu�rio a ser capturado as transa��es.</param>
		/// <returns> Lista de permiss�es </returns>
		IList ObterPermissoes(string transacao, string acao, Usuario usuario);

        /// <summary>
        /// Obtem a lista de Usuarios por nome
        /// </summary>
        /// <param name="nome">nome do usuario</param>
        /// <returns>lista de usuarios</returns>
	    IList<UsuarioDto> ObterPorNome(string nome);

        /// <summary>
        /// Obtem a ultima data de login com sucesso
        /// </summary>
        /// <param name="usuario">Usu�rio para verifica��o de data de login</param>
        /// <returns>DateTime</returns>
        DateTime? ObterDataUltimoAcessoSoxs(Usuario usuario);

        /// <summary>
        /// Obtem o nro de dias para expirar a senha
        /// </summary>
        /// <param name="usuario">Usu�rio para verifica��o de data da senha</param>
        /// <returns></returns>
        int ObterDiasAlterar2D(Usuario usuario);

        /// <summary>
        /// Verifica as ultimas 5 senhas
        /// </summary>
        /// <param name="usuario">Objeto usuario</param>
        /// <param name="senhaNova">Senha nova</param>
        /// <returns></returns>
        bool VerificarUltimas5Senhas(Usuario usuario, String senhaNova);
	}
}