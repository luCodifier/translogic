﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.Collections.Generic;
	using ALL.Core.Dominio;

	/// <summary>
	/// Permissão de acesso da Transação ao Grupo de Usuários
	/// </summary>
	public class Permissao : EntidadeBase<int>
	{
		#region ATRIBUTOS PRIVADOS
		private IList<Usuario> _usuarios;
		private IList<GrupoUsuario> _grupos;
		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Construtor que inicializará a lista de Permissões
		/// </summary>
		public Permissao()
		{
			_usuarios = new List<Usuario>();
			_grupos = new List<GrupoUsuario>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// <see cref="Operacao"/> que estará sendo concedida/revogada o acesso
		/// </summary>
		public Operacao Operacao { get; set; }

		/// <summary>
		/// Lista de <see cref="GrupoUsuario"/> que está sendo concedida o acesso
		/// </summary>
		public IList<GrupoUsuario> Grupos
		{
			get { return _grupos; }
			set { _grupos = value; }
		}

		/// <summary>
		/// Lista de <see cref="Usuario"/> que está sendo concedido o acesso
		/// </summary>
		public IList<Usuario> Usuarios
		{
			get { return _usuarios; }
			set { _usuarios = value; }
		}

		#endregion
	}
}