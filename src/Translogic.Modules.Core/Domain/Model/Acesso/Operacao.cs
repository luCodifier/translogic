﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System.ComponentModel;
	using ALL.Core.Dominio;

	/// <summary>
	/// Enumeration que representa a o tipo da operação 
	/// </summary>
	public enum TipoOperacaoEnum
	{
		/// <summary>
		/// Transação de acesso
		/// </summary>
		[Description("TRANSACAO")]
		Transacao,

		/// <summary>
		/// Ação da Transação
		/// </summary>
		[Description("ACAO")]
		Acao
	}

	/// <summary>
	/// Enumeration que representa os sistemas da ALL
	/// </summary>
	public enum SistemaEnum
	{
		/// <summary>
		/// Sistema de containers
		/// </summary>
		[Description("CONT")] Container,

		/// <summary>
		/// Sistema de Via Permanente
		/// </summary>
		[Description("SIV")] SIV,

		/// <summary>
		/// Sistema SOL
		/// </summary>
		[Description("SOL")] SOL,

		/// <summary>
		/// Translogic Ferroviario
		/// </summary>
		[Description("TLFER")] TLFerro,

		/// <summary>
		/// Translogic Rodoviario
		/// </summary>
		[Description("TLROD")] TLRodo
	}

	/// <summary>
	/// Representa a funcionalidade de Autorização de acesso às páginas
	/// </summary>
	public class Operacao : EntidadeBase<int>
	{
		#region CONSTRUTORES

		/// <summary>
		/// Construtor vazio
		/// inicializa a lista de ações da transação
		/// </summary>
		public Operacao()
		{
		}

		#endregion

		#region PROPRIEDADES
        
		/// <summary>
		/// Indica se a transação está ativa ou não
		/// </summary>
		public virtual bool Ativo { get; set; }

		/// <summary>
		/// Código da Transação que será colocado na Action para verificar o acesso
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da transação
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica a Transação "Pai" se houver
		/// </summary>
		public virtual Operacao Pai { get; set; }
		
		/// <summary>
		/// Indica o sistema que a transação pertence - <see cref="SistemaEnum"/>
		/// </summary>
		public virtual SistemaEnum Sistema { get; set; }

		/// <summary>
		/// Tipo da Operação - <see cref="TipoOperacaoEnum"/>
		/// </summary>
		public virtual TipoOperacaoEnum TipoOperacao { get; set; }

		#endregion
	}
}