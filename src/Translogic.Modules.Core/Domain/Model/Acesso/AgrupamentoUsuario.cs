﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o Agrupamento de Usuario
	/// </summary>
	public class AgrupamentoUsuario : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Grupo do usuário
		/// </summary>
		public virtual GrupoUsuario Grupo { get; set; }

		/// <summary>
		/// Usuário do grupo
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}