﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	/// <summary>
	/// Classe estática para "facilitar" na utilização de <see cref="Operacao"/>
	/// </summary>
	public static class Acao
	{
		/// <summary>
		/// Ação de Pesquisar
		/// </summary>
		public static readonly string Pesquisar = "Pesquisar";

		/// <summary>
		/// Ação de Limpar
		/// </summary>
		public static readonly string Limpar = "Limpar";

		/// <summary>
		/// Ação de Salvar
		/// </summary>
		public static readonly string Salvar = "Salvar";
	}
}
