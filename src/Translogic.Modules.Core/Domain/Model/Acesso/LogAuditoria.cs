namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System;
	using System.ComponentModel;
	using ALL.Core.Dominio;

	/// <summary>
	/// Tipo de Opera��o da Auditoria
	/// </summary>
	public enum OperacaoAuditoria
	{
		/// <summary>
		/// Tipo de Auditoria: Inser��o
		/// </summary>
		[Description("I")] Insercao,

		/// <summary>
		/// Tipo de Auditoria: Atualiza��o
		/// </summary>
		[Description("A")] Atualizacao,

		/// <summary>
		/// Tipo de Auditoria: Exclus�o
		/// </summary>
		[Description("E")] Exclusao
	}

	/// <summary>
	/// Interface de Auditoria
	/// </summary>
	public interface IAuditoria
	{
	}

	/// <summary>
	/// Interface de Auditoria Detalhada
	/// </summary>
	public interface IAuditoriaDetalhada : IAuditoria
	{
	}

	/// <summary>
	/// Entidade de Log de Auditoria
	/// </summary>
	public class LogAuditoria : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Alteracoes feitas na entidade.
		/// </summary>
		public virtual string Alteracoes { get; set; }

		/// <summary>
		/// Id da Entidade.
		/// </summary>
		public virtual string EntidadeId { get; set; }

		/// <summary>
		/// Operacao da Auditoria - <see cref="OperacaoAuditoria"/>.
		/// </summary>
		public virtual OperacaoAuditoria Operacao { get; set; }

		/// <summary>
		/// Data de Ocorrencia do Evendo.
		/// </summary>
		public virtual DateTime Quando { get; set; }

		/// <summary>
		/// Tipo da Entidade auditada.
		/// </summary>
		public virtual Type TipoEntidade { get; set; }

		/// <summary>
		/// Usuario que executou a opera��o.
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}