namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System;

	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de configura��o de usuario
	/// </summary>
	public class UsuarioConfiguracao : EntidadeBase<int>
	{
		/// <summary>
		/// Usu�rio propriet�rio da configura��o
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Chave da configura��o
		/// </summary>
		public virtual UsuarioConfiguracaoEnum Chave { get; set; }

		/// <summary>
		/// Valor da configura��o
		/// </summary>
		public virtual string Valor { get; set; }

		/// <summary>
		/// Data de cadastro
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }
	}
}