﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de associação dos usuários com os grupos
	/// </summary>
	/// <remarks>
	/// Esta classe não deveria existir, 
	/// porém na tabela legada já existia a PK que impossibilitava a criação do many-to-many
	/// </remarks>
	public class AssociacaoGrupoUsuario : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// <see cref="GrupoUsuario"/> da associação
		/// </summary>
		public GrupoUsuario GrupoUsuario { get; set; }

		/// <summary>
		/// <see cref="Usuario"/> da associação
		/// </summary>
		public Usuario Usuario { get; set; }

		#endregion
	}
}