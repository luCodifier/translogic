﻿namespace Translogic.Modules.Core.Domain.Model.Acesso
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o log de uma sessão quando expirar
	/// </summary>
	public class SessaoExpirada : EntidadeBase<int>
	{
		/// <summary>
		/// Ip do cliente
		/// </summary>
		public virtual string IpCliente { get; set; }

		/// <summary>
		/// Ip do servidor que disparou a mensagem
		/// </summary>
		public virtual string IpServidor { get; set; }

		/// <summary>
		/// Código do usuario que expirou a sessão
		/// </summary>
		public virtual string CodigoUsuario { get; set; }

		/// <summary>
		/// Tempo restante em segundos
		/// </summary>
		public virtual int TempoRestante { get; set; }

		/// <summary>
		/// Data de ocorrencia da expiração
		/// </summary>
		public virtual DateTime DataOcorrencia { get; set; }
	}
}
