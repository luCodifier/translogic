namespace Translogic.Modules.Core.Domain.Model.Acesso.DTO
{
	using System.Collections.Generic;

	/// <summary>
	/// Representa um Menu simples para ser serializado na view
	/// </summary>
	public class MenuSimples
	{
		#region PROPRIEDADES

		/// <summary>
		/// Lista de Menus Filhos.
		/// </summary>
		public IList<MenuSimples> Filhos { get; set; }

		/// <summary>
		/// Id do Menu.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Titulo do Menu.
		/// </summary>
		public string Titulo { get; set; }

		/// <summary>
		/// Codigo da Tela.
		/// </summary>
		public int? CodigoTela { get; set; }

		/// <summary>
		/// Path da Tela.
		/// </summary>
		public string Path { get; set; }

        /// <summary>
        /// Sistema Origem da Tela.
        /// </summary>
        public string SistemaOrigem { get; set; }

		#endregion
	}
}