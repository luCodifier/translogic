namespace Translogic.Modules.Core.Domain.Model.Acesso.DTO
{
	using System.Collections.Generic;

	/// <summary>
	/// Model de MeuTranslogicSimples para ser serializado na view
	/// </summary>
	public class MeuTranslogicSimples
	{
		#region PROPRIEDADES

		/// <summary>
		/// Id do MeuTranslogic.
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// Indica quando � pasta.
		/// </summary>
		public virtual bool EhPasta { get; set; }

		/// <summary>
		/// Titulo a ser exibido.
		/// </summary>
		public virtual string Titulo { get; set; }

		/// <summary>
		/// Codigo da Tela.
		/// </summary>
		public virtual int? CodigoTela { get; set; }

		/// <summary>
		/// Itens filhos do MeuTranslogic
		/// </summary>
		public IList<MeuTranslogicSimples> Filhos { get; set; }

		/// <summary>
		/// Ordem do item do MeuTranslogic.
		/// </summary>
		public int Ordem { get; set; }

        /// <summary>
        /// Sistema Origem da Tela.
        /// </summary>
        public string SistemaOrigem { get; set; }

		#endregion
	}
}