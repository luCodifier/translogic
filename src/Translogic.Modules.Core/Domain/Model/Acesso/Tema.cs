namespace Translogic.Modules.Core.Domain.Model.Acesso
{	
	/// <summary>
	/// Tema da aplica��o
	/// </summary>
	public class Tema
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor inicializando com um determinado tema
		/// </summary>
		/// <param name="tema">Tema a ser inicializada</param>
		public Tema(string tema)
		{
            if (tema.Equals("default"))
			{
				DisplayName = "Padr�o";
			    Css = string.Empty;
			}
            else if (tema.Equals("dark"))
			{
				DisplayName = "Dark";
                Css = "xtheme-slickness";
			}

            Codigo = tema;
		}

		#endregion

		#region PROPRIEDADES EST�TICAS

		/// <summary>
		/// Cultura padr�o
		/// </summary>
		public static Tema Default
		{
			get { return Todos[0]; }
		}

		/// <summary>
		/// Todos os temas dispon�veis
		/// </summary>
		public static Tema[] Todos
		{
			get
			{
				return new[]
				       	{
				       		new Tema("default"),
				       		new Tema("dark")
				       	};
			}
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Nome a ser mostrado
		/// </summary>
		public string DisplayName { get; private set; }

	    /// <summary>
	    /// Codigo do tema.
	    /// </summary>
        public string Codigo { get; private set; }

        /// <summary>
        /// Css do tema.
        /// </summary>
        public string Css { get; private set; }

		#endregion
	}
}