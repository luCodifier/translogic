﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Edi.Enum
{
    using System.ComponentModel;

    public enum EdiTipoErroEnum
    {
        [Description("Nota Fiscal não recebida")]
        NotaFiscalNaoRecebida = 1
    }
}