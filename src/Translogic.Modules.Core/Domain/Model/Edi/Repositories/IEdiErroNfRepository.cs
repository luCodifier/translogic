﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Edi.Repositories
{
    public interface IEdiErroNfRepository : IRepository<EdiErroNf, int>
    {
        IList<EdiErroNf> ObterPorIdEdiErro(int id);
        bool ExisteChaveNfeEdi(string chaveNfe);
    }
}
