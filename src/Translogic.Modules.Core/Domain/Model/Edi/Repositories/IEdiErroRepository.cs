﻿namespace Translogic.Modules.Core.Domain.Model.Edi.Repositories
{
    using System;
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IEdiErroRepository : IRepository<EdiErro, int>
    {
        ResultadoPaginado<EdiErroResultPesquisaDto> ObterEdiErrosPesquisa(
            DetalhesPaginacao detalhesPaginacao,
            DateTime dataInicial,
            DateTime dataFinal,
            string fluxo,
            string[] vagoes,
            string responsavelEnvio);

        IList<MensagemRecebimentoDto> ObterMensagensRecebimentoErro(DateTime dataInicial, DateTime dataFinal);
        EdiErro ObterEdiErro(int idMensagemRecebimento);
        List<MensagemRecebimentoDto> ObterMensagensRecebimentoParaReprocessar(List<decimal> listaIdMensagemRecebimento);
        List<string> RetornarChaves(string ids); 
        void AtualizarMensagensRecebimentoParaReprocessar(List<decimal> listaIdMensagemRecebimento);
        void AtualizarEdiErrosProcessados(List<decimal> ids);
    }
}
