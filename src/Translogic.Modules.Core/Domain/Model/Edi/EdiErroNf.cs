﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Edi
{
    public class EdiErroNf : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Erro de EDI
        /// </summary>
        public virtual EdiErro EdiErro { get; set; }

        /// <summary>
        /// Chave da Nota vinculada ao Erro de EDI
        /// </summary>
        public virtual string ChaveNfe { get; set; }

        /// <summary>
        /// Data de regularização do erro de EDI
        /// </summary>
        public virtual DateTime? DataRegularizacao { get; set; }

        #endregion
    }
}