﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Edi.Enum;

namespace Translogic.Modules.Core.Domain.Model.Edi
{
    public class EdiErro : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Tipo do erro de EDI
        /// </summary>
        public virtual int TipoErro { get; set; }

        public virtual EdiTipoErroEnum TipoErroEnum
        {
            get
            {
                EdiTipoErroEnum resultado = EdiTipoErroEnum.NotaFiscalNaoRecebida;
                switch (TipoErro)
                {
                    case (int)EdiTipoErroEnum.NotaFiscalNaoRecebida:
                        resultado = EdiTipoErroEnum.NotaFiscalNaoRecebida;
                        break;
                }

                return resultado;
            }
        }

        /// <summary>
        /// Id da mensagem de recebimento
        /// </summary>
        public virtual int IdMensagemRecebimento { get; set; }

        /// <summary>
        /// Número do Vagão vinculado ao erro de EDI
        /// </summary>
        public virtual string Vagao { get; set; }

        /// <summary>
        /// Código do fluxo vinculado ao erro de EDI
        /// </summary>
        public virtual string Fluxo { get; set; }

        /// <summary>
        /// Data do erro de EDI
        /// </summary>
        public virtual DateTime DataErro { get; set; }

        /// <summary>
        /// Data de envio do EDI com erro
        /// </summary>
        public virtual DateTime DataEnvio { get; set; }

        /// <summary>
        /// Responsavel pelo envio do EDI com erro
        /// </summary>
        public virtual string ResponsavelEnvio { get; set; }

        /// <summary>
        /// Reprocessado Sim ou Não
        /// </summary>
        public virtual string Reprocessado { get; set; }

        #endregion
    }
}