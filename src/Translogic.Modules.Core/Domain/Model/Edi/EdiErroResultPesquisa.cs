﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Edi
{
    public class EdiErroResultPesquisa
    {
        public decimal IdMensagemRecebimento { get; set; }
        public decimal IdErroEdiNfe { get; set; }
        public string ChaveNfe { get; set; }
        public DateTime? DataRegularizacao { get; set; }
    }
}