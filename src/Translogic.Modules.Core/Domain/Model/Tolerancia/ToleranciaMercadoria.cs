﻿
namespace Translogic.Modules.Core.Domain.Model.Tolerancia
{
    using ALL.Core.Dominio;
    using System;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

    public class ToleranciaMercadoria : EntidadeBase<int>
    {
        /// <summary>
        /// Valor da tolerancia
        /// </summary>
        public virtual decimal Tolerancia { get; set; }
        /// <summary>
        /// Mercadoria
        /// </summary>
        public virtual Mercadoria Mercadoria { get; set; }

        /// <summary>
        /// Usuário que criou
        /// </summary>
        public virtual string Usuario { get; set; }

        public ToleranciaMercadoria()
        {
            VersionDate = DateTime.Now;
        }

    }
}