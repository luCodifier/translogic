﻿namespace Translogic.Modules.Core.Domain.Model.Tolerancia.Repositories
{
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using System;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IToleranciaMercadoriaRepository : IRepository<ToleranciaMercadoria, int>
    {
        ResultadoPaginado<ToleranciaMercadoriaDto> ObterConsulta(DetalhesPaginacaoWeb detalhesPaginacaoWeb, DateTime? dataInicial, DateTime? dataFinal,
           string mercadoria);

        ToleranciaMercadoria Obter(int id);

        void Salvar(ToleranciaMercadoria toleranciaMercadoria);
        void Excluir(ToleranciaMercadoria toleranciaMercadoria);
    }
}
