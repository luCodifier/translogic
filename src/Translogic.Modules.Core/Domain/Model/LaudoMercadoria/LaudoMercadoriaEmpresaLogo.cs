﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria
{
    using ALL.Core.Dominio;

    public class LaudoMercadoriaEmpresaLogo : EntidadeBase<int?>
    {   
        /// <summary>
        /// Imagem da logo da empresa
        /// </summary>
        public virtual byte[] Logo { get; set; }
    }
}