﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria
{
    public class LaudoMercadoriaClassificacao
    {
        public string Descricao { get; set; }
        public decimal Porcentagem { get; set; }
        public decimal? Ordem { get; set; }
    }
}