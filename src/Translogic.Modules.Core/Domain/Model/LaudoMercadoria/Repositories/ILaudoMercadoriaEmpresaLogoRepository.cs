﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de repositório de LaudoMercadoriaEmpresaLogo
    /// </summary>
    public interface ILaudoMercadoriaEmpresaLogoRepository : IRepository<LaudoMercadoriaEmpresaLogo, int?>
    {
    }
}