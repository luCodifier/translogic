﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria
{
    using System;
    using System.Collections.Generic;

    public class LaudoMercadoriaImpressao
    {
        public string CodigoVagao { get; set; }
        public DateTime DataClassificacao { get; set; }
        public string NumeroLaudo { get; set; }
        public string NumeroOsLaudo { get; set; }
        public decimal PesoLiquido { get; set; }
        public string Destino { get; set; }
        public string AutorizadoPor { get; set; }
        public string TipoLaudo { get; set; }
        public string EmpresaNome { get; set; }
        public string Produto { get; set; }
        public string NumeroNfe { get; set; }
        public string ChaveNfe { get; set; }
        public string ChaveNfeFormatada
        {
            get
            {
                var chaveNfe = ChaveNfe;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(chaveNfe))
                {
                    chaveNfe = chaveNfe.PadLeft(44, '0');
                    for (var i = 0; i < 11; i++)
                    {
                        // Inserindo espaços a cada 4 caracteres
                        resultado = String.IsNullOrEmpty(resultado)
                                        ? String.Format("{0}", chaveNfe.Substring(0, 4))
                                        : String.Format("{0} {1}", resultado, chaveNfe.Substring(i * 4, 4));
                    }
                }

                return resultado;
            }
        }
        public string Observacao { get; set; }
        public string DescricaoProduto { get; set; }

        public virtual LaudoMercadoriaEmpresa EmpresaGeradora { get; set; }

        public virtual LaudoMercadoriaCliente Cliente { get; set; }

        public virtual LaudoMercadoriaClassificador Classificador { get; set; }

        public virtual List<LaudoMercadoriaClassificacao> Classificacoes { get; set; }
    }
}