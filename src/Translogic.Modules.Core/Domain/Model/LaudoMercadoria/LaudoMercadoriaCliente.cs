﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria
{
    using System;

    public class LaudoMercadoriaCliente
    {
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string CnpjFormatado
        {
            get
            {
                var cnpj = Cnpj;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cnpj))
                {
                    cnpj = cnpj.Replace(".", String.Empty).Replace("/", String.Empty).Replace("-", String.Empty).PadLeft(14, '0');
                    resultado = String.Format("{0}.{1}.{2}/{3}-{4}", cnpj.Substring(0, 2), cnpj.Substring(2, 3), cnpj.Substring(5, 3), cnpj.Substring(8, 4), cnpj.Substring(12, 2));
                }

                return resultado;
            }
        }
    }
}