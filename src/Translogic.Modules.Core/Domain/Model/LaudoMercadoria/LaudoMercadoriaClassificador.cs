﻿namespace Translogic.Modules.Core.Domain.Model.LaudoMercadoria
{
    using System;

    public class LaudoMercadoriaClassificador
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string CpfFormatado
        {
            get
            {
                var cpf = Cpf;
                var resultado = String.Empty;
                if (!String.IsNullOrEmpty(cpf))
                {
                    cpf = cpf.Replace(".", String.Empty).Replace("-", String.Empty).PadLeft(11, '0');
                    resultado = String.Format("{0}.{1}.{2}-{3}", cpf.Substring(0, 3), cpf.Substring(3, 3), cpf.Substring(6, 3), cpf.Substring(9, 2));
                }

                return resultado;
            }
        }
    }
}