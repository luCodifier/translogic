﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using ALL.Core.Dominio;

    public class SolicitacaoVooMotivo : EntidadeBase<Decimal>
    {
        // Tabelas mapeadas
        // VAGAO E EQUIPAMENTOS = EVENTO_MOTIVO
        // LOCO = EVENTO_LOCO_VOADOR_MOTIVO        
        public string Descricao { get; set; }
    }
}