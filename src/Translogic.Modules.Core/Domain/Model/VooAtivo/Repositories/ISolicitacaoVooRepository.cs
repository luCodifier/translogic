﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface ISolicitacaoVooRepository : IRepository<SolicitacaoVooAtivo, int>
    {
        /// <summary>
        /// Pesquisar ativos avaliar
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="id"></param>
        /// <param name="dtPeriodoInicio"></param>
        /// <param name="dtPeriodoFinal"></param>
        /// <param name="Solicitante"></param>
        /// <param name="StatusChamado"></param>
        /// <param name="tipoSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivosAvaliarDto> BuscarAtivosVooAvaliar(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int? id, DateTime? dtPeriodoInicio, DateTime? dtPeriodoFinal, string Solicitante, int? idStatusChamado, int? idTipoSolicitacao);
        
    }
}
