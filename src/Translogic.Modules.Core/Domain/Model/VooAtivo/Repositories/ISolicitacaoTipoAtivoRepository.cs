﻿
namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface ISolicitacaoTipoAtivoRepository : IRepository<SolicitacaoTipoAtivo, int>
    {
    }
}
