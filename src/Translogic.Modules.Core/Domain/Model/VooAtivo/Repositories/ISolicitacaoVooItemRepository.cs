﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Util;
    using System.Data;

    public interface ISolicitacaoVooItemRepository : IRepository<SolicitacaoVooAtivoItem, int>
    {
        /// <summary>
        /// Buscar EOT disponíveis para Voo
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="ativos"></param>
        /// <param name="patio"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarEotDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string os);

        /// <summary>
        /// Buscar locomotivas disponíveis para Voo
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="ativos"></param>
        /// <param name="patio"></param>
        /// <param name="linha"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarLocomotivasDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string linha, string os);

        /// <summary>
        /// Buscar vagões disponíveis para Voo
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="ativos"></param>
        /// <param name="patio"></param>
        /// <param name="linha"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarVagoesDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string patio, string linha, string os);

        /// <summary>
        /// Buscar vagões de refaturamento disponíveis para Voo
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="ativos"></param>
        /// <param name="patio"></param>
        /// <param name="linha"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarVagoesRefaturamentoDisponiveisVoo(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string ativos, string os);

        /// <summary>
        /// Buscar os itens da solicitação VAGAO
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarVagaoPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id);

        /// <summary>
        /// Buscar os itens da solicitação LOCOMOTIVA
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarLocomotivaPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id);

        /// <summary>
        /// Buscar os itens da solicitação EOT
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarEotPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id);

        /// <summary>
        /// Buscar os itens da solicitação VAGAO
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="idSolicitacao"></param>
        /// <returns></returns>
        ResultadoPaginado<SolicitacaoVooAtivoItemDto> BuscarItemsAtivosVooAvaliarVagaoRefaturamentoPorID(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int id);

        /// <summary>
        /// Buscar dados para execução de PKGs
        /// </summary>
        /// <param name="idAtivo"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        SolicitacaoVooAtivoItemPkgDto BuscarAtivosEotsParaPkgs(string ativo, string patioDestino);

        /// <summary>
        /// Buscar dados para execução de PKGs
        /// </summary>
        /// <param name="idAtivo"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        SolicitacaoVooAtivoItemPkgDto BuscarAtivosLocomotivasParaPkgs(string ativo, string patioDestino);

        /// <summary>
        /// Buscar dados para execução de PKGs
        /// </summary>
        /// <param name="idAtivo"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        SolicitacaoVooAtivoItemPkgDto BuscarAtivosVagoesParaPkgs(string ativo, string patioDestino);

        /// <summary>
        /// Buscar dados para execução de PKGs
        /// </summary>
        /// <param name="idAtivo"></param>
        /// <param name="patioDestino"></param>
        /// <returns></returns>
        SolicitacaoVooAtivoItemPkgDto BuscarAtivosVagoesRefaturamentoParaPkgs(string ativo, string patioDestino);    

        /// <summary>
        /// Aprovar Voo Ativos EOT PKG.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        string AprovarEotPKG(string xml);

        /// <summary>
        /// Aprovar Voo Ativos LOCOMOTIVA PKG.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        string AprovarLocomotivaPKG(string xml);

        /// <summary>
        /// Aprovar Voo Ativos VAGAO PKG.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        string AprovarVagaoPKG(string xml);

        /// <summary>
        /// Preparar Voo Ativos VAGAO REFATURAMENTO PKG.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        string PrepararVagaoRefaturamentoPKG(string xml);

        /// <summary>
        /// Buscar quantidade de itens ativos do VOO
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        decimal BuscarItemsVagoesPendentes(int id);

        /// <summary>
        /// Buscar quantidade de itens ativos do VOO
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        decimal BuscarItemsLocomotivasPendentes(int id);

        /// <summary>
        /// Buscar quantidade de itens ativos do VOO
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        decimal BuscarItemsEotsPendentes(int id);

        DataTable BuscarDadosPrepararRefaturamento(int idAtivo);

        /// <summary>
        /// Buscar a ultima data do evento do vagão
        /// </summary>
        /// <param name="ativo"></param>
        /// <returns></returns>
        DateTime BuscarDataUltimoEventoVagao(string ativo);

    }
}
