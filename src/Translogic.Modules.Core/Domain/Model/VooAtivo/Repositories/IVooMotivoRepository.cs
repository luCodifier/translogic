﻿
namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Util;

    public interface IVooMotivoRepository
    {
        /// <summary>
        /// Buscar lista de motivos de acordo com o tipo de ativo
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        IList<SolicitacaoVooMotivo> BuscarMotivos(EnumTipoAtivo tipo);

        /// Buscar ID Motivo por descrição motivo
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="motivo"></param>
        /// <returns>retorna ID da descrição</returns>
        decimal BuscaIDPorDescricaoMotivo(EnumTipoAtivo tipo, string motivo);
    }
}
