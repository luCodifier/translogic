﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    public class SolicitacaoTipoAtivo : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Tipo Ativo
        /// </summary>
        public virtual string Descricao { get; set; }
      
        #endregion
    }
}    