﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;
	using Codificador;

	/// <summary>
	/// Representa a associação de um Evento ao Trem
	/// </summary>
	public class SolicitacaoVooAtivo : EntidadeBase<int>
	{
		#region PROPRIEDADES

        /// <summary>
        /// Justificativa Solicitacao
        /// </summary>
        public virtual string Justificativa { get; set; }

        /// <summary>
        /// Motivo Solicitacao
        /// </summary>
        public virtual string Motivo { get; set; }

        /// <summary>
        ///  Linha Destino
        /// </summary>
        public virtual string PatioDestino { get; set; }

        /// <summary>
        ///  Linha Destino
        /// </summary>
        public virtual string LinhaDestino { get; set; }

        /// <summary>
        /// Solicitante
        /// </summary>
        public virtual string Solicitante { get; set; }

        /// <summary>
        /// Area Solicitante
        /// </summary>
        public virtual string PatioSolicitante { get; set; }

        /// <summary>
        /// Data e hora da chegada
        /// </summary>
        public virtual DateTime DataHoraChegada { get; set; }

        /// <summary>
        /// Area Responsavel
        /// </summary>
        public virtual string AreaResponsavel { get; set; }

        /// <summary>
        /// Justificativa CCO
        /// </summary>
        public virtual string JustificativaCCO { get; set; }

        /// <summary>
        /// Observação CCO
        /// </summary>
        public virtual string ObservacaoCCO { get; set; }

        /// <summary>
		/// Data Solicitacao
		/// </summary>
		public virtual DateTime DataSolicitacao { get; set; }

        /// <summary>
        ///  Data e hora da ultima alteração
        /// </summary>
        public virtual DateTime DataUltimaAlteracao { get; set; }

        /// <summary>
        ///  Versão
        /// </summary>
        public virtual DateTime VersionDate { get; set; }

        /// <summary>
        /// Usuário que criou
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
		/// <see cref="Solicitacao Voo Status"/> de origem
		/// </summary>
		public virtual SolicitacaoVooAtivoStatus SolicitacaoVooAtivoStatus { get; set; }

        /// <summary>
		/// <see cref="Tipo Status Solicitação "/> de origem
		/// </summary>
		public virtual SolicitacaoTipoAtivo SolicitacaoTipoAtivo { get; set; }

		#endregion
	}
}