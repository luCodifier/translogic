﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using ALL.Core.Dominio;

    public class SolicitacaoVooAtivoStatus : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Status Solicitação Voo
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}