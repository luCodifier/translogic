﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    public class SolicitacaoVooAtivoItem : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        ///  Ativo
        /// </summary>
        public virtual int IdAtivo { get; set; }

        /// <summary>
        ///  Ativo
        /// </summary>
        public virtual string Ativo { get; set; }

        /// <summary>
        /// Data e hora do evento
        /// </summary>
        public virtual DateTime DataEvento { get; set; }

        /// <summary>
        /// Patio Origem
        /// </summary>
        public virtual string PatioOrigem { get; set; }

        /// <summary>
        /// Patio Destino
        /// </summary>
        public virtual string PatioDestino { get; set; }

        /// <summary>
        ///  Condições Uso
        /// </summary>
        public virtual string CondicaoUso { get; set; }

        /// <summary>
        ///  Situação
        /// </summary>
        public virtual string Situacao { get; set; }

        /// <summary>
        ///  Local
        /// </summary>
        public virtual string Local { get; set; }

        /// <summary>
        ///  Responsável
        /// </summary>
        public virtual string Responsavel { get; set; }

        /// <summary>
        ///  Lotacao
        /// </summary>
        public virtual string Lotacao { get; set; }

        /// <summary>
        ///  Intercâmbio
        /// </summary>
        public virtual string Intercambio { get; set; }

        /// <summary>
        ///  EstadoFuturo
        /// </summary>
        public virtual string EstadoFuturo { get; set; }

        /// <summary>
        /// <see cref="Solicitacao Voo Status Item"/> de origem
        /// </summary>
        public virtual SolicitacaoVooAtivoStatusItem SolicitacaoVooAtivoStatusItem { get; set; }

        /// <summary>
        /// <see cref="Solicitacao Voo Ativos "/> de origem
        /// </summary>
        public virtual SolicitacaoVooAtivo SolicitacaoVooAtivo { get; set; }

        /// <summary>
        /// <see cref="Tipo Status Solicitação "/> de origem
        /// </summary>
        public virtual SolicitacaoTipoAtivo SolicitacaoTipoAtivo { get; set; }

        /// <summary>
        ///  Número Sequência
        /// </summary>
        //public virtual int NumSeq { get; set; }

        #endregion
    }
}