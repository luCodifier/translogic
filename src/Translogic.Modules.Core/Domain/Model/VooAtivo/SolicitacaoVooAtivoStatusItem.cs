﻿namespace Translogic.Modules.Core.Domain.Model.VooAtivo
{
    using System;
    using ALL.Core.Dominio;

    public class SolicitacaoVooAtivoStatusItem : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Status Solicitação Item Voo
        /// </summary>
        public virtual string Descricao { get; set; }

        #endregion
    }
}