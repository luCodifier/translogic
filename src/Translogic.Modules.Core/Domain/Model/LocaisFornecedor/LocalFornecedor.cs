﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Fornecedor;
using Translogic.Modules.Core.Domain.Model.TipoServicos;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Model.LocaisFornecedor
{
    public class LocalFornecedor : EntidadeBase<int>
    {
        /// <summary>
        /// FornecedoresOs
        /// </summary>
        public virtual FornecedorOs FornecedoresOs { get; set; }

        /// <summary>
        /// TipoServicos
        /// </summary>
        public virtual TipoServico TipoServicos { get; set; }

        /// <summary>
        /// Area Operacional
        /// </summary>
        public virtual EstacaoMae EstacaoMae { get; set; }

        
    }
}