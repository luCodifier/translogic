﻿
namespace Translogic.Modules.Core.Domain.Model.LocaisFornecedor.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Util;


    public interface ILocalFornecedorRepository: IRepository<LocalFornecedor, int>
    {
        //IList<LocalFornedorDto> ObterLocaisFornencedor();
        ResultadoPaginado<LocalFornecedorDto> ObterLocaisFornecedorPorId(DetalhesPaginacaoWeb detalhesPaginacaoWeb, int idLocal);

        IList<LocalFornecedorDto> ObterLocalFornencedor(string local);

        /// <summary>
        /// Retorna o objeto Estação serviço, pelo nome do Local da estação
        /// </summary>
        /// <param name="localEstacao"></param>
        /// <returns></returns>
        LocalTipoServicoFornecedorDto ObterLocalFornecedorPorNome(string localEstacao);


        /// <summary>
        /// Insere os locais fornecedores
        /// </summary>
        /// <param name="locaisFornecedores"></param>        
        void Inserir(IList<LocalFornecedor> locaisFornecedores);

        /// <summary>
        /// Retorna lista de Id de fornecedorOs
        /// </summary>
        /// <param name="idFornecedorOs"></param>
        /// <returns></returns>
        IList<LocalFornecedor> ObterLocalFornecedorPorIdFornecedorOs(int? idFornecedorOs);

        /// <summary>
        /// Exclui registros a partir do id de fornecedorOs
        /// </summary>
        /// <param name="?"></param>
        void Remover(int idFornecedorOs);


        /// <summary>
        /// Retorna um objeto LocalFornecedorDto, conforme parametros informado
        /// </summary>
        /// <param name="idFornecedorOs"></param>
        /// <param name="idTipoServico"></param>
        /// <param name="idLocal"></param>
        /// <returns></returns>
        LocalFornecedorDto ObterLocalFornecedorPorIds(int idFornecedorOs, int idTipoServico, int idLocal);  
    }
}
