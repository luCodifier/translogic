﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Super-classe dos motivos
	/// </summary>
	public abstract class BaseMotivoALL : EntidadeBase<int?>, IMotivoALL
	{
		#region IMotivoALL MEMBROS

		/// <summary>
		/// Código do Motivo
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do Motivo
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Grupo do Motivo
		/// </summary>
		public virtual string Grupo { get; set; }

		#endregion
	}
}