﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa um País
	/// </summary>
	public class Pais : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código do País
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição Detalhada do País
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição Resumida do País
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// Sigla do País
		/// </summary>
		public virtual string Sigla { get; set; }

		#endregion
	}
}