namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe Cota��o
    /// </summary>
    public class Cotacao : EntidadeBase<int>
    {
        /// <summary>
        ///  Campo MOEDA
        /// </summary>
        public virtual string Moeda { get; set; }

        /// <summary>
        ///  Campo VALOR
        /// </summary>
        public virtual double? Valor { get; set; }

        /// <summary>
        ///  Campo DATA
        /// </summary>
        public virtual DateTime? Data { get; set; }
    }
}

