﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de Certificados da ALL
    /// </summary>
    public class Certificado : EntidadeBase<int>
    {
        /// <summary>
        /// Cnpj do certificado
        /// </summary>
        public virtual string CnpjCertificado { get; set; }

        /// <summary>
        /// Subject name do certificado
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Cnpj do certificado
        /// </summary>
        public virtual string CnpjBusca { get; set; }
    }
}