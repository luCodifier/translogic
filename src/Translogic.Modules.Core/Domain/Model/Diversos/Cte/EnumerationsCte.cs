﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Cte
{
    using System.ComponentModel;

    /// <summary>
    /// Possiveis situações de um CTE (workflow interno do Robo)
    /// </summary>
    public enum SituacaoCteEnum
    {
        /// <summary>
        ///    Preparando a chave do Cte - Quando o cte é gravado pela PKG do cte, ele grava com chave 00000... na tabela e fica em 
        ///    status PCC na tabela, e o job do banco gera a chave do cte.
        /// </summary>
        [Description("PCC")]
        PreparandoChaveCte,

        /// <summary>
        ///    Preparando a chave do Cte Anulacao - Quando o cte anulacao é gravado pela PKG do cte, ele grava com chave 00000... na tabela e fica em 
        ///    status PCC na tabela, e o job do banco gera a chave do cte e coloca em PAN ou PAR
        /// </summary>
        [Description("PCA")]
        PreparandoChaveCteAnulacao,

        /// <summary>
        ///    Preparando a chave do Cte Anulacao - Quando o cte anulacao é gravado pela PKG do cte, ele grava com chave 00000... na tabela e fica em 
        ///    status PCC na tabela, e o job do banco gera a chave do cte e coloca em PAN ou PAR
        /// </summary>
        [Description("PCN")]
        PreparandoChaveCteAnulacaoNc,


        /// <summary>
        /// Situação do Cte Pendente de envio
        /// </summary>
        [Description("PAE")]
        PendenteArquivoEnvio,

        /// <summary>
        /// Situação do Cte Pendente de envio de cancelamento
        /// </summary>
        [Description("PAC")]
        PendenteArquivoCancelamento,

        /// <summary>
        ///    Situação do Cte Pendente de envio de anulação contribuinte
        /// </summary>
        [Description("PAN")]
        PendenteArquivoAnulacaoContribuinte,

        /// <summary>
        ///    Situação do Cte Pendente de envio de anulação não contribuinte
        /// </summary>
        [Description("PAR")]
        PendenteArquivoAnulacaoNContribuinte,

        /// <summary>
        /// Situação do Cte Pendente
        /// </summary>
        [Description("PAI")]
        PendenteArquivoInutilizacao,



        /// <summary>
        /// Situação do Cte enviado para a fila de envio - EFA
        /// </summary>
        [Description("EFA")]
        EnviadoFilaArquivoEnvio,

        /// <summary>
        /// Situacao do Cte enviado para a fila de complemento - EFT
        /// </summary>
        [Description("EFT")]
        EnviadoFilaArquivoComplemento,

        /// <summary>
        /// Situação do Cte enviado para a fila o arquivo de cancelamento - EFC
        /// </summary>
        [Description("EFC")]
        EnviadoFilaArquivoCancelamento,

        /// <summary>
        /// Situação do Cte enviado para a fila o arquivo de inutilização - EFI
        /// </summary>
        [Description("EFI")]
        EnviadoFilaArquivoInutilizacao,

        /// <summary>
        /// Situação do Cte enviado para a fila o arquivo de anulacao - contribuinte. - EFN
        /// </summary>
        [Description("EFN")]
        EnviadoFilaArquivoAnulacaoContribuinte,
        
        /// <summary>
        /// Situação do Cte enviado para a fila o arquivo de anulacao - não contribuinte. - EFR
        /// </summary>
        [Description("EFR")]
        EnviadoFilaArquivoAnulacaoNContribuinte,



        /// <summary>
        ///    Situação do Cte enviado arquivo de envio - O cte-runner seleciona os ctes com status EFA e seta no status EAE.
        /// </summary>
        [Description("EAE")]
        EnviadoArquivoEnvio,

        /// <summary>
        /// Situação do Cte enviado arquivo de complemento
        /// </summary>
        [Description("EAT")]
        EnviadoArquivoComplemento,

        /// <summary>
        /// Situação do Cte enviado arquivo de cancelamento
        /// </summary>
        [Description("EAC")]
        EnviadoArquivoCancelamento,

        /// <summary>
        /// Situação do Cte enviado arquivo de inutilização
        /// </summary>
        [Description("EAI")]
        EnviadoArquivoInutilização,
        
        /// <summary>
        /// Situação do Cte enviado arquivo de anulação
        /// </summary>
        [Description("EAA")]
        EnviadoArquivoAnulacao,
        
        /// <summary>
        /// Situação do Cte enviado arquivo de anulação não contribuinte.
        /// </summary>
        [Description("EAN")]
        EnviadoArquivoAnulacaoNc,




        /// <summary>
        /// Situação do Cte Autorizado
        /// </summary>
        [Description("AUT")]
        Autorizado,

        /// <summary>
        /// Situação do Cte Cancelado
        /// </summary>
        [Description("CAN")]
        Cancelado,

        /// <summary>
        /// Situação do Cte com Erro
        /// </summary>
        [Description("ERR")]
        Erro,

        /// <summary>
        /// Situação do Cte com Erro autorizado re envio
        /// </summary>
        [Description("EAR")]
        ErroAutorizadoReEnvio,

        /// <summary>
        /// Situação do Cte Invalidado
        /// </summary>
        [Description("INV")]
        Invalidado,

        /// <summary>
        /// Situação do Cte Inutilizado
        /// </summary>
        [Description("INT")]
        Inutilizado,

        /// <summary>
        /// Situação do Cte Uso Denegado
        /// </summary>
        [Description("UND")]
        UsoDenegado,

        /// <summary>
        /// Situação do Cte Aguardando Inutilização
        /// </summary>
        [Description("AGI")]
        AguardandoInutilizacao,

        /// <summary>
        /// Situação do Cte Aguardando Anulação - Para aparecer na tela de anulação tem que estar AGA (1274)
        /// </summary>
        [Description("AGA")]
        AguardandoAnulacao,

        /// <summary>
        /// Situação do Cte Aguardando Anulação
        /// </summary>
        [Description("ANL")]
        Anulado,

        /// <summary>
        /// Situação do Cte Aguardando Cancelamento
        /// </summary>
        [Description("AGC")]
        AguardandoCancelamento,

        /// <summary>
        /// Situação do Cte Autorizado Cancelamento
        /// </summary>
        [Description("AUC")]
        AutorizadoCancelamento,

        /// <summary>
        /// Situação do Cte Autorizado Inutilização
        /// </summary>
        [Description("AUI")]
        AutorizadoInutilizacao,



        /// <summary>
        /// Situação do Cte Pendente de Correção
        /// </summary>
        [Description("PCE")]
        PendenteCorrecaoEnvio,

        /// <summary>
        /// Cte em processamento
        /// </summary>
        [Description("PRC")]
        EmProcessamento,

        /// <summary>
        /// Situação do Cte enviado para a fila o arquivo de correcao - ECC
        /// </summary>
        [Description("ECC")]
        EnviadoFilaArquivoCorrecao,

        /// <summary>
        /// Situação do Cte Anulado Nao contribuinte - Situação finalizada - Anulação autorizada sefaz.
        /// Vai virar anulacao config 0 tanto para ctrbuinte e nao contribuiny
        /// </summary>
        [Description("ANC")]
        AnulacaoConfig
    }

    /// <summary>
    /// Possiveis tipos de log do CTE
    /// </summary>
    public enum TipoLogCteEnum
    {
        /// <summary>
        /// Tipo de log de erro
        /// </summary>
        [Description("ERRO")]
        Erro,

        /// <summary>
        /// Tipo de log de informação
        /// </summary>
        [Description("INFO")]
        Info
    }

    /// <summary>
    /// Status do Cte na manutenção
    /// </summary>
    public enum StatusCteEnum
    {
        /// <summary>
        /// Status Cte Ativo
        /// </summary>
        [Description("A")]
        Ativo,

        /// <summary>
        /// Status Cte Inativo
        /// </summary>
        [Description("I")]
        Inativo
    }

    /// <summary>
    /// Unidade comercial da NFe
    /// </summary>
    public enum UnidadeComercialNfeEnum
    {
        /// <summary>
        /// Unidade de quilos
        /// </summary>
        Quilos,

        /// <summary>
        /// Unidade de toneladas
        /// </summary>
        Toneladas,

        /// <summary>
        /// Unidade de litros
        /// </summary>
        Litros,

        /// <summary>
        /// Metros cúbicos
        /// </summary>
        MetrosCubicos,

        /// <summary>
        /// Quando não for uma unidade que utilizamos
        /// </summary>
        Outros
    }

    /// <summary>
    /// Tipo de trafego mutuo utilizado na geração do CTE
    /// </summary>
    public enum TipoTrafegoMutuoEnum
    {
        /// <summary>
        /// Quando o CTE não utiliza intercâmbio, Todos os CTEs passam por ferrovias do Grupo
        /// </summary>
        FaturamentoEmpresaGrupo,

        /// <summary>
        /// Quando o CTE é de intercâmbio e quem fatura é a ferrovia de origem
        /// </summary>
        IntercambioFaturamentoOrigem,

        /// <summary>
        /// Quando o CTE é de intercâmbio e quem fatura é a ferrovia de destino
        /// </summary>
        IntercambioFaturamentoDestino,

        /// <summary>
        /// Quando o CTE tem o faturamento em uma empresa de fora do grupo, no entanto a origem e destino
        /// são estações do grupo
        /// </summary>
        FaturamentoEmpresaForaGrupo,

        /// <summary>
        /// Quando a empresa que fatura é Argentina
        /// </summary>
        FaturamentoArgentina,

        /// <summary>
        /// Quando não foi possivel detectar o tipo de trafego mutuo
        /// </summary>
        Indefinido
    }

    /// <summary>
    /// Tipo de nota para emissão do CT-e
    /// Nota de entrada / Nota ed saida
    /// Caso seja uma nota de entrada deve trocar o emitente pelo destinatário
    /// </summary>
    public enum TipoNotaCteEnum
    {
        /// <summary>
        /// Nota de entrada
        /// </summary>
        NotaEntrada,

        /// <summary>
        /// Nota de saida
        /// </summary>
        NotaSaida
    }

    /// <summary>
    /// Utiliza as unidades para realizar o truncate dos valores
    /// </summary>
    public enum TipoTruncateValueEnum
    {
        /// <summary>
        /// Divide por 10, trunca o valor e multiplica por 10
        /// </summary>
        [Description("10")]
        Dezena,

        /// <summary>
        /// Divide por 100, trunca o valor e multiplica por 100
        /// </summary>
        [Description("100")]
        Centena,

        /// <summary>
        /// Divide por 1000, trunca o valor e multiplica por 1000
        /// </summary>
        [Description("1000")]
        UnidadeMilhar,

        /// <summary>
        /// Divide por 10000, trunca o valor e multiplica por 10000
        /// </summary>
        [Description("10000")]
        DezenaMilhar
    }

    /// <summary>
    /// Tipo de Operação utilizado para envio para interface SAP - CTE 
    /// </summary>
    public enum TipoOperacaoCteEnum
    {
        /// <summary>
        /// Tipo Operacao para CTE Autorizado
        /// </summary>
        [Description("I")]
        Inclusao,

        /// <summary>
        /// Tipo Operacao para CTE Virtual
        /// </summary>
        [Description("V")]
        Virtual,

        /// <summary>
        /// Tipo Operacao para CTE Cancelado
        /// </summary>
        [Description("E")]
        Exclusao,

        /// <summary>
        /// Tipo Operacao para CTE Cancelado
        /// </summary>
        [Description("S")]
        Substituicao,

        /// <summary>
        /// Tipo Operacao para CTE Enviado Arquivo Complemento
        /// </summary>
        [Description("C")]
        Complemento,

        /// <summary>
        /// Tipo Operacao para CTE Enviado Arquivo Complemento
        /// </summary>
        [Description("CI")]
        ComplementoIcms,

        /// <summary>
        /// Tipo Operacao para CTE Cancelamento Rejeitado (Erro código do SEFAZ 220)
        /// </summary>
        [Description("CR")]
        CancelamentoRejeitado,

        /// <summary>
        /// Tipo Operacao para CTE Anulado
        /// </summary>
        [Description("A")]
        Anulacao,

        /// <summary>
        /// Tipo Operacao para CTE anulação enviado para SEFAZ.
        /// </summary>
        [Description("AC")]
        AnulacaoSefaz,

        /// <summary>
        /// Tipo Operacao para CTE Uso Denegado
        /// </summary>
        [Description("D")]
        Denegado,

        /// <summary>
        /// Tipo Operacao invalida
        /// </summary>
        [Description("IN")]
        Invalida,

        /// <summary>
        /// Tipo Operacao invalida
        /// </summary>
        [Description("IN")]
        Inutilizado,

        /// <summary>
        /// Tipo Operacao - Carta de Correção
        /// </summary>
        [Description("CCE")]
        CartaDeCorrecao
    }

    /// <summary>
    /// Tipo de envio dos Cte para os Clientes
    /// </summary>
    public enum TipoEnvioCteEnum
    {
        /// <summary>
        /// Envio por email
        /// </summary>
        [Description("EMA")]
        Email,

        /// <summary>
        /// Envio por email como cópia
        /// </summary>
        [Description("ECC")]
        EmailCopia,

        /// <summary>
        /// Envio por email por cópia oculta
        /// </summary>
        [Description("ECO")]
        EmailCopiaOculta
    }

    /// <summary>
    /// Tipo dps arquivos enviados por email
    /// </summary>
    public enum TipoArquivoEnvioEnum
    {
        /// <summary>
        /// Arquivos do tipo PDF
        /// </summary>
        [Description("PDF")]
        Pdf,

        /// <summary>
        /// Arquivos do tipo XML
        /// </summary>
        [Description("XML")]
        Xml,

        /// <summary>
        /// Arquivos do tipo PDF e XML
        /// </summary>
        [Description("PEX")]
        PdfXml
    }

    /// <summary>
    /// Enum utilizado para Origem da Nfe
    /// </summary>
    public enum TelaProcessamentoNfe
    {
        /// <summary>
        /// Origem da Nfe Sispat
        /// </summary>
        [Description("1131")]
        Carregamento,

        /// <summary>
        /// Origem da Nfe Sispat
        /// </summary>
        [Description("1146")]
        Manutencao,

        /// <summary>
        /// Origem da Nfe BO
        /// </summary>
        [Description("1154")]
        PreCarregamento,

        /// <summary>
        /// Origem da Nfe BO
        /// </summary>
        [Description("0000")]
        Pooling,

        /// <summary>
        /// Origem da Nfe BO
        /// </summary>
        [Description("0000")]
        CorrecaoNfeEdi,

        /// <summary>
        /// Origem da Nfe BO
        /// </summary>
        [Description("0000")]
        WsGestaoDocumentos,

        /// <summary>
        /// Origem da Nfe BO
        /// </summary>
        [Description("0000")]
        DFeSefaz
    }

    /// <summary>
    /// Enum utilizado para Origem da Nfe
    /// </summary>
    public enum TipoCteEnum
    {
        /// <summary>
        /// Cte Normal
        /// </summary>
        [Description("NML")]
        Normal,

        /// <summary>
        /// Cte Complementar
        /// </summary>
        [Description("CPL")]
        Complementar,

        /// <summary>
        /// Cte Virtual
        /// </summary>
        [Description("VTL")]
        Virtual,

        /// <summary>
        /// Cte Substituto
        /// </summary>
        [Description("SBT")]
        Substituto,

        /// <summary>
        /// Cte Substituto
        /// </summary>
        [Description("RFT")]
        Refat,

        /// <summary>
        /// Cte Anulação
        /// </summary>
        [Description("ANL")]
        Anulacao
    }

    /// <summary>
    /// Enum utilizado para Origem da Nfe
    /// </summary>
    public enum CorrecaoCteGrupoAlteradoEnum
    {
        /// <summary>
        /// Grupo Ferroviário
        /// </summary>
        [Description("ferrov")]
        Ferroviario,

        /// <summary>
        /// Grupo Identificador do Cte
        /// </summary>
        [Description("ide")]
        IdentificadorDoCte,

        /// <summary>
        /// Grupo Expedidor
        /// </summary>
        [Description("exped")]
        Expedidor,

        /// <summary>
        /// Endereço Expedidor
        /// </summary>
        [Description("enderExped")]
        EnderecoExpedidor,

        /// <summary>
        /// Grupo Recebedor
        /// </summary>
        [Description("receb")]
        Recebedor,

        /// <summary>
        /// Endereço Recebedor
        /// </summary>
        [Description("enderReceb")]
        EnderecoRecebedor,

        /// <summary>
        /// Complemento do Cte
        /// </summary>
        [Description("compl")]
        ComplementoCte,

        /// <summary>
        /// Unidade de Transporte
        /// </summary>
        [Description("infUnidTransp")]
        InformacoesDaUnidadeDeTransporte,

        /// <summary>
        /// Unidade de Transporte
        /// </summary>
        [Description("infUnidCarga")]
        InformacoesDaUnidadeDeCarga
    }

    /// <summary>
    /// Enum utilizado para Origem da Nfe
    /// </summary>
    public enum CorrecaoCteCampoAlteradoEnum
    {
        /// <summary>
        /// Campo Fluxo Comercial
        /// </summary>
        [Description("fluxo")]
        FluxoComercial,

        /// <summary>
        /// Campo CFOP
        /// </summary>
        [Description("CFOP")]
        CodigoFiscalDeOperacoesEPrestacoes,

        /// <summary>
        /// Campo Natureza da Operação
        /// </summary>
        [Description("natOp")]
        NaturezaDaOperacao,

        /// <summary>
        /// Campo CNPJ
        /// </summary>
        [Description("CNPJ")]
        Cnpj,

        /// <summary>
        /// Campo CNPJ
        /// </summary>
        [Description("CPF")]
        Cpf,

        /// <summary>
        /// Inscrição Estadual
        /// </summary>
        [Description("IE")]
        InscEst,

        /// <summary>
        /// Razão Social
        /// </summary>
        [Description("xNome")]
        Nome,

        /// <summary>
        /// Campo Telefone
        /// </summary>
        [Description("fone")]
        Telefone,

        /// <summary>
        /// Campo Logradouro
        /// </summary>
        [Description("xLgr")]
        Logradouro,

        /// <summary>
        /// Campo Alterado Cep
        /// </summary>
        [Description("CEP")]
        Cep,

        /// <summary>
        /// Campo Município
        /// </summary>
        [Description("cMun")]
        CodMunicipio,

        /// <summary>
        /// Campo Município
        /// </summary>
        [Description("xMun")]
        Municipio,

        /// <summary>
        /// Sigla Estado
        /// </summary>
        [Description("UF")]
        Uf,

        /// <summary>
        /// Código do País
        /// </summary>
        [Description("cPais")]
        CodigoPais,

        /// <summary>
        /// Campo Alterado País
        /// </summary>
        [Description("xPais")]
        NomePais,

        /// <summary>
        /// Observação do Cte
        /// </summary>
        [Description("xObs")]
        Observacao,

        /// <summary>
        /// Código do Vagão
        /// </summary>
        [Description("idUnidTransp")]
        CodigoVagao,

        /// <summary>
        /// Código do Conteiner
        /// </summary>
        [Description("idUnidCarga")]
        Conteiner
    }

    /// <summary>
    /// Enum Tipo Empresa
    /// </summary>
    public enum TipoEmpresaEnum
    {
        /// <summary>
        /// Remetente do Cte
        /// </summary>
        [Description("Remetente")]
        Remetente,

        /// <summary>
        /// Destinatario do Cte
        /// </summary>
        [Description("Destinatario")]
        Destinatario,

        /// <summary>
        /// Expedidor do Cte
        /// </summary>
        [Description("Expedidor")]
        Expedidor,

        /// <summary>
        /// Recebedor do Cte
        /// </summary>
        [Description("Recebedor")]
        Recebedor,

        /// <summary>
        /// Tomador do Cte
        /// </summary>
        [Description("Tomador")]
        Tomador
    }

    /// <summary>
    /// Enum Tipo Envio Seguradora
    /// </summary>
    public enum TipoEnvioSeguradora
    {
        /// <summary>
        /// enviado por webservice
        /// </summary>
        [Description("WEB SERVICE")]
        WebService,

        /// <summary>
        /// Tomador do Cte
        /// </summary>
        [Description("Email")]
        Email
    }

    /// <summary>
    /// Enum Status Envio Seguradora 
    /// </summary>
    public enum StatusEnvioSeguradora
    {
        /// <summary>
        /// Cte sem volume 
        /// </summary>
        [Description("SV")]
        SemVolume,

        /// <summary>
        /// Cte sem peso
        /// </summary>
        [Description("SP")]
        SemPeso,

        /// <summary>
        /// Cte validada
        /// </summary>
        [Description("VD")]
        Validada,
    }
    
    /// <summary>
    /// Tipo dps arquivos enviados por email
    /// </summary>
    public enum TipoServicoCteEnum
    {
        /// <summary>
        /// Tipo de serviço normal do CTE
        /// </summary>
        [Description("0")]
        Normal = 0,

        /// <summary>
        /// Tipo de serviço Sub Contratacao
        /// </summary>
        [Description("1")]
        SubContratacao = 1,

        /// <summary>
        /// Tipo de serviço Redespacho
        /// </summary>
        [Description("2")]
        Redespacho = 2,

        /// <summary>
        /// Tipo de serviço Redespacho Intermediario
        /// </summary>
        [Description("3")]
        RedespachoIntermediario = 3,
        
        /// <summary>
        /// Tipo de serviço Vinculado Multimodal
        /// </summary>
        [Description("4")]
        VinculadoMultimodal = 4
    }
}