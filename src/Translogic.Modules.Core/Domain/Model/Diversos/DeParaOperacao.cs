﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de de_para da tabela DE_PARA_OPERACAO
	/// </summary>
    public class DeParaOperacao :  EntidadeBase<int>
    {
		#region PROPRIEDADES

		/// <summary>
		/// Objeto de
		/// </summary>
		public virtual string De { get; set; }

		/// <summary>
		/// Objeto para
		/// </summary>
		public virtual string Para { get; set; }

		/// <summary>
		/// Objetoo Tipo
		/// </summary>
		public virtual string Tipo { get; set; }

		#endregion
	}
}