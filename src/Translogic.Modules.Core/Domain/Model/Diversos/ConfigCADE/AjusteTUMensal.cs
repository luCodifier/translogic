﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.ConfigCADE
{
    using System;
    using ALL.Core.Dominio;

    public class AjusteTUMensal : EntidadeBase<int>
    {
        public DateTime DataReferencia { get; set; }
        public string Fluxo { get; set; }
        public string UP { get; set; }
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string CodClienteCorrentista { get; set; }
        public string ClienteCorrentista { get; set; }
        public string CodClienteRemetente { get; set; }
        public string ClienteRementente { get; set; }
        public string Mercadoria { get; set; }
        public string Segmento { get; set; }
        public decimal TUMes { get; set; }
        public decimal Carregamentos { get; set; }
        public decimal TKUMes { get; set; }
        public decimal? TUMesAjust { get; set; }
        public decimal? CarregamentosAjust { get; set; }
    }
}