﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.ConfigCADE.Repositories
{
    using System;
    using System.Collections.Generic;

    public interface IConfigCADERepository
    {
        IList<TremTipo> ListarTrensTipo(DateTime mesReferencia, string origem, string destino, string cliente, string segmento);
        IList<AjusteTUMensal> ListarAjustesTU(DateTime mesReferencia, List<string> fluxo, string cliente, string segmento);

        void SalvarAjustes(IEnumerable<AjusteTUMensal> ajustes);
        void SalvarTrensTipo(IEnumerable<TremTipo> ajustes);
    }
}