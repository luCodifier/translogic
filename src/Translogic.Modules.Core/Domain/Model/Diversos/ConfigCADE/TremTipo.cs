﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Diversos.ConfigCADE
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Modelo de dados do trem tipo, utilizado para relatórios do CADE
    /// </summary>
    public class TremTipo : EntidadeBase<int?>
    {
        /// <summary>
        /// Mês de referência
        /// </summary>
        public int Mes { get; set; }

        /// <summary>
        /// Ano de referência
        /// </summary>
        public int Ano { get; set; }

        /// <summary>
        /// Estação de origem
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Estação de destino
        /// </summary>
        public string Destino { get; set; }

        /// <summary>
        /// Segmento
        /// </summary>
        public string Segmento { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        public string Cliente { get; set; }

        /// <summary>
        /// Qtde Vagoes
        /// </summary>
        public decimal? QtdeVagoes { get; set; }
    }
}