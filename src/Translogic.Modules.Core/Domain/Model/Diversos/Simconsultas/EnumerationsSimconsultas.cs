﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Simconsultas
{
	using System.ComponentModel;

	/// <summary>
	/// Forma de pagamento dos dados do Simconsultas
	/// </summary>
	public enum FormaPagamentoEnum
	{
		/// <summary>
		/// Pagamento a vista
		/// </summary>
		[Description("0")]
		Avista,

		/// <summary>
		/// Pagamento a prazo
		/// </summary>
		[Description("1")]
		Aprazo,

		/// <summary>
		/// Outros tipos de pagamento
		/// </summary>
		[Description("2")]
		Outros
	}

	/// <summary>
	/// Tipo da nota fiscal NFE
	/// </summary>
	public enum TipoNotaFiscalEnum
	{
		/// <summary>
		/// Nota fiscal de entrada
		/// </summary>
		[Description("0")]
		Entrada,

		/// <summary>
		/// Nota fiscal de saida
		/// </summary>
		[Description("1")]
		Saida
	}
	
	/// <summary>
	/// Formato de impressão
	/// </summary>
	public enum FormatoImpressaoEnum
	{
		/// <summary>
		/// Impressão no formato retrato
		/// </summary>
		[Description("1")]
		Retrato,

		/// <summary>
		/// Impressao no formato paisagem
		/// </summary>
		[Description("2")]
		Paisagem
	}

	/// <summary>
	/// Tipo de emissão do Cte
	/// </summary>
	public enum TipoEmissaoEnum
	{
		/// <summary>
		/// Tipo de emissão normal
		/// </summary>
		[Description("1")]
		Normal,

		/// <summary>
		/// Tipo de emissão contingência FS3
		/// </summary>
		[Description("2")]
		ContigenciaFs3,

		/// <summary>
		/// Tipo de emissão contingência SCAN
		/// </summary>
		[Description("3")]
		ContigenciaScan,

		/// <summary>
		/// Tipo de emissão contingência DEPEC
		/// </summary>
		[Description("4")]
		ContigenciaDedec,

		/// <summary>
		/// Tipo de emissão contingência FSDA
		/// </summary>
		[Description("5")]
		ContingenciaFsda,

        /// <summary>
        /// Tipo de emissão contingência SVC-AN
        /// </summary>
        [Description("6")]
        ContingenciaSvcan,

        /// <summary>
        /// Tipo de emissão contingência SVC-RS
        /// </summary>
        [Description("7")]
        ContingênciaSVCRS
	}

	/// <summary>
	/// Tipo de ambiente
	/// </summary>
	public enum TipoAmbienteEnum
	{
		/// <summary>
		/// Ambiente de produção
		/// </summary>
		[Description("1")]
		Producao,

		/// <summary>
		/// Ambiente de homologação
		/// </summary>
		[Description("2")]
		Homologacao
	}

	/// <summary>
	/// Finalidade da emissao
	/// </summary>
	public enum FinalidadeEmissaoEnum
	{
		/// <summary>
		/// Emissão da NFE normal
		/// </summary>
		[Description("1")]
		Normal,

		/// <summary>
		/// Emissão da NFE complementar
		/// </summary>
		[Description("2")]
		Complementar,

		/// <summary>
		/// Emissão da NFE para ajuste
		/// </summary>
		[Description("3")]
		Ajuste,

        /// <summary>
        /// Emissão da NFE Devolucao
        /// </summary>
        [Description("4")]
        Devolucao
	}

	/// <summary>
	/// Processo de emissão da nota fiscal
	/// </summary>
	public enum ProcessoEmissaoEnum
	{
		/// <summary>
		/// Emissão pelo aplicativo do contribuinte
		/// </summary>
		[Description("0")]
		EmissaoAplicativoContribuinte,

		/// <summary>
		/// Emissão avulsa pelo fisco
		/// </summary>
		[Description("1")]
		EmissaoAvulsadPeloFisco,

		/// <summary>
		/// emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do fisco
		/// </summary>
		[Description("2")]
		EmissaoAvulsaPeloContribuinte,

		/// <summary>
		/// emissão de NF-e pelo contribuinte com aplicativo fornecido pelo Fisco
		/// </summary>
		[Description("3")]
		EmissaoPeloContribuinteAplicativoFisco
	}

	/// <summary>
	/// Modelo frete da nota fiscal
	/// </summary>
	public enum ModeloFreteEnum
	{
		/// <summary>
        /// Contratação do Frete por conta do Remetente (CIF)
		/// </summary>
		[Description("0")]
		PorContaEmitente,

		/// <summary>
        /// Contratação do Frete por conta do Destinatário (FOB)
		/// </summary>
		[Description("1")]
		PorContaDestinatarioRemetente,

		/// <summary>
        /// Contratação do Frete por conta de Terceiros
		/// </summary>
		[Description("2")]
		PorContaTerceiros,

        /// <summary>
        /// Transporte Próprio por conta do Remetente
		/// </summary>
		[Description("3")]
		PorContaProprietarioRemetente,

        /// <summary>
        /// Transporte Próprio por conta do Destinatário
        /// </summary>
        [Description("4")]
        PorContaProprioDestinatario,

		/// <summary>
        /// Sem Ocorrência de Transporte
		/// </summary>
		[Description("9")]
		SemFrete
	}

	/// <summary>
	/// Modalidade da determinação da base de calculo do Icms
	/// </summary>
	public enum ModalidadeDeterminacaoBaseCalculoIcmsEnum
	{
		/// <summary>
		/// Margem do valor agragado (%)
		/// </summary>
		MargemValorAgregado,

		/// <summary>
		/// Pauta da modalidade 
		/// </summary>
		Pauta,

		/// <summary>
		/// Preço tabelado maximo (valor)
		/// </summary>
		PrecoTabeladoMaximo,

		/// <summary>
		/// Valor da operação
		/// </summary>
		ValorOperacao
	}
}
