namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using AvisoFat;

    /// <summary>
    /// Interface da tabela de envio de SMS
    /// </summary>
    public interface IContatoAvisoFatRepository : IRepository<ContatoAvisoFat, int>
    {
        IList<EnvioMensagemNaoFaturadosDto> CarregaObjetosEnvio();
    }
}