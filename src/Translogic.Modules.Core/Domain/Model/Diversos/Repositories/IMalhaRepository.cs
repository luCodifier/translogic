namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio de Malha
	/// </summary>
	public interface IMalhaRepository : IRepository<Malha, int?>
	{	
	}
}