namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio de Atividade
	/// </summary>
    public interface IDeParaOperacaoRepository : IRepository<DeParaOperacao,int>
	{
        /// <summary>
        /// Retorna valor do objeto
        /// </summary>
        /// <param name="valor">valor de entrada</param>
        /// <param name="tipo">tipo do valor de entrada</param>
        /// <returns>retorna valor de saida</returns>
        string ObterString(string valor, string tipo);

	    /// <summary>
	    /// Obtem todos 
	    /// </summary>
	    /// <returns>retorna lista de registros</returns>
	    IList<DeParaOperacao> ObterTodosRegistros();
	}
}