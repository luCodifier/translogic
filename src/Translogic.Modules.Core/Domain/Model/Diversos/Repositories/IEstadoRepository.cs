namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do repositório Estado
	/// </summary>
	public interface IEstadoRepository : IRepository<Estado, int>
	{
	}
}