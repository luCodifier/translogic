﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using System;
	using System.Collections.Generic;

	using ALL.Core.AcessoDados;

	using Translogic.Modules.Core.Interfaces.DadosFiscais;

	/// <summary>
	/// The SispatRepository interface.
	/// </summary>
	public interface ISispatRepository : IRepository
	{
		/// <summary>
		/// Obtém ctes descarregados por periodo.
		/// </summary>
		/// <param name="dataInicial"> Data inicial. </param>
		/// <param name="dataFinal"> Data final. </param>
		/// <returns>
		/// Lista de Ctes rodoviarios
		/// </returns>
		IList<CteNfeRodoviarioDescarregado> ObterCtesDescarregadosPorPeriodo(DateTime dataInicial, DateTime dataFinal);
	}
}