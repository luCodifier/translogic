namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio Municipio
	/// </summary>
	public interface IMunicipioRepository : IRepository<Municipio, int>
	{
		/// <summary>
		/// Obt�m os municipios pelo id do estado
		/// </summary>
		/// <param name="idEstado">Id do estado</param>
		/// <returns>Lista de municipios</returns>
		IList<Municipio> ObterPorIdEstado(int idEstado);
	}
}