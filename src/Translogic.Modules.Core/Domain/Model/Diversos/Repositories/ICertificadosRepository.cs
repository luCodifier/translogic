﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface de acesso aos dados dos certificados
    /// </summary>
    public interface ICertificadosRepository : IRepository<Certificado, int>
    {
    }
}