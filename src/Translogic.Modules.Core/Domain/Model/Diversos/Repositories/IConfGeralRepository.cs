﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using System.Collections.Generic;
    using Administracao;
    using ALL.Core.AcessoDados;
    using Translogic.Core.Infrastructure.Web;
    using ALL.Core.Dominio;

    public interface IConfGeralRepository : IRepository<ConfGeral, string>
    {
        ResultadoPaginado<ConfGeral> ObterTodosPaginado(DetalhesPaginacaoWeb pagination, string chave);
    }
}
