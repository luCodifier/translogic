﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using ALL.Core.AcessoDados;
    using System.Collections.Generic;
    using Translogic.Modules.Core.Domain.Model.PainelExpedicao.Parametrizacao;

    public interface IPainelExpedicaoRepository : IRepository<PainelExpedicao, int>
    {
        IList<Operador> ObterOperadores();
    }
}
