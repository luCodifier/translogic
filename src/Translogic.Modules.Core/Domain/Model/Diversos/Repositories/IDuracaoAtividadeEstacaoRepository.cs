namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using ALL.Core.AcessoDados;
	using Via;

	/// <summary>
	/// Interface de repositorio de dura��o de atividade
	/// </summary>
	public interface IDuracaoAtividadeEstacaoRepository : IRepository<DuracaoAtividadeEstacao, int?>
	{
		/// <summary>
		/// Atividade Dura��o Esta��o
		/// </summary>
		/// <param name="atividade">Atividade para pesquisa</param>
		/// <param name="estacao">Esta��o de pesquisa</param>
		/// <returns>Dura��o de atividade</returns>
		DuracaoAtividadeEstacao ObterPorEstacaoAtividade(Atividade atividade, IAreaOperacional estacao);
	}
}