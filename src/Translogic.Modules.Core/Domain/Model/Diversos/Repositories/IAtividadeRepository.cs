namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio de Atividade
	/// </summary>
	public interface IAtividadeRepository : IRepository<Atividade, int?>
	{
	}
}