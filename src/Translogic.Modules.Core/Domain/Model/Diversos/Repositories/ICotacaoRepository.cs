namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using System;
	using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface da tabela de Cota��o
    /// </summary>
    public interface ICotacaoRepository : IRepository<Cotacao, int>
    {
			/// <summary>
			/// Obt�m a cota��o pela data de referencia. Obt�m a cota��o pelo Max da dataReferencia - 1
			/// </summary>
			/// <param name="dataReferencia">Data de referencia da cota��o</param>
			/// <param name="moeda">Sigla da moeda para encontrar a cota��o</param>
			/// <returns>Retorna a cota��o</returns>
    	Cotacao ObterCotacaoPorData(DateTime dataReferencia, string moeda);
    }
}