﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using System;
    using Administracao;
    using ALL.Core.AcessoDados;

    public interface ILogConfGeralRepository : IRepository<LogConfGeral, Int32>
    {
    }
}
