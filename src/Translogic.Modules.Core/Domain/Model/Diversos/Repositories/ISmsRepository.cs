namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface da tabela de envio de SMS
    /// </summary>
    public interface ISmsRepository : IRepository<Sms, int>
    {
    }
}