﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    /// <summary>
    ///     Inteface de repositorio de cadastro de corredores
    /// </summary>
    public interface ICadCorredorRepository : IRepository<CadCorredor, int>
    {
        /// <summary>
        ///     Obtem a lista enumerada de corredores
        /// </summary>
        /// <returns>Lista enumerada de Corredores</returns>
        IEnumerable<CadCorredor> ObterCorredores();
    }
}