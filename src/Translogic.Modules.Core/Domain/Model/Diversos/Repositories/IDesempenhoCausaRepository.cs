namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
	using System.Collections.Generic;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositorio de Desempenho de causa
	/// </summary>
	public interface IDesempenhoCausaRepository : IRepository<DesempenhoCausa, int?>
	{
		/// <summary>
		/// Obt�m causas de desempenho com a descri��o informada
		/// </summary>
		/// <param name="descricao"> Descri��o da causa de desempenho. </param>
		/// <returns> Lista de DesempenhoCausa </returns>
		IList<DesempenhoCausa> ObterPorDescricao(string descricao);
	}
}