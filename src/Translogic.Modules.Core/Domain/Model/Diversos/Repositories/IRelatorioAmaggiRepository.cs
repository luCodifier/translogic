namespace Translogic.Modules.Core.Domain.Model.Diversos.Repositories
{
    using System;
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio Estado
	/// </summary>
    public interface IRelatorioAmaggiRepository : IRepository<Estado, int>
	{
	    /// <summary>
	    /// gera o relatorio da amaggi
	    /// </summary>
	    /// <param name="dataInicial">data inicial da filtyragem</param>
	    /// <param name="dataFinal">data final da filtragem</param>
	    /// <param name="estacoes">esta��es separadas por ;</param>
	    /// <param name="emails">e-mails separados por ;</param>
	    void Gerar(DateTime dataInicial, DateTime dataFinal, string estacoes, string emails);
	}
}