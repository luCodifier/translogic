﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using System;
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa a causa de desempenho
	/// </summary>
	public class DesempenhoCausa : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Bitola
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da bitola
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica se é para ser exibido na tela de Planejamento
		/// </summary>
		public virtual bool? Ativo { get; set; }

		/// <summary>
		/// Usuário que cadastrou
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		/// <summary>
		/// Data de Cadastro.
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

		#endregion
	}
}