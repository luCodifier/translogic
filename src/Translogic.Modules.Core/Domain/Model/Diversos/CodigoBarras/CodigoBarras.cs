﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.CodigoBarras
{
    using Acesso;
    using ALL.Core.Dominio;

    /// <summary>
    /// Representa o Codigo de Barras
    /// </summary>
    public class CodigoBarras : EntidadeBase<int>
    {
        #region PROPRIEDADES

        /// <summary>
        /// Get e Set Tipo
        /// </summary>
        public virtual string Tipo { get; set; }

        /// <summary>
        /// Get e Set Valor
        /// </summary>
        public virtual string Valor { get; set; }

        /// <summary>
        /// Get e Set Codificação
        /// </summary>
        public virtual string Codificacao { get; set; }

        /// <summary>
        /// Get e Set Indice
        /// </summary>
        public virtual int Indice { get; set; }

        #endregion
    }
}