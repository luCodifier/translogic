﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.CodigoBarras.Repositories
{
    using ALL.Core.AcessoDados;

    /// <summary>
    /// Interface do repositório da classe Codigo Barras
    /// </summary>
    public interface ICodigoBarrasRepository : IRepository<CodigoBarras, int>
    {
        /// <summary>
        /// Obtém o tipo do Código de barras
        /// </summary>
        /// <param name="valor">Valor do código de barras</param>
        /// <param name="tipo">Tipo do código de barras</param>
        /// <returns>Retorna a instância do objeto</returns>
        CodigoBarras FindByValorAndTipo(string valor, string tipo);

        /// <summary>
        /// Obtém CodigoBarras por valor
        /// </summary>
        /// <param name="indice">Indice do código de barras</param>
        /// <param name="tipo">Valor do código de barras</param>
        /// <returns>Retorna a instância do objeto</returns>
        CodigoBarras FindByIndiceAndTipo(int indice, string tipo);
    }
}