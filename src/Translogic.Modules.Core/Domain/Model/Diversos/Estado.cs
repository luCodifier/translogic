﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o Estado de um País
	/// </summary>
	public class Estado : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// CGC do Estado
		/// </summary>
		public virtual string Cgc { get; set; }

		/// <summary>
		/// Descricao do Estado
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Inscrição Estadual do Estado
		/// </summary>
		public virtual string InscricaoEstadual { get; set; }

		/// <summary>
		/// <see cref="Pais"/> do Estado
		/// </summary>
		public virtual Pais Pais { get; set; }

		/// <summary>
		/// Sigla do Estado
		/// </summary>
		public virtual string Sigla { get; set; }

		#endregion
	}
}