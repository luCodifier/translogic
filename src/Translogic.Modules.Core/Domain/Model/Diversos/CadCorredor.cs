using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    /// <summary>
    ///     Classe de Cadastro de Corredor
    /// </summary>
    public class CadCorredor : EntidadeBase<int>
    {
        /// <summary>
        ///     Descricao corredor
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        ///     Utilizador
        /// </summary>
        public virtual string Utilizador { get; set; }

        /// <summary>
        ///     Indicador Ativo
        /// </summary>
        public virtual string IndicadorAtivo { get; set; }

        /// <summary>
        ///     Data
        /// </summary>
        public virtual DateTime Data { get; set; }

        /// <summary>
        ///     Identificador para "c"orredor ou "r"egi�o
        /// </summary>
        public virtual string Identificador { get; set; }

        /// <summary>
        ///     Ordem
        /// </summary>
        public virtual int Ordem { get; set; }

        /// <summary>
        ///     Indicador Vag�o
        /// </summary>
        public virtual string IndicadorVagao { get; set; }

        /// <summary>
        ///     Indicador Loco
        /// </summary>
        public virtual string IndicadorLoco { get; set; }

        /// <summary>
        ///     Regi�o M�e
        /// </summary>
        public virtual int RegiaoMae { get; set; }

        /// <summary>
        ///     �rea Operacional Origem
        /// </summary>
        public virtual EstacaoMae AreaOperacionalOrigem { get; set; }

        /// <summary>
        ///     �rea Operacional Destino
        /// </summary>
        public virtual EstacaoMae AreaOperacionalDestino { get; set; }
    }
}