﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa as malhas da ferrovia
	/// </summary>
	/// <remarks>Atualmente temos apenas malha norte e malha sul</remarks>
	public class Malha : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Malha
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da malha
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}