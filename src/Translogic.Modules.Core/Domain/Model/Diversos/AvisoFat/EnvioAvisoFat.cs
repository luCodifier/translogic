﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.AvisoFat
{
    using ALL.Core.Dominio;
    using Via;

    /// <summary>
    /// Classe associativa de Contatos com Fluxos para envio de mensagem com os vagões que estão sem faturamento nos pátios
    /// </summary>
    public class EnvioAvisoFat : EntidadeBase<int>
    {
        /// <summary>
        /// Contato a ser avisado
        /// </summary>
        public virtual ContatoAvisoFat Contato { get; set; }

        /// <summary>
        /// Area Operacional
        /// </summary>
        public virtual EstacaoMae AreaOp { get; set; }
    }
}