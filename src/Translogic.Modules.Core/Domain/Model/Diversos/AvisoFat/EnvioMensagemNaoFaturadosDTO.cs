﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.AvisoFat
{
    /// <summary>
    /// Dto para consulta de envio dos avisos
    /// </summary>
    public class EnvioMensagemNaoFaturadosDto
    {
        /// <summary>
        /// Id do Contato 
        /// </summary>
        public decimal ContatoId { get; set; }

        /// <summary>
        /// Nome do Contato 
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// Email do contato
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone do contato
        /// </summary>
        public decimal Telefone { get; set; }

        /// <summary>
        /// Indica de deve enviar aviso por email (S/N)
        /// </summary>
        public string EnviaEmail { get; set; }

        /// <summary>
        /// Indica de deve enviar aviso por sms (S/N)
        /// </summary>
        public string EnviaSms { get; set; }

        /// <summary>
        /// Area Operacional
        /// </summary>
        public string AreaOp { get; set; }

        /// <summary>
        /// Quantidade de vagões nao faturados
        /// </summary>
        public decimal CountVagoes { get; set; }
    }
}