﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.AvisoFat
{
    using ALL.Core.Dominio;

    /// <summary>
    /// Model de contato para envio de email e sms
    /// </summary>
    public class ContatoAvisoFat : EntidadeBase<int>
    {
        /// <summary>
        /// Nome do contado
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Email do contato
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Telefone do contato
        /// </summary>
        public virtual double Telefone { get; set; }

        /// <summary>
        /// Indica se envia email
        /// </summary>
        public virtual EnviaMensagemAvisoEnum EnviaEmail { get; set; }

        /// <summary>
        /// Indica se envia sms
        /// </summary>
        public virtual EnviaMensagemAvisoEnum EnviaSms { get; set; }

        /// <summary>
        /// Indica se envia sms
        /// </summary>
        public virtual EnviaMensagemAvisoEnum EnviaPlanilha { get; set; }

        /// <summary>
        /// Indica se envia planilha da malha norte
        /// </summary>
        public virtual EnviaMensagemAvisoEnum EnviaPlanilhaNorte { get; set; }
    }
}