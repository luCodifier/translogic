﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using System.ComponentModel;

    /// <summary> Sentidos da Rota </summary>
    public enum RotaSentidoEnum
    {
        /// <summary> Sentido da Rota Subindo</summary>
        [Description("S")]
        Subindo,

        /// <summary> Sentido da Rota Descendo</summary>
        [Description("D")]
        Descendo
    }

    /// <summary> Situações do Trem </summary>
    public enum SituacaoTremEnum
    {
        /// <summary> Situação de Formação </summary>
        [Description("P")]
        Formacao,

        /// <summary> Situação em Circulação </summary>
        [Description("C")]
        Circulando,

        /// <summary> Situação Parado </summary>
        [Description("R")]
        Parado,

        /// <summary> Situação de Encerrado </summary>
        [Description("E")]
        Encerrado
    }

    /// <summary> Situações da Composicao </summary>
    public enum SituacaoComposicaoEnum
    {
        /// <summary> Composição com Situação Aberta</summary>
        [Description("A")]
        Aberta,

        /// <summary> Composição com Situação Encerrada</summary>
        [Description("E")]
        Encerrada
    }

    /// <summary> Tipo de Situação da Locomotiva </summary>
    public enum TipoSituacaoComandoLocomotivaEnum
    {
        /// <summary> Locomotiva comandante</summary>
        [Description("C")]
        Comandante,

        /// <summary> Locomotiva Comandada </summary>
        [Description("D")]
        Comandada
    }

    /// <summary> Tipos de abastecimento </summary>
    public enum AbastecimentoEnum
    {
        /// <summary> Abastecimento de Combustivel - Óleo ou Diesel</summary>
        [Description("C")]
        Combustivel,

        /// <summary> Abastecimento de Combustivel - Óleo</summary>
        [Description("O")]
        Oleo,

        /// <summary> Nenhum Abastecimento de Combustivel</summary>
        [Description("N")]
        Nenhum
    }

    /// <summary> Medidas de tempo </summary>
    public enum MedidaTempoEnum
    {
        /// <summary> Medida de Tempo - Dia</summary>
        [Description("D")]
        Dia,

        /// <summary> Medida de Tempo - Mês</summary>
        [Description("M")]
        Mes,

        /// <summary> Medida de Tempo - Ano</summary>
        [Description("A")]
        Ano
    }

    /// <summary> Estados de freio </summary>
    public enum EstadoFreioEnum
    {
        /// <summary> Estado do Freio Avariado</summary>
        [Description("A")]
        Avariado,

        /// <summary> Estado do Freio Bom</summary>
        [Description("B")]
        Bom
    }

    /// <summary> Funções de maquinistas </summary>
    public enum FuncaoMaquinistaEnum
    {
        /// <summary> Função de Maquinista</summary>
        [Description("M")]
        Maquinista,

        /// <summary> Função de Auxiliar de Maquinista</summary>
        [Description("A")]
        Auxiliar,

        /// <summary> Função de Trainee de Maquinista</summary>
        [Description("T")]
        Trainee
    }

    /// <summary> Status carregado ou vazio </summary>
    public enum CarregadoVazioEnum
    {
        /// <summary> Status Carregado</summary>
        [Description("C")]
        Carregado,

        /// <summary> Status Vazio</summary>
        [Description("V")]
        Vazio
    }

    /// <summary> Tipos de pessoa - Física ou jurídica </summary>
    public enum TipoPessoaEnum
    {
        /// <summary> Pessoa Jurídica</summary>
        [Description("J")]
        Juridica,

        /// <summary> Pessoa Física</summary>
        [Description("F")]
        Fisica
    }

    /// <summary> Tipos de trem </summary>
    public enum TipoTremEnum
    {
        /// <summary> Tipo do trem manobrador</summary>
        [Description("B")]
        TremBlocadoManobrador,

        /// <summary> Tipo do trem de combustivel</summary>
        [Description("C")]
        TremDeCombustivel,

        /// <summary> Tipo do trem intermodal</summary>
        [Description("I")]
        TremIntermodal,

        /// <summary> Tipo do trem de plataforma road railler</summary>
        [Description("R")]
        TremPlataformaRoadRailler,

        /// <summary> Tipo do trem Direto</summary>
        [Description("U")]
        TremDireto,

        /// <summary> Tipo do trem Expresso</summary>
        [Description("X")]
        TremExpresso
    }

    /// <summary> Tipos de grupo de fluxo </summary>
    public enum TipoGrupoFluxoEnum
    {
        /// <summary> Tipo do grupo de fluxo ferroviario</summary>
        [Description("F")]
        Ferroviario,

        /// <summary> Tipo do grupo de fluxo rodoviario</summary>
        [Description("R")]
        Rodoviario
    }

    /// <summary> Tipos de fluxos </summary>
    public enum TipoFluxoEnum
    {
        /// <summary> Tipo de fluxo ferroviario</summary>
        [Description("F")]
        Ferroviario,

        /// <summary> Tipo de fluxo rodoviário</summary>
        [Description("R")]
        Rodoviario,

        /// <summary> Tipo de fluxo intermodal</summary>
        [Description("I")]
        Intermodal
    }

    /// <summary> Unidades de medida </summary>
    public enum TipoUnidadeMedidaEnum
    {
        /// <summary> Tipo da unidade de medida - Peso </summary>
        [Description("P")]
        Peso,

        /// <summary> Tipo da unidade de medida - Vagao </summary>
        [Description("V")]
        Vagao,

        /// <summary> Tipo da unidade de medida - Avulso </summary>
        [Description("A")]
        Avulso
    }

    /// <summary> Estados da locomotiva </summary>
    public enum EstadoLocomotivaEnum
    {
        /// <summary> Estado da locomotiva Avariado </summary>
        [Description("A")]
        Avariado,

        /// <summary> Estado da locomotiva bom </summary>
        [Description("B")]
        Bom
    }

    /// <summary> Tipo de pedido </summary>
    public enum PedidoEmergenciaEnum
    {
        /// <summary> Pedido Emergencial </summary> 
        [Description("E")]
        Emergencial,

        /// <summary> Pedido normal</summary>
        [Description("N")]
        Normal
    }

    /// <summary> Ferrovias de faturamento do Contrato </summary>
    /// <remarks> Não é utilizado a tabela de empresa pois nem todas as letras correspondem à uma sigla de empresa</remarks>
    public enum FerroviaFaturamentoEnum
    {
        /// <summary>Ferrovia: ALL_DO_BRASIL </summary>
        [Description("L")]
        ALL_DO_BRASIL,

        /// <summary>Ferrovia: ALL_HOLDING </summary>
        [Description("H")]
        ALL_HOLDING,

        /// <summary>Ferrovia: MRS </summary>
        [Description("F")]
        MRS,

        /// <summary>Ferrovia: FCA </summary>
        [Description("E")]
        FCA,

        /// <summary>Ferrovia: Ferropar </summary>
        [Description("S")]
        Ferropar,

        /// <summary>Ferrovia: Ferroban </summary>
        [Description("Z")]
        Ferroban,

        /// <summary>Ferrovia: Novoeste </summary>
        [Description("J")]
        Novoeste,

        /// <summary>Ferrovia: Ferronorte </summary>
        [Description("T")]
        Ferronorte,

        /// <summary>Ferrovia: Argentina </summary>
        [Description("B")]
        Argentina,

        /// <summary>Ferrovia: Uruguai </summary>
        [Description("U")]
        Uruguai,

        /// <summary>Ferrovia: Bolivia </summary>
        [Description("X")]
        Bolivia,

        /// <summary>Ferrovia: Portofer </summary>
        [Description("P")]
        Portofer
    }

    /// <summary>
    /// Tipo de Veículo - utilizado no projeto de Pesagem Dinâmica
    /// </summary>
    public enum TipoVeiculoEnum
    {
        /// <summary>Tipo: Locomotiva </summary>
        [Description("L")]
        Locomotiva,

        /// <summary>Tipo: Vagão </summary>
        [Description("V")]
        Vagao
    }

    /// <summary>
    /// Tipo de Operação - operação de escrita no banco da dados
    /// </summary>
    public enum TipoOperacaoEnum
    {
        /// <summary>Operação de inserção no banco de dados</summary>
        [Description("I")]
        Inserir,

        /// <summary>Operação de atualização no banco de dados</summary>
        [Description("U")]
        Alterar,

        /// <summary>Operação de deleção no banco de dados</summary>
        [Description("D")]
        Excluir
    }

    /// <summary>
    /// Tipo do trafego passado no fluxo comercial
    /// </summary>
    public enum TipoTrafegoFluxoEnum
    {
        /// <summary>
        /// Tipo do trafego próprio
        /// </summary>
        [Description("P")]
        Proprio,

        /// <summary>
        /// Tipo de trafego mutuo
        /// </summary>
        [Description("M")]
        Mutuo,
    }

    /// <summary>
    /// Status Envio Email Enum
    /// </summary>
    public enum StatusEnvioEmailEnum
    {
        /// <summary>
        /// Status Envio Email Enum Não Enviado
        /// </summary>
        [Description("N")]
        NaoEnviado,

        /// <summary>
        /// Status Envio Email Enum Enviado
        /// </summary>
        [Description("S")]
        Sim,

        /// <summary>
        /// Status Envio Email Enum Erro
        /// </summary>
        [Description("E")]
        Erro,
    }

    /// <summary>
    /// Natureza Da Operacao Enum
    /// </summary>
    public enum NaturezaDaOperacaoEnum
    {
        /// <summary>
        /// Natureza Da Operacao Enum Manual
        /// </summary>
        [Description("M")]
        Manual,

        /// <summary>
        /// Natureza Da Operacao Enum Restaurado
        /// </summary>
        [Description("R")]
        Restaurado,
    }

    /// <summary> Tipo de Situação de Tração da Locomotiva </summary>
    public enum TipoSituacaoTracaoLocomotivaEnum
    {
        /// <summary> Locomotiva comandante</summary>
        [Description("T")]
        Tracionando,

        /// <summary> Locomotiva Comandada </summary>
        [Description("R")]
        Rebocada
    }

    /// <summary> Tipo de Situação de Posição da Locomotiva </summary>
    public enum TipoSituacaoPosicaoLocomotivaEnum
    {
        /// <summary> Locomotiva de Frente</summary>
        [Description("F")]
        Frente,

        /// <summary> Locomotiva de Ré </summary>
        [Description("R")]
        Re
    }

    /// <summary> Limitado Por Enum </summary>
    public enum LimitadoPorEnum
    {
        /// <summary>enum da manga</summary>
        [Description("M")]
        Manga,

        /// <summary>enum da via</summary>
        [Description("V")]
        Via,

        /// <summary>enum da via</summary>
        [Description("A")]
        Minimo,

        /// <summary>enum do vagão</summary>
        [Description("E")]
        PesoMinVagao,

        /// <summary>enum da frota</summary>
        [Description("F")]
        PesoMaxFrota
    }

    /// <summary> Situações do Trem no PX </summary>
    public enum SituacaoTremPxEnum
    {
        /// <summary> Situação de Cancelado </summary>
        [Description("CND")]
        Cancelado,

        /// <summary> Situação em Circulando </summary>
        [Description("CLO")]
        Circulando,

        /// <summary> Situação Encerrado </summary>
        [Description("ENC")]
        Encerrado,

        /// <summary> Situação de Indefinido </summary>
        [Description("IND")]
        Indefinido,

        /// <summary> Situação de Programado </summary>
        [Description("PGD")]
        Programado,

        /// <summary> Situação de Indefinido </summary>
        [Description("SPD")]
        Suprimido
    }

    /// <summary> Tipo de Restrição</summary>
    public enum RestricaoEnum
    {
        /// <summary> Tipo Restrição </summary>
        [Description("Restrição")]
        Restricao = 1,

        /// <summary> Tipo - Ronda </summary>
        [Description("Ronda")]
        Ronda = 2,

        /// <summary> Tipo - Interdição </summary>
        [Description("Interdicão")]
        Interdicao = 3
    }

    public enum EnviaMensagemAvisoEnum
    {
        /// <summary> Flag envia mensagem </summary>
        [Description("S")]
        Envia,

        /// <summary> Flag não envia mensagem </summary>
        [Description("N")]
        NaoEnvia
    }
}