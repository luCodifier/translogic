﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using Acesso;
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa o tipo de Bitola
	/// </summary>
	public class Bitola : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código da Bitola
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição da bitola
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Indica se é para ser exibido na tela de Planejamento
		/// </summary>
		public virtual bool? IndExibePlanejamento { get; set; }

		/// <summary>
		/// Usuário que cadastrou
		/// </summary>
		public virtual Usuario Usuario { get; set; }

		#endregion
	}
}