namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Entidade de Motivos da ALL
	/// </summary>
	public interface IMotivoALL : IIdentificavel<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo do Motivo
		/// </summary>
		string Codigo { get; set; }

		/// <summary>
		/// Descri��o do Motivo
		/// </summary>
		string Descricao { get; set; }

		/// <summary>
		/// Grupo do Motivo
		/// </summary>
		string Grupo { get; set; }

		#endregion
	}
}