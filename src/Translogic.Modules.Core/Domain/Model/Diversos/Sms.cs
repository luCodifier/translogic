namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using System;
    using ALL.Core.Dominio;

    /// <summary>
    /// Classe de envio de SMS
    /// </summary>
    public class Sms : EntidadeBase<int>
    {
        /// <summary>
        /// Corpo da mensagem
        /// </summary>
        public virtual string Mensagem { get; set; }

        /// <summary>
        /// Quem est� enviando o sms
        /// </summary>
        public virtual string From { get; set; }

        /// <summary>
        /// Destinatario do sms
        /// </summary>
        public virtual string To { get; set; }

        /// <summary>
        /// Data que foi inserido na fila de envio
        /// </summary>
        public virtual DateTime DataAdd { get; set; }
    }
}