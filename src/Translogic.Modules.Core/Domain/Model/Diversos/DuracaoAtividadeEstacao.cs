﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using System;
	using ALL.Core.Dominio;
	using Via;

	/// <summary>
	/// Representa o tempo que uma atividade leva para ser executada na Estação
	/// </summary>
	public class DuracaoAtividadeEstacao : EntidadeBase<int?>
	{
		#region PROPRIEDADES

		/// <summary>
		/// <see cref="Diversos.Atividade"/> na Estação
		/// </summary>
		public virtual Atividade Atividade { get; set; }

		/// <summary>
		/// Tempo de duração
		/// </summary>
		/// <remarks>
		/// ao recuperar o valor pegar apenas as horas e minutos
		/// </remarks>
		public virtual DateTime Duracao { get; set; }

		/// <summary>
		/// <see cref="EstacaoMae"/> da duração da Atividade
		/// </summary>
		public virtual EstacaoMae Estacao { get; set; }

		#endregion
	}
}