﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
	using ALL.Core.Dominio;
	using Ibge;

	/// <summary>
	/// Representa um municipio
	/// </summary>
	public class Municipio : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Descrição do Município
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// <see cref="Estado"/> do MunicipioB
		/// </summary>
		public virtual Estado Estado { get; set; }

		/// <summary>
		/// Sigla do Município
		/// </summary>
		public virtual string Sigla { get; set; }

		/// <summary>
		/// Classe com ó código da cidade IBGE
		/// </summary>
		public virtual CidadeIbge CidadeIbge { get; set; }

		#endregion
	}
}