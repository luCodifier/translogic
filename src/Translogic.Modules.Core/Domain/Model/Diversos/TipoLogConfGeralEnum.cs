﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    public enum TipoLogConfGeralEnum
    {
        /// <summary>
        /// Inclusão de uma nova chave de configuração
        /// </summary>
        New = 1,

        /// <summary>
        /// Edição de uma chave existente
        /// </summary>
        Edit = 2,

        /// <summary>
        /// Exclusão de uma chave existente
        /// </summary>
        Delete = 3
    }
}