namespace Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da classe UfIbge
	/// </summary>
	public interface IUfIbgeRepository : IRepository<UfIbge, int>
	{
		/// <summary>
		/// Obt�m a uf pela sigla do estado
		/// </summary>
		/// <param name="sigla">Sigla do estado</param>
		/// <returns>Retorna a inst�ncia do objeto</returns>
		UfIbge ObterPorSigla(string sigla);
	}
}