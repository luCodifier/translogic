namespace Translogic.Modules.Core.Domain.Model.Diversos.Ibge.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio da CidadeIbge
	/// </summary>
	public interface ICidadeIbgeRepository : IRepository<CidadeIbge, int>
	{
		/// <summary>
		/// Obt�m a cidade por descri��o e Uf
		/// </summary>
		/// <param name="descricao">Descricao da cidade</param>
		/// <param name="uf">Uf da cidade</param>
		/// <returns>Retorna a instancia do objeto</returns>
		CidadeIbge ObterPorDescricaoUf(string descricao, string uf);

		/// <summary>
		/// Obt�m a cidade ibge pelo c�digo do municipio
		/// </summary>
		/// <param name="codigoIbge">C�digo da cidade IBGE</param>
		/// <returns>Retorna a inst�ncia do objeto</returns>
		CidadeIbge ObterPorCodigoMunicipioIbge(int codigoIbge);
	}
}