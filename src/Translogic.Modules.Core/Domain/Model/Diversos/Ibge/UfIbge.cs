namespace Translogic.Modules.Core.Domain.Model.Diversos.Ibge
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe de unidade federativa do IBGE
	/// </summary>
	public class UfIbge : EntidadeBase<int>
	{
		/// <summary>
		/// Sigla da unidade federativa
		/// </summary>
		public virtual string Sigla { get; set; }

		/// <summary>
		/// Nome do estado da unidade federativa
		/// </summary>
		public virtual string Nome { get; set; }
	}
}