namespace Translogic.Modules.Core.Domain.Model.Diversos.Ibge
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Cidades com os c�digos utilizados pelo IBGE
	/// </summary>
	public class CidadeIbge : EntidadeBase<int>
	{
		/// <summary>
		/// C�digo utilizado pelo Ibge
		/// </summary>
		public virtual int CodigoIbge { get; set; }

		/// <summary>
		/// Descri��o da cidade
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Sigla do estado da cidade do Ibge
		/// </summary>
		public virtual string SiglaEstado { get; set; }

		/// <summary>
		/// Sigla do pais da cidade do Ibge
		/// </summary>
		public virtual string SiglaPais { get; set; }
	}
}