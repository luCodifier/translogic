namespace Translogic.Modules.Core.Domain.Model.Diversos.Bacen
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Dados do Pais utilizado pelo BACEN
	/// </summary>
	public class PaisBacen : EntidadeBase<int>
	{
		/// <summary>
		/// C�digo do pais utilizado pelo BACEN
		/// </summary>
		public virtual string CodigoBacen { get; set; }

		/// <summary>
		/// Nome do Pais (descri��o)
		/// </summary>
		public virtual string Nome { get; set; }

		/// <summary>
		/// Sigla do pais com 3 digitos
		/// </summary>
		public virtual string Sigla { get; set; }

		/// <summary>
		/// Sigla do pais com 2 digitos
		/// </summary>
		public virtual string SiglaResumida { get; set; }
	}
}