namespace Translogic.Modules.Core.Domain.Model.Diversos.Bacen.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface do reposit�rio do PaisBacen
	/// </summary>
	public interface IPaisBacenRepository : IRepository<PaisBacen, int>
	{
		/// <summary>
		/// Obt�m o pais por sigla resumida
		/// </summary>
		/// <param name="siglaResumida">Sigla resumida do pais</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		PaisBacen ObterPorSiglaResumida(string siglaResumida);

		/// <summary>
		/// Obtem o pais pelo c�digo do pais
		/// </summary>
		/// <param name="codigoPais">C�digo do pais Bacen</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		PaisBacen ObterPorCodigoPais(string codigoPais);

		/// <summary>
		/// Obt�m o pais pela descri��o do pais Bacen
		/// </summary>
		/// <param name="descricaoPais">Descri��o do Paiz</param>
		/// <returns>Retorna o objeto PaisBacen</returns>
		PaisBacen ObterPorDescricaoPais(string descricaoPais);
	}
}