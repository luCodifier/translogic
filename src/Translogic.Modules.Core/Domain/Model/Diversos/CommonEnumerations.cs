﻿namespace Translogic.Modules.Core.Domain.Model.Diversos
{
    using System.ComponentModel;

    /// <summary>
    /// Demonstra Quantas horas as Ctes ou Mdfes estão com erro
    /// </summary>
    public enum HorasErroEnum
    {
        /// <summary>
        /// Ctes ou Mdfes com erro em menos de 12 horas
        /// </summary>
        [Description("0-12")]
        DeZeroADozeHoras,

        /// <summary>
        /// Ctes ou Mdfes com erro entre 12 e 24 horas
        /// </summary>
        [Description("12-24")]
        DeDozeAVinteQuatroHoras,

        /// <summary>
        /// Ctes ou Mdfes com erro a mais de 24 horas
        /// </summary>
        [Description("+24")]
        MaisQueVinteQuatroHoras,

        /// <summary>
        /// Ctes ou Mdfes com erro
        /// </summary>
        [Description("TODAS")]
        TodasAsHoras
    }
}