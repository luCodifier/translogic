﻿namespace Translogic.Modules.Core.Domain.Model.Diversos.Mdfe
{
	using System.ComponentModel;

	/// <summary>
	/// Possiveis situações de um MDF-e (workflow interno do Robo)
	/// </summary>
	public enum SituacaoMdfeEnum
	{
		/// <summary>
		/// Situação do Cte Pendente de envio
		/// </summary>
		[Description("PAE")]
		PendenteArquivoEnvio,	
        
        /// <summary>
		/// Situação do Mdfe Pendente de envio pela tela
		/// </summary>
		[Description("PME")]
		PendenteMdfeEnvio,

		/// <summary>
		/// Situação do Cte Pendente de envio de cancelamento
		/// </summary>
		[Description("PAC")]
		PendenteArquivoCancelamento,

		/// <summary>
		/// Situação do Cte Pendente
		/// </summary>
		[Description("PAI")]
		PendenteArquivoInutilizacao,

		/// <summary>
		/// Situação do Cte enviado para a fila de envio
		/// </summary>
		[Description("EFA")]
		EnviadoFilaArquivoEnvio,

		/// <summary>
		/// Situação do Cte enviado para a fila o arquivo de cancelamento
		/// </summary>
		[Description("EFC")]
		EnviadoFilaArquivoCancelamento,

		/// <summary>
		/// Situação do Cte enviado para a fila o arquivo de cancelamento
		/// </summary>
		[Description("EFE")]
		EnviadoFilaArquivoEncerramento,

		/// <summary>
		/// Situação do Cte enviado arquivo de envio
		/// </summary>
		[Description("EAE")]
		EnviadoArquivoEnvio,

		/// <summary>
		/// Situação do Mdfe enviado arquivo de envio
		/// </summary>
		[Description("EAF")]
		EnviadoArquivoEncerramento,

		/// <summary>
		/// Situação do Cte enviado arquivo de cancelamento
		/// </summary>
		[Description("EAC")]
		EnviadoArquivoCancelamento,

		/// <summary>
		/// Situação do Cte Autorizado
		/// </summary>
		[Description("AUT")]
		Autorizado,

		/// <summary>
		/// Situação do Cte Cancelado
		/// </summary>
		[Description("CAN")]
		Cancelado,

		/// <summary>
		/// MDF-e encerrado
		/// </summary>
		[Description("ENC")]
		Encerrado,

		/// <summary>
		/// Situação do Cte com Erro
		/// </summary>
		[Description("ERR")]
		Erro,

		/// <summary>
		/// Situação do Cte com Erro autorizado re envio
		/// </summary>
		[Description("EAR")]
		ErroAutorizadoReEnvio,

		/// <summary>
		/// Situação do Cte Invalidado
		/// </summary>
		[Description("INV")]
		Invalidado,

		/// <summary>
		/// Situação do Cte Uso Denegado
		/// </summary>
		[Description("UND")]
		UsoDenegado,

		/// <summary>
		/// Situação do Cte Aguardando Cancelamento
		/// </summary>
		[Description("AGC")]
		AguardandoCancelamento,

		/// <summary>
		/// Situação do Cte Aguardando Encerramento
		/// </summary>
		[Description("AGE")]
		AguardandoEncerramento,

		/// <summary>
		/// Situação do Cte Aguardando Encerramento
		/// </summary>
		[Description("AME")]
		AguardandoMdfeEncerramento,

		/// <summary>
		/// Situação do Cte Autorizado Cancelamento
		/// </summary>
		[Description("AUC")]
		AutorizadoCancelamento,

		/// <summary>
		/// Preparando a chave do Cte
		/// </summary>
		[Description("PGC")]
		PendendeGeracaoChaveMdfe,

		/// <summary>
		/// MDF-e em processamento
		/// </summary>
		[Description("MEP")]
		MdfeEmProcessamento,

        /// <summary>
        /// Trem sem mdfe (Santos)
        /// </summary>
        [Description("SEM")]
        TremSemMdfe,
	}

	/// <summary>
	/// Possiveis tipos de log do CTE
	/// </summary>
	public enum TipoLogMdfeEnum
	{
		/// <summary>
		/// Tipo de log de erro
		/// </summary>
		[Description("ERRO")]
		Erro,

		/// <summary>
		/// Tipo de log de informação
		/// </summary>
		[Description("INFO")]
		Info
	}
}
