﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
    public class MalhaSeguro : EntidadeBase<int>
    {
        public decimal IdMalha { get; set; }
        public int Posicao { get; set; }
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }
}