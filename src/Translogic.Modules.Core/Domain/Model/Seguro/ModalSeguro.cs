using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de modal seguro
	/// </summary>
	public class ModalSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   Id do vag�o avariado
		/// </summary>
		public virtual Vagao Vagao { get; set; }

		/// <summary>
		///   Id da tabela PROCESSO_SEGURO
		/// </summary>
		public virtual ProcessoSeguro ProcessoSeguro { get; set; }

		/// <summary>
		///   Id da causa da ocorr�ncia
		/// </summary>
		public virtual CausaSeguro CausaSeguro { get; set; }

		/// <summary>
		///   Id da esta��o de origem da ocorr�ncia
		/// </summary>
		public virtual int? Origem { get; set; }

		/// <summary>
		///   Id da esta��o de destino da ocorr�ncia
		/// </summary>
		public virtual int? Destino { get; set; }

        /// <summary>
        ///   Id do terminal
        /// </summary>
        public virtual int? Terminal { get; set; }

		/// <summary>
		///   Quantidade de perda estimada em toneladas.
		/// </summary>
		public virtual double? QuantidadePerda { get; set; }

		/// <summary>
		///   Indica se o vag�o est� gambitado. Valores: 'S' - Gambitado 'N' - N�o Gambitado 'P' - Parcialmente Gambitado
		/// </summary>
		public virtual string Gambit { get; set; }

		/// <summary>
		///   C�digo do fluxo do vag�o
		/// </summary>
		public virtual string CodFluxo { get; set; }

		/// <summary>
		///   C�digo da mercadoria
		/// </summary>
		public virtual string CodMercadoria { get; set; }

		/// <summary>
		///   Id da esta��o origem do fluxo
		/// </summary>
		public virtual int? OrigemFluxo { get; set; }

		/// <summary>
		///   Id da esta��o destino do fluxo
		/// </summary>
		public virtual int? DestinoFluxo { get; set; }

		/// <summary>
		///   Usu�rio logado
		/// </summary>
		public virtual string Usuario { get; set; }

		/// <summary>
		///   N�mero do termo de falta
		/// </summary>
		public virtual string Termo { get; set; }

		/// <summary>
		///   Data do termo de falta
		/// </summary>
		public virtual DateTime? DataTermo { get; set; }

		/// <summary>
		///   Respons�vel Termo
		/// </summary>
		public virtual string ResponsavelTermo { get; set; }

		/// <summary>
		///   Valor da mercadoria por tonelada
		/// </summary>
		public virtual double? ValorMercadoria { get; set; }

		/// <summary>
		///   N�mero da nota fiscal
		/// </summary>
		public virtual string NotaFiscal { get; set; }

		/// <summary>
		///   Data
		/// </summary>
		public virtual DateTime? Data { get; set; }

		/// <summary>
		///   S�rie Despacho
		/// </summary>
		public virtual string Serie { get; set; }

		/// <summary>
		///   N�mero Despacho
		/// </summary>
		public virtual int? Despacho { get; set; }

		/// <summary>
		///   Lacre
		/// </summary>
		public virtual string Lacre { get; set; }

		/// <summary>
		///   Historico
		/// </summary>
		public virtual string Historico { get; set; }

        /// <summary>
        ///   Piscofins
        /// </summary>
        public virtual double? Piscofins { get; set; }
	}
}