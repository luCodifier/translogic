using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de unidade seguro
	/// </summary>
	public class UnidadeSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   Descri��o
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		///   Indica qual o tipo da unidade. Valores: 'P' - UP (Unidade Produ��o) 'N' - UN (Unidade Neg�cio) 'T' - Terminal
		/// </summary>
		public virtual string Tipo { get; set; }
	}
}