using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
    /// <summary>
    ///   Classe de processo seguro cache
    /// </summary>
    public class ProcessoSeguroCache : EntidadeBase<int>
    {
        /// <summary>
        ///   Usu�rio logado
        /// </summary>
        public virtual Usuario Usuario { get; set; }

        /// <summary>
        ///   Despacho
        /// </summary>
        public virtual int? Despacho { get; set; }

        /// <summary>
        ///   S�rie do despacho
        /// </summary>
        public virtual string Serie { get; set; }

        /// <summary>
        ///   Data do sinistro
        /// </summary>
        public virtual DateTime? DataSinistro { get; set; }

        /// <summary>
        ///   N�mero do laudo
        /// </summary>
        public virtual int? Laudo { get; set; }

        /// <summary>
        ///   Terminal
        /// </summary>
        public virtual decimal Terminal { get; set; }

        /// <summary>
        ///   Quantidade Perda
        /// </summary>
        public virtual double? Perda { get; set; }

        /// <summary>
        ///   Causa
        /// </summary>
        public virtual CausaSeguro Causa { get; set; }

        /// <summary>
        ///   Gambit
        /// </summary>
        public virtual string Gambit { get; set; }

        /// <summary>
        ///   Lacre
        /// </summary>
        public virtual string Lacre { get; set; }

        /// <summary>
        ///   Vistoriador
        /// </summary>
        public virtual string Vistoriador { get; set; }

        /// <summary>
        ///   Avaliacao
        /// </summary>
        public virtual string Avaliacao { get; set; }

        /// <summary>
        ///   Unidade
        /// </summary>
        public virtual UnidadeSeguro Unidade { get; set; }

        /// <summary>
        ///   Rateio
        /// </summary>
        public virtual double? Rateio { get; set; }

        /// <summary>
        ///   Historico
        /// </summary>
        public virtual string Historico { get; set; }

        /// <summary>
        ///   Nota
        /// </summary>
        public virtual string Nota { get; set; }

        /// <summary>
        ///   Cliente
        /// </summary>
        public virtual string Cliente { get; set; }

        /// <summary>
        ///   Produto
        /// </summary>
        public virtual string Produto { get; set; }

        /// <summary>
        ///   Peso
        /// </summary>
        public virtual double? Peso { get; set; }

        /// <summary>
        ///   Tipo
        /// </summary>
        public virtual string Tipo { get; set; }

        /// <summary>
        ///   C�digo do fluxo
        /// </summary>
        public virtual string CodFluxo { get; set; }

        /// <summary>
        ///   C�digo da mercadoria
        /// </summary>
        public virtual string CodMercadoria { get; set; }

        /// <summary>
        ///   Destino
        /// </summary>
        public virtual int? Destino { get; set; }

        /// <summary>
        ///   Destino Fluxo
        /// </summary>
        public virtual int? DestinoFluxo { get; set; }

        /// <summary>
        ///   Origem
        /// </summary>
        public virtual int? Origem { get; set; }

        /// <summary>
        ///   Origem Fluxo
        /// </summary>
        public virtual int? OrigemFluxo { get; set; }

        /// <summary>
        ///   Vag�o
        /// </summary>
        public virtual Vagao Vagao { get; set; }

        /// <summary>
        ///   Valor Mercadoria
        /// </summary>
        public virtual double? ValorMercadoria { get; set; }

        /// <summary>
        ///   C�digo do vag�o
        /// </summary>
        public virtual string CodVagao { get; set; }

        /// <summary>
        ///   Descri��o da origem
        /// </summary>
        public virtual string OrigemDesc { get; set; }

        /// <summary>
        ///   Piscofins
        /// </summary>
        public virtual double? Piscofins { get; set; }


        ///<sumary>
        ///---Sindicancia
        ///</sumary>
        public virtual long? Sindicancia { get; set; }

        /// <summary>
        ///   Conta Cont�bil
        /// </summary>
        public virtual ContaContabilSeguro ContaContabil { get; set; }

        /// <summary>
        ///   Cliente Seguro
        /// </summary>
        public virtual ClienteSeguro ClienteSeguro { get; set; }
    }
}