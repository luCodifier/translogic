using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de causa seguro
	/// </summary>
	public class CausaSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   Descri��o
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		///   Tipo da causa P-Processo e V-Vagao
		/// </summary>
		public virtual string Tipo { get; set; }

		/// <summary>
		///   Indicador de situa��o da causa seguro
		/// </summary>
		public virtual string Situacao { get; set; }

        /// <summary>
        ///   Indicador se Causa � Sinistro ou Avaria
        /// </summary>
        public virtual string TipoSinistro { get; set; }
	}
}