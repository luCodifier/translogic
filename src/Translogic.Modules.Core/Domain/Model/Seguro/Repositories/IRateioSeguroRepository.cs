﻿using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de rateio seguro
	/// </summary>
	public interface IRateioSeguroRepository : IRepository<RateioSeguro, int>
	{
        /// <summary>
        /// Retorna o Rateio de Seguro para o processo informado
        /// </summary>
        /// <param name="processoSeguro">Processo Seguro</param>
        /// <returns>RateioSeguro</returns>
        RateioSeguro ObterPorProcesso(ProcessoSeguro processoSeguro);        
	}
}