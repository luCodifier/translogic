﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de processo seguro cache
	/// </summary>
    public interface IMangaRepository : IRepository<Manga, int>
	{

        /// <summary>
        ///   Retorna todos os processos em cache do usuário
        /// </summary>
        /// <param name="usuario">Usuário logado</param>
        /// <returns>lista de processos em cache</returns>
        string ObterPesoMangaPorVagao(string vagao);

	}
}