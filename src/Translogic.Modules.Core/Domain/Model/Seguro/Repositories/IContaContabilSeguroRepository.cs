﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
    /// <summary>
    ///     Inteface de repositorio de conta contábil seguro
    /// </summary>
    public interface IContaContabilSeguroRepository : IRepository<ContaContabilSeguro, int>
    {
        /// <summary>
        ///     Obtem a lista enumerada de contas contabeis
        /// </summary>
        /// <returns>Lista enumerada de Contas Contábeis</returns>
        IEnumerable<ContaContabilSeguro> ObterContasContabeis();
    }
}