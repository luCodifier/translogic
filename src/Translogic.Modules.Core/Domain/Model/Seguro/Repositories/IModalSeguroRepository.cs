﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
    /// <summary>
    ///     Inteface de repositorio de processo seguro
    /// </summary>
    public interface IModalSeguroRepository : IRepository<ModalSeguro, int>
    {
        /// <summary>
        ///     Retorna lista de registros para o despacho e série informado
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <returns>lista de modais seguro</returns>
        IList<ModalSeguro> ObterPorDespachoSerie(int despacho, string serie);

        /// <summary>
        ///     Retorna lista de registros para o despacho, série e vagão informado
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <param name="numeroVagao">Número do Vagão</param>
        /// <returns>lista de modais seguro</returns>
        IList<ModalSeguro> ObterPorDespachoSerieVagao(int despacho, string serie, string numeroVagao);

        /// <summary>
        /// Verifica se numTermo existe na tabela
        /// </summary>
        /// <param name="numTermo"></param>
        /// <returns> Retorna TRUE se existe e FALSE se não existir</returns>
        bool VerificaNumTermoExiste(string numTermo);

        /// <summary>
        /// Retorna o Modal de Seguro para o processo informado
        /// </summary>
        /// <param name="processoSeguro">Processo Seguro</param>
        /// <returns>ModalSeguro</returns>
        ModalSeguro ObterPorProcesso(ProcessoSeguro processoSeguro);

        /// <summary>
        /// Atualiza o Valor da Mercadoria do Modal de Seguro
        /// </summary>
        /// <param name="modalSeguro">objeto modal seguro à ser atualizado</param>
        void AtualizarValorMercadoria(ModalSeguro modalSeguro);

         /// <summary>
        /// Verifica se existe modal de seguro para o Número de Despacho e Série informado
        /// </summary>
        /// <param name="numeroDespacho">Número do Despacho</param>
        /// <param name="Serie">Número de Série</param>
        /// <param name="numeroVagao">Número do Vagão</param>
        /// <returns> Retorna TRUE se o modal seguro foi encontrado na tabela, se não encontrar retorna FALSE</returns>
        bool VerificaDespachoSerieVagaoExiste(int numeroDespacho, string serie, string numeroVagao);
        
    }
}