﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
    public interface IMalhaSeguroRepository : IRepository<MalhaSeguro, int>
    {
        IList<MalhaSeguroDto> ObterMalha();
    }
}
