﻿using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.Dto;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de causa seguro
	/// </summary>
	public interface ICausaSeguroRepository : IRepository<CausaSeguro, int>
	{
		/// <summary>
		///   Obtem a lista enumerada de causas
		/// </summary>
		/// <returns>Lista enumerada de Causas</returns>
		IEnumerable<CausaSeguro> ObterCausas();

        ///<sumary>
        ///   Obtem a lista de causas tipo v
        /// </summary>
        /// <returns>Obtem a lista de causas tipo v</returns>
        IList<CausaSeguroDto> ObterCausasTipoCausaV();
	}
}