﻿using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de processo seguro
	/// </summary>
	public interface IProcessoSeguroRepository : IRepository<ProcessoSeguro, int>
	{
	}
}