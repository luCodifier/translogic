﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de processo seguro cache
	/// </summary>
	public interface IProcessoSeguroCacheRepository : IRepository<ProcessoSeguroCache, int>
	{
		/// <summary>
		///   Retorna todos os processos em cache do usuário
		/// </summary>
		/// <param name="pagination">Detalhes da Paginação</param>
		/// <param name="usuario">Usuário logado</param>
		/// <returns>lista de Cte</returns>
		ResultadoPaginado<ProcessoSeguroCacheDto> ObterProcessosSeguroCacheUsuario(DetalhesPaginacao pagination,
			Usuario usuario);

		/// <summary>
		///   Retorna todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário logado</param>
		/// <returns>lista de processos em cache</returns>
		IList<ProcessoSeguroCache> ObterPorUsuario(Usuario usuario);

        /// <summary>
        ///   Retorna se o laudo já está cadastrado para o despacho e série informados
        /// </summary>
        /// <param name="despacho">Despacho</param>
        /// <param name="serie">Série</param>
        /// <returns>True para cadastrado, False para não cadastrado</returns>
        bool ExisteDespachoSerie(int despacho, string serie);

		/// <summary>
		///   Remove determinado processo da tabela de cache
		/// </summary>
		/// <param name="processoSeguroCache">Processo a ser Removido</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		bool RemoverProcessoSeguroCache(ProcessoSeguroCache processoSeguroCache);

		/// <summary>
		///   Remove todos os processos em cache do usuário
		/// </summary>
		/// <param name="usuario">Usuário</param>
		/// <returns>Retorna se foi possível remover o processo</returns>/// 
		bool RemoverPorUsuario(Usuario usuario);

		/// <summary>
		///   Salva o processo seguro em cache do usuário
		/// </summary>
		/// <param name="processoSeguroCache">Objeto a ser salvo</param>
		/// <returns></returns>
		ProcessoSeguroCache Salvar(ProcessoSeguroCache processoSeguroCache);
	}
}