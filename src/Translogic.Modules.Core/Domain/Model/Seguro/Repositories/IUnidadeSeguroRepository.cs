﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;
using Translogic.Modules.Core.Domain.Model.Dto;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de unidade seguro
	/// </summary>
	public interface IUnidadeSeguroRepository : IRepository<UnidadeSeguro, int>
	{
		/// <summary>
		///   Obtem a lista enumerada de terminais
		/// </summary>
		/// <returns>Lista enumerada de Terminais</returns>
		IEnumerable<UnidadeSeguro> ObterTerminais();

		/// <summary>
		///   Obtem a lista enumerada de unidades
		/// </summary>
		/// <returns>Lista enumerada de Unidades</returns>
		IEnumerable<UnidadeSeguro> ObterUnidades();

        /// <summary>
        ///   Obtem a lista enumerada de unidades do tipo N
        /// </summary>
        /// <returns>Lista enumerada de Unidades do tipo N</returns>
        IEnumerable<UnidadeSeguro> ObterUnidadesTipoN();

        /// <summary>
        ///   Obtem a lista enumerada de UPs
        /// </summary>
        /// <returns>Lista enumerada de UPs</returns>
        IEnumerable<UnidadeSeguro> ObterUP();

        /// <summary>
        ///  Obtem a lista enumerada de unidades de acordo com o ID do Terminal
        /// </summary>
        /// <returns>Lista enumerada de Unidades</returns>
        UnidadeSeguro ObterUnidadeSeguroPorIdTerminal(int idTerminal);

	}
}