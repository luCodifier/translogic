﻿using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
    /// <summary>
    ///     Inteface de repositorio de cliente seguro
    /// </summary>
    public interface IClienteSeguroRepository : IRepository<ClienteSeguro, int>
    {
        /// <summary>
        ///     Obtem o cliente seguro pela sigla
        /// </summary>
        /// <returns>Cliente seguro</returns>
        ClienteSeguro ObterClientePorSigla(string sigla);
    }
}