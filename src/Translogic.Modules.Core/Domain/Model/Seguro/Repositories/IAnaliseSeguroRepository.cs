﻿using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Seguro.Repositories
{
	/// <summary>
	///   Inteface de repositorio de analise seguro
	/// </summary>
	public interface IAnaliseSeguroRepository : IRepository<AnaliseSeguro, int>
	{
	}
}