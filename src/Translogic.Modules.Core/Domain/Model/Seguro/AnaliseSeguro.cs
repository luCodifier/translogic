using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de an�lise seguro
	/// </summary>
	public class AnaliseSeguro : EntidadeBase<int>
	{
		/// <summary>
		///  Indica a situa��o. Valores: 'A' - Atualizando 'F' - Fechado
		/// </summary>
		public virtual string IndicadorSituacao { get; set; }

		/// <summary>
		///   Processo Seguro
		/// </summary>
		public virtual ProcessoSeguro ProcessoSeguro { get; set; }

		/// <summary>
		///   Data
		/// </summary>
		public virtual DateTime? Data { get; set; }
	}
}