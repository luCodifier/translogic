using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de cliente seguro
	/// </summary>
	public class ClienteSeguro : EntidadeBase<int>
	{
		/// <summary>
        ///  Descri��o do cliente indeniza��o
		/// </summary>
		public virtual string Descricao { get; set; }

        /// <summary>
        ///  Banco do cliente
        /// </summary>
        public virtual string Banco { get; set; }

        /// <summary>
        ///  Ag�ncia do cliente
        /// </summary>
        public virtual string Agencia { get; set; }

        /// <summary>
        ///  Conta corrente do cliente
        /// </summary>
        public virtual string ContaCorrente { get; set; }

        /// <summary>
        ///  Contato do cliente
        /// </summary>
        public virtual string Contato { get; set; }

        /// <summary>
        ///  Telefone do cliente
        /// </summary>
        public virtual string Telefone { get; set; }

        /// <summary>
        ///  Email do cliente
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        ///  Data
        /// </summary>
        public virtual DateTime? Data { get; set; }

        /// <summary>
        ///  Usu�rio
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
        ///  C�digo do sap
        /// </summary>
        public virtual string CodSap { get; set; }

		/// <summary>
		///   SiglaEmpresa
		/// </summary>
		public virtual string SiglaEmpresa { get; set; }
	}
}