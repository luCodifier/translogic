using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
    /// <summary>
    ///     Classe de conta cont�bil seguro
    /// </summary>
    public class ContaContabilSeguro : EntidadeBase<int>
    {
        /// <summary>
        ///     Conta cont�bil para os registro de processos do seguro
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        ///     Descri��o da conta cont�bil
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        ///     Indicador de situacao
        /// </summary>
        public virtual string IndicadorSituacao { get; set; }
    }
}