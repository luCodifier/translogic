using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de rateio seguro
	/// </summary>
	public class RateioSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   Unidade Seguro
		/// </summary>
		public virtual UnidadeSeguro UnidadeSeguro { get; set; }

		/// <summary>
		///   Processo Seguro
		/// </summary>
		public virtual ProcessoSeguro ProcessoSeguro { get; set; }

		/// <summary>
		///   Percentual de Rateio
		/// </summary>
		public virtual double? PercentualRateio { get; set; }

		/// <summary>
		///   C�digo do usu�rio
		/// </summary>
		public virtual string CodUsuario { get; set; }

		/// <summary>
		///   Centro de custo da unidade
		/// </summary>
		public virtual string CentroCusto { get; set; }

		/// <summary>
		///   Data
		/// </summary>
		public virtual DateTime? Data { get; set; }
	}
}