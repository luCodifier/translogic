using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de an�lise seguro
	/// </summary>
    public class HistoricoSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   Processo Seguro
		/// </summary>
		public virtual ProcessoSeguro ProcessoSeguro { get; set; }
        
        /// <summary>
        ///   Usuario
        /// </summary>
        public virtual string Usuario { get; set; }

        /// <summary>
        ///   Descricao
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        ///   Data
        /// </summary>
        public virtual DateTime? TimeStamp { get; set; }

	}
}