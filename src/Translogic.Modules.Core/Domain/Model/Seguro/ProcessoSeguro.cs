using System;
using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Acesso;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de processo seguro
	/// </summary>
	public class ProcessoSeguro : EntidadeBase<int>
	{
		/// <summary>
		///   N�mero do processo de seguro
		/// </summary>
		public virtual int? NumeroProcesso { get; set; }

		/// <summary>
		///   Data do processo
		/// </summary>
		public virtual DateTime? DataProcesso { get; set; }

		/// <summary>
		///   Nome do vistoriador
		/// </summary>
		public virtual string Vistoriador { get; set; }

		/// <summary>
		///   Data da vistoria
		/// </summary>
		public virtual DateTime? DataVistoria { get; set; }

		/// <summary>
		///   Usu�rio que cadastrou o processo
		/// </summary>
		public virtual string UsuarioCadastro { get; set; }

		/// <summary>
		///   Data que cadastro do processo foi conclu�do
		/// </summary>
		public virtual DateTime? DataCadastro { get; set; }

		/// <summary>
		///   Data do registro
		/// </summary>
		public virtual DateTime? Data { get; set; }

		/// <summary>
		///   Data sinistro
		/// </summary>
		public virtual DateTime? DataSinistro { get; set; }

		/// <summary>
		///   Indicador de Indevido 'S' para Indevido 'N' para Devido
		/// </summary>
		public virtual string IndicadorIndevido { get; set; }

		/// <summary>
		///   Indicador da situa��o do processo. Valores: 'P' - Pendente 'U' - UP analisando 'G' - Ger�ncia Analisando 'A' - Analisado 'R' - Preparado para pagamento 'O' - Pago 'E' - Encerrado sem pagamento
		/// </summary>
		public virtual string IndicadorSituacao { get; set; }

        /// <summary>
        ///   Id da Conta Cont�bil do Processo
        /// </summary>
        public virtual ContaContabilSeguro ContaContabil { get; set; }

        /// <summary>
        ///   Id do Cliente Seguro
        /// </summary>
        public virtual ClienteSeguro ClienteSeguro { get; set; }

        /// <summary>
        ///   N�mero da Sindicancia
        /// </summary>
        public virtual long? Sindicancia { get; set; }

	}
}