using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Seguro
{
	/// <summary>
	///   Classe de cliente seguro
	/// </summary>
	public class Manga : EntidadeBase<int>
	{
		/// <summary>
        ///  Manga
		/// </summary>
        public virtual string LM_MANGA { get; set; }

        /// <summary>
        ///  Peso MAX Manga
        /// </summary>
        public virtual double LM_LIMITE_PESO { get; set; }
	}
}