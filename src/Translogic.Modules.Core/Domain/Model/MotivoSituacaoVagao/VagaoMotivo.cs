﻿using System;

namespace Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao
{
    using ALL.Core.Dominio;

    public class VagaoMotivo : EntidadeBase<int>
    {
        #region Propriedades

        /// <summary>
        /// Código do Vagao Motivo
        /// </summary>
        public virtual int IdVagaoMotivo { get; set; }

        /// <summary>
        /// Código do Vagão
        /// </summary>
        public virtual int IdVagao { get; set; }

        /// <summary>
        /// Código do Motivo
        /// </summary>
        public virtual int IdMotivo { get; set; }

        /// <summary>
        /// Data da Inclusão
        /// </summary>
        public virtual DateTime? DataDaInclusao { get; set; }

        /// <summary>
        /// Usuário da Inclusão
        /// </summary>
        public virtual string UsuarioInclusao { get; set; }

        /// <summary>
        /// Data da Exclusão
        /// </summary>
        public virtual DateTime? DataDaExclusao { get; set; }

        /// <summary>
        /// Usuario da Exclusão
        /// </summary>
        public virtual string UsuarioExclusao { get; set; }

        /// <summary>
        /// Data da ultima atualização
        /// </summary>
        public virtual DateTime DataUltimaAtualizacao { get; set; }

        #endregion
    }
}