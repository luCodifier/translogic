﻿using Translogic.Core.Infrastructure.Web;

namespace Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.AcessoDados;
    using ALL.Core.Dominio;
    using Translogic.Modules.Core.Domain.Model.Dto;

    using System;

    public interface IMotivoSituacaoVagaoRepository : IRepository<VagaoMotivo, int>
    {
        IList<MotivoSituacaoVagaoLotacaoDto> ObterLotacoes();

        IList<MotivoSituacaoVagaoSituacaoDto> ObterTodasSituacoes();

        IList<MotivoSituacaoVagaoSituacaoDto> ObterSituacoes();

        IList<MotivoSituacaoVagaoMotivoDto> ObterMotivos(string idSituacao);

        ResultadoPaginado<MotivoSituacaoVagaoDto> ObterConsultaDeVagoesPatio(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string listaDeVagoes, string idLocal, string idOS, 
                                                                              string idLotacao, string idSituacao, string idMotivo);

        VagaoMotivo ObterVagaoMotivo(string idVagaoMotivo, string idVagao, string idMotivo);
    }
}
