﻿namespace Translogic.Modules.Core.Domain.Model.MotivoSituacaoVagao.Repositories
{
    using ALL.Core.AcessoDados;
    using Translogic.Modules.Core.Domain.Model.QuadroEstados.Dimensoes;

    public interface ISituacaoRepository : IRepository<Situacao, int?>
    {
    }
}