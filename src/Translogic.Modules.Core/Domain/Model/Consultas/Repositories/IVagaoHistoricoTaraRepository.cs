﻿namespace Translogic.Modules.Core.Domain.Model.Consultas.Repositories
{
    using System.Collections.Generic;
    using ALL.Core.Dominio;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.Dto;

    public interface IVagaoHistoricoTaraRepository
    {
        
        /// <summary>
        /// Retorna Lista de registros conforme filtro informado pelo usuario
        /// </summary>
        /// <param name="detalhesPaginacaoWeb"></param>
        /// <param name="vagoes"></param>
        /// <param name="serie"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <returns></returns>
        ResultadoPaginado<VagaoHistoricoTaraDto> ObterHistoricoTaras(DetalhesPaginacaoWeb detalhesPaginacaoWeb, string vagoes, string serie, string dataInicial, string dataFinal);    
        
        /// <summary>
        /// Metodo retorna lista de registros, para a exportação excel, conforme filtro informado pelo usuario
        /// </summary>
        /// <param name="vagoes"></param>
        /// <param name="serie"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFinal"></param>
        /// <returns></returns>
        IEnumerable<VagaoHistoricoTaraDto> ExportarHistoricoTaras(string vagoes, string serie, string dataInicial, string dataFinal);
    }
}