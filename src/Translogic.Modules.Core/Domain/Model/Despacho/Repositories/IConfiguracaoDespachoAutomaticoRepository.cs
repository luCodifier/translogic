﻿using System.Collections.Generic;
using ALL.Core.AcessoDados;

namespace Translogic.Modules.Core.Domain.Model.Despacho.Repositories
{
    using ALL.Core.Dominio;
    using Dto.Despacho;
    using Translogic.Core.Infrastructure.Web;

    public interface IConfiguracaoDespachoAutomaticoRepository : IRepository<ConfiguracaoDespachoAutomatico, int>
    {
        ResultadoPaginado<ConfiguracaoDespachoAutomaticoDto> Pesquisar(DetalhesPaginacaoWeb pagination, ConfiguracaoDespachoAutomaticoFiltroDto filtro);

        bool JaCadastrado(int id, string cliente, int areaOperacional, string segmento);

        IList<ConfiguracaoDespachoAutomaticoDto> Exportar(ConfiguracaoDespachoAutomaticoFiltroDto filtro);
    }
}
