﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Domain.Model.Despacho
{
    public class ConfiguracaoDespachoAutomatico : EntidadeBase<int>
    {
        public ConfiguracaoDespachoAutomatico()
        {
            Data = DateTime.Now;
        }

        public virtual string Cliente { get; set; }
        public virtual int AreaOperacionaId { get; set; }
        public virtual string Terminal { get; set; }
        public virtual string Patio { get; set; }
        public virtual int? LocalizacaoId { get; set; }
        public virtual string Segmento { get; set; }
        public virtual DateTime Data { get; set; }
        public virtual string Manual { get; set; }

        public virtual bool EValido()
        {
            return !string.IsNullOrEmpty(Cliente) && AreaOperacionaId > 0 && !string.IsNullOrEmpty(Segmento);
        }
    }
}