namespace Translogic.Modules.Core.Domain.Model.Codificador
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Classe que determina uma configuração do translogic
	/// </summary>
	public class ConfiguracaoTranslogic : EntidadeBase<string>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Valor da configuração.
		/// </summary>
		public virtual string Valor { get; set; }

		/// <summary>
		/// Descrição da configuração
		/// </summary>
		public virtual string Descricao { get; set; }

		/// <summary>
		/// Data de criação da configuração.
		/// </summary>
		public virtual DateTime DataCriacao { get; set; }

		#endregion
	}
}