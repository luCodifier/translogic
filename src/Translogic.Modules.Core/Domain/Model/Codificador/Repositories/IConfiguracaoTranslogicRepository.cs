namespace Translogic.Modules.Core.Domain.Model.Codificador.Repositories
{
	using ALL.Core.AcessoDados;

	/// <summary>
	/// Interface de repositório das configurações do translogic
	/// </summary>
	public interface IConfiguracaoTranslogicRepository : IRepository<ConfiguracaoTranslogic, string>
	{
		#region MÉTODOS
		#endregion
	}
}