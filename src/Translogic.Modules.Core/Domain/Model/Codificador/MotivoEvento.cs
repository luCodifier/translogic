﻿namespace Translogic.Modules.Core.Domain.Model.Codificador
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa um motivo de um Evento
	/// </summary>
	/// <remarks>Utilizado na gravação de uma SituacaoTrem</remarks>
	public class MotivoEvento : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Código do Motivo
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição detalhada do Motivo
		/// </summary>
		public virtual string DescricaoDetalhada { get; set; }

		/// <summary>
		/// Descrição Resumida do Motivo
		/// </summary>
		public virtual string DescricaoResumida { get; set; }

		/// <summary>
		/// <see cref="Evento"/> do Motivo
		/// </summary>
		public virtual Evento Evento { get; set; }

		#endregion
	}
}