﻿namespace Translogic.Modules.Core.Domain.Model.Codificador
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Representa toda ação que um veículo sofre quando muda sua situação, de acordo com a sequencia lógica
	/// </summary>
	public class Evento : EntidadeBase<int>
	{
		#region PROPRIEDADES

		/// <summary>
		/// Indica se o Evento é aplicável à um Container
		/// </summary>
		public virtual bool? AplicavelContainer { get; set; }

		/// <summary>
		/// Indica se o Evento é aplicável à uma Locomotiva
		/// </summary>
		public virtual bool? AplicavelLocomotiva { get; set; }

		/// <summary>
		/// Indica se o Evento é aplicável à um Trem
		/// </summary>
		public virtual bool? AplicavelTrem { get; set; }

		/// <summary>
		/// Indica se o Evento é aplicável à um Vagao
		/// </summary>
		public virtual bool? AplicavelVagao { get; set; }

		/// <summary>
		/// Código do Evento
		/// </summary>
		public virtual string Codigo { get; set; }

		/// <summary>
		/// Descrição do Evento
		/// </summary>
		public virtual string Descricao { get; set; }

		#endregion
	}
}