﻿namespace Translogic.Modules.Core.Domain.Jobs.SefazNfe
{
    using Interfaces.Jobs;
    using Services.FluxosComerciais;
    public class DFeServiceJob : IJob
    {

        private readonly DfeSefazService _dfeSefazService;

        public DFeServiceJob(DfeSefazService dfeSefazService)
        {
            _dfeSefazService = dfeSefazService;
        }

        public bool Execute(params object[] args)
        {
            _dfeSefazService.ProcessarDfe();   
            return true;
        }
    }
}