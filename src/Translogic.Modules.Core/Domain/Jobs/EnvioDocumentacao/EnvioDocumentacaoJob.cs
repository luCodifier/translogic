﻿namespace Translogic.Modules.Core.Domain.Jobs.EnvioDocumentacao
{
    using Speed.Common;
    using System;
    using System.Diagnostics;
    using System.Linq;
    using Translogic.Core;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.EDI.Infrastructure;

    public class EnvioDocumentacaoJob : IJob
    {
        private readonly string _source = "Translogic.JobRunner";
        private readonly string _logName = "Envio de Documentação";

        private readonly DocumentacaoVagaoService _documentacaoService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvioDocumentacaoJob"/> class.
        /// </summary>
        /// <param name="documentacaoService"> Serviço de documentação de vagões injetado.</param>
        public EnvioDocumentacaoJob(DocumentacaoVagaoService documentacaoService)
        {
            _documentacaoService = documentacaoService;
        }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            var sucesso = true;
            LogHelper.Log("Iniciando job EnvioDocumentacaoJob", EventLogEntryType.Information, _source, _logName);

            try
            {
                int[] ids = args.Select(p => p.ToInt32()).Where(p => p > 0).ToArray();

                _documentacaoService.ExecutarEnvioDocumentacao(ids);
            }
            catch (Exception ex)
            {
                sucesso = false;
            }

            LogHelper.Log("Job EnvioDocumentacaoJob finalizado", EventLogEntryType.Information, _source, _logName);
            return sucesso;
        }
    }
}