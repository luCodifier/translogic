﻿namespace Translogic.Modules.Core.Domain.Jobs.EnvioDocumentacao
{
    using Speed.Common;
    using System;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Interfaces.Jobs;

    public class GerenciamentoDocumentacaoJob : IJob
    {
        private readonly DocumentacaoVagaoService _documentacaoService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GerenciamentoDocumentacaoJob"/> class.
        /// </summary>
        /// <param name="documentacaoService"> Serviço de documentação de vagões injetado.</param>
        public GerenciamentoDocumentacaoJob(DocumentacaoVagaoService documentacaoService)
        {
            _documentacaoService = documentacaoService;
        }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            var sucesso = true;
            Console.WriteLine(@"Iniciando job GerenciamentoDocumentacaoJob");
            try
            {
                var args2 = args == null ? null : args.Select(p => p as string).Where(p => !string.IsNullOrWhiteSpace(p)).ToArray();
                _documentacaoService.ExecutarGerenciamentoDocumentacao(args2);
            }
            catch (Exception ex)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job EnvioDocumentacaoJob finalizado");
            return sucesso;
        }
    }
}