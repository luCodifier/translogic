﻿namespace Translogic.Modules.Core.Domain.Jobs
{
    using Interfaces.Jobs;
    using Services;

    public class GerarArquivosXmlJob : IJob
    {
        private readonly GerarArquivosXmlService _service;

        public GerarArquivosXmlJob(GerarArquivosXmlService service)
        {
            _service = service;
        }

        public bool Execute(params object[] args)
        {
            try
            {
                _service.GerarArquivos();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}