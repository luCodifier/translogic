﻿namespace Translogic.Modules.Core.Domain.Jobs.Trem
{
    using System;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Interfaces.Jobs;

    /// <summary>
    /// Job Responsável por atualizar a tabela de movimentacao trem real.
    /// </summary>
    public class MovimentacaoTremRealJob : IJob
    {
        private readonly CentroTremService _centroTremService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovimentacaoTremRealJob"/> class.
        /// </summary>
        /// <param name="centroTremService">Serviço centro trem injetado.</param>
        public MovimentacaoTremRealJob(CentroTremService centroTremService)
        {
            _centroTremService = centroTremService;
        }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job MovimentacaoTremRealJob");
            try
            {
                _centroTremService.AtualizarMovimentacaoTremReal();
            }
            catch (Exception)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job MovimentacaoTremRealJob finalizado");
            return sucesso;
        }
    }
}