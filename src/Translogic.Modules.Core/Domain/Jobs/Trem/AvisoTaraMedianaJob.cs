﻿namespace Translogic.Modules.Core.Domain.Jobs.Trem
{
    using System;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Interfaces.Jobs;

    /// <summary>
    /// Job Responsável por atualizar a tabela de movimentacao trem real.
    /// </summary>
    public class AvisoTaraMedianaJob : IJob
    {
        private readonly AvisoTaraMedianaService _avisoTaraMedianaService;

        /// <summary>
        /// Job para envio de aviso das taras medianas
        /// </summary>
        /// <param name="avisoTaraMedianaService">Serviço aviso tara mediana injetado.</param>
        public AvisoTaraMedianaJob(AvisoTaraMedianaService avisoTaraMedianaService)
        {
            _avisoTaraMedianaService = avisoTaraMedianaService;
        }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job AvisoTaraMedianaJob");
            try
            {
                _avisoTaraMedianaService.EnviarEmailAviso();
            }
            catch (Exception)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job AvisoTaraMedianaJob finalizado");
            return sucesso;
        }
    }
}