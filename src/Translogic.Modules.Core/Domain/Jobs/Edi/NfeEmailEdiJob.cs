﻿namespace Translogic.Modules.Core.Domain.Jobs.Edi
{
    using System;
    using Interfaces.Jobs;
    using Services.Edi;
    using Translogic.Modules.Core.Spd;
    using Translogic.Modules.EDI.Spd.BLL;

    /// <summary>
    /// Classe responsável por executar o job de leitura de emails das Nfe's
    /// </summary>
    public class NfeEmailEdiJob : IJob
    {
        private readonly EdiReadNfeService _readNfeEmailEdiReadNfeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NfeEmailEdiJob"/> class.
        /// </summary>
        /// <param name="ediReadNfeService"> Serviço de nfe injetado.</param>
        public NfeEmailEdiJob(EdiReadNfeService ediReadNfeService)
        {
            _readNfeEmailEdiReadNfeService = ediReadNfeService;
        }

        /// <summary>
        /// Hora que será injetado pelo conteiner
        /// </summary>
        public int Hora { get; set; }

        /// <summary>
        /// Order que será injetado pelo conteiner
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job NfeEmailEdiJob");
            try
            {
                // Chama 1 método do Speed pra forçar a Compilação do EDI e não poluir a performance depois
                // Lembrando que a base default é a translogic. Então é necessário executar dentro do método ExecuteDbi
                Dbs.ExecuteEdi(db =>  BL_Edi2Status.SelectByPk(db, 1));

                string chave = string.Empty;
                _readNfeEmailEdiReadNfeService.ProcessarArquivos();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "NfeEmailEdiJob Exception. Abortando ....");
                sucesso = false;
            }

            Sis.LogTrace(@"Job NfeEmailEdiJob finalizado");
            return sucesso;
        }
    }
}