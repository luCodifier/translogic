﻿namespace Translogic.Modules.Core.Domain.Jobs.Edi
{
    using System;
    using Interfaces.Jobs;
    using Services.Edi;

    /// <summary>
    /// Classe responsável por executar o job de leitura de emails das Nfe's
    /// </summary>
    public class NfeEmailEdiGestaoFluxoJob : IJob
    {
        private readonly EdiReadNfeService _readNfeEmailEdiReadNfeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NfeEmailEdiJob"/> class.
        /// </summary>
        /// <param name="ediReadNfeService"> Serviço de nfe injetado.</param>
        public NfeEmailEdiGestaoFluxoJob(EdiReadNfeService ediReadNfeService)
        {
            _readNfeEmailEdiReadNfeService = ediReadNfeService;
        }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job NfeEmailEdiGestaoFluxoJob");
            try
            {
                string chave = string.Empty;
                _readNfeEmailEdiReadNfeService.ValidaExistenciaFluxoNota();
            }
            catch (Exception)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job NfeEmailEdiGestaoFluxoJob finalizado");
            return sucesso;
        }
    }
}
