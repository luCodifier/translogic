namespace Translogic.Modules.Core.Domain.Jobs.Edi
{
	using System;
	using FluxosComerciais;
	using Interfaces.Jobs;
	using Services.FluxosComerciais;

	/// <summary>
	/// Classe responsavel por executar o job de correcao das Nfe
	/// </summary>
	public class CorrecaoNfeEdiJob : IJob
	{
		private readonly NfeService _nfeService;

		/// <summary>
		/// Initializes a new instance of the <see cref="CorrecaoNfeEdiJob"/> class.
		/// </summary>
		/// <param name="nfeService"> Servi�o de nfe injetado.</param>
		public CorrecaoNfeEdiJob(NfeService nfeService)
		{
			_nfeService = nfeService;
		}

		/// <summary>
		/// M�todo principal da interface IJob que executa a tarefa
		/// </summary>
		/// <param name="args">Argumentos passados para a execu��o da tarefa</param>
		/// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contr�rio</returns>
		public bool Execute(params object[] args)
		{
			bool sucesso = true;
			Console.WriteLine(@"Iniciando job CorrecaoNfeEdiJob");
			try
			{
				string chave = string.Empty;

				_nfeService.AtualizarDadosNfeEdi();
			}
			catch (Exception)
			{
				sucesso = false;
			}

			Console.WriteLine(@"Job CorrecaoNfeEdiJob finalizado");
			return sucesso;
		}
	}
}