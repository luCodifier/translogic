﻿namespace Translogic.Modules.Core.Domain.Jobs.Edi
{
    using System;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Modules.Core.Interfaces.Jobs;

    /// <summary>
    /// Classe responsavel por executar o job de correcao das Nfe
    /// </summary>
    public class AtualizacaoNfePdfId : IJob
    {
        private readonly NfePdfService _nfePdfService;

        /// <summary>
		/// Initializes a new instance of the <see cref="AtualizacaoNfePdfId"/> class.
		/// </summary>
		/// <param name="nfeService"> Serviço de nfe injetado.</param>
		public AtualizacaoNfePdfId(NfePdfService nfePdfService)
		{
			_nfePdfService = nfePdfService;
		}

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job AtualizacaoNfePdfId");
            try
            {
                _nfePdfService.AtualizacaoNfePdfSemIdNota();
            }
            catch (Exception)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job AtualizacaoNfePdfId finalizado");
            return sucesso;
        }
    }
}