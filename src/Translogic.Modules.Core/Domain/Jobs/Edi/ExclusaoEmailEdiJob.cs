﻿namespace Translogic.Modules.Core.Domain.Jobs.Edi
{
    using System;
    using Interfaces.Jobs;
    using Services.Edi;

    /// <summary>
    /// Classe responsável por executar o job de exclusão de emails das Nfe's já lidas das pastas secundárias
    public class ExclusaoEmailEdiJob : IJob
    {
        private readonly EdiReadNfeService _readNfeEmailEdiReadNfeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NfeEmailEdiJob"/> class.
        /// </summary>
        /// <param name="ediReadNfeService"> Serviço de nfe injetado.</param>
        public ExclusaoEmailEdiJob(EdiReadNfeService ediReadNfeService)
        {
            _readNfeEmailEdiReadNfeService = ediReadNfeService;
        }

        /// <summary>
        /// Pasta (para exclusão de e-mails) que será injetado pelo conteiner
        /// </summary>
        public string PastaExclusaoEmails { get; set; }

        /// <summary>
        /// Método principal da interface IJob que executa a tarefa
        /// </summary>
        /// <param name="args">Argumentos passados para a execução da tarefa</param>
        /// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contrário</returns>
        public bool Execute(params object[] args)
        {
            bool sucesso = true;
            Console.WriteLine(@"Iniciando job ExclusaoEmailEdiJob");
            try
            {
                string chave = string.Empty;
                _readNfeEmailEdiReadNfeService.ExcluirEmailsMovidosCaixasSecundarias(this.PastaExclusaoEmails);
            }
            catch (Exception)
            {
                sucesso = false;
            }

            Console.WriteLine(@"Job ExclusaoEmailEdiJob finalizado");
            return sucesso;
        }
    }
}