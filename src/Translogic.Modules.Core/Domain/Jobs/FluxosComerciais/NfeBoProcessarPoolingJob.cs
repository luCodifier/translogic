namespace Translogic.Modules.Core.Domain.Jobs.FluxosComerciais
{
	using System;
	using Interfaces.Jobs;
	using Services.FluxosComerciais;

	/// <summary>
	/// Classe NfeBoProcessarPoolingJob
	/// </summary>
	public class NfeBoProcessarPoolingJob : IJob
	{
		private readonly NfeBoService _nfeBoService;

		/// <summary>
		/// Initializes a new instance of the <see cref="NfeBoProcessarPoolingJob"/> class.
		/// </summary>
		/// <param name="nfeBoService"> Servi�o de nota fiscal injetado. </param>
		public NfeBoProcessarPoolingJob(NfeBoService nfeBoService)
		{
			_nfeBoService = nfeBoService;
		}

		/// <summary>
		/// UF Ibge que ser� injetado pelo conteiner
		/// </summary>
		public string UfIbge { get; set; }

		/// <summary>
		/// M�todo principal da interface IJob que executa a tarefa
		/// </summary>
		/// <param name="args">Argumentos passados para a execu��o da tarefa</param>
		/// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contr�rio</returns>
		public bool Execute(params object[] args)
		{
			bool sucesso = true;
			Console.WriteLine(@"Iniciando job NfeBoProcessarPoolingJob");
			try
			{
				_nfeBoService.ProcessarNfePooling(UfIbge);
			}
			catch (Exception)
			{
				sucesso = false;
			}

			Console.WriteLine(@"Job NfeBoProcessarPoolingJob finalizado");
			return sucesso;
		}
	}
}