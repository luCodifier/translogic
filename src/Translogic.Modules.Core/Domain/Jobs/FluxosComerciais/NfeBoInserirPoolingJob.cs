namespace Translogic.Modules.Core.Domain.Jobs.FluxosComerciais
{
	using System;
	using Interfaces.Jobs;
	using Services.FluxosComerciais;

	/// <summary>
	/// Classe NfeBoInserirPoolingJob
	/// </summary>
	public class NfeBoInserirPoolingJob : IJob
	{
		private readonly NfeBoService _nfeBoService;

		/// <summary>
		/// Initializes a new instance of the <see cref="NfeBoProcessarPoolingJob"/> class.
		/// </summary>
		/// <param name="nfeBoService">
		/// Servi�o de nota fiscal injetado.
		/// </param>
		public NfeBoInserirPoolingJob(NfeBoService nfeBoService)
		{
			_nfeBoService = nfeBoService;
		}

		/// <summary>
		/// M�todo principal da interface IJob que executa a tarefa
		/// </summary>
		/// <param name="args">Argumentos passados para a execu��o da tarefa</param>
		/// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contr�rio</returns>
		public bool Execute(params object[] args)
		{
			bool sucesso = true;
			Console.WriteLine(@"Iniciando job NfeBoInserirPoolingJob");
			try
			{
				_nfeBoService.InserirNfeBoPooling();
			}
			catch (Exception)
			{
				sucesso = false;
			}

			Console.WriteLine(@"Job NfeBoInserirPoolingJob finalizado");
			return sucesso;
		}
	}
}