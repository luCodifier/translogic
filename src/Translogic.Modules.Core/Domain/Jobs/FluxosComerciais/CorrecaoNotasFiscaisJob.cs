namespace Translogic.Modules.Core.Domain.Jobs.FluxosComerciais
{
    using System;
    using Services.FluxosComerciais;
    using Translogic.Modules.Core.Interfaces.Jobs;

    /// <summary>
	/// Classe responsavel por executar o job das viagens analisadas
	/// </summary>
	public class CorrecaoNotasFiscaisJob : IJob
	{
		private readonly NotaFiscalService _notaFiscalService;

		/// <summary>
        /// Initializes a new instance of the <see cref="CorrecaoNotasFiscaisJob"/> class.
		/// </summary>
        /// <param name="notaFiscalService">
		/// Servi�o de nota fiscal injetado.
		/// </param>
        public CorrecaoNotasFiscaisJob(NotaFiscalService notaFiscalService)
		{
            _notaFiscalService = notaFiscalService;
		}

		/// <summary>
		/// M�todo principal da interface IJob que executa a tarefa
		/// </summary>
		/// <param name="args">Argumentos passados para a execu��o da tarefa</param>
		/// <returns>Retorna verdadeiro caso tenha conseguido executar a tarefa e falso caso contr�rio</returns>
		public bool Execute(params object[] args)
		{
			bool sucesso = true;
            Console.WriteLine(@"Iniciando job CorrecaoNotasFiscaisJob");
			try
			{
				_notaFiscalService.AtualizarDadosNotasFiscais();
			}
			catch (Exception)
			{
				sucesso = false;
			}

            Console.WriteLine(@"Job CorrecaoNotasFiscaisJob finalizado");
			return sucesso;
		}
	}
}