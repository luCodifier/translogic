﻿namespace Translogic.Modules.Core.Domain.Jobs.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Interfaces.Jobs;

    public class CteCanceladoRefaturamentoJob : IJob
    {
        #region Fields

        private readonly CteCanceladoRefaturamentoService _cteCanceladoRefaturamentoService;

        #endregion

        #region Constructors and Destructors

        public CteCanceladoRefaturamentoJob(CteCanceladoRefaturamentoService cteCanceladoRefaturamentoService)
        {
            this._cteCanceladoRefaturamentoService = cteCanceladoRefaturamentoService;
        }

        #endregion

        #region Public Methods and Operators

        public bool Execute(params object[] args)
        {
            return this._cteCanceladoRefaturamentoService.InserirCteCanceladoRefaturamento();
        }

        #endregion
    }
}