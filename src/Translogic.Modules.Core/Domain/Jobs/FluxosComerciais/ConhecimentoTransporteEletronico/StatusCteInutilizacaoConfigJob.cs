﻿namespace Translogic.Modules.Core.Domain.Jobs.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Interfaces.Jobs;

    public class StatusCteInutilizacaoConfigJob : IJob
    {
        #region Fields

        private readonly CteInutilizacaoConfigService _cteInutilizacaoConfigService;

        #endregion

        #region Constructors and Destructors

        public StatusCteInutilizacaoConfigJob(CteInutilizacaoConfigService cteInutilizacaoConfigService)
        {
            this._cteInutilizacaoConfigService = cteInutilizacaoConfigService;
        }

        #endregion

        #region Public Methods and Operators

        public bool Execute(params object[] args)
        {
            return this._cteInutilizacaoConfigService.AtualizarStatusCtesInutilizacaoConfig();
        }

        #endregion
    }
}