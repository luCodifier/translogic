﻿namespace Translogic.Modules.Core.Domain.Jobs.AcompanhamentoFat
{
    using System;
    using System.Globalization;
    using System.Text;
    using System.Threading;
    using Interfaces.Jobs;
    using Services.AcompanhamentoFat;

    public class AcompanhamentoFatJob : IJob
    {
        private readonly AcompanhamentoFatService _service;
        private readonly AcompanhamentoFatMalhaNorteService _serviceMn;

        public AcompanhamentoFatJob(AcompanhamentoFatService service, AcompanhamentoFatMalhaNorteService serviceMn)
        {
            _service = service;
            _serviceMn = serviceMn;
        }

        public bool Execute(params object[] args)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
                EnviarDados();
                return true; 
            }
            catch
            {
                return false;
            }
        }

        public void EnviarDados()
        {
            _serviceMn.EnviarAcompanhamento();
            _service.EnviarAcompanhamento();
        }
    }
}