﻿namespace Translogic.Modules.Core.Domain.Jobs.AvisoFat
{
    using Interfaces.Jobs;
    using Services.AvisoFat;

    public class AvisoVagoesNaoFaturadosJob : IJob
    {
        private readonly AvisoVagoesNaoFaturadosService _avisoService;

        public AvisoVagoesNaoFaturadosJob(AvisoVagoesNaoFaturadosService avisoService)
        {
            _avisoService = avisoService;
        }

        public bool Execute(params object[] args)
        {
            try
            {
                _avisoService.EnviarAvisos();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}