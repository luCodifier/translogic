﻿namespace Translogic.Modules.Core.Domain.Jobs
{
    using Interfaces.Jobs;
    using Services;

    public class GerarArquivosPdfJob : IJob
    {
        private readonly GerarArquivosPdfService _service;

        public GerarArquivosPdfJob(GerarArquivosPdfService service)
        {
            _service = service;
        }

        public bool Execute(params object[] args)
        {
            try
            {
                _service.GerarArquivos();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}