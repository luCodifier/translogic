﻿namespace Translogic.Modules.Core.Domain.Jobs.Petrobras
{
    using System;
    using System.Globalization;
    using Interfaces.Jobs;
    using Model.FluxosComerciais.Nfes.Repositories;
    using Services.FluxosComerciais.NfePetrobras;

    public class NotasPetrobrasServiceJob : IJob
    {

        private readonly NfePetrobrasService _nfePetrobrasService;
        private readonly IParametrosPetrobrasRepository _parametrosPetrobras;

        public NotasPetrobrasServiceJob(NfePetrobrasService nfePetrobrasService, IParametrosPetrobrasRepository parametrosPetrobras)
        {
            _nfePetrobrasService = nfePetrobrasService;
            _parametrosPetrobras = parametrosPetrobras;
        }

        public bool Execute(params object[] args)
        {
            var consultas = _parametrosPetrobras.ObterTodos();
            var dia = DateTime.Now;
            var strDia = dia.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Replace("/", "");
            var hora = dia.ToString("HH");
            var horam = dia.AddHours(-1).ToString("HH");

            foreach (var item in consultas)
            {
                var input = new NfePetrobrasRequest
                {
                    Cnpj = item.CnpjBusca,
                    DataEmissao = strDia,
                    HoraEmissaoFinal = hora + "00",
                    HoraEmissaoInicial = horam + "00",
                    Token = item.Token,
                    Url = item.Url
                };

                _nfePetrobrasService.ObterNotasBr(input);   
            }

            return true;
        }
    }
}