﻿namespace Translogic.Modules.Core.Spd.BLL
{

    /// <summary>
    /// Métodos úteis de notas fiscais
    /// </summary>
    public class BL_NotasFiscais
    {

        public static string GetXml(string chaveNfe)
        {
            return BL_VwNfeXml.GetXml(chaveNfe);
        }

    }
}