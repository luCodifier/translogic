using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_StatusRetornoNfe
    {
        public static StatusRetornoNfe ObterPorCodigo(string codigo)
        {
            return SelectSingle(new StatusRetornoNfe { Codigo = codigo });
        }

        public static StatusRetornoNfe ObterPorCodigo(Database db, string codigo)
        {
            return SelectSingle(db, new StatusRetornoNfe { Codigo = codigo });
        }

    }

}
