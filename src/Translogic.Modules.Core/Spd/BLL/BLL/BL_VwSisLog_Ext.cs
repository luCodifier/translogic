using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Core;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwSisLog
    {

        public static List<VwSisLog> SelectRelatorio(DetalhesPaginacaoWeb pagination, DateTime? dataIni, DateTime? dataFim, string mensagem, string exception, string sistema, string tipo)
        {
            if (dataIni == null)
                dataIni = DateTime.Today;
            if (dataFim == null)
                dataFim = DateTime.Today;
            if (pagination.Inicio == null)
                pagination.Inicio = 0;

            dataIni = dataIni.Value.Date;
            dataFim = dataFim.Value.Date.AddDays(1).AddMilliseconds(-1);

            pagination.Limite = 50;

            string sort;
            if (pagination.ColunasOrdenacao.Any())
                sort = string.Join(",", pagination.ColunasOrdenacao) + " " + pagination.Dir;
            else
                sort = "DATA DESC";

            var filters = new List<string>();

            filters.Add($"DATA BETWEEN {dataIni.OracleDate()} AND {dataFim.OracleDate()}");
            
            if (!string.IsNullOrWhiteSpace(mensagem))
                filters.Add($"MENSAGEM like '%{mensagem}%'");
            
            if (!string.IsNullOrWhiteSpace(exception))
                filters.Add($"EXCEPTION like '%{exception}%'");
            
            if (!string.IsNullOrWhiteSpace(sistema))
                filters.Add($"SISTEMA_NOME = '{sistema}'");
            
            if (!string.IsNullOrWhiteSpace(tipo))
                filters.Add($"TIPO_NOME = '{tipo}'");

            string where = string.Join(" AND ", filters);

            return BL_VwSisLog.SelectPage(where, pagination.Inicio.GetValueOrDefault(), pagination.Limite.GetValueOrDefault(), sort, 300);
        }

    }

}
