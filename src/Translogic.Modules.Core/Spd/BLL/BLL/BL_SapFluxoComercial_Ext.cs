using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using System.Linq;
using Speed.Data;
using Translogic.Core.Spd;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_SapFluxoComercial
    {

        /// <summary>
        /// Obtem Dados dos fluxos vigentes para o cnpj raiz do usuario logado
        /// </summary>
        /// <param name="cnpjRaiz">Cnpj raiz do usuario logado</param>
        /// <param name="origem">Origem do fluxo</param>
        /// <param name="destino">Destino do fluxo</param>
        /// <param name="mercadoria">Descricao da mescadoria</param>
        /// <returns>retorna dados dos fluxos</returns>
        public static List<SapFluxoComercial> ObterFluxos(string cnpjRaiz, string origem, string destino, string mercadoria, string chaveNota = null, string status = null)
        {
            var where = new List<string>();

            if (!string.IsNullOrEmpty(cnpjRaiz))
                where.Add($"EMIT_CNPJ_RAIZ = '{cnpjRaiz}'");

            if (!string.IsNullOrEmpty(chaveNota))
                where.Add($"NFE_CHAVE_NFE = '{chaveNota}'");

            if (!string.IsNullOrEmpty(mercadoria) && !mercadoria.Equals("0"))
                where.Add($"COD_PRODUTO = '{mercadoria}'");

            if (!string.IsNullOrEmpty(origem))
                where.Add($"AO_COD_AOP_ORI = '{origem}'");

            if (!string.IsNullOrEmpty(destino))
                where.Add($"AO_COD_AOP_DES = '{destino}'");

            if (!string.IsNullOrEmpty(status))
                where.Add($"STATUS_APROVACAO = '{status}'");

            string _where = null;
            if (where.Any())
            {
                _where = string.Join(" AND ", where);
            }

            return Select(_where);
        }

        public static List<SapFluxoComercial> ObterHistorico(
            DateTime dataIni, DateTime dataFim, string expedidorCnpj, string login, string chaveNota)
        {
            var where = new List<string>();
            dataFim = dataFim.Date.AddDays(1).AddSeconds(-1);
            string dataI = "TO_DATE('" + dataIni.ToString("ddMMyyyy") + "', 'DDMMYYYY')";
            string dataF = "TO_DATE('" + dataFim.ToString("ddMMyyyy HHmmss") + "', 'DDMMYYYY HH24MISS')";

            where.Add($"TIMESTAMP BETWEEN {dataI} AND {dataF}");

            if (!string.IsNullOrEmpty(expedidorCnpj))
            {
                where.Add($"CLI_EXP_CGC = '{expedidorCnpj}'");
            }

            if (!string.IsNullOrEmpty(login))
            {
                where.Add($"EMIT_LOGIN = '{login}'");
            }

            if (!string.IsNullOrEmpty(chaveNota))
            {
                where.Add($"NFE_CHAVE_NFE LIKE '{chaveNota}'");
            }

            return Select(string.Join(" AND ", where));
        }

    }

}
