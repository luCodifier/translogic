using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_ConfGeral
    {

        public static ConfGeralInfo GetInfo()
        {
            return Sys.RunInDb((db) => GetInfo(db));
        }
        public static ConfGeralInfo GetInfo(Database dbT)
        {
            ConfGeralInfo info = new ConfGeralInfo();

            //var x = generateInfo(dbT);

            var confs = BL_ConfGeral.Select(dbT);

            var props = info.GetType().GetProperties().GroupByToDictionaryDistinct(p => p.Name, StringComparer.OrdinalIgnoreCase);

            foreach (var conf in confs)
            {
                string propName = conf.Chave.ToPascalCase();
                var prop = props.GetValue(propName);
                if (prop != null)
                    prop.SetValue(info, conf.Valor, null);
            }

            return info;

        }

        private static string generateInfo(Database dbT)
        {
            var b = new StringBuilder();
            var confs = Select(dbT).OrderBy(p => p.Chave.ToLower());
            foreach (var conf in confs)
            {
                b.AppendLine("/// <summary>");
                b.AppendLine("/// " + conf.Descricao);
                b.AppendLine("/// </summary>");
                b.AppendLine("public string " + conf.Chave.ToPascalCase() + " {get; set;}");
            }
            return b.ToString();

        }

    }

}
