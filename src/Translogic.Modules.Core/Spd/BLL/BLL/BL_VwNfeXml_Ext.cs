using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfeXml
    {

        public static string GetXml(string chaveNfe)
        {
            var rec = SelectSingle(new VwNfeXml { Chave = chaveNfe });
            if (rec != null)
                return rec.Xml;
            else
                return null;
        }

    }

}

