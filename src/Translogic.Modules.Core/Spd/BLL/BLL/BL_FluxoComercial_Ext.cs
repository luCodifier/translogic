using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_FluxoComercial
    {

        public static FluxoComercial ObterPorCodigoSimplificado(Database db, string codigofluxo)
        {
            string where = $"SUBSTR(FX_COD_FLX, 3) = '{codigofluxo}' ORDER BY FX_ID_FLX";
            return SelectSingle(db, where);
        }

        public static FluxoComercial ObterPorCodigo(string codigo)
        {
            return SelectSingle(new FluxoComercial { Codigo = codigo });
        }

        public static decimal ObterDensidadeLiquido(Database db, string codigoFluxo)
        {
            string sql = string.Format(
    @"
SELECT
    DD.DS_DENSIDADE
FROM
    FLUXO_COMERCIAL FC
    JOIN MERCADORIA M ON M.MC_ID_MRC = FC.MC_ID_MRC
    JOIN DENSIDADE_MERCADORIA DD ON DD.MC_COD_MRC = M.MC_COD_MRC
WHERE
    FC.FX_COD_FLX = '{0}'", codigoFluxo);

            return Conv.ToDecimal(db.ExecuteScalar(sql));
        }

    }

}
