using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_Vagao
    {
        public static List<Vagao> SelectPorDespachos(Database db, IEnumerable<decimal?> vgIdVgs)
        {
            string where = string.Format("VG_ID_VG in ({0})", Conv.GetIn(vgIdVgs));
            return Select(db, where);
        }
    }

}
