using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_CidadeIbge
    {
        public static CidadeIbge ObterPorCodigoMunicipioIbge(Database db, int? ibgeMun)
        {
            return SelectSingle(new CidadeIbge { CodigoIbge = ibgeMun });
        }
    }

}
