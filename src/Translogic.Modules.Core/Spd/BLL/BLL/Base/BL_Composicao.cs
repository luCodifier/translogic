// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_Composicao : BLClass<Translogic.Modules.Core.Spd.Data.Composicao>
    {

        public static Translogic.Modules.Core.Spd.Data.Composicao SelectByPk(Database db, Decimal? _CpIdCps)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.Composicao>(string.Format("CP_ID_CPS={0}", _CpIdCps));
        }
        public static Translogic.Modules.Core.Spd.Data.Composicao SelectByPk(Decimal? _CpIdCps)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.Composicao>(string.Format("CP_ID_CPS={0}", _CpIdCps));
        }

        public static int DeleteByPk(Database db, Decimal? _CpIdCps)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.Composicao>(string.Format("CP_ID_CPS={0}", _CpIdCps));
        }
        public static int DeleteByPk(Decimal? _CpIdCps)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.Composicao>(string.Format("CP_ID_CPS={0}", _CpIdCps));
        }



    }

}
