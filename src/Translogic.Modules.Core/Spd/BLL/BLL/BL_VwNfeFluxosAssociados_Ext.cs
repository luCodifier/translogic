using System;
using System.Collections.Generic;
using System.Data;
using ALL.Core.Dominio;
using Speed.Data;
using Translogic.Core;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfeFluxosAssociados
    {

        /// <summary>
        /// Fluxos associados das Notas Fiscais
        /// </summary>
        /// <param name="chavesNfe"> chaves nfe, separadas por v�rgula ou ponto e v�rgula</param>
        /// <param name="somenteVigentes">Se retorna somente os vigentes: FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)</param>
        /// <param name="tomadorBloqueado">Status do Tomador: null, S ou N</param>
        /// <returns> Retorna lista de Objetos do tipo VwNfeFluxosAssociados </returns>
        public static IList<VwNfeFluxosAssociados> ObterFluxosAssociadosChaveNfe(string chavesNfe,bool somenteVigentes, string cnpjRaiz = null, string tomadorBloqueado = null)
        {
            var chaves = Helper.FormataListaStr(chavesNfe, "," , true);
            var where = new List<string>();

            where.Add("CHAVE_NFE IN (" + chaves + ")");

            if (somenteVigentes)
                where.Add("FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)");

            if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                where.Add("USUARIO_CNPJ LIKE '%" + cnpjRaiz + "%'");

            if (!string.IsNullOrWhiteSpace(tomadorBloqueado))
                where.Add("TOMADOR_BLOQUEADO = '" + tomadorBloqueado + "'");

            var listSpd = BL_VwNfeFluxosAssociados.Select(string.Join(" AND " , where));
            return listSpd;
        }

    }

}
