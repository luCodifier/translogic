using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Core;
using Translogic.Core.Spd;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfe
    {
        public static VwNfe ObterPorChaveNfe(string chaveNfe)
        {
            return SelectSingle(new VwNfe { ChaveNfe = chaveNfe });
        }
        public static VwNfe ObterPorChaveNfe(Database db, string chaveNfe)
        {
            return SelectSingle(db, new VwNfe { ChaveNfe = chaveNfe });
        }

        public static List<VwNfe> ObterDadosNfesBancoDados(string chavesNfe)
        {
            chavesNfe = Helper.FormataListaStr(chavesNfe, ",", true);
            return Select("NFE_CHAVE IN ("  + chavesNfe + ")");
        }


    }

}
