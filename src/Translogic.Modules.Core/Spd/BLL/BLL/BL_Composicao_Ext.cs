using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_Composicao
    {
        public static List<Composicao> SelectPorIntegracoes(Database db, List<MrsInterfEnvioWs> integracoes)
        {
            return SelectPorIntegracoes(db, integracoes.Select(p => p.IdComposicao));
        }

        public static List<Composicao> SelectPorIntegracoes(Database db, IEnumerable<decimal?> cpIdCps)
        {
            string where = string.Format("CP_ID_CPS in ({0})", Conv.GetIn(cpIdCps));
            return Select(db, where);
        }

    }

}
