using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_NotaFiscal
    {

        public static List<NotaFiscal> SelectPorDespachos(Database db, List<Despacho> despachos)
        {
            var inStr = Conv.GetIn(despachos.Select(p => p.DpIdDp));
            string where = $"DP_ID_DP in ({inStr})";
            return Select(db, where);
        }

    }

}
