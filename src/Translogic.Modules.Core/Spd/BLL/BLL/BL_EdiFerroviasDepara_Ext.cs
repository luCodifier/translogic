using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_EdiFerroviasDepara
    {

        public static EdiFerroviasDepara ObterPorCodigoFluxoExternoOrdenado(string codigoFluxoExterno)
        {
            return Select(new EdiFerroviasDepara { CodigoFluxoExterno = codigoFluxoExterno })
                .OrderByDescending(p => p.DataMensagem).FirstOrDefault();
        }

        public static EdiFerroviasDepara ObterPorCodigoFluxoExternoOrdenado(Database dbT, string codigoFluxoExterno)
        {
            return Select(dbT, new EdiFerroviasDepara { CodigoFluxoExterno = codigoFluxoExterno })
                .OrderByDescending(p => p.DataMensagem).FirstOrDefault();
        }

    }

}
