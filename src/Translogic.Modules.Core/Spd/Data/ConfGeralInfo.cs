﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Translogic.Modules.Core.Spd.Data
{

    /// <summary>
    /// As configurações num formato mais amigável, com propriedades
    /// </summary>
    public class ConfGeralInfo
    {

        /// <summary>
        /// testiculo
        /// </summary>
        public string Aldolino2 { get; set; }
        /// <summary>
        /// Define se os e-mail devem ser apagdos depois de processados
        /// </summary>
        public string ApagarEmailNfe { get; set; }
        /// <summary>
        /// Flag para ativar e desativar apontamento automático via ATW
        /// </summary>
        public string ApontAutomaticoAtivo { get; set; }
        /// <summary>
        /// Data retroativa para buscar dados que ficaram para trás sem integração por qualquer motivo. Informar a data no formato DDMMYYYY
        /// </summary>
        public string AppaDataRetroativaDescarga { get; set; }
        /// <summary>
        /// Liga trava para atualizar TARA AUTOMATICAMENTE
        /// </summary>
        public string AtualizacaoAutoTara { get; set; }
        /// <summary>
        /// "Define o limite de registros pendentes que cada execucao do job responsavel pela atualizacao de PDF obtem do Banco de Dados. Nao utilizar um valor muito grande (VALOR: 100).Atualizado por Aldo para 150."
        /// </summary>
        public string AtualizacaoPdfNfeLimite { get; set; }
        /// <summary>
        /// Trava que indica se deve verificar a NF-e quando BO
        /// </summary>
        public string BoNfeVerificarSimconsultas { get; set; }
        /// <summary>
        /// Chave de acesso do webservice do simconsultas
        /// </summary>
        public string ChaveAcessoSimconsultas { get; set; }
        /// <summary>
        /// CHAVE PARA DETERMINAR O PERCURSO MINIMO ENTRE LAP E LCV
        /// </summary>
        public string ChaveTempoLapLcv { get; set; }
        /// <summary>
        /// CHAVE PARA DETERMINAR O PERCURSO MINIMO ENTRE ZAR E ZTO
        /// </summary>
        public string ChaveTempoZarZto { get; set; }
        /// <summary>
        /// Codigo da Frota de Locomotivas geral
        /// </summary>
        public string CodFrotaManobraGeral { get; set; }
        /// <summary>
        /// Codigo da Frota de Locomotivas para a manobra de Santos.
        /// </summary>
        public string CodFrotaManobraSantos { get; set; }
        /// <summary>
        /// Contingencia Planejamento de Descarga
        /// </summary>
        public string ComposicaoTravadaPlanejamentoDescarga { get; set; }
        /// <summary>
        /// Constante utilizada na tela OnTime 125 para cálculo de atraso de trens suprimidos, esta constante está em horas.
        /// </summary>
        public string ConstHoraOntime { get; set; }
        /// <summary>
        /// Constante utilizada no Indicador Headway para calcular pares de trem
        /// </summary>
        public string ConstanteHeadway { get; set; }
        /// <summary>
        /// Valor de mercadoria para contêineres vazios do tipo frigorifico
        /// </summary>
        public string ContainerVazioTipoFrigorifico { get; set; }
        /// <summary>
        /// Envia o cte de anulacao contribuinte para SEFAZ pela CONFIG
        /// </summary>
        public string CteAnulCtrEnviaSefaz { get; set; }
        /// <summary>
        /// S ativo envio e N inativo
        /// </summary>
        public string CteAtivaEnvioSeguradora { get; set; }
        /// <summary>
        /// Prestação serv. transp. p/ exec. serv. mesma nat.
        /// </summary>
        public string CteCfopCodTrafegoMutuoEstadual { get; set; }
        /// <summary>
        /// Prestação serv. transp. p/ exec. serv. mesma nat.
        /// </summary>
        public string CteCfopCodTrafegoMutuoInterestadual { get; set; }
        /// <summary>
        /// Prestação de Serviço de Transporte
        /// </summary>
        public string CteCfopCodTrafegoMutuoInternacional { get; set; }
        /// <summary>
        /// Código do grupo fiscal
        /// </summary>
        public string CteCodigoGrupoFiscal { get; set; }
        /// <summary>
        /// Utilizado esse valor default para os ctes que não tenham fluxo. Esse valor é utilizado nas tabelas da config
        /// </summary>
        public string CteCodigoInternoFilialDefault { get; set; }
        /// <summary>
        /// Quantidade maxima de dias para o cancelamento do CTE
        /// </summary>
        public string CteDataMaximaCancelamento { get; set; }
        /// <summary>
        /// Quantidade de dias da trava para cancelamento
        /// </summary>
        public string CteDiasCancelamentoCte { get; set; }
        /// <summary>
        /// Limite de dias para aparecer na tela de inutilização
        /// </summary>
        public string CteDiasInutilizacao { get; set; }
        /// <summary>
        /// Remetente dos arquivos de envio do CTE
        /// </summary>
        public string CteEmailRemetente { get; set; }
        /// <summary>
        /// email de envio cte para seguradora
        /// </summary>
        public string CteEmailSeguradoraBradesco { get; set; }
        /// <summary>
        /// Servidor de email para envio do CTE
        /// </summary>
        public string CteEmailServer { get; set; }
        /// <summary>
        /// senha para autenticar o usuario do email allcte@all-logistica.com
        /// </summary>
        public string CteEmailSmtpSenha { get; set; }
        /// <summary>
        /// Inserir aqui o código(AO_COD_AOP) das estações de origem e destino que a data utilizada para definir o contrato histórico para calculo do frete será a data do CTE (DATA_CTE) e não a data do despacho (DP_DT) para o Faturamento 2.0
        /// </summary>
        public string CteEstacoesFat20 { get; set; }
        /// <summary>
        /// Envia o CTE para verificacao de evento de desacordo
        /// </summary>
        public string CteEventoDesacordo { get; set; }
        /// <summary>
        /// Exibe a mensagem de PIS/COFINS
        /// </summary>
        public string CteExibirObsPisCofins { get; set; }
        /// <summary>
        /// 1 . Normal 2 - ContingÃªia
        /// </summary>
        public string CteFormaEmissao { get; set; }
        /// <summary>
        /// Servidor de FTP utilizado para gerar o arquivo pdf
        /// </summary>
        public string CteFtpHost { get; set; }
        /// <summary>
        /// Caminho dos arquivos
        /// </summary>
        public string CteFtpPath { get; set; }
        /// <summary>
        /// Senha do FTP
        /// </summary>
        public string CteFtpSenha { get; set; }
        /// <summary>
        /// Usuario de FTP
        /// </summary>
        public string CteFtpUsuario { get; set; }
        /// <summary>
        /// S Para habilitar a trava de cancelamento
        /// </summary>
        public string CteHabilitarTravaCancelamentoCte { get; set; }
        /// <summary>
        /// Trava que faz a validação da alteração do fluxo comercial na carta de correção eletrônica.
        /// </summary>
        public string CteHabilitarTravaValidacaoFluxoComercial { get; set; }
        /// <summary>
        /// S para habilitar a validação de substituição tributária em cte complementar
        /// </summary>
        public string CteHabilitarVerificacaoSubsTrib { get; set; }
        /// <summary>
        /// Quantidade de horas para cancelamento
        /// </summary>
        public string CteHorasCancelamentoCte { get; set; }
        /// <summary>
        /// Host utilizado pelo robo para reenviar o e-mail
        /// </summary>
        public string CteHostReenvioEmail { get; set; }
        /// <summary>
        /// S- Ativa geracao de Cte Substituto N-Desativado.
        /// </summary>
        public string CteIndGeracaoSubstituto { get; set; }
        /// <summary>
        /// Margem de variação de valor da nota de anulação permitida.(em reais)
        /// </summary>
        public string CteMargemLibAnulacao { get; set; }
        /// <summary>
        /// Número máximo de tentativas de processamento da nfe.
        /// </summary>
        public string CteMaxTentativas { get; set; }
        /// <summary>
        /// Quantidade máxima de threads a serem executadas simultaneamente
        /// </summary>
        public string CteMaxThreadSimconsulta { get; set; }
        /// <summary>
        /// Chave de acesso do webservice do simconsultas
        /// </summary>
        public string CteMesesRetroativosNf { get; set; }
        /// <summary>
        /// Caminhos dos arquivos PDF na rede
        /// </summary>
        public string CtePdfPath { get; set; }
        /// <summary>
        /// Quantidade de dias para efetuar a consulta
        /// </summary>
        public string CteQtdDiasSimconsulta { get; set; }
        /// <summary>
        /// email do remetente do cte enviado para seguradora Bradesco
        /// </summary>
        public string CteRemetenteEmailSeguradoraBradesco { get; set; }
        /// <summary>
        /// SEGURADORA BRADESCO
        /// </summary>
        public string CteSeguradoraEnvio { get; set; }
        /// <summary>
        /// Tempo em minutos de intervalo para reenvio da nfe.
        /// </summary>
        public string CteTempoProcessamento { get; set; }
        /// <summary>
        /// Tentativas de reenvio do Cte
        /// </summary>
        public string CteTentativaReenvio { get; set; }
        /// <summary>
        /// 1 - ProduÃ§ 2 - HomologaÃ§
        /// </summary>
        public string CteTipoAmbiente { get; set; }
        /// <summary>
        /// Usuario default do robo(CteRunner)
        /// </summary>
        public string CteUsuarioRobo { get; set; }
        /// <summary>
        /// INDICA SE Ã¿PARA VALIDAR O CFOP DO CONTRATO NO CARREGAMENTO
        /// </summary>
        public string CteVerificarCfop { get; set; }
        /// <summary>
        /// INDICA SE Ã¿PARA VALIDAR O CNPJ DO DESTINATARIO FISCAL NO CARREGAMENTO
        /// </summary>
        public string CteVerificarCnpjDestinatarioFiscal { get; set; }
        /// <summary>
        /// INDICA SE Ã¿PARA VALIDAR O CNPJ DO REMETENTE FISCAL NO CARREGAMENTO
        /// </summary>
        public string CteVerificarCnpjRementeFiscal { get; set; }
        /// <summary>
        /// Identificador da versÃ£do processo de emissÃ£(informar a versÃ£do aplicativo emissor de CT-e)
        /// </summary>
        public string CteVersaoProcesso { get; set; }
        /// <summary>
        /// Versão de envio dos dados para SEFAZ
        /// </summary>
        public string CteVersaoSefaz { get; set; }
        /// <summary>
        /// Caminhos dos arquivos XML na rede
        /// </summary>
        public string CteXmlPath { get; set; }
        /// <summary>
        /// Data que passa a considerar o desconto do redespacho e dos multiplos-despachos
        /// </summary>
        public string DataIniDescontaRedesp { get; set; }
        /// <summary>
        /// Data que passa a considerar o desconto do redespacho e dos multiplos-despachos para a Descarga
        /// </summary>
        public string DataIniDescontaRedespDescarga { get; set; }
        /// <summary>
        /// Valor utilizado pela funcao TO_DATEFROMJAVA para converter um date java(inteiro) em date oracle
        /// </summary>
        public string DateJava { get; set; }
        /// <summary>
        /// Desativar os usuarios que nao fazem login no sistema ha mais de X dias
        /// </summary>
        public string DesativarUsuarioXDias { get; set; }
        /// <summary>
        /// Maximo de dias para troca do pedido na ind_passagem_vagao
        /// </summary>
        public string DiasTrocaPedido { get; set; }
        /// <summary>
        /// Define se o job que realiza as descargas automaticas dos vag¿es serao liberadas para todos os clientes (VALOR: N) ou se utilizara o bloqueio parametrizado (VALOR: S).
        /// </summary>
        public string EdiDescargaUtilizaBloqueio { get; set; }
        /// <summary>
        /// Endereços de e-mails separados por ;(ponto e virgula) que vão receber a solicitação de cadastro de fluxo para a Nota que esta sendo salva nos Emails do EDI AdmNfe
        /// </summary>
        public string EdiEmailGestaoFluxo { get; set; }
        /// <summary>
        /// Remetente de notificação de envio para MRS
        /// </summary>
        public string EdiEmailRemetente { get; set; }
        /// <summary>
        /// Servidor de email para envio
        /// </summary>
        public string EdiEmailServer { get; set; }
        /// <summary>
        /// Envia os dados do EDI entre ferrovias apenas se houver CT-e autorizado. Quando for Direito de passagem (FCA->ALL->MRS) sempre envia as informaÃÄ±es
        /// </summary>
        public string EdiEnvioApenasCteAutorizado { get; set; }
        /// <summary>
        /// Id da empresa fca
        /// </summary>
        public string EdiIdEmpresaFca { get; set; }
        /// <summary>
        /// Id da empresa mrs
        /// </summary>
        public string EdiIdEmpresaMrs { get; set; }
        /// <summary>
        /// Id da empresa vli
        /// </summary>
        public string EdiIdEmpresaVli { get; set; }
        /// <summary>
        /// Intervalo de tempo para envio para mrs caso ocorra erro.
        /// </summary>
        public string EdiIntervaloProc { get; set; }
        /// <summary>
        /// Conta de email para receber as NFEs do Edi
        /// </summary>
        public string EdiNfeContaEmail { get; set; }
        /// <summary>
        /// Quantidade de dias retroativos. Utilizado para excluir os e-mails (antigos) recebidos ha mais de N dias retroativos e que estao nas demais pastas (conta admNfe).
        /// </summary>
        public string EdiNfeDiasRetroativosExclusaoOutras { get; set; }
        /// <summary>
        /// Quantidade de dias retroativos. Utilizado para excluir os e-mails (antigos) recebidos ha mais de N dias retroativos e que estao na pasta Processadas (conta admNfe).
        /// </summary>
        public string EdiNfeDiasRetroativosExclusaoProcessadas { get; set; }
        /// <summary>
        /// Pasta do Outlook que será feita a leitura para processar os Emails do EDI AdmNfe
        /// </summary>
        public string EdiNfePastaEmail { get; set; }
        /// <summary>
        /// Quantidade de e-mail que será lida por vez que o serviço for ate a caixa de e-mails de NFE do EDI
        /// </summary>
        public string EdiNfeQtdaEmail { get; set; }
        /// <summary>
        /// Senha da conta de email que recebe as NFEs do Edi
        /// </summary>
        public string EdiNfeSenhaEmail { get; set; }
        /// <summary>
        /// Número de tentativas de envio para mrs
        /// </summary>
        public string EdiNumTentativasEnvio { get; set; }
        /// <summary>
        /// E-mail de aviso para dos trens que listados na PKG_GARBAGE que possuem composição em aberto e não podem ser cancelados.
        /// </summary>
        public string EmailAvisoTrensCompPendCc { get; set; }
        /// <summary>
        /// E-mail de aviso para dos trens que listados na PKG_GARBAGE que possuem composição em aberto e não podem ser cancelados.
        /// </summary>
        public string EmailAvisoTrensCompPendDe { get; set; }
        /// <summary>
        /// E-mail de aviso para dos trens que listados na PKG_GARBAGE que possuem composição em aberto e não podem ser cancelados.
        /// </summary>
        public string EmailAvisoTrensCompPendPara { get; set; }
        /// <summary>
        /// E-mail do Grupo de Coordena¿¿o de Controle de Perdas
        /// </summary>
        public string EmailCoordenacaoControlePerdas { get; set; }
        /// <summary>
        /// Lista de e-mails que recebem o aviso de taras medianas
        /// </summary>
        public string EmailEnvioTarasMedianas { get; set; }
        /// <summary>
        /// Email dos responsaveis pelo campeonato maquinistas e supervisores
        /// </summary>
        public string EmailGerenciaDieselCampeonatos { get; set; }
        /// <summary>
        /// Lista de Emails para receber disponibilização de vagões projeto transformação
        /// </summary>
        public string EmailMudancaSituacaoMalhanorte { get; set; }
        /// <summary>
        /// Lista de Emails para receber disponibilização de vagões projeto transformação
        /// </summary>
        public string EmailMudancaSituacaoMalhasul { get; set; }
        /// <summary>
        /// Email para usar na sp SP_NOTIFICA_LIBERACAO_TREM
        /// </summary>
        public string EmailNotificaLibTrem { get; set; }
        /// <summary>
        /// E-mails da execu¿¿o para recebimento de notifica¿¿o de OS de Revistamento de Vag¿o.
        /// </summary>
        public string EmailNotificaRevistamentoExecucao { get; set; }
        /// <summary>
        /// E-mails da mec¿nica para recebimento de notifica¿¿o de OS de Revistamento de Vag¿o.
        /// </summary>
        public string EmailNotificaRevistamentoMecanica { get; set; }
        /// <summary>
        /// E-mail do remetente da mensagem de notificação de usuário e senha
        /// </summary>
        public string EmailSenhaUsuario { get; set; }
        /// <summary>
        /// Lista de email para receber toda vez que o vagao for voado
        /// </summary>
        public string EmailVagaoVoador { get; set; }
        /// <summary>
        /// Endereços de e-mail que receberão notificações de alteração de NFe
        /// </summary>
        public string EmailsAlteracaoNfe { get; set; }
        /// <summary>
        /// E-mails em que será enviado as planilhas da Copa Diesel
        /// </summary>
        public string EmailsPlanilhaDiesel { get; set; }
        /// <summary>
        /// Empresas que irão aparecer o valor do ICMS de substituição tributária na observação do CTE
        /// </summary>
        public string EmpresasIcmsSt { get; set; }
        /// <summary>
        /// Habilita envio de painel diesel por up
        /// </summary>
        public string Enviapaineldieselup { get; set; }
        /// <summary>
        /// Habilita o envio de paineis formação lotação Diesel & Tracao
        /// </summary>
        public string Enviapainelformacaolotacao { get; set; }
        /// <summary>
        /// Habilita o envio de paineis supervisores Diesel & Tracao
        /// </summary>
        public string Enviapainelsupervisoreslotacao { get; set; }
        /// <summary>
        /// Estações para anexar e desanexar automaticamente via ATW
        /// </summary>
        public string EstacoesAnxDnxAuto { get; set; }
        /// <summary>
        /// Chave para ligar ou desligar o faturamento automatico pela 363
        /// </summary>
        public string FatAutomatico363 { get; set; }
        /// <summary>
        /// Chave para nro de tentativas para faturar automatico
        /// </summary>
        public string FatAutomatico363Tentativas { get; set; }
        /// <summary>
        /// Flag para ativação do processo de faturamento operacional.
        /// </summary>
        public string Fat20AtivaFaturamentoOperacional { get; set; }
        /// <summary>
        /// Lista de estações onde será limitado o trânsito do faturamento de fluxo operacional (Separados por ;).
        /// </summary>
        public string Fat20EstacaoLimiteFaturamento { get; set; }
        /// <summary>
        /// Flag para ativar/desativar o recurso de validação do Travas da Engenharia no Translogic.
        /// </summary>
        public string FlagTravasEngenharia { get; set; }
        /// <summary>
        /// TELEFONES QUE RECEBEM SMS DE MONITORAMENTO DE POOLING
        /// </summary>
        public string FoneMonitorNfe { get; set; }
        /// <summary>
        /// VALIDAÇÃO DE FROTA CHARUTAO
        /// </summary>
        public string Frota { get; set; }
        /// <summary>
        /// Frotas de vagões da Ferropar que não é permitido anexar em trem
        /// </summary>
        public string FrotaNaoAnexa { get; set; }
        /// <summary>
        /// Ativa a geração de TFA (Termo de Falta ou Avaria) ao salvar o Laudo de indenizações
        /// </summary>
        public string GerarTfaLaudo { get; set; }
        /// <summary>
        /// Define o limite de dias para que a documentacao fique valida depois de criada. Depois deste tempo a documentacao sera excluida. (VALOR: 7).
        /// </summary>
        public string GerenciamentoDocumentacaoDiasValidade { get; set; }
        /// <summary>
        /// Porta do servidor de email para envio das documentacoes.
        /// </summary>
        public string GerenciamentoDocumentacaoEmailPorta { get; set; }
        /// <summary>
        /// Remetente dos arquivos de envio das documentacoes.
        /// </summary>
        public string GerenciamentoDocumentacaoEmailRemetente { get; set; }
        /// <summary>
        /// Servidor de email para envio das documentacoes.
        /// </summary>
        public string GerenciamentoDocumentacaoEmailServer { get; set; }
        /// <summary>
        /// Senha para autenticar o usuario do email de envio das documentacoes.
        /// </summary>
        public string GerenciamentoDocumentacaoEmailSmtpSenha { get; set; }
        /// <summary>
        /// Define a URL que da imagem do header do e-mail de documentacao.
        /// </summary>
        public string GerenciamentoDocumentacaoEmailUrlLogo { get; set; }
        /// <summary>
        /// Define a URL que o job de criacao da documentacao gerara os documentos.
        /// </summary>
        public string GerenciamentoDocumentacaoUrl { get; set; }
        /// <summary>
        /// Define o dominio do usuario que acessara a URL que o job de criacao da documentacao.
        /// </summary>
        public string GerenciamentoDocumentacaoUrlDominio { get; set; }
        /// <summary>
        /// Define a senha do usuario que acessara a URL que o job de criacao da documentacao.
        /// </summary>
        public string GerenciamentoDocumentacaoUrlSenha { get; set; }
        /// <summary>
        /// Define o usuario que acessara a URL que o job de criacao da documentacao.
        /// </summary>
        public string GerenciamentoDocumentacaoUrlUsuario { get; set; }
        /// <summary>
        /// Grava log de todas as chamadas para SP_INSERE_LOG
        /// </summary>
        public string GravaSpInsereLog { get; set; }
        /// <summary>
        /// Hora que devera executar o processo para recuperar os vagoes faturados a mais de X horas.
        /// </summary>
        public string HoraFotoFatXDias { get; set; }
        /// <summary>
        /// Hora que devera executar o processo para recuperar os vagoes parados a mais de 
        /// </summary>
        public string HoraFotoVgParado { get; set; }
        /// <summary>
        /// host das nfes que chegam no e-mail (office 365)
        /// </summary>
        public string HostEmailNfe { get; set; }
        /// <summary>
        /// Utilizar para fazer o envio de uma composição ja encerrada para o serviço de Planejamento de Entrega(Descarregamento), assim vai buscar nas tabelas de histórico.
        /// </summary>
        public string IdComposicaoRequestUnloadingWs { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string InformacoesRumo { get; set; }
        /// <summary>
        /// Define a quantidade de dias retrocendentes que a busca realizara. (VALOR: 7).
        /// </summary>
        public string IntegracaoNotaFaturamentoDiasRetrocedentes { get; set; }
        /// <summary>
        /// S = Operando normalmente. N = SAP desativado, permite operações de manutenção no TL sem a participação do SAP.
        /// </summary>
        public string IntegracaoSapLigada { get; set; }
        /// <summary>
        /// Quantidade de dias incrementados e decrementados á partir da data de Despacho para busca dos tickets do vagão
        /// </summary>
        public string IntervaloDiasTicketTsp { get; set; }
        /// <summary>
        /// LIBERA TRAVA PARA DIGITAÇÃO MANUAL QUANDO PASSA DE 180 NO CARREGAMENTO
        /// </summary>
        public string LiberacaoDt180CgtoErroV07 { get; set; }
        /// <summary>
        /// Limite para alterar horário da OS D
        /// </summary>
        public string LimiteHoraAlteracaoOsD { get; set; }
        /// <summary>
        /// Limite para alterar horário da OS D+N
        /// </summary>
        public string LimiteHoraAlteracaoOsD1 { get; set; }
        /// <summary>
        /// Limite maximo para um lote no patio de ZAR
        /// </summary>
        public string LimiteMaxLoteZar { get; set; }
        /// <summary>
        /// Limite minimo para um lote no patio de ZAR
        /// </summary>
        public string LimiteMinLoteZar { get; set; }
        /// <summary>
        /// Quantidade máxima de dias de log que a tabela LOG_ERRO conter.
        /// </summary>
        public string LimpezaLogErro { get; set; }
        /// <summary>
        /// Quantidade máxima de dias de log que a tabela LOG_ERRO_TL conter.
        /// </summary>
        public string LimpezaLogErroTl { get; set; }
        /// <summary>
        /// Quantidade máxima de dias de log que a tabela MODIF_VAGAO conter.
        /// </summary>
        public string LimpezaModifVagao { get; set; }
        /// <summary>
        /// Quantidade máxima de dias de log que a tabela TIPO_BITOLA_LOG conter.
        /// </summary>
        public string LimpezaTipoBitolaLog { get; set; }
        /// <summary>
        /// Trava para ligar ou desligar envio de email para MDFe
        /// </summary>
        public string ListaEmailMdfe { get; set; }
        /// <summary>
        /// TRAVA PARA LIGAR ENVIO DE EMAIL TRENS COOPERSUCAR
        /// </summary>
        public string ListaEmailMdfeCooper { get; set; }
        /// <summary>
        /// Caminhos dos arquivos PDF na rede
        /// </summary>
        public string MdfePdfPath { get; set; }
        /// <summary>
        /// Configuração em segundos para enviar o mdfe para a sefaz
        /// </summary>
        public string MdfeTimer { get; set; }
        /// <summary>
        /// Usuario default do robo (MdfeRunner)
        /// </summary>
        public string MdfeUsuarioRobo { get; set; }
        /// <summary>
        /// QUANTIDADE DE NFES PARA O MONITOR DISPARAR AVISO
        /// </summary>
        public string MonitorNfeMaxItensFila { get; set; }
        /// <summary>
        /// TEMPO QUE O MONITOR DEVE CONSIDERAR PARA DISPARAR AVISO
        /// </summary>
        public string MonitorTempoNfeFila { get; set; }
        /// <summary>
        /// Flag para permitir o faturamento DoubleStack com apenas 1 conteiner de 40
        /// </summary>
        public string MultiploDpDoblestack { get; set; }
        /// <summary>
        /// TRAVA QUE FAZ A VERIFICAÇÃO DE SALDO DA NFE
        /// </summary>
        public string NfeTravaSaldo { get; set; }
        /// <summary>
        /// Porcentagem para margem de balanço de NF-e. Obs: Para porcetagem não exata utilizar vírgula
        /// </summary>
        public string NfeTravaSaldoPorcentagem { get; set; }
        /// <summary>
        /// Habilita validação da somatório da nota com o dcl na pkg
        /// </summary>
        public string NfeTravarSomatoriaSaldo { get; set; }
        /// <summary>
        /// Número de tentativas que devem ser efetuadas antes de considerar um LOCO como inválida.
        /// </summary>
        public string NumTentativasCtaplus { get; set; }
        /// <summary>
        /// Número de Ctes que serão enviados para inutilização através do Job CteInutilizacaoConfigJob
        /// </summary>
        public string NumeroCtesParaInutilizacao { get; set; }
        /// <summary>
        /// Permitir/Impedir que seja formada/atualizada uma composição onde existam veículos com bitolas diferentes. -- TESTE TMJ Spring boot
        /// </summary>
        public string ObrigatorioVagaoMadrinha { get; set; }
        /// <summary>
        /// Último horário de corte da oferta de carga/descarga. Compreende das 20hrs até as 23hrs. Chave usada nas PACKAGES: PKG_CONSOLIDA_OFERTA_DESCARGA PKG_CONSOLIDA_OFERTA_CARGA
        /// </summary>
        public string OfertaFinalHorario { get; set; }
        /// <summary>
        /// Primeiro horário de corte da oferta de carga/descarga. Compreende das 00 hrs até as 07hrs. Chave usada nas PACKAGESPKG_CONSOLIDA_OFERTA_DESCARGA PKG_CONSOLIDA_OFERTA_CARGA
        /// </summary>
        public string OfertaPrimeiroHorario { get; set; }
        /// <summary>
        /// Quarto horário de corte da oferta de carga/descarga. Compreende das 17hrs até as 19hrs. Chave usada nas PACKAGES:PKG_CONSOLIDA_OFERTA_DESCARGA PKG_CONSOLIDA_OFERTA_CARGA
        /// </summary>
        public string OfertaQuartoHorario { get; set; }
        /// <summary>
        /// Segundo horário de corte da oferta de carga/descarga. Compreende das 08 hrs até as 12hrs. Chave usada nas PACKAGES: PKG_CONSOLIDA_OFERTA_DESCARGA PKG_CONSOLIDA_OFERTA_CARGA
        /// </summary>
        public string OfertaSegundoHorario { get; set; }
        /// <summary>
        /// Terceiro horário de corte da oferta de carga/descarga. Compreende das 13hrs até as 16hrs. Chave usada nas PACKAGES:PKG_CONSOLIDA_OFERTA_DESCARGA PKG_CONSOLIDA_OFERTA_CARGA
        /// </summary>
        public string OfertaTerceiroHorario { get; set; }
        /// <summary>
        /// Intervalo de horas a serem mostradas no painel
        /// </summary>
        public string PainelBacklogVazios { get; set; }
        /// <summary>
        /// Tempo em minutos para atualizacao/reload automatico da tela de painel de expedicao
        /// </summary>
        public string PainelExpedicaoRefreshTela { get; set; }
        /// <summary>
        /// Pasta que salva as planilhas de pesagens dos vagoes
        /// </summary>
        public string PastaInputPesagens { get; set; }
        /// <summary>
        /// Percentual para diferença de peso de vagoes da LARGA na composição
        /// </summary>
        public string PercentualPesoVagoesLarga { get; set; }
        /// <summary>
        /// Percentual para diferença de peso de vagoes da métrica na composição
        /// </summary>
        public string PercentualPesoVagoesMetrica { get; set; }
        /// <summary>
        /// Percentual para diferença de peso de vagoes TANQUE na composição
        /// </summary>
        public string PercentualPesoVagoesTanque { get; set; }
        /// <summary>
        /// Percentual de rateio utilizado na cria¿¿o de processo de seguro pelo aplicativo mobile
        /// </summary>
        public string PercentualRateioProcessoSeguro { get; set; }
        /// <summary>
        /// Trava para permitir ajuste retroativos de TU
        /// </summary>
        public string PermiteAjusteMesAnterior { get; set; }
        /// <summary>
        /// Peso do Conteiner de 20 Peso em TON
        /// </summary>
        public string PesoCont20 { get; set; }
        /// <summary>
        /// Peso do Conteiner de 40 Peso em TON
        /// </summary>
        public string PesoCont40 { get; set; }
        /// <summary>
        /// porta das nfes que chegam no e-mail (office 365)
        /// </summary>
        public string PortaEmailNfe { get; set; }
        /// <summary>
        /// Se habilita precificação por origem. S=habilitado; N=desabilitado
        /// </summary>
        public string PrecificacaoPorOrigem { get; set; }
        /// <summary>
        /// Quantidade critica para reprocessar nfe no pooling
        /// </summary>
        public string QtdCriticaRepNfe { get; set; }
        /// <summary>
        /// Quantidade de horas que define se o vagao faturado vai ou nao para a lista de controle.
        /// </summary>
        public string QtdHorasFatXDias { get; set; }
        /// <summary>
        /// Quantidade de horas que define se o vagao parado vai ou nao para a lista de controle.
        /// </summary>
        public string QtdHorasVgParado { get; set; }
        /// <summary>
        /// Host do servidor de Restri¿¿o, caso o mesmo mude dev ser alterado aqui
        /// </summary>
        public string RestricaoHost { get; set; }
        /// <summary>
        /// Somente as telas que ser¿o acessadas dentro do ambiente do servidor de Restri¿¿o
        /// </summary>
        public string RestricaoTelas { get; set; }
        /// <summary>
        /// Trava para ligar ou desligar a retencao automatica por motivo MECANICA
        /// </summary>
        public string RetencaoAutPorMotivo { get; set; }
        /// <summary>
        /// senha das nfes que chegam no e-mail (office 365)
        /// </summary>
        public string SenhaEmailNfe { get; set; }
        /// <summary>
        /// Tempo em dias para expirar senha do TL por inatividade
        /// </summary>
        public string SenhaTlfExpiraDiasInativo { get; set; }
        /// <summary>
        /// Tempo máximo de expiração de senha do Translogic
        /// </summary>
        public string SenhaTlfExpiraEmXDias { get; set; }
        /// <summary>
        /// Siglas que representam unidade de medida em toneladas
        /// </summary>
        public string SiglasUnidadeTonelada { get; set; }
        /// <summary>
        /// Qual a mensagem que deve mostrar quando o usuário logar
        /// </summary>
        public string SistemaAvisoAtivo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SistemasAtivos { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SistemasTestes { get; set; }
        /// <summary>
        /// Host do servidor SIV, caso o mesmo mude, deve ser alterado aqui
        /// </summary>
        public string SivHost { get; set; }
        /// <summary>
        /// Permite (S)im / Impede (N)ão a Abertura de nota no SAP para Vagões TAG
        /// </summary>
        public string TagAbrirNotaSap { get; set; }
        /// <summary>
        /// TESTE
        /// </summary>
        public string TesteLigaDesliga { get; set; }
        /// <summary>
        /// teste3
        /// </summary>
        public string TesteLogErro { get; set; }
        /// <summary>
        /// Valor em milissegundos para AUTO consulta de LIBERAÇÃO DE FORMAÇÃO DE TREM.
        /// </summary>
        public string TimerAutoConsultaLiberacaoFormacaoTrem { get; set; }
        /// <summary>
        /// Qtde dias que um trem pode ficar em formação antes que a interface PKG_GARBAGE cancele-o.
        /// </summary>
        public string TmpExclTrensFormacao { get; set; }
        /// <summary>
        /// TEMPO PARA CONSIDERADO PARA EXCLUIR OSs NO FUTURO
        /// </summary>
        public string TmpSuprOsFutura { get; set; }
        /// <summary>
        /// Qtde dias que uma O.S. fique sem registro na tabela trem antes que a interface PKG_GARBAGE cancele-a.
        /// </summary>
        public string TmpSuprOsSemTrem { get; set; }
        /// <summary>
        /// TEMPO EM HORAS PARA ENCERRAR TREM QUE LIBERADO Q NAO SE MOVIMENTOU
        /// </summary>
        public string TmpTremCSemMovimentacao { get; set; }
        /// <summary>
        /// TEMPO PARA EXCLUIR TREM EM FORMACAO COM COMPOSICAO
        /// </summary>
        public string TmpTremPComComp { get; set; }
        /// <summary>
        /// TEMPO PARA EXCLUIR TREM EM FORMACAO SEM COMPOSICAO
        /// </summary>
        public string TmpTremPSemComp { get; set; }
        /// <summary>
        /// TEMPO EM HORAS PARA CANCELAR TREM EM FORMACAO SEM COMPOSICAO ABERTA
        /// </summary>
        public string TmpTremPSemComposicao { get; set; }
        /// <summary>
        /// TEMPO EM HORAS PARA ENCERRAR TREM QUE NAO FOI LIBERADO
        /// </summary>
        public string TmpTremPSemMovimentacao { get; set; }
        /// <summary>
        /// TEMPO PARA EXCLUIR TREM PARADO COM COMPOSICAO
        /// </summary>
        public string TmpTremRComComp { get; set; }
        /// <summary>
        /// Token de acesso ao Barramento Corporativo da RUMO.
        /// </summary>
        public string TokenBarramentoCorporativo { get; set; }
        /// <summary>
        /// TOLERANCIA_ENTRE_ABASTECIMENTOS EM MINUTOS
        /// </summary>
        public string ToleranciaEntreAbastecimentos { get; set; }
        /// <summary>
        /// Tolerancia em toneladas para limite mínimo de peso do vagão.
        /// </summary>
        public string ToleranciaPesoMinimoVagaoFaturamento { get; set; }
        /// <summary>
        /// Tolerancia em toneladas para limite de peso do vagão por via e manga.
        /// </summary>
        public string ToleranciaPesoVagaoFaturamento { get; set; }
        /// <summary>
        /// Essas siglas indicam que o peso líquido e peso bruto devem ser obtidos da configuração de trasnporte do xml da Nfe
        /// </summary>
        public string TransporteUnidadeComercial { get; set; }
        /// <summary>
        /// Chave para ligar ou desligar a trava de alteração de OS
        /// </summary>
        public string TravaAlteracaoOsLigada { get; set; }
        /// <summary>
        /// LISTA DE IDS DE FROTAS Q NAO PODEM ENTRAR NO TREM COM DESTINO NA CHAVE TRAVA_ANX_VG_SERRA_LOCAIS
        /// </summary>
        public string TravaAnxVgSerraFrotas { get; set; }
        /// <summary>
        /// LIGA A TRAVA DE ANX DE VAGÕES DA FROTA 354 EM TRENS
        /// </summary>
        public string TravaAnxVgSerraLigada { get; set; }
        /// <summary>
        /// LISTA DE IDS DE ESTACOES QUE SERAO OS DESTINOS DE TRENS QUE OS VAGOES DA FROTA 354 NAO PODEM ENTRAR
        /// </summary>
        public string TravaAnxVgSerraLocais { get; set; }
        /// <summary>
        /// Se estiver ligada fara a consistencia de consumo ate proximo PA
        /// </summary>
        public string TravaConsumo { get; set; }
        /// <summary>
        /// Travar a criação de OS em X horas.Estava em 6h foi retirado por solicitação do CT.
        /// </summary>
        public string TravaCriacaoOsHoras { get; set; }
        /// <summary>
        /// S = Aplica as travas de CTE. N = NÃ£aplica as travas de CTE.  (Voo de vagÃ£334 e formaÃ§ de trem 300)
        /// </summary>
        public string TravaCte { get; set; }
        /// <summary>
        /// Data limite para realizar descarga do dia anterior. Usar como 2/24 para considerar 2:00 do dia.
        /// </summary>
        public string TravaDescargaRetroativa { get; set; }
        /// <summary>
        /// Indicador de trava de diferenca de peso CCP para liberacao do trem
        /// </summary>
        public string TravaDiferencaPesoComposicao { get; set; }
        /// <summary>
        /// Trava para ligar ou desligar envio de email para MDFe
        /// </summary>
        public string TravaEmailParadaZev { get; set; }
        /// <summary>
        /// Trava para ligar ou desligar envio de email do vagao voado
        /// </summary>
        public string TravaEmailVoador { get; set; }
        /// <summary>
        /// Trava de Fluxo espelho
        /// </summary>
        public string TravaFaturamentoFluxoEspelho { get; set; }
        /// <summary>
        /// Data limite para realizar o despacho
        /// </summary>
        public string TravaHorarioDespacho { get; set; }
        /// <summary>
        /// TRAVA PARA MANUTENCAO DE ABASTECIMENTO RETROATIVO
        /// </summary>
        public string TravaManutAbastecimento { get; set; }
        /// <summary>
        /// Habilitar trava para limitar mudança de margem para 1 vez por vagão.
        /// </summary>
        public string TravaMudancaMargemVagao { get; set; }
        /// <summary>
        /// Indica se deve ser verificado o limite de peso por manga e via.
        /// </summary>
        public string TravaPesoVagaoFaturamento { get; set; }
        /// <summary>
        /// Trava de retenção retroativa
        /// </summary>
        public string TravaRetencao { get; set; }
        /// <summary>
        /// Trava para nao permitir troca de evento de vagao sem motivo
        /// </summary>
        public string TravaSituacaoMotivo { get; set; }
        /// <summary>
        /// Trava (percentual) de tempo minimo de apontamento de percurso na movimentacao_trem para o ACT
        /// </summary>
        public string TravaTremBalaAct { get; set; }
        /// <summary>
        /// Trava (percentual) de tempo minimo de apontamento de percurso na movimentacao_trem para o Translogic
        /// </summary>
        public string TravaTremBalaTl { get; set; }
        /// <summary>
        /// Se habilitado permite a aprovação de manutenção para o mes anterior ate o meio dia do dia primeiro
        /// </summary>
        public string Travaaprovacaomanutencaoabastecimento { get; set; }
        /// <summary>
        /// Travar a validação dos EDI nos despachos
        /// </summary>
        public string TravarEstacaoEdiGeral { get; set; }
        /// <summary>
        /// Travar a liberacao do trem quando a tbc do trem for maior que a tbc do trecho
        /// </summary>
        public string TravarTbcTrechoTrem { get; set; }
        /// <summary>
        /// Indica se deve ser verificado o limite mínimo de peso.
        /// </summary>
        public string TravarVagaoAbaixoPesoFaturamento { get; set; }
        /// <summary>
        /// URL do Barramento Corporativo da RUMO.
        /// </summary>
        public string UrlBarramentoCorporativo { get; set; }
        /// <summary>
        /// Url para o consumo do servico de notas da BR
        /// </summary>
        public string UrlServicoNotasBr { get; set; }
        /// <summary>
        /// usuário das nfes que chegam no e-mail (office 365)
        /// </summary>
        public string UsuarioEmailNfe { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UsuariosAtivos { get; set; }
        /// <summary>
        /// S- valida o fluxo e tomador da nota de anulação. N-não valida
        /// </summary>
        public string ValidarFluxoTomadorAnulacao { get; set; }
        /// <summary>
        /// Trava para verificar grupo do usuario q esta retendo o vagao. Usado na PKG_EVENTO
        /// </summary>
        public string VerificaGrupoRetAvariado { get; set; }


    }

}
