using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data.Enums;

namespace Translogic.Modules.Core.Spd.Data
{

    public partial class SisLog
    {
        public EnumSistema Sistema
        {
            get
            {
                return (EnumSistema)SistemaId.GetValueOrDefault();
            }
            set
            {
                SistemaId = (decimal)value;
            }
        }

        public EnumLogTipo Tipo
        {
            get
            {
                return (EnumLogTipo)TipoId.GetValueOrDefault();
            }
            set
            {
                TipoId = (short)value;
            }
        }

    }
}
