using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;

namespace Translogic.Modules.Core.Spd.Data
{

    public partial class VwPedraRealizado
    {
        /// <summary>
        /// Pedra atual, caso n�o venha do SADE pega a editada pelo usuario se tiver
        /// </summary>
        public decimal Pedra
        {
            get
            {
                return PedraEditado != null && PedraEditado.Value > 0 ? PedraEditado.Value : PedraSade.Value;
            }
        }

        /// <summary>
        /// Saldo Calculado
        /// </summary>
        public decimal Saldo
        {
            get
            {
                return Realizado.Value - Pedra;
            }
        }

        /// <summary>
        /// Mostra a porcentagem do que foi realizado, levando em considera��o se for zero 
        /// a pedra ou o realizado ir� exibir zero para n�o dar DIVIDE BY ZERO EXCEPTION
        /// </summary>
        public decimal Porcentagem
        {
            get
            {
                if (Realizado.Value > 0 && Pedra > 0)
                    return Math.Round((Realizado.Value / (PedraSade.Value == 0 ? PedraEditado.Value : PedraSade.Value)) * 100);
                else
                    return 0;
            }
        }

        /// <summary>
        /// Verifica se a Pedra foi Editada, mostrando uma cor diferente na tela do usu�rio
        /// </summary>
        public bool PedraEditadaFlag
        {
            get
            {
                return PedraEditado > 0 && ((PedraSade != null ? PedraSade.Value : 0) != (PedraEditado != null ? PedraEditado.Value : 0));
            }
        }

        public bool PedraZerada
        {
            get
            {
                return Realizado == 0 && ((PedraSade != null ? PedraSade.Value : 0) == 0 && (PedraEditado != null ? PedraEditado.Value : 0) == 0);
            }
        }
    }

}
