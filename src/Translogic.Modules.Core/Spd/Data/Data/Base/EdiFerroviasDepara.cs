// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "EDI_FERROVIAS_DEPARA", "", "")]
    [Serializable]
    [DataContract(Name = "EdiFerroviasDepara", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class EdiFerroviasDepara : Record, ICloneable, INotifyPropertyChanged
    {

        private Decimal? z_Id;
        [DbColumn("ID_EF_DP")]
        [DataMember]
        
        public Decimal? Id
        {
            get { return z_Id; }
            set
            {
                if (z_Id != value)
                {
                    z_Id = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }

        private DateTime? z_DataMensagem;
        [DbColumn("TS_MSG")]
        [DataMember]
        
        public DateTime? DataMensagem
        {
            get { return z_DataMensagem; }
            set
            {
                if (z_DataMensagem != value)
                {
                    z_DataMensagem = value;
                    this.RaisePropertyChanged("DataMensagem");
                }
            }
        }

        private String z_CodigoFluxoExterno;
        [DbColumn("COD_EXT")]
        [DataMember]
        
        public String CodigoFluxoExterno
        {
            get { return z_CodigoFluxoExterno; }
            set
            {
                if (z_CodigoFluxoExterno != value)
                {
                    z_CodigoFluxoExterno = value;
                    this.RaisePropertyChanged("CodigoFluxoExterno");
                }
            }
        }

        private String z_CodigoFluxoInterno;
        [DbColumn("COD_INT")]
        [DataMember]
        
        public String CodigoFluxoInterno
        {
            get { return z_CodigoFluxoInterno; }
            set
            {
                if (z_CodigoFluxoInterno != value)
                {
                    z_CodigoFluxoInterno = value;
                    this.RaisePropertyChanged("CodigoFluxoInterno");
                }
            }
        }

        private String z_Tipo;
        [DbColumn("TIPO")]
        [DataMember]
        
        public String Tipo
        {
            get { return z_Tipo; }
            set
            {
                if (z_Tipo != value)
                {
                    z_Tipo = value;
                    this.RaisePropertyChanged("Tipo");
                }
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(EdiFerroviasDepara value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.DataMensagem == value.DataMensagem &&
				this.CodigoFluxoExterno == value.CodigoFluxoExterno &&
				this.CodigoFluxoInterno == value.CodigoFluxoInterno &&
				this.Tipo == value.Tipo;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public EdiFerroviasDepara CloneT()
        {
            EdiFerroviasDepara value = new EdiFerroviasDepara();
            value.RecordStatus = this.RecordStatus;
            value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.DataMensagem = this.DataMensagem;
			value.CodigoFluxoExterno = this.CodigoFluxoExterno;
			value.CodigoFluxoInterno = this.CodigoFluxoInterno;
			value.Tipo = this.Tipo;

            return value;
        }

        #endregion Clone

        #region Create

        public static EdiFerroviasDepara Create(Decimal _Id, DateTime? _DataMensagem, String _CodigoFluxoExterno, String _CodigoFluxoInterno, String _Tipo)
        {
            EdiFerroviasDepara __value = new EdiFerroviasDepara();

			__value.Id = _Id;
			__value.DataMensagem = _DataMensagem;
			__value.CodigoFluxoExterno = _CodigoFluxoExterno;
			__value.CodigoFluxoInterno = _CodigoFluxoInterno;
			__value.Tipo = _Tipo;

            return __value;
        }

        #endregion Create

   }

}
