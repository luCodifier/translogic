// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "BO_BOLAR_IMP_CFG_FILIAL", "ID_BO_BOLAR_IMP_CFG_FILIAL", "BO_BOLAR_IMP_CFG_FILIAL_SEQ")]
    [Serializable]
    [DataContract(Name = "BoBolarImpCfgFilial", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class BoBolarImpCfgFilial : Record, ICloneable, INotifyPropertyChanged
    {

        private Decimal? z_Id;
        [DbColumn("ID_BO_BOLAR_IMP_CFG_FILIAL")]
        [DataMember]
        
        public Decimal? Id
        {
            get { return z_Id; }
            set
            {
                if (z_Id != value)
                {
                    z_Id = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }

        private Decimal? z_IdBolarBoImpCfg;
        [DbColumn("ID_BO_BOLAR_IMP_CFG")]
        [DataMember]
        
        public Decimal? IdBolarBoImpCfg
        {
            get { return z_IdBolarBoImpCfg; }
            set
            {
                if (z_IdBolarBoImpCfg != value)
                {
                    z_IdBolarBoImpCfg = value;
                    this.RaisePropertyChanged("IdBolarBoImpCfg");
                }
            }
        }

        private String z_Cnpj;
        [DbColumn("CNPJ")]
        [DataMember]
        
        public String Cnpj
        {
            get { return z_Cnpj; }
            set
            {
                if (z_Cnpj != value)
                {
                    z_Cnpj = value;
                    this.RaisePropertyChanged("Cnpj");
                }
            }
        }

        private DateTime? z_Timestamp;
        [DbColumn("TIMESTAMP")]
        [DataMember]
        
        public DateTime? Timestamp
        {
            get { return z_Timestamp; }
            set
            {
                if (z_Timestamp != value)
                {
                    z_Timestamp = value;
                    this.RaisePropertyChanged("Timestamp");
                }
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(BoBolarImpCfgFilial value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.IdBolarBoImpCfg == value.IdBolarBoImpCfg &&
				this.Cnpj == value.Cnpj &&
				this.Timestamp == value.Timestamp;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public BoBolarImpCfgFilial CloneT()
        {
            BoBolarImpCfgFilial value = new BoBolarImpCfgFilial();
            value.RecordStatus = this.RecordStatus;
            value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.IdBolarBoImpCfg = this.IdBolarBoImpCfg;
			value.Cnpj = this.Cnpj;
			value.Timestamp = this.Timestamp;

            return value;
        }

        #endregion Clone

        #region Create

        public static BoBolarImpCfgFilial Create(Decimal? _Id, Decimal? _IdBolarBoImpCfg, String _Cnpj, DateTime? _Timestamp)
        {
            BoBolarImpCfgFilial __value = new BoBolarImpCfgFilial();

			__value.Id = _Id;
			__value.IdBolarBoImpCfg = _IdBolarBoImpCfg;
			__value.Cnpj = _Cnpj;
			__value.Timestamp = _Timestamp;

            return __value;
        }

        #endregion Create

   }

}
