// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "PE_OPERACAO", "ID", "PE_OPERACAO_SEQ_ID")]
    [Serializable]
    [DataContract(Name = "PeOperacao", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PeOperacao : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ID
        /// </summary>
        [DbColumn("ID")]
        
        public Decimal? Id { get; set; }

        /// <summary>
        /// Column NOME
        /// </summary>
        [DbColumn("NOME")]
        
        public String Nome { get; set; }

        /// <summary>
        /// Column DATA_CADASTRO
        /// </summary>
        [DbColumn("DATA_CADASTRO")]
        
        public DateTime? DataCadastro { get; set; }

        /// <summary>
        /// Column USUARIO_CADASTRO
        /// </summary>
        [DbColumn("USUARIO_CADASTRO")]
        
        public Decimal? UsuarioCadastro { get; set; }

        /// <summary>
        /// Column DATA_ALTERACAO
        /// </summary>
        [DbColumn("DATA_ALTERACAO")]
        
        public DateTime? DataAlteracao { get; set; }

        /// <summary>
        /// Column USUARIO_ALTERACAO
        /// </summary>
        [DbColumn("USUARIO_ALTERACAO")]
        
        public Decimal? UsuarioAlteracao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PeOperacao value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.Nome == value.Nome &&
				this.DataCadastro == value.DataCadastro &&
				this.UsuarioCadastro == value.UsuarioCadastro &&
				this.DataAlteracao == value.DataAlteracao &&
				this.UsuarioAlteracao == value.UsuarioAlteracao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PeOperacao CloneT()
        {
            PeOperacao value = new PeOperacao();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.Nome = this.Nome;
			value.DataCadastro = this.DataCadastro;
			value.UsuarioCadastro = this.UsuarioCadastro;
			value.DataAlteracao = this.DataAlteracao;
			value.UsuarioAlteracao = this.UsuarioAlteracao;

            return value;
        }

        #endregion Clone

        #region Create

        public static PeOperacao Create(Decimal _Id, String _Nome, DateTime _DataCadastro, Decimal _UsuarioCadastro, DateTime? _DataAlteracao, Decimal? _UsuarioAlteracao)
        {
            PeOperacao __value = new PeOperacao();

			__value.Id = _Id;
			__value.Nome = _Nome;
			__value.DataCadastro = _DataCadastro;
			__value.UsuarioCadastro = _UsuarioCadastro;
			__value.DataAlteracao = _DataAlteracao;
			__value.UsuarioAlteracao = _UsuarioAlteracao;

            return __value;
        }

        #endregion Create

   }

}
