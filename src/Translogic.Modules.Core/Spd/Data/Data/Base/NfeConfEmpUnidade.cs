// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "NFE_CONF_EMP_UNIDADE", "ID_NFE_CONF_EMP_UNID", "NFE_CONF_EMP_UNIDADE_SEQ_ID")]
    [Serializable]
    [DataContract(Name = "NfeConfEmpUnidade", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class NfeConfEmpUnidade : Record, ICloneable, INotifyPropertyChanged
    {

        private Decimal? z_Id;
        [DbColumn("ID_NFE_CONF_EMP_UNID")]
        [DataMember]
        
        public Decimal? Id
        {
            get { return z_Id; }
            set
            {
                if (z_Id != value)
                {
                    z_Id = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }

        private String z_Cnpj;
        [DbColumn("CNPJ_EMPRESA")]
        [DataMember]
        
        public String Cnpj
        {
            get { return z_Cnpj; }
            set
            {
                if (z_Cnpj != value)
                {
                    z_Cnpj = value;
                    this.RaisePropertyChanged("Cnpj");
                }
            }
        }

        private String z_UnidadeMedida;
        [DbColumn("UNID_MEDIDA")]
        [DataMember]
        
        public String UnidadeMedida
        {
            get { return z_UnidadeMedida; }
            set
            {
                if (z_UnidadeMedida != value)
                {
                    z_UnidadeMedida = value;
                    this.RaisePropertyChanged("UnidadeMedida");
                }
            }
        }

        private DateTime? z_DataCadastro;
        [DbColumn("TIMESTAMP")]
        [DataMember]
        
        public DateTime? DataCadastro
        {
            get { return z_DataCadastro; }
            set
            {
                if (z_DataCadastro != value)
                {
                    z_DataCadastro = value;
                    this.RaisePropertyChanged("DataCadastro");
                }
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(NfeConfEmpUnidade value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.Cnpj == value.Cnpj &&
				this.UnidadeMedida == value.UnidadeMedida &&
				this.DataCadastro == value.DataCadastro;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public NfeConfEmpUnidade CloneT()
        {
            NfeConfEmpUnidade value = new NfeConfEmpUnidade();
            value.RecordStatus = this.RecordStatus;
            value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.Cnpj = this.Cnpj;
			value.UnidadeMedida = this.UnidadeMedida;
			value.DataCadastro = this.DataCadastro;

            return value;
        }

        #endregion Clone

        #region Create

        public static NfeConfEmpUnidade Create(Decimal _Id, String _Cnpj, String _UnidadeMedida, DateTime? _DataCadastro)
        {
            NfeConfEmpUnidade __value = new NfeConfEmpUnidade();

			__value.Id = _Id;
			__value.Cnpj = _Cnpj;
			__value.UnidadeMedida = _UnidadeMedida;
			__value.DataCadastro = _DataCadastro;

            return __value;
        }

        #endregion Create

   }

}
