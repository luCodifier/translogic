// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;

using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "UNID_PRODUCAO", "", "")]
    [Serializable]
    [DataContract(Name = "UnidProducao", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class UnidProducao : Record, ICloneable, INotifyPropertyChanged
    {

        private Decimal? z_UpIdUnp;
        [DbColumn("UP_ID_UNP")]
        [DataMember]
        
        public Decimal? UpIdUnp
        {
            get { return z_UpIdUnp; }
            set
            {
                if (z_UpIdUnp != value)
                {
                    z_UpIdUnp = value;
                    this.RaisePropertyChanged("UpIdUnp");
                }
            }
        }

        private Decimal? z_EpIdEmp;
        [DbColumn("EP_ID_EMP")]
        [DataMember]
        
        public Decimal? EpIdEmp
        {
            get { return z_EpIdEmp; }
            set
            {
                if (z_EpIdEmp != value)
                {
                    z_EpIdEmp = value;
                    this.RaisePropertyChanged("EpIdEmp");
                }
            }
        }

        private String z_UpCodUnd;
        [DbColumn("UP_COD_UND")]
        [DataMember]
        
        public String UpCodUnd
        {
            get { return z_UpCodUnd; }
            set
            {
                if (z_UpCodUnd != value)
                {
                    z_UpCodUnd = value;
                    this.RaisePropertyChanged("UpCodUnd");
                }
            }
        }

        private String z_UpDscRsm;
        [DbColumn("UP_DSC_RSM")]
        [DataMember]
        
        public String UpDscRsm
        {
            get { return z_UpDscRsm; }
            set
            {
                if (z_UpDscRsm != value)
                {
                    z_UpDscRsm = value;
                    this.RaisePropertyChanged("UpDscRsm");
                }
            }
        }

        private DateTime? z_UpTimestamp;
        [DbColumn("UP_TIMESTAMP")]
        [DataMember]
        
        public DateTime? UpTimestamp
        {
            get { return z_UpTimestamp; }
            set
            {
                if (z_UpTimestamp != value)
                {
                    z_UpTimestamp = value;
                    this.RaisePropertyChanged("UpTimestamp");
                }
            }
        }

        private String z_UpDscDtl;
        [DbColumn("UP_DSC_DTL")]
        [DataMember]
        
        public String UpDscDtl
        {
            get { return z_UpDscDtl; }
            set
            {
                if (z_UpDscDtl != value)
                {
                    z_UpDscDtl = value;
                    this.RaisePropertyChanged("UpDscDtl");
                }
            }
        }

        private String z_UpIndMalha;
        [DbColumn("UP_IND_MALHA")]
        [DataMember]
        
        public String UpIndMalha
        {
            get { return z_UpIndMalha; }
            set
            {
                if (z_UpIndMalha != value)
                {
                    z_UpIndMalha = value;
                    this.RaisePropertyChanged("UpIndMalha");
                }
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(UnidProducao value)
        {
            if (value == null)
                return false;
            return
				this.UpIdUnp == value.UpIdUnp &&
				this.EpIdEmp == value.EpIdEmp &&
				this.UpCodUnd == value.UpCodUnd &&
				this.UpDscRsm == value.UpDscRsm &&
				this.UpTimestamp == value.UpTimestamp &&
				this.UpDscDtl == value.UpDscDtl &&
				this.UpIndMalha == value.UpIndMalha;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public UnidProducao CloneT()
        {
            UnidProducao value = new UnidProducao();
            value.RecordStatus = this.RecordStatus;
            value.RecordTag = this.RecordTag;

			value.UpIdUnp = this.UpIdUnp;
			value.EpIdEmp = this.EpIdEmp;
			value.UpCodUnd = this.UpCodUnd;
			value.UpDscRsm = this.UpDscRsm;
			value.UpTimestamp = this.UpTimestamp;
			value.UpDscDtl = this.UpDscDtl;
			value.UpIndMalha = this.UpIndMalha;

            return value;
        }

        #endregion Clone

        #region Create

        public static UnidProducao Create(Decimal _UpIdUnp, Decimal _EpIdEmp, String _UpCodUnd, String _UpDscRsm, DateTime? _UpTimestamp, String _UpDscDtl, String _UpIndMalha)
        {
            UnidProducao __value = new UnidProducao();

			__value.UpIdUnp = _UpIdUnp;
			__value.EpIdEmp = _EpIdEmp;
			__value.UpCodUnd = _UpCodUnd;
			__value.UpDscRsm = _UpDscRsm;
			__value.UpTimestamp = _UpTimestamp;
			__value.UpDscDtl = _UpDscDtl;
			__value.UpIndMalha = _UpIndMalha;

            return __value;
        }

        #endregion Create

   }

}
