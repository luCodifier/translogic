﻿namespace Translogic.Modules.Core.Util
{
    using System;

    /// <summary>
    /// Exception customizada para o processamento de NFE
    /// </summary>
    public class NfeException : Exception
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="mensagem">Mensagem de erro</param>
        public NfeException(string mensagem)
            : base(mensagem)
        {
        }
    }
}