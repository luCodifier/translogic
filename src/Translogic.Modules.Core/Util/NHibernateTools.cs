﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NHibernate
{

    /// <summary>
    /// Método úteis do nhibernate
    /// </summary>
    public static class NHibernateTools
    {

        /// <summary>
        /// Converte um texto no formato HQL para o formato 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="hqlQuery"></param>
        /// <returns></returns>
        public static string ConvertToSql(this ISession session, string hqlQuery)
        {
            var filter = new Dictionary<string, IFilter>();
            var sf = (NHibernate.Engine.ISessionFactoryImplementor)session.SessionFactory;
            var sqls = new NHibernate.Engine.Query.HQLStringQueryPlan(hqlQuery, true, filter, sf);
            var sql = string.Join(";", sqls.SqlStrings);
            return sql;
        }
    }
}