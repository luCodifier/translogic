﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Translogic.Modules.Core.Util
{
    public class TimerLogger : IDisposable
    {
        string _message;
        Stopwatch _timer;
        private string m_exePath = string.Empty;

        public TimerLogger(string message)
        {
            this._message = message;
            this._timer = new Stopwatch();
            this._timer.Start();
        }

        public void Dispose()
        {
            this._timer.Stop();
            var ms = this._timer.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ms.Hours, ms.Minutes, ms.Seconds, ms.Milliseconds / 10);

            // log the performance timing with the Logging library of your choice
            // Example:
            // Logger.Write(
            //     string.Format("{0} - Elapsed Milliseconds: {1}", this._message, ms)
            // );

            LogWrite(string.Format("{0} - Elapse: {1}", this._message, elapsedTime));
        }

        public void LogWrite(string logMessage)
        {
            m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + "log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
            }
            catch (Exception ex)
            {
            }
        }
    }
}