﻿namespace Translogic.Modules.Core.Util
{
    public enum EnumStatusAtivo
    {
        Aberto = 1,
        Encerrado = 2
    }
}