﻿
namespace Translogic.Modules.Core.Util
{
    public enum EnumStatusAtivoItem
    {
        Pendente = 1,
        Aprovado = 2,
        Reprovado = 3,
        Refaturando = 4
    }
}