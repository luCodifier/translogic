﻿using System.Net;
using System.Xml;
using System.IO;
using System.Text;
using System;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Web;
using Translogic.Modules.Core.Domain.Model.Dto.AnexacaoDesanexacao;
using Translogic.Modules.Core.Domain.Services.Administracao;
using Translogic.Modules.Core.Domain.Services;
namespace Translogic.Modules.Core.Util
{


    /// <summary>
    /// Classe que realiza a conexão e obtenção de emails
    /// </summary>
    public class WebService : IWebService
    {      
        private readonly ConfGeralService _configGeralService;

        public WebService(ConfGeralService configGeralService)
        {
            _configGeralService = configGeralService;
        }

        #region Trava

        /// <summary>
        /// Execute a Soap WebService call
        /// </summary>
        public ResultTravaTremDto ExecutarTravaTrem(decimal os, string usuario)
        {
            string result = string.Empty;
            ResultTravaTremDto resulTravaTremDto;
            var configuracaoURL = _configGeralService.Obter("URL_BARRAMENTO_CORPORATIVO");

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"http://soa2-qas.rumolog.net:8080/osb/Rumo/Ferrovia/RestService/ManterRegrasSegurancaVia/analisarRegrasEngenharia?os=" + os + "&login=" + usuario + "&nocache=" + Environment.TickCount);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(configuracaoURL.Valor + "osb/Rumo/Ferrovia/RestService/ManterRegrasSegurancaVia/analisarRegrasEngenharia?os=" + os + "&login=" + usuario + "&nocache=" + Environment.TickCount);
            //webRequest.Headers.Add(@"SOAP:Action");
            var configuracaoToken = _configGeralService.Obter("TOKEN_BARRAMENTO_CORPORATIVO");
            //request.Headers.Add("Authorization", "Basic " + "Y29uc3VsdGFGZXJyb3ZpYXM6YzBuc3VsdEAxNg==");
            request.Headers.Add("Authorization", "Basic " + configuracaoToken.Valor);
            request.ContentType = "application/xml;charset=\"utf-8\"";
            request.Accept = "application/xml";
            request.Method = "GET";

            //ler e exibir a resposta
            using (var resposta = request.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();
                result = objResponse.ToString();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(result);

                XmlNodeList nodeCriticidade = xmlDoc.GetElementsByTagName("criticidade");
                string criticidade = nodeCriticidade[0].InnerText;

                XmlNodeList nodeConformidade = xmlDoc.GetElementsByTagName("conformidade");
                string conformidade = nodeConformidade[0].InnerText ;

                resulTravaTremDto = new ResultTravaTremDto() {
                    criticidade = criticidade,
                    conformidade = conformidade
                };           

                //Object obj = ObjectToXML(result, typeof(Employee));
                //var post = JsonConvert.DeserializeObject<Post>(objResponse.ToString());
                streamDados.Close();
                resposta.Close();
            }
            return resulTravaTremDto;
        }
        #endregion
    }
}