﻿
namespace Translogic.Modules.Core.Util
{
    public enum EnumTipoAtivo
    {
        Eot = 1,
        Locomotiva = 2,
        Vagao = 3,
        VagaoRefaturamento = 4
    }
}