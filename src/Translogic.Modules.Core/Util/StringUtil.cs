﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace Translogic.Modules.Core.Util
{
    public static class StringUtil
    {
        public static string RemoveAspas(string texto)
        {
            texto = Regex.Replace(texto, @"[\u2018\u2019\u201a\u201b\u0022\u201c\u201d\u201e\u201f\u301d\u301e\u301f]", "");
            return texto;
        }

        /// <summary>
        /// Formata texto TEXT AREA
        /// </summary>
        /// <param name="texto"> texto a ser formatado</param>
        /// <returns>Retorna texto formatado para renderizar HTML</returns>
        public static string FormataTextoTextArea(string texto)
        {
            string textoFormatado = string.Empty;

            textoFormatado = texto.Replace("'", "\\'").Replace("\n", "\\n").Replace("\r", "\\r");

            return textoFormatado;
        }
    }
}