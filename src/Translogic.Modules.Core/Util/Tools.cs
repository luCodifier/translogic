using System.Linq;
using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;

namespace Translogic.Modules.Core.Util
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Text;
    using System.Text.RegularExpressions;
    using Domain.Model.Diversos.Cte;
    using iTextSharp.text.pdf;
    using iTextSharp.text.pdf.parser;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Op��es para o trunc da string
    /// </summary>
    [Flags]
    public enum TruncateOptions
    {
        /// <summary>
        /// Sem op��o do trunc
        /// </summary>
        None = 0x0,

        /// <summary>
        /// Make sure that the string is not truncated in the middle of a word
        /// </summary>
        [Description("Make sure that the string is not truncated in the middle of a word")]
        FinishWord = 0x1,

        /// <summary>
        /// If FinishWord is set, this allows the string to be longer than the maximum length if there is a word started and not finished before the maximum length
        /// </summary>
        [Description("If FinishWord is set, this allows the string to be longer than the maximum length if there is a word started and not finished before the maximum length")]
        AllowLastWordToGoOverMaxLength = 0x2,

        /// <summary>
        /// Include an ellipsis HTML character at the end of the truncated string.  This counts as one of the characters for the maximum length
        /// </summary>
        [Description("Include an ellipsis HTML character at the end of the truncated string.  This counts as one of the characters for the maximum length")]
        IncludeEllipsis = 0x4
    }

    public enum RegexChaveNfe
    {
        /// <summary>
        /// Campo da Chave Nfe
        /// </summary>
        [Description(@"^([0-9]{44})$")]
        ChaveNfe
    }

    /// <summary>
    /// Classe de utilidades copiado do codeproject
    /// http://www.codeproject.com/Articles/47930/String-Truncation-Function-for-C
    /// </summary>
    public static class Tools
    {
        private static string mask = "()-.;[]{}!@#$%�+/?: ";


        /// <summary>
        /// Copia os bytes do Stream
        /// </summary>
        /// <param name="input">Stream de entrada</param>
        /// <returns>Retorna um Memory stream</returns>
        public static MemoryStream CopyToMemory(Stream input)
        {
            // It won't matter if we throw an exception during this method;
            // we don't *really* need to dispose of the MemoryStream, and the
            // caller should dispose of the input stream
            MemoryStream ret = new MemoryStream();

            byte[] buffer = new byte[8192];
            int bytesRead;
            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ret.Write(buffer, 0, bytesRead);
            }
            // Rewind ready for reading (typical scenario)
            ret.Position = 0;
            return ret;
        }

        /// <summary>
        /// Copia os dados do Stream para um FileStream
        /// </summary>
        /// <param name="fileName">Nome do arquivo que ser� salvado as informa��es</param>
        /// <param name="inputStream">Stream de entrada</param>
        public static void CopyStreamToFileStream(string fileName, Stream inputStream)
        {
            byte[] buffer = null;

            BinaryReader binaryReader = new System.IO.BinaryReader(inputStream);

            long totalBytes = inputStream.Length;

            buffer = binaryReader.ReadBytes((int)totalBytes);

            SaveByteArrayToFile(fileName, buffer);
        }

        /// <summary>
        /// Converte um arquivo binario em um array de bytes
        /// </summary>
        /// <param name="fileName">Nome (caminho) do arquivo</param>
        /// <returns>Retorna um array de bytes</returns>
        public static byte[] ConvertBinaryFileToByteArray(string fileName)
        {
            byte[] buffer = null;

            try
            {
                FileStream fileStream = new FileStream(fileName, FileMode.Open, System.IO.FileAccess.Read);

                BinaryReader binaryReader = new System.IO.BinaryReader(fileStream);

                long totalBytes = new FileInfo(fileName).Length;

                buffer = binaryReader.ReadBytes((int)totalBytes);

                // close file reader
                fileStream.Close();
                fileStream.Dispose();
                binaryReader.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return buffer;
        }

        /// <summary>
        /// Grava em arquivo o conteudo binario
        /// </summary>
        /// <param name="fileName">Nome do arquivo de saida</param>
        /// <param name="byteArray">Conte�do do arquivo</param>
        public static void SaveByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                FileStream fileStream = new FileStream(fileName, FileMode.Create, System.IO.FileAccess.Write);

                BinaryWriter binaryWriter = new BinaryWriter(fileStream);
                binaryWriter.Write(byteArray);

                // close file reader
                fileStream.Close();
                fileStream.Dispose();
                binaryWriter.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Converte uma Stream para um Array de Bytes
        /// </summary>
        /// <param name="stream"> Stream a ser convertida</param>
        /// <returns> Retorna o array de bytes</returns>
        public static byte[] ConverterStreamToArray(Stream stream)
        {
            // Recupera o tamanho da stream
            byte[] buffer = new byte[stream.Length];

            using (MemoryStream ms = new MemoryStream())
            {
                // Transforma a stream para array de bytes
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                // Retorna o array de bytes
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Converte o conteudo binario para Stream
        /// </summary>
        /// <param name="byteArray">Conte�do do arquivo</param>
        /// <returns>Retorna a Stream do arquivo</returns>
        public static Stream ByteArrayToStream(byte[] byteArray)
        {
            if (byteArray == null)
            {
                return null;
            }

            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream(byteArray);

                return memoryStream;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Converte o conteudo string para Stream
        /// </summary>
        /// <param name="mensagem">string do arquivo</param>
        /// <returns>Retorna a Stream do arquivo</returns>
        public static Stream StringToStream(string mensagem)
        {
            if (!String.IsNullOrEmpty(mensagem))
            {
                UTF8Encoding encoding = new UTF8Encoding();
                return ByteArrayToStream(encoding.GetBytes(mensagem));
            }

            return null;
        }

        /// <summary>
        /// Grava em arquivo o conteudo da string
        /// </summary>
        /// <param name="fileName">Nome do arquivo de saida</param>
        /// <param name="buffer">Conte�do do arquivo</param>
        public static void SaveStringToFile(string fileName, string buffer)
        {
            try
            {
                StreamWriter writer = new StreamWriter(fileName);
                writer.WriteLine(buffer);

                // close file reader
                writer.Close();
                writer.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Converte um arquivo binario em uma string
        /// </summary>
        /// <param name="fileName">Nome (caminho) do arquivo</param>
        /// <returns>Retorna uma string com os valores do arquivo</returns>
        public static string ConvertFileToString(string fileName)
        {
            string retorno = string.Empty;

            byte[] buffer = null;

            try
            {
                FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);

                BinaryReader binaryReader = new BinaryReader(fileStream);

                long totalBytes = new FileInfo(fileName).Length;

                buffer = binaryReader.ReadBytes((int)totalBytes);

                // close file reader
                fileStream.Close();
                fileStream.Dispose();
                binaryReader.Close();

                retorno = System.Text.ASCIIEncoding.UTF8.GetString(buffer);
            }
            catch (Exception ex)
            {
                throw;
            }

            return retorno;
        }

        /// <summary>
        /// L� texto de um arquivo de resource
        /// </summary>
        /// <param name="resourceName">Nome (caminho) do arquivo</param>
        /// <returns>Retorna uma string com os valores do arquivo</returns>
        public static string GetFromResources(string resourceName)
        {
            string result;
            System.Reflection.Assembly assem = System.Reflection.Assembly.GetExecutingAssembly();
            using (System.IO.Stream stream = assem.GetManifestResourceStream(assem.GetName().Name + '.' + resourceName))
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }

        /// <summary>
        /// Converter timespan para formato de horas
        /// </summary>
        /// <param name="ts">TimeSpan para formata��o</param>
        /// <returns>String formatada</returns>
        public static string ToTimeString(this TimeSpan ts)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0:00}", Math.Truncate(ts.TotalHours));
            sb.Append(":");
            sb.AppendFormat("{0:00}", ts.Minutes);
            sb.Append(":");
            sb.AppendFormat("{0:00}", ts.Seconds);

            return sb.ToString();
        }

        /// <summary>
        /// Realiza o truncate da string
        /// </summary>
        /// <param name="valueToTruncate">String para ser truncada</param>
        /// <param name="maxLength">Tamanho m�ximo da string</param>
        /// <returns>Retorna a string truncada</returns>
        public static string TruncateString(string valueToTruncate, int maxLength)
        {
            return TruncateString(valueToTruncate, maxLength, TruncateOptions.None);
        }

        /// <summary>
        /// Truncate e remove os caracteres pessoal
        /// </summary>
        /// <param name="valueToTruncate">String para ser truncada</param>
        /// <param name="maxLength">Tamanho m�ximo da string</param>
        /// <returns>Retorna a string truncada</returns>
        public static string TruncateRemoveSpecialChar(string valueToTruncate, int maxLength)
        {
            if (valueToTruncate == null || maxLength <= 0)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(valueToTruncate))
            {
                return string.Empty;
            }

            string retorno = string.Empty;
            string[] valorArray = valueToTruncate.Split(mask.ToCharArray());

            retorno = string.Empty;

            foreach (string aux in valorArray)
            {
                if (!string.IsNullOrEmpty(aux.Trim()))
                {
                    retorno += aux;
                }
            }

            return TruncateString(retorno, maxLength);
        }

        /// <summary>
        /// Truncate e remove os caracteres pessoal
        /// </summary>
        /// <param name="valueToTruncate">String para ser truncada</param>
        /// <param name="maxLength">Tamanho m�ximo da string</param>
        /// <param name="minLength">Tamanho minimo da string</param>
        /// <returns>Retorna a string truncada</returns>
        public static string TruncateRemoveSpecialChar(string valueToTruncate, int maxLength, int minLength)
        {
            if (valueToTruncate == null || maxLength <= 0)
            {
                return string.Empty;
            }

            if (String.IsNullOrEmpty(valueToTruncate))
            {
                return string.Empty;
            }

            if (valueToTruncate.Length < minLength)
            {
                return string.Empty;
            }

            string retorno = string.Empty;
            string[] valorArray = valueToTruncate.Split(mask.ToCharArray());

            retorno = string.Empty;

            foreach (string aux in valorArray)
            {
                if (!string.IsNullOrEmpty(aux.Trim()))
                {
                    retorno += aux;
                }
            }

            return TruncateString(retorno, maxLength);
        }

        /// <summary>
        /// Trunca os valores de acordo com o parametro
        /// </summary>
        /// <param name="valor">Valor para ser truncado</param>
        /// <param name="tipoTruncate">Tipo de truncate (casas decimais)</param>
        /// <returns>Valor truncado</returns>
        public static double TruncateValue(double? valor, TipoTruncateValueEnum tipoTruncate)
        {
            if (valor == null)
            {
                return 0.0;
            }

            decimal valorAux;
            string valorDesc = ALL.Core.Util.Enum<TipoTruncateValueEnum>.GetDescriptionOf(tipoTruncate);
            int valorCalculo = int.Parse(valorDesc);

            decimal x = (decimal)valor.Value;

            // double valorx = Math.Round(valor.Value, 3);

            valorAux = Math.Round(x * valorCalculo);

            // valorAux = (int)(valor.Value * valorCalculo);
            return (double)(valorAux / valorCalculo);
        }

        public static decimal TruncateValue(decimal? valor, TipoTruncateValueEnum tipoTruncate)
        {
            if (valor == null)
            {
                return 0M;
            }

            decimal valorAux;
            string valorDesc = ALL.Core.Util.Enum<TipoTruncateValueEnum>.GetDescriptionOf(tipoTruncate);
            int valorCalculo = int.Parse(valorDesc);

            decimal x = (decimal)valor.Value;

            // double valorx = Math.Round(valor.Value, 3);

            valorAux = Math.Round(x * valorCalculo);

            // valorAux = (int)(valor.Value * valorCalculo);
            return (decimal)(valorAux / valorCalculo);
        }

        /// <summary>
        /// Separa uma string separado por ",;|" numa lista
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<string> ParseList(string value)
        {
            return ParseList(value, ',', ';', '|');
        }
        public static List<string> ParseList(string value, params char[] separators)
        {
            return (value ?? "").Split(new char[] { ',', ';', '|' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToList();
        }

        /// <summary>
        /// Valida se o n�mero de telefone � v�lido
        /// </summary>
        /// <param name="telefone">telefone da empresa</param>
        /// <returns>retorna o telefone</returns>
        public static string ValidarTelefone(string telefone)
        {
            if (telefone == null)
            {
                return string.Empty;
            }

            string retorno = Tools.TruncateRemoveSpecialChar(telefone, 12, 7);

            Regex regex = new Regex(ALL.Core.Util.Enum<RegexSefaz>.GetDescriptionOf(RegexSefaz.ER36));
            Match match = regex.Match(retorno);

            retorno = match.Success ? match.Value : string.Empty;

            return retorno;
        }

        /// <summary>
        /// Calculo do digito verificador baseado no modulo 11
        /// </summary>
        /// <param name="chave">Chave a ser verificada</param>
        /// <returns>Retorna o digito verificador</returns>
        public static string CalculoDigitoVerificador(string chave)
        {
            int soma = 0; // Guardar a Soma
            int mod = -1; // Guardar o Resto da divis�o
            int dv = -1;  // Guardar o DigitoVerificador
            int peso = 2; // Guardar o peso de multiplicacao

            // Percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o peso
            for (int i = chave.Length - 1; i != -1; i--)
            {
                int ch = Convert.ToInt32(chave[i].ToString());
                soma += ch * peso;
                // Sempre que for 9 voltamos o peso a 2
                if (peso < 9)
                    peso += 1;
                else
                    peso = 2;
            }

            // Agora que tenho a soma vamos pegar o resto da divis�o por 11
            mod = soma % 11;
            // Se o resto da divis�o for 0 ou 1 ent�o o dv vai ser 0
            if (mod == 0 || mod == 1)
                dv = 0;
            else
                dv = 11 - mod;

            return dv.ToString();
        }


        /// <summary>
        /// Valida a chave do Conhecimento Eletr�nico atrav�s do Digito Verificador
        /// </summary>
        /// <param name="chaveCte">Chave do Conhecimento Eletronico</param>
        /// <returns>Se a chave da nota fiscal � verdadeira</returns>
        public static bool ValidaChaveCte(string chaveCte)
        {
            if (String.IsNullOrEmpty(chaveCte))
                return false;

            if (chaveCte.Length != 44)
                return false;

            var modelo = chaveCte.Substring(20, 2);
            if (modelo != "57")
                return false;

            var digitoVerificador = chaveCte.Substring(43, 1);
            var digitoVerificadorCalculado = CalculoDigitoVerificador(chaveCte.Substring(0, 43));

            return digitoVerificador == digitoVerificadorCalculado;
        }

        /// <summary>
        /// Valida a chave da Nota Fiscal atrav�s do Digito Verificador
        /// </summary>
        /// <param name="chaveNfe">Chave da Nota Fiscal</param>
        /// <returns>Se a chave da nota fiscal � verdadeira</returns>
        public static bool ValidaChaveNfe(string chaveNfe)
        {
            if (String.IsNullOrEmpty(chaveNfe))
                return false;

            if (chaveNfe.Length != 44)
                return false;

            var modelo = chaveNfe.Substring(20, 2);
            if (modelo != "55")
                return false;

            var digitoVerificadorNFe = chaveNfe.Substring(43, 1);
            var digitoVerificadorCalculado = CalculoDigitoVerificador(chaveNfe.Substring(0, 43));

            return digitoVerificadorNFe == digitoVerificadorCalculado;
        }

        /// <summary>
        /// Realiza o truncate da string
        /// </summary>
        /// <param name="valueToTruncate">String para ser truncada</param>
        /// <param name="maxLength">Tamanho m�ximo da string</param>
        /// <param name="options">Op��es para o trunc</param>
        /// <returns>Retorna a string truncada</returns>
        private static string TruncateString(string valueToTruncate, int maxLength, TruncateOptions options)
        {
            if (valueToTruncate == null || maxLength <= 0)
            {
                return string.Empty;
            }

            if (valueToTruncate.Length <= maxLength)
            {
                return valueToTruncate;
            }

            bool includeEllipsis = (options & TruncateOptions.IncludeEllipsis) == TruncateOptions.IncludeEllipsis;
            bool finishWord = (options & TruncateOptions.FinishWord) == TruncateOptions.FinishWord;
            bool allowLastWordOverflow = (options & TruncateOptions.AllowLastWordToGoOverMaxLength) == TruncateOptions.AllowLastWordToGoOverMaxLength;

            string retValue = valueToTruncate;

            if (includeEllipsis)
            {
                maxLength -= 1;
            }

            int lastSpaceIndex = retValue.LastIndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);

            if (!finishWord)
            {
                retValue = retValue.Remove(maxLength);
            }
            else if (allowLastWordOverflow)
            {
                int spaceIndex = retValue.IndexOf(" ", maxLength, StringComparison.CurrentCultureIgnoreCase);
                if (spaceIndex != -1)
                {
                    retValue = retValue.Remove(spaceIndex);
                }
            }
            else if (lastSpaceIndex > -1)
            {
                retValue = retValue.Remove(lastSpaceIndex);
            }

            if (includeEllipsis && retValue.Length < valueToTruncate.Length)
            {
                retValue += "&hellip;";
            }

            return retValue;
        }

        public static string BuscarChaveNfeDoArquivoPDF(byte[] pdf)
        {
            bool isCartaCorrecao = false;

            try
            {
                var pdfReader = new PdfReader(pdf);

                for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                    var currentText = PdfTextExtractor.GetTextFromPage(
                        pdfReader,
                        page,
                        strategy);

                    currentText =
                        Encoding.UTF8.GetString(Encoding.Convert(
                            Encoding.Default,
                            Encoding.UTF8,
                            Encoding.Default.GetBytes(currentText)));

                    isCartaCorrecao = currentText.IndexOf("Carta de Corre��o", StringComparison.OrdinalIgnoreCase) > -1
                                   || currentText.IndexOf("Carta de Correcao", StringComparison.OrdinalIgnoreCase) > -1;

                    if (isCartaCorrecao)
                    {
                        throw new Exception("Documento inv�lido. Este PDF � uma Carta de Corre��o");
                    }

                    IEnumerable<string> linhas = new LineReader(() => new StringReader(currentText));
                    foreach (string line in linhas)
                    {
                        var textoSemEspaco = line.Replace(" ", String.Empty)
                                                 .Replace("\0", String.Empty)
                                                 .Replace(".", String.Empty)
                                                 .Replace("-", String.Empty);

                        // Retirando todos os caracteres diferentes de numerais
                        var pattern = "[^\\d+]+";
                        var replacement = String.Empty;
                        var rgx = new Regex(pattern);
                        var textoApenasNumerais = rgx.Replace(textoSemEspaco, replacement);

                        // Caso a linha possuir mais de 44 caracteres, que � o tamanho da chave da NFes, devemos pegar a �ltima parte contendo 44 caracteres
                        if (textoApenasNumerais.Length >= 44)
                        {
                            var texto = (textoApenasNumerais.Length > 44) ? textoApenasNumerais.Substring(textoApenasNumerais.Length - 44, 44) : textoApenasNumerais;
                            /* Alguns clientes est�o enviando no e-mail os PDFs de CT-e. */
                            /* Devemos ignorar esses PDFs pois sen�o o job considera-os como NF-e e imprime como NF-e. */
                            /* Como o PDF do CT-e possui chaves de NF-e em seu corpo, o job acaba associando o PDF do CT-e na NF-e. */
                            if (ValidaChaveCte(texto))
                            {
                                return String.Empty;
                            }

                            if (ValidaChaveNfe(texto))
                            {
                                return texto;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (isCartaCorrecao)
                {
                    throw;
                }

                return String.Empty;
            }

            return String.Empty;
        }

        /// <summary>
        /// Busca as poss�veis chaves de nota fiscal do arquivo PDF
        /// </summary>
        /// <param name="pdf">PDF a ser analisado</param>
        /// <returns>Uma lista das poss�veis chaves de nota fiscal</returns>
        public static List<string> BuscarChavesNfeDoArquivoPDF(byte[] pdf)// Stream pdf)
        {
            var pdfCte = false;
            var chavesNfe = new List<string>();

            try
            {
                var pdfReader = new PdfReader(pdf);

                for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                    var currentText = PdfTextExtractor.GetTextFromPage(
                        pdfReader,
                        page,
                        strategy);

                    currentText =
                        Encoding.UTF8.GetString(Encoding.Convert(
                            Encoding.Default,
                            Encoding.UTF8,
                            Encoding.Default.GetBytes(currentText)));

                    IEnumerable<string> linhas = new LineReader(() => new StringReader(currentText));
                    foreach (var line in linhas)
                    {
                        var textoSemEspaco = line.Replace(" ", String.Empty)
                                                 .Replace("\0", String.Empty)
                                                 .Replace(".", String.Empty)
                                                 .Replace("-", String.Empty);

                        // Retirando todos os caracteres diferentes de numerais
                        string pattern = "[^\\d+]+";
                        string replacement = String.Empty;
                        Regex rgx = new Regex(pattern);
                        var textoApenasNumerais = rgx.Replace(textoSemEspaco, replacement);

                        // Caso a linha possuir mais de 44 caracteres, que � o tamanho da chave da NFes, devemos pegar a �ltima parte contendo 44 caracteres
                        if (textoApenasNumerais.Length >= 44)
                        {
                            string texto = (textoApenasNumerais.Length > 44) ? textoApenasNumerais.Substring(textoApenasNumerais.Length - 44, 44) : textoApenasNumerais;
                            /* Alguns clientes est�o enviando no e-mail os PDFs de CT-e. */
                            /* Devemos ignorar esses PDFs pois sen�o o job considera-os como NF-e e imprime como NF-e. */
                            /* Como o PDF do CT-e possui chaves de NF-e em seu corpo, o job acaba associando o PDF do CT-e na NF-e. */
                            if (ValidaChaveCte(texto))
                            {
                                pdfCte = true;
                                chavesNfe.Clear();
                            }
                            else if (ValidaChaveNfe(texto))
                            {
                                if (!pdfCte)
                                {
                                    chavesNfe.Add(texto);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return chavesNfe.Distinct().ToList();
            }

            return chavesNfe.Distinct().ToList();
        }

        /// <summary>
        /// Verifica se o pdf em bytes n�o est� corrompido
        /// </summary>
        /// <param name="pdf">Pdf em Bytes</param>
        /// <returns>Retorno se o PDF est� correto</returns>
        public static bool VerificaPdfValido(byte[] pdf)
        {
            var pdfValido = true;
            try
            {
                // Quando o PDF vier corrompido, o PdfReader lancar� exce��o de "trailer not found" e cair� no catch
                var reader = new PdfReader(pdf);

                // Em alguns casos o PdfMerge (utilizado em v�rias partes do Translogic) ocorre erro de que o PDF n�o possui p�ginas.
                // Se ocorrer erro, cair� no catch abortando o salvamento deste bin�rio
                var pdfMerge = new PdfMerge();
                pdfMerge.AddFile(pdf);
                // using serve para desalocar a mem�ria alocada
                using (var stream = pdfMerge.Execute()) { }
            }
            catch (Exception ex)
            {
                pdfValido = false;
            }

            return pdfValido;
        }

        /// <summary>
        /// Retorna o valor do attributo Description do Enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Desc(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string Q(this string value)
        {
            return string.Format("'{0}'", value);
        }

        public static string SN(this bool value)
        {
            return value ? "S" : "N";
        }
        public static string SN(this bool? value)
        {
            return value == true ? "S" : "N";
        }

        public static string SNQ(this bool value)
        {
            return string.Format("'{0}'", value == true ? "S" : "N");
        }
        public static string SNQ(this bool? value)
        {
            return string.Format("'{0}'", value == true ? "S" : "N");
        }

        public static string ToDate(this DateTime date)
        {
            return string.Format("TO_DATE('{0}', 'YYYY/MM/DD HH24:MI:SS')", date.ToString("yyyy/MM/dd HH:mm:ss"));
        }
        public static string ToDate(this DateTime? date)
        {
            return ToDate(date.Value);
        }

    }
}
