﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Translogic.Modules.Core.Util
{
    public enum EnumOSLimpezaVagaoStatus
    {
        Aberta = 1,
        Parcial = 2,
        Fechada = 3, 
        Cancelada = 4
    }
}