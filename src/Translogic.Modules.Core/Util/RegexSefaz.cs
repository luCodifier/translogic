namespace Translogic.Modules.Core.Util
{
	using System.ComponentModel;

	/// <summary>
	/// Regular Expression de valida��o para campo especifico.
	/// </summary>
	public enum RegexSefaz
	{
		/// <summary>
		/// Campos ER1
		/// </summary>
		[Description(@"[0-9]{2}")]
		ER1,

		/// <summary>
		/// Campo da Sefaz ER2
		/// </summary>
		[Description(@"[0-9]{7}")]
		ER2,

		/// <summary>
		/// Campo da Sefaz ER3
		/// </summary>
		[Description(@"[0-9]{44}")]
		ER3,

		/// <summary>
		/// Campo da Sefaz ER4
		/// </summary>
		[Description(@"[0-9]{14}")]
		ER4,

		/// <summary>
		/// Campo da Sefaz ER5
		/// </summary>
		[Description(@"[0-9]{3,14}")]
		ER5,

		/// <summary>
		/// Campo da Sefaz ER6
		/// </summary>
		[Description(@"[0-9]{0}|[0-9]{14}")]
		ER6,

		/// <summary>
		/// Campo da Sefaz ER7
		/// </summary>
		[Description(@"[0-9]{11}")]
		ER7,

		/// <summary>
		/// Campo da Sefaz ER8
		/// </summary>
		[Description(@"[0-9]{3,11}")]
		ER8,

		/// <summary>
		/// Campo da Sefaz ER9
		/// </summary>
		[Description(@"(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))")]
		ER9,

		/// <summary>
		/// Campo da Sefaz ER10
		/// </summary>
		[Description(@"0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?")]
		ER10,

		/// <summary>
		/// Campo da Sefaz ER11
		/// </summary>
		[Description(@"0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?")]
		ER11,

		/// <summary>
		/// Campo da Sefaz ER12
		/// </summary>
		[Description(@"0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?")]
		ER12,

		/// <summary>
		/// Campo da Sefaz ER13
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?")]
		ER13,

		/// <summary>
		/// Campo da Sefaz ER14
		/// </summary>
		[Description(@"0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?")]
		ER14,

		/// <summary>
		/// Campo da Sefaz ER15
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?")]
		ER15,

		/// <summary>
		/// Campo da Sefaz ER16
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{5}|0\.[0-9]{1}[1-9]{1}[0-9]{4}|0\.[0-9]{2}[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}[0-9]{2}|0\.[0-9]{4}[1-9]{1}[0-9]{1}|0\.[0-9]{5}[1-9]{1}|[1-9]{1}[0-9]{0,8}(\.[0-9]{6})?")]
		ER16,

		/// <summary>
		/// Campo da Sefaz ER17
		/// </summary>
		[Description(@"0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?")]
		ER17,

		/// <summary>
		/// Campo da Sefaz ER18
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?")]
		ER18,

		/// <summary>
		/// Campo da Sefaz ER19
		/// </summary>
		[Description(@"0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?")]
		ER19,

		/// <summary>
		/// Campo da Sefaz ER20
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?")]
		ER20,

		/// <summary>
		/// Campo da Sefaz ER21
		/// </summary>
		[Description(@"0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?")]
		ER21,

		/// <summary>
		/// Campo da Sefaz ER22
		/// </summary>
		[Description(@"0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?")]
		ER22,

		/// <summary>
		/// Campo da Sefaz ER23
		/// </summary>
		[Description(@"0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?")]
		ER23,

		/// <summary>
		/// Campo da Sefaz ER24
		/// </summary>
		[Description(@"0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?")]
		ER24,

		/// <summary>
		/// Campo da Sefaz ER25
		/// </summary>
		[Description(@"[0-9]{2,14}")]
		ER25,

		/// <summary>
		/// Campo da Sefaz ER26
		/// </summary>
		[Description(@"[0-9]{0,14}|ISENTO|PR[0-9]{4,8}")]
		ER26,

		/// <summary>
		/// Campo da Sefaz ER27
		/// </summary>
		[Description(@"[0-9]{1,4}")]
		ER27,

		/// <summary>
		/// Campo da Sefaz ER28
		/// </summary>
		[Description(@"[1-9]{1}[0-9]{0,8}")]
		ER28,

		/// <summary>
		/// Campo da Sefaz ER29
		/// </summary>
		[Description(@"[0-9]{15}")]
		ER29,

		/// <summary>
		/// Campo da Sefaz ER30
		/// </summary>
		[Description(@"0|[1-9]{1}[0-9]{0,2}")]
		ER30,

		/// <summary>
		/// Campo da Sefaz ER31
		/// </summary>
		[Description(@"[0-9]{3}")]
		ER31,

		/// <summary>
		/// Campo da Sefaz ER32
		/// </summary>
		[Description(@"[!-�]{1}[ -�]{0,}[!-�]{1}|[!-�]{1}")]
		ER32,

		/// <summary>
		/// Campo da Sefaz ER33
		/// </summary>
		[Description(@"[0-9]{8}")]
		ER33,

		/// <summary>
		/// Campo da Sefaz ER34
		/// </summary>
		[Description(@"(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))T(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d")]
		ER34,

		/// <summary>
		/// Campo da Sefaz ER35
		/// </summary>
		[Description(@"[0-9]{1}")]
		ER35,

		/// <summary>
		/// Campo da Sefaz ER36
		/// </summary>
		[Description(@"^$|[0-9]{7,12}")]
		ER36,

		/// <summary>
		/// Campo da Sefaz ER37
		/// </summary>
		[Description(@"[1-9]{1}[0-9]{1,8}")]
		ER37,

		/// <summary>
		/// Campo da Sefaz ER38
		/// </summary>
		[Description(@"[0-9]{8,9}")]
		ER38,

		/// <summary>
		/// Campo da Sefaz ER39
		/// </summary>
		[Description(@"[0-9]{1,20}")]
		ER39,

		/// <summary>
		/// Campo da Sefaz ER40
		/// </summary>
		[Description(@"1\.04")]
		ER40,

		/// <summary>
		/// Campo da Sefaz ER41
		/// </summary>
		[Description(@"[1-9]{1}[0-9]{0,3}|ND")]
		ER41,

		/// <summary>
		/// Campo da Sefaz ER42
		/// </summary>
		[Description(@"[A-Z0-9]+")]
		ER42,

		/// <summary>
		/// Campo da Sefaz ER43
		/// </summary>
		[Description(@"[0-9]{1,6}")]
		ER43,

		/// <summary>
		/// Campo da Sefaz ER44
		/// </summary>
		[Description(@"CTe[0-9]{44}")]
		ER44,

		/// <summary>
		/// Campo da Sefaz ER45
		/// </summary>
		[Description(@"[0-9]{7,10}")]
		ER45,

		/// <summary>
		/// Campo da Sefaz ER46
		/// </summary>
		[Description(@"[123567][0-9]([0-9][1-9]|[1-9][0-9])")]
		ER46,

		/// <summary>
		/// Campo da Sefaz ER47
		/// </summary>
		[Description(@"[^@]+@[^\.]+\..+")]
		ER47,

		/// <summary>
		/// Campo da Sefaz ER48
		/// </summary>
		[Description(@"[0-9]{1,15}")]
		ER48,

		/// <summary>
		/// Campo da Sefaz ER49
		/// </summary>
		[Description(@"(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])")]
		ER49,

		/// <summary>
		/// Campo da Sefaz ER50
		/// </summary>
		[Description(@"[A-Z]{3}(([1-9]\d{3})|(0[1-9]\d{2})|(00[1-9]\d)|(000[1-9]))")]
		ER50,

		/// <summary>
		/// Campo da Sefaz ER51
		/// </summary>
		[Description(@"[0-9]{12}")]
		ER51,

		/// <summary>
		/// Campo da Sefaz ER52
		/// </summary>
		[Description(@"[1-9]{1}[0-9]{0,5}")]
		ER52,

		/// <summary>
		/// Campo da Sefaz ER53
		/// </summary>
		[Description(@"0|[1-9]{1}[0-9]{0,5}")]
		ER53,

		/// <summary>
		/// Campo da Sefaz ER54
		/// </summary>
		[Description(@"[0-9]{9}")]
		ER54,

		/// <summary>
		/// Campo da Sefaz ER55
		/// </summary>
		[Description(@"M")]
		ER55,

		/// <summary>
		/// Campo da Sefaz ER56
		/// </summary>
		[Description(@"[1-9]{1}[0-9]{0,9}")]
		ER56,

		/// <summary>
		/// Valida campos de ICMS
		/// </summary>
		[Description(@"^(00|40|41|45|51|60|80|81|90)$")]
		CST
	}
}