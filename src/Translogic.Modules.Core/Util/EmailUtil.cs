using Translogic.Modules.Core.Helpers.ControlePerdas;

namespace Translogic.Modules.Core.Util
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net.Mail;
	using System.Text;
	using System.Text.RegularExpressions;
	using Translogic.Core.Infrastructure;
	using Translogic.Modules.TPCCO.Domain.Model;
	using Translogic.Modules.TPCCO.Domain.Model.Enumeradores;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using System.IO;

    /// <summary>
    /// Classe que implementa envio de email
    /// </summary>
    public static class EmailUtil
    {

        private const string REGEX_EMAIL_VALIDO = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// Envia um e-mail
        /// </summary>
        /// <param name="msg">Mensagem de e-mail</param>
        private static void Send(MailMessage msg)
        {
            if (msg.From == null)
            {
                msg.From = new MailAddress("no-reply@rumolog.com.br");
            }

            msg.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
            msg.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

            SmtpClient client = new SmtpClient();

            client.Send(msg);
        }

        /// <summary>
        /// Metodo que envia email
        /// </summary>
        /// <param name="emailDestino">Emails de destino</param>
        /// <param name="assunto">Assunto do email</param>
        /// <param name="corpo">Corpo do Email</param>
        public static void Send(string emailDestino, string assunto, string corpo)
        {
            Send(emailDestino, assunto, corpo, string.Empty);
        }

        /// <summary>
        /// Metodo que envia email
        /// </summary>
        /// <param name="emailDestino">Emails de destino</param>
        /// <param name="assunto">Assunto do email</param>
        /// <param name="corpo">Corpo do Email</param>
        /// <param name="emailCopia">Emails de destinat�rios em c�pia</param>
        public static void Send(string emailDestino, string assunto, string corpo, string emailCopia)
        {
            Send(emailDestino, assunto, corpo, emailCopia, null);
        }

        /// <summary>
        /// Metodo que envia email
        /// </summary>
        /// <param name="emailDestino">Emails de destino</param>
        /// <param name="assunto">Assunto do email</param>
        /// <param name="corpo">Corpo do Email</param>
        /// <param name="emailCopia">Emails de destinat�rios em c�pia</param>
        /// <param name="anexos">Lista de Anexos do e-mail</param>
        public static void Send(string emailDestino, string assunto, string corpo, string emailCopia, List<Attachment> anexos)
        {
            MailMessage mailMessage = new MailMessage { Body = corpo , IsBodyHtml = true, Subject = assunto};
            
            if (!string.IsNullOrEmpty(emailDestino))
            {
                foreach (var email in emailDestino.Split(';'))
                {
                    if (validarEmail(email))
                    {
                        mailMessage.To.Add(email);
                    }
                }
            }


            if (!string.IsNullOrEmpty(emailCopia))
            {
                foreach (var email in emailCopia.Split(';'))
                {
                    if (validarEmail(email))
                    {
                        mailMessage.CC.Add(email);
                    }
                }
            }

            if (mailMessage.To.Count == 0 && mailMessage.CC.Count == 0)
            {
                throw new TranslogicException("Nenhum destinat�rio informado para o envio de e-mail");
            }


            if (anexos != null && anexos.Count > 0 )
            {
                foreach (var anexo in anexos)
                {
                    mailMessage.Attachments.Add(anexo);
                }
            }

            try
            {
                //LogTfaHelper.TraceEmail("Inicio envio email.");
                Send(mailMessage);
                //LogTfaHelper.TraceEmail("Fim envio email.");
            }
            catch (Exception e)
            {
                //LogTfaHelper.TraceEmail("Entrou TRY CATCH.");
                throw new TranslogicException(e.Message);
            }
        }

        public static Attachment CreateAttachment(Stream attachmentFile, string displayName)
        {
            var attachment = new Attachment(attachmentFile, displayName);
            //attachment.ContentType = new ContentType("application/vnd.ms-excel");
            //attachment.TransferEncoding = TransferEncoding.Base64;
            //attachment.NameEncoding = Encoding.UTF8;
            //string encodedAttachmentName = Convert.ToBase64String(Encoding.UTF8.GetBytes(displayName));
            //encodedAttachmentName = SplitEncodedAttachmentName(encodedAttachmentName);
            //attachment.Name = encodedAttachmentName;
            return attachment;
        }

        /// <summary>
        /// M�todo para validar formato de e-mail
        /// </summary>
        /// <param name="email">e-mail � ser validado</param>
        /// <returns>Retorna true se o formato de e-mail for v�lido</returns>
        public static bool validarEmail(string email)
        {
            return Regex.IsMatch(email, REGEX_EMAIL_VALIDO);
        }
    }
}