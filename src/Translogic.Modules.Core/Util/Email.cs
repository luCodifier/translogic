﻿using System.Collections.Generic;

namespace Translogic.Modules.Core.Util
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml;
    using Microsoft.Exchange.WebServices.Data;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes.Repositories;
    using Translogic.Modules.EDI.Domain.Models.Nfes;

    /// <summary>
    /// Classe que realiza a conexão e obtenção de emails
    /// </summary>
    public class Email
    {
        private readonly string _enderecoEmail;
        private readonly string _senhaEmail;
        private readonly ExchangeService _emailService;
        private readonly string _servidorExchange = "https://outlook.office365.com/EWS/Exchange.asmx";
        private readonly IMonitorEmailJobRunnerRepository _logPRTG;
        private readonly string _pastaEmail;
        private readonly int _qtdEmail;
        private readonly int _qtdDiasExclusaoProcessadas;
        private readonly int _qtdDiasExclusaoOutras;

        /// <summary>
        /// Construtor para inicializar a conexão e obtenção de emails
        /// </summary>
        /// <param name="email">Conta de email que se deseja obter os emails</param>
        /// <param name="senha">Senha da conta de email</param>
        /// <param name="pastaEmail">Pasta do Outlook que será processada</param>
        /// <param name="logPrtg">Log Processamento Prtg</param>
        public Email(string email, string senha, string pastaEmail, int qtdEmail, IMonitorEmailJobRunnerRepository logPrtg, int qtdDiasExclusaoProcessadas, int qtdDiasExclusaoOutras)
        {
            _enderecoEmail = email;
            _senhaEmail = senha;
            _logPRTG = logPrtg;
            //_emailService = new ExchangeService();
            _emailService = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
#if debug
            //pastaEmail = "Teste2";
#endif

            _pastaEmail = pastaEmail;
            _qtdEmail = qtdEmail;
            _qtdDiasExclusaoProcessadas = qtdDiasExclusaoProcessadas;
            _qtdDiasExclusaoOutras = qtdDiasExclusaoOutras;
        }

        /// <summary>
        /// Delegate para realizar a chamada do método
        /// </summary>
        /// <param name="args">Método para invocar</param>
        public delegate void MetodoDelegate(params object[] args);

        /// <summary>
        /// Obtém os emails e realiza o processamento com o método passado por parâmetro
        /// </summary>
        /// <param name="metodoProcessamento">Nome do método de callback</param>
        /// <param name="metodoLogEmail">Método para realizar o Log do Email</param>
        /// <param name="metodoLogAnexoEmail">Método para realiza o Log do processamento do Anexo do Email</param>
        /// <param name="hora">hora de range que será processados os e-mails</param>
        /// <param name="order">o tipo de ordenação para a busca dos e-mails</param>
        /// <param name="metodoProcessamentoPdf">Métodos que salva os PDFs para o Shirata </param>
        public void ObterEmailsNaoLidosRealizarProcessamento(Action<XmlDocument> metodoProcessamento, Action<LogReadEmailEdi> metodoLogEmail, Action<LogReadEmailEdiNfe> metodoLogAnexoEmail, int hora, string order, Action<byte[]> metodoProcessamentoPdf)
        {
            string passo = "";
            try
            {
                _emailService.Credentials = new NetworkCredential(_enderecoEmail, _senhaEmail);
                _emailService.Url = new Uri(_servidorExchange);
                var folder = Folder.Bind(_emailService, WellKnownFolderName.Inbox);

                passo = "Bind Inbox";

                var itensVisualizacao = new ItemView(_qtdEmail, 0, OffsetBasePoint.Beginning);
                itensVisualizacao.OrderBy.Add(ItemSchema.DateTimeReceived, order.Equals("ASC") ? SortDirection.Ascending : SortDirection.Descending);
                itensVisualizacao.PropertySet = new PropertySet(
                BasePropertySet.IdOnly,
                ItemSchema.Subject,
                ItemSchema.DateTimeReceived);

                passo = "criação da visualização";

                FindItemsResults<Item> emails;
                FindItemsResults<Item> emailsSemAnexo;

                /* move para a pasta de processadas, Erro ou SemXml de acordo com a validação que cair */
                var folders = _emailService.FindFolders(folder.ParentFolderId, new FolderView(1000));
                var moveProcessados = folders.FirstOrDefault(f => f.DisplayName.Equals("Processadas", StringComparison.OrdinalIgnoreCase));
                var moveErro = folders.FirstOrDefault(f => f.DisplayName.Equals("Erro", StringComparison.OrdinalIgnoreCase));
                var moveSemXml = folders.FirstOrDefault(f => f.DisplayName.Equals("SemXML", StringComparison.OrdinalIgnoreCase));
                var eventosNFe = folders.FirstOrDefault(f => f.DisplayName.Equals("Eventos_NFe", StringComparison.OrdinalIgnoreCase));

                /* essa é a nova caixa de entrada, pois a Inbox esta padecendo */

                string pasta;
#if DEBUG
                pasta = "Teste";
#else
                pasta = _pastaEmail;
#endif
                var inbox2 = folders.FirstOrDefault(f => f.DisplayName.Equals(pasta, StringComparison.OrdinalIgnoreCase));

                passo = "Mapea as pastas";

                /* usar essa pasta para teste desse método de processamento de e-mail */
                //var teste = folders.FirstOrDefault(f => f.DisplayName.Equals("Teste", StringComparison.OrdinalIgnoreCase));

                var filtros = new SearchFilter.SearchFilterCollection(LogicalOperator.And)
                                      {
                                          new SearchFilter.IsEqualTo(ItemSchema.HasAttachments, true),
                                          new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false),
                                          new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived,
                                                                                  DateTime.Now.AddHours(hora*-1)),
                                          new SearchFilter.IsLessThan(ItemSchema.DateTimeReceived,
                                                                      DateTime.Now.AddHours(Math.Abs(hora - 1)*-1))
                                      };

                var filtrosSemAnexo = new SearchFilter.SearchFilterCollection(LogicalOperator.And)
                                      {
                                          new SearchFilter.IsEqualTo(ItemSchema.HasAttachments, false),
                                          new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false)
                                      };

                if (hora == 99)
                {
                    filtros = new SearchFilter.SearchFilterCollection(LogicalOperator.And)
                    {
                        new SearchFilter.IsEqualTo(ItemSchema.HasAttachments, true),
                        new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false),
                        new SearchFilter.IsLessThanOrEqualTo(ItemSchema.DateTimeReceived,
                            DateTime.Now.AddHours(3*-1))
                    };
                }

                // A parte comentada é apenas para ficar mais facil o entendimento do desenvolvedor pois do jeito que está (if de negação) 
                // fica dificil de entender 
                // if(inbox2.UnreadCount >= _qtdEmail || order.Equals("DESC"))
                if (!((inbox2.UnreadCount < _qtdEmail) && order.Equals("ASC")))
                {

                    /* atualiza a tabela de monitoramento para o PRTG*/
                    var logPrtg = new MonitorEmailJobRunner
                    {
                        ContEmail = inbox2.UnreadCount,
                        // FindItems(filtros, itensVisualizacao).Count(),
                        Processo = _enderecoEmail,
                        TimestampAtz = DateTime.Now
                    };
                    _logPRTG.Inserir(logPrtg);

                    passo = "Log Prtg";

                    do
                    {
                        emails = inbox2.FindItems(filtros, itensVisualizacao);

                        passo = "Carrega Inbox2";

                        // emails = folder.FindItems(filtros, itensVisualizacao);

                        var emailsList = emails.OfType<EmailMessage>().Select(e => e).ToList();
                        
                        Sis.LogTrace(string.Format("{0} - {1} encontrados", _pastaEmail, emailsList.Count));

                        int count = 0;
                        foreach (var email in emailsList)
                        {
                            count++;
                            Sis.LogTrace(string.Format("Processando {0} de {1}", count, emailsList.Count));
                            email.Load();
                            passo = "Carrega Email";

                            var logEmail = new LogReadEmailEdi()
                            {
                                DataInicioLog = DateTime.Now,
                                DataRecebimentoEmail = email.DateTimeReceived,
                                IdEmailExchange = email.Id.UniqueId,
                                Remetente = email.From.Name + " : " + email.From.Address,
                                AssuntoEmail = email.Subject
                            };

                            ExecutarMetodoLogEmail(metodoLogEmail, logEmail);

                            if (email.HasAttachments && email.Attachments.Any(a => a is FileAttachment))
                            {
                                if (email.Attachments.Any(a => ExtensaoXmlPdf(a.Name)))
                                {
                                    // total XML
                                    var listaAnexos = ObterAnexos(email, ".xml");

                                    // total PDF
                                    var listaPdf = ObterAnexos(email, ".pdf");

                                    passo = "Carrega Anexos";

                                    /* conta os anexos para ver se não enviaram mais notas em pdf do que XML */
                                    if (listaPdf.Count > listaAnexos.Count)
                                    {
                                        var logEmailSemAnexo = new LogReadEmailEdi()
                                        {
                                            DataInicioLog = DateTime.Now,
                                            DataRecebimentoEmail = email.DateTimeReceived,
                                            IdEmailExchange = email.Id.UniqueId,
                                            Remetente = email.From.Name + " : " + email.From.Address,
                                            AssuntoEmail = email.Subject,
                                            MensagemErro = "O Emails possui mais anexos PDF do que XML| " + listaAnexos.Count.ToString() + " XML e " +
                                                                listaPdf.Count.ToString() + " PDF"
                                        };

                                        ExecutarMetodoLogEmail(metodoLogEmail, logEmailSemAnexo);
                                    }

                                    #region Processa lista XML
                                    int contAnexo = 1;
                                    foreach (var arquivoAnexo in listaAnexos)
                                    {
                                        var anexo = arquivoAnexo as FileAttachment;

                                        if (anexo != null)
                                        {
                                            var doc = new XmlDocument();

                                            try
                                            {
                                                #region Processa Anexo
                                                anexo.Load();
                                                /* tem q fazer o encoding pois esta vindo caracter especiais na porcaria do XML */
                                                var ms = new StreamReader(new MemoryStream(anexo.Content),
                                                                          Encoding.GetEncoding("ISO-8859-1"));

                                                //MemoryStream(anexo.Content).ToString().Replace((char) 186, (char)32);
                                                passo = "Processa Anexo";

                                                doc.Load(ms);

                                                var root = doc.DocumentElement;
                                                var nsmgr = new XmlNamespaceManager(doc.NameTable);
                                                nsmgr.AddNamespace("d", root.NamespaceURI);

                                                /* e-mails de cancelamento */
                                                if (root.Name.Equals("procEventoNFe"))
                                                {
                                                    email.Move(eventosNFe.Id);
                                                }
                                                else
                                                {
                                                    /* esse If trata o XML novo da Brado */
                                                    if (root.Name.Equals("soap:Envelope"))
                                                    {
                                                        nsmgr.AddNamespace("soap", root.NamespaceURI);

                                                        var xmlBrado = new StringBuilder();
                                                        xmlBrado.Append(
                                                            "<nfeProc xmlns:ns2=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"3.10\">");

                                                        xmlBrado.Append(
                                                            (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).
                                                                FirstChild).InnerXml.ToString());

                                                        xmlBrado.Append("</nfeProc>");

                                                        /* recarrega o XML em um formato decente */
                                                        doc.LoadXml(xmlBrado.ToString());
                                                        root = doc.DocumentElement;
                                                        nsmgr = new XmlNamespaceManager(doc.NameTable);
                                                        nsmgr.AddNamespace("d", root.NamespaceURI);
                                                    }
                                                    //var noInfNfe = root.Name.Equals("soap:Envelope") ? (((root.SelectSingleNode("//soap:Body", nsmgr)).FirstChild).FirstChild).FirstChild.FirstChild : root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);

                                                    var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);
                                                    var chaveNfe = noInfNfe != null && noInfNfe.Attributes["Id"] == null
                                                                       ? noInfNfe.Attributes["id"].Value
                                                                       : noInfNfe.Attributes["Id"].Value;

                                                    var logAnexoEmail = new LogReadEmailEdiNfe()
                                                    {
                                                        DataInicioProcessamento = DateTime.Now,
                                                        NfeNomeArquivo =
                                                                                    arquivoAnexo.Name ?? anexo.FileName,
                                                        NfeChave = chaveNfe.Substring(3)
                                                    };

                                                    ExecutarMetodoProcessamento(metodoProcessamento, doc);

                                                    logAnexoEmail.DataFimProcessamento = DateTime.Now;
                                                    logAnexoEmail.LogReadEmailEdi = logEmail;

                                                    ExecutarMetodoLogAnexoEmail(metodoLogAnexoEmail, logAnexoEmail);

                                                    contAnexo++;
                                                }

                                                #endregion
                                            }
                                            catch (Exception excepProcAnexo)
                                            {
                                                Sis.LogException(excepProcAnexo);
                                                var root = doc.DocumentElement;
                                                string chaveNfe = null;

                                                if (root != null)
                                                {
                                                    var nsmgr = new XmlNamespaceManager(doc.NameTable);
                                                    nsmgr.AddNamespace("d", root.NamespaceURI);

                                                    var noInfNfe = root.SelectSingleNode("//d:NFe/d:infNFe", nsmgr);
                                                    chaveNfe = noInfNfe != null && noInfNfe.Attributes != null
                                                                       ? (noInfNfe.Attributes["Id"] == null
                                                                              ? noInfNfe.Attributes["id"].Value
                                                                              : noInfNfe.Attributes["Id"].Value)
                                                                       : String.Empty;
                                                }

                                                var logAnexoEmailEx = new LogReadEmailEdiNfe()
                                                {
                                                    DataInicioProcessamento = DateTime.Now,
                                                    DataFimProcessamento = DateTime.Now,
                                                    IndicacaoErro = "E",
                                                    NfeNomeArquivo =
                                                        arquivoAnexo.Name ?? anexo.FileName,
                                                    MensagemErro = excepProcAnexo.Message + " | " + passo,
                                                    NfeChave =
                                                        !string.IsNullOrEmpty(chaveNfe)
                                                            ? chaveNfe.Substring(3)
                                                            : string.Empty,
                                                    LogReadEmailEdi = logEmail
                                                };

                                                logEmail.MensagemErro =
                                                    "O anexo possui extensão XML porém não pode ser processado";

                                                ExecutarMetodoLogAnexoEmail(metodoLogAnexoEmail, logAnexoEmailEx);

                                                if (contAnexo == listaAnexos.Count)
                                                {
                                                    passo = "Move Para Erro1";
                                                    email.Move(moveErro.Id);
                                                }
                                                else
                                                {
                                                    contAnexo++;
                                                    continue;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var logAnexoEmail = new LogReadEmailEdiNfe()
                                            {
                                                DataInicioProcessamento = DateTime.Now,
                                                DataFimProcessamento = DateTime.Now,
                                                IndicacaoErro = "C",
                                                LogReadEmailEdi = logEmail,
                                                NfeNomeArquivo = arquivoAnexo.Name,
                                                MensagemErro =
                                                    "Houve um erro na conversão do arquivo",
                                            };

                                            logEmail.MensagemErro =
                                                "O anexo possui extensão XML porém não pode ser processado";

                                            ExecutarMetodoLogAnexoEmail(metodoLogAnexoEmail, logAnexoEmail);

                                            if (contAnexo == listaAnexos.Count)
                                            {
                                                passo = "Move Para Erro2";
                                                email.Move(moveErro.Id);
                                            }
                                            else
                                            {
                                                contAnexo++;
                                                continue;
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Processa lista PDFs
                                    /* salva os pdf que vem no email */
                                    passo = "Salva PDFs na tabela edi2_nfe_pdf para Shirata";
                                    foreach (var anexo in listaPdf)
                                    {
                                        var arq = anexo as FileAttachment;
                                        try
                                        {
                                            arq.Load();
                                            var sr = new StreamReader(new MemoryStream(arq.Content), Encoding.GetEncoding("ISO-8859-1"));
                                            var buffer = new byte[sr.BaseStream.Length];
                                            sr.BaseStream.Read(buffer, 0, buffer.Length);

                                            sr.Close();
                                            sr.Dispose();

                                            ////ExecutarMetodoProcessamentoPdf(metodoProcessamentoPdf, Tools.ByteArrayToStream(buffer));
                                            ExecutarMetodoProcessamentoPdf(metodoProcessamentoPdf, buffer);

                                            contAnexo++;
                                        }
                                        catch (Exception)
                                        { }
                                    }
                                    #endregion

                                    /* só marca o email como lido depois de processar TODOS os anexos */
                                    var totalGeralAnexos = listaAnexos.Count + listaPdf.Count;
                                    if (contAnexo >= totalGeralAnexos)
                                    {
                                        try
                                        {
                                            email.IsRead = true;
                                            email.Update(ConflictResolutionMode.AlwaysOverwrite);

                                            email.Move(moveProcessados.Id);
                                            passo = "Move Para Processados";
                                        }
                                        catch (Exception exMovAnexo)
                                        {
                                            Sis.LogException(exMovAnexo, "Move para não processados");
                                            // anular exceção para NÃO impedir que sejam lidos os próximos e-mails
                                        }

                                    }

                                } // possui anexo contendo extensão XML ou PDF (ExtensaoXmlPdf(a.Name))
                                else
                                {
                                    var logEmailSemAnexo = new LogReadEmailEdi()
                                    {
                                        DataInicioLog = DateTime.Now,
                                        DataRecebimentoEmail = email.DateTimeReceived,
                                        IdEmailExchange = email.Id.UniqueId,
                                        Remetente = email.From.Name + " : " + email.From.Address,
                                        AssuntoEmail = email.Subject,
                                        MensagemErro = "O formato do anexo no email não é XML"
                                    };

                                    ExecutarMetodoLogEmail(metodoLogEmail, logEmailSemAnexo);

                                    passo = "Move Para SemXml1";
                                    email.Move(moveSemXml.Id);
                                }
                            }
                            else
                            {
                                var logEmailSemAnexo = new LogReadEmailEdi()
                                {
                                    DataInicioLog = DateTime.Now,
                                    DataRecebimentoEmail = email.DateTimeReceived,
                                    IdEmailExchange = email.Id.UniqueId,
                                    Remetente = email.From.Name + " : " + email.From.Address,
                                    AssuntoEmail = email.Subject,
                                    MensagemErro = "Email não contém anexos"
                                };

                                ExecutarMetodoLogEmail(metodoLogEmail, logEmailSemAnexo);

                                passo = "Move Para SemXml2";
                                email.Move(moveSemXml.Id);
                            }

                            ExecutarMetodoLogEmail(metodoLogEmail, logEmail);
                        }

                        if (emails.NextPageOffset.HasValue)
                        {
                            itensVisualizacao.Offset = emails.NextPageOffset.Value;
                        }
                    } while (emails.MoreAvailable);

                    itensVisualizacao = new ItemView(_qtdEmail, 0, OffsetBasePoint.Beginning);
                    itensVisualizacao.OrderBy.Add(ItemSchema.DateTimeReceived, order.Equals("ASC") ? SortDirection.Ascending : SortDirection.Descending);
                    itensVisualizacao.PropertySet = new PropertySet(
                    BasePropertySet.IdOnly,
                    ItemSchema.Subject,
                    ItemSchema.DateTimeReceived);

                    do
                    {
                        // E-mails sem anexo (Mover para SemXml)
                        emailsSemAnexo = inbox2.FindItems(filtrosSemAnexo, itensVisualizacao);
                        foreach (var email in emailsSemAnexo.OfType<EmailMessage>().Select(e => e))
                        {
                            email.Load();

                            if (!email.HasAttachments)
                            {
                                var logEmailSemAnexo = new LogReadEmailEdi()
                                {
                                    DataInicioLog = DateTime.Now,
                                    DataRecebimentoEmail = email.DateTimeReceived,
                                    IdEmailExchange = email.Id.UniqueId,
                                    Remetente = email.From.Name + " : " + email.From.Address,
                                    AssuntoEmail = email.Subject,
                                    MensagemErro = "Email não contém anexos"
                                };

                                ExecutarMetodoLogEmail(metodoLogEmail, logEmailSemAnexo);

                                passo = "Move Para SemXml1";
                                email.Move(moveSemXml.Id);
                            }
                            else
                            {
                                var logEmailSemAnexo = new LogReadEmailEdi()
                                {
                                    DataInicioLog = DateTime.Now,
                                    DataRecebimentoEmail = email.DateTimeReceived,
                                    IdEmailExchange = email.Id.UniqueId,
                                    Remetente = email.From.Name + " : " + email.From.Address,
                                    AssuntoEmail = email.Subject,

                                    MensagemErro = "E-mail com arquivos inválidos"
                                };

                                ExecutarMetodoLogEmail(metodoLogEmail, logEmailSemAnexo);

                                passo = "Move Para Erro";
                                email.Move(moveErro.Id);
                            }

                        }

                        if (emailsSemAnexo.NextPageOffset.HasValue)
                        {
                            itensVisualizacao.Offset = emailsSemAnexo.NextPageOffset.Value;
                        }
                    } while (emailsSemAnexo.MoreAvailable);


                }
            }
            catch (Exception ex)
            {
                Sis.LogException(ex);
                var logEmailErro = new LogReadEmailEdi()
                {
                    DataInicioLog = DateTime.Now,
                    DataRecebimentoEmail = DateTime.Now,
                    IdEmailExchange = String.Empty,
                    Remetente = "Serviço e leitura de email do Edi",
                    AssuntoEmail = "Erro ao tentar processar a leitura de emails",
                    MensagemErro = ex.Message + " | " + passo,
                    DataFimLog = DateTime.Now
                };

                ExecutarMetodoLogEmail(metodoLogEmail, logEmailErro);
                Sis.LogException(ex, ex.Message, null);
            }
        }

        /// <summary>
        /// Execução do método passado por prâmetro
        /// </summary>
        /// <param name="metodoProcessamento">Método que deve ser excutado</param>
        /// <param name="doc">Documento xml para processamento</param>
        public void ExecutarMetodoProcessamento(Action<XmlDocument> metodoProcessamento, XmlDocument doc)
        {
            if (metodoProcessamento != null)
            {
                metodoProcessamento(doc);
            }
        }

        /// <summary>
        /// Realiza a gravação do Log da leitura dos emails na base
        /// </summary>
        /// <param name="metodoLogEmail">Metodo a ser executado</param>
        /// <param name="objetoLog">Objeto de Log</param>
        public void ExecutarMetodoLogEmail(Action<LogReadEmailEdi> metodoLogEmail, LogReadEmailEdi objetoLog)
        {
            if (metodoLogEmail != null)
            {
                metodoLogEmail(objetoLog);
            }
        }

        /// <summary>
        /// Realiza a gravação do Log da leitura dos anexos dos emails na base
        /// </summary>
        /// <param name="metodoLogAnexoEmail">Metodo a ser executado</param>
        /// <param name="objetoLog">Objeto de Log</param>
        public void ExecutarMetodoLogAnexoEmail(Action<LogReadEmailEdiNfe> metodoLogAnexoEmail, LogReadEmailEdiNfe objetoLog)
        {
            if (metodoLogAnexoEmail != null)
            {
                metodoLogAnexoEmail(objetoLog);
            }
        }

        /// <summary>
        /// Verifica a url da conta e redireciona se SSL for verdadeiro
        /// </summary>
        /// <param name="url">Endereço do domínio da conta de email</param>
        /// <returns>Verdadeiro se deve usar https</returns>
        private static bool RedirectionCallback(string url)
        {
            // Return true if the URL is an HTTPS URL.
            return url.ToLower().StartsWith("https://");
        }

        /// <summary>
        /// Execução do método passado por prâmetro
        /// </summary>
        /// <param name="metodoProcessamento">Método que deve ser excutado</param>
        /// <param name="doc">Documento xml para processamento</param>
        public void ExecutarMetodoProcessamentoPdf(Action<byte[]> metodoProcessamento, byte[] doc)
        {
            if (metodoProcessamento != null)
            {
                metodoProcessamento(doc);
            }
        }

        public void ExcluirEmailsMovidosCaixasSecundarias(Action<LogReadEmailEdi> metodoLogEmail, string nomePastaExclusaoEmails, int qtdDiasExclusao)
        {
            string passo = "";
            try
            {
                passo = "Conectando no Servidor Exchange";

                if (_emailService.Credentials == null)
                {
                    _emailService.Credentials = new NetworkCredential(_enderecoEmail, _senhaEmail);
                    _emailService.Url = new Uri(_servidorExchange);
                }

                passo = "Carregando as informações das pastas";
                var folder = Folder.Bind(_emailService, WellKnownFolderName.Inbox);
                var folders = _emailService.FindFolders(folder.ParentFolderId, new FolderView(1000));

                DateTime dtApartirExclusaoRetroativa = DateTime.Now.AddDays(-qtdDiasExclusao);
                DateTime dtFiltroApartirExclusaoRetroativa = new DateTime(dtApartirExclusaoRetroativa.Year, dtApartirExclusaoRetroativa.Month, dtApartirExclusaoRetroativa.Day, 23, 59, 59);
                var filtrosApartirExclusaoRetroativa = new SearchFilter.SearchFilterCollection(LogicalOperator.And)
                {
                    new SearchFilter.IsLessThanOrEqualTo(ItemSchema.DateTimeReceived, dtFiltroApartirExclusaoRetroativa)
                };

                if (String.IsNullOrEmpty(nomePastaExclusaoEmails))
                {
                    passo = "Análise de Pasta de Exclusão de Emails";
                    throw new Exception("A pasta + " + nomePastaExclusaoEmails + " é inválida ou não foi informada como parâmetro!");
                }

                passo = "Realizando a exclusão dos e-mails - Pasta " + nomePastaExclusaoEmails;
                this.GravarLogExclusaoEmail(metodoLogEmail, "Exclusão de emails AdmNfe - Pasta " + nomePastaExclusaoEmails, passo);

                passo = "Excluindo os e-mails - Pasta " + nomePastaExclusaoEmails;
                var folderExclusao = folders.FirstOrDefault(f => f.DisplayName.Equals(nomePastaExclusaoEmails, StringComparison.OrdinalIgnoreCase));
                ProcessarExclusaoEmailsPasta(folderExclusao, filtrosApartirExclusaoRetroativa);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex, "Erro ao tentar processar a exclusão de emails");
                this.GravarLogExclusaoEmail(metodoLogEmail, "Erro ao tentar processar a exclusão de emails", ex.Message + " | " + passo);
            }

        }

        private void ProcessarExclusaoEmailsPasta(Folder pastaEmail, SearchFilter.SearchFilterCollection filterCollection)
        {
            if (pastaEmail.TotalCount > 0)
            {
                FindItemsResults<Item> emailsExclusao;
                var itensVisualizacaoExclusao = new ItemView(200, 0, OffsetBasePoint.Beginning); // Excluir de 200 em 200
                itensVisualizacaoExclusao.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
                itensVisualizacaoExclusao.PropertySet = new PropertySet(
                BasePropertySet.IdOnly,
                ItemSchema.Id,
                ItemSchema.Subject,
                ItemSchema.DateTimeReceived);

                do
                {
                    emailsExclusao = pastaEmail.FindItems(filterCollection, itensVisualizacaoExclusao);
                    if (emailsExclusao.Count() != 0)
                    {
                        foreach (var email in emailsExclusao.OfType<EmailMessage>().Select(e => e))
                        {
                            email.Delete(DeleteMode.SoftDelete);
                        }
                    }

                    if (emailsExclusao.NextPageOffset.HasValue)
                    {
                        itensVisualizacaoExclusao.Offset = emailsExclusao.NextPageOffset.Value;
                    }

                } while (emailsExclusao.MoreAvailable);
            }
        }

        private bool ExtensaoXmlPdf(string nomeArquivo)
        {
            string extensaoArquivo = System.IO.Path.GetExtension(nomeArquivo);
            extensaoArquivo = (extensaoArquivo != null ? extensaoArquivo.ToLower() : "");
            return (extensaoArquivo.Contains(".xml") || extensaoArquivo.Contains(".pdf"));
        }

        private void GravarLogExclusaoEmail(Action<LogReadEmailEdi> metodoLogEmail, string assunto, string passo)
        {
            var logEmailErro = new LogReadEmailEdi()
            {
                DataInicioLog = DateTime.Now,
                DataRecebimentoEmail = DateTime.Now,
                IdEmailExchange = String.Empty,
                Remetente = "Exclusão de emails das pastas secundárias",
                AssuntoEmail = assunto,
                MensagemErro = passo,
                DataFimLog = DateTime.Now
            };

            ExecutarMetodoLogEmail(metodoLogEmail, logEmailErro);
        }

        private List<Attachment> ObterAnexos(EmailMessage email, string tipo)
        {
            return email.Attachments.Where(a => Path.GetExtension(a.Name.ToLower()) == tipo.ToLower() && (ExtensaoXmlPdf(a.Name))).ToList();
        }
    }
}