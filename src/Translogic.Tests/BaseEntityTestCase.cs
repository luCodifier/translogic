/*namespace Translogic.Tests
{
	using System.Collections.Generic;
	// using Modules.Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class BaseEntityTestCase
	{
		[Test]
		public void Equality()
		{
			var a = new Usuario { Id = 1 };
			var b = new Usuario { Id = 2 };
			var c = new Usuario { Id = 1 };

			Assert.AreEqual(a, c);
			Assert.AreNotEqual(a, b);
		}

		[Test]
		public void ListEquality()
		{
			var list = new List<Usuario>();

			var a = new Usuario { Id = 1 };
			var b = new Usuario { Id = 2 };
			var c = new Usuario { Id = 1 };

			Assert.IsFalse(list.Contains(a));
			Assert.IsFalse(list.Contains(b));
			Assert.IsFalse(list.Contains(c));

			list.Add(a);

			Assert.IsTrue(list.Contains(a));
			Assert.IsFalse(list.Contains(b));
			Assert.IsTrue(list.Contains(c));
			Assert.AreEqual(1, list.Count);

			list.Add(b);

			Assert.IsTrue(list.Contains(a));
			Assert.IsTrue(list.Contains(b));
			Assert.IsTrue(list.Contains(c));
			Assert.AreEqual(2, list.Count);
		}

		[Test]
		public void HashEquality()
		{
			var hash = new HashSet<Usuario>();

			var a = new Usuario { Id = 1 };
			var b = new Usuario { Id = 2 };
			var c = new Usuario { Id = 1 };

			hash.Add(a);
			hash.Add(b);
			hash.Add(c);

			Assert.IsTrue(hash.Contains(a));
			Assert.IsTrue(hash.Contains(b));
			Assert.IsTrue(hash.Contains(c));
			Assert.AreEqual(2, hash.Count);
		}
	}
}*/