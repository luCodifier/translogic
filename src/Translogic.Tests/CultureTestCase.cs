namespace Translogic.Tests
{
	using System;
	using System.Globalization;
	using System.Threading;
	using NUnit.Framework;

	[TestFixture]
	public class CultureTestCase
	{
		[Test]
		public void DisplayNames()
		{
			var pt_BR = new CultureInfo("pt-BR");
			var es_AR = new CultureInfo("es-AR");
			var en_US = new CultureInfo("en-US");

			Thread.CurrentThread.CurrentUICulture = pt_BR;

			Console.WriteLine(pt_BR.DisplayName);
			Console.WriteLine(es_AR.DisplayName);
			Console.WriteLine(en_US.DisplayName);

			Thread.CurrentThread.CurrentUICulture = es_AR;

			Console.WriteLine(pt_BR.DisplayName);
			Console.WriteLine(es_AR.DisplayName);
			Console.WriteLine(en_US.DisplayName);

			Thread.CurrentThread.CurrentUICulture = en_US;

			Console.WriteLine(pt_BR.DisplayName);
			Console.WriteLine(es_AR.DisplayName);
			Console.WriteLine(en_US.DisplayName);
		}
	}
}