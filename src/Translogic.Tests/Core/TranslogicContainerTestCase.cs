namespace Translogic.Tests.Core
{
	using System;
	using System.Collections;
	using ALL.Core.Dominio.Services;
	using Castle.Core.Logging;
	using Castle.MicroKernel.Registration;
	using Castle.Services.Transaction;
	// using Modules.Core.Domain.Model.CadastroUnicoEstacoes;
	// using Modules.Core.Domain.Model.Diversos;
	using NHibernate;
	using NUnit.Framework;
	using ITransaction=Castle.Services.Transaction.ITransaction;

	[TestFixture]
	public sealed class TranslogicContainerTestCase : BaseTranslogicContainerTestCase
	{
		[Test]
		public void CanUseATMFacility()
		{
			Container.Register(
				Component.For<ServiceWithTransaction>()
				);

			ServiceWithTransaction serviceWithTransaction = Container.Resolve<ServiceWithTransaction>();

			try
			{
				serviceWithTransaction.FaultDeclarativeTransaction();
			}
			catch
			{
			}

			serviceWithTransaction.SuccessfulInlineTransaction();
		}

		[Test]
		public void CanUseLogFacility()
		{
			Container.Register(
				Component.For<ServiceWithLog>()
				);

			ServiceWithLog serviceWithLog = Resolve<ServiceWithLog>();

			Assert.IsNotNull(serviceWithLog.Logger);
		}

		[Test]
		public void CanUseNHFacility()
		{
			Container.Register(
				Component.For<ServiceWithNHibernate>()
				);

			ServiceWithNHibernate ServiceWithNHibernate = Resolve<ServiceWithNHibernate>();

			ServiceWithNHibernate.SomeBasicDatabaseOperation();
		}

		/*[Test]
		public void ResolveWithGenericType()
		{
			Type type = typeof (BaseCrudService<,>)
				.MakeGenericType(new[] {typeof (Estacao), typeof (int)});

			Array services = Container.ResolveAll(type);

			Assert.IsNotNull(services);
		}*/
	}

	public class ServiceWithLog
	{
		#region PROPRIEDADES

		public ILogger Logger { get; set; }

		#endregion
	}

	[Transactional]
	public class ServiceWithTransaction
	{
		#region PROPRIEDADES

		public ITransactionManager TransactionManager { get; set; }

		#endregion

		#region M�TODOS

		[Transaction]
		public virtual void FaultDeclarativeTransaction()
		{
			throw new Exception();
		}

		public void SuccessfulInlineTransaction()
		{
			ITransaction tx = null;

			try
			{
				tx = TransactionManager.CreateTransaction(TransactionMode.Requires, IsolationMode.ReadCommitted);

				tx.Begin();

				Console.WriteLine("Some transaction required code");

				tx.Commit();
			}
			catch (Exception e)
			{
				if (tx != null)
				{
					Console.WriteLine(e);
					tx.Rollback();
				}
			}
		}

		#endregion
	}

	public class ServiceWithNHibernate
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly ISessionFactory sessionFactory;

		#endregion

		#region CONSTRUTORES

		public ServiceWithNHibernate(ISessionFactory sessionFactory)
		{
			this.sessionFactory = sessionFactory;
		}

		#endregion

		#region M�TODOS

		public void SomeBasicDatabaseOperation()
		{
			using (ISession session = sessionFactory.OpenSession())
			{
				// IList atividades = session.CreateCriteria(typeof (Atividade)).List();
			}
		}

		#endregion
	}
}