namespace Translogic.Tests.DataAccess.Mappings
{
	// using Modules.Core.Domain.Model.Acesso;
	// using Modules.TPCCO.Domain.Model;
	using NHibernate.Bytecode;
	using NHibernate.Cfg;
	using NHibernate.Dialect;
	using NHibernate.Driver;
	using NHibernate.Tool.hbm2ddl;
	using NUnit.Framework;
      
	[TestFixture]
	public class SchemaExportTestCase
	{
		[Test]
		public void ConsegueCriar()
		{
			Configuration cfg = new Configuration();

			cfg.Properties["connection.driver_class"] = typeof(OracleClientDriver).AssemblyQualifiedName;
			cfg.Properties["dialect"] = typeof(Oracle9iDialect).AssemblyQualifiedName;
			cfg.Properties["proxyfactory.factory_class"] = typeof(DefaultProxyFactoryFactory).AssemblyQualifiedName;

			cfg.Properties["connection.connection_string"] = "Data Source=tlferro-homologacao;User ID=translogic;Password=engesis;";

			// cfg.AddAssembly(typeof(MeuTranslogic).Assembly);
			// cfg.AddAssembly(typeof(Parada).Assembly);

			cfg.BuildSessionFactory();

			var schemaExport = new SchemaExport(cfg);

			schemaExport.Create(true, false);
		}
	}
}
