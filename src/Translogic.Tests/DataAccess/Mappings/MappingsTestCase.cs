namespace Translogic.Tests.DataAccess.Mappings
{
	using System;
	using System.Reflection;
	using Modules.Core.Domain.Model.Acesso;
	using NHibernate.Bytecode;
	using NHibernate.Cfg;
	using NHibernate.Dialect;
	using NHibernate.Driver;
	using NHibernate.Mapping;
	using NUnit.Framework;

	[TestFixture]
	public class MappingsTestCase
	{
		[Test]
		public void VerifyAllMappings()
		{
			Configuration cfg = new Configuration();

			cfg.Properties["connection.driver_class"] = typeof(OracleClientDriver).AssemblyQualifiedName;
			cfg.Properties["dialect"] = typeof(Oracle9iDialect).AssemblyQualifiedName;
			cfg.Properties["proxyfactory.factory_class"] = typeof(DefaultProxyFactoryFactory).AssemblyQualifiedName;

			// TODO: Get ConnectionString from config
			// cfg.Properties["connection.connection_string"] = "Data Source=translogic_11;User ID=translogic;Password=engesis;";
			cfg.Properties["connection.connection_string"] = "Data Source=engesis-prod;User ID=translogic;Password=engesis;";
			// cfg.Properties["connection.connection_string"] = "Data Source=HOM-CONFIG;User ID=config;Password=config;";

			cfg.AddAssembly(typeof(Usuario).Assembly);

			cfg.BuildSessionFactory();

			Assert.AreNotEqual(0, cfg.ClassMappings.Count);
			Assert.AreNotEqual(0, cfg.CollectionMappings.Count);

			foreach (PersistentClass item in cfg.ClassMappings)
			{
				try
				{
					Type itemType = Type.GetType(item.ClassName);

					if (itemType.IsInterface)
					{
						Console.WriteLine("Warning! Properties of interface {0} not verified.", itemType.Name);
						continue;
					}

					foreach (Property property in item.UnjoinedPropertyIterator)
					{
						PropertyInfo memberInfo;
						try
						{
							memberInfo = itemType.GetProperty(property.Name);
						}
						catch (Exception)
						{
							memberInfo = itemType.GetProperty(property.Name, property.Type.ReturnedClass);
						}
						
						Assert.IsNotNull(memberInfo, "Class: {0} - Property: {1}", item.ClassName, property.Name);
					}
				}
				catch (Exception e)
				{
					Console.Write(e.Message);
					throw;
				}
			}
		}
	}
}
