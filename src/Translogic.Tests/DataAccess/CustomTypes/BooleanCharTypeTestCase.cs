/*namespace Translogic.Tests.DataAccess.CustomTypes
{
	using Modules.Core.Domain.DataAccess.Repositories.Acesso;
	using Modules.Core.Domain.Model.Acesso;
	using ALL.TestsSupport.AcessoDados;
	using NUnit.Framework;

	[TestFixture]
	public class BooleanCharTypeTestCase : CrudRepositoryTestCase<MenuRepository, Menu, int>
	{
		protected override void PopularParaInsercao(Menu entidade)
		{
			entidade.CodigoTela = 1;
			entidade.Path = "/";
			entidade.Chave = "Home";
			entidade.Ativo = false;
		}

		protected override void PopularParaAtualizacao(Menu entidade)
		{
			entidade.CodigoTela = 1;
			entidade.Path = "/";
			entidade.Chave = "Home";
			entidade.Ativo = true;
		}

		protected override void VerificarIgualdade(Menu entidade, Menu recuperada)
		{
			Assert.AreEqual(entidade.Ativo, recuperada.Ativo);
			Assert.AreEqual(entidade.Chave, recuperada.Chave);
			Assert.AreEqual(entidade.CodigoTela, recuperada.CodigoTela);
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Legado, recuperada.Legado);
			Assert.AreEqual(entidade.Ordem, recuperada.Ordem);
			Assert.AreEqual(entidade.Path, recuperada.Path);
			Assert.AreEqual(entidade.Titulo, recuperada.Titulo);
			Assert.AreEqual(entidade.Transacao, recuperada.Transacao);
		}
	}
}*/