namespace Translogic.Tests
{
	using ALL.Core.IoC;
	using ALL.TestsSupport.IoC;
	using Translogic.Core;
	using Translogic.Core.Infrastructure.Validation;
    using Translogic.Modules.Core.Spd.Data.Enums;

    public abstract class BaseTranslogicContainerTestCase : ContainerBaseTestCase
	{
		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();
            Sis.Inicializar(EnumSistema.Testes, "Job");

            validatorEngine = Resolve<IValidatorEngine>();
		}

		#endregion

		protected IValidatorEngine validatorEngine;

		protected override ContainerBase ObterContainer()
		{
			TranslogicStarter.Initialize();

			TranslogicStarter.SetupForTests();

			return TranslogicStarter.Container;
		}
	}
}