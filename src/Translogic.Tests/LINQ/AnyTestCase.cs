namespace Translogic.Tests.LINQ
{
	using System.Linq;
	using NUnit.Framework;

	[TestFixture]
	public class AnyTestCase
	{
		[Test]
		public void Any()
		{
			bool[] items = new[] {true, true, false};
			bool[] items2 = new[] {false, true, true};
			bool[] items3 = new[] {true, false, true};
			bool[] items4 = new[] {true, true, true};
			bool[] items5 = new[] {false, false, false};

			Assert.IsTrue(items.Any(b => !b));
			Assert.IsTrue(items2.Any(b => !b));
			Assert.IsTrue(items3.Any(b => !b));
			Assert.IsFalse(items4.Any(b => !b));
			Assert.IsTrue(items5.Any(b => !b));
		}
	}
}