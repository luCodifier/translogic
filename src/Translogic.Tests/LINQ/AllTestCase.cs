namespace Translogic.Tests.LINQ
{
	using System.Linq;
	using NUnit.Framework;

	[TestFixture]
	public class AllTestCase
	{
		[Test]
		public void All()
		{
			bool[] items = new[] {true, true, false};
			bool[] items2 = new[] {false, true, true};
			bool[] items3 = new[] {true, false, true};
			bool[] items4 = new[] {true, true, true};
			bool[] items5 = new[] {false, false, false};

			Assert.IsFalse(items.All(b => b));
			Assert.IsFalse(items2.All(b => b));
			Assert.IsFalse(items3.All(b => b));
			Assert.IsTrue(items4.All(b => b));
			Assert.IsFalse(items5.All(b => b));
		}
	}
}