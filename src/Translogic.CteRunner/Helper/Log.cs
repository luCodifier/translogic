﻿namespace Translogic.CteRunner.Helper
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// Objeto para gerenciar o log enviado ao eventviewer do windows.
    /// </summary>
    public static class Log
    {
        private const string Source = "Translogic";

        /// <summary>
        /// Incluir um log no eventviewer de informação
        /// </summary>
        /// <param name="className">Classe origem</param>
        /// <param name="methodName">Método origem</param>
        /// <param name="description">descrição da informação no log</param>
        public static void SetInformation(string className, string methodName, string description)
        {
            description = GetDescription(className, methodName, description);
            Sis.LogTrace(description);
        }

        /// <summary>
        /// Incluir um log no eventviewer de atenção
        /// </summary>
        /// <param name="className">Classe origem</param>
        /// <param name="methodName">Método origem</param>
        /// <param name="description">descrição da atenção no log</param>
        public static void SetWarning(string className, string methodName, string description)
        {
            description = GetDescription(className, methodName, description);
            Sis.LogWarning(description);
        }

        /// <summary>
        /// Incluir um log no eventviewer de error
        /// </summary>
        /// <param name="className">Classe origem</param>
        /// <param name="methodName">Método origem</param>
        /// <param name="ex">Exception de erro</param>
        public static void SetError(string className, string methodName, Exception ex)
        {
            var description = GetDescription(className, methodName, ex.Message);
            Sis.LogException2(ex, description);
        }

        private static string GetDescription(string className, string methodName, string description)
        {
            description = string.Format("{0} - {1}: {2}", className, methodName, description);
            return description;
        }
    }
}
