﻿namespace Translogic.CteRunner
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;
    using Translogic.CteRunner.Helper;
    using Translogic.CteRunner.Services;
    using Translogic.CteRunner.Services.Commands;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Serviço de configuração do cterunner
    /// </summary>
    public class CteRunnerSetupService
    {
        private const string ClassName = "CteRunnerSetupService";

        private static bool _senderConfigService;
        private static bool _receiverConfigService;
        private static bool _senderInterfaceSapService;
        private static bool _importerPdfFileService;
        private static bool _importerXmlFileService;
        private static bool _senderEmailService;
        private static bool _efetuarLimpezaDiretorioService;

        /// <summary>
        /// Método chamado na inicialização do serviço
        /// </summary>
        /// <param name="carregarServicos">Se carrega os serviços ou se apenas configura</param>
        public void Start(bool carregarServicos)
        {
            try
            {
                TranslogicStarter.Initialize();
                TranslogicStarter.SetupForJobRunner();
                TranslogicContainer container = TranslogicStarter.Container;
                RegistrarComponentes(TranslogicStarter.Container);

                // Inicializa as variaveis
                ////Initialize();

                Log.SetInformation(ClassName, "Start", "Chama métodos...");

                InitializeServices(container, carregarServicos);
            }
            catch (Exception ex)
            {
                Log.SetError(ClassName, "Start", ex);
            }
        }

        /// <summary>
        /// Método chamado no término do serviço
        /// </summary>
        public void Stop()
        {
            TranslogicStarter.Container.Dispose();
            Log.SetWarning(ClassName, "Stop", "Serviço interrompido.");
        }

        private static void InitializeServices(TranslogicContainer container, bool carregarServicos)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                _senderConfigService = Boolean.Parse(config.AppSettings.Settings["SenderConfigService"].Value);
                _receiverConfigService = Boolean.Parse(config.AppSettings.Settings["ReceiverConfigService"].Value);
                _senderInterfaceSapService = Boolean.Parse(config.AppSettings.Settings["SenderInterfaceSapService"].Value);
                _importerPdfFileService = Boolean.Parse(config.AppSettings.Settings["ImporterPdfFileService"].Value);
                _importerXmlFileService = Boolean.Parse(config.AppSettings.Settings["ImporterXmlFileService"].Value);
                _senderEmailService = Boolean.Parse(config.AppSettings.Settings["SenderEmailService"].Value);
                _efetuarLimpezaDiretorioService = Boolean.Parse(config.AppSettings.Settings["EfetuarLimpezaDiretorioService"].Value);
            }
            catch (Exception ex)
            {
                Log.SetError(ClassName, "InitializeServices", ex);
            }

            //// REMOVI OS LOGS DE BANCO POR ENQUANTO ATE DESCOBRIR O PQ DA Object reference not set to an instance of an object. QUANDO RODA O CTERUNNER NO SERVERS
            ////var cteRunnerLogService = container.Resolve<CteRunnerLogService>();
            ////cteRunnerLogService.InserirLogInfo("CteRunnerService", "Iniciado o serviço CteRunner");
            ////

            if (carregarServicos)
            {
                if (_senderConfigService)
                {
                    // Inicia gerente de sender
                    SenderQueueManagerService senderService = container.Resolve<SenderQueueManagerService>();
                    senderService.Inicializar();
                }

                if (_receiverConfigService)
                {
                    ReceiverManagerService receiverManagerService = container.Resolve<ReceiverManagerService>();
                    receiverManagerService.Inicializar(true);
                }

                if (_senderInterfaceSapService)
                {
                    SenderManagerInterfaceSapService senderInterfaceSap =
                        container.Resolve<SenderManagerInterfaceSapService>();
                    senderInterfaceSap.Inicializar();
                }

                if (_importerPdfFileService)
                {
                    ImporterPdfFileManagerService importerPdfFile =
                        container.Resolve<ImporterPdfFileManagerService>();
                    importerPdfFile.Inicializar(true);
                }

                if (_importerXmlFileService)
                {
                    ImporterXmlFileManagerService importerXmlFile =
                        container.Resolve<ImporterXmlFileManagerService>();
                    importerXmlFile.Inicializar();
                }

                if (_senderEmailService)
                {
                    SenderEmailManagerService senderEmail = container.Resolve<SenderEmailManagerService>();
                    senderEmail.Inicializar();
                }

                if (_efetuarLimpezaDiretorioService)
                {
                    DirectoryFileManagerService limpezaDiretorioCteService =
                        container.Resolve<DirectoryFileManagerService>();
                    limpezaDiretorioCteService.Inicializar();
                }
            }
        }

        /////// <summary>
        /////// Inicializa com as variaveis do ambiente (arquivo de configuração)
        /////// </summary>
        ////protected void Initialize()
        ////{
        ////    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        ////    _senderConfigService = Boolean.Parse(config.AppSettings.Settings["SenderConfigService"].Value);
        ////    _receiverConfigService = Boolean.Parse(config.AppSettings.Settings["ReceiverConfigService"].Value);
        ////    _senderInterfaceSapService = Boolean.Parse(config.AppSettings.Settings["SenderInterfaceSapService"].Value);
        ////    _importerPdfFileService = Boolean.Parse(config.AppSettings.Settings["ImporterPdfFileService"].Value);
        ////    _importerXmlFileService = Boolean.Parse(config.AppSettings.Settings["ImporterXmlFileService"].Value);
        ////    _senderEmailService = Boolean.Parse(config.AppSettings.Settings["SenderEmailService"].Value);
        ////    _efetuarLimpezaDiretorioService = Boolean.Parse(config.AppSettings.Settings["EfetuarLimpezaDiretorioService"].Value);
        ////}

        private void RegistrarComponentes(TranslogicContainer container)
        {
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFA").LifeStyle.PerThread);
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteCancelamentoCommand>().Named("EFC").LifeStyle.PerThread);
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteInutilizacaoCommand>().Named("EFI").LifeStyle.PerThread);
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteComplementoCommand>().Named("EFT").LifeStyle.PerThread);
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCorrecaoCteCommand>().Named("ECC").LifeStyle.PerThread);

            ////container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoContribuinteCommand>().Named("EFN").LifeStyle.PerThread);
            ////container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoNContribuinteCommand>().Named("EFR").LifeStyle.PerThread);

            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFN").LifeStyle.PerThread);
            container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFR").LifeStyle.PerThread);

            container.Register(Component.For<SenderQueueManagerService>().LifeStyle.PerThread);
            container.Register(Component.For<ReceiverManagerService>().LifeStyle.PerThread);
            container.Register(Component.For<SenderManagerInterfaceSapService>().LifeStyle.PerThread);
            container.Register(Component.For<ImporterPdfFileManagerService>().LifeStyle.PerThread);
            container.Register(Component.For<ImporterXmlFileManagerService>().LifeStyle.PerThread);
            container.Register(Component.For<SenderEmailManagerService>().LifeStyle.PerThread);
            container.Register(Component.For<CteRunnerLogService>().ImplementedBy<CteRunnerLogService>());
            container.Register(Component.For<ICteRunnerLogRepository>().ImplementedBy<CteRunnerLogRepository>());
            container.Register(Component.For<DirectoryFileManagerService>());
        }
    }
}