﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.CteRunner.Models
{
    
    /// <summary>
    /// Enum com os serviços da solução
    /// </summary>
    public enum EnumService
    {
        SenderConfigService,
        ReceiverConfigService,
        SenderInterfaceSapService,
        ImporterPdfFileService,
        ImporterXmlFileService,
        SenderEmailService,
        EfetuarLimpezaDiretorioService
    }

}
