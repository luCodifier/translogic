namespace Translogic.CteRunner.Models
{
	/// <summary>
	/// Classe de configuração de FTP
	/// </summary>
	public class ConfiguracaoFtp
	{
		/// <summary>
		/// Gets or sets Servidor.
		/// </summary>
		public string Servidor { get; set; }

		/// <summary>
		/// Gets or sets Usuario.
		/// </summary>
		public string Usuario { get; set; }

		/// <summary>
		/// Gets or sets Senha.
		/// </summary>
		public string Senha { get; set; }
	}
}