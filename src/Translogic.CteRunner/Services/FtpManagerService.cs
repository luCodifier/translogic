namespace Translogic.CteRunner.Services
{
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.Net;
	using System.Threading;
	using System.Timers;
	using Castle.Windsor;
	using Timer = System.Timers.Timer;

	/// <summary>
	/// Servi�o que gerencia o funcionamento de obten��o dos arquivos de FTP pela NDD (N�o utilizado)
	/// </summary>
	public class FtpManagerService
	{
		private const string NomeIntervaloTimer = "IntervaloTimerFtpEmSegundos";
		private readonly FtpCteService _ftpCteService;
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly WindsorContainer _container;
		private Timer _timer;

		/// <summary>
		/// Initializes a new instance of the <see cref="FtpManagerService"/> class.
		/// </summary>
		/// <param name="container">
		/// The container.
		/// </param>
		public FtpManagerService(WindsorContainer container)
		{
			_container = container;
			_ftpCteService = _container.Resolve<FtpCteService>();
			_cteRunnerLogService = container.Resolve<CteRunnerLogService>();
			_cteRunnerLogService.InserirLogInfo("FtpManagerService", "criado o servi�o");
		}

		/// <summary>
		/// Inicializa o timer
		/// </summary>
		public void Inicializar()
		{
			int tempoSegundos;
			try
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
				Int32.TryParse(aux, out tempoSegundos);
				if (tempoSegundos.Equals(0))
				{
					tempoSegundos = 30;
				}
			}
			catch (Exception)
			{
				tempoSegundos = 3;
			}

			_timer = new Timer();
			_timer.Interval = tempoSegundos * 1000;
			_timer.Elapsed += OnTimerElapsed;
			_timer.Enabled = true;

			_cteRunnerLogService.InserirLogInfo("FtpManagerService", string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
		}

		/// <summary>
		/// Executa o recebimento do Ftp
		/// </summary>
		public void ExecutarRecebimentoFtp()
		{
			try
			{
				_cteRunnerLogService.InserirLogInfo("FtpManagerService", "executando o processo de recebimento dos arquivos");
				_timer.Enabled = false;

				_ftpCteService.ProcessarDadosFtp();
			}
			catch (Exception ex)
			{
				_cteRunnerLogService.InserirLogErro("FtpManagerService", string.Format("{0}", ex.Message), ex);
			}
			finally
			{
				_timer.Enabled = true;
			}
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			ExecutarRecebimentoFtp();
		}
	}
}