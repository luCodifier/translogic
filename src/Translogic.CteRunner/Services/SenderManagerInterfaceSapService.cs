namespace Translogic.CteRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.CteRunner.Helper;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Servi�o de gerenciamento de envio dos arquivos para interface SAP
    /// </summary>
    public class SenderManagerInterfaceSapService
    {
        private const string NomeIntervaloTimer = "IntervaloTimerInterfaceSap";
        private const string ChaveIndAmbienteSefaz = "IndAmbienteSefaz";
        private const string ClassName = "SenderManagerInterfaceSapService";
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
        private readonly IWindsorContainer _container;
        private bool _executando;
        private int _indAmbienteSefaz;
        private int _tempoSegundos;

        /// <summary>
        /// Initializes a new instance of the <see cref="SenderManagerInterfaceSapService"/> class.
        /// </summary>
        /// <param name="cteService"> Servi�o do Cte Injetado</param>
        /// <param name="cteRunnerLogService">Servi�o do log do cte runner injetado </param>
        /// <param name="container"> Container injetado </param>
        /// <param name="cteInterfaceEnvioSapRepository">Reposit�rio de interface de envio de cte para o SAP injetado</param>
        public SenderManagerInterfaceSapService(CteService cteService, CteRunnerLogService cteRunnerLogService, IWindsorContainer container, ICteInterfaceEnvioSapRepository cteInterfaceEnvioSapRepository)
        {
            _container = container;
            _cteService = cteService;
            _cteRunnerLogService = cteRunnerLogService;
            _cteInterfaceEnvioSapRepository = cteInterfaceEnvioSapRepository;
            ////_cteRunnerLogService.InserirLogInfo(ClassName, "criado o servi�o");
        }

        /// <summary>
        /// Inicializa o timer
        /// </summary>
        public void Inicializar()
        {
            ////int tempoSegundos;
            ////try
            ////{
            ////    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ////    string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
            ////    Int32.TryParse(aux, out tempoSegundos);
            ////    if (tempoSegundos.Equals(0))
            ////    {
            ////        tempoSegundos = 30;
            ////    }

            ////    aux = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value;
            ////    if (!Int32.TryParse(aux, out _indAmbienteSefaz))
            ////    {
            ////        _indAmbienteSefaz = 2;
            ////    }
            ////}
            ////catch (Exception)
            ////{
            ////    tempoSegundos = 30;
            ////    _indAmbienteSefaz = 2;
            ////}

            ////_executando = false;

            InicializarParametros();
            new ThreadIntervalSenderSapManager(ExecutarEnvioSingleThread, TimeSpan.FromSeconds(_tempoSegundos)).Start();

            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", _tempoSegundos));
        }

        /// <summary> 
        /// Inicializa os parametros
        /// </summary>
        public void InicializarParametros()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out _tempoSegundos);
                if (_tempoSegundos.Equals(0))
                {
                    _tempoSegundos = 30;
                    Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }

                aux = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value;
                if (!Int32.TryParse(aux, out _indAmbienteSefaz))
                {
                    _indAmbienteSefaz = 2;
                    Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER InicializarParametros: N�o foi poss�vel atribuir o ambiente sefaz, atualizado para 2");
                }
            }
            catch (Exception ex)
            {
                _tempoSegundos = 30;
                _indAmbienteSefaz = 2;
                Log.SetError(ClassName, "InicializarParametros", ex);
            }

            _executando = false;
        }

        /// <summary>
        /// Executa o recebimento (processamento do envio dos Ctes para o SAP) - Single Thread
        /// </summary>
        public void ExecutarEnvioSingleThread()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                _cteRunnerLogService.InserirLogInfo(ClassName, "executando o processo de varredura dos dados");
                _executando = true;

                string hostName = Dns.GetHostName();

#if DEBUG
                hostName = "CRW-VWPS01";
#endif

                IList<CteInterfaceEnvioSap> listaCtesNaoProcessados = _cteService.ObterCtesProcessarEnvioSap(_indAmbienteSefaz);

#if DEBUG
                if (!listaCtesNaoProcessados.Any())
                {
                    var interf = _cteInterfaceEnvioSapRepository.ObterPorId(7338976);
                    listaCtesNaoProcessados.Add(interf);
                }
#endif

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("SenderManagerInterfaceSapService: enviando para o pooling de envio do SAP {0} Ctes", listaCtesNaoProcessados.Count));
                _cteService.InserirPoolingEnvioSapCte(listaCtesNaoProcessados, hostName);

                IList<CteSapPooling> listaProcessamento = _cteService.ObterPoolingEnvioSapPorServidor(hostName);
                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("obtendo do pooling de envio para o SAP {0} Ctes para serem processados", listaProcessamento.Count));

                Log.SetInformation(ClassName, "ExecutarEnvioSingleThread", string.Format("Foram identificados {0} registros para processar", listaProcessamento.Count));

                foreach (CteSapPooling cteSapPooling in listaProcessamento)
                {
                    try
                    {
                        _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("criando e colocando no pool a thread para o cte: {0} ", cteSapPooling.Id));

                        ProcessoEnvioInterfaceSap envioInterfaceSap = new ProcessoEnvioInterfaceSap(_container, _cteRunnerLogService);
                        envioInterfaceSap.Executar(cteSapPooling);
                    }
                    catch (Exception ex)
                    {
                        _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                        Log.SetError(ClassName, "ExecutarEnvioSingleThread", ex);
                    }
                }

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("removendo do pooling de envio para o SAP {0} Ctes processados", listaProcessamento.Count));
                _cteService.RemoverPoolingEnvioSapCte(listaProcessamento);
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                Log.SetError(ClassName, "ExecutarEnvioSingleThread", ex);
            }
            finally
            {
                _executando = false;
            }
        }
    }

    /// <summary>
    /// Executa o delegate a cada intervalo
    /// </summary>
    public class ThreadIntervalSenderSapManager
    {
        private readonly BackgroundWorker _worker;
        private bool _executarImediatamente;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        /// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
        /// <param name="interval">Intervalo que ser� executado o delegate</param>
        public ThreadIntervalSenderSapManager(Action delegateInterval, TimeSpan interval)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;

            _worker.DoWork += delegate
            {
                if (_executarImediatamente)
                {
                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }

                while (true)
                {
                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    Thread.Sleep(interval);

                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }
            };
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        public void Start()
        {
            Start(false);
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        /// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
        public void Start(bool executarImediatamente)
        {
            _executarImediatamente = executarImediatamente;

            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancela a execu��o
        /// </summary>
        public void Stop()
        {
            _worker.CancelAsync();
        }
    }
}