namespace Translogic.CteRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using Castle.Windsor;
    using Commands;
    using Core.Commons;
    using Modules.Core.Domain.Model.Diversos.Cte;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Speed.Common;
    using Translogic.CteRunner.Helper;

    /// <summary>
    /// Servi�o de enfileiramento de gera��o/envio de ctes
    /// </summary>
    public class SenderQueueManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloTimerEnvioEmSegundos";
        private const string ChaveIndAmbienteSefaz = "IndAmbienteSefaz";
        private const string ClassName = "SenderQueueManagerService";
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly IWindsorContainer _container;
        private bool _executando;
        private int _indAmbienteSefaz;
        private int _tempoSegundos;

        /// <summary>
        /// Initializes a new instance of the <see cref="SenderQueueManagerService"/> class.
        /// </summary>
        /// <param name="cteService"> Servi�o do Cte injetado </param>
        /// <param name="cteRunnerLogService"> Servi�o de log do cte runner injetado </param>
        /// <param name="container"> Container injetado</param>
        public SenderQueueManagerService(CteService cteService, CteRunnerLogService cteRunnerLogService, IWindsorContainer container)
        {
            _cteService = cteService;
            _cteRunnerLogService = cteRunnerLogService;
            _container = container;
            ////_cteRunnerLogService.InserirLogInfo(ClassName, "criado o servi�o");
        }

        /// <summary> 
        /// Inicializa o timer
        /// </summary>
        public void Inicializar()
        {
            InicializarParametros();
            new ThreadIntervalSenderQueueManager(ExecutarGeracaoSingleThread, TimeSpan.FromSeconds(_tempoSegundos)).Start();
            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", _tempoSegundos));
        }

        /// <summary> 
        /// Inicializa os parametros
        /// </summary>
        public void InicializarParametros()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                try
                {
                    _tempoSegundos = config.AppSettings.Settings[NomeIntervaloTimer].Value.ToInt32();
                    if (_tempoSegundos == 0)
                    {
                        _tempoSegundos = 30;
                        Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                    }
                }
                catch
                {
                    _tempoSegundos = 30;
                    Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }

                try
                {
                    _indAmbienteSefaz = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value.ToInt32();
                    if (_indAmbienteSefaz == 0)
                    {
                        _indAmbienteSefaz = 2;
                        Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER: N�o foi poss�vel atribuir o ambiente sefaz, atualizado para 2");
                    }
                }
                catch
                {
                    _indAmbienteSefaz = 2;
                    Log.SetWarning(ClassName, "InicializarParametros", "CTE RUNNER: N�o foi poss�vel atribuir o ambiente sefaz, atualizado para 2");
                }
            }
            catch (Exception ex)
            {
                _tempoSegundos = 30;

                Log.SetError(ClassName, "InicializarParametros", ex);
            }

            _executando = false;
        }

        /// <summary>
        /// Executa a gera��o
        /// </summary>
        public void ExecutarGeracaoMultiThread()
        {
            /*
            try
            {
                _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", "executando o processo de varredura dos dados");
                _timer.Enabled = false;

                string hostName = Dns.GetHostName();
                IList<Cte> listaCtesNaoProcessados = _cteService.ObterCtesProcessarEnvio(_indAmbienteSefaz);
                _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", string.Format("enviando para o pooling de envio {0} Ctes", listaCtesNaoProcessados.Count));
                _cteService.InserirPoolingEnvioCte(listaCtesNaoProcessados, hostName);

                IList<Cte> listaProcessamento = _cteService.ObterPoolingEnvioPorServidor(hostName);
                _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", string.Format("obtendo do pooling de envio {0} Ctes para serem processados", listaProcessamento.Count));
                if (listaProcessamento.Count > 0)
                {
                    ManualResetEvent[] doneEvents = new ManualResetEvent[listaProcessamento.Count];

                    int i = 0;
                    foreach (Cte cte in listaProcessamento)
                    {
                        try
                        {
                            doneEvents[i] = new ManualResetEvent(false);

                            _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", cte.Id));

                            string statusCte = Enum<SituacaoCteEnum>.GetDescriptionOf(cte.SituacaoAtual);
                            ICommandCte commandCte = _container.Resolve<ICommandCte>(statusCte);

                            ProcessoGeracaoArquivo geracaoArquivo = new ProcessoGeracaoArquivo(cte, commandCte, doneEvents[i], _cteRunnerLogService);
                            ThreadPool.QueueUserWorkItem(geracaoArquivo.ThreadPoolCallback, i);
                        }
                        catch (Exception ex)
                        {
                            _cteRunnerLogService.InserirLogErro("SenderQueueManagerService", string.Format("{0}", ex.Message));
                        }

                        i++;
                    }

                    WaitHandle.WaitAll(doneEvents);
                    _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", string.Format("SenderQueueManagerService: removendo do pooling de envio {0} Ctes processados", listaProcessamento.Count));
                    _cteService.RemoverPoolingEnvioCte(listaProcessamento);
                }
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro("SenderQueueManagerService", string.Format("{0}", ex.Message));
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                _timer.Enabled = true;
            }
          */
        }

        /// <summary>
        /// Executa a gera��o em single thread
        /// </summary>
        public void ExecutarGeracaoSingleThread()
        {
            var tc = new TimerCount("ExecutarGeracaoSingleThread");
            if (_executando)
            {
                return;
            }

            try
            {
                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Inicio m�todo..., ambiente:" + _indAmbienteSefaz.ToString());
                ////_cteRunnerLogService.InserirLogInfo(ClassName, "executando o processo de varredura dos dados...");

                _executando = true;

                string hostName = Dns.GetHostName();

#if DEBUG
                hostName = "CRW-VWPS01";
                _indAmbienteSefaz = 1;
#endif

                IList<Cte> listaCtesNaoProcessados;

                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Vai fazer a busca ObterCtesProcessarEnvio, ambiente:" + _indAmbienteSefaz.ToString());

                tc.Next("ObterCtesProcessarEnvio");
                listaCtesNaoProcessados = _cteService.ObterCtesProcessarEnvio(_indAmbienteSefaz);

                tc.Next("Log.SetInformation");
                if (listaCtesNaoProcessados.Count == 0)
                {
                    Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Lista Vazia");
                }
                else if (listaCtesNaoProcessados.Count(x => x.Id == 5508027) > 0)
                {
                    Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Esta na Lista");
                }

                tc.Next("InserirLogInfo");
                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("enviando para o pooling de envio {0} Ctes", listaCtesNaoProcessados.Count));

                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "VAi inserir no Pooling");
                _cteService.InserirPoolingEnvioCte(listaCtesNaoProcessados, hostName);

                IList<Cte> listaProcessamento = _cteService.ObterPoolingEnvioPorServidor(hostName);
                IList<Cte> listaProcessamentoComplemento = _cteService.ObterPoolingEnvioComplementadoPorServidor(hostName);

                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", string.Format("Foram identificados {0} registros de complementos para processar", listaProcessamentoComplemento.Count));

                // Adiciona a lista de cte complementado na lista de cte para ser processado
                foreach (var cte in listaProcessamentoComplemento)
                {
                    // Garante a adi��o na lista apenas se j� n�o estiver na mesma
                    if (!listaProcessamento.Any(g => g.Id == cte.Id))
                    {
                        listaProcessamento.Add(cte);
                    }
                }

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("obtendo do pooling de envio {0} Ctes para serem processados", listaProcessamento.Count));

#if DEBUG2

                var cteTeste = _cteService.ObterCtePorId(2911819);
                try
                {
                    _cteRunnerLogService.InserirLogInfo("SenderQueueManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", cteTeste.Id));

                    string statusCte = Enum<SituacaoCteEnum>.GetDescriptionOf(cteTeste.SituacaoAtual);
                    ICommandCte commandCte = _container.Resolve<ICommandCte>(statusCte);

                    ProcessoGeracaoArquivo geracaoArquivo = new ProcessoGeracaoArquivo(cteTeste, commandCte, _cteRunnerLogService);
                    geracaoArquivo.Executar();
                }
                catch (Exception ex)
                {
                    _cteRunnerLogService.InserirLogErro("SenderQueueManagerService", string.Format("{0}", ex.Message), ex);
                }
#endif
                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", string.Format("Foram identificados {0} registros de para processar", listaProcessamento.Count));

                if (listaProcessamento.Any())
                {
                    Sis.LogTrace("Processando " + listaProcessamento.Count + " itens");

                    foreach (Cte cte in listaProcessamento)
                    {
                        try
                        {
                            Sis.LogTrace("Processando Cte: " + cte.Id);
                            tc.Next("Processando Cte: " + cte.Id);

                            _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("criando e colocando no pool a thread para o cte: {0} ", cte.Id));

                            string statusCte = Enum<SituacaoCteEnum>.GetDescriptionOf(cte.SituacaoAtual);
                            ICommandCte commandCte = _container.Resolve<ICommandCte>(statusCte);

                            ProcessoGeracaoArquivo geracaoArquivo = new ProcessoGeracaoArquivo(cte, commandCte, _cteRunnerLogService);
                            geracaoArquivo.Executar();
                        }
                        catch (Exception ex)
                        {
                            _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                            Log.SetError(ClassName, "ExecutarGeracaoSingleThread", ex);
                        }
                    }

                    Sis.LogTrace("Relat�rio de performance resumido: " + tc.ToString());
                }

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("SenderQueueManagerService: removendo do pooling de envio {0} Ctes processados", listaProcessamento.Count));
                _cteService.RemoverPoolingEnvioCte(listaProcessamento);
            }
            catch (NHibernate.Exceptions.GenericADOException e)
            {
                Log.SetError(ClassName, "ExecutarGeracaoSingleThread", e);
                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Deu erro:" + e.Message);
            }
            catch (Exception ex)
            {
                Log.SetError(ClassName, "ExecutarGeracaoSingleThread", ex);
                Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Deu erro:" + ex.Message);
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
            }
            finally
            {
                _executando = false;
            }

            Log.SetInformation(ClassName, "ExecutarGeracaoSingleThread", "Fim m�todo");
        }
    }

    /// <summary>
    /// Executa o delegate a cada intervalo
    /// </summary>
    public class ThreadIntervalSenderQueueManager
    {
        private readonly BackgroundWorker _worker;
        private bool _executarImediatamente;

        /// <summary> 
        /// Construtor padr�o
        /// </summary>
        /// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
        /// <param name="interval">Intervalo que ser� executado o delegate</param>
        public ThreadIntervalSenderQueueManager(Action delegateInterval, TimeSpan interval)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;

            _worker.DoWork += delegate
            {
                if (_executarImediatamente)
                {
                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }

                while (true)
                {
                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    Thread.Sleep(interval);

                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }
            };
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        public void Start()
        {
            Start(false);
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        /// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
        public void Start(bool executarImediatamente)
        {
            _executarImediatamente = executarImediatamente;

            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancela a execu��o
        /// </summary>
        public void Stop()
        {
            _worker.CancelAsync();
        }
    }
}
