namespace Translogic.CteRunner.Services
{
	using System;
	using System.Net;
	using System.Threading;
	using Castle.Windsor;
	using Modules.Core.Domain.Model.Acesso;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Processo de envio dos Ctes para a interface SAP
	/// </summary>
	public class ProcessoEnvioInterfaceSap
	{
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly ManualResetEvent _doneEvent;
		private readonly CteInterfaceEnvioSap _interfaceEnvioSap;
		private readonly CteService _cteService;
		private readonly GravarSapCteEnvioService _gravarSapCteEnvioService;
		private readonly ICteRepository _cteRepository;
		private readonly CteLogService _cteLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoEnvioInterfaceSap"/> class.
		/// </summary>
		/// <param name="container"> Container injetado. </param>
		/// <param name="doneEvent"> The done event. </param>
		/// <param name="cteRunnerLogService">Servi�o do log do Cte</param>
		public ProcessoEnvioInterfaceSap(IWindsorContainer container, ManualResetEvent doneEvent, CteRunnerLogService cteRunnerLogService)
		{
			_cteService = container.Resolve<CteService>();
			_gravarSapCteEnvioService = container.Resolve<GravarSapCteEnvioService>();
			_cteRepository = container.Resolve<ICteRepository>();
			_cteLogService = container.Resolve<CteLogService>();
			_cteRunnerLogService = cteRunnerLogService;
			_doneEvent = doneEvent;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoEnvioInterfaceSap"/> class.
		/// </summary>
		/// <param name="container"> Container injetado. </param>
		/// <param name="cteRunnerLogService">Servi�o do log do Cte</param>
		public ProcessoEnvioInterfaceSap(IWindsorContainer container, CteRunnerLogService cteRunnerLogService)
		{
			_cteService = container.Resolve<CteService>();
			_gravarSapCteEnvioService = container.Resolve<GravarSapCteEnvioService>();
			_cteRepository = container.Resolve<ICteRepository>();
			_cteLogService = container.Resolve<CteLogService>();
			_cteRunnerLogService = cteRunnerLogService;

			_doneEvent = null;
		}

		/// <summary>
		/// Executa no modo single thread
		/// </summary>
		/// <param name="itemPooling">Item do Pooling</param>
		public void Executar(CteSapPooling itemPooling)
		{
			try
			{
				ProcessarEnvioInterfaceSap(itemPooling);
			}
			catch (Exception ex)
			{
                Sis.LogException(ex);
				throw;
			}
		}

		private void ProcessarEnvioInterfaceSap(CteSapPooling itemPooling)
		{
		    Cte cteAux = null;
			try
			{
                cteAux = _cteRepository.ObterPorId(itemPooling.Id);
				Usuario usuario = _cteService.ObterUsuarioRobo();

				// Servi�o de grava��o nas tabelas do SAP
				_gravarSapCteEnvioService.Executar(cteAux, usuario);
			}
			catch (Exception ex)
			{
				Sis.LogException(ex);
				_cteLogService.InserirLogErro(cteAux, "ProcessarEnvioInterfaceSap", string.Format("EnviarInterfaceSap ({0}): {1}", Dns.GetHostName(), ex.Message), ex);
				throw;
			}
		}
	}
}
