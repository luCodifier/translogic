namespace Translogic.CteRunner.Services
{
	using System.Threading;
	using Castle.Windsor;
	using Modules.Core.Domain.Model.Codificador;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Processo para importar o pdf
	/// </summary>
	public class ProcessoImportacaoPdfCte
	{
		private readonly CteService _cteService;
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly ManualResetEvent _doneEvent;
		private readonly CteInterfacePdfConfig _itemInterface;
		private readonly ConfiguracaoTranslogic _pdfPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoImportacaoPdfCte"/> class.
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="doneEvent">The done event.</param>
		/// <param name="cteRunnerLogService">Serviço de log do runner</param>
		/// <param name="itemInterface">Item da interface de importação</param>
		/// <param name="pdfPath">Item de configuração do pdf path</param>
		public ProcessoImportacaoPdfCte(IWindsorContainer container, ManualResetEvent doneEvent, CteRunnerLogService cteRunnerLogService, CteInterfacePdfConfig itemInterface, ConfiguracaoTranslogic pdfPath)
		{
			_cteService = container.Resolve<CteService>();
			_doneEvent = doneEvent;
			_itemInterface = itemInterface;
			_cteRunnerLogService = cteRunnerLogService;
			_pdfPath = pdfPath;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoImportacaoPdfCte"/> class.
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="cteRunnerLogService">Serviço de log do runner</param>
		/// <param name="itemInterface">Item da interface de importação</param>
		/// /// <param name="pdfPath">Item de configuração do pdf path</param>
		public ProcessoImportacaoPdfCte(IWindsorContainer container, CteRunnerLogService cteRunnerLogService, CteInterfacePdfConfig itemInterface, ConfiguracaoTranslogic pdfPath)
		{
			_cteService = container.Resolve<CteService>();
			_doneEvent = null;
			_itemInterface = itemInterface;
			_cteRunnerLogService = cteRunnerLogService;
			_pdfPath = pdfPath;
		}

		/// <summary>
		/// Executa no modo single thread
		/// </summary>
		public void Executar()
		{
			ProcessarImportacao();
		}

		/// <summary>
		/// Wrapper method for use with thread pool.
		/// </summary>
		/// <param name="threadContext"> The thread context. </param>
		public void ThreadPoolCallback(object threadContext)
		{
			try
			{
				ProcessarImportacao();
			}
			finally
			{
				_doneEvent.Set();
			}
		}

		private void ProcessarImportacao()
		{
			if (_itemInterface != null)
			{
				_cteService.ImportarArquivoPdf(_itemInterface, _pdfPath.Valor);
			}
		}
	}
}