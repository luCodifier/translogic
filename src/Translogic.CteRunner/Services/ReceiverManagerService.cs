namespace Translogic.CteRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Diagnostics;
    using System.Net;
    using System.Threading;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.CteRunner.Helper;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Servi�o de gerenciamento de retornos de CT-e
    /// </summary>
    public class ReceiverManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloTimerRecebimentoEmSegundos";
        private const string ChaveIndAmbienteSefaz = "IndAmbienteSefaz";
        private const string ClassName = "ReceiverManagerService";
        private readonly CteService _cteService;
        private readonly ICteInterfaceRecebimentoConfigRepository _cteInterfaceRecebimentoConfigRepository;
        private readonly ICteRecebimentoPoolingRepository _cteRecebimentoPoolingRepository;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly IWindsorContainer _container;
        private bool _executando;
        private int _indAmbienteSefaz;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiverManagerService"/> class.
        /// </summary>
        /// <param name="cteService"> Servi�o do Cte Injetado</param>
        /// <param name="cteRunnerLogService">Servi�o do log do cte runner injetado </param>
        /// <param name="cteInterfaceRecebimentoConfigRepository">Servi�o do InterfaceRecebimentoConfig do cte runner injetado </param>
        /// <param name="cteRecebimentoPoolingRepository">Servi�o do CteRecebimentoPooling do cte runner injetado </param>
        /// <param name="container"> Container injetado </param>
        public ReceiverManagerService(CteService cteService, CteRunnerLogService cteRunnerLogService, ICteInterfaceRecebimentoConfigRepository cteInterfaceRecebimentoConfigRepository, ICteRecebimentoPoolingRepository cteRecebimentoPoolingRepository, IWindsorContainer container)
        {
            _container = container;
            _cteService = cteService;
            _cteRunnerLogService = cteRunnerLogService;
            _cteInterfaceRecebimentoConfigRepository = cteInterfaceRecebimentoConfigRepository;
            _cteRecebimentoPoolingRepository = cteRecebimentoPoolingRepository;
            ////_cteRunnerLogService.InserirLogInfo(ClassName, "criado o servi�o");
        }

        /// <summary>
        /// Inicializa o timer
        /// </summary>
        /// <param name="execute">Se inicia o thread de processamento</param>
        public void Inicializar(bool execute)
        {
            int tempoSegundos;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out tempoSegundos);
                if (tempoSegundos.Equals(0))
                {
                    tempoSegundos = 30;
                    Log.SetWarning(ClassName, "Inicializar", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }

                aux = config.AppSettings.Settings[ChaveIndAmbienteSefaz].Value;
                if (!Int32.TryParse(aux, out _indAmbienteSefaz))
                {
                    _indAmbienteSefaz = 2;
                    Log.SetWarning(ClassName, "Inicializar", "CTE RUNNER: N�o foi poss�vel atribuir o ambiente sefaz, atualizado para 2");
                }
            }
            catch (Exception ex)
            {
                tempoSegundos = 30;
                _indAmbienteSefaz = 2;
                Log.SetError(ClassName, "Inicializar", ex);
            }

            _executando = false;

            if (execute)
            {
                new ThreadIntervalReceiveManager(ExecutarRecebimentoSingleThread, TimeSpan.FromSeconds(tempoSegundos)).Start();
            }

            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
        }

        /// <summary>
        /// Executa o recebimento (processamento do cte de retorno da Config)
        /// </summary>
        public void ExecutarRecebimentoMultiThread()
        {
            /*
            try
            {
                _cteRunnerLogService.InserirLogInfo("ReceiverManagerService", "executando o processo de varredura dos dados");
                _timer.Enabled = false;

                string hostName = Dns.GetHostName();
                IList<CteInterfaceRecebimentoConfig> listaCtesNaoProcessados = _cteService.ObterCtesProcessarRecebimento();

                _cteRunnerLogService.InserirLogInfo("ReceiverManagerService", string.Format("ReceiverManagerService: enviando para o pooling de recebimento {0} Ctes", listaCtesNaoProcessados.Count));
                _cteService.InserirPoolingRecebimentoCte(listaCtesNaoProcessados, hostName);

                IList<CteRecebimentoPooling> listaProcessamento = _cteService.ObterPoolingRecebimentoPorServidor(hostName);
                _cteRunnerLogService.InserirLogInfo("ReceiverManagerService", string.Format("obtendo do pooling de recebimento {0} Ctes para serem processados", listaProcessamento.Count));
                if (listaProcessamento.Count > 0)
                {
                    ManualResetEvent[] doneEvents = new ManualResetEvent[listaProcessamento.Count];

                    int i = 0;
                    foreach (CteRecebimentoPooling cteRecebimentoPooling in listaProcessamento)
                    {
                        doneEvents[i] = new ManualResetEvent(false);

                        _cteRunnerLogService.InserirLogInfo("ReceiverManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", cteRecebimentoPooling.Id));

                        ProcessoRecebimentoRetornoCte recebimentoRetornoCte = new ProcessoRecebimentoRetornoCte(_container, doneEvents[i], _cteRunnerLogService, cteRecebimentoPooling);
                        ThreadPool.QueueUserWorkItem(recebimentoRetornoCte.ThreadPoolCallback, i);

                        i++;
                    }

                    WaitHandle.WaitAll(doneEvents);
                    _cteRunnerLogService.InserirLogInfo("ReceiverManagerService", string.Format("removendo do pooling de recebimento {0} Ctes processados", listaProcessamento.Count));
                    _cteService.RemoverPoolingRecebimentoCte(listaProcessamento);
                }
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro("ReceiverManagerService", string.Format("{0}", ex.Message));
                throw;
            }
            finally
            {
                _timer.Enabled = true;
            }
             */
        }

        /// <summary>
        /// Executa o recebimento (processamento do cte de retorno da Config) - Single Thread
        /// </summary>
        public void ExecutarRecebimentoSingleThread()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                _cteRunnerLogService.InserirLogInfo(ClassName, "executando o processo de varredura dos dados");

                _executando = true;

                string hostName = Dns.GetHostName();
#if DEBUG
                hostName = "CRW-VWPS01";
                _indAmbienteSefaz = 1;
#endif
                IList<CteInterfaceRecebimentoConfig> listaCtesNaoProcessados = _cteService.ObterCtesProcessarRecebimento(_indAmbienteSefaz);

#if DEBUG2
                listaCtesNaoProcessados.Clear();
                listaCtesNaoProcessados.Add(_cteInterfaceRecebimentoConfigRepository.ObterPorId(8059955));
#endif

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("ReceiverManagerService: enviando para o pooling de recebimento {0} Ctes", listaCtesNaoProcessados.Count));
                _cteService.InserirPoolingRecebimentoCte(listaCtesNaoProcessados, hostName);

                IList<CteRecebimentoPooling> listaProcessamento = _cteService.ObterPoolingRecebimentoPorServidor(hostName);

#if DEBUG2
                do
                {
                    listaProcessamento = _cteService.ObterPoolingRecebimentoPorServidor(hostName);
                    System.Threading.Thread.Sleep(100);
                } 
                while (listaProcessamento.Count == 0);

                // listaProcessamento.Clear();
                // // busca pelo IdCte. o campo Id � o IdCte
                // listaProcessamento.Add(_cteRecebimentoPoolingRepository.ObterPorId(7363110));
#endif

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("obtendo do pooling de recebimento {0} Ctes para serem processados", listaProcessamento.Count));

                Log.SetInformation(ClassName, "ExecutarRecebimentoSingleThread", string.Format("Foram identificados {0} registros para processar", listaProcessamento.Count));

                foreach (CteRecebimentoPooling cteRecebimentoPooling in listaProcessamento)
                {
                    try
                    {
                        _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("criando e colocando no pool a thread para o cte: {0} ", cteRecebimentoPooling.Id));

                        ProcessoRecebimentoRetornoCte recebimentoRetornoCte = new ProcessoRecebimentoRetornoCte(_container, _cteRunnerLogService, cteRecebimentoPooling);
                        recebimentoRetornoCte.Executar();
                    }
                    catch (Exception ex)
                    {
                        _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                        Log.SetError(ClassName, "ExecutarRecebimentoSingleThread", ex);
                    }
                }

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("removendo do pooling de recebimento {0} Ctes processados", listaProcessamento.Count));
                _cteService.RemoverPoolingRecebimentoCte(listaProcessamento);
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                Log.SetError(ClassName, "ExecutarRecebimentoSingleThread", ex);
            }
            finally
            {
                _executando = false;
            }
        }
    }

    /// <summary>
    /// Executa o delegate a cada intervalo
    /// </summary>
    public class ThreadIntervalReceiveManager
    {
        private readonly BackgroundWorker _worker;
        private bool _executarImediatamente;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        /// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
        /// <param name="interval">Intervalo que ser� executado o delegate</param>
        public ThreadIntervalReceiveManager(Action delegateInterval, TimeSpan interval)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;

            _worker.DoWork += delegate
            {
                if (_executarImediatamente)
                {
                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }

                while (true)
                {
                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    Thread.Sleep(interval);

                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }
            };
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        public void Start()
        {
            Start(false);
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        /// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
        public void Start(bool executarImediatamente)
        {
            _executarImediatamente = executarImediatamente;

            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancela a execu��o
        /// </summary>
        public void Stop()
        {
            _worker.CancelAsync();
        }
    }
}
