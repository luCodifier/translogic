namespace Translogic.CteRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Net;
    using System.Threading;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.Codificador;
    using Modules.Core.Domain.Model.Codificador.Repositories;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Translogic.CteRunner.Helper;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Servi�o de gerenciamento de importa��o dos Ctes
    /// </summary>
    public class ImporterPdfFileManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloTimerImportadorEmSegundos";
        private const string ClassName = "ImporterPdfFileManagerService";
        private readonly string _chavePdfPath = "CTE_PDF_PATH";
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly IWindsorContainer _container;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly ICteInterfacePdfConfigRepository _cteInterfacePdfRepository;
        private bool _executando;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImporterPdfFileManagerService"/> class.
        /// </summary>
        /// <param name="cteService"> Servi�o do Cte Injetado</param>
        /// <param name="cteRunnerLogService">Servi�o do log do cte runner injetado </param>
        /// <param name="container"> Container injetado </param>
        /// <param name="configuracaoTranslogicRepository"> Repositorio de configura��o do translogic injetado </param>
        /// <param name="cteInterfacePdfRepository"> Repositorio de configura��o do cteInterfacePdf injetado </param>
        public ImporterPdfFileManagerService(CteService cteService, CteRunnerLogService cteRunnerLogService, IWindsorContainer container, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository, ICteInterfacePdfConfigRepository cteInterfacePdfRepository)
        {
            _container = container;
            _cteService = cteService;
            _cteRunnerLogService = cteRunnerLogService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            _cteInterfacePdfRepository = cteInterfacePdfRepository;
            ////_cteRunnerLogService.InserirLogInfo(ClassName, "criado o servi�o");
        }

        /// <summary>
        /// Inicializa o timer
        /// </summary>
        /// <param name="execute">Se inicia o timer</param>
        public void Inicializar(bool execute)
        {
            int tempoSegundos;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out tempoSegundos);
                if (tempoSegundos.Equals(0))
                {
                    tempoSegundos = 30;
                    Log.SetWarning(ClassName, "Inicializar", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }
            }
            catch (Exception ex)
            {
                tempoSegundos = 30;
                Log.SetError(ClassName, "Inicializar", ex);
            }

            _executando = false;

            if (execute)
            {
                new ThreadIntervalPdfImporter(ExecutarImportacaoSingleThread, TimeSpan.FromSeconds(tempoSegundos)).Start();
            }

            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
        }

        /// <summary>
        /// Executa a importa��o dos arquivos de pdf - Single Thread
        /// </summary>
        public void ExecutarImportacaoSingleThread()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                ConfiguracaoTranslogic pdfPath = _configuracaoTranslogicRepository.ObterPorId(_chavePdfPath);

                _cteRunnerLogService.InserirLogInfo(ClassName, "executando o processo de varredura dos dados");
                _executando = true;

                string hostName = Dns.GetHostName();
                IList<CteInterfacePdfConfig> listaCtesNaoImportados = _cteService.ObterCtesImportarPdf(hostName);

#if DEBUG
                listaCtesNaoImportados.Clear();
                listaCtesNaoImportados.Add(_cteInterfacePdfRepository.ObterPorId(7177515));
#endif

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("importando os {0} PDF para o banco de dados", listaCtesNaoImportados.Count));

                Log.SetInformation(ClassName, "ExecutarImportacaoSingleThread", string.Format("Foram identificados {0} registros para processar", listaCtesNaoImportados.Count));

                foreach (CteInterfacePdfConfig cteInterfacePdfConfig in listaCtesNaoImportados)
                {
                    _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("colocando na fila de processamento para importa��o o Cte: {0} ", cteInterfacePdfConfig.Cte.Id));

                    ProcessoImportacaoPdfCte processoImportacao = new ProcessoImportacaoPdfCte(_container, _cteRunnerLogService, cteInterfacePdfConfig, pdfPath);
                    processoImportacao.Executar();
                }
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                Log.SetError(ClassName, "ExecutarImportacaoSingleThread", ex);
            }
            finally
            {
                _executando = false;
            }
        }

        /// <summary>
        /// Executa a importa��o dos arquivos de pdf - Multi Thread
        /// </summary>
        public void ExecutarImportacaoMultiThread()
        {
            /*
            try
            {
                ConfiguracaoTranslogic pdfPath = _configuracaoTranslogicRepository.ObterPorId(_chavePdfPath);

                _cteRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", "executando o processo de varredura dos dados");
                _timer.Enabled = false;

                string hostName = Dns.GetHostName();
                IList<CteInterfacePdfConfig> listaCtesNaoImportados = _cteService.ObterCtesImportarPdf(hostName);

                _cteRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", string.Format("importando os {0} PDF para o banco de dados", listaCtesNaoImportados.Count));

                if (listaCtesNaoImportados.Count > 0)
                {
                    ManualResetEvent[] doneEvents = new ManualResetEvent[listaCtesNaoImportados.Count];

                    int i = 0;

                    foreach (CteInterfacePdfConfig cteInterfacePdfConfig in listaCtesNaoImportados)
                    {
                        doneEvents[i] = new ManualResetEvent(false);

                        _cteRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", string.Format("criando e colocando no pool a thread para o cte: {0} ", cteInterfacePdfConfig.Id));

                        ProcessoImportacaoPdfCte processoImportacao = new ProcessoImportacaoPdfCte(_container, doneEvents[i], _cteRunnerLogService, cteInterfacePdfConfig, pdfPath);
                        ThreadPool.QueueUserWorkItem(processoImportacao.ThreadPoolCallback, i);

                        i++;
                    }

                    WaitHandle.WaitAll(doneEvents);
                }
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro("ImporterPdfFileManagerService", string.Format("{0}", ex.Message));
                throw;
            }
            finally
            {
                _timer.Enabled = true;
            }
             */
        }
    }

    /// <summary>
    /// Executa o delegate a cada intervalo
    /// </summary>
    public class ThreadIntervalPdfImporter
    {
        private readonly BackgroundWorker _worker;
        private bool _executarImediatamente;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        /// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
        /// <param name="interval">Intervalo que ser� executado o delegate</param>
        public ThreadIntervalPdfImporter(Action delegateInterval, TimeSpan interval)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;

            _worker.DoWork += delegate
            {
                if (_executarImediatamente)
                {
                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }

                while (true)
                {
                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    Thread.Sleep(interval);

                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }
            };
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        public void Start()
        {
            Start(false);
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        /// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
        public void Start(bool executarImediatamente)
        {
            _executarImediatamente = executarImediatamente;

            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancela a execu��o
        /// </summary>
        public void Stop()
        {
            _worker.CancelAsync();
        }
    }
}