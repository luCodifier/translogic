namespace Translogic.CteRunner.Services
{
	using System;
	using System.Collections.Generic;
	using System.Configuration;
	using System.IO;
	using System.Linq;
	using System.Text;
	using EnterpriseDT.Net.Ftp;
	using Models;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

	/// <summary>
	/// Servi�o de recebimento de arquivos de ftp pela NDD (N�o utilizado)
	/// </summary>
	public class FtpCteService
	{
		private const string QuantidadeDiasParaLimpeza = "QuantidadeDiasParaLimpeza";
		// private const string PastaCteServidorFtp = "/";
		private const string PastaCteServidorFtp = "/TesteCteRetorno";
		private readonly ICteArquivoFtpRepository _cteArquivoFtpRepository;
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly ConfiguracaoFtp _configuracaoFtp;

		/// <summary>
		/// Initializes a new instance of the <see cref="FtpCteService"/> class.
		/// </summary>
		/// <param name="cteArquivoFtpRepository"> The cte arquivo ftp repository. </param>
		/// <param name="configuracaoFtp"> Configura��es de FTP injetadas</param>
		/// <param name="cteRunnerLogService">Servi�o de log do runner injetado</param>
		public FtpCteService(ICteArquivoFtpRepository cteArquivoFtpRepository, ConfiguracaoFtp configuracaoFtp, CteRunnerLogService cteRunnerLogService)
		{
			_cteArquivoFtpRepository = cteArquivoFtpRepository;
			_cteRunnerLogService = cteRunnerLogService;
			_configuracaoFtp = configuracaoFtp;
		}

		/// <summary>
		/// Processa os dados do FTP, persistindo os dados na base de dados para tratamento de concorrencia
		/// </summary>
		public void ProcessarDadosFtp()
		{
			_cteRunnerLogService.InserirLogInfo("FtpCteService", "executando o processo de varredura dos arquivos");
			IList<string> listaArquivosFtp = ObterListaArquivosFtp(PastaCteServidorFtp).Where(c => c.EndsWith(".txt")).ToList();

			IList<CteArquivoFtp> listaArquivosNaFila = _cteArquivoFtpRepository.ObterTodos();
			_cteRunnerLogService.InserirLogInfo("FtpCteService", string.Format("recebendo {0} arquivos do Ftp", listaArquivosNaFila.Count));

			int idxPasta = PastaCteServidorFtp.Length;
			for (int i = 0; i < listaArquivosFtp.Count; i++)
			{
				listaArquivosFtp[i] = listaArquivosFtp[i].Substring(idxPasta);
			}

			List<string> listaPersistir = listaArquivosFtp.Where(c => listaArquivosNaFila.FirstOrDefault(d => d.ChaveCte.Equals(c.Substring(0, 44))) == null).ToList();
			_cteRunnerLogService.InserirLogInfo("FtpCteService", string.Format("inserindo arquivos na fila de Ftp: {0} arquivos", listaPersistir.Count));
			InserirArquivosNovosFtpNaFila(listaPersistir);

			_cteRunnerLogService.InserirLogInfo("FtpCteService", "procurar ctes para atualizar");
			ProcurarCteAtualizar();

			LimparListaAntigosNaoTranslogic();
		}

		/// <summary>
		/// Limpa a tabela de arquivos do FTP que n�o s�o do translogic e que s�o antigos
		/// </summary>
		public void LimparListaAntigosNaoTranslogic()
		{
			int diasParaLimpeza;
			try
			{
				Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				string aux = config.AppSettings.Settings[QuantidadeDiasParaLimpeza].Value;
				Int32.TryParse(aux, out diasParaLimpeza);
				if (diasParaLimpeza.Equals(0))
				{
					diasParaLimpeza = 30;
				}
			}
			catch (Exception)
			{
				diasParaLimpeza = 30;
			}

			_cteArquivoFtpRepository.LimparAntigos(diasParaLimpeza);
		}

		/// <summary>
		/// Procura os Ctes pela chave para verificar se for CTE do translogic e atualiza com o Id
		/// </summary>
		public void ProcurarCteAtualizar()
		{
			_cteArquivoFtpRepository.AtualizarQuandoAcharCte();
		}

		/// <summary>
		/// Insere os arquivos novos na fila
		/// </summary>
		/// <param name="listaPersistir">Lista de novos arquivos</param>
		public void InserirArquivosNovosFtpNaFila(List<string> listaPersistir)
		{
			string nomeArquivo = string.Empty;

			foreach (string arquivo in listaPersistir)
			{
				nomeArquivo = RemoverCaracterEspecial(arquivo);
				string chaveCte = nomeArquivo.Substring(0, 44);
				CteArquivoFtp cteArquivoFtp = new CteArquivoFtp();
				cteArquivoFtp.Id = arquivo;
				cteArquivoFtp.DataCadastro = DateTime.Now;
				cteArquivoFtp.ChaveCte = chaveCte;
				try
				{
					_cteArquivoFtpRepository.Inserir(cteArquivoFtp);
				}
				catch (Exception ex)
				{
					_cteArquivoFtpRepository.ClearSession();
				}
			}
		}

		/// <summary>
		/// Obt�m a lista de arquivos do FTP
		/// </summary>
		/// <param name="pasta"> The pasta. </param>
		/// <returns> Lista de nomes de arquivos do ftp </returns>
		public IList<string> ObterListaArquivosFtp(string pasta)
		{
			FTPConnection ftpConnection = null;
			try
			{
				ftpConnection = new FTPConnection
				{
					ServerAddress = _configuracaoFtp.Servidor,
					UserName = _configuracaoFtp.Usuario,
					Password = _configuracaoFtp.Senha
				};
				ftpConnection.Connect();

				return ftpConnection.GetFiles(pasta);
			}
			catch (Exception ex)
			{
				_cteRunnerLogService.InserirLogErro("FtpCteService", string.Format("{0}", ex.Message), ex);
				throw;
			}
			finally
			{
				if (ftpConnection != null && ftpConnection.IsConnected)
				{
					ftpConnection.Close();
				}
			}
		}

		/// <summary>
		/// Obtem o arquivo de retorno do FTP
		/// </summary>
		/// <param name="nomeArquivo">Nome do arquivo a ser obtido no FTP</param>
		/// <returns>Conte�do do arquivo de retorno</returns>
		public string ObterConteudoArquivoRetorno(string nomeArquivo)
		{
			FTPConnection ftpConnection = null;
			try
			{
				ftpConnection = new FTPConnection
				{
					ServerAddress = _configuracaoFtp.Servidor,
					UserName = _configuracaoFtp.Usuario,
					Password = _configuracaoFtp.Senha
				};
				ftpConnection.Connect();

				string arquivo = PastaCteServidorFtp + nomeArquivo;

				// byte[] retorno = ftpConnection.DownloadByteArray(nomeArquivo);
				byte[] retorno = ftpConnection.DownloadByteArray(arquivo);

				UTF8Encoding encoding = new UTF8Encoding();
				return encoding.GetString(retorno);
			}
			catch (Exception ex)
			{
				_cteRunnerLogService.InserirLogErro("FtpCteService", string.Format("{0}", ex.Message), ex);
				throw;
			}
			finally
			{
				if (ftpConnection != null && ftpConnection.IsConnected)
				{
					ftpConnection.Close();
				}
			}
		}

		/// <summary>
		/// Apaga o arquivo do FTP
		/// </summary>
		/// <param name="nomeArquivo">Nome do arquivo a ser apagado</param>
		public void ApagarArquivo(string nomeArquivo)
		{
			FTPConnection ftpConnection = null;
			try
			{
				ftpConnection = new FTPConnection
				{
					ServerAddress = _configuracaoFtp.Servidor,
					UserName = _configuracaoFtp.Usuario,
					Password = _configuracaoFtp.Senha
				};
				ftpConnection.Connect();

				ftpConnection.DeleteFile(PastaCteServidorFtp + nomeArquivo);
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (ftpConnection != null && ftpConnection.IsConnected)
				{
					ftpConnection.Close();
				}
			}
		}

		private string RemoverCaracterEspecial(string valor)
		{
			StringBuilder sb = new StringBuilder();

			foreach (char c in valor)
			{
				if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_')
				{
					sb.Append(c);
				}
			}

			return sb.ToString();
		}
	}
}