namespace Translogic.CteRunner.Services
{
    using System;
    using System.Configuration;
    using Castle.Windsor;
    using Commands;
    using Translogic.CteRunner.Helper;

    /// <summary>
    /// Classe DirectoryFileManagerService
    /// </summary>
    public class DirectoryFileManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloTimerLimpezaEmSegundos";
        private const string ClassName = "DirectoryFileManagerService";
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly IWindsorContainer _container;
        private bool _executando;

        /// <summary>
        /// Construtor DirectoryFileManagerService
        /// </summary>
        /// <param name="cteRunnerLogService">Servi�o log CteRunner</param>
        /// <param name="container"> Container injetado</param>
        public DirectoryFileManagerService(CteRunnerLogService cteRunnerLogService, IWindsorContainer container)
        {
            _cteRunnerLogService = cteRunnerLogService;
            _container = container;
        }

        /// <summary> 
        /// Inicializa o timer
        /// </summary>
        public void Inicializar()
        {
            int tempoSegundos;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out tempoSegundos);

                if (tempoSegundos.Equals(0))
                {
                    tempoSegundos = 30;
                    Log.SetWarning(ClassName, "Inicializar", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }
            }
            catch (Exception ex)
            {
                tempoSegundos = 30;
                Log.SetError(ClassName, "Inicializar", ex);
            }

            _executando = false;
            new ThreadIntervalSenderQueueManager(ExecutarGeracaoSingleThread, TimeSpan.FromSeconds(tempoSegundos)).Start();

            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
        }

        /// <summary>
        /// Executa a gera��o em single thread
        /// </summary>
        public void ExecutarGeracaoSingleThread()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                _executando = true;

                try
                {
                    _cteRunnerLogService.InserirLogInfo(ClassName, "Executando limpeza no diret�rio");

                    ProcessoLimpezaDiretorio limpezaDiretorio = new ProcessoLimpezaDiretorio(_container, _cteRunnerLogService);
                    limpezaDiretorio.Executar();
                }
                catch (Exception ex)
                {
                    _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                    Log.SetError(ClassName, "ExecutarGeracaoSingleThread", ex);
                }

                _cteRunnerLogService.InserirLogInfo(ClassName, "DirectoryFileManagerService: Conclu�da limpeza no diret�rio.");
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                Log.SetError(ClassName, "ExecutarGeracaoSingleThread", ex);
            }
            finally
            {
                _executando = false;
            }
        }
    }
}
