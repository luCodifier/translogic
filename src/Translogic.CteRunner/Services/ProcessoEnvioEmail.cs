namespace Translogic.CteRunner.Services
{
    using System;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.Codificador;
    using Modules.Core.Domain.Model.Codificador.Repositories;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Modules.Core.Domain.Services.FluxosComerciais;

    /// <summary>
    /// Processo para envio de email
    /// </summary>
    public class ProcessoEnvioEmail
    {
        private readonly CteService _cteService;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private readonly CteRunnerLogService _cteRunnerLogService;

        private readonly string _cteEmailServer = "CTE_EMAIL_SERVER";
        private readonly string _cteEmailRemetente = "CTE_EMAIL_REMETENTE";
        private readonly string _cteEmailSenhaSmtp = "CTE_EMAIL_SMTP_SENHA";

        private string _remetente;
        private string _senhaSmtp;
        private int _emailRemetenteIndex;
        private int _smtpIndex;
        private string _emailServer;
        private int _emailPort;
        private string[] _listaRemetente;
        private string[] _listaSmtp;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessoImportacaoXmlCte"/> class.
        /// </summary>
        /// <param name="container">Container injetado.</param>
        /// <param name="cteRunnerLogService">Servi�o de log do runner</param>
        public ProcessoEnvioEmail(IWindsorContainer container, CteRunnerLogService cteRunnerLogService)
        {
            _cteService = container.Resolve<CteService>();
            _configuracaoTranslogicRepository = container.Resolve<IConfiguracaoTranslogicRepository>();
            _cteRunnerLogService = cteRunnerLogService;
            Inicializar();
        }

        /// <summary>
        /// Executa o envio de email
        /// </summary>
        /// <param name="itemInterface">Item da interface para enviar o email</param>
        public void Executar(CteInterfaceEnvioEmail itemInterface)
        {
            ProcessarEnvioEmail(itemInterface);
        }

        private void ProcessarEnvioEmail(CteInterfaceEnvioEmail itemInterface)
        {
            if (itemInterface != null)
            {
                _remetente = _listaRemetente[_emailRemetenteIndex = --_emailRemetenteIndex < 0 ? _listaRemetente.Length - 1 : _emailRemetenteIndex];
                var smtp = _listaSmtp[_smtpIndex = --_smtpIndex < 0 ? _listaSmtp.Length - 1 : _smtpIndex];
                
                _emailServer = smtp.Split(':')[0];
                _emailPort = Convert.ToInt32(smtp.Split(':')[1]);
                
                _cteService.EnviarEmail(itemInterface, _emailServer, _emailPort, _remetente, _senhaSmtp);
            }
        }

        private void Inicializar()
        {
            // Servidor de email
            ConfiguracaoTranslogic configuracaoTranslogic = _configuracaoTranslogicRepository.ObterPorId(_cteEmailServer);
            _listaSmtp = configuracaoTranslogic.Valor.Split(';');

            ConfiguracaoTranslogic configuracaoRemetente = _configuracaoTranslogicRepository.ObterPorId(_cteEmailRemetente);
            _listaRemetente = configuracaoRemetente.Valor.Split(';');

            ConfiguracaoTranslogic configuracaocteEmailSenhaSmtp = _configuracaoTranslogicRepository.ObterPorId(_cteEmailSenhaSmtp);
            _senhaSmtp = configuracaocteEmailSenhaSmtp != null ? configuracaocteEmailSenhaSmtp.Valor : string.Empty;
        }
    }
}