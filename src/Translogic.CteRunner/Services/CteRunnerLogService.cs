namespace Translogic.CteRunner.Services
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Text;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;

    /// <summary>
    /// Classe para grava��o dos logs do Cte Runner
    /// </summary>
    public class CteRunnerLogService
    {
        private readonly ICteRunnerLogRepository _cteRunnerLogRepository;
        private readonly bool _logInformacao;
        private readonly bool _logErro;

        /// <summary>
        /// Initializes a new instance of the <see cref="CteRunnerLogService"/> class.
        /// </summary>
        /// <param name="cteRunnerLogRepository">Reposit�rio Cte Runner Log injetado</param>
        public CteRunnerLogService(ICteRunnerLogRepository cteRunnerLogRepository)
        {
            _cteRunnerLogRepository = cteRunnerLogRepository;

            string informacao = ConfigurationManager.AppSettings["LogInformacao"];
            string erro = ConfigurationManager.AppSettings["LogErro"];
            _logInformacao = !string.IsNullOrEmpty(informacao) && Convert.ToBoolean(informacao);
            _logErro = !string.IsNullOrEmpty(erro) && Convert.ToBoolean(erro);
        }

        /// <summary>
        /// Insere o log de informa��o do runner
        /// </summary>
        /// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        public void InserirLogInfo(string nomeServico, string mensagem)
        {
            if (_logInformacao)
            {
                _cteRunnerLogRepository.InserirLogInfo(Dns.GetHostName(), nomeServico, mensagem);
            }
        }

        /// <summary>
        /// Insere o log de informa��o do runner
        /// </summary>
        /// <param name="hostName">Nome do host que est� hospedando o servi�o</param>
        /// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        public void InserirLogInfo(string hostName, string nomeServico, string mensagem)
        {
            if (_logInformacao)
            {
                _cteRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
            }
        }

        /// <summary>
        /// Insere log de informa��o do runner
        /// </summary>
        /// <param name="hostName">Nome do host que est� hospedando o servi�o</param>
        /// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        public void InserirLogInfo(string hostName, string nomeServico, StringBuilder mensagem)
        {
            if (_logInformacao)
            {
                _cteRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
            }
        }

        /// <summary>
        /// Insere o log de erro do runner
        /// </summary>
        /// <param name="nomeServico">Nome do servi�o que est� sendo logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        /// <param name="ex">Exception do log</param>
        public void InserirLogErro(string nomeServico, string mensagem, Exception ex)
        {
            if (_logErro)
            {
                _cteRunnerLogRepository.InserirLogErro(Dns.GetHostName(), nomeServico, mensagem, ex);
            }
        }

        /// <summary>
        /// Insere o log de erro do runner
        /// </summary>
        /// <param name="hostName">Nome do host que est� hospedando o servi�o</param>
        /// <param name="nomeServico">Nome do servi�o a ser logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        /// <param name="ex">Exception do log</param>
        public void InserirLogErro(string hostName, string nomeServico, string mensagem, Exception ex)
        {
            if (_logErro)
            {
                _cteRunnerLogRepository.InserirLogErro(hostName, nomeServico, mensagem, ex);
            }
        }

        /// <summary>
        /// Insere o log de erro do runner
        /// </summary>
        /// <param name="hostName">Nome do host que est� hospedando o servi�o</param>
        /// <param name="nomeServico">Nome do servi�o a ser logado</param>
        /// <param name="mensagem">Mensagem do log</param>
        public void InserirLogErro(string hostName, string nomeServico, StringBuilder mensagem)
        {
            if (_logErro)
            {
                _cteRunnerLogRepository.InserirLogInfo(hostName, nomeServico, mensagem);
            }
        }
    }
}