namespace Translogic.CteRunner.Services
{
	using Castle.Windsor;
	using Modules.Core.Domain.Model.Codificador;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Processo para importar o arquivo de XML
	/// </summary>
	public class ProcessoImportacaoXmlCte
	{
		private readonly CteService _cteService;
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly CteInterfaceXmlConfig _itemInterface;
		private readonly ConfiguracaoTranslogic _xmlPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoImportacaoXmlCte"/> class.
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="cteRunnerLogService">Serviço de log do runner</param>
		/// <param name="itemInterface">Item da interface de importação</param>
		/// <param name="xmlPath">Item de configuração do xml path</param>
		public ProcessoImportacaoXmlCte(IWindsorContainer container, CteRunnerLogService cteRunnerLogService, CteInterfaceXmlConfig itemInterface, ConfiguracaoTranslogic xmlPath)
		{
			_cteService = container.Resolve<CteService>();
			_itemInterface = itemInterface;
			_cteRunnerLogService = cteRunnerLogService;
			_xmlPath = xmlPath;
		}

		/// <summary>
		/// Executa no modo single thread
		/// </summary>
		public void Executar()
		{
			ProcessarImportacao();
		}

		private void ProcessarImportacao()
		{
			if (_itemInterface != null)
			{
				_cteService.ImportarArquivoXml(_itemInterface, _xmlPath.Valor);
			}
		}
	}
}