﻿namespace Translogic.CteRunner.Services.Commands
{
    using System;
    using Modules.Core.Domain.Model.Diversos.Cte;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Comando para gravar os dados de anulação do CTE na base da Config
    /// </summary>
    public class GravarConfigCteAnulacaoContribuinteCommand : ICommandCte
    {
        private readonly GravarConfigCteAnulacaoContribuinteService _gravarConfigCteAnulacaoService;
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GravarConfigCteAnulacaoContribuinteCommand"/> class.
        /// </summary>
        /// <param name="gravarConfigCteAnulacaoService">Serviço que grava os dados no banco da config injetado</param>
        /// <param name="cteService">Serviço do cte injetado</param>
        /// <param name="cteRunnerLogService">Serviço de log do Cte Runner injetado</param>
        public GravarConfigCteAnulacaoContribuinteCommand(GravarConfigCteAnulacaoContribuinteService gravarConfigCteAnulacaoService, CteService cteService, CteRunnerLogService cteRunnerLogService)
        {
            _gravarConfigCteAnulacaoService = gravarConfigCteAnulacaoService;
            _cteRunnerLogService = cteRunnerLogService;
            _cteService = cteService;
        }
        
        /// <summary>
        /// Executa a implementação do comando
        /// </summary>
        /// <param name="cte">Cte a ser processado</param>
        public void Executar(Cte cte)
        {
            try
            {
                // Executa a gravação do arquivo
                _gravarConfigCteAnulacaoService.Executar(cte);

                // Insere o cte de complemento na fila de processamento da config
                _cteService.InserirConfigCteAnulacao(cte);
            }
            catch (Exception ex)
            {
                _cteService.MudarSituacaoCte(cte, SituacaoCteEnum.ErroAutorizadoReEnvio, null, new CteStatusRetorno { Id = 14 }, "Alterado a situação do Cte para ERRO", ex);
                _cteRunnerLogService.InserirLogErro("GravarConfigCteAnulacaoContribuinteCommand", string.Format("{0}", ex.Message), ex);
            }
        }
    }
}