namespace Translogic.CteRunner.Services.Commands
{
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

	/// <summary>
	/// Interface do comando
	/// </summary>
	public interface ICommandCte
	{
		/// <summary>
		/// Executa a implementação do comando 
		/// </summary>
		/// <param name="cte">Cte a ser processado. </param>
		void Executar(Cte cte);
	} 
}