namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using Modules.Core.Domain.Model.Diversos.Cte;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Comando para a grava��o dos dados de cancelamento de Cte na base da Config
	/// </summary>
	public class GravarConfigCteCancelamentoCommand : ICommandCte
	{
		private readonly GravarConfigCteCancelamentoService _gravarConfigCteCancelamentoService;
		private readonly CteService _cteService;
		private readonly CteRunnerLogService _cteRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarConfigCteCancelamentoCommand"/> class.
		/// </summary>
		/// <param name="gravarConfigCteCancelamentoService">Servi�o que grava os dados no banco da config injetado</param>
		/// <param name="cteService">Servi�o do cte injetado</param>
		/// <param name="cteRunnerLogService">Servi�o de log do Cte Runner injetado</param>
		public GravarConfigCteCancelamentoCommand(GravarConfigCteCancelamentoService gravarConfigCteCancelamentoService, CteService cteService, CteRunnerLogService cteRunnerLogService)
		{
			_gravarConfigCteCancelamentoService = gravarConfigCteCancelamentoService;
			_cteRunnerLogService = cteRunnerLogService;
			_cteService = cteService;
		}

		/// <summary>
		/// Executa a implementa��o do comando
		/// </summary>
		/// <param name="cte">Cte a ser processado</param>
		public void Executar(Cte cte)
		{
			try
			{
				// Insere o cte na fila de processamento da config
				_cteService.InserirConfigCteCancelamento(cte);
			}
			catch (Exception ex)
			{
				_cteService.MudarSituacaoCte(cte, SituacaoCteEnum.ErroAutorizadoReEnvio, null, new CteStatusRetorno { Id = 14 }, "Alterado a situa��o do Cte para ERRO", ex);
				_cteRunnerLogService.InserirLogErro("GravarConfigCteCancelamentoCommand", string.Format("{0}", ex.Message), ex);
			}
		}
	}
}