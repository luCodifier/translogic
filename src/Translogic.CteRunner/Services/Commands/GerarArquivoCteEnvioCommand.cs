namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using System.Text;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Model.FluxosComerciais.Ndd.Repositories;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Comando para gera��o de arquivo de Cte de envio para NDD (N�o Utilizado)
	/// </summary>
	public class GerarArquivoCteEnvioCommand : ICommandCte
	{
		private readonly GerarArquivoCteEnvioService _gerarArquivoCteEnvioService;
		private readonly CteService _cteService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteEnvioCommand"/> class.
		/// </summary>
		/// <param name="gerarArquivoCteEnvioService"> The gerar arquivo cte envio service. </param>
		/// <param name="cteService">Servi�o do CTE injetado</param>
		public GerarArquivoCteEnvioCommand(GerarArquivoCteEnvioService gerarArquivoCteEnvioService, CteService cteService)
		{
			_gerarArquivoCteEnvioService = gerarArquivoCteEnvioService;
			_cteService = cteService;
		}

		/// <summary> 
		/// Executa a implementa��o do comando
		/// </summary>
		/// <param name="cte">Cte a ser processado. </param>
		public void Executar(Cte cte)
		{
			// Gerar o arquivo de envio
			StringBuilder arquivoGerado = _gerarArquivoCteEnvioService.Executar(cte);

			// Gravar os dados na NDD
			_cteService.InserirArquivoEnvioNdd(cte, arquivoGerado);
		}
	}
}