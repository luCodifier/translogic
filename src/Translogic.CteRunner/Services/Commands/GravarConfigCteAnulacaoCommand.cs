﻿namespace Translogic.CteRunner.Services.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Comando para gravar os dados de anulação do CTE na base da Config
    /// </summary>
    public class GravarConfigCteAnulacaoCommand : ICommandCte
    {
        private readonly GravarConfigCteAnulacaoService _gravarConfigCteAnulacaoService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GravarConfigCteAnulacaoCommand"/> class.
        /// </summary>
        /// <param name="gravarConfigCteAnulacaoService">Serviço que grava os dados no banco da config injetado</param>
        public GravarConfigCteAnulacaoCommand(GravarConfigCteAnulacaoService gravarConfigCteAnulacaoService)
        {
            _gravarConfigCteAnulacaoService = gravarConfigCteAnulacaoService;
        }
        
        /// <summary>
        /// Executa a implementação do comando
        /// </summary>
        /// <param name="cte">Cte a ser processado</param>
        public void Executar(Cte cte)
        {
            try
            {
                _gravarConfigCteAnulacaoService.Executar(cte); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}