﻿namespace Translogic.CteRunner.Services.Commands
{
    using System;
    using Modules.Core.Domain.Model.Diversos.Cte;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Comando para a gravação dos dados de correção de Cte na base da Config
    /// </summary>
    public class GravarConfigCorrecaoCteCommand : ICommandCte
    {
        private readonly GravarConfigCorrecaoCteService _gravarConfigCorrecaoCteService;
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GravarConfigCorrecaoCteCommand"/> class.
        /// </summary>
        /// <param name="gravarConfigCorrecaoCteService">Serviço que grava os dados no banco da config injetado</param>
        /// <param name="cteService">Serviço do cte injetado</param>
        /// <param name="cteRunnerLogService">Serviço de log do Cte Runner injetado</param>
        public GravarConfigCorrecaoCteCommand(GravarConfigCorrecaoCteService gravarConfigCorrecaoCteService, CteService cteService, CteRunnerLogService cteRunnerLogService)
        {
            _gravarConfigCorrecaoCteService = gravarConfigCorrecaoCteService;
            _cteRunnerLogService = cteRunnerLogService;
            _cteService = cteService;
        }

        /// <summary>
        /// Executa a implementação do comando
        /// </summary>
        /// <param name="cte">Cte a ser processado</param>
        public void Executar(Cte cte)
        {
            try
            {
                // Executa a gravação do arquivo
                _gravarConfigCorrecaoCteService.Executar(cte);

                // Insere o cte na fila de processamento da config
                _cteService.InserirConfigCteCorrecao(cte);
            }
            catch (Exception ex)
            {
                _cteService.MudarSituacaoCte(cte, SituacaoCteEnum.ErroAutorizadoReEnvio, null, new CteStatusRetorno { Id = 14 }, "Alterado a situação do Cte para ERRO", ex);
                _cteRunnerLogService.InserirLogErro("GravarConfigCorrecaoCteCommand", string.Format("{0}", ex.Message), ex);
            }
        }
    }
}
