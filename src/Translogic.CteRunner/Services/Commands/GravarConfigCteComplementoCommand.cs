namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using Modules.Core.Domain.Model.Diversos.Cte;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;

	/// <summary>
	/// Comando para a grava��o dos dados de complemento de Cte na base da Config
	/// </summary>
	public class GravarConfigCteComplementoCommand : ICommandCte 
	{
		private readonly GravarConfigCteComplementoService _gravarConfigCteComplementoService;
		private readonly CteService _cteService;
		private readonly CteRunnerLogService _cteRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GravarConfigCteComplementoCommand"/> class.
		/// </summary>
		/// <param name="gravarConfigCteComplementoService">Servi�o que grava os dados no banco da config injetado</param>
		/// <param name="cteService">Servi�o do cte injetado</param>
		/// <param name="cteRunnerLogService">Servi�o de log do Cte Runner injetado</param>
		public GravarConfigCteComplementoCommand(GravarConfigCteComplementoService gravarConfigCteComplementoService, CteService cteService, CteRunnerLogService cteRunnerLogService)
		{
			_gravarConfigCteComplementoService = gravarConfigCteComplementoService;
			_cteRunnerLogService = cteRunnerLogService;
			_cteService = cteService;
		}

		/// <summary>
		/// Executa a implementa��o do comando
		/// </summary>
		/// <param name="cte">Cte a ser processado</param>
		public void Executar(Cte cte)
		{
			try
			{
				// Executa a grava��o do arquivo
				_gravarConfigCteComplementoService.Executar(cte);

				// Insere o cte de complemento na fila de processamento da config
				_cteService.InserirConfigCteComplemento(cte);
			}
			catch (Exception ex)
			{
				_cteService.MudarSituacaoCte(cte, SituacaoCteEnum.ErroAutorizadoReEnvio, null, new CteStatusRetorno { Id = 14 }, "Alterado a situa��o do Cte para ERRO", ex);
				_cteRunnerLogService.InserirLogErro("GravarConfigCteComplementoCommand", string.Format("{0}", ex.Message), ex);
			}
		}
	}
}