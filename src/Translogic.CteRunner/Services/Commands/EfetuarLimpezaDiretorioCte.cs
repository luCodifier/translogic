namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;

	/// <summary>
	/// Classe EfetuarLimpezaDiretorioCte
	/// </summary>
	public class EfetuarLimpezaDiretorioCte : ICommandCte
	{
		private readonly EfetuarLimpezaDiretorioCteService _efetuarLimpezaDiretorioCteService;

		/// <summary>
		/// Initializes a new instance of the <see cref="EfetuarLimpezaDiretorioCteService"/> class.
		/// </summary>
		/// <param name="efetuarLimpezaDiretorioCteService">Serviço de limpeza de diretórioB</param>
		public EfetuarLimpezaDiretorioCte(EfetuarLimpezaDiretorioCteService efetuarLimpezaDiretorioCteService)
		{
			_efetuarLimpezaDiretorioCteService = efetuarLimpezaDiretorioCteService;
		}

		/// <summary>
		/// Executa a implementação do comando
		/// </summary>
		/// <param name="cte"> Cte a ser processado</param>
		public void Executar(Cte cte)
		{
			try
			{
				// Executa a limpeza do diretório
				_efetuarLimpezaDiretorioCteService.Executar();
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}