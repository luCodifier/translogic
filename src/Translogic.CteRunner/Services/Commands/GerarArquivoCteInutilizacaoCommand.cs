namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using System.Text;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Comando de envio de arquivo de cte de inutiliza��o para a NDD (N�o utilizado)
	/// </summary>
	public class GerarArquivoCteInutilizacaoCommand : ICommandCte
	{
		private readonly GerarArquivoCteInutilizacaoService _gerarArquivoInutilizacaoCte;
		private readonly CteService _cteService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteEnvioCommand"/> class.
		/// </summary>
		/// <param name="gerarArquivoCte"> Servi�o de gera��o de arquivo de CTe injetado. </param>
		/// <param name="cteService">Servi�o do CTE injetado</param>
		public GerarArquivoCteInutilizacaoCommand(GerarArquivoCteInutilizacaoService gerarArquivoCte, CteService cteService)
		{
			_gerarArquivoInutilizacaoCte = gerarArquivoCte;
			_cteService = cteService;
		}

		/// <summary>
		/// Executa a implementa��o do comando
		/// </summary>
		/// <param name="cte">Cte a ser processado. </param>
		public void Executar(Cte cte)
		{
			// Gerar o arquivo de envio
			StringBuilder arquivoGerado = _gerarArquivoInutilizacaoCte.Executar(cte, string.Empty);

			// Gravar os dados na NDD
			_cteService.InserirArquivoInutilizacaoNdd(cte, arquivoGerado);
		}
	}
}