namespace Translogic.CteRunner.Services.Commands
{
	using System;
	using System.Text;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Modules.Core.Domain.Services.Ctes;
	using Modules.Core.Domain.Services.FluxosComerciais;

	/// <summary>
	/// Comando de gera��o de arquivo de cte de cancelamento para NDD (N�o utilizado)
	/// </summary>
	public class GerarArquivoCteCancelamentoCommand : ICommandCte
	{
		private readonly GerarArquivoCteCancelamentoService _gerarArquivoCteCancelamento;
		private readonly CteService _cteService;

		/// <summary>
		/// Initializes a new instance of the <see cref="GerarArquivoCteEnvioCommand"/> class.
		/// </summary>
		/// <param name="gerarArquivoCteCancelamento"> The gerar arquivo cte cancelamento service. </param>
		/// /// <param name="cteService">Servi�o do CTE injetado</param>
		public GerarArquivoCteCancelamentoCommand(GerarArquivoCteCancelamentoService gerarArquivoCteCancelamento, CteService cteService)
		{
			_gerarArquivoCteCancelamento = gerarArquivoCteCancelamento;
			_cteService = cteService;
		}

		/// <summary>
		/// Executa a implementa��o do comando
		/// </summary>
		/// <param name="cte">Cte a ser processado. </param>
		public void Executar(Cte cte)
		{
			// Gerar o arquivo de envio
			StringBuilder arquivoGerado = _gerarArquivoCteCancelamento.Executar(cte, string.Empty);

			// Gravar os dados na NDD
			_cteService.InserirArquivoCancelamentoNdd(cte, arquivoGerado);
		}
	}
}