namespace Translogic.CteRunner.Services
{
	using System;
	using System.Threading;
	using Castle.Windsor;
	using Commands;
	using Modules.Core.Domain.Services.Ctes;

	/// <summary>
	/// Classe ProcessoLimpezaDiretorio
	/// </summary>
	public class ProcessoLimpezaDiretorio
	{
		private readonly ManualResetEvent _doneEvent;
		private readonly CteRunnerLogService _cteRunnerLogService;
		private readonly EfetuarLimpezaDiretorioCteService _efetuarLimpezaDiretorioCteService;

		/// <summary>
		/// Construtor ProcessoLimpezaDiretorio
		/// </summary>
		/// <param name="container">Container injetado.</param>
		/// <param name="cteRunnerLogService">Servi�o de log do runner</param>
		public ProcessoLimpezaDiretorio(IWindsorContainer container, CteRunnerLogService cteRunnerLogService)
		{
			_cteRunnerLogService = cteRunnerLogService;
			_efetuarLimpezaDiretorioCteService = container.Resolve<EfetuarLimpezaDiretorioCteService>();
		}

		/// <summary>
		/// Executa o m�todo em single thread
		/// </summary>
		public void Executar()
		{
			try
			{
				ProcessarLimpeza();
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Wrapper method for use with thread pool.
		/// </summary>
		/// <param name="threadContext"> The thread context. </param>
		public void ThreadPoolCallback(object threadContext)
		{
			try
			{
				int threadIndex = (int)threadContext;
				Console.WriteLine(@"thread {0} started...", threadIndex);
				ProcessarLimpeza();
				Console.WriteLine(@"thread {0} result calculated...", threadIndex);
			}
			finally
			{
				_doneEvent.Set();
			}
		}

		private void ProcessarLimpeza()
		{
			try
			{
				_cteRunnerLogService.InserirLogInfo("ProcessarLimpeza", string.Format("processando a limpeza do diret�rio"));
				_efetuarLimpezaDiretorioCteService.Executar();
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}