﻿namespace Translogic.CteRunner.Services
{
	using System;
	using System.Threading;
	using Commands;
	using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

	/// <summary>
	/// Processo para gerar arquivo
	/// </summary>
	public class ProcessoGeracaoArquivo
	{
		private readonly Cte _cte;
		private readonly ManualResetEvent _doneEvent;
		private readonly ICommandCte _commandoCte;
		private readonly CteRunnerLogService _cteRunnerLogService;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoGeracaoArquivo"/> class.
		/// </summary>
		/// <param name="cte"> Objeto cte. </param>
		/// <param name="commandoCte"> The commando cte. </param>
		/// <param name="doneEvent"> The done event. </param>
		/// <param name="cteRunnerLogService">Serviço de log do runner.</param>
		public ProcessoGeracaoArquivo(Cte cte, ICommandCte commandoCte, ManualResetEvent doneEvent, CteRunnerLogService cteRunnerLogService)
		{
			_cte = cte;
			_commandoCte = commandoCte;
			_doneEvent = doneEvent;
			_cteRunnerLogService = cteRunnerLogService;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessoGeracaoArquivo"/> class.
		/// </summary>
		/// <param name="cte"> Objeto cte. </param>
		/// <param name="commandoCte"> The commando cte. </param>
		/// <param name="cteRunnerLogService">Serviço de log do runner.</param>
		public ProcessoGeracaoArquivo(Cte cte, ICommandCte commandoCte, CteRunnerLogService cteRunnerLogService)
		{
			_cte = cte;
			_commandoCte = commandoCte;
			_doneEvent = null;
			_cteRunnerLogService = cteRunnerLogService;
		}

		/// <summary>
		/// Executa o método em single thread
		/// </summary>
		public void Executar()
		{
			try
			{
				ProcessarArquivo();
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// Wrapper method for use with thread pool.
		/// </summary>
		/// <param name="threadContext"> The thread context. </param>
		public void ThreadPoolCallback(object threadContext)
		{
			try
			{
				int threadIndex = (int)threadContext;
				Console.WriteLine(@"thread {0} started...", threadIndex);
				ProcessarArquivo();
				Console.WriteLine(@"thread {0} result calculated...", threadIndex);
			}
			finally 
			{
				_doneEvent.Set();
			}
		}

		private void ProcessarArquivo()
		{
			try
			{
				if (_cte != null)
				{
					_cteRunnerLogService.InserirLogInfo("ProcessoGeracaoArquivo", string.Format("processando a geração do Cte {0}", _cte.Id));
					_commandoCte.Executar(_cte);
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}
	}
}