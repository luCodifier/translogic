namespace Translogic.CteRunner.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Net;
    using System.Threading;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.Codificador;
    using Modules.Core.Domain.Model.Codificador.Repositories;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Services.Ctes;
    using Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.CteRunner.Helper;

    /// <summary>
    /// Servi�o de gerenciamento de importa��o dos arquivos de XML
    /// </summary>
    public class ImporterXmlFileManagerService
    {
        private const string NomeIntervaloTimer = "IntervaloImportadorXMLEmSegundos";
        private const string ClassName = "ImporterXmlFileManagerService";
        private readonly string _chaveXmlPath = "CTE_XML_PATH";
        private readonly CteService _cteService;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly IWindsorContainer _container;
        private readonly IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private bool _executando;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImporterXmlFileManagerService"/> class.
        /// </summary>
        /// <param name="cteService"> Servi�o do Cte Injetado</param>
        /// <param name="cteRunnerLogService">Servi�o do log do cte runner injetado </param>
        /// <param name="container"> Container injetado </param>
        /// <param name="configuracaoTranslogicRepository">Configura��o do translogic injetado</param>
        public ImporterXmlFileManagerService(CteService cteService, CteRunnerLogService cteRunnerLogService, IWindsorContainer container, IConfiguracaoTranslogicRepository configuracaoTranslogicRepository)
        {
            _container = container;
            _cteService = cteService;
            _cteRunnerLogService = cteRunnerLogService;
            _configuracaoTranslogicRepository = configuracaoTranslogicRepository;
            ////_cteRunnerLogService.InserirLogInfo(ClassName, "criado o servi�o");
        }

        /// <summary>
        /// Inicializa o timer
        /// </summary>
        public void Inicializar()
        {
            int tempoSegundos;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                string aux = config.AppSettings.Settings[NomeIntervaloTimer].Value;
                Int32.TryParse(aux, out tempoSegundos);
                if (tempoSegundos.Equals(0))
                {
                    tempoSegundos = 30;
                    Log.SetWarning(ClassName, "Inicializar", "CTE RUNNER: N�o foi poss�vel atribuir o intervalo do tempo, atualizado para 30");
                }
            }
            catch (Exception ex)
            {
                tempoSegundos = 30;
                Log.SetError(ClassName, "Inicializar", ex);
            }

            _executando = false;
            new ThreadIntervalXmlImporter(ExecutarImportacaoSingleThread, TimeSpan.FromSeconds(tempoSegundos)).Start();

            ////_cteRunnerLogService.InserirLogInfo(ClassName, string.Format("tempo de execu��o de {0} em {0} segundos", tempoSegundos));
        }

        /// <summary>
        /// Executa a importa��o dos arquivos de pdf - Single Thread
        /// </summary>
        public void ExecutarImportacaoSingleThread()
        {
            if (_executando)
            {
                return;
            }

            try
            {
                ConfiguracaoTranslogic xmlPath = _configuracaoTranslogicRepository.ObterPorId(_chaveXmlPath);

                _cteRunnerLogService.InserirLogInfo(ClassName, "executando o processo de varredura dos dados");
                _executando = true;

                string hostName = Dns.GetHostName();
                IList<CteInterfaceXmlConfig> listaCtesNaoImportados = _cteService.ObterCtesImportarXml(hostName);

                _cteRunnerLogService.InserirLogInfo(ClassName, string.Format("importando os {0} PDF para o banco de dados", listaCtesNaoImportados.Count));

                Log.SetInformation(ClassName, "ExecutarImportacaoSingleThread", string.Format("Foram identificados {0} registros para processar", listaCtesNaoImportados.Count));

                foreach (CteInterfaceXmlConfig cteInterfaceXmlConfig in listaCtesNaoImportados)
                {
                    _cteRunnerLogService.InserirLogInfo("ImporterPdfFileManagerService", string.Format("colocando na fila de processamento para importa��o o Cte: {0} ", cteInterfaceXmlConfig.Cte.Id));

                    ProcessoImportacaoXmlCte processoImportacao = new ProcessoImportacaoXmlCte(_container, _cteRunnerLogService, cteInterfaceXmlConfig, xmlPath);
                    processoImportacao.Executar();
                }
            }
            catch (Exception ex)
            {
                _cteRunnerLogService.InserirLogErro(ClassName, string.Format("{0}", ex.Message), ex);
                Log.SetError(ClassName, "ExecutarImportacaoSingleThread", ex);
            }
            finally
            {
                _executando = false;
            }
        }
    }

    /// <summary>
    /// Executa o delegate a cada intervalo
    /// </summary>
    public class ThreadIntervalXmlImporter
    {
        private readonly BackgroundWorker _worker;
        private bool _executarImediatamente;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        /// <param name="delegateInterval">O que ser� executado a cada intervalo</param>
        /// <param name="interval">Intervalo que ser� executado o delegate</param>
        public ThreadIntervalXmlImporter(Action delegateInterval, TimeSpan interval)
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;

            _worker.DoWork += delegate
            {
                if (_executarImediatamente)
                {
                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }

                while (true)
                {
                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    Thread.Sleep(interval);

                    if (_worker.CancellationPending)
                    {
                        return;
                    }

                    try
                    {
                        delegateInterval.Invoke();
                    }
                    catch
                    {
                    }
                }
            };
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        public void Start()
        {
            Start(false);
        }

        /// <summary>
        /// Inicia a execu��o
        /// </summary>
        /// <param name="executarImediatamente">Indica que � para executar imediatamente</param>
        public void Start(bool executarImediatamente)
        {
            _executarImediatamente = executarImediatamente;

            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Cancela a execu��o
        /// </summary>
        public void Stop()
        {
            _worker.CancelAsync();
        }
    }
}