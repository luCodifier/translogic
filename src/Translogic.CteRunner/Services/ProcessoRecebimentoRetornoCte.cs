﻿namespace Translogic.CteRunner.Services
{
    using System;
    using System.Threading;
    using Castle.Windsor;
    using Modules.Core.Domain.Model.Diversos.Cte;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Modules.Core.Domain.Services.Ctes;

    /// <summary>
    /// Processo para receber o arquivo
    /// </summary>
    public class ProcessoRecebimentoRetornoCte
    {
        private readonly CteService _cteService;
        private readonly ICteRepository _cteRepository;
        private readonly CteRunnerLogService _cteRunnerLogService;
        private readonly ManualResetEvent _doneEvent;
        private readonly CteRecebimentoPooling _itemPooling;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessoRecebimentoRetornoCte"/> class.
        /// </summary>
        /// <param name="container"> Container injetado. </param>
        /// <param name="doneEvent"> The done event. </param>
        /// <param name="cteRunnerLogService">Serviço de log do runner.</param>
        /// <param name="itemPooling">Item do pooling do recebimento</param>
        public ProcessoRecebimentoRetornoCte(IWindsorContainer container, ManualResetEvent doneEvent, CteRunnerLogService cteRunnerLogService, CteRecebimentoPooling itemPooling)
        {
            _cteService = container.Resolve<CteService>();
            _cteRepository = container.Resolve<ICteRepository>();
            _doneEvent = doneEvent;
            _cteRunnerLogService = cteRunnerLogService;
            _itemPooling = itemPooling;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessoRecebimentoRetornoCte"/> class.
        /// </summary>
        /// <param name="container"> Container injetado. </param>
        /// <param name="cteRunnerLogService">Serviço de log do runner.</param>
        /// /// <param name="itemPooling">Item do pooling do recebimento</param>
        public ProcessoRecebimentoRetornoCte(IWindsorContainer container, CteRunnerLogService cteRunnerLogService, CteRecebimentoPooling itemPooling)
        {
            _cteService = container.Resolve<CteService>();
            _cteRepository = container.Resolve<ICteRepository>();
            _doneEvent = null;
            _cteRunnerLogService = cteRunnerLogService;
            _itemPooling = itemPooling;
        }

        /// <summary>
        /// Executa no modo single thread
        /// </summary>
        public void Executar()
        {
            ProcessarRetorno();
        }

        /// <summary>
        /// Wrapper method for use with thread pool.
        /// </summary>
        /// <param name="threadContext"> The thread context. </param>
        public void ThreadPoolCallback(object threadContext)
        {
            try
            {
                ProcessarRetorno();
            }
            finally
            {
                _doneEvent.Set();
            }
        }

        private void ProcessarRetorno()
        {
            // Sis.LogTrace("CteRunner ProcessarRetorno");

            if (_itemPooling != null)
            {
                try
                {
                    _cteRunnerLogService.InserirLogInfo("ProcessoRecebimentoRetornoCte", string.Format("Processando o recebimento do Cte {0}", _itemPooling.Id));

                    if (_cteService.ProcessarCteRetorno(_itemPooling))
                    {
                        _cteRunnerLogService.InserirLogInfo("ProcessoRecebimentoRetornoCte", string.Format("Apagando o Cte {0} da interface", _itemPooling.Id));
                    }
                    else
                    {
                        _cteRunnerLogService.InserirLogInfo("ProcessoRecebimentoRetornoCte", string.Format("Não processado / encontrado o Cte {0} da interface", _itemPooling.Id));
                    }
                }
                catch (Exception ex)
                {
                    // var versao = _cteVersaoRepository.ObterVigente();
                    // Cte cteAux = new Cte { Id = _itemPooling.Id, Versao = versao };

                    Sis.LogException(ex, "Erro ao processar CTE: Id = " + _itemPooling.Id);

                    _cteRunnerLogService.InserirLogErro("ProcessoRecebimentoRetorno", string.Format("{0}", ex.Message), ex);

                    Cte cteAux = _cteRepository.ObterCtePorIdHql(_itemPooling.Id.Value);
                    _cteService.MudarSituacaoCte(cteAux, SituacaoCteEnum.Erro, null, new CteStatusRetorno { Id = 14 }, "Alterado a situação do Cte para ERRO", ex);
                }
            }
        }
    }
}