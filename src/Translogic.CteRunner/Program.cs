﻿namespace Translogic.CteRunner
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Text;
    using System.Threading;
    using Speed.Common;
    using Topshelf;
    using Translogic.CteRunner.Helper;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe estática que inicia ao programa
    /// </summary>
    public static class Program
    {
        private const string ClassName = "Program";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Argumentos enviados ao programa</param>
        public static void Main(string[] args)
        {
            try
            {
                Sis.Inicializar(EnumSistema.CteRunner, "Job");
                LogConfiguration();

#if DEBUG
                Testes();
                return;
#endif

#if !DEBUG
                Thread.Sleep(5000);
#endif

                HostFactory.Run(x =>
                                    {
                                        x.Service<CteRunnerSetupService>(s =>
                                                                             {
                                                                                 s.ConstructUsing(
                                                                                     name => new CteRunnerSetupService());
                                                                                 s.WhenStarted(tc => tc.Start(true));
                                                                                 s.WhenStopped(tc => tc.Stop());
                                                                             });

                                        x.RunAsLocalSystem();

                                        x.SetDescription("Serviço de CteRunner");
                                        x.SetDisplayName("Translogic.CteRunner");
                                        x.SetServiceName("Translogic.CteRunner");
                                    });

                Log.SetInformation(ClassName, "Main", "Fim");
            }
            catch (Exception ex)
            {
                Log.SetError(ClassName, "Main", ex);
            }
        }

        private static void LogConfiguration()
        {
            var value = new StringBuilder();
            value.AppendLine("CTERUNNER AppSettings of config:");
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "environmentType", GetValueConfig("environmentType")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "QuantidadeDiasParaLimpeza", GetValueConfig("QuantidadeDiasParaLimpeza")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IndAmbienteSefaz", GetValueConfig("IndAmbienteSefaz")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerFtpEmSegundos", GetValueConfig("IntervaloTimerFtpEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerEnvioEmSegundos", GetValueConfig("IntervaloTimerEnvioEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerRecebimentoEmSegundos", GetValueConfig("IntervaloTimerRecebimentoEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerImportadorEmSegundos", GetValueConfig("IntervaloTimerImportadorEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerInterfaceSap", GetValueConfig("IntervaloTimerInterfaceSap")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloImportadorXMLEmSegundos", GetValueConfig("IntervaloImportadorXMLEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloSenderEmail", GetValueConfig("IntervaloSenderEmail")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "IntervaloTimerLimpezaEmSegundos", GetValueConfig("IntervaloTimerLimpezaEmSegundos")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "LogInformacao", GetValueConfig("LogInformacao")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "LogErro", GetValueConfig("LogErro")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "ClientSettingsProvider.ServiceUri", GetValueConfig("ClientSettingsProvider.ServiceUri")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "SenderConfigService", GetValueConfig("SenderConfigService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "ReceiverConfigService", GetValueConfig("ReceiverConfigService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "SenderInterfaceSapService", GetValueConfig("SenderInterfaceSapService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "ImporterPdfFileService", GetValueConfig("ImporterPdfFileService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "ImporterXmlFileService", GetValueConfig("ImporterXmlFileService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "SenderEmailService", GetValueConfig("SenderEmailService")));
            value.AppendLine(string.Format("KEY: {0}, Value: {1}", "EfetuarLimpezaDiretorioService", GetValueConfig("EfetuarLimpezaDiretorioService")));

            ////log.InformationLog(value.ToString());
            Log.SetInformation(ClassName, "LogConfiguration", value.ToString());
        }

        private static string GetValueConfig(string key)
        {
            var retorno = "Key not found";
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            if (config.AppSettings.Settings[key] != null)
            {
                retorno = config.AppSettings.Settings[key].Value;
            }

            return retorno;
        }

#if DEBUG
        private static void Testes()
        {
            (new CteRunnerSetupService()).Start(false);

            if (false)
            {
                var cte = Core.TranslogicStarter.Container.Resolve<Modules.Core.Domain.Services.Ctes.CteService>();
                cte.EnviarEmail(7399672, Modules.Core.Domain.Model.Diversos.Cte.TipoArquivoEnvioEnum.PdfXml);
                return;
            }

            if (false)
            {
                var srv = Core.TranslogicStarter.Container.Resolve<Services.SenderEmailManagerService>();
                srv.Inicializar();
                srv.ExecutarImportacaoSingleThread();
            }

            if (false)
            { 
                var srv = Core.TranslogicStarter.Container.Resolve<Services.SenderManagerInterfaceSapService>();
                srv.Inicializar();
                srv.ExecutarEnvioSingleThread();
            }

            if (true)
            {
                var srv = Core.TranslogicStarter.Container.Resolve<Services.SenderQueueManagerService>();
                srv.InicializarParametros();

                do
                {
                    srv.ExecutarGeracaoSingleThread();
                }
                while (true);
            }

            if (false)
            {
                var srv = Core.TranslogicStarter.Container.Resolve<Services.ReceiverManagerService>();
                srv.Inicializar(false);

                do
                {
                    srv.ExecutarRecebimentoSingleThread();
                }
                while (true);
            }

            if (false)
            {
                var srv = Core.TranslogicStarter.Container.Resolve<Services.ImporterPdfFileManagerService>();
                srv.Inicializar(false);
                srv.ExecutarImportacaoSingleThread();
            }

            return;
        }
#endif
    }
}