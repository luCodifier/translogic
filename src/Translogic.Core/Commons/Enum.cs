namespace Translogic.Core.Commons
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Linq;
	using System.Reflection;

	/// <summary>
	/// Provide access to enum helpers
	/// </summary>
	/// <typeparam name="T">
	/// Tipo gen�rico do Enum
	/// </typeparam>
	public static class Enum<T>
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private static readonly Type EnumType;
		private static readonly DescribedEnumHandler<T> Handler;

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Inicializa membros est�ticos da classe
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Retorna exce��o se o tipo gen�rico n�o for um Enumeration
		/// </exception>
		static Enum()
		{
			EnumType = typeof(T);
			if (EnumType.IsEnum == false)
			{
				throw new ArgumentException(string.Format("{0} is not an enum", EnumType));
			}

			Handler = new DescribedEnumHandler<T>();
		}

		#endregion

		#region M�TODOS EST�TICOS

		/// <summary>
		/// Gets the enum value for a given description or value
		/// </summary>
		/// <param name="descriptionOrName">The enum value or description</param>
		/// <returns>An enum value matching the given string value, as description (using <see cref="DescriptionAttribute"/>) or as value</returns>
		public static T From(string descriptionOrName)
		{
			return Handler.GetValueFrom(descriptionOrName);
		}

		/// <summary>
		/// Extract the description for a given enum value
		/// </summary>
		/// <param name="value">An enum value</param>
		/// <returns>It's description, or it's name if there's no registered description for the given value</returns>
		public static string GetDescriptionOf(T value)
		{
			return Handler.GetDescriptionFrom(value);
		}

		#endregion
	}

	/// <summary>
	/// Used to cache enum values descriptions mapper
	/// </summary>
	/// <typeparam name="T">
	/// Tipo gen�rico do enum
	/// </typeparam>
	public class DescribedEnumHandler<T>
	{
		#region CONSTANTES

		private const BindingFlags PUBLIC_STATIC = BindingFlags.Public | BindingFlags.Static;

		#endregion

		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly IDictionary<string, T> _fromDescription = new Dictionary<string, T>();
		private readonly IDictionary<T, string> _toDescription = new Dictionary<T, string>();

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Creates a new DescribedEnumHandler instance for a given enum type
		/// </summary>
		public DescribedEnumHandler()
		{
			var type = typeof(T);
			var enumEntrys = from f in type.GetFields(PUBLIC_STATIC)
			                 let attributes = f.GetCustomAttributes(typeof(DescriptionAttribute), false)
			                 let description =
			                 	attributes.Length == 1
			                 		? ((DescriptionAttribute) attributes[0]).Description
			                 		: f.Name
			                 select new
			                        	{
			                        		Value = (T) Enum.Parse(type, f.Name),
			                        		Description = description
			                        	};

			foreach (var enumEntry in enumEntrys)
			{
				_toDescription[enumEntry.Value] = enumEntry.Description;
				_fromDescription[enumEntry.Description] = enumEntry.Value;
			}
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Extracts the description for the given enum value.
		/// <remarks>if no description was set for the given value, it's name will be retrieved</remarks>
		/// </summary>
		/// <param name="value">The enum value</param>
		/// <returns>The value's description</returns>
		public string GetDescriptionFrom(T value)
		{
			return _toDescription[value];
		}

		/// <summary>
		/// Parse the given string and return the enum value for with the given string acts as description
		/// </summary>
		/// <param name="description">The given description</param>
		/// <returns>A matching enum value</returns>
		public T GetValueFrom(string description)
		{
			return _fromDescription[description];
		}

		#endregion
	}
}