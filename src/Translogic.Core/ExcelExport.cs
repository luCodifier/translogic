using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Reflection;
using System.Data;
using Speed.Common;
using System.Runtime.Serialization;
using System.Data.Common;
using System.Collections;
using Amazon.DataPipeline.Model;
using Speed.IO;

namespace Translogic.Core
{

    /// <summary>
    /// Classe de exportação para o Excel, no formato xlsx
    /// Ex:
    ///        var list = new List<Teste>();
    ///        for (int i = 1; i <= 100; i++)
    ///            list.Add(new Teste { Title = "Title " + i, Date = DateTime.Now.AddDays(i), Value = i + i });
    ///        List<ReportColumn> columns = new List<ReportColumn>();
    ///        columns.Add(new ReportColumn { ColumnName = "Title", Title = "Título", ColumnType = ReportColumnType.Text, Format = "", Width = 30 });
    ///        columns.Add(new ReportColumn { ColumnName = "Date", Title = "Data", ColumnType = ReportColumnType.Date, Width = 12, Format = "dd/MM/yyyy" });
    ///        columns.Add(new ReportColumn { ColumnName = "Value", Title = "Valor", ColumnType = ReportColumnType.Numeric, Width = 10 });
    ///        ExcelExport.Export<Teste>("Relatório de Testes", "./Report.xlsx", columns, list, 5, 2, "./Template.xlsx");
    /// </summary>

    public sealed class ExcelExport : IDisposable
    {

        private ExcelPackage Package;
        /// <summary>
        /// Current WorkSheet
        /// </summary>
        private ExcelWorksheet ws { get; set; }
        private int row { get; set; }
        private Color backColor { get; set; }
        private Color fontColor { get; set; }

        public ExcelExport()
        {
        }


        #region Dispose

        /// <summary>
        /// Dispose dos recursos
        /// </summary>
        public void Dispose()
        {
            if (Package != null)
            {
                Package.Dispose();
                Package = null;
            }
        }

        #endregion Dispose

        #region Export Reader

        public static string ExportReader(DbDataReader reader, string title, string fileName, int firstRow = 1, int firstCol = 1, string template = null, List<ReportColumn> columns = null, string reportTitle = null, int maxRows = 0, string[] columnNames = null, Func<DbDataReader, string, object, object> actionDatareader = null)
        {
            using (var excel = new ExcelExport())
                return excel.exportDataReader(reader, title, fileName, firstRow, firstCol, template, columns, reportTitle, maxRows, columnNames, actionDatareader);
        }
        private string exportDataReader(DbDataReader reader, string title, string fileName, int firstRow = 1, int firstCol = 1, string template = null, List<ReportColumn> columns = null, string reportTitle = null, int maxRows = 0, string[] columnNames = null, Func<DbDataReader, string, object, object> actionDatareader = null)
        {
            string fullName = null;

            if (fileName == null)
                fileName = Path.Combine(Path.GetTempPath(), title + " - " + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + ".xlsx");

            FileInfo file = new FileInfo(fileName);
            if (!file.Directory.Exists)
                file.Directory.Create();
            if (file.Exists)
                file.Delete();

            bool useTemmplate = template != null;
            var temp = useTemmplate ? new FileInfo(template) : null;

            Package = useTemmplate ? new ExcelPackage(file, temp) : new ExcelPackage(file);

            using (Package)
            {
                if (!useTemmplate)
                    CreateWorkSheet(title ?? reportTitle);
                else
                    CreateWorkSheet(title ?? reportTitle);
                //ws = Package.Workbook.Worksheets[1];

                List<ReportColumn> cols = new List<ReportColumn>();

                if (columns != null)
                {
                    cols = columns;

                    //// checa se foram passadas os nomes das propriedades ao invés dos nomes das colunas da tabela
                    //var dicCols = columns.ToDictionary(p => p.ColumnName, StringComparer.OrdinalIgnoreCase);
                    //for (int i = 0; i < reader.FieldCount; i++)
                    //{
                    //    var colIndex = i + firstCol;
                    //    string colName = reader.GetName(i);
                    //    ReportColumn rc;
                    //    if (dicCols.TryGetValue(GetPropertyName(colName), out rc))
                    //    {
                    //        if (string.IsNullOrEmpty(rc.Title))
                    //            rc.Title = rc.ColumnName;
                    //        rc.ColumnName = colName;

                    //        Type type = reader.GetFieldType(i);
                    //        if (type == typeof(string))
                    //        {
                    //            rc.ColumnType = ReportColumnType.Text;
                    //            if (rc.HorizontalAlignment == null)
                    //                rc.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    //        }
                    //        else if (type == typeof(DateTime) || type == typeof(DateTime?))
                    //        {
                    //            rc.ColumnType = ReportColumnType.Date;
                    //            if (rc.HorizontalAlignment == null)
                    //                rc.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //            rc.Format = "d/m/yyyy hh:mm;@"; // @"dd\/mm\/yyyy\ hh:mm"; ;
                    //        }
                    //        else
                    //        {
                    //            rc.ColumnType = ReportColumnType.Numeric;
                    //            if (rc.HorizontalAlignment == null)
                    //                rc.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    //        }
                    //        cols.Add(rc);
                    //    }
                    //}
                }
                else if (columnNames != null)
                {
                    var dicCols = columnNames.ToDictionary(p => p, StringComparer.OrdinalIgnoreCase);

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var colIndex = i + firstCol;
                        ReportColumn rc = new ReportColumn();
                        rc.ColumnName = reader.GetName(i);

                        if (dicCols.ContainsKey(rc.ColumnName))
                        {
                            Type type = reader.GetFieldType(i);
                            if (type == typeof(string))
                            {
                                rc.ColumnType = ReportColumnType.Text;
                                if (rc.HorizontalAlignment == null)
                                    rc.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            }
                            else if (type == typeof(DateTime) || type == typeof(DateTime?))
                            {
                                rc.ColumnType = ReportColumnType.Date;
                                if (rc.HorizontalAlignment == null)
                                    rc.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                rc.Format = "d/m/yyyy hh:mm;@"; // @"dd\/mm\/yyyy\ hh:mm"; ;
                            }
                            else if (type == typeof(int) || type == typeof(int?) || type == typeof(long) || type == typeof(long?) || type == typeof(byte) || type == typeof(byte?))
                            {
                                rc.ColumnType = ReportColumnType.Integer;
                                if (rc.HorizontalAlignment == null)
                                    rc.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                rc.Format = "#,##0";
                            }
                            else
                            {
                                rc.ColumnType = ReportColumnType.Numeric;
                                if (rc.HorizontalAlignment == null)
                                    rc.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                            cols.Add(rc);
                        }
                    }

                }
                else
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        var colIndex = i + firstCol;
                        ReportColumn rc = new ReportColumn();
                        rc.ColumnName = reader.GetName(i);

                        Type type = reader.GetFieldType(i);
                        if (type == typeof(string))
                        {
                            rc.ColumnType = ReportColumnType.Text;
                            if (rc.HorizontalAlignment == null)
                                rc.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else if (type == typeof(DateTime) || type == typeof(DateTime?))
                        {
                            rc.ColumnType = ReportColumnType.Date;
                            if (rc.HorizontalAlignment == null)
                                rc.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            rc.Format = "d/m/yyyy hh:mm;@"; // @"dd\/mm\/yyyy\ hh:mm"; ;
                        }
                        else if (type == typeof(int) || type == typeof(int?) || type == typeof(long) || type == typeof(long?) || type == typeof(byte) || type == typeof(byte?))
                        {
                            rc.ColumnType = ReportColumnType.Integer;
                            if (rc.HorizontalAlignment == null)
                                rc.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            rc.Format = "#,##0";
                        }
                        else
                        {
                            rc.ColumnType = ReportColumnType.Numeric;
                            if (rc.HorizontalAlignment == null)
                                rc.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                        cols.Add(rc);
                    }
                }

                row = firstRow;

                for (int i = 2; i <= cols.Count; i++)
                {
                    ws.Column(i).Width = 11;
                    ws.Cells[row, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[row, i].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#063e61"));
                    ws.Cells[row, i].Style.Font.Color.SetColor(Color.White);
                }

                ws.Cells[row, 1, row, cols.Count].AutoFilter = true;

                //if (reportTitle != null)
                //{
                //    Config(ColorTranslator.FromHtml("#063e61"), Color.White, true, 35);
                //    SetReportTitle(1, reportTitle);
                //    firstRow += 2;
                //}

                row = firstRow;
                Config(ColorTranslator.FromHtml("#063e61"), Color.White, true, 16);

                ws.View.FreezePanes(2, 1);

                int row1 = row + 1;
                int row2;

                for (int i = 0; i < cols.Count; i++)
                {
                    var colName = cols[i].ColumnName;
                    var caption = cols[i].Title ?? cols[i].ColumnName;
                    var colIndex = i + firstCol;

                    if (cols[i].Width == 0)
                    {
                        ReportColumnType type = cols[i].ColumnType;
                        if (type == ReportColumnType.Text)
                            cols[i].Width = 20;
                        else if (type == ReportColumnType.Date)
                            cols[i].Width = 15;
                        else
                            cols[i].Width = 10;
                    }

                    SetHeader(colIndex, caption, cols[i].Width);
                }

                var readerCols = new List<string>();
                for (int i = 0; i < reader.FieldCount; i++)
                    readerCols.Add(reader.GetName(i));

                for (int i = 0; i < cols.Count; i++)
                {
                    if (readerCols.Exists(p => p.Equals(cols[i].ColumnName)))
                        cols[i].Exists = true;
                }

                while (reader.Read())
                {
                    row++;

                    ws.Row(row).Height = 13;

                    if (maxRows > 0 && row > maxRows)
                        throw new Exception("Favor filtrar mais os resultados ou exporte para CSV. Excedeu o número máximo, de " + maxRows + ", linhas para exportação para o Excel.");

                    for (int i = 0; i < cols.Count; i++)
                    {
                        var col = cols[i];
                        var colName = col.ColumnName;
                        var colIndex = i + firstCol;

                        object val = col.Exists ? reader[colName] : "";
                        if (actionDatareader != null)
                            val = actionDatareader(reader, colName, val);

                        ReportColumnType type = cols[i].ColumnType;
                        if (val != null && !string.IsNullOrEmpty(val.ToString()))
                        {
                            if (type == ReportColumnType.Text)
                            {
                                val = (val ?? "").ToString();
                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Left);
                            }
                            else if (type == ReportColumnType.Date)
                            {
                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Center);
                            }
                            else if (col.ColumnType == ReportColumnType.Numeric || col.ColumnType == ReportColumnType.Integer)
                            {
                                double db;
                                if ((val ?? "").ToString() != "****")
                                {
                                    if (!double.TryParse((val ?? "0").ToString(), out db))
                                        val = 0;
                                    else
                                        val = db;
                                }

                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Right);
                                //if (col.HorizontalAlignment == null)
                                //    col.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                //SetCell(colIndex, val);
                            }
                            //else if (col.ColumnType == ReportColumnType.TimeSpanHM)
                            //{
                            //    val = (val ?? "").ToString();
                            //    // SetCell(colIndex, val, null, null, ExcelHorizontalAlignment.Left);
                            //    col.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            //    SetCell(colIndex, val);
                            //}
                            else
                            {
                                if (val as string != "****")
                                {
                                    double db;
                                    if (!double.TryParse((val ?? "0").ToString(), out db))
                                        val = 0;
                                    else
                                        val = db;
                                }

                                SetCell(colIndex, val);
                            }
                        }
                        else
                        {
                            val = "";
                            SetCell(colIndex, val);
                        }
                    }
                }

                row2 = Math.Max(row, row1);

                // formata por range, que é muito mais rápido e eficiente, pq o arquivo fica com um tamanho bem menor

                ExcelRange range = ws.Cells[row1, 1, row2, cols.Count];

                Package.Save();
                fullName = Package.File.FullName;
                Package.Dispose();
                Package = null;
            }
            return fullName;
        }

        public static string Export(DataTable tb, string title, string usuario, string fileName, List<ReportColumn> columns, int firstRow = 1, int firstCol = 1, string template = null)
        {
            using (var excel = new ExcelExport())
                return excel.ExportDataTable(tb, title, usuario, fileName, columns, firstRow, firstCol, template);
        }

        public string ExportDataTable(DataTable tb, string title, string usuario, string fileName, List<ReportColumn> columns, int firstRow = 1, int firstCol = 1, string template = null)
        {
            TimerCount tc = new TimerCount("ExportDataTable");

            string fullName = null;

            if (fileName == null)
                fileName = Path.Combine(Path.GetTempPath(), title + " - " + DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss") + ".xlsx");

            FileInfo file = new FileInfo(fileName);
            if (!file.Directory.Exists)
                file.Directory.Create();
            if (file.Exists)
                file.Delete();

            bool useTemmplate = template != null;
            var temp = useTemmplate ? new FileInfo(template) : null;

            if (columns == null)
                columns = GetReportColumns(tb);

            Package = useTemmplate ? new ExcelPackage(file, temp) : new ExcelPackage(file);

            using (Package)
            {
                if (!useTemmplate)
                    CreateWorkSheet(title);
                else
                    ws = Package.Workbook.Worksheets[1];

                for (int i = 2; i <= columns.Count; i++)
                    ws.Column(i).Width = 10;
                //ws.defaultColWidth = 10;

                //ws.SetValue("A2", title);
                ws.Cells["A1"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                ws.Cells["B1"].Value = usuario;

                tc.Next("Config");
                row = firstRow;
                Config(ColorTranslator.FromHtml("#063e61"), Color.White, true, 16);

                for (int i = 0; i < columns.Count; i++)
                {
                    var colName = columns[i].ColumnName;
                    var caption = columns[i].Title ?? columns[i].ColumnName;
                    var colIndex = i + firstCol;

                    if (columns[i].Width == 0)
                    {
                        ReportColumnType type = columns[i].ColumnType;
                        if (type == ReportColumnType.Text)
                            columns[i].Width = 20;
                        else if (type == ReportColumnType.Date)
                            columns[i].Width = 15;
                        else if (type == ReportColumnType.Color)
                            columns[i].Width = 2;
                        else
                            columns[i].Width = 10;
                    }

                    SetHeader(colIndex, caption, columns[i].Width);
                }

                ws.Cells[row, 1, row, columns.Count].AutoFilter = true;

                int row1 = row + 1;
                int row2;

                foreach (DataRow data in tb.Rows)
                {
                    row++;

                    for (int i = 1; i <= columns.Count; i++)
                    {
                        var colName = columns[i - 1].ColumnName;
                        var col = columns[i - 1];
                        var colIndex = i + firstCol - 1;

                        object val = null;

                        try
                        {
                            val = data[colName];
                        }
                        catch { }

                        if (val != null && !string.IsNullOrEmpty(val.ToString()))
                        {
                            if (col.ColumnType == ReportColumnType.Color)
                            {
                                val = (val ?? "").ToString();
                                if (!string.IsNullOrWhiteSpace(val.ToString()))
                                {
                                    var cell = ws.Cells[row, i];
                                    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    try
                                    {
                                        cell.Style.Fill.BackgroundColor.SetColor(Color.FromName(val.ToString()));
                                    }
                                    catch { }
                                }
                            }
                            else if (col.ColumnType == ReportColumnType.Text)
                            {
                                val = (val ?? "").ToString();
                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Left);
                                if (col.HorizontalAlignment == null)
                                    col.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                //SetCell(colIndex, val);
                            }
                            else if (col.ColumnType == ReportColumnType.Numeric || col.ColumnType == ReportColumnType.Integer)
                            {
                                double db;
                                if (!double.TryParse((val ?? "0").ToString(), out db))
                                    val = 0;
                                else
                                    val = db;

                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Right);
                                //if (col.HorizontalAlignment == null)
                                //    col.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                //SetCell(colIndex, val);
                            }
                            else if (col.ColumnType == ReportColumnType.Date)
                            {
                                SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Center);
                                //if (col.HorizontalAlignment == null)
                                //    col.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //SetCell(colIndex, val);
                            }
                        }
                        else
                        {
                            val = "";
                            SetCell(colIndex, val);
                            if (col.HorizontalAlignment == null)
                                col.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                    }
                }

                row2 = Math.Max(row, row1);

                // Config(Color.White, Color.Black, false, 16);
                // formata por range, que é muito mais rápido e eficiente, pq o arquivo fica com um tamanho bem menor

                tc.Next("Range");
                ExcelRange range = ws.Cells[row1, 1, row2, columns.Count];

                for (int i = 1; i <= columns.Count; i++)
                {
                    var col = columns[i - 1];
                    var colIndex = i + firstCol - 1;
                    range = ws.Cells[row1, colIndex, row2, colIndex];
                    range.Style.Font.Size = 8;

                    range.Style.HorizontalAlignment = col.HorizontalAlignment ?? ExcelHorizontalAlignment.Left;
                    range.Style.VerticalAlignment = col.VerticalAlignment ?? ExcelVerticalAlignment.Center;
                    if (col.Format != null)
                        range.Style.Numberformat.Format = col.Format;
                }

                ws.View.FreezePanes(2, 1);

                tc.Next("Save");
                Package.Save();
                fullName = Package.File.FullName;
                Package.Dispose();
                Package = null;
            }
            return fullName;
        }

        /// <summary>
        /// Converte o nome da coluna no banco para o da propriedade do objeto
        /// </summary>
        /// <param name="dbColumnName"></param>
        /// <returns></returns>
        private static string GetPropertyName(string dbColumnName)
        {
            return dbColumnName.Replace(" ", "").Replace("_", "");
        }

        #endregion Export Reader

        public static string Export<T>(string title, Dictionary<string, Func<T, string>> fields, IList<T> list)
        {
            var cols = new List<ReportColumn>[1];

            using (var excel = new ExcelExport())
                return excel.ExportReport(title, fields, list);
        }

        public static string Export(string title, string usuario, string fileName, List<ReportColumn> columns, object list, int firstRow = 1, int firstCol = 1, string template = null)
        {
            var cols = new List<ReportColumn>[1];
            cols[0] = columns;

            using (var excel = new ExcelExport())
                return excel.ExportReport(new string[] { title }, usuario, fileName, cols, new object[1] { list }, firstRow, firstCol, template);
        }

        public static string Export(string[] titles, string usuario, string fileName, List<ReportColumn>[] cols, object[] lists, int firstRow = 1, int firstCol = 1, string template = null)
        {
            using (var excel = new ExcelExport())
                return excel.ExportReport(titles, usuario, fileName, cols, lists, firstRow, firstCol, template);
        }

        public string ExportReport(string[] titles, string usuario, string fileName, List<ReportColumn>[] cols, object[] lists, int firstRow = 1, int firstCol = 1, string template = null)
        {
            string fullName = null;

            if (fileName == null)
                fileName = Path.Combine(Path.GetTempPath(), titles[0] + " - " + DateTime.Now.ToString("dd-MM-yyyy HH-mm") + ".xlsx");

            FileInfo file = new FileInfo(fileName);
            if (!file.Directory.Exists)
                file.Directory.Create();
            if (file.Exists)
                file.Delete();

            bool useTemmplate = template != null;
            var temp = useTemmplate ? new FileInfo(template) : null;

            Package = useTemmplate ? new ExcelPackage(file, temp) : new ExcelPackage(file);

            using (Package)
            {
                for (int j = 0; j < titles.Length; j++)
                {
                    var title = titles[j];
                    var columns = cols[j];
                    var list = lists[j];

                    if (!useTemmplate)
                        CreateWorkSheet(title);
                    else
                        ws = Package.Workbook.Worksheets[1];

                    for (int i = 2; i <= columns.Count; i++)
                        ws.Column(i).Width = 10;
                    //ws.defaultColWidth = 10;

                    //ws.SetValue("A2", title);
                    ws.Cells["A1"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    ws.Cells["B1"].Value = usuario;

                    row = firstRow;
                    Config(ColorTranslator.FromHtml("#063e61"), Color.White, true, 16);

                    for (int i = 1; i <= columns.Count; i++)
                    {
                        var col = columns[i - 1];
                        var colIndex = i + firstCol - 1;
                        SetHeader(colIndex, col.Title, col.Width);
                    }
                    ws.Cells[row, 1, row, columns.Count].AutoFilter = true;
                    ws.Row(row).Height = 25;

                    Type type = GetGenericElementType(list.GetType());
                    Dictionary<string, PropertyInfo> props = type.GetProperties().Distinct(p => p.Name.ToUpperInvariant()).ToDictionary(p => p.Name, StringComparer.OrdinalIgnoreCase);

                    int row1 = row + 1;
                    int row2;

                    foreach (var data in (IEnumerable)list)
                    {
                        row++;
                        //Config(Color.White, Color.Black, false, 16);
                        for (int i = 1; i <= columns.Count; i++)
                        {
                            var col = columns[i - 1];
                            var colIndex = i + firstCol - 1;

                            object val = null;

                            try
                            {
                                val = props[col.ColumnName].GetValue(data, null);
                            }
                            catch (Exception ex)
                            {
                                val = null;
                            }

                            if (val != null && !string.IsNullOrEmpty(val.ToString()))
                            {
                                if (col.Title.ContainsI("HH:MM"))
                                {
                                    SetCell(colIndex, ToExcelDate(Conv.ToStringN(val)));
                                    //SetFormula(colIndex, val == null ? null : "=value(\"" + val + "\")");
                                }
                                else if (col.ColumnType == ReportColumnType.Text)
                                {
                                    val = (val ?? "").ToString();
                                    SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Left);
                                    //col.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    //SetCell(colIndex, val);
                                }
                                else if (col.ColumnType == ReportColumnType.Numeric || col.ColumnType == ReportColumnType.Integer)
                                {
                                    double db;
                                    if (!double.TryParse((val ?? "0").ToString(), out db))
                                        val = 0;
                                    else
                                        val = db;

                                    SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Right);
                                    //col.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    //SetCell(colIndex, val);
                                }
                                else if (col.ColumnType == ReportColumnType.Date)
                                {
                                    SetCell(colIndex, val, null, col.Format, col.HorizontalAlignment ?? ExcelHorizontalAlignment.Center);
                                    //col.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    //SetCell(colIndex, val);
                                }
                            }
                            else
                            {
                                val = "";
                                SetCell(colIndex, val);

                                if (col.HorizontalAlignment == null)
                                    col.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            }
                        }
                    }

                    row2 = row;

                    // formata por range, que é muito mais rápido e eficiente, pq o arquivo fica com um tamanho bem menor


                    // Config(Color.White, Color.Black, false, 16);
                    try
                    {
                        ExcelRange range = ws.Cells[row1, 1, row2, columns.Count];
                        for (int i = 1; i <= columns.Count; i++)
                        {
                            var col = columns[i - 1];
                            var colIndex = i + firstCol - 1;

                            range = ws.Cells[row1, colIndex, row2, colIndex];

                            range.Style.Font.Size = 8;

                            range.Style.HorizontalAlignment = col.HorizontalAlignment ?? ExcelHorizontalAlignment.Left;
                            range.Style.VerticalAlignment = col.VerticalAlignment ?? ExcelVerticalAlignment.Center;
                            if (col.Format != null)
                                range.Style.Numberformat.Format = col.Format;
                        }
                    }
                    catch { }

                    ws.View.FreezePanes(2, 1);
                }

                Package.Save();
                fullName = Package.File.FullName;
                Package.Dispose();
                Package = null;
            }
            return fullName;
        }

        public string ExportReport<T>(string reportTitle, Dictionary<string, Func<T, string>> fields, IList<T> list)
        {
            string fullName = null;

            if (reportTitle.Contains("."))
                reportTitle = reportTitle.Left(reportTitle.IndexOf("."));

            var fileName = Path.Combine(Path.GetTempPath(), FileTools.ToValidPath(reportTitle) + " - " + DateTime.Now.ToString("dd-MM-yyyy HH-mm") + ".xlsx");

            FileInfo file = new FileInfo(fileName);

            if (!file.Directory.Exists)
                file.Directory.Create();
            if (file.Exists)
                file.Delete();

            Package = new ExcelPackage(file);

            using (Package)
            {
                CreateWorkSheet(reportTitle);

                ws = Package.Workbook.Worksheets[1];

                for (int i = 2; i <= fields.Count; i++)
                {
                    ws.Column(i).Width = 10;
                }

                row = 1;

                Config(ColorTranslator.FromHtml("#063e61"), Color.White, true, 16);

                int numCols = fields.Keys.Count;
                for (int c = 0; c < numCols; c++)
                {
                    string key = fields.Keys.ElementAt(c);
                    SetHeader(c + 1, key, null);
                }

                ws.Cells[row, 1, row, numCols].AutoFilter = true;
                ws.Row(row).Height = 25;

                Type type = GetGenericElementType(list.GetType());
                Dictionary<string, PropertyInfo> props = type.GetProperties().Distinct(p => p.Name.ToUpperInvariant()).ToDictionary(p => p.Name, StringComparer.OrdinalIgnoreCase);

                int row1 = row + 1;
                int row2;

                for (int i = 0; i < list.Count; i++)
                {
                    row++;

                    var colIndex = 0;

                    foreach (var key in fields.Keys)
                    {
                        colIndex++;

                        Func<T, string> valor = fields[key];
                        string valorString = valor(list[i]) ?? string.Empty;

                        valorString = valorString.Replace("\n", " ");
                        valorString = valorString.Replace("\r", " ");

                        SetCell(colIndex, valorString);
                    }
                }

                row2 = row;

                // formata por range, que é muito mais rápido e eficiente, pq o arquivo fica com um tamanho bem menor


                // Config(Color.White, Color.Black, false, 16);
                try
                {
                    ExcelRange range = ws.Cells[row1, 1, row2, numCols];
                    for (int i = 1; i <= numCols; i++)
                    {
                        var colIndex = i + 1;
                        range = ws.Cells[row1, colIndex, row2, colIndex];
                        range.Style.Font.Size = 8;
                    }
                }
                catch { }

                ws.View.FreezePanes(2, 1);

                for (int i = 1; i <= numCols; i++)
                {
                    ws.Column(i).AutoFit();
                }

                Package.Save();
                fullName = Package.File.FullName;
                Package.Dispose();
                Package = null;
            }
            return fullName;
        }

        #region Excel

        /// <summary>
        /// Cria uma WorkSheet e seta ws como o WorkSheet atual
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ExcelWorksheet CreateWorkSheet(string name)
        {
            ExcelWorksheet ws = Package.Workbook.Worksheets.Add(name ?? "Relatório");
            this.ws = ws;
            return ws;
        }

        /// <summary>
        /// Configura as cores e fontes
        /// </summary>
        /// <param name="backColor"></param>
        /// <param name="fontColor"></param>
        /// <param name="fontBold"></param>
        public void Config(Color backColor, Color fontColor, bool fontBold, double rowHeigth = 10.25)
        {
            ExcelRow xRow = ws.Row(row);
            xRow.Height = rowHeigth;
            xRow.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
            xRow.Style.Font.Name = "Calibri";
            xRow.Style.Font.Size = 8;

            this.backColor = backColor;
            this.fontColor = fontColor;

        }

        public void SetCell(int col, object value, string formulaR1C1 = null, string numberFormat = null, ExcelHorizontalAlignment horizontalAlignment = ExcelHorizontalAlignment.Left)
        {
            using (var cell = ws.Cells[row, col])
            {
                if (value != null)
                    cell.Value = value;
                if (formulaR1C1 != null)
                    cell.FormulaR1C1 = formulaR1C1;
                if (numberFormat != null)
                    cell.Style.Numberformat.Format = numberFormat;
                if (horizontalAlignment != ExcelHorizontalAlignment.Left)
                    cell.Style.HorizontalAlignment = horizontalAlignment;

                //cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                //cell.Style.WrapText = true;
                //cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                //cell.Style.Fill.BackgroundColor.SetColor(backColor);
                //cell.Style.Font.Name = "Calibri";
                //cell.Style.Font.Size = 8;
                //cell.Style.Font.Bold = fontBold;
                //cell.Style.Font.Color.SetColor(fontColor);
                //// Border
                //SetBorder(cell.Style.Border, ExcelBorderStyle.Thin, Color.Black);
            }
        }

        public void SetReportTitle(int col, string text)
        {
            using (ExcelRange cell = ws.Cells[row, col])
            {
                cell.Value = text;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Style.WrapText = false;
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(backColor);
                cell.Style.Font.Name = "Calibri";
                cell.Style.Font.Size = 18;
                cell.Style.Font.Bold = true;
                cell.Style.Font.Color.SetColor(fontColor);
                //SetBorder(cell.Style.Border, ExcelBorderStyle.Thin, Color.Black);
            }
        }

        void SetBorder(Border border, ExcelBorderStyle style, Color color)
        {
            border.Bottom.Style = style;
            border.Left.Style = style;
            border.Top.Style = style;
            border.Right.Style = style;
            border.Bottom.Color.SetColor(color);
            border.Left.Color.SetColor(color);
            border.Top.Color.SetColor(color);
            border.Right.Color.SetColor(color);
        }

        public void SetHeader(int col, string name, double? width)
        {
            using (ExcelRange cell = ws.Cells[row, col])
            {
                cell.Value = name;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Style.WrapText = true;
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(backColor);
                cell.Style.Font.Name = "Calibri";
                cell.Style.Font.Size = 8;
                cell.Style.Font.Bold = true;
                cell.Style.Font.Color.SetColor(fontColor);
                if (width.HasValue)
                    ws.Column(cell.Start.Column).Width = width.Value;
                SetBorder(cell.Style.Border, ExcelBorderStyle.Thin, Color.Black);
            }
        }

        private static TimeSpan ToExcelDate(string hhmm)
        {
            hhmm = hhmm ?? "00:00";

            /*
            var vals = hhmm.Trim().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            var h = vals.Length > 0 ? vals[0].ToInt32() : 0;
            var m = vals.Length > 1 ? vals[1].ToInt32() : 0;

            //DateTime date = new DateTime(1900, 1, 1, 0, 0, 0);

            DateTime date = new DateTime(1900, 1, 1, 0, 0, 0).AddHours(h).AddMinutes(m);
            
            //return GetExcelDecimalValueForDate(date);
            //string s = date.ToString("hh:mm tt");
            //return s;
            return date;
            */

            TimeSpan ts;
            if (TimeSpan.TryParse(hhmm, out ts))
                return ts;
            else
                return TimeSpan.Zero;
        }

        #endregion Excel

        /// <summary>
        /// Gera ReportColumns de um DataTable
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        List<ReportColumn> GetReportColumns(DataTable tb)
        {
            var cols = new List<ReportColumn>();

            foreach (DataColumn col in tb.Columns)
            {
                ReportColumn column = new ReportColumn();
                column.ColumnName = col.ColumnName;
                column.Title = col.Caption ?? col.ColumnName;
                var type = col.DataType;

                if (col.ColumnName.EqualsICIC("Cor") || col.ColumnName.EqualsICIC("Color"))
                {
                    column.ColumnType = ReportColumnType.Color;
                    column.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    column.Width = 1;
                }
                else if (col.DataType == typeof(string))
                {
                    column.ColumnType = ReportColumnType.Text;
                    column.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    column.Width = 20;
                }
                else if (col.DataType == typeof(DateTime) || col.DataType == typeof(DateTime?))
                {
                    column.ColumnType = ReportColumnType.Date;
                    column.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    column.Format = "d/m/yyyy hh:mm;@"; // @"dd\/mm\/yyyy\ hh:mm"; ;
                    column.Width = 15;
                }
                else if (type == typeof(int) || type == typeof(int?) || type == typeof(long) || type == typeof(long?) || type == typeof(byte) || type == typeof(byte?))
                {
                    column.ColumnType = ReportColumnType.Integer;
                    if (column.HorizontalAlignment == null)
                        column.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    column.Format = "#,##0";
                }
                else
                {
                    column.ColumnType = ReportColumnType.Numeric;
                    column.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    column.Width = 10;
                }
                cols.Add(column);
            }

            return cols;
        }

        public static Type GetGenericElementType(Type type)
        {
            // Short-circuit for Array types
            if (typeof(Array).IsAssignableFrom(type))
            {
                return type.GetElementType();
            }

            while (true)
            {
                // Type is IEnumerable<T>
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                {
                    return type.GetGenericArguments().First();
                }

                // Type implements/extends IEnumerable<T>
                Type elementType = (from subType in type.GetInterfaces()
                                    let retType = GetGenericElementType(subType)
                                    where retType != subType
                                    select retType).FirstOrDefault();

                if (elementType != null)
                {
                    return elementType;
                }

                if (type.BaseType == null)
                {
                    return type;
                }

                type = type.BaseType;
            }
        }

    }

    public class ReportColumn
    {

        public string ColumnName { get; set; }
        public ReportColumnType ColumnType { get; set; }
        public string Title { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
        public ExcelHorizontalAlignment? HorizontalAlignment { get; set; }
        public ExcelVerticalAlignment? VerticalAlignment { get; set; }

        public bool Exists { get; set; }

        public ReportColumn()
        {
            ColumnType = ReportColumnType.Text;
        }

        public ReportColumn(string columnName, string title, int width, ReportColumnType columnType = ReportColumnType.Text, string format = null, ExcelHorizontalAlignment? horizontalAlignment = null, ExcelVerticalAlignment? verticalAlignment = null)
            : this()
        {
            this.ColumnName = columnName;
            this.Title = title;
            this.Width = width;
            this.ColumnType = columnType;
            this.HorizontalAlignment = horizontalAlignment;
            this.VerticalAlignment = verticalAlignment;

            if (columnType == ReportColumnType.Date && format == null)
                format = @"dd\/mm\/yyyy\ hh:mm"; ;
            if (columnType == ReportColumnType.Numeric && format == null)
                format = "#,##0.00";
            if (columnType == ReportColumnType.Integer && format == null)
                format = "#,##0";

            this.Format = format;
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}", ColumnName, this.ColumnType, this.Title);
        }

    }

    public enum ReportColumnType
    {
        Numeric,
        Integer,
        Text,
        Date,
        Color,
        //[EnumMember()]
        //TimeSpanHM
    }

    [Serializable]
    public sealed class SavePackage : IDisposable
    {

        public ExcelPackage Package { get; set; }

        // http://stackoverflow.com/questions/7488442/epplus-2-9-0-1-throws-system-io-isolatedstorage-isolatedstorageexception-when-tr

        public void CreatePackage(FileInfo file, bool useTemmplate, FileInfo template = null)
        {
            Package = useTemmplate ? new ExcelPackage(file, template) : new ExcelPackage(file);
        }

        public void Save()
        {
            Package.Save();
        }

        public void Dispose()
        {
            if (Package != null)
            {
                Package.Dispose();
                Package = null;
            }
        }

    }

}
