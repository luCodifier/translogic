using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#if !SILVERLIGHT
[assembly: SuppressIldasmAttribute()]
#endif
[assembly: CLSCompliantAttribute(true )]
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute("Translogic.Core")]
[assembly: AssemblyDescriptionAttribute("")]
[assembly: AssemblyCompanyAttribute("ALL - América Latina Logística S/A")]
[assembly: AssemblyProductAttribute("TRANSLOGIC")]
[assembly: AssemblyCopyrightAttribute("Copyright © ALL - América Latina Logística S/A 2012")]
[assembly: AssemblyVersionAttribute("3.0.0.0")]
[assembly: AssemblyInformationalVersionAttribute("3.0.0.0")]
[assembly: AssemblyFileVersionAttribute("3.0.0.0")]
[assembly: AssemblyDelaySignAttribute(false)]

