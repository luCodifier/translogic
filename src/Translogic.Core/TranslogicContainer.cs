namespace Translogic.Core
{
	using ALL.Core.IoC;
	using AutoMapper;
	using Castle.Facilities.NHibernateIntegration;
	using Castle.MicroKernel.Registration;
	using Infrastructure.DataAccess;
	using Infrastructure.Modules;
	// using Infrastructure.Web;
	// using Infrastructure.Web.Services;
	using NHibernate.Validator.Cfg.Loquacious;

	/// <summary>
	/// Container do Translogic
	/// </summary>
	public class TranslogicContainer : ContainerBase
	{
		#region M�TODOS

		/// <summary>
		/// Registra m�todos padr�es do Translogic
		/// </summary>
		public override void RegistrarServicosPadrao()
		{
			base.RegistrarServicosPadrao();

			Register(
				Component.For<IConfigurationContributor>().ImplementedBy<ModuleNHibernateConfigBuilder>(),
				Component.For<FluentConfiguration>().Named("dasdsa"),
				Component.For<ModuleSetupService>()
			);

			// Auto Mapper
			Mapper.Reset();
		}

		#endregion
	}
}