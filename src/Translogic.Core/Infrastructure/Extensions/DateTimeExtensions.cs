﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.Core.Infrastructure.Extensions
{
    /// <summary>
    /// Classe de extension methods de DateTime type
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Extension method para conversão de valores string para DateTime
        /// </summary>
        /// <param name="stringDateTime"></param>
        /// <returns></returns>
        public static DateTime? ConvertToDateTime(this string stringDateTime)
        {
            DateTime date;

            if (DateTime.TryParse(stringDateTime, out date)) 
                return date;

            return null;

        }
    }
}
