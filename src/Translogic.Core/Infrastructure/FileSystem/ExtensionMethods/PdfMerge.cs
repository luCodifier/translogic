namespace Translogic.Core.Infrastructure.FileSystem.ExtensionMethods
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using ICSharpCode.SharpZipLib.Core;
    using iTextSharp.text;
    using iTextSharp.text.pdf;

    /// <summary>
    /// Classe Responsavel por efetuar o merge de Pdf
    /// </summary>
    public class PdfMerge
    {
        private List<Stream> fileList = new List<Stream>();

        /// <summary>
        /// Retorna a quantidade de arquivos adicionados
        /// </summary>
        /// <returns>A quantidade de arquivos adicionados</returns>
        public int FileListCount()
        {
            var result = 0;

            if (fileList != null)
            {
                result = fileList.Count;
            }

            return result;
        }

        /// <summary>
        ///  Adiciona um arquivo
        /// </summary>
        /// <param name="file"> Arquivo a ser adicionado</param>
        public void AddFile(byte[] file)
        {
            Stream returnContent = new MemoryStream(file, 0, file.Length);
            fileList.Add(returnContent);
        }

        /// <summary>
        ///  Adiciona um arquivo
        /// </summary>
        /// <param name="file"> Arquivo a ser adicionado</param>
        public void AddFile(Stream file)
        {
            fileList.Add(file);
        }

        /// <summary>
        /// Efetua o merge dos arquivo
        /// </summary>
        /// <returns>Retorna o Pdf efetuado o merge</returns>
        public Stream Execute()
        {
            return MergeDocs();
        }

        /// <summary>
        /// Efetua o merge dos arquivos
        /// </summary>
        /// <returns>Retorna o Pdf efetuado o merge</returns>
        private Stream MergeDocs()
        {
            var document = new Document();

            try
            {
                var path = Path.GetTempFileName();
                var writer = PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));

                document.Open();

                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;

                int n = 0;
                int rotation = 0;

                foreach (var filename in fileList)
                {
                    PdfReader reader = new PdfReader(filename);
                    n = reader.NumberOfPages;

                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader.GetPageSizeWithRotation(1));
                        document.NewPage();

                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);

                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                    }
                }

                document.Close();

                var sr = new StreamReader(path);
                var ms = new MemoryStream();
                StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
                sr.Close();
                File.Delete(path);
                ms.Seek(0, SeekOrigin.Begin);

                return ms;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                document.Close();
            }
        }
    }
}