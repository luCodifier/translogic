﻿

namespace Translogic.Core.Infrastructure.FileSystem
{
    using System;
    using System.IO;
    using Ionic.Zip;
    using System.Collections.Specialized;
    using System.Collections.Generic;

    public class FileService
    {
        public void DeleteFilesInDirectory(string directoryPath)
        {
            var files = GetDirectoryFiles(directoryPath, false);
            if (files.Count > 0)
            {
                foreach (var file in files)
                {
                    System.IO.File.Delete(file);
                }
            }
        }

        public void DeleteAllFolders(string directoryPath)
        {
            var folders = GetDirectories(directoryPath);
            if (folders.Count > 0)
            {
                foreach (var folder in folders)
                {
                    System.IO.Directory.Delete(folder);
                }
            }
        }

        public string GetAndCreateDirectory(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            return directoryPath;
        }

        public void CopyFileStreamTo(Stream stream, string fileDirectory)
        {
            using (var fileStream = System.IO.File.Create(fileDirectory))
            {
                stream.CopyTo(fileStream);
                fileStream.Close();
                fileStream.Dispose();
            }
        }

        public void ExtractZipToDirectory(string fileZipDirectory, string directoryToExtract)
        {
            ZipFile zipFile = new ZipFile(fileZipDirectory);
            zipFile.ExtractAll(directoryToExtract);
            zipFile.Dispose();
        }

        public string ZipFiles(StringCollection files, string fileZipDirectory)
        {
            var zipFileName = Path.Combine(fileZipDirectory, "tfa.zip");
            using (ZipFile zip = new ZipFile("tfa.zip"))
            {
                foreach (var file in files)
                {
                    zip.AddFile(file, "");
                }

                zip.Save(zipFileName);

                return zipFileName;
            }
        }

        public string ZipDirectories(StringCollection directories, string zipFileDirectory, string zipFileName)
        {
            var zipFolderNameToSave = Path.Combine(zipFileDirectory, zipFileName);
            using (ZipFile zip = new ZipFile(zipFileName))
            {
                zip.AddDirectory(zipFileDirectory, "");
                zip.Save(zipFolderNameToSave);

                return zipFolderNameToSave;
            }
        }

        public MemoryStream ZipFiles(IList<FileZipDto> files)
        {
            var streamResult = new MemoryStream();

            using (ZipFile zip = new ZipFile())
            {

                foreach (var file in files)
                {
                    zip.AddEntry(file.NomeArquivo, file.bytes);
                }

                zip.Save(streamResult);

                streamResult.Seek(0, SeekOrigin.Begin);
                streamResult.Flush();

                return streamResult;
            }
        }

        public StringCollection GetDirectoryFiles(string directoryPath, bool ignoreZip)
        {
            var files = new StringCollection();
            GetFilesInDirectories(directoryPath, files, ignoreZip);
            return files;
        }

        public void DeleteDirectory(string directoryPath)
        {
            Directory.Delete(directoryPath, true);
        }

        public void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }

        private void GetFilesInDirectories(string path, StringCollection files, bool ignoreZip)
        {
            foreach (string d in Directory.GetDirectories(path))
            {
                foreach (string f in Directory.GetFiles(d))
                {
                    if (ignoreZip)
                    {
                        if (f.ToLower().Contains(".zip"))
                            files.Add(f);
                    }
                    else
                    {
                        files.Add(f);
                    }
                }
                GetFilesInDirectories(d, files, ignoreZip);
            }

            foreach (string f in Directory.GetFiles(path))
            {
                files.Add(f);
            }
        }

        public StringCollection GetDirectories(string path)
        {
            var directories = new StringCollection();
            foreach (string d in Directory.GetDirectories(path))
            {
                directories.Add(d);
                GetDirectories(d, directories);
            }
            return directories;
        }

        private void GetDirectories(string path, StringCollection directories)
        {
            foreach (string d in Directory.GetDirectories(path))
            {
                directories.Add(d);
                GetDirectories(d, directories);
            }
        }
      
    }

    public class FileZipDto
    {
        public string NomeArquivo { get; set; }
        public string TipoArquivo { get; set; }
        public string NumOs { get; set; }
        public byte[] bytes { get; set; }
    }
}
