﻿namespace Translogic.Core.Infrastructure.Threading
{
	using System;
	using System.ComponentModel;
	using System.Threading;

	/// <summary>
	/// Gerente de exeução de threads do translogic
	/// </summary>
	public class TranslogicThreadManager
	{
		private readonly BackgroundWorker _worker;
		private bool _executarImediatamente;

		/// <summary>
		/// Construtor padrão
		/// </summary>
		/// <param name="delegateInterval">O que será executado a cada intervalo</param>
		/// <param name="interval">Intervalo que será executado o delegate</param>
		public TranslogicThreadManager(Action delegateInterval, TimeSpan interval)
		{
			_worker = new BackgroundWorker();
			_worker.WorkerSupportsCancellation = true;

			_worker.DoWork += delegate
			{
				if (_executarImediatamente)
				{
					try
					{
						delegateInterval.Invoke();
					}
					catch
					{
					}
				}

				while (true)
				{
					if (_worker.CancellationPending)
					{
						return;
					}

					Thread.Sleep(interval);

					if (_worker.CancellationPending)
					{
						return;
					}

					try
					{
						delegateInterval.Invoke();
					}
					catch
					{
					}
				}
			};
		}

		/// <summary>
		/// Inicia a execução
		/// </summary>
		public void Start()
		{
			Start(false);
		}

		/// <summary>
		/// Inicia a execução
		/// </summary>
		/// <param name="executarImediatamente">Indica que é para executar imediatamente</param>
		public void Start(bool executarImediatamente)
		{
			_executarImediatamente = executarImediatamente;

			_worker.RunWorkerAsync();
		}

		/// <summary>
		/// Cancela a execução
		/// </summary>
		public void Stop()
		{
			_worker.CancelAsync();
		}
	}
}