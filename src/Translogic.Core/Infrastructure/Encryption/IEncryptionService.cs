namespace Translogic.Core.Infrastructure.Encryption
{
	/// <summary>
	/// Interface de servi�o de encripta��o
	/// </summary>
	public interface IEncryptionService
	{
		#region M�TODOS
		/// <summary>
		/// M�todo que faz a decripta��o de uma string
		/// </summary>
		/// <param name="content">String a ser decriptada</param>
		/// <returns>String decriptada</returns>
		string Decrypt(string content);

		/// <summary>
		/// M�todo que faz a encripta��o de uma string
		/// </summary>
		/// <param name="content">String a ser encriptada</param>
		/// <returns>String encriptada</returns>
		string Encrypt(string content);

		#endregion
	}
}