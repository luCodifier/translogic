namespace Translogic.Core.Infrastructure.Encryption
{
	using System;
	using System.Security.Cryptography;
	using System.Text;

	/// <summary>
	/// Servi�o de encripta��o implementando o algoritimo TripleDES
	/// </summary>
	public class TripleDESEncryptionService : IEncryptionService
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly byte[] _key = new byte[]
		                              	{
		                              		11, 250, 1, 6, 0x10, 0x12, 0x5b, 7, 0x5b, 0x5e, 250, 11, 250, 0x9f, 0x95, 1,
		                              		6, 0x10, 0x12, 0x5b, 7, 0x5b, 0x5e, 250
		                              	};

		#endregion

		#region M�TODOS

		/// <summary>
		/// Faz a decripta��o do texto
		/// </summary>
		/// <param name="content">
		/// String que ser� decriptada.
		/// </param>
		/// <returns>
		/// String decriptada.
		/// </returns>
		public string Decrypt(string content)
		{
			ICryptoTransform transform = CreateTranformer(false);
			var encoding = new UTF8Encoding(false);
			byte[] inputBuffer = Convert.FromBase64String(content);
			byte[] bytes = transform.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);
			return encoding.GetString(bytes);
		}

		/// <summary>
		/// Faz a encripta��o do texto
		/// </summary>
		/// <param name="content">String que ser� encriptada</param>
		/// <returns>String encriptada.</returns>
		public string Encrypt(string content)
		{
			ICryptoTransform transform = CreateTranformer(true);
			byte[] bytes = new UTF8Encoding(false).GetBytes(content);
			return Convert.ToBase64String(transform.TransformFinalBlock(bytes, 0, bytes.Length));
		}

		private static byte[] GenerateSalt()
		{
			var provider = new RNGCryptoServiceProvider();
			var data = new byte[20];
			provider.GetBytes(data);
			return data;
		}

		private ICryptoTransform CreateTranformer(bool forEncryption)
		{
			byte[] rgbKey = null;
			byte[] salt = null;
			byte[] rgbIV = null;
			ICryptoTransform transform;
			try
			{
				salt = GenerateSalt();
				var bytes = new PasswordDeriveBytes(_key, salt);
				rgbIV = new byte[8];
				rgbKey = bytes.CryptDeriveKey("TripleDES", "SHA1", 0xc0, rgbIV);
				if (forEncryption)
				{
					return TripleDES.Create().CreateEncryptor(rgbKey, rgbIV);
				}

				transform = TripleDES.Create().CreateDecryptor(rgbKey, rgbIV);
			}
			catch (CryptographicException)
			{
				transform = null;
			}
			finally
			{
				if (rgbKey != null)
				{
					Array.Clear(rgbKey, 0, rgbKey.Length);
				}

				if (salt != null)
				{
					Array.Clear(salt, 0, salt.Length);
				}

				if (rgbIV != null)
				{
					Array.Clear(rgbIV, 0, rgbIV.Length);
				}
			}

			return transform;
		}
		#endregion
	}
}