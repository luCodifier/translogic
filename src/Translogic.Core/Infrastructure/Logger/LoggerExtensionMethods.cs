﻿namespace Translogic.Core.Infrastructure.Logger
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using Castle.Core.Logging;
	using Speed.Data;

	/// <summary>
	/// Classe de extensão de logger
	/// </summary>
	/// <remarks>Criada esta classe pois no upgrade foi removido/renomeados alguns metodos</remarks>
	public static class LoggerExtensionMethods
	{
		/// <summary>
		/// Erro fatal
		/// </summary>
		/// <param name="logger"> Logger extendido. </param>
		/// <param name="message"> The message. </param>
		public static void FatalError(this ILogger logger, string message)
		{
			Sis.LogFatalError(message);
			logger.Fatal(message);
		}

		/// <summary>
		/// Erro fatal
		/// </summary>
		/// <param name="logger"> Logger extendido. </param>
		/// <param name="message"> The message</param>
		/// <param name="ex"> Excessão . </param>
		public static void FatalError(this ILogger logger, string message, Exception ex)
		{
			Sis.LogFatalError(ex, message);
			logger.Fatal(message, ex);
		}

		/// <summary>
		/// Log de info
		/// </summary>
		/// <param name="logger"> Logger extendido. </param>
		/// <param name="message"> The message. </param>
		/// <param name="parametros">The parameters</param>
		public static void Info(this ILogger logger, string message, params object[] parametros)
		{
			Sis.LogInfo(string.Join(message, parametros));
			logger.InfoFormat(message, parametros);
		}

		/// <summary>
		/// Log de debug
		/// </summary>
		/// <param name="logger"> Logger extendido. </param>
		/// <param name="message"> The message. </param>
		/// <param name="parametros">The parameters</param>
		public static void Debug(this ILogger logger, string message, params object[] parametros)
		{
			Sis.LogInfo(string.Join(message, parametros));
			logger.DebugFormat(message, parametros);
		}
	}
}
