namespace Translogic.Core.Infrastructure.Validation.Constraints
{
	using System;
	using NHibernate.Validator.Cfg.Loquacious;
	using NHibernate.Validator.Cfg.Loquacious.Impl;
	using NHibernate.Validator.Engine;

	/// <summary>
	/// M�todo de Extens�o para verifica��o de datas
	/// </summary>
	public static class DateTimeConstraintsExtensionMethods
	{
		#region M�TODOS EST�TICOS
		/// <summary>
		/// Verifica se a data � v�lida
		/// </summary>
		/// <param name="constraint">Restri��es de data - <see cref="IDateTimeConstraints"/></param>
		/// <returns>Op��es de regras - <see cref="IRuleArgsOptions"/></returns>
		public static IRuleArgsOptions IsValid(this IDateTimeConstraints constraint)
		{
			BaseConstraints baseConstraints = constraint as BaseConstraints;

			return baseConstraints.AddWithFinalRuleArgOptions(new IsValidDateAttribute());
		}

		#endregion
	}

	/// <summary>
	/// Atributo para verificar se a data � v�lida
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	[ValidatorClass(typeof(IsValidDateValidator))]
	public class IsValidDateAttribute : Attribute, IRuleArgs
	{
		// TODO : Add tolerance

		#region ATRIBUTOS

		private string _message = "{validator.future}";

		#endregion

		#region IRuleArgs MEMBROS

		/// <summary>
		/// Retorna a Mensagem
		/// </summary>
		public string Message
		{
			get { return _message; }
			set { _message = value; }
		}

		#endregion
	}

	/// <summary>
	/// Validador de data
	/// </summary>
	[Serializable]
	public class IsValidDateValidator : IValidator
	{
		#region IValidator MEMBROS
		/// <summary>
		/// Valida se a data informada � v�lida
		/// </summary>
		/// <param name="value">Data informada</param>
		/// <param name="constraintValidatorContext">Contexto de valida��o - <see cref="IConstraintValidatorContext"/></param>
		/// <returns>Valor booleano</returns>
		bool IValidator.IsValid(object value, IConstraintValidatorContext constraintValidatorContext)
		{
			if (value == null)
			{
				return false;
			}

			DateTime date;

			if (DateTime.TryParse(value.ToString(), out date))
			{
				return date > DateTime.MinValue;
			}

			return false;
		}

		#endregion
	}
}