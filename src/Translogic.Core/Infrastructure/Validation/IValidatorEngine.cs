namespace Translogic.Core.Infrastructure.Validation
{
	using NHibernate.Validator.Engine;

	/// <summary>
	/// Interface de valida��o de entidades persist�veis
	/// </summary>
	public interface IValidatorEngine
	{
		#region M�TODOS
		/// <summary>
		/// Verifica se o objeto est� v�lido
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		void AssertIsValid(object entity);

		/// <summary>
		/// Verifica se o objeto � v�lido
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		/// <returns>Valor booleano.</returns>
		bool IsValid(object entity);

		/// <summary>
		/// Valida se a entidade � v�lida
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		/// <returns>Lista de Valores inv�lidos</returns>
		InvalidValue[] Validate(object entity);

		#endregion
	}
}