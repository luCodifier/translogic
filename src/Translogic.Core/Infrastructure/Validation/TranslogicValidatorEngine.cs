namespace Translogic.Core.Infrastructure.Validation
{
	using System;
	using System.Text;
	using Exceptions;
	using NHibernate.Validator.Engine;

	/// <summary>
	/// Implementa��o de valida��o
	/// </summary>
	public class TranslogicValidatorEngine : ValidatorEngine, IValidatorEngine
	{
		#region IValidatorEngine MEMBROS

		/// <summary>
		/// Verifica se a entidade est� v�lida
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		public void AssertIsValid(object entity)
		{
			if (entity == null)
			{
				throw new ArgumentNullException("entity", "A entidade n�o pode ser nula.");
			}

			if (!IsValid(entity))
			{
				var builder = new StringBuilder();

				builder.AppendFormat("\nFalha na valida��o: {0}\n", entity.GetType().Name);

				foreach (var invalidValue in Validate(entity))
				{
					builder.AppendLine(invalidValue.Message);
				}

				throw new ValidationException(builder.ToString());
			}
		}

		/// <summary>
		/// Verifica se o objeto � v�lido
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		/// <returns>Valor booleano.</returns>
		public bool IsValid(object entity)
		{
			return base.IsValid(entity);
		}

		/// <summary>
		/// Valida se a entidade � v�lida
		/// </summary>
		/// <param name="entity">Entidade a ser verificada</param>
		/// <returns>Lista de Valores inv�lidos</returns>
		public InvalidValue[] Validate(object entity)
		{
			return base.Validate(entity);
		}

		#endregion
	}
}