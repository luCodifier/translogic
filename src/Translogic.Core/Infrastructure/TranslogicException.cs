namespace Translogic.Core.Infrastructure
{
	using System;
	using System.Runtime.Serialization;

	/// <summary>
	/// Classe de Exceção da Aplicação Translogic
	/// </summary>
    public class TranslogicException : ApplicationException
	{
		/// <summary>
		/// Inicializa uma nova instância da classe sem parametros
		/// </summary>
	    public TranslogicException()
	    {
	    }

		/// <summary>
		/// Inicializa uma nova instância da classe recebendo a mensagem de erro
		/// </summary>
		/// <param name="message">Mensagem do erro</param>
		public TranslogicException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Inicializa uma nova instância da classe recebendo a mensagem de erro com parâmetros
		/// </summary>
		/// <param name="format">String da mensagem de erro com parâmetros a serem substituídos</param>
		/// <param name="args">Parâmetros que substituirão na string</param>
		public TranslogicException(string format, params string[] args)
			: base(string.Format(format, args))
		{
		}

		/// <summary>
		/// Inicializa uma nova instância da classe recebendo a mensagem de erro e a exceção interna
		/// </summary>
		/// <param name="message">Mensagem de erro</param>
		/// <param name="innerException">Exceção interna</param>
	    public TranslogicException(string message, Exception innerException) : base(message, innerException)
	    {
	    }

		/// <summary>
		/// Inicializa uma nova instância da classe recebendo informações de serialização e o contexto
		/// </summary>
		/// <param name="info">Informações de serialização - <see cref="SerializationInfo"/></param>
		/// <param name="context">Contexto - <see cref="StreamingContext"/></param>
	    protected TranslogicException(SerializationInfo info, StreamingContext context) : base(info, context)
	    {
	    }
	}
}