namespace Translogic.Core.Infrastructure.WCF
{
	using System.Collections;
	using System.Collections.Generic;
	using System.ServiceModel.Channels;
	using System.ServiceModel.Description;
	using System.ServiceModel.Dispatcher;
	using System.Xml.Schema;
	using ServiceDescription = System.Web.Services.Description.ServiceDescription;

	/// <summary>
	/// Implementação para montar WSDL sem XSD's externos
	/// </summary>
	public class FlatWsdl : IWsdlExportExtension, IEndpointBehavior
	{
		#region MÉTODOS

		/// <summary>
		/// Adiciona parametros de binding no endpoint
		/// </summary>
		/// <param name="endpoint">Endpoint a ser configurado</param>
		/// <param name="bindingParameters">Parametros de binding</param>
		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
		{
		}

		/// <summary>
		/// Adiciona configuração de runtime do cliente no endpoint
		/// </summary>
		/// <param name="endpoint">Endpoint a ser configurado</param>
		/// <param name="clientRuntime">Runtime do client</param>
		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
		{
		}

		/// <summary>
		/// Aplica comportamento de envio
		/// </summary>
		/// <param name="endpoint">Endpoint a ser configurado</param>
		/// <param name="endpointDispatcher">Dispatcher de Endpoint - <see cref="EndpointDispatcher"/> </param>
		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
		{
		}

		/// <summary>
		/// Valida o Endpoint
		/// </summary>
		/// <param name="endpoint">Endpoint a ser configurado</param>
		public void Validate(ServiceEndpoint endpoint)
		{
		}

		/// <summary>
		/// Exporta os métodos da interface no WSDL
		/// </summary>
		/// <param name="exporter">Exportador para WSDL</param>
		/// <param name="context">Contexto de contrato WSDL</param>
		public void ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
		{
		}

		/// <summary>
		/// Exporta o endpoint
		/// </summary>
		/// <param name="exporter">Exportador para WSDL</param>
		/// <param name="context">Contexto de contrato WSDL</param>
		public void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
		{
			XmlSchemaSet schemaSet = exporter.GeneratedXmlSchemas;

			foreach (ServiceDescription wsdl in exporter.GeneratedWsdlDocuments)
			{
				List<XmlSchema> importsList = new List<XmlSchema>();

				foreach (XmlSchema schema in wsdl.Types.Schemas)
				{
					AddImportedSchemas(schema, schemaSet, importsList);
				}

				wsdl.Types.Schemas.Clear();

				foreach (XmlSchema schema in importsList)
				{
					RemoveXsdImports(schema);
					wsdl.Types.Schemas.Add(schema);
				}
			}
		}

		private void AddImportedSchemas(XmlSchema schema, XmlSchemaSet schemaSet, List<XmlSchema> importsList)
		{
			foreach (XmlSchemaImport import in schema.Includes)
			{
				ICollection realSchemas =
					schemaSet.Schemas(import.Namespace);

				foreach (XmlSchema ixsd in realSchemas)
				{
					if (!importsList.Contains(ixsd))
					{
						importsList.Add(ixsd);
						AddImportedSchemas(ixsd, schemaSet, importsList);
					}
				}
			}
		}

		private void RemoveXsdImports(XmlSchema schema)
		{
			for (int i = 0; i < schema.Includes.Count; i++)
			{
				if (schema.Includes[i] is XmlSchemaImport)
				{
					schema.Includes.RemoveAt(i--);
				}
			}
		}

		#endregion
	}
}