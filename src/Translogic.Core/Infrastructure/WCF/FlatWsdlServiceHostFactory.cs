namespace Translogic.Core.Infrastructure.WCF
{
	using System;
	using System.ServiceModel;
	using System.ServiceModel.Description;
	using Castle.Facilities.WcfIntegration;

	/// <summary>
	/// F�brica de Servi�o de FlatWsdl
	/// </summary>
	public sealed class FlatWsdlServiceHostFactory : DefaultServiceHostFactory
	{
		#region M�TODOS

		/// <summary>
		/// Cria ServiceHost
		/// </summary>
		/// <param name="constructorString">String de constru��o</param>
		/// <param name="baseAddresses">Endere�o base</param>
		/// <returns>Retorna ServiceHost</returns>
		public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
		{
			ServiceHostBase serviceHost = base.CreateServiceHost(constructorString, baseAddresses);

			ApplyWsdlBehaviors(serviceHost);

			return serviceHost;
		}

		/// <summary>
		/// Cria ServiceHost
		/// </summary>
		/// <param name="serviceType">Tipo do Servi�o</param>
		/// <param name="baseAddresses">Endere�o base</param>
		/// <returns>Retorna ServiceHost</returns>
		protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
		{
			ServiceHost serviceHost = base.CreateServiceHost(serviceType, baseAddresses);

			ApplyWsdlBehaviors(serviceHost);

			return serviceHost;
		}

		private void ApplyWsdlBehaviors(ServiceHostBase serviceHost)
		{
			foreach (ServiceEndpoint endpoint in serviceHost.Description.Endpoints)
			{
				endpoint.Behaviors.Add(new FlatWsdl());
			}
		}

		#endregion
	}
}