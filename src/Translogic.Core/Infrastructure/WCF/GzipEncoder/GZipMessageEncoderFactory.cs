﻿namespace Translogic.Core.Infrastructure.WCF.GzipEncoder
{
	//----------------------------------------------------------------
	// Copyright (c) Microsoft Corporation.  All rights reserved.
	//----------------------------------------------------------------
	using System;
	using System.IO;
	using System.IO.Compression;
	using System.ServiceModel.Channels;
	
	/// <summary>
	/// This class is used to create the custom encoder (GZipMessageEncoder)
	/// </summary>
	internal class GZipMessageEncoderFactory : MessageEncoderFactory
	{
		private MessageEncoder encoder;

		/// <summary>
		/// The GZip encoder wraps an inner encoder
		/// We require a factory to be passed in that will create this inner encoder
		/// </summary>
		/// <param name="messageEncoderFactory"> The message encoder factory. </param>
		public GZipMessageEncoderFactory(MessageEncoderFactory messageEncoderFactory)
		{
			if (messageEncoderFactory == null)
			{
				throw new ArgumentNullException("messageEncoderFactory", "A valid message encoder factory must be passed to the GZipEncoder");
			}
				
			this.encoder = new GZipMessageEncoder(messageEncoderFactory.Encoder);
		}

		/// <summary>
		/// The service framework uses this property to obtain an encoder from this encoder factory
		/// </summary>
		public override MessageEncoder Encoder
		{
			get { return this.encoder; }
		}

		/// <summary>
		/// Gets the message version.
		/// </summary>
		public override MessageVersion MessageVersion
		{
			get { return this.encoder.MessageVersion; }
		}

		/// <summary>
		/// This is the actual GZip encoder
		/// </summary>
		internal class GZipMessageEncoder : MessageEncoder
		{
			/// <summary>
			/// The gzip content type.
			/// </summary>
			private static string gzipContentType = "application/x-gzip";

			/// <summary>
			/// This implementation wraps an inner encoder that actually converts a WCF Message
			/// into textual XML, binary XML or some other format. This implementation then compresses the results.
			/// Theinternal  opposite happens when reading messages.
			/// This member stores this inner encoder.
			/// </summary>
			private MessageEncoder innerEncoder;

			/// <summary>
			/// Initializes a new instance of the <see cref="GZipMessageEncoder"/> class.
			/// </summary>
			/// <param name="messageEncoder">
			/// The message encoder.
			/// </param>
			/// <remarks>We require an inner encoder to be supplied (see comment above)</remarks>
			internal GZipMessageEncoder(MessageEncoder messageEncoder) : base()
			{
				if (messageEncoder == null)
				{
					throw new ArgumentNullException("messageEncoder", "A valid message encoder must be passed to the GZipEncoder");
				}
					
				this.innerEncoder = messageEncoder;
			}

			/// <summary>
			/// Gets the content type.
			/// </summary>
			public override string ContentType
			{
				get { return gzipContentType; }
			}

			/// <summary>
			/// Gets the media type.
			/// </summary>
			public override string MediaType
			{
				get { return gzipContentType; }
			}

			/// <summary>
			/// SOAP version to use - we delegate to the inner encoder for this
			/// </summary>
			public override MessageVersion MessageVersion
			{
				get { return this.innerEncoder.MessageVersion; }
			}

			/// <summary>
			/// One of the two main entry points into the encoder. Called by WCF to decode a buffered byte array into a Message.
			/// </summary>
			/// <param name="buffer"> The buffer. </param>
			/// <param name="bufferManager"> The buffer manager. </param>
			/// <param name="contentType"> The content type. </param>
			/// <returns> The <see cref="Message"/>. </returns>
			public override Message ReadMessage(ArraySegment<byte> buffer, BufferManager bufferManager, string contentType)
			{
				// Decompress the buffer
				ArraySegment<byte> decompressedBuffer = DecompressBuffer(buffer, bufferManager);
				// Use the inner encoder to decode the decompressed buffer
				Message returnMessage = this.innerEncoder.ReadMessage(decompressedBuffer, bufferManager);
				returnMessage.Properties.Encoder = this;
				return returnMessage;
			}

			/// <summary>
			/// One of the two main entry points into the encoder. Called by WCF to encode a Message into a buffered byte array.
			/// </summary>
			/// <param name="message"> The message. </param>
			/// <param name="maxMessageSize"> The max message size. </param>
			/// <param name="bufferManager"> The buffer manager. </param>
			/// <param name="messageOffset"> The message offset. </param>
			/// <returns> The <see cref="ArraySegment"/>. </returns>
			public override ArraySegment<byte> WriteMessage(Message message, int maxMessageSize, BufferManager bufferManager, int messageOffset)
			{
				// Use the inner encoder to encode a Message into a buffered byte array
				ArraySegment<byte> buffer = this.innerEncoder.WriteMessage(message, maxMessageSize, bufferManager, 0);

				// Compress the resulting byte array
				// Console.WriteLine("Original size: {0}", buffer.Count);
				buffer = CompressBuffer(buffer, bufferManager, messageOffset);
				// Console.WriteLine("Compressed size: {0}", buffer.Count);

				return buffer;
			}

			/// <summary>
			/// The read message.
			/// </summary>
			/// <param name="stream"> The stream. </param>
			/// <param name="maxSizeOfHeaders"> The max size of headers. </param>
			/// <param name="contentType"> The content type. </param>
			/// <returns> The <see cref="Message"/>. </returns>
			public override Message ReadMessage(Stream stream, int maxSizeOfHeaders, string contentType)
			{
				// Pass false for the "leaveOpen" parameter to the GZipStream constructor.
				// This will ensure that the inner stream gets closed when the message gets closed, which
				// will ensure that resources are available for reuse/release.
				GZipStream gzipStream = new GZipStream(stream, CompressionMode.Decompress, false);
				return this.innerEncoder.ReadMessage(gzipStream, maxSizeOfHeaders);
			}

			/// <summary>
			/// The write message.
			/// </summary>
			/// <param name="message"> The message. </param>
			/// <param name="stream"> The stream. </param>
			public override void WriteMessage(Message message, Stream stream)
			{
				using (GZipStream gzipStream = new GZipStream(stream, CompressionMode.Compress, true))
				{
					this.innerEncoder.WriteMessage(message, gzipStream);
				}

				// innerEncoder.WriteMessage(message, gzStream) depends on that it can flush data by flushing 
				// the stream passed in, but the implementation of GZipStream.Flush will not flush underlying
				// stream, so we need to flush here.
				stream.Flush();
			}

			/// <summary>
			/// Helper method to compress an array of bytes
			/// </summary>
			/// <param name="buffer">Message buffer</param>
			/// <param name="bufferManager">Buffer Manager</param>
			/// <param name="messageOffset">Message Offset</param>
			/// <returns>ArraySegment of bytes</returns>
			internal static ArraySegment<byte> CompressBuffer(ArraySegment<byte> buffer, BufferManager bufferManager, int messageOffset)
			{
				MemoryStream memoryStream = new MemoryStream();

				using (GZipStream stream = new GZipStream(memoryStream, CompressionMode.Compress, true))
				{
					stream.Write(buffer.Array, buffer.Offset, buffer.Count);
				}

				byte[] compressedBytes = memoryStream.ToArray();
				int totalLength = messageOffset + compressedBytes.Length;
				byte[] bufferedBytes = bufferManager.TakeBuffer(totalLength);

				Array.Copy(compressedBytes, 0, bufferedBytes, messageOffset, compressedBytes.Length);

				bufferManager.ReturnBuffer(buffer.Array);
				// ArraySegment<byte> byteArray = new ArraySegment<byte>(bufferedBytes, messageOffset, bufferedBytes.Length - messageOffset);
				ArraySegment<byte> byteArray = new ArraySegment<byte>(bufferedBytes, messageOffset, compressedBytes.Length);
				return byteArray;
			}

			/// <summary>
			/// Helper method to decompress an array of bytes
			/// </summary>
			/// <param name="buffer">Message Buffer</param>
			/// <param name="bufferManager">Buffer Manger</param>
			/// <returns>ArraySegment of bytes</returns>
			internal static ArraySegment<byte> DecompressBuffer(ArraySegment<byte> buffer, BufferManager bufferManager)
			{
				MemoryStream memoryStream = new MemoryStream(buffer.Array, buffer.Offset, buffer.Count);
				MemoryStream decompressedStream = new MemoryStream();
				int totalRead = 0;
				int blockSize = 1024;
				byte[] tempBuffer = bufferManager.TakeBuffer(blockSize);
				using (GZipStream stream = new GZipStream(memoryStream, CompressionMode.Decompress))
				{
					while (true)
					{
						int bytesRead = stream.Read(tempBuffer, 0, blockSize);
						if (bytesRead == 0)
						{
							break;
						}
							
						decompressedStream.Write(tempBuffer, 0, bytesRead);
						totalRead += bytesRead;
					}
				}

				bufferManager.ReturnBuffer(tempBuffer);

				byte[] decompressedBytes = decompressedStream.ToArray();
				byte[] bufferManagerBuffer = bufferManager.TakeBuffer(decompressedBytes.Length + buffer.Offset);
				Array.Copy(buffer.Array, 0, bufferManagerBuffer, 0, buffer.Offset);
				Array.Copy(decompressedBytes, 0, bufferManagerBuffer, buffer.Offset, decompressedBytes.Length);

				ArraySegment<byte> byteArray = new ArraySegment<byte>(bufferManagerBuffer, buffer.Offset, decompressedBytes.Length);
				bufferManager.ReturnBuffer(buffer.Array);

				return byteArray;
			}
		}
	}
}