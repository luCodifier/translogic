namespace Translogic.Core.Infrastructure.WCF.GzipEncoder
{
	/// <summary>
	/// This is constants for GZip message encoding policy.
	/// </summary>
	public static class GZipMessageEncodingPolicyConstants
	{
		/// <summary>
		/// The g zip encoding name.
		/// </summary>
		public const string GZipEncodingName = "GZipEncoding";

		/// <summary>
		/// The g zip encoding namespace.
		/// </summary>
		public const string GZipEncodingNamespace = "http://schemas.microsoft.com/ws/06/2004/mspolicy/netgzip1";

		/// <summary>
		/// The g zip encoding prefix.
		/// </summary>
		public const string GZipEncodingPrefix = "gzip";
	}
}