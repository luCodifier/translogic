namespace Translogic.Core.Infrastructure.WCF.GzipEncoder
{
	using System;
	using System.ServiceModel.Channels;
	using System.ServiceModel.Description;
	using System.Xml;

	/// <summary>
	/// This is the binding element that, when plugged into a custom binding, will enable the GZip encoder
	/// </summary>
	public sealed class GZipMessageEncodingBindingElement
		: MessageEncodingBindingElement, // BindingElement
		  IPolicyExportExtension
	{
		// We will use an inner binding element to store information required for the inner encoder
		private MessageEncodingBindingElement innerBindingElement;

		/// <summary>
		/// Initializes a new instance of the <see cref="GZipMessageEncodingBindingElement"/> class.
		/// </summary>
		/// <remarks>By default, use the default text encoder as the inner encoder</remarks>
		public GZipMessageEncodingBindingElement() : this(new TextMessageEncodingBindingElement())
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GZipMessageEncodingBindingElement"/> class.
		/// </summary>
		/// <param name="messageEncoderBindingElement"> The message encoder binding element. </param>
		public GZipMessageEncodingBindingElement(MessageEncodingBindingElement messageEncoderBindingElement)
		{
			this.innerBindingElement = messageEncoderBindingElement;
		}

		/// <summary>
		/// Gets or sets the inner message encoding binding element.
		/// </summary>
		public MessageEncodingBindingElement InnerMessageEncodingBindingElement
		{
			get { return this.innerBindingElement; }
			set { this.innerBindingElement = value; }
		}

		/// <summary>
		/// Gets or sets the message version.
		/// </summary>
		public override MessageVersion MessageVersion
		{
			get { return this.innerBindingElement.MessageVersion; }
			set { this.innerBindingElement.MessageVersion = value; }
		}

		/// <summary>
		/// Main entry point into the encoder binding element. Called by WCF to get the factory that will create the message encoder
		/// </summary>
		/// <returns> The <see cref="MessageEncoderFactory"/>. </returns>
		public override MessageEncoderFactory CreateMessageEncoderFactory()
		{
			return new GZipMessageEncoderFactory(this.innerBindingElement.CreateMessageEncoderFactory());
		}

		/// <summary>
		/// The clone.
		/// </summary>
		/// <returns> The <see cref="BindingElement"/>. </returns>
		public override BindingElement Clone()
		{
			return new GZipMessageEncodingBindingElement(this.innerBindingElement);
		}

		/// <summary>
		/// The get property.
		/// </summary>
		/// <param name="context"> The context. </param>
		/// <typeparam name="T"> Generic Type </typeparam>
		/// <returns> The generic type<see cref="T"/>. </returns>
		public override T GetProperty<T>(BindingContext context)
		{
			if (typeof(T) == typeof(XmlDictionaryReaderQuotas))
			{
				return this.innerBindingElement.GetProperty<T>(context);
			}
			
			return base.GetProperty<T>(context);
		}

		/// <summary>
		/// The build channel factory.
		/// </summary>
		/// <param name="context"> The context. </param>
		/// <typeparam name="TChannel"> Generic type of channel </typeparam> 
		/// <returns> The channel factory <see cref="IChannelFactory"/>. </returns>
		public override IChannelFactory<TChannel> BuildChannelFactory<TChannel>(BindingContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			context.BindingParameters.Add(this);
			return context.BuildInnerChannelFactory<TChannel>();
		}

		/// <summary>
		/// The build channel listener.
		/// </summary>
		/// <param name="context"> The context. </param>
		/// <typeparam name="TChannel"> generic type of channel </typeparam> 
		/// <returns> The <see cref="IChannelListener"/>. </returns>
		public override IChannelListener<TChannel> BuildChannelListener<TChannel>(BindingContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			context.BindingParameters.Add(this);
			return context.BuildInnerChannelListener<TChannel>();
		}

		/// <summary>
		/// The can build channel listener.
		/// </summary>
		/// <param name="context"> The context. </param>
		/// <typeparam name="TChannel"> generic type of channel </typeparam>
		/// <returns> The bool value. </returns>
		public override bool CanBuildChannelListener<TChannel>(BindingContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			context.BindingParameters.Add(this);
			return context.CanBuildInnerChannelListener<TChannel>();
		}

		/// <summary>
		/// Implement to include for exporting a custom policy assertion about bindings.
		/// </summary>
		/// <param name="exporter"> The <see cref="T:System.ServiceModel.Description.MetadataExporter"/> that you can use to modify the exporting process. </param>
		/// <param name="policyContext"> Policy context </param>
		void IPolicyExportExtension.ExportPolicy(MetadataExporter exporter, PolicyConversionContext policyContext)
		{
			if (policyContext == null)
			{
				throw new ArgumentNullException("policyContext");
			}

			XmlDocument document = new XmlDocument();
			policyContext.GetBindingAssertions().Add(document.CreateElement(
				GZipMessageEncodingPolicyConstants.GZipEncodingPrefix,
				GZipMessageEncodingPolicyConstants.GZipEncodingName,
				GZipMessageEncodingPolicyConstants.GZipEncodingNamespace));
		}
	}
}