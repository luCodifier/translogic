﻿namespace Translogic.Core.Infrastructure.WCF.GzipEncoder
{
	using System;
	using System.Collections.Generic;
	using System.ServiceModel.Description;
	using System.Xml;

	/// <summary>
	/// The g zip message encoding binding element importer.
	/// </summary>
	public class GZipMessageEncodingBindingElementImporter : IPolicyImportExtension
	{
		/// <summary>
		/// Defines a method that can import custom policy assertions and add implementing binding elements.
		/// </summary>
		/// <param name="importer"> The <see cref="T:System.ServiceModel.Description.MetadataImporter"/> object in use. </param>
		/// <param name="context"> The <see cref="T:System.ServiceModel.Description.PolicyConversionContext"/> that contains both the policy assertions that can be imported and the collections of binding elements to which implementing binding elements can be added. </param>
		void IPolicyImportExtension.ImportPolicy(MetadataImporter importer, PolicyConversionContext context)
		{
			if (importer == null)
			{
				throw new ArgumentNullException("importer");
			}

			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			ICollection<XmlElement> assertions = context.GetBindingAssertions();
			foreach (XmlElement assertion in assertions)
			{
				if ((assertion.NamespaceURI == GZipMessageEncodingPolicyConstants.GZipEncodingNamespace) &&
					(assertion.LocalName == GZipMessageEncodingPolicyConstants.GZipEncodingName)
					)
				{
					assertions.Remove(assertion);
					context.BindingElements.Add(new GZipMessageEncodingBindingElement());
					break;
				}
			}
		}
	}
}

