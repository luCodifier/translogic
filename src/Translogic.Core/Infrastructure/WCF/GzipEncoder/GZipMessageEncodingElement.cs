﻿namespace Translogic.Core.Infrastructure.WCF.GzipEncoder
{
	using System;
	using System.Configuration;
	using System.ServiceModel.Channels;
	using System.ServiceModel.Configuration;

	/// <summary>
	/// This class is necessary to be able to plug in the GZip encoder binding element through a configuration file
	/// </summary>
	public class GZipMessageEncodingElement : BindingElementExtensionElement
	{
		/// <summary>
		/// Called by the WCF to discover the type of binding element this config section enables
		/// </summary>
		public override Type BindingElementType
		{
			get { return typeof(GZipMessageEncodingBindingElement); }
		}

		/// <summary>
		/// The only property we need to configure for our binding element is the type of inner encoder to use. Here, we support text and binary.
		/// </summary>
		[ConfigurationProperty("innerMessageEncoding", DefaultValue = "textMessageEncoding")]
		public string InnerMessageEncoding
		{
			get { return (string)base["innerMessageEncoding"]; }
			set { base["innerMessageEncoding"] = value; }
		}

		/// <summary>
		/// Called by the WCF to apply the configuration settings (the property above) to the binding element
		/// </summary>
		/// <param name="bindingElement"> The binding element. </param>
		public override void ApplyConfiguration(BindingElement bindingElement)
		{
			GZipMessageEncodingBindingElement binding = (GZipMessageEncodingBindingElement)bindingElement;
			PropertyInformationCollection propertyInfo = this.ElementInformation.Properties;
			if (propertyInfo["innerMessageEncoding"].ValueOrigin != PropertyValueOrigin.Default)
			{
				switch (this.InnerMessageEncoding)
				{
					case "textMessageEncoding":
						binding.InnerMessageEncodingBindingElement = new TextMessageEncodingBindingElement();
						break;
					case "binaryMessageEncoding":
						binding.InnerMessageEncodingBindingElement = new BinaryMessageEncodingBindingElement();
						break;
				}
			}
		}

		/// <summary>
		/// Called by the WCF to create the binding element
		/// </summary>
		/// <returns>
		/// The <see cref="BindingElement"/>.
		/// </returns>
		protected override BindingElement CreateBindingElement()
		{
			GZipMessageEncodingBindingElement bindingElement = new GZipMessageEncodingBindingElement();
			this.ApplyConfiguration(bindingElement);
			return bindingElement;
		}
	}
}
