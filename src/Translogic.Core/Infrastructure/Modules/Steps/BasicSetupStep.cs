namespace Translogic.Core.Infrastructure.Modules.Steps
{
	using Castle.Windsor;

	/// <summary>
	/// Classe de setup de "passo" basico
	/// </summary>
	public class BasicSetupStep : IModuleSetupStep
	{
		#region ATRIBUTOS

		private readonly IWindsorContainer _container;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova instancia da classe, injetando o container
		/// </summary>
		/// <param name="container">Container a ser injetado</param>
		public BasicSetupStep(IWindsorContainer container)
		{
			_container = container;
		}

		#endregion

		#region IModuleSetupStep MEMBROS
		/// <summary>
		/// M�todo que ser� executado ao fazer o setup do m�dulo
		/// </summary>
		/// <param name="module">M�dulo a ser instalado</param>
		public void Execute(IModuleInstaller module)
		{
			module.InstallComponents(_container);
		}

		#endregion
	}
}