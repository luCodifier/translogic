namespace Translogic.Core.Infrastructure.Modules.Steps
{
	using NHibernate.Validator.Cfg.Loquacious;

	/// <summary>
	/// Implementa��o de "Passo" de configura��o de valida��es
	/// </summary>
	public class ValidationSetupStep : IModuleSetupStep
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly FluentConfiguration _validationConfig;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="validationConfig">Configura��o de valida��o</param>
		public ValidationSetupStep(FluentConfiguration validationConfig)
		{
			_validationConfig = validationConfig;
		}

		#endregion

		#region IModuleSetupStep MEMBROS

		/// <summary>
		/// Executa a insta��o do m�dulo
		/// </summary>
		/// <param name="module">M�dulo a ser instalado</param>
		public void Execute(IModuleInstaller module)
		{
			_validationConfig.Register(
				module.GetType().Assembly.ValidationDefinitions()
				);
		}

		#endregion
	}
}