namespace Translogic.Core.Infrastructure.Modules.Steps
{
	/// <summary>
	/// Interface de "passo" de setup de m�dulos
	/// </summary>
	public interface IModuleSetupStep
	{
		#region M�TODOS

		/// <summary>
		/// M�todo que ser� executado ao fazer o setup do m�dulo
		/// </summary>
		/// <param name="module">M�dulo a ser instalado</param>
		void Execute(IModuleInstaller module);

		#endregion
	}
}