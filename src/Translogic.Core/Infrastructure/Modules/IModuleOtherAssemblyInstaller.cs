namespace Translogic.Core.Infrastructure.Modules
{
	using System.Reflection;

	/// <summary>
	/// Interface que define intala��o de m�dulo setando o(s) assembly(ies) que cont�m os mapeamentos
	/// </summary>
	public interface IModuleOtherAssemblyInstaller : IModuleInstaller
	{
		/// <summary>
		/// Gets the mapping assemblies.
		/// </summary>
		/// <returns> The assemblies. </returns>
		Assembly[] GetMappingAssemblies();
	}
}