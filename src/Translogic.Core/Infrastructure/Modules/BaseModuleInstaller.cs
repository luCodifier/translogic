﻿namespace Translogic.Core.Infrastructure.Modules
{
	using System.Web.Routing;
	using Castle.Windsor;

	/// <summary>
	/// Implementação base do instalador de módulos
	/// </summary>
	public abstract class BaseModuleInstaller : IModuleInstaller
	{
		/// <summary>
		/// Nome do módulo, utilizado na estrutura de diretórios
		/// </summary>
		public abstract string Name { get; }

		/// <summary>
		/// Registra controllers, helpers e outros componentes web.
		/// </summary>
		/// <param name="container">Instância do IWindsorContainer</param>
		public virtual void InstallComponents(IWindsorContainer container)
		{
		}

		/// <summary>
		/// Registra as Rotas do Asp.Net MVC
		/// </summary>
		/// <param name="routes">
		/// Coleção de rotas.
		/// </param>
		public virtual void RegisterRoutes(RouteCollection routes)
		{
		}
	}
}