namespace Translogic.Core.Infrastructure.Modules
{
	using System.Web.Routing;
	using Castle.Windsor;

	/// <summary>
	/// Interface que define que a classe � uma instala��o de m�dulo
	/// </summary>
	public interface IModuleInstaller
	{
		#region PROPRIEDADES

		/// <summary>
		/// Nome do m�dulo, utilizado na estrutura de diret�rios
		/// </summary>
		string Name { get; }

		#endregion

		#region M�TODOS

		/// <summary>
		/// Registra controllers, helpers e outros componentes web.
		/// </summary>
		/// <param name="container">Inst�ncia do IWindsorContainer</param>
		void InstallComponents(IWindsorContainer container);

		/// <summary>
		/// Registra as Rotas do Asp.Net MVC
		/// </summary>
		/// <param name="routes">
		/// Cole��o de rotas.
		/// </param>
		void RegisterRoutes(RouteCollection routes);

		#endregion
	}
}