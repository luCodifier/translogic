namespace Translogic.Core.Infrastructure.Modules
{
    using Castle.Core.Logging;
    using Logger;
    using Steps;
    using System;
    using System.Linq;
    using System.Diagnostics;

    /// <summary>
    /// Servi�o de setup de m�dulos
    /// </summary>
    public class ModuleSetupService
    {
        #region ATRIBUTOS READONLY & EST�TICOS

        private readonly IModuleInstaller[] _modules;
        private readonly IModuleSetupStep[] _steps;

        #endregion

        #region ATRIBUTOS

        private ILogger logger = NullLogger.Instance;

        #endregion

        #region CONSTRUTORES

        /// <summary>
        /// Construtor default
        /// </summary>
        /// <param name="modules">M�dulos configurados</param>
        /// <param name="steps">Os passos que ser�o executados para cada m�dulo</param>
        public ModuleSetupService(IModuleInstaller[] modules, IModuleSetupStep[] steps)
        {
            _modules = modules;
            _steps = steps;
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Logger default
        /// </summary>
        public ILogger Logger
        {
            get { return logger; }
            set { logger = value; }
        }

        #endregion

        #region M�TODOS

        /// <summary>
        /// Executa os passos em cada m�dulo
        /// </summary>
        public void Initialize()
        {
            foreach (IModuleInstaller module in _modules)
            {
                foreach (IModuleSetupStep step in _steps)
                {
                    string stepName = step.GetType().Name;

                    try
                    {
                        logger.Debug("Executando passo {0} no m�dulo {1}", stepName, module.Name);
#if DEBUG
                        Sis.LogDebug(string.Format("Executando passo {0} no m�dulo {1} - {2}", stepName, module.Name, module.ToString()));
#endif
                        Debug.WriteLine(string.Format("Executando passo {0} no m�dulo {1} - {2}", stepName, module.Name, module.ToString()));

                        step.Execute(module);
                    }
                    catch (Exception ex) 
                    {
                        string msg = string.Format("Erro ao registrar passo {0} no m�dulo {1}", stepName, module.Name);
                        Sis.LogException(ex, msg);
                        throw new Exception(msg, ex);
                    }
                }
            }
        }

#endregion
    }
}