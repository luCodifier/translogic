namespace Translogic.Core.Infrastructure.DataAccess
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Castle.Core.Logging;
    using Castle.Facilities.NHibernateIntegration;
    using Modules;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Cfg.MappingSchema;
    using NHibernate.Mapping.ByCode;

    /// <summary>
    /// Builder respons�vel por adicionar o assembly 
    /// de cada m�dulo na configura��o do NHibernate
    /// </summary>
    public class ModuleNHibernateConfigBuilder : IConfigurationContributor
    {
        #region ATRIBUTOS

        private readonly IModuleInstaller[] modules;
        private readonly IReservedMapping[] reservedMappings;
        private ILogger logger = NullLogger.Instance;

        #endregion

        #region CONSTRUTORES

        /// <summary>
        /// Construtor default
        /// </summary>
        /// <param name="modules">Os m�dulos configurados. <remarks>Injetado</remarks></param>
        /// <param name="reservedMappings">Os mappings customizados para conex�es espec�ficas</param>
        public ModuleNHibernateConfigBuilder(IModuleInstaller[] modules, IReservedMapping[] reservedMappings)
        {
            this.modules = modules;
            this.reservedMappings = reservedMappings;
        }

        #endregion

        #region PROPRIEDADES

        /// <summary>
        /// Retorna o Logger
        /// </summary>
        public ILogger Logger
        {
            get { return logger; }
            set { logger = value; }
        }

        #endregion

        #region IConfigurationContributor MEMBROS

        //static ConcurrentDictionary<string, Configuration> dicConfig = new ConcurrentDictionary<string, Configuration>();

        /// <summary>
        /// Processa a configura��o de cada sessionFactory
        /// </summary>
        /// <param name="name">Nome da Session Factory</param>
        /// <param name="config">Configura��o da Session Factory</param>
        public void Process(string name, Configuration config)
        {
            //if (dicConfig.ContainsKey(name))
            //{
            //    config = dicConfig[name];
            //    return;
            //}

            if (name.Contains(".reserved."))
            {
                string splited = name.Split('.').LastOrDefault();
                var mappingTypes = reservedMappings
                    .Where(a => a.Aliases.Contains(splited))
                    .SelectMany(r => r.GetReservedMappings())
                    .Distinct()
                    .ToArray();
                this.RegisterTypeMappings(config, mappingTypes);
            }
            else
            {
                foreach (IModuleInstaller module in modules)
                {
                    IModuleOtherAssemblyInstaller otherAssembliesInstaller = module as IModuleOtherAssemblyInstaller;
                    if (otherAssembliesInstaller != null)
                    {
                        foreach (Assembly assembly in otherAssembliesInstaller.GetMappingAssemblies())
                        {
                            this.RegisterAssemblyMappings(config, assembly);
                        }
                    }
                    else
                    {
                        Assembly moduleAssembly = module.GetType().Assembly;

                        this.RegisterAssemblyMappings(config, moduleAssembly);
                    }
                }
            }
            //dicConfig.TryAdd(name, config);
        }

        /// <summary>
        /// Metodo de contribuicao
        /// </summary>
        /// <param name="alias">Nome do alias</param>
        /// <param name="isDefault">Indica se � padrao</param>
        /// <param name="configuration">Configura��o a ser contribuida</param>
        public void Contribute(string alias, bool isDefault, Configuration configuration)
        {
            throw new NotImplementedException();
        }

        private void RegisterTypeMappings(Configuration config, Type[] mappingTypes)
        {
            try
            {
                ModelMapper mapper = new ModelMapper();
                mapper.AddMappings(mappingTypes);
                config.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
            }
            catch (DuplicateMappingException e)
            {
                this.Logger.Warn(e.InnerException.Message);
                return;
            }
            catch (MappingException e)
            {
                if (e.InnerException is DuplicateMappingException)
                {
                    this.Logger.Warn(e.InnerException.Message);
                    return;
                }

                throw;
            }
        }

        private void RegisterAssemblyMappings(Configuration config, Assembly moduleAssembly)
        {
            try
            {
                config.AddAssembly(moduleAssembly);

                ModelMapper mapper = new ModelMapper();
                Type[] mappingTypes = moduleAssembly.GetExportedTypes();
                var filteredTypes = mappingTypes.Where(t => !t.Name.EndsWith("ReservedClassMapping"));
                mapper.AddMappings(filteredTypes);

                config.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
            }
            catch (DuplicateMappingException e)
            {
                this.Logger.Warn(e.InnerException.Message);
                return;
            }
            catch (MappingException e)
            {
                if (e.InnerException is DuplicateMappingException)
                {
                    this.Logger.Warn(e.InnerException.Message);
                    return;
                }

                throw;
            }
        }

        #endregion
    }
}