﻿namespace Translogic.Core.Infrastructure.DataAccess
{
    using System;

    /// <summary>
    /// A class mappings reserved provider
    /// </summary>
    public interface IReservedMapping
    {
        /// <summary>
        /// Factory Aliases
        /// </summary>
        string[] Aliases { get; }

        /// <summary>
        /// Return a collection of type mappings for session
        /// </summary>
        /// <returns>A collection of types</returns>
        Type[] GetReservedMappings();
    }
}
