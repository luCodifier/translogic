namespace Translogic.Core.Infrastructure.DataAccess.CustomType
{
	using System;
	using System.Data;
	using Encryption;
	using NHibernate.SqlTypes;
	using NHibernate.Type;

	/// <summary>
	/// Tipo de string encriptada
	/// </summary>
	public class EncryptedStringUserType : AbstractStringType
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly IEncryptionService _encryptionService;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe configurando o servi�o de encripta��o TripleDES
		/// </summary>
		public EncryptedStringUserType()
			: base(new StringSqlType())
		{
			_encryptionService = new TripleDESEncryptionService();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Retorna o nome.
		/// </summary>
		public override string Name
		{
			get { return "EncryptedString"; }
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Pega o objeto do dataReader pelo �ndice
		/// </summary>
		/// <param name="rs">
		/// Datareader que cont�m o objeto.
		/// </param>
		/// <param name="index">
		/// �ndice a ser buscado no datareader.
		/// </param>
		/// <returns>
		/// Objeto decriptado
		/// </returns>
		public override object Get(IDataReader rs, int index)
		{
			string content = Convert.ToString(base.Get(rs, index));
			return _encryptionService.Decrypt(content);
		}

		/// <summary>
		/// Pega o objeto do dataReader pela chave
		/// </summary>
		/// <param name="rs">
		/// Datareader que cont�m o objeto.
		/// </param>
		/// <param name="name">
		/// Chave a ser procurada.
		/// </param>
		/// <returns>
		/// Objeto decriptado
		/// </returns>
		public override object Get(IDataReader rs, string name)
		{
			string content = Convert.ToString(base.Get(rs, name));
			return _encryptionService.Decrypt(content);
		}

		/// <summary>
		/// Insere o objeto
		/// </summary>
		/// <param name="cmd">Command a ser executado - <see cref="IDbCommand"/></param>
		/// <param name="value">Objeto a ser persistido</param>
		/// <param name="index">�ndice que entrar�</param>
		public override void Set(IDbCommand cmd, object value, int index)
		{
			string str = _encryptionService.Encrypt(Convert.ToString(value));
			base.Set(cmd, str, index);
		}

		#endregion
	}
}