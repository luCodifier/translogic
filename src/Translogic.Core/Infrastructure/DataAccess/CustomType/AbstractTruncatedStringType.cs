﻿namespace Translogic.Core.Infrastructure.DataAccess.CustomType
{
    using NHibernate.SqlTypes;
    using NHibernate.Type;

    /// <summary>
    /// Classe para realizar o trunc da string conforme o tamanho da coluna no banco de dados
    /// </summary>
    /// <see cref="http://www.primordialcode.com/blog/post/nhibernate-custom-parametric-usertype-truncate-strings"/>
    public abstract class AbstractTruncatedStringType : AbstractStringType
    {
        /// <summary>
        /// Metodo base para o tipo
        /// </summary>
        internal AbstractTruncatedStringType() : base(new StringSqlType())
        {
        }

        /// <summary>
        /// Define o tipo de sql para o mapeamento
        /// </summary>
        /// <param name="sqlType">tipo do sql empregado</param>
        internal AbstractTruncatedStringType(StringSqlType sqlType) : base(sqlType)
        {
        }

        /// <summary>
        /// propriedade de tamanho
        /// </summary>
        public abstract int Length { get; }

        /// <summary>
        /// Método para realizar o corte da string
        /// </summary>
        /// <param name="cmd">variavel command</param>
        /// <param name="value">valor do objeto</param>
        /// <param name="index">indice de corte</param>
        public override void Set(System.Data.IDbCommand cmd, object value, int index)
        {
            string str = (string)value;
            if (str.Length > Length)
            {
                str = str.Substring(0, Length);
            }

            base.Set(cmd, str, index);
        }
    }
}
