﻿namespace Translogic.Core.Infrastructure.DataAccess.CustomType
{
    using NHibernate.UserTypes;

    /// <summary>
    /// Especialização da classe para mapeamento truncando o tipo String
    /// </summary>
    /// <see cref="http://www.primordialcode.com/blog/post/nhibernate-custom-parametric-usertype-truncate-strings"/>
    public class TruncatedString : AbstractTruncatedStringType, IParameterizedType
    {
        private const int DefaultLimit = 1;

        private int _length = DefaultLimit;

        /// <summary>
        /// define o tamanho
        /// </summary>
        public override int Length
        {
            get { return _length; }
        }

        /// <summary>
        /// nome do tipo
        /// </summary>
        public override string Name
        {
            get { return "TruncatedString"; }
        }

        /// <summary>
        /// recupera o parametro de entrada e valida o tamanho do corte da string
        /// </summary>
        /// <param name="parameters"> tamanho do campo </param>
        public void SetParameterValues(System.Collections.Generic.IDictionary<string, string> parameters)
        {
            if (false == int.TryParse(parameters["length"], out _length))
            {
                _length = DefaultLimit;
            }
        }
    }
}