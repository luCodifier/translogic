namespace Translogic.Core.Infrastructure.Settings
{
	/// <summary>
	/// Configura��es do servidor
	/// </summary>
	public class ServerSettings
	{
		#region PROPRIEDADES

		/// <summary>
		/// Url do translogic legado.
		/// </summary>
		public string UrlTranslogicLegado { get; set; }

		/// <summary>
		/// Sufixo do servidor, normalmente � .all-logistica.net
		/// </summary>
		public string SufixoServidor { get; set; }

		/// <summary>
		/// Garante que o sufixo do ser� sempre adicionado
		/// </summary>
		public bool GarantirUsoDoSufixoServidor { get; set; }

        /// <summary>
        /// Url do SIV.
        /// </summary>
        public string UrlSiv { get; set; }

        /// <summary>
        /// Servlet do SIV para acesso do Translogic. 
        /// </summary>
        public string ServletSiv { get; set; }

        /// <summary>
        /// Link do Gis no SIV para acesso do Translogic. 
        /// </summary>
        public string Gis { get; set; }

		#endregion
	}
}