﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;

using Amazon.S3.Model;
using System.IO;
using System.Configuration;
using Amazon.S3.Util;
using Amazon.S3.IO;
using System.Text.RegularExpressions;


namespace Translogic.Core.Infrastructure.Aws
{
    public class AmazonUploader
    {
        #region Configuration Keys
        private const string AWSBucketName = "AWSBucketName";
        #endregion

        /// <summary>
        /// Bucket Name
        /// </summary>
        public string Bucket
        {
            get
            {
                string bucketValue = string.Empty;
                try
                {
                    AppSettingsReader reader = new AppSettingsReader();
                    bucketValue = (string)reader.GetValue(AWSBucketName, bucketValue.GetType());
                }
                catch { }
                return bucketValue;
            }
        }

        public string BucketWithFolder
        {
            get
            {
                return String.Concat(this.Bucket, "/", "processos");
            }
        }

        public AmazonS3Client Client { get; set; }

        public AmazonUploader()
        {
            Client = new AmazonS3Client(RegionEndpoint.SAEast1);
        }

        /// <summary>
        /// Realiza upload de um arquivo para o bucket do translogic da Amazon (chaves devem ser definidas no web.config)
        /// </summary>
        /// <param name="fileName">Caminho completo do arquivo a ser carregado</param>
        /// <returns>Verdadeiro se arquivo foi carregado</returns>
        public bool UploadObject(string fileName, string folder)
        {
            try
            {
                TransferUtility fileTransferUtility = new TransferUtility(this.Client);
                TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = String.Concat(this.BucketWithFolder, "/", folder),
                    FilePath = fileName,
                    CannedACL = S3CannedACL.PublicRead
                };

                fileTransferUtility.Upload(fileTransferUtilityRequest);
                //fileTransferUtility.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Realiza upload de um stream para o bucket do translogic da Amazon (chaves devem ser definidas no web.config)
        /// </summary>
        /// <param name="fileName">Stream do arquivo as ser carregado</param>
        /// <returns>Verdadeiro se o stream foi carregado</returns>
        public bool UploadObject(Stream stream, string fileName, string folder)
        {
            try
            {
                TransferUtility fileTransferUtility = new TransferUtility(this.Client);
                TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = this.BucketWithFolder,
                    InputStream = stream,
                    CannedACL = S3CannedACL.PublicRead,
                    Key = fileName
                };

                if (!String.IsNullOrEmpty(folder))
                {
                    fileTransferUtilityRequest.BucketName = String.Concat(this.BucketWithFolder, "/", folder);
                }

                fileTransferUtility.Upload(fileTransferUtilityRequest);
                //fileTransferUtility.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Remove um objeto do Bucket da Amazon. Deve ser nome exado do arquivo.
        /// </summary>
        /// <param name="fileName">Nome do arquivo</param>
        /// <returns>Verdadeiro se arquivo foi removido</returns>
        public bool DeleteObject(string fileName, string folder)
        {
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = String.Concat(this.BucketWithFolder, "/", folder),
                Key = fileName
            };

            try
            {
                this.Client.DeleteObject(deleteObjectRequest);
                return true;

            }
            catch { return false; }
        }

        /// <summary>
        /// Buscar um objeto do Bucket da Amazon.
        /// </summary>
        /// <param name="fileName">Nome do arquivo</param>
        /// <param name="temporaryPath">Local para salvar o arquivo</param>
        /// <returns>Caminho completo do arquivo obtido</returns>
        public string GetObject(string fileName, string temporaryPath, string folder)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = String.Concat(this.BucketWithFolder, "/", folder),
                    Key = fileName
                };

                using (GetObjectResponse response = this.Client.GetObject(request))
                {
                    string destination = Path.Combine(temporaryPath, fileName);
                    response.WriteResponseStreamToFile(destination);
                    return destination;
                }

            }
            catch { return string.Empty; }
        }

        public void GetObjectsInFolder(string temporaryPath, string folder)
        {
            try
            {
                ListObjectsResponse objectsResponse = this.Client.ListObjects(this.Bucket);
                foreach (S3Object rootObject in objectsResponse.S3Objects)
                {
                    if (rootObject.Key.ToString().Contains(".") && rootObject.Key.ToString().Contains(folder))
                    {
                        GetObjectRequest request = new GetObjectRequest
                        {
                            BucketName = this.Bucket,
                            Key = rootObject.Key
                        };

                        GetObjectResponse objectResponse = this.Client.GetObject(request);
                        var saveFolderAndFileName = Path.Combine(temporaryPath, rootObject.Key.Replace("processos/", ""));
                        objectResponse.WriteResponseStreamToFile(saveFolderAndFileName);
                    }
                }
            }
            catch { }
        }


        public void GetObjectsInFolder(string temporaryPath, string folder, string pattern = "")
        {
            try
            {

                var objectRequest = new ListObjectsRequest();
                objectRequest.BucketName = this.Bucket;
                objectRequest.Prefix = string.Format("{0}/", folder);

                ListObjectsResponse objectsResponse = this.Client.ListObjects(objectRequest);


                foreach (S3Object rootObject in objectsResponse.S3Objects)
                {
                    if (!string.IsNullOrEmpty(pattern))
                    {
                        var regex = new Regex(objectRequest.Prefix + pattern, RegexOptions.IgnoreCase);

                        if (!regex.Match(rootObject.Key).Success)
                            continue;
                    }


                    GetObjectRequest request = new GetObjectRequest
                    {
                        BucketName = this.Bucket,
                        Key = rootObject.Key
                    };

                    GetObjectResponse objectResponse = this.Client.GetObject(request);
                    var saveFolderAndFileName = Path.Combine(temporaryPath, rootObject.Key.Replace("processos/", ""));
                    objectResponse.WriteResponseStreamToFile(saveFolderAndFileName);
                }
            }
            catch { }
        }
    }
}