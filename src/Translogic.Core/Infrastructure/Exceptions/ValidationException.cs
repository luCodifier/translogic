namespace Translogic.Core.Infrastructure.Exceptions
{
	using System;

	/// <summary>
	/// Exce��o de Valida��o na aplica��o
	/// </summary>
	public class ValidationException : ApplicationException
	{
		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe 
		/// </summary>
		/// <param name="message">Mensagem da exce��o</param>
		public ValidationException(string message) : base(message)
		{
		}

		#endregion
	}
}