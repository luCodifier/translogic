namespace Translogic.Core.Infrastructure
{
	/// <summary>
	/// Interface de servi�o de controle de acesso
	/// </summary>
	public interface IControleAcessoService
	{
		#region M�TODOS
		/// <summary>
		/// Obt� os grupos do usuario
		/// </summary>
		/// <param name="usuario">codigo do usuario</param>
		/// <returns>array de strings</returns>
		string[] ObterGruposPorUsuario(string usuario);

		/// <summary>
		/// valida o usuario
		/// </summary>
		/// <param name="usuario">codigo do usuario</param>
		/// <param name="senha">senha do usuario</param>
		/// <returns>valor booleano</returns>
		bool Validar(string usuario, string senha);

		/// <summary>
		/// Insere o log de sess�o expirada
		/// </summary>
		/// <param name="codigoUsuario">C�digo do usuario</param>
		/// <param name="ipClient">Ip que mostra na maquina do client</param>
		/// <param name="ipServer">Ip que mostra a m�quina do server</param>
		/// <param name="tempoRestante">Tempo Restante em segundos</param>
		void InserirLogSessaoExpirada(string codigoUsuario, string ipClient, string ipServer, int tempoRestante);

	    /// <summary>
	    /// Valida o usu�rio e verificar acesso
	    /// </summary>
	    /// <param name="codigo">c�digo do usuario</param>
	    /// <param name="senha">senha do usuario</param>
	    /// <param name="transacao">transacao da tela</param>
	    /// <returns>Valor booleano</returns>
	    bool ValidarAutorizarAcesso(string codigo, string senha, string transacao);

		/// <summary>
		/// Verifica se o Usu�rio pertence ao grupo.
		/// </summary>
		/// <param name="codigoUsuario">C�digo do Usu�rio do grupo.</param>
		/// <param name="codigoGrupo">C�digo do grupo.</param>
		/// <returns>Retorna verdadeiro se o usu�rio pertencer ao grupo.</returns>
		bool VerificarUsuarioPertenceGrupo(string codigoUsuario, string codigoGrupo);

		#endregion
	}
}