namespace Translogic.Core
{
	using System.Web.Mvc;
	using Castle.MicroKernel.Registration;
	using Infrastructure.Modules;
	using Infrastructure.Modules.Steps;
	using Infrastructure.Settings;
	using Infrastructure.Validation;
	// using Infrastructure.Web.ControllerFactories;
	using NHibernate.Validator.Cfg.Loquacious;
	using NHibernate.Validator.Engine;

	/// <summary>
	/// Classe que tem defini��es de inicializa��o da aplica��o
	/// </summary>
	public abstract class TranslogicStarter
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		/// <summary>
		/// Setup done
		/// </summary>
		public static bool SetupDone { get; set; }

		#endregion

		#region PROPRIEDADES EST�TICAS

		/// <summary>
		/// Retorna o Container - <see cref="TranslogicContainer" />.
		/// </summary>
		public static TranslogicContainer Container { get; private set; }

		/// <summary>
		/// Informa se a aplica��o j� est� inicializada
		/// </summary>
		public static bool Initialized { get; private set; }

		#endregion

		#region M�TODOS EST�TICOS
		/// <summary>
		/// M�todo que inicializa a aplica��o
		/// </summary>
		public static void Initialize()
		{
			if (Initialized)
			{
				return;
			}

			ResetInitializationFlag();

            Container = new TranslogicContainer();

#if PROFILE_NH
			InitNHProfiler();
#endif

			Initialized = true;
		}

        /// <summary>
        /// Reseta o flag de inicializa��o
        /// </summary>
		public static void ResetInitializationFlag()
		{
			Initialized = false;

			if (Container != null)
			{
				Container.Dispose();
				Container = null;
			}
		}
		
		/// <summary>
		/// Inicializa as insta��es dos m�dulos
		/// </summary>
		public static void RunModulesSetup()
		{
			// IModuleInstaller[] installers = Container.ResolveAll<IModuleInstaller>();
			Container.Resolve<ModuleSetupService>().Initialize();
		}

		/// <summary>
		/// Base Setup
		/// </summary>
		public static void BaseSetup()
		{
			if (SetupDone)
			{
				return;
			}

			Container.RegistrarServicosPadrao();
			Container.Register(
				Component.For<IModuleSetupStep>().ImplementedBy<BasicSetupStep>().Named("basic.step"),
				Component.For<IModuleSetupStep>().ImplementedBy<ValidationSetupStep>().Named("validation.step")
			);
		}

		/// <summary>
		/// Executa m�todos de configura��o para projetos tipo SOA
		/// </summary>
		public static void SetupForSOA()
		{
			BaseSetup();

			RunModulesSetup();
			SetupValidation();

			SetupDone = true;
		}

		/// <summary>
		/// Executa m�todos de configura��o para projetos tipo Testes
		/// </summary>
		public static void SetupForTests()
		{
			BaseSetup();

			RunModulesSetup();
			SetupValidation();

			SetupDone = true;
		}

		/// <summary>
		/// Executa m�todo de configura��o para projeto JobRunner
		/// </summary>
		public static void SetupForJobRunner()
		{
			BaseSetup();

			RunModulesSetup();
			SetupValidation();

			SetupDone = true;
		}
        
		/// <summary>
		/// Setup validation
		/// </summary>
		public static void SetupValidation()
		{
			var validationConfig = Container.Resolve<FluentConfiguration>();

			validationConfig.SetDefaultValidatorMode(ValidatorMode.OverrideExternalWithAttribute);

			var validatorEngine = new TranslogicValidatorEngine();
			validatorEngine.Configure(validationConfig);

			Container.Register(
				Component.For<IValidatorEngine>().
					Instance(validatorEngine)
				);
		}

		#endregion
	}
}
