﻿using Newtonsoft.Json;
using Speed.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;

namespace Translogic.Core.Spd
{

    /// <summary>
    /// Classe com as connections
    /// </summary>
    public static class Dbs
    {

        static Dbs()
        {
            // configura a conexão default do Speed como do Translogic
            if (ConfigurationManager.ConnectionStrings["spd_" + Db.Translogic] != null)
            {
                Sys.ConnectionString = ConnectionString(Db.Translogic);
                Sys.ProviderType = EnumDbProviderType.Oracle;
            }
        }

        /// Retorna uma database para a bae de dados especificada
        public static Database NewDb(Db db)
        {
            return Sys.NewDb(EnumDbProviderType.Oracle, ConnectionString(db));
        }

        /// Retorna uma database para a bae de dados especificada
        public static string ConnectionString(Db db)
        {
            try
            {
                return ConfigurationManager.ConnectionStrings["spd_" + db].ConnectionString;
            }
            catch
            {
                throw new Exception($"Conection string '{"spd_" + db}' não definida no arquivo de configuração");
            }
        }

        /// <summary>
        /// Executa uma Action na base de dados
        /// </summary>
        /// <param name="db"></param>
        /// <param name="action"></param>
        public static void ExecuteNonQuery(Db db, Action<Database> action)
        {
            using (var _db = NewDb(db))
            {
                action(_db);
            }
        }

        /// <summary>
        /// Executa uma Func na base de dados
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="db"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        public static T Execute<T>(Db db, Func<Database, T> function)
        {
            using (var _db = NewDb(db))
            {
                return function(_db);
            }
        }


    }

}
