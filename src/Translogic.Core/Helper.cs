﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.Core
{

    /// <summary>
    /// Classe de métodos úteis
    /// </summary>
    public static class Helper
    {

        /// <summary>
        /// Formata uma lista e retorna um List<string>
        /// </summary>
        /// <param name="valores"></param>
        /// <returns>Lista de valores</returns>
        public static List<string> FormataLista(string valores)
        {
            var vals = valores.Trim().ToUpper()
                             .Split(new char[] { ';', ',', ' ', '|' }, StringSplitOptions.RemoveEmptyEntries)
                             .ToList();
            return vals;
        }

        /// <summary>
        /// Formata uma lista e retorna um List<string>
        /// </summary>
        /// <param name="valores"></param>
        /// <returns>Valores formatados separados por ','</returns>
        public static string FormataListaStr(string valores, string separador = ",", bool addAspas = false)
        {
            var lista = FormataLista(valores);
            if (addAspas)
                return string.Join(separador, lista.Select(p => "'" + p + "'"));
            else
                return string.Join(separador, lista);
        }

        /// <summary>
        /// Formata CNPJ
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns></returns>
        public static string FormataCnpj(string cnpj)
        {
            var resultado = String.Empty;
            if (!String.IsNullOrEmpty(cnpj))
            {
                cnpj = String.Format("{0, 14:D14}", cnpj.Replace(".", String.Empty).Replace("/", String.Empty).Replace("-", String.Empty));
                resultado = String.Format(
                    "{0}.{1}.{2}/{3}-{4}",
                    cnpj.Substring(0, 2),
                    cnpj.Substring(2, 3),
                    cnpj.Substring(5, 3),
                    cnpj.Substring(8, 4),
                    cnpj.Substring(12, 2));
            }

            return resultado;
        }

        /// <summary>
        /// passa todas as propriedades string pra maiúsculas
        /// </summary>
        /// <param name="obj"></param>
        public static void ToUpperCase(object obj)
        {
            if (obj == null)
                return;

            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.DeclaringType == typeof(string))
                    prop.SetValue(obj, ((string)prop.GetValue(obj, null)).ToUpper(), null);
            }

        }

        public static string OracleDate(this DateTime date)
        {
            return $"TO_DATE('{date.ToString("yyyy-MM-dd HH:mm:ss")}', 'YYYY-MM-DD HH24:MI:SS')";
        }
        public static string OracleDate(this DateTime? date)
        {
            return $"TO_DATE('{date.Value.ToString("yyyy-MM-dd HH:mm:ss")}', 'YYYY-MM-DD HH24:MI:SS')";
        }

    }

}
