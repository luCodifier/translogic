﻿namespace Translogic.SOA
{
    using System.ServiceModel.Activation;
    using System.Web.Routing;

    using Castle.Facilities.WcfIntegration;
    using Core;
    using Core.Infrastructure.Web;

    using Translogic.Core.Infrastructure.WCF;
    using Translogic.Modules.Core.Interfaces.DadosFiscais.Interfaces;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Global.asax herdado do abstrato
    /// </summary>
    public class Global : AbstractGlobalApplication
    {
        #region MÉTODOS

        /// <summary>
        /// Inicializador para fazer a configuração dos componentes
        /// </summary>
        protected override void InstallWebComponents()
        {
            // TranslogicStarter.SetupForSOA();
            // RouteTable.Routes.Add(new ServiceRoute("DadosFiscais/DadosFiscaisService", new FlatWsdlServiceHostFactory(), typeof(IDadosFiscaisService)));
            Sis.Inicializar(EnumSistema.TranslogicSoaWebService, "Job");

// #if DEBUG
//            TranslogicStarter.Initialize();
//            TranslogicStarter.SetupForSOA();
// #endif
        }

        #endregion
    }

    /// <summary>
    /// HostFactory para serviços que utilizam <see cref="netTcpBinding"/>
    /// </summary>
    public class TcpHostFactory : DefaultServiceHostFactory
    {
        /// <summary>
        /// Construtor estático
        /// </summary>
        static TcpHostFactory()
        {
            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForSOA();
        }
    }
}