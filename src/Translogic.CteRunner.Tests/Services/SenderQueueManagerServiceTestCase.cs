﻿namespace Translogic.CteRunner.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.MicroKernel.Registration;
    using NUnit.Framework;
    using Translogic.Core;
    using Translogic.CteRunner.Services;
    using Translogic.CteRunner.Services.Commands;
    using Translogic.Tests;

    public class SenderQueueManagerServiceTestCase
    {
        [Test]
        public void ExecutarGeracaoSingleThreadTestCase()
        { 
            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForTests();
            TranslogicContainer container = TranslogicStarter.Container;
            container.Register(Component.For<SenderQueueManagerService>());
            
            // Inicia gerente de sender
            SenderQueueManagerService senderService =  container.Resolve<SenderQueueManagerService>();
            senderService.ExecutarGeracaoSingleThread();
        }
    }
}
