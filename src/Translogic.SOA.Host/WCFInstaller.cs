﻿namespace Translogic.SOA.Host.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.Web.Routing;
    using System.Xml;
    using Castle.Core.Logging;
    using Castle.Facilities.WcfIntegration;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Infra.Behavior;
    using Translogic.Core.Infrastructure.Modules;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
    using Translogic.Modules.Core.Interfaces.Simconsultas.Interfaces;
    using Translogic.Modules.ServicesClients.Domain.Services.SimConsultas;
    using Translogic.SOA.Host.Infra;
    using Translogic.SOA.Host.Infra.WCF;

    /// <summary>
    /// Installer do módulo de wcf
    /// </summary>
    public class WCFInstaller : IModuleInstaller
    {
        private ServerConfigure _serviceHelper;

        private List<string> lstServicos = new List<string>();

        /// <summary>
        /// Nome do serviço
        /// </summary>
        public string Name
        {
            get { return "Host"; }
        }

        /// <summary>
        /// Registro de rotas
        /// </summary>
        /// <param name="routes">rotas a serem registradas</param>
        public void RegisterRoutes(RouteCollection routes)
        {
        }

        /// <summary>
        /// Instalação dos componentes.
        /// </summary>
        /// <param name="container">Container do translogic</param>
        public void InstallComponents(IWindsorContainer container)
        {
            container.Register(
                Component.For<ISimconsultasService>().ImplementedBy<NfeSimconsultasService>().Named("nfe.simconsultas"),
                Component.For<WcfMessageInspector>().ImplementedBy<WcfMessageInspector>()
            );

            // container.Register(Component.For<SerializationContractBehavior>());
            // container.Register(Component.For<EndpointBehavior>());
            // container.Register(Component.For<WcfMetadataExporter>());

            RegisterServices(container);
        }

        private static Type GetServiceContract(Type type)
        {
            Type[] interfaces = type.GetInterfaces();

            foreach (var contract in interfaces)
            {
                if (contract != null && Attribute.IsDefined(contract, typeof(ServiceContractAttribute)))
                {
                    return contract;
                }
            }

            return null;
        }

        private void RegisterServices(IWindsorContainer container)
        {
            _serviceHelper = new ServerConfigure(container);

            var config = _serviceHelper.ObterConfig(string.Empty).FirstOrDefault();

            _serviceHelper.RegisterBehaviors(config);

            var assemblyFilter = new AssemblyFilter(AppDomain.CurrentDomain.BaseDirectory).FilterByAssembly(a => !a.FullName.ToUpper().Contains("MICROSOFT.WEB.MVC"));

            BasedOnDescriptor registrations =
                AllTypes.FromAssemblyInDirectory(assemblyFilter)
                    .Pick()
                    .If(IsWcfService)
                    .WithService.Select(SelectService)
                    .Configure(r => ConfigureService(r));

            container.Register(registrations);
        }

        private bool IsWcfService(Type type)
        {
            var interfaceType = GetServiceContract(type);

            if (interfaceType != null && interfaceType.IsInterface && Attribute.IsDefined(interfaceType, typeof(ServiceContractAttribute)))
            {
                return true;
            }

            return false;
        }

        private IEnumerable<Type> SelectService(Type type, Type[] basetypes)
        {
            Type contrato = GetServiceContract(type);

            if (contrato != null)
            {
                yield return contrato;
            }
        }

        private void ConfigureService(ComponentRegistration registro)
        {
            var contract = GetServiceContract(registro.Implementation);

            if (contract == null || _serviceHelper.ServicoExcluido(contract.Name))
            {
                return;
            }

            IList<ServerHostSettings> serverHostSettings = _serviceHelper.ObterConfig(contract.Name);

            DefaultServiceModel serviceModel = null;

            foreach (var config in serverHostSettings.Distinct())
            {
                Binding binding = _serviceHelper.CreateBinding(config);
                var addr = config.GetServiceAddress(contract);
                var metadataAddr = config.GetMetadataServiceAddress(contract);

                BindingAddressEndpointModel endpoint = WcfEndpoint.BoundTo(binding).At(addr);
                
                if (lstServicos.Any(n => n.Equals(endpoint.Address, StringComparison.OrdinalIgnoreCase)))
                {
                    Console.WriteLine(string.Format("{0} está duplicado no registro !", endpoint.Address));
                    continue;
                }

                if (serviceModel == null)
                {
                    serviceModel = new DefaultServiceModel().OpenEagerly().AddEndpoints(endpoint);
                }
                else
                {
                    serviceModel.AddEndpoints(endpoint);
                }

                lstServicos.Add(endpoint.Address);

                if (config.PublishMetadata)
                {
                    serviceModel.AddBaseAddresses(metadataAddr);
                    serviceModel.PublishMetadata(x => x.EnableHttpGet());
                }

                string metadataArg = config.PublishMetadata ? string.Format("{0} Metadata: {1}", config.Protocol, metadataAddr) : string.Empty;
                Console.WriteLine(metadataArg);
            }

             registro.AsWcfService(serviceModel);
        }
    }
}