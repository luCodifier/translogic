﻿namespace Translogic.SOA.Host
{
    using Topshelf;

    using Translogic.SOA.Host.Infra;

    /// <summary>
    /// Startup do projeto
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Argumentos enviados ao programa</param>
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
                {
                    x.Service<HostBootstrapper>(
                        s =>
                        {
                            s.ConstructUsing(name => new HostBootstrapper());
                            s.WhenStarted(tc => tc.Start());
                            s.WhenStopped(tc => tc.Stop());
                        });

                    x.RunAsLocalSystem();

                    x.SetDescription("Serviço do Translogic SOA");
                    x.SetDisplayName("Translogic.SOA.Host");
                    x.SetServiceName("Translogic.SOA.Host");
                });
        }
    }
}