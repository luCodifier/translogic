﻿namespace Translogic.SOA.Host.Infra
{
    using System;
    using System.Diagnostics;
    using Castle.Core.Logging;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;

    /// <summary>
    /// Classe de start do serviço
    /// </summary>
    public class HostBootstrapper
    {
        private const string EventSource = "Translogic";

        /// <summary>
        /// Propriedade de log
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Método que inicia o serviço
        /// </summary>
        public void Start()
        {
            if (!EventLog.SourceExists(EventSource))
            {
                EventLog.CreateEventSource(EventSource, "Application");
            }

            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForJobRunner();
        }

        /// <summary>
        /// Método que para o serviço
        /// </summary>
        public void Stop()
        {
            TranslogicStarter.Container.Dispose();
        }
    }
}
