﻿namespace Translogic.SOA.Host.Infra
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Text;
    using System.Xml;
    using Castle.MicroKernel.SubSystems.Conversion;

    /// <summary>
    /// Classe responsável por carregar as configurações dos serviços wcf
    /// </summary>
    public class ServicesSettings
    {
        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="servicos">Serviços carregados do services.config</param>
        /// <param name="servicosExcluidos">Serviços exluidos carregados do services.config</param>
        public ServicesSettings(IEnumerable<ServerHostSettings> servicos, IEnumerable<string> servicosExcluidos)
        {
            this.Servicos = servicos;
            this.ServicosExcluidos = servicosExcluidos;
        }

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="servicos">Serviços carregados do services.config</param>
        public ServicesSettings(IEnumerable<ServerHostSettings> servicos)
        {
            this.Servicos = servicos;
            this.ServicosExcluidos = new List<string>();
        }

        /// <summary>
        ///  configurações dos Serviços
        /// </summary>
        public IEnumerable<ServerHostSettings> Servicos { get; set; }

        /// <summary>
        /// Serviços que não devem ser carregados
        /// </summary>
        public IEnumerable<string> ServicosExcluidos { get; set; }
    }

    /// <summary>
    /// Classe de configuração do serviço
    /// </summary>
    [Convertible]
    public class ServerHostSettings
    {
        private const string Format = "{0}://{1}:{2}/{3}";

        /// <summary>
        /// Nome do serviço
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Protocolo do serviço
        /// </summary>
        public string Protocol { get; set; }

        /// <summary>
        /// Endereço do serviço
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Porta do serviço
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Publica metadata
        /// </summary>
        public bool PublishMetadata { get; set; }

        /// <summary>
        /// Log da mensagem recebido
        /// </summary>
        public bool LogMessage { get; set; }

        /// <summary>
        /// Porta do Metadata
        /// </summary>
        public int MetadataPort { get; set; }

        /// <summary>
        /// Nome do Biding especifico
        /// </summary>
        public string BindingName { get; set; }

        /// <summary>
        /// Valida os dados do serviço
        /// </summary>
        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Protocol))
            {
                throw new ArgumentException("O protocolo (Protocol) deve ser informado", "Protocol");
            }

            if (string.IsNullOrWhiteSpace(Address))
            {
                throw new ArgumentException("O endereço (Address) deve ser informado", "Address");
            }

            if (Port <= 0)
            {
                throw new ArgumentException("A porta (Port) deve ser informada (Port > 0)", "Port");
            }

            if (PublishMetadata && MetadataPort <= 0)
            {
                throw new ArgumentException("A porta (MetadataPort) deve ser informada (MetadataPort > 0)", "MetadataPort");
            }
        }

        /// <summary>
        /// Recupera o endereço do serviço 
        /// </summary>
        /// <param name="contract">Type do contrato</param>
        /// <returns>Endereço do serviço</returns>
        public Uri GetServiceAddress(Type contract)
        {
            if (Protocol.ToUpper().Trim().Equals("CUSTOM"))
            {
                return new Uri(GetAddress(contract, "http", Port));
            }
            else
            {
                return new Uri(GetAddress(contract, Protocol, Port));
            }
        }

        /// <summary>
        /// Recupera o endereço do metadata do serviço
        /// </summary>
        /// <param name="contract">type do contrato</param>
        /// <returns>Endereço do serviço</returns>
        public string GetMetadataServiceAddress(Type contract)
        {
            return GetAddress(contract, "http", MetadataPort);
        }

        /// <summary>
        /// Reescrita do método tostring
        /// </summary>
        /// <returns>Dados do serviço</returns>
        public override string ToString()
        {
            return string.Format("Protocol: {0}, Address: {1}, Port: {2}", Protocol, Address, Port);
        }

        /// <summary>
        /// Recupera o endereço no formato correto
        /// </summary>
        /// <param name="contract">type do contrato</param>
        /// <param name="protocol">Protocolo do serviço</param>
        /// <param name="port">porta do serviço</param>
        /// <returns>retorna o endereço</returns>
        private string GetAddress(Type contract, string protocol, int port)
        {
            string addr = string.Format(Format, protocol, Address, port, contract.Name);

            return addr;
        }
    }
}