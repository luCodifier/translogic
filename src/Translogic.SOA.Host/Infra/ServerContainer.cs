﻿namespace Translogic.SOA.Host.Infra
{
	using Castle.Core.Logging;
	using Castle.MicroKernel.Registration;
	using Castle.Windsor;
	using Castle.Windsor.Configuration.Interpreters;

	using Translogic.SOA.Host.Modules;

	public class ServerContainer : WindsorContainer
	{
		public ServerContainer() : base(new XmlInterpreter())
		{
			Logger = NullLogger.Instance;
		}

		public ILogger Logger { get; set; }

		public void Setup()
		{
			Logger = Resolve<ILogger>();

			SetupModules();
		}

		private void SetupModules()
		{
			Register(
				Component.For<IWindsorInstaller>().ImplementedBy<WCFInstaller>()
			);

			var installers = ResolveAll<IWindsorInstaller>();

			Install(installers);
		}
	}
}