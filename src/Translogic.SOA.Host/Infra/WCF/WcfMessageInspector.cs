﻿namespace Translogic.SOA.Host.Infra.WCF
{
    using System;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;
    using System.Text;
    using Castle.Core.Logging;
    using Enumeradores;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;

    /// <summary>
    /// Classe de controle o receive e response do webservice.
    /// </summary>
    public class WcfMessageInspector : IDispatchMessageInspector
    {
        private readonly IWsTicketLogRecebimentoRepository _ticketLogRecebimentoRepository;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="ticketLogRecebimentoRepository">Repositório de log de recebimento.</param>
        public WcfMessageInspector(IWsTicketLogRecebimentoRepository ticketLogRecebimentoRepository)
        {
            _ticketLogRecebimentoRepository = ticketLogRecebimentoRepository;
            Logger = NullLogger.Instance;
        }

        /// <summary>
        /// Propriedade de log
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Método disparado ao receber a requisição
        /// </summary>
        /// <param name="request">Mensagem Requisição</param>
        /// <param name="channel">Canal de comunicação</param>
        /// <param name="instanceContext">objeto Contexto</param>
        /// <returns> objeto de retorno</returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            try
            {
                object correlationState = null;

                if (!string.IsNullOrEmpty(request.ToString()))
                {
                    StringBuilder log = new StringBuilder();
                    log.AppendLine("Mensagem: " + request.ToString());

                    var ticktRetorno = _ticketLogRecebimentoRepository.Inserir(new WsTicketLogRecebimento()
                    {
                        LogMensagem = log.ToString(),
                        DataCadastro = DateTime.Now
                    });

                    Logger.Info(request.ToString());

                    correlationState = ticktRetorno;
                    return correlationState;
                }
            }
            catch (Exception)
            {
            }

            return null;
        }

        /// <summary>
        /// Método disparado na resposta a requisição
        /// </summary>
        /// <param name="reply">Mensagem de resposta</param>
        /// <param name="correlationState">Stato da resposta</param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            try
            {
                if (!reply.Headers.Action.Contains(ALL.Core.Util.Enum<ServicesEnum>.GetDescriptionOf(ServicesEnum.ConclusaoExportacao)) &&
                    !reply.Headers.Action.Contains(ALL.Core.Util.Enum<ServicesEnum>.GetDescriptionOf(ServicesEnum.Cancelamento)) &&
                    !reply.Headers.Action.Contains(ALL.Core.Util.Enum<ServicesEnum>.GetDescriptionOf(ServicesEnum.Rodoviario)))
                {
                    WsTicketLogRecebimento ticketLog = correlationState as WsTicketLogRecebimento;

                    if (ticketLog != null)
                    {
                        ticketLog.LogMensagemRetorno = reply.ToString();
                        _ticketLogRecebimentoRepository.Atualizar(ticketLog);
                    }

                    Logger.Info(reply.ToString());    
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
