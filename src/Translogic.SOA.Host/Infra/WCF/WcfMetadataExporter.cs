﻿namespace Translogic.SOA.Host.Infra.WCF
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using System.Xml.Schema;

    /// <summary>
    /// Metadata exporter para gerar um único arquivo
    /// </summary>
    public class WcfMetadataExporter : IWsdlExportExtension, IEndpointBehavior
    {
        /// <summary>
        /// Método para export do contrato
        /// </summary>
        /// <param name="exporter">Instância do 'Exporter' do WSDL</param>
        /// <param name="context">Instância do contexto do WSDL</param>
        public void ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
        {
        }

        /// <summary>
        /// Método para export do endpoint
        /// </summary>
        /// <param name="exporter">Instância do 'Exporter' do WSDL</param>
        /// <param name="context">Instância do contexto do WSDL</param>
        public void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
        {
            XmlSchemaSet schemaSet = exporter.GeneratedXmlSchemas;
            foreach (System.Web.Services.Description.ServiceDescription wsdl in exporter.GeneratedWsdlDocuments)
            {
                List<XmlSchema> importsList = new List<XmlSchema>();
                foreach (XmlSchema schema in wsdl.Types.Schemas)
                {
                    __addImportedSchemas(schema, schemaSet, importsList);
                }

                if (importsList.Count == 0)
                {
                    return;
                }

                wsdl.Types.Schemas.Clear();

                foreach (XmlSchema schema in importsList)
                {
                    __removeXsdImports(schema);
                    wsdl.Types.Schemas.Add(schema);
                }
            }
        }

        /// <summary>
        /// Adiciona novos parametro para o binding
        /// </summary>
        /// <param name="endpoint">EndPoint de destino</param>
        /// <param name="bindingParameters">Lista de parâmetros</param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// ApplyClientBehavior method
        /// </summary>
        /// <param name="endpoint">EndPoint de destino</param>
        /// <param name="clientRuntime">Runtime do cliente</param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        /// <summary>
        /// ApplyDispatchBehavior method
        /// </summary>
        /// <param name="endpoint">EndPoint de destino</param>
        /// <param name="endpointDispatcher">Dispatcher do endpoint</param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        /// <summary>
        /// Validação do endpoint
        /// </summary>
        /// <param name="endpoint">Endpoint de destino</param>
        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #region Private methods

        private void __addImportedSchemas(XmlSchema schema, XmlSchemaSet schemaSet, List<XmlSchema> importsList)
        {
            foreach (XmlSchemaImport import in schema.Includes)
            {
                ICollection realSchemas = schemaSet.Schemas(import.Namespace);

                foreach (XmlSchema ixsd in realSchemas)
                {
                    if (!importsList.Contains(ixsd))
                    {
                        importsList.Add(ixsd);
                        __addImportedSchemas(ixsd, schemaSet, importsList);
                    }
                }
            }
        }

        private void __removeXsdImports(XmlSchema schema)
        {
            for (int i = 0; i < schema.Includes.Count; i++)
            {
                if (schema.Includes[i] is XmlSchemaImport)
                {
                    schema.Includes.RemoveAt(i--);
                }
            }
        }

        #endregion
    }
}