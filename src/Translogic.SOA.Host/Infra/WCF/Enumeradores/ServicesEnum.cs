﻿namespace Translogic.SOA.Host.Infra.WCF.Enumeradores
{
    using System.ComponentModel;

    /// <summary>
    /// Enumerador de Servicos
    /// </summary>
    public enum ServicesEnum
    {
        /// <summary>
        /// Servico de Conclusao e Exportacao das Ctes
        /// </summary>
        [Description("ICteConclusaoExportacaoService/InformationCteConclusaoExportacaoResponse")]
        ConclusaoExportacao,

        /// <summary>
        /// Servico de Ctes Cancelados
        /// </summary>
        [Description("ICteFerroviarioCanceladoService/InformationCteResponse")]
        Cancelamento,

        /// <summary>
        /// Servico de Ctes Rodoviarios
        /// </summary>
        [Description("ICteRodoviarioService/InformationCteRodoviarioResponse")]
        Rodoviario
    }
}
