﻿namespace Translogic.SOA.Host.Infra.WCF
{
    using System;
    using System.Collections.ObjectModel;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// Atributo do wcf message Inspection
    /// </summary>
    public class WcfMessageInspectionBehavior : IServiceBehavior
    {
        private readonly WcfMessageInspector _wcfMessageInspector;

        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="wcfMessageInspector">Objeto inspector message</param>
        public WcfMessageInspectionBehavior(WcfMessageInspector wcfMessageInspector)
        {
            _wcfMessageInspector = wcfMessageInspector;
        }

        /// <summary>
        /// Implement to confirm that the contract and endpoint can support the contract behavior.
        /// </summary>
        /// <param name="serviceDescription">Service descrição</param>
        /// <param name="serviceHostBase">Base Service host</param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        /// <summary>
        /// Configures any binding elements to support the contract behavior.
        /// </summary>
        /// <param name="serviceDescription">Service descrição</param>
        /// <param name="serviceHostBase">Base Service host</param>
        /// <param name="endpoints">The endpoint that exposes the contract.</param>
        /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Implements a modification or extension of the client across a contract.
        /// </summary>
        /// <param name="serviceDescription">Service descrição</param>
        /// <param name="serviceHostBase">Base Service host</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatch in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher endpointDispatch in channelDispatch.Endpoints)
                {
                    endpointDispatch.DispatchRuntime.MessageInspectors.Add(_wcfMessageInspector);
                }
            }
        }
    }
}