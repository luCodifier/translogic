﻿namespace Translogic.SOA.Host.Infra.WCF
{
	using System;
	using System.ServiceModel;
	using System.ServiceModel.Channels;
	using System.ServiceModel.Dispatcher;

	using Castle.Core.Logging;

    /// <summary>
    /// Classe de erro de wcf
    /// </summary>
	public class WcfErrorHandler : IErrorHandler
	{
        /// <summary>
        /// Construtor da classe
        /// </summary>
		public WcfErrorHandler()
		{
			Logger = NullLogger.Instance;
		}

        /// <summary>
        /// Propriedade de log
        /// </summary>
		public ILogger Logger { get; set; }

        /// <summary>
        /// Método de exceção
        /// </summary>
        /// <param name="error">exception de Erro</param>
        /// <param name="version">versão da mensagem </param>
        /// <param name="fault">Mensagem de erro</param>
		public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
		{
			FaultException faultException = new FaultException(error.Message);

			MessageFault messageFault = faultException.CreateMessageFault();

			fault = Message.CreateMessage(version, messageFault, faultException.Action);
		}

        /// <summary>
        /// Efetua o tratamento do erro
        /// </summary>
        /// <param name="error">Exception de erro</param>
        /// <returns>retorna true</returns>
		public bool HandleError(Exception error)
		{
			Logger.Error(error.Message, error);

			return true;
		}
	}
}