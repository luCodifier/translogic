﻿namespace Translogic.SOA.Host.Infra
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Security;
    using System.Text;
    using System.Xml;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Core.Infrastructure.WCF.GzipEncoder;
    using Translogic.Modules.Core.Interfaces.Simconsultas.Interfaces;
    using Translogic.SOA.Host.Infra.WCF;

    /// <summary>
    /// Classe responsável por criar e configurar as configurações dos serviços
    /// </summary>
    public class ServerConfigure
    {
        private Dictionary<string, Binding> _bindings;
        private ServicesSettings _settings;
        private IWindsorContainer _container;

        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="container">Container do windosor</param>
        public ServerConfigure(IWindsorContainer container)
        {
            this._container = container;
            this.LoadServicesSettings(container);
        }

        /// <summary>
        /// Cria o Binding para o serviço
        /// </summary>
        /// <param name="config">Configurações do serviço</param>
        /// <returns>Retorna o Biding</returns>
        public Binding CreateBinding(ServerHostSettings config)
        {
            if (_bindings == null)
            {
                _bindings = new Dictionary<string, Binding>();
            }

            if (_bindings.Any(x => x.Key.Equals(string.Format("{0}:{1}", config.Protocol.ToUpperInvariant(), config.Port))))
            {
                return _bindings.SingleOrDefault(x => x.Key.Equals(string.Format("{0}:{1}", config.Protocol.ToUpperInvariant(), config.Port))).Value;
            }

            Binding binding = null;
            switch (config.Protocol)
            {
                case "net.tcp":
                    binding = CreateNetTcpBinding(config);
                    break;
                case "http":
                    binding = CreateBasicHttpBinding(config);
                    break;
                case "custom":
                    binding = CreateCustomBinding(config);
                    break;
                default:
                    throw new ArgumentException(string.Format("Protocolo '{0}' não reconhecido", config.Protocol), "settings.Protocol");
            }

            // _bindings.Add(string.Format("{0}:{1}", config.Protocol.ToUpperInvariant(), config.Port), binding);
            return binding;
        }

        /// <summary>
        ///  Obtem as configurações para o serviço
        /// </summary>
        /// <param name="serviceName">Nome do serviço</param>
        /// <returns>Config do serviço</returns>
        public IList<ServerHostSettings> ObterConfig(string serviceName)
        {
            if (_settings == null)
            {
                throw new ArgumentException("Configurações não carregadas");
            }

            if (_settings.Servicos.Any(x => x.ServiceName.ToUpperInvariant().Equals(serviceName.ToUpperInvariant())))
            {
                return _settings.Servicos.Where(x => x.ServiceName.ToUpperInvariant().Equals(serviceName.ToUpperInvariant())).ToList();
            }

            if (_settings.Servicos.Any(x => x.ServiceName.ToUpperInvariant().Equals("DEFAULT")))
            {
                return _settings.Servicos.Where(x => x.ServiceName.ToUpperInvariant().Equals("DEFAULT")).ToList();
            }

            throw new ArgumentException("Não foi localizado uma configurações DEFAULT");
        }

        /// <summary>
        /// Registra o behavior
        /// </summary>
        /// <param name="settings">Config do serviço</param>
        public void RegisterBehaviors(ServerHostSettings settings)
        {
            var returnFaults = new ServiceDebugBehavior
            {
                IncludeExceptionDetailInFaults = true,
                HttpHelpPageEnabled = settings.PublishMetadata
            };

            var metadata = new ServiceMetadataBehavior { HttpGetEnabled = settings.PublishMetadata };

            _container.Register(
                // Component.For<IServiceBehavior>().Instance(returnFaults),
                // Component.For<IServiceBehavior>().Instance(metadata),
                    Component.For<WcfMessageInspectionBehavior>(),
                    Component.For<WcfErrorHandler>()
            );
        }

        /// <summary>
        /// Carrega as configurações
        /// </summary>
        /// <param name="container">Container do translogic</param>
        public void LoadServicesSettings(IWindsorContainer container)
        {
            container.Register(
              Component.For<ServicesSettings>().Named("server.host.services")
          );

            _settings = container.Resolve<ServicesSettings>();
        }

        /// <summary>
        /// Verifica se é prara excluir o serviço
        /// </summary>
        /// <param name="serviceName">Nome do serviço</param>
        /// <returns>Retorna true se o serviço for excluido</returns>
        public bool ServicoExcluido(string serviceName)
        {
            return _settings.ServicosExcluidos.Any(x => x.ToUpperInvariant().Equals(serviceName.ToUpperInvariant()));
        }

        private NetTcpBinding CreateNetTcpBinding(ServerHostSettings config)
        {
            var binding = new NetTcpBinding(SecurityMode.None);

            if (string.IsNullOrEmpty(config.BindingName))
            {
                var quotas = CreateReaderQuotas();

                binding.TransactionFlow = true;
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.ReaderQuotas = quotas;
                binding.ReceiveTimeout = new TimeSpan(1, 0, 0, 0);
                binding.ReliableSession.Enabled = true;
                binding.ReliableSession.InactivityTimeout = new TimeSpan(1, 0, 0, 0);
            }
            else
            {
                return new NetTcpBinding(config.BindingName);
            }

            return binding;
        }

        private BasicHttpBinding CreateBasicHttpBinding(ServerHostSettings config)
        {
            if (string.IsNullOrEmpty(config.BindingName))
            {
                var binding = new BasicHttpBinding(BasicHttpSecurityMode.None);

                var quotas = CreateReaderQuotas();
                binding.HostNameComparisonMode = new HostNameComparisonMode();
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.MaxBufferPoolSize = int.MaxValue;
                binding.ReaderQuotas = quotas;
                binding.ReceiveTimeout = new TimeSpan(1, 0, 0, 0);
                return binding;
            }

            return new BasicHttpBinding(config.BindingName);
        }

        private CustomBinding CreateCustomBinding(ServerHostSettings config)
        {
            return new CustomBinding(config.BindingName);
        }

        private XmlDictionaryReaderQuotas CreateReaderQuotas()
        {
            XmlDictionaryReaderQuotas quotasReturno = null;

            quotasReturno = new XmlDictionaryReaderQuotas
            {
                MaxArrayLength = Int32.MaxValue,
                MaxBytesPerRead = Int32.MaxValue,
                MaxDepth = Int32.MaxValue,
                MaxNameTableCharCount = Int32.MaxValue,
                MaxStringContentLength = Int32.MaxValue
            };

            return quotasReturno;
        }
    }
}