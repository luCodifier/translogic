﻿namespace Translogic.SOA.Host.Infra.Behavior
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.Xml;

    /// <summary>
    /// Cria um <see cref="DataContractSerializer"/> com a opção de preservar as referências dos objetos
    /// </summary>
    public class SerializationOperationBehavior : DataContractSerializerOperationBehavior
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="operation">Instância de <see cref="OperationDescription"/></param>
        public SerializationOperationBehavior(OperationDescription operation)
            : base(operation)
        {
        }

        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="operation">Instância de <see cref="OperationDescription"/></param>
        /// <param name="dataContractFormatAttribute">Instância de <see cref="DataContractFormatAttribute"/></param>
        public SerializationOperationBehavior(OperationDescription operation, DataContractFormatAttribute dataContractFormatAttribute)
            : base(operation, dataContractFormatAttribute)
        {
        }

        /// <summary>
        /// Cria um <see cref="DataContractSerializer"/>
        /// </summary>
        /// <param name="type"><see cref="Type"/> da serialização</param>
        /// <param name="name">Nome  da serialização</param>
        /// <param name="ns">Namespace da serialização</param>
        /// <param name="knownTypes">Lista de <see cref="KnownTypeAttribute"/></param>
        /// <returns>Instância de <see cref="DataContractSerializer"/></returns>
        public override XmlObjectSerializer CreateSerializer(Type type, string name, string ns, IList<Type> knownTypes)
        {
            return new DataContractSerializer(type, name, ns, knownTypes, Int32.MaxValue, IgnoreExtensionDataObject, true, DataContractSurrogate);
        }

        /// <summary>
        /// Cria um <see cref="DataContractSerializer"/>
        /// </summary>
        /// <param name="type"><see cref="Type"/> da serialização</param>
        /// <param name="name">Nome  da serialização</param>
        /// <param name="ns">Namespace da serialização</param>
        /// <param name="knownTypes">Lista de <see cref="KnownTypeAttribute"/></param>
        /// <returns>Instância de <see cref="DataContractSerializer"/></returns>
        public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
        {
            return new DataContractSerializer(type, name, ns, knownTypes, Int32.MaxValue, IgnoreExtensionDataObject, true, DataContractSurrogate);
        }
    }
}
