namespace Translogic.Core.Infrastructure.Web.Providers
{
	using System;
	using System.Web.Security;
	using Microsoft.Practices.ServiceLocation;

	/// <summary>
	/// Implementa��o do membershipProvider
	/// </summary>
	public class MembershipProvider : BaseMembershipProvider
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly IControleAcessoService _controleAcessoService;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="controleAcessoService">Injeta o servi�o de controle de acesso</param>
		public MembershipProvider(IControleAcessoService controleAcessoService)
		{
			_controleAcessoService = controleAcessoService;
		}

		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		public MembershipProvider()
			: this(ServiceLocator.Current.GetInstance<IControleAcessoService>())
		{
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Valida o usuario
		/// </summary>
		/// <param name="username">C�digo do usuario</param>
		/// <param name="password">Senha do usu�rio</param>
		/// <returns>Valor booleano</returns>
		public override bool ValidateUser(string username, string password)
		{
			return _controleAcessoService.Validar(username, password);
		}

		#endregion
	}

	/// <summary>
	/// Implementa��o de classe base de membership provider
	/// </summary>
	public abstract class BaseMembershipProvider : System.Web.Security.MembershipProvider
	{
		#region PROPRIEDADES

		/// <summary>
		/// Nome da aplica��o
		/// </summary>
		public override string ApplicationName
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

        /// <summary>
        /// Indica se habilita o reset da senha
        /// </summary>
		public override bool EnablePasswordReset
		{
			get { return false; }
		}

		/// <summary>
		/// Indica se habilita retornar a senha
		/// </summary>
		public override bool EnablePasswordRetrieval
		{
			get { return false; }
		}

		/// <summary>
		/// Quantidade de senhas inv�lidas
		/// </summary>
		public override int MaxInvalidPasswordAttempts
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Quantidade m�nima requerida de n�o alfanum�ricos
		/// </summary>
		public override int MinRequiredNonAlphanumericCharacters
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Tamanho m�nimo da senha
		/// </summary>
		public override int MinRequiredPasswordLength
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Janela de lembrete de senha
		/// </summary>
		public override int PasswordAttemptWindow
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Formato de senha
		/// </summary>
		public override MembershipPasswordFormat PasswordFormat
		{
			get { throw new NotImplementedException(); }
		}

        /// <summary>
        /// For�a da senha com express�es regulares
        /// </summary>
		public override string PasswordStrengthRegularExpression
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Necessita de pergunta e resposta
		/// </summary>
		public override bool RequiresQuestionAndAnswer
		{
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Necessita e-mail �nico
		/// </summary>
		public override bool RequiresUniqueEmail
		{
			get { throw new NotImplementedException(); }
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Valida usuario
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="password">senha do usuario</param>
		/// <returns>valor bool</returns>
		public abstract override bool ValidateUser(string username, string password);

		/// <summary>
		/// Muda a senha
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="oldPassword">senha antiga</param>
		/// <param name="newPassword">senha nova</param>
		/// <returns>valor booleano</returns>
		public override bool ChangePassword(string username, string oldPassword, string newPassword)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Mudan�a de senha com pergunta/resposta
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="password">senha do usuario</param>
		/// <param name="newPasswordQuestion">nova pergunta</param>
		/// <param name="newPasswordAnswer">nova senha</param>
		/// <returns>valor booleano</returns>
		public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Cria novo usuario
		/// </summary>
		/// <param name="username">Nome do usuairo</param>
		/// <param name="password">senha do usuario</param>
		/// <param name="email">email do usuario</param>
		/// <param name="passwordQuestion">pergunta de lembrete</param>
		/// <param name="passwordAnswer">resposta de lembrete</param>
		/// <param name="isApproved">est� ativo</param>
		/// <param name="providerUserKey">provider user key</param>
		/// <param name="status">status do usuario</param>
		/// <returns>Usuario - <see cref="MembershipUser"/></returns>
		public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Apaga usuario
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="deleteAllRelatedData">apagar todos os dados relacionados?</param>
		/// <returns>valor booleano</returns>
		public override bool DeleteUser(string username, bool deleteAllRelatedData)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Localizar usuarios por e-mail
		/// </summary>
		/// <param name="emailToMatch">email do usuario</param>
		/// <param name="pageIndex">�ndice da pagina</param>
		/// <param name="pageSize">quantidade de registros</param>
		/// <param name="totalRecords">quantidade total de usuarios</param>
		/// <returns>Cole��o de usuarios - <see cref="MembershipUserCollection"/></returns>
		public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Pesquisa de usuarios por nome
		/// </summary>
		/// <param name="usernameToMatch">nome do usuario</param>
		/// <param name="pageIndex">indice da pagina</param>
		/// <param name="pageSize">tamanho da pagina</param>
		/// <param name="totalRecords">quantidade de usuarios</param>
		/// <returns>Lista de usuarios - <see cref="MembershipUserCollection"/></returns>
		public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna todos os usuarios
		/// </summary>
		/// <param name="pageIndex">indice da pagina</param>
		/// <param name="pageSize">tamanho da pagina</param>
		/// <param name="totalRecords">quantidade de registros</param>
		/// <returns>Lista de usuarios - <see cref="MembershipUserCollection"/></returns>
		public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// pega a quantidade de usuarios online
		/// </summary>
		/// <returns>quantidade de usuarios</returns>
		public override int GetNumberOfUsersOnline()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna a senha
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="answer">resposta da pergunta</param>
		/// <returns>senha do usuario</returns>
		public override string GetPassword(string username, string answer)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna o usuario
		/// </summary>
		/// <param name="providerUserKey">provider user key</param>
		/// <param name="userIsOnline">est� online?</param>
		/// <returns>Usuario - <see cref="MembershipUser"/></returns>
		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna o usuario
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="userIsOnline">est� on-line?</param>
		/// <returns>Usuario - <see cref="MembershipUser"/></returns>
		public override MembershipUser GetUser(string username, bool userIsOnline)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna nomes de usuarios por e-mail
		/// </summary>
		/// <param name="email">email informado</param>
		/// <returns>String de usuarios</returns>
		public override string GetUserNameByEmail(string email)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Reseta a senha
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="answer">resposta da pergunta</param>
		/// <returns>senha nova</returns>
		public override string ResetPassword(string username, string answer)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Desbloquear usuario
		/// </summary>
		/// <param name="userName">Nome do usuario</param>
		/// <returns>valor booleano</returns>
		public override bool UnlockUser(string userName)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Altera dadso do usuario
		/// </summary>
		/// <param name="user">Usuario - <see cref="MembershipUser"/></param>
		public override void UpdateUser(MembershipUser user)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}