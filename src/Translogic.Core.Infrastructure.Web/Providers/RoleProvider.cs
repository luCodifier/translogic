namespace Translogic.Core.Infrastructure.Web.Providers
{
	using System;
	using Microsoft.Practices.ServiceLocation;

	/// <summary>
	/// Role provider
	/// </summary>
	public class RoleProvider : BaseRoleProvider
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly IControleAcessoService _controleAcessoService;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Incializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="controleAcessoService">controle de acesso injetado</param>
		public RoleProvider(IControleAcessoService controleAcessoService)
		{
			_controleAcessoService = controleAcessoService;
		}

		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		public RoleProvider()
			: this(ServiceLocator.Current.GetInstance<IControleAcessoService>())
		{
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Retorna as regras do usuario
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <returns>array de strings</returns>
		public override string[] GetRolesForUser(string username)
		{
			return _controleAcessoService.ObterGruposPorUsuario(username);
		}

		#endregion
	}

	/// <summary>
	/// Base role provider
	/// </summary>
	public abstract class BaseRoleProvider : System.Web.Security.RoleProvider
	{
		#region PROPRIEDADES
		/// <summary>
		/// Nome da aplica��o
		/// </summary>
		public override string ApplicationName
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Retorna as regras do usuario
		/// </summary>
		/// <param name="username">codigo do usuario</param>
		/// <returns>array de string</returns>
		public abstract override string[] GetRolesForUser(string username);

		/// <summary>
		/// Adiciona usuarios para senhas
		/// </summary>
		/// <param name="usernames">array de usuarios</param>
		/// <param name="roleNames">array de regras</param>
		public override void AddUsersToRoles(string[] usernames, string[] roleNames)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Cria regra
		/// </summary>
		/// <param name="roleName">nome da regra</param>
		public override void CreateRole(string roleName)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Apaga regra
		/// </summary>
		/// <param name="roleName">nome da regra</param>
		/// <param name="throwOnPopulatedRole">bool de regra populada</param>
		/// <returns>valor booleano</returns>
		public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Pesquisa usuarios na regra
		/// </summary>
		/// <param name="roleName">nome da regra</param>
		/// <param name="usernameToMatch">nome do usuario</param>
		/// <returns>array de string</returns>
		public override string[] FindUsersInRole(string roleName, string usernameToMatch)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retorna todas as regras
		/// </summary>
		/// <returns>array de string</returns>
		public override string[] GetAllRoles()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// retorna usuarios da regra
		/// </summary>
		/// <param name="roleName">nome da regra</param>
		/// <returns>array de string</returns>
		public override string[] GetUsersInRole(string roleName)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Verifica se o usuario est� na regra
		/// </summary>
		/// <param name="username">nome do usuario</param>
		/// <param name="roleName">nome da regra</param>
		/// <returns>valor booleano</returns>
		public override bool IsUserInRole(string username, string roleName)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// remove usaurios das regras
		/// </summary>
		/// <param name="usernames">array de usuarios</param>
		/// <param name="roleNames">array de regras</param>
		public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Verifica se a regra existe
		/// </summary>
		/// <param name="roleName">nome da regra</param>
		/// <returns>valor booleano</returns>
		public override bool RoleExists(string roleName)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}