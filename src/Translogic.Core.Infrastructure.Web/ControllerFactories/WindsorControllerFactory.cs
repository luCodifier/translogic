namespace Translogic.Core.Infrastructure.Web.ControllerFactories
{
	using System;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Routing;
	using Castle.MicroKernel;

	/// <summary>
	/// Controller factory do windsor
	/// </summary>
	public class WindsorControllerFactory : DefaultControllerFactory
	{
		private readonly IKernel kernel;

		/// <summary>
		/// Construtor injetando kernel
		/// </summary>
		/// <param name="kernel">Kernel do container</param>
		public WindsorControllerFactory(IKernel kernel)
		{
			this.kernel = kernel;
		}

		/// <summary>
		/// Removendo controller do container
		/// </summary>
		/// <param name="controller">Controller a ser removido</param>
		public override void ReleaseController(IController controller)
		{
			kernel.ReleaseComponent(controller);
		}

		/// <summary>
		/// Obt�m inst�ncia do controller
		/// </summary>
		/// <param name="requestContext">Contexto do request</param>
		/// <param name="controllerType">Tipo do controller</param>
		/// <returns>Inst�ncia de um controller</returns>
		protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
		{
			if (controllerType == null)
			{
				throw new HttpException(404, string.Format("The controller for path '{0}' could not be found.", requestContext.HttpContext.Request.Path));
			}

			return (IController)kernel.Resolve(controllerType);
		}
	}
}