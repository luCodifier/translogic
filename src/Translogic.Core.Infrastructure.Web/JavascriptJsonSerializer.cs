namespace Translogic.Core.Infrastructure.Web
{
	using System.Web.Script.Serialization;

	/// <summary>
	/// Implementa��o de serializa��o JSON
	/// </summary>
	public class JavascriptJsonSerializer : IJsonSerializer
	{
		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly JavaScriptSerializer _serializer = new JavaScriptSerializer();

		#endregion

		#region IJsonSerializer MEMBROS
		/// <summary>
		/// Decodifica uma string JSON em objeto
		/// </summary>
		/// <typeparam name="T">Tipo do objeto que ser� feito o cast</typeparam>
		/// <param name="json">String JSON</param>
		/// <returns>Objeto convertido</returns>
		public T Decode<T>(string json)
		{
			return _serializer.Deserialize<T>(json);
		}

		/// <summary>
		/// Codifica um objeto em string JSON
		/// </summary>
		/// <param name="data">Objeto a ser transformado</param>
		/// <returns>String JSON</returns>
		public string Encode(object data)
		{
			return _serializer.Serialize(data);
		}

		#endregion
	}
}