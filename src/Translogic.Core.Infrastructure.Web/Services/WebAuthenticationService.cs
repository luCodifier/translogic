namespace Translogic.Core.Infrastructure.Web.Services
{
	using System;
	using System.Web;
	using System.Web.Security;

	/// <summary>
	/// Servi�o para autentica��o Web.
	/// </summary>
	public class WebAuthenticationService
	{
		#region ATRIBUTOS

		private readonly HttpContextBase _context;

		#endregion

		#region CONSTRUTORES

		/// <summary>
		/// Inicializa uma nova inst�ncia da classe passando o contexto como par�metro.
		/// </summary>
		/// <param name="context">
		/// Contexto Http da aplica��o.
		/// </param>
		public WebAuthenticationService(HttpContextBase context)
		{
			_context = context;
		}

		/// <summary>
		/// Inicializa uma nova inst�ncia da classe sem passagem de par�metros
		/// </summary>
		public WebAuthenticationService()
		{
			_context = new HttpContextWrapper(HttpContext.Current);
		}

		#endregion

		#region M�TODOS
		/*
		/// <summary>
		/// Retorna o �ltimo ticket de autentica��o
		/// </summary>
		/// <returns>Ticket de autentica��o</returns>
		public static FormsAuthenticationTicket GetLastTicket()
		{
			for (int i = HttpContext.Current.Request.Cookies.Count - 1; i >= 0; i--)
			{
				if (HttpContext.Current.Request.Cookies[i].Name == FormsAuthentication.FormsCookieName)
				{
					try
					{
						FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies[i].Value);
						return ticket;
					}
					catch (Exception x)
					{
					}
				}
			}

			return null;
		}
	   */

		/// <summary>
		/// Escreve o cookie de autentica��o
		/// </summary>
		/// <param name="userName">
		/// C�digo do usu�rio.
		/// </param>
		/// <param name="createPersistentCookie">
		/// Indica se � para fazer um cookie persistente.
		/// </param>
		public virtual void WriteAuthenticationCookie(string userName, bool createPersistentCookie)
		{
			HttpCookie cookie = FormsAuthentication.GetAuthCookie(userName, createPersistentCookie, "/");

			if (_context.Request.Cookies[cookie.Name] != null)
			{
				_context.Request.Cookies.Remove(cookie.Name);
			}

			_context.Request.Cookies.Add(cookie);
		}

		/// <summary>
		/// Indica se a sess�o est� autenticada
		/// </summary>
		/// <returns>
		/// Retorna valor bool
		/// </returns>
		public bool IsSessionAuthenticated()
		{
			var authCookie = _context.Request.Cookies[FormsAuthentication.FormsCookieName];

			if (authCookie == null || string.IsNullOrEmpty(authCookie.Value))
			{
				return false;
			}

			try
			{
				FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

				if (ticket.Expired)
				{
					FormsAuthentication.RenewTicketIfOld(ticket);
				}
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Remove o cookie de autentica��o
		/// </summary>
		public void RemoveAuthCookie()
		{
			_context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
			FormsAuthentication.SignOut();
		}

		#endregion
	}
}