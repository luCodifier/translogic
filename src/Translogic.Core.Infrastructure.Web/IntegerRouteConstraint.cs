namespace Translogic.Core.Infrastructure.Web
{
	using System.Web;
	using System.Web.Routing;

	/// <summary>
	/// Restri��o de rota que deve vir um par�metro num�rico inteiro
	/// </summary>
	public class IntegerRouteConstraint : IRouteConstraint
	{
		#region IRouteConstraint MEMBROS
		/// <summary>
		/// Verifica se o par�metro que est� vindo na rota � um numero inteiro
		/// </summary>
		/// <param name="httpContext">Contexto Http</param>
		/// <param name="route">Rota do mvc</param>
		/// <param name="parameterName">Nome do par�metro</param>
		/// <param name="values">Valores passados</param>
		/// <param name="routeDirection">Dire��o da rota - <see cref="RouteDirection"/></param>
		/// <returns>Valor booleano</returns>
		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
		{
			int parameterValue;

			return int.TryParse(values[parameterName].ToString(), out parameterValue);
		}

		#endregion
	}
}