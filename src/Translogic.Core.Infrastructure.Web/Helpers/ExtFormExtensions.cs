namespace Translogic.Core.Infrastructure.Web.Helpers
{
	using System.Collections.Generic;

	/// <summary>
	/// Classe do ExtJs
	/// </summary>
	public static class ExtJs
	{
		#region PROPRIEDADES EST�TICAS
		/// <summary>
		/// Campo datepicker do ext
		/// </summary>
		public static ExtProperty DatePicker
		{
			get { return new ExtProperty("behavior", "date-picker"); }
		}

		/// <summary>
		/// Campo num�rico do ext
		/// </summary>
		public static ExtProperty Numeric
		{
			get { return new ExtProperty("behavior", "numeric"); }
		}

		/// <summary>
		/// Campo n�merico inteiro do ext
		/// </summary>
		public static ExtProperty Integer
		{
			get { return Numeric && new ExtProperty("allowDecimals", "false"); }
		}

		/// <summary>
		/// Campo n�o pode ser vazio do ext
		/// </summary>
		public static ExtProperty Required
		{
			get { return new ExtProperty("required", "true"); }
		}

		/// <summary>
		/// Campo com m�scara de e-mail
		/// </summary>
		public static ExtProperty ValidateAsEmail
		{
			get { return new ExtProperty("vtype", "email"); }
		}

		/// <summary>
		/// Campo somente leitura
		/// </summary>
		public static ExtProperty ReadOnly
		{
			get { return new ExtProperty("READONLY", "READONLY"); }
		}

		/// <summary>
		/// Campo num�rico decimal do ext
		/// </summary>
		/// <param name="decimalPrecision">Precis�o do decimal</param>
		/// <returns>Propriedade Ext - <see cref="ExtProperty"/></returns>
		public static ExtProperty Decimal(int decimalPrecision)
		{
			return Numeric && new ExtProperty("decimalPrecision", decimalPrecision);
		}

		/// <summary>
		/// Tamanho m�ximo do campo
		/// </summary>
		/// <param name="max">Tamano m�ximo de caracteres</param>
		/// <returns>Propriedade do Ext - <see cref="ExtProperty"/></returns>
		public static ExtProperty MaxLength(int max)
		{
			return new ExtProperty("maxLength", max);
		}

		/// <summary>
		/// Quantidade m�nima de caracteres do ext
		/// </summary>
		/// <param name="min">Quantidade m�nima de caracteres</param>
		/// <returns>Propriedade do Ext </returns>
		public static ExtProperty MinLength(int min)
		{
			return new ExtProperty("minLength", min);
		}

		/// <summary>
		/// Texto vazio
		/// </summary>
		/// <param name="emptyText">String que deve aparecer quando o texto estiver vazio</param>
		/// <returns>Propriedade do Ext</returns>
		public static ExtProperty EmptyText(string emptyText)
		{
			return new ExtProperty("emptyText", emptyText);
		}

		#endregion
	}
	
	/// <summary>
	/// Propriedade do Ext
	/// </summary>
	public class ExtProperty : Dictionary<string, object>
	{
		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		public ExtProperty()
		{
		}

        /// <summary>
        /// Inicializa uma nova instancia da classe
        /// </summary>
        /// <param name="key">chave da Propriedade</param>
        /// <param name="value">Valor da propriedade</param>
		public ExtProperty(string key, object value)
		{
			Add(key, value);
		}

		#endregion

		#region OPERADORES
		/// <summary>
		/// Sobreescreve o operador
		/// </summary>
		/// <param name="a">Propriedade a</param>
		/// <param name="b">Propriedade b</param>
		/// <returns>Propriedade concatenado o a com o b</returns>
		public static ExtProperty operator &(ExtProperty a, ExtProperty b)
		{
			foreach (var pair in b)
			{
				if (!a.ContainsKey(pair.Key))
				{
					a.Add(pair.Key, pair.Value);
					continue;
				}

				if (a[pair.Key] is string && pair.Value is string)
				{
					a[pair.Key] += " " + pair.Value;
				}
			}

			return a;
		}

		/// <summary>
		/// Sobrescreve o operador falso
		/// </summary>
		/// <param name="prop">Propriedade do ext</param>
		/// <returns>Valor falso</returns>
		public static bool operator false(ExtProperty prop)
		{
			return false;
		}

		/// <summary>
		/// Sobrescreve o operador verdadeiro
		/// </summary>
		/// <param name="prop">Propriedade do ext</param>
		/// <returns>Valor verdadeiro</returns>
		public static bool operator true(ExtProperty prop)
		{
			return true;
		}

		#endregion
	}
}