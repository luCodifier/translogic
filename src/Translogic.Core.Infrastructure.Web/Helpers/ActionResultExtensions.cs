﻿namespace Translogic.Core.Infrastructure.Web.Helpers
{
	using System;
	using System.Collections.Generic;
	using System.Drawing;
	using System.Globalization;
	using System.IO;
	using System.Text;
	using System.Web;
	using System.Web.Mvc;

	using ICSharpCode.SharpZipLib.Core;

	using iTextSharp.text;
	using iTextSharp.text.pdf;
	using iTextSharp.tool.xml;
	using iTextSharp.tool.xml.html;
	using iTextSharp.tool.xml.parser;
	using iTextSharp.tool.xml.pipeline.css;
	using iTextSharp.tool.xml.pipeline.end;
	using iTextSharp.tool.xml.pipeline.html;

	using Font = iTextSharp.text.Font;
	using Rectangle = iTextSharp.text.Rectangle;

	/// <summary>
	/// Classe estática de extensão do ActionResult
	/// </summary>
	public static class ActionResultExtensions
	{
		/// <summary>
		/// The capture.
		/// </summary>
		/// <param name="result"> The result. </param>
		/// <param name="controllerContext"> The controller context. </param>
		/// <returns> The <see cref="string"/>. </returns>
		public static string Capture(this ActionResult result, ControllerContext controllerContext)
		{
			using (var it = new ResponseCapture(controllerContext.RequestContext.HttpContext.Response))
			{
				result.ExecuteResult(controllerContext);
				return it.ToString();
			}
		}

		/// <summary>
		/// Returns the excel result generating html automatically
		/// </summary>
		/// <param name="controller"> The controller. </param>
		/// <param name="titulo">Titulo do pdf</param>
		/// <param name="fileName"> The file name. </param>
		/// <param name="content"> The Content. </param>
        /// <param name="orientacao">The orientation page.</param>
		/// <returns> ActionResult de excel </returns>
		public static ActionResult PdfResult(
			this Controller controller,
			string titulo,
			string fileName,
			string content,
            PdfOrientacao orientacao = PdfOrientacao.DEFAULT)
		{
			// string returnContent = GetText(fields, list);
			Stream returnContent = HtmlToPdf(content, titulo, orientacao);

			var fsr = new FileStreamResult(returnContent, "application/pdf");
			fsr.FileDownloadName = fileName;
			return fsr;
			// return new ExcelResult(fileName, returnContent);
		}

		/// <summary>
		/// Converte html em pdf
		/// </summary>
		/// <param name="html">Conteudo html</param>
		/// <param name="titulo">Titulo do documento</param>
        /// <param name="orientacao">Orientação da página</param>
		/// <returns>Stream do pdf</returns>
		public static Stream HtmlToPdf(string html, string titulo, PdfOrientacao orientacao = PdfOrientacao.DEFAULT)
		{
			TextReader textReader = new StringReader(html);

			string tempFilePath = Path.GetTempFileName();
			
            Document document = null;
            if (orientacao ==  PdfOrientacao.PAISAGEM)
            {
                document = new Document(PageSize.A4.Rotate(), 0, 0, 85, 30);
            }
            else if (orientacao == PdfOrientacao.RETRATO)
            {
                document = new Document(PageSize.A4, 0, 0, 85, 30);
            }
            else
            {
                document = new Document(PageSize.A4, 0, 0, 85, 30);
            }
            

			PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(tempFilePath, FileMode.Create));
			var pageEventHandler = new HeaderFooterEventHelper();
			writer.PageEvent = pageEventHandler;
			pageEventHandler.Title = titulo;
			
			document.Open();

			var xmlWorkerFontProvider = new XMLWorkerFontProvider();
			IList<string> fonts = new List<string>();
			fonts.Add("Arial");
			foreach (string font in fonts)
			{
				xmlWorkerFontProvider.Register(font);
			}

			var cssAppliers = new CssAppliersImpl(xmlWorkerFontProvider);
			var htmlContext = new HtmlPipelineContext(cssAppliers);

			htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
			htmlContext.SetImageProvider(new TranslogicImageProvider());

			ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
			IPipeline pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
			XMLWorker worker = new XMLWorker(pipeline, true);
			XMLParser p = new XMLParser(true, worker, Encoding.UTF8);
			p.Parse(textReader);
			p.Flush();
			document.Close();
			document.Dispose();

			StreamReader sr = new StreamReader(tempFilePath);
			MemoryStream ms = new MemoryStream();
			StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
			sr.Close();
			File.Delete(tempFilePath);
			ms.Seek(0, SeekOrigin.Begin);
			return ms;
		}

        public enum PdfOrientacao
        {
            DEFAULT, 
            RETRATO,
            PAISAGEM
        }
	}

	/// <summary>
	/// Translogic Image Provider
	/// </summary>
	public class TranslogicImageProvider : AbstractImageProvider
	{
		/// <summary>
		/// Get image root path
		/// </summary>
		/// <returns>String do path</returns>
		public override string GetImageRootPath()
		{
			string url = string.Format(
				"http://{0}",
				HttpContext.Current.Request.ServerVariables["HTTP_HOST"]);
			return url;
		}
	}

	/// <summary>
	/// EventHelper para adicionar header e footer
	/// </summary>
	public class HeaderFooterEventHelper : PdfPageEventHelper
	{
		#region Properties
		private string _title;
		
		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		#endregion

		/// <summary>
		/// The on start page.
		/// </summary>
		/// <param name="writer"> The writer. </param>
		/// <param name="document"> The document. </param>
		public override void OnStartPage(PdfWriter writer, Document document)
		{
			base.OnStartPage(writer, document);
			Rectangle pageSize = document.PageSize;

			PdfPTable headerTbl = new PdfPTable(2);
			headerTbl.TotalWidth = writer.PageSize.Width - 60;
			headerTbl.SetWidths(new[] { 100f, 435f });

			Stream stream = this.GetType().Assembly.GetManifestResourceStream("Translogic.Core.Infrastructure.Web.EmbeddedFiles.Content.Images.rumo_logo.png");
			System.Drawing.Image logoImage = new Bitmap(stream);
			iTextSharp.text.Image pdfLogoImage = iTextSharp.text.Image.GetInstance(logoImage, BaseColor.WHITE);

			PdfPCell cell = new PdfPCell(pdfLogoImage);
			cell.BorderWidthTop = 0;
			cell.BorderWidthRight = 0;
			cell.BorderWidthLeft = 0;
			cell.BorderWidthBottom = 1;
			cell.BorderColorBottom = BaseColor.GRAY;
			cell.PaddingLeft = 10;
			cell.PaddingBottom = 5;
			cell.FixedHeight = 50;
			headerTbl.AddCell(cell);

			Paragraph paragrafoTextoCol2 = new Paragraph(Title, FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.GRAY));
			PdfPCell cell2 = new PdfPCell(paragrafoTextoCol2);
			cell2.HorizontalAlignment = Element.ALIGN_CENTER;
			cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
			cell2.BorderWidthTop = 0;
			cell2.BorderWidthRight = 0;
			cell2.BorderWidthLeft = 0;
			cell2.BorderWidthBottom = 1;
			cell2.BorderColorBottom = BaseColor.GRAY;
			cell2.FixedHeight = 50;
			headerTbl.AddCell(cell2);
			headerTbl.WriteSelectedRows(0, -1, pageSize.GetLeft(30), pageSize.GetTop(30), writer.DirectContent);
		}

		/// <summary>
		/// The on end page.
		/// </summary>
		/// <param name="writer"> The writer. </param>
		/// <param name="doc"> The document. </param>
		public override void OnEndPage(PdfWriter writer, Document doc)
		{
			base.OnEndPage(writer, doc);

			PdfPTable footerTbl = new PdfPTable(2);
			footerTbl.TotalWidth = writer.PageSize.Width - 60;
			// footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;

			string textoCol1 = string.Format("Gerado pelo Translogic em {0}", DateTime.Now.ToString(CultureInfo.CurrentUICulture));
			Paragraph paragrafoTextoCol1 = new Paragraph(textoCol1, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL, BaseColor.GRAY));
			paragrafoTextoCol1.Alignment = Element.ALIGN_LEFT;
			PdfPCell cell = new PdfPCell(paragrafoTextoCol1);
			cell.BorderWidthTop = 1;
			cell.BorderWidthRight = 0;
			cell.BorderWidthLeft = 0;
			cell.BorderWidthBottom = 0;
			cell.BorderColorTop = BaseColor.GRAY;
			cell.PaddingLeft = 10;
			footerTbl.AddCell(cell);

			string textoCol2 = string.Format("Página {0}", writer.PageNumber);
			Paragraph paragrafoTextoCol2 = new Paragraph(textoCol2, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL, BaseColor.GRAY));
			PdfPCell cell2 = new PdfPCell(paragrafoTextoCol2);
			cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
			cell2.BorderWidthTop = 1;
			cell2.BorderWidthRight = 0;
			cell2.BorderWidthLeft = 0;
			cell2.BorderWidthBottom = 0;
			cell2.BorderColorTop = BaseColor.GRAY;
			cell2.PaddingRight = 10;
			footerTbl.AddCell(cell2);

			footerTbl.WriteSelectedRows(0, -1, 30, 30, writer.DirectContent);
		}
	}
}