namespace Translogic.Core.Infrastructure.Web
{
	using ALL.Core.Dominio;

	/// <summary>
	/// Detalhes de pagina��o quando for web
	/// </summary>
	public class DetalhesPaginacaoWeb : DetalhesPaginacao
	{
		#region CONSTANTES
		/// <summary>
		/// Limite padr�o
		/// </summary>
		public const int DefaultLimit = 50;
		private const char DefaultSeparator = '|';
		#endregion

		#region PROPRIEDADES
		/// <summary>
		/// Dire��o da ordena��o
		/// </summary>
		public string Dir
		{
			get
			{
				return string.Join(DefaultSeparator.ToString(), DirecoesOrdenacao.ToArray());
			}

			set
			{
				DirecoesOrdenacao.Clear();

				if (value.IndexOf(DefaultSeparator) == -1)
				{
					DirecoesOrdenacao.Add(value);
				}
				else
				{
					DirecoesOrdenacao.AddRange(value.Split(DefaultSeparator));
				}
			}
		}

		/// <summary>
		/// Limite da pagina��o
		/// </summary>
		public int? Limit
		{
			get
			{
				return Limite ?? LimitePadrao;
			}

			set 
			{
				Limite = value.HasValue ? value : LimitePadrao;
			}
		}

		/// <summary>
		/// Gets or sets Start.
		/// </summary>
		public int? Start
		{
			get
			{
				return Inicio;
			}

			set 
			{
				Inicio = value.HasValue ? value : 0;
			}
		}

		/// <summary>
		/// Campo a ser ordernado
		/// </summary>
		public string Sort
		{
			get
			{
				return string.Join(DefaultSeparator.ToString(), ColunasOrdenacao.ToArray());
			}

			set
			{
				ColunasOrdenacao.Clear();

				if (value.IndexOf(DefaultSeparator) == -1)
				{
					ColunasOrdenacao.Add(value);
				}
				else
				{
					ColunasOrdenacao.AddRange(value.Split(DefaultSeparator));
				}
			}
		}

		#endregion
	}
}