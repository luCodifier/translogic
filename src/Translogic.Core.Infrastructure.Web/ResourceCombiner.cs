namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Text.RegularExpressions;
	using System.Threading;
	using System.Web;
	using Yahoo.Yui.Compressor;

	/// <summary>
	/// Tipo do resource
	/// </summary>
	public enum ResourceType
	{
		/// <summary>
		/// Resource de javascript
		/// </summary>
		JS,

		/// <summary>
		/// Resource de CSS
		/// </summary>
		CSS
	}

	/// <summary>
	/// Combinador de arquivos Javascript e CSS
	/// </summary>
	public class ResourceCombiner : IDisposable
	{
		#region CONSTANTES

		/// <summary>
		/// String do caminho relativo da pasta de cache
		/// </summary>
		public const string CachePath = "~/Content/Cache/";

		/// <summary>
		/// Caminho arquivo de css de cache
		/// </summary>
		public const string CSSCachePath = "~/Content/Cache/{0}_{1}.css";

		/// <summary>
		/// Caminho do arquivo de javascript de cache
		/// </summary>
		public const string JSCachePath = "~/Content/Cache/{0}_{1}.js";

		#endregion

		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly string _key;

		#endregion

		#region ATRIBUTOS

		private readonly HttpContext _context;

		private readonly HashSet<string> _resources;
		private readonly Dictionary<string, Stream> _resourcesFiles;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="key">Chave do ResourceCombiner</param>
		public ResourceCombiner(string key)
		{
			_context = HttpContext.Current;
			_key = key + "_" + Thread.CurrentThread.CurrentUICulture.Name;
			_resources = new HashSet<string>();
			_resourcesFiles = new Dictionary<string, Stream>();
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Verifica se j� existe no cache
		/// </summary>
		/// <param name="key"> Chave do resourcecombiner. </param>
		/// <param name="hash"> Hash do resourcecombiner. </param>
		/// <param name="resourceType"> Tipo do resourcecombiner - <see cref="ResourceType"/> </param>
		/// <returns> Valor booleano </returns>
		public static bool ExistsInCache(string key, long hash, ResourceType resourceType)
		{
			return File.Exists(GetServerPath(key, hash, resourceType));
		}

		/// <summary>
		/// Faz corre��o do caminho relativo
		/// </summary>
		/// <param name="content">String do conte�do</param>
		/// <param name="basePath">String do caminho base</param>
		/// <returns>String do caminho relativo corrigido</returns>
		public static string FixRelativePath(string content, string basePath)
		{
			return Regex.Replace(content, @"(\.\./){1}(.*)", basePath + "$2");
		}

		/// <summary>
		/// Caminho absoluto no servidor
		/// </summary>
		/// <param name="key">Chave do resourcecombiner</param>
		/// <param name="hash">Hash do resourcecombiner</param>
		/// <param name="resourceType">Tipo do recurso - <see cref="ResourceType"/></param>
		/// <returns>String do caminho</returns>
		public static string GetServerPath(string key, long hash, ResourceType resourceType)
		{
			string cachePath = JSCachePath;

			if (resourceType == ResourceType.CSS)
			{
				cachePath = CSSCachePath;
			}

			string path = string.Format(cachePath, key, hash);

			path = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			return path;
		}

		/// <summary>
		/// Faz o dispose
		/// </summary>
		public void Dispose()
		{
			string[] javascriptResources = _resources.Where(s => s.ToLowerInvariant().EndsWith(".js")).ToArray();
			string[] cssResources = _resources.Where(s => s.ToLowerInvariant().EndsWith(".css")).ToArray();

			CheckAndWriteClientScript(javascriptResources, ResourceType.JS);
			CheckAndWriteClientScript(cssResources, ResourceType.CSS);
		}

		/// <summary>
		/// Adiciona um novo recurso
		/// </summary>
		/// <param name="path">Caminho do arquivo a ser adicionado</param>
		/// <param name="typeInAssemblyName">Tipo que o recurso est� contido no assembly</param>
		public void Add(string path, Type typeInAssemblyName = null)
		{
			Assembly assembly = typeInAssemblyName == null ? GetType().Assembly : typeInAssemblyName.Assembly;
			string resourceName = path.Replace("/", ".").Substring(1);
			string resourceAssemblyName = assembly.GetManifestResourceNames().
					Where(r => r.EndsWith(resourceName, StringComparison.InvariantCultureIgnoreCase))
					.FirstOrDefault();

			if (string.IsNullOrEmpty(resourceAssemblyName))
			{
				throw new TranslogicException("N�o foi poss�vel carregar o arquivo {0}, no assembly {1}", path, assembly.FullName);
			}

			string assemblyName = assembly.FullName.Substring(0, assembly.FullName.IndexOf(","));
			string newPath = string.Concat("{", assemblyName, "}", path.StartsWith("/") ? path : string.Concat("/", path));
			_resources.Add(newPath);
			if (_resources.Count > _resourcesFiles.Count)
			{
				Stream stream = assembly.GetManifestResourceStream(resourceAssemblyName);
				_resourcesFiles[newPath] = stream;
			}
		}

		/// <summary>
		/// Verifica e escreve o script do cliente
		/// </summary>
		/// <param name="items">Array de itens</param>
		/// <param name="resourceType">Tipo do recurso - <see cref="ResourceType"/></param>
		public void CheckAndWriteClientScript(string[] items, ResourceType resourceType)
		{
			if (!(items.Length > 0))
			{
				return;
			}

			long hash = 0;

			foreach (string resource in items)
			{
				Stream stream = _resourcesFiles[resource];
				hash += stream.Length;
				// hash += File.GetLastWriteTimeUtc(resource).Ticks * 37;
			}

			// Evitar que o cache seja compartilhado entre 2 application path
			// Exemplo: 
			// WebServer / 
			// IIS		 /Translogic.Web/
			hash += VirtualPathUtility.ToAbsolute("~").Length;
			hash = Math.Abs(hash);

			if (!ExistsInCache(_key, hash, resourceType))
			{
				string path = GetServerPath(_key, hash, resourceType);

				CombineAndSave(items, path, resourceType);

				CleanOldFiles(resourceType, path);
			}

			string formatString;
			string formatPath;
			if (resourceType == ResourceType.JS)
			{
				formatString = "<script type=\"text/javascript\" src=\"{0}?version={1}\"></script>\n\n";
				formatPath = VirtualPathUtility.ToAbsolute("~/resx/" + _key + "/js.all");
			}
			else
			{
				formatString = "<link href=\"{0}?version={1}\" rel=\"stylesheet\" type=\"text/css\" />\n\n";
				formatPath = VirtualPathUtility.ToAbsolute("~/resx/" + _key + "/css.all");
			}

			_context.Response.Write(string.Format(formatString, formatPath, hash));
		}

		/// <summary>
		/// Adiciona tradu��o
		/// </summary>
		/// <param name="path">
		/// Path das tradu��es.
		/// </param>
		public void AddLocalizable(string path)
		{
			var culture = Thread.CurrentThread.CurrentUICulture;

			path = _context.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			var info = new FileInfo(path);

			var pathLocalizable = info.FullName.Substring(0, info.FullName.Length - info.Extension.Length);

			pathLocalizable += "." + culture.Name + info.Extension;

			if (File.Exists(pathLocalizable))
			{
				_resources.Add(pathLocalizable);
			}
			else
			{
				_resources.Add(path);
			}
		}

		private void CleanOldFiles(ResourceType resourceType, string newPath)
		{
			string pattern = "*.js";

			if (resourceType == ResourceType.CSS)
			{
				pattern = "*.css";
			}

			string[] filesToDelete = Directory.GetFiles(_context.Server.MapPath(VirtualPathUtility.ToAbsolute(CachePath)), pattern);

			foreach (string toDelete in filesToDelete.Except(new[] { newPath }))
			{
				var info = new FileInfo(toDelete);

				if (info.Name.StartsWith(_key))
				{
					try
					{
						File.Delete(toDelete);
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
					}
				}
			}
		}

		private void CombineAndSave(string[] items, string path, ResourceType resourceType)
		{
			VerificarCriarDiretorios(CachePath);
			using (var writer = File.CreateText(path))
			{
				foreach (var resource in items)
				{
					string content;

					Stream resourceStream = _resourcesFiles[resource];
					using (StreamReader sr = new StreamReader(resourceStream))
					{
						content = sr.ReadToEnd();
					}

					string pathReduzido = resource.Substring(resource.IndexOf("}/") + 2);
					string assemblyName = resource.Replace("~", string.Empty).Replace("{", string.Empty).Substring(0, resource.IndexOf("}/") - 1);
					string[] arrayLevels = pathReduzido.Split('/');
					int countLevels = arrayLevels.Length - 1;
					int countLevelsImutavel = countLevels;
					while (countLevels > 0)
					{
						string relativePath = string.Empty;
						string embeddedPath = string.Empty;
						for (int i = countLevels; i > 0; i--)
						{
							relativePath += "../";
						}

						for (int i = 0; i < countLevelsImutavel - countLevels; i++)
						{
							embeddedPath = embeddedPath + "/" + arrayLevels[i];
						}

						string urlParam = string.Concat("{", assemblyName, "}", embeddedPath, "/");
						embeddedPath = string.Concat("/Get.efh?file=", urlParam);
						content = content.Replace(relativePath, embeddedPath);

						countLevels--;
					}

					/*
					TODO: alterar os paths para o handler de imagens
					ex: {Translogic.Core}/teste/das.gif/GetObject.efh
					
					var info = new FileInfo(resource);
					var basePath = info.Directory.Parent.FullName;
					var indexOfContent = resource.IndexOf(@"\Content\");
					basePath = basePath.Substring(indexOfContent);
					basePath = VirtualPathUtility.ToAbsolute("~") + basePath.Replace(@"\", "/") + "/";
					basePath = basePath.Replace(@"//", "/");
					content = FixRelativePath(content, basePath);
					*/

					try
					{
						if (resourceType == ResourceType.CSS)
						{
							content = CssCompressor.Compress(content);
						}
						else if (resourceType == ResourceType.JS)
						{
#if !DEBUG
							content = JavaScriptCompressor.Compress(content);
#endif
						}
					}
					catch (Exception ex)
					{
						Console.Write(ex.Message);
					}

					writer.Write(content);
				}
			}
		}

		private void VerificarCriarDiretorios(string path)
		{
			string pathApp = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute("~/"));
			string pathAbsoluto = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			string pathReduzido = pathAbsoluto.Replace(pathApp, string.Empty);
			string[] pastas = pathReduzido.Split(Convert.ToChar(@"\"));

			string tmpFolder = string.Empty;

			foreach (var pasta in pastas)
			{
				tmpFolder = string.Concat(tmpFolder, @"\", pasta);
				string tmpPath = string.Concat(pathApp, tmpFolder, @"\");
				if (!Directory.Exists(tmpPath))
				{
					Directory.CreateDirectory(tmpPath);
				}
			}
		}

		#endregion
	}

	/// <summary>
	/// Combinador de arquivos Javascript e CSS
	/// </summary>
	public class ResourceCombinerOld : IDisposable
	{
		#region CONSTANTES

		/// <summary>
		/// String do caminho relativo da pasta de cache
		/// </summary>
		public const string CachePath = "~/Content/Cache/";

		/// <summary>
		/// Caminho arquivo de css de cache
		/// </summary>
		public const string CSSCachePath = "~/Content/Cache/{0}_{1}.css";

		/// <summary>
		/// Caminho do arquivo de javascript de cache
		/// </summary>
		public const string JSCachePath = "~/Content/Cache/{0}_{1}.js";

		#endregion

		#region ATRIBUTOS READONLY & EST�TICOS

		private readonly string _key;

		#endregion

		#region ATRIBUTOS

		private readonly HttpContext _context;

		private readonly HashSet<string> _resources;

		#endregion

		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		/// <param name="key">Chave do ResourceCombiner</param>
		public ResourceCombinerOld(string key)
		{
			_context = HttpContext.Current;
			_key = key + "_" + Thread.CurrentThread.CurrentUICulture.Name;
			_resources = new HashSet<string>();
		}

		#endregion

		#region M�TODOS

		/// <summary>
		/// Verifica se j� existe no cache
		/// </summary>
		/// <param name="key">
		/// Chave do resourcecombiner.
		/// </param>
		/// <param name="hash">
		/// Hash do resourcecombiner.
		/// </param>
		/// <param name="resourceType">
		/// Tipo do resourcecombiner - <see cref="ResourceType"/>
		/// </param>
		/// <returns>
		/// Valor booleano
		/// </returns>
		public static bool ExistsInCache(string key, long hash, ResourceType resourceType)
		{
			return File.Exists(GetServerPath(key, hash, resourceType));
		}

		/// <summary>
		/// Faz corre��o do caminho relativo
		/// </summary>
		/// <param name="content">String do conte�do</param>
		/// <param name="basePath">String do caminho base</param>
		/// <returns>String do caminho relativo corrigido</returns>
		public static string FixRelativePath(string content, string basePath)
		{
			return Regex.Replace(content, @"(\.\./){1}(.*)", basePath + "$2");
		}

		/// <summary>
		/// Caminho absoluto no servidor
		/// </summary>
		/// <param name="key">Chave do resourcecombiner</param>
		/// <param name="hash">Hash do resourcecombiner</param>
		/// <param name="resourceType">Tipo do recurso - <see cref="ResourceType"/></param>
		/// <returns>String do caminho</returns>
		public static string GetServerPath(string key, long hash, ResourceType resourceType)
		{
			string cachePath = JSCachePath;

			if (resourceType == ResourceType.CSS)
			{
				cachePath = CSSCachePath;
			}

			string path = string.Format(cachePath, key, hash);

			path = HttpContext.Current.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			return path;
		}

		/// <summary>
		/// Faz o dispose
		/// </summary>
		public void Dispose()
		{
			string[] javascriptResources = _resources.Where(s => s.EndsWith(".js")).ToArray();
			string[] cssResources = _resources.Where(s => s.EndsWith(".css")).ToArray();

			CheckAndWriteClientScript(javascriptResources, ResourceType.JS);
			CheckAndWriteClientScript(cssResources, ResourceType.CSS);
		}

		/// <summary>
		/// Adiciona um novo recurso
		/// </summary>
		/// <param name="path">Caminho do arquivo a ser adicionado</param>
		/// <param name="assemblyName">Nome do assembly em que est� o recurso</param>
		public void Add(string path, string assemblyName = null)
		{
			if (assemblyName == null)
			{
				assemblyName = GetType().Assembly.FullName;
			}

			path = _context.Server.MapPath(VirtualPathUtility.ToAbsolute(path));
			_resources.Add(path);
		}

		/*
		/// <summary>
		/// Adiciona pasta
		/// </summary>
		/// <param name="path">Caminho da pasta a ser adicionada</param>
		public void AddFolder(string path)
		{
			path = _context.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			AddUnique(Directory.GetFiles(path));
		}

		/// <summary>
		/// Adiciona padr�o
		/// </summary>
		/// <param name="path">Caminho do padr�o</param>
		/// <param name="pattern">Padr�o a ser adicionado</param>
		public void AddPattern(string path, string pattern)
		{
			path = _context.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			AddUnique(Directory.GetFiles(path, pattern));
		}*/

		/// <summary>
		/// Verifica e escreve o script do cliente
		/// </summary>
		/// <param name="items">Array de itens</param>
		/// <param name="resourceType">Tipo do recurso - <see cref="ResourceType"/></param>
		public void CheckAndWriteClientScript(string[] items, ResourceType resourceType)
		{
			if (!(items.Length > 0))
			{
				return;
			}

			long hash = 0;

			foreach (string resource in items)
			{
				hash += File.GetLastWriteTimeUtc(resource).Ticks * 37;
			}

			// Evitar que o cache seja compartilhado entre 2 application path
			// Exemplo: 
			// WebServer / 
			// IIS		 /Translogic.Web/
			hash += VirtualPathUtility.ToAbsolute("~").Length;

			hash = Math.Abs(hash);

			if (!ExistsInCache(_key, hash, resourceType))
			{
				string path = GetServerPath(_key, hash, resourceType);

				CombineAndSave(items, path, resourceType);

				CleanOldFiles(resourceType, path);
			}

			if (resourceType == ResourceType.JS)
			{
				_context.Response.Write("<script type=\"text/javascript\" src=\"" +
				                        VirtualPathUtility.ToAbsolute("~/resx/" + _key + "/js.all") + "?version=" + hash +
				                        "\"></script>\n\n");
			}
			else
			{
				_context.Response.Write("<link href=\"" + VirtualPathUtility.ToAbsolute("~/resx/" + _key + "/css.all") + "?version=" +
				                        hash +
				                        "\" rel=\"stylesheet\" type=\"text/css\" />\n\n");
			}
		}

		/// <summary>
		/// Adiciona tradu��o
		/// </summary>
		/// <param name="path">
		/// Path das tradu��es.
		/// </param>
		public void AddLocalizable(string path)
		{
			var culture = Thread.CurrentThread.CurrentUICulture;

			path = _context.Server.MapPath(VirtualPathUtility.ToAbsolute(path));

			var info = new FileInfo(path);

			var pathLocalizable = info.FullName.Substring(0, info.FullName.Length - info.Extension.Length);

			pathLocalizable += "." + culture.Name + info.Extension;

			if (File.Exists(pathLocalizable))
			{
				_resources.Add(pathLocalizable);
			}
			else
			{
				_resources.Add(path);
			}
		}

		/*private void AddUnique(params string[] items)
		{
			foreach (string file in items)
			{
				if (!_resources.Contains(file))
				{
					_resources.Add(file);
				}
			}
		}*/

		private void CleanOldFiles(ResourceType resourceType, string newPath)
		{
			string pattern = "*.js";

			if (resourceType == ResourceType.CSS)
			{
				pattern = "*.css";
			}

			string[] filesToDelete = Directory.GetFiles(_context.Server.MapPath(VirtualPathUtility.ToAbsolute(CachePath)), pattern);

			foreach (string toDelete in filesToDelete.Except(new[] { newPath }))
			{
				var info = new FileInfo(toDelete);

				if (info.Name.StartsWith(_key))
				{
					try
					{
						File.Delete(toDelete);
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
					}
				}
			}
		}

		private void CombineAndSave(string[] items, string path, ResourceType resourceType)
		{
			using (var writer = File.CreateText(path))
			{
				foreach (var resource in items)
				{
					string content;

					using (var file = File.OpenText(resource))
					{
						content = file.ReadToEnd();
					}
					
					var info = new FileInfo(resource);

					var basePath = info.Directory.Parent.FullName;

					var indexOfContent = resource.IndexOf(@"\Content\");

					basePath = basePath.Substring(indexOfContent);

					basePath = VirtualPathUtility.ToAbsolute("~") + basePath.Replace(@"\", "/") + "/";

					basePath = basePath.Replace(@"//", "/");

					content = FixRelativePath(content, basePath);

					try
					{
						if (resourceType == ResourceType.CSS)
						{
							content = CssCompressor.Compress(content);
						}
						else if (resourceType == ResourceType.JS)
						{
#if !DEBUG
							content = JavaScriptCompressor.Compress(content);
#endif
						}
					}
					catch (Exception ex)
					{
						Console.Write(ex.Message);
					}

					writer.Write(content);
				}
			}
		}
		#endregion
	}
}