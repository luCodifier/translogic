﻿namespace Translogic.Core.Infrastructure.Web
{
	using System.Web.Routing;

	/// <summary>
	/// Helper de rotas de módulos
	/// </summary>
	public static class ModuleRouteHelper
	{
		#region MÉTODOS ESTÁTICOS

		/// <summary>
		/// An extension method to facilitate the registration of module routes.  Described by Steve Sanderson
		/// at http://blog.codeville.net/2008/11/05/app-areas-in-aspnet-mvc-take-2/
		/// </summary>
		/// <param name="routes">
		/// Coleção de rotas - <see cref="RouteCollection"/>.
		/// </param>
		/// <param name="moduleName">
		/// Nome do módulo.
		/// </param>
		/// <param name="controllersNamespace">
		/// Namespace do controller.
		/// </param>
		/// <param name="routeEntries">
		/// Entradas de rota.
		/// </param>
		public static void CreateModule(
										this RouteCollection routes, 
										string moduleName, 
										string controllersNamespace,
		                                params Route[] routeEntries)
		{
			foreach (Route route in routeEntries)
			{
				if (route.Constraints == null)
				{
					route.Constraints = new RouteValueDictionary();
				}

				if (route.Defaults == null)
				{
					route.Defaults = new RouteValueDictionary();
				}

				if (route.DataTokens == null)
				{
					route.DataTokens = new RouteValueDictionary();
				}

				route.Constraints.Add("module", moduleName);
				route.Defaults.Add("module", moduleName);
				route.Defaults.Add("controller", "home");
				route.Defaults.Add("action", "index");
				route.Defaults.Add("id", string.Empty);
				route.DataTokens.Add("namespaces", new[] { controllersNamespace });

				// To support "new Route()" in addition to "routes.MapRoute()"
				if (!routes.Contains(route)) 
				{
					routes.Add(route);
				}
			}
		}

		#endregion
	}
}