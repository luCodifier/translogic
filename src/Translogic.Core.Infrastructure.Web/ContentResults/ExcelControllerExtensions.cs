﻿namespace Translogic.Core.Infrastructure.Web.ContentResults
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.UI;
    using ExcelLibrary.SpreadSheet;
    using ICSharpCode.SharpZipLib.Core;

    /// <summary>
    /// Extenção de controller
    /// </summary>
    public static class ExcelControllerExtensions
    {
        /// <summary>
        /// Action de excel passando controller, linhas, arquivo, e headers
        /// </summary>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="htmlContent">
        /// Conteudo html
        /// </param>
        /// <returns>
        /// ActionResult de Excel
        /// </returns>
        public static ActionResult Excel(
            this Controller controller,
            string fileName,
            string htmlContent
            )
        {
            return new ExcelResult(fileName, htmlContent);
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="list">
        /// Lista de objetos
        /// </param>
        /// <typeparam name="T">Tipo do objeto da lista </typeparam>
        /// <returns> Stream de excel </returns>
        public static Stream ExcelStream<T>(
            Dictionary<string, Func<T, string>> fields,
            IList<T> list)
        {
            string fileName = null;
            return GetFromComponent(fields, list, ref fileName);
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="list">
        /// Lista de objetos
        /// </param>
        /// <typeparam name="T">Tipo do objeto da lista </typeparam>
        /// <returns> Stream de excel </returns>
        public static Stream ExcelStream<T>(
            this Controller controller,
            Dictionary<string, Func<T, string>> fields,
            IList<T> list)
        {
            string fileName = null;
            return GetFromComponent(fields, list, ref fileName);
        }

        /// <summary>
        /// Returns the excel result generating html automatically
        /// </summary>
        /// <param name="controller">
        /// The controller.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="fields">
        /// The fields.
        /// </param>
        /// <param name="list">
        /// Lista de objetos
        /// </param>
        /// <typeparam name="T">Tipo do objeto da lista </typeparam>
        /// <returns> ActionResult de excel </returns>
        public static ActionResult Excel<T>(this Controller controller, string fileName, Dictionary<string, Func<T, string>> fields, IList<T> list)
        {
            // string returnContent = GetText(fields, list);
            Stream returnContent = GetFromComponent(fields, list, ref fileName);
            fileName = Path.GetFileName(fileName);

            var fsr = new FileStreamResult(returnContent, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //var fsr = new FileStreamResult(returnContent, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            fsr.FileDownloadName = fileName;
            return fsr;
            // return new ExcelResult(fileName, returnContent);
        }

        public static string GetHtml<T>(Dictionary<string, Func<T, string>> fields, IList<T> list)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);

            tw.RenderBeginTag(HtmlTextWriterTag.Table);

            // Cria a linha do titulo
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            foreach (string key in fields.Keys)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Td);
                tw.Write(HttpUtility.HtmlEncode(key));
                tw.RenderEndTag();
            }

            tw.RenderEndTag();
            for (int i = 0; i < list.Count; i++)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (var key in fields.Keys)
                {
                    tw.RenderBeginTag(HtmlTextWriterTag.Td);

                    Func<T, string> valor = fields[key];

                    tw.Write(HttpUtility.HtmlEncode(valor.Invoke(list[i])));
                    tw.RenderEndTag();
                }

                tw.RenderEndTag();
            }

            tw.RenderEndTag();

            return sw.ToString();
        }

        private static string GetText<T>(Dictionary<string, Func<T, string>> fields, IList<T> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in fields.Keys)
            {
                sb.Append(key);
                sb.Append("\t");
            }

            sb.Append(Environment.NewLine);

            for (int i = 0; i < list.Count; i++)
            {
                foreach (var key in fields.Keys)
                {
                    Func<T, string> valor = fields[key];
                    string valorString = valor.Invoke(list[i]);
                    valorString = valorString.Replace("\n", " ");
                    valorString = valorString.Replace("\r", " ");
                    sb.Append(valorString);
                    sb.Append("\t");
                }

                sb.Append(Environment.NewLine);
            }

            return sb.ToString();
        }

        private static Stream GetFromComponent<T>(Dictionary<string, Func<T, string>> fields, IList<T> list, ref string fileName, string reportTitle = null)
        {
            fileName = fileName ?? "Relatório";
            fileName = ExcelExport.Export(fileName ?? reportTitle, fields, list);

            /*
			string tempFilePath = Path.GetTempFileName();
			Workbook workbook = new Workbook();
			Worksheet worksheet = new Worksheet("Dados");

			int contador = 0;
			foreach (string key in fields.Keys)
			{
				worksheet.Cells[0, contador] = new Cell(key);
				contador++;
			}

			int linhas = 0;
			for (int i = 0; i < list.Count; i++)
			{
				contador = 0;
				foreach (var key in fields.Keys)
				{
					Func<T, string> valor = fields[key];
					string valorString = valor.Invoke(list[i]);
					valorString = valorString.Replace("\n", " ");
					valorString = valorString.Replace("\r", " ");
					worksheet.Cells[i + 1, contador] = new Cell(valorString);
					contador++;
				}

				linhas++;
			}

			linhas++;
			for (int i = linhas; i < linhas + 100; i++)
			{
				for (int z = 0; z < 8; z++)
				{
					worksheet.Cells[i + 1, z] = new Cell(" ");
				}
			}

			workbook.Worksheets.Add(worksheet);
			workbook.Save(tempFilePath);
			*/

            StreamReader sr = new StreamReader(fileName);
            MemoryStream ms = new MemoryStream();
            StreamUtils.Copy(sr.BaseStream, ms, new byte[4096]);
            sr.Close();
            File.Delete(fileName);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }
    }
}