namespace Translogic.Core.Infrastructure.Web.ContentResults
{
	using System;
	using System.Text;
	using System.Web;
	using System.Web.Mvc;
	using Newtonsoft.Json;

	/// <summary>
	/// Resultado de a��o do controller em formato JSON
	/// </summary>
	public class JsonNetResult : ActionResult
	{
		#region CONSTRUTORES

		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		public JsonNetResult()
		{
			SerializerSettings = new JsonSerializerSettings();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Encoding do conte�do
		/// </summary>
		public Encoding ContentEncoding { get; set; }

		/// <summary>
		/// Tipo do conte�do(ContentType)
		/// </summary>
		public string ContentType { get; set; }

		/// <summary>
		/// Dados a serem formatados
		/// </summary>
		public object Data { get; set; }

		/// <summary>
		/// Formata��o do resultado.
		/// </summary>
		public Formatting Formatting { get; set; }

		/// <summary>
		/// Configura��es do serializador
		/// </summary>
		public JsonSerializerSettings SerializerSettings { get; set; }

		#endregion

		#region M�TODOS
		/// <summary>
		/// Implementa��o do evento ao executar o resultado
		/// </summary>
		/// <param name="context">Contexto do controller - <see cref="ControllerContext"/></param>
		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			HttpResponseBase response = context.HttpContext.Response;

			response.ContentType = !string.IsNullOrEmpty(ContentType)
			                       	? ContentType
			                       	: "application/json";

			if (ContentEncoding != null)
			{
				response.ContentEncoding = ContentEncoding;
			}

			if (Data != null)
			{
				JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting };

				JsonSerializer serializer = JsonSerializer.Create(SerializerSettings);
				serializer.Serialize(writer, Data);

				writer.Flush();
			}
		}

		#endregion
	}
}