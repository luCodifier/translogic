﻿namespace Translogic.Core.Infrastructure.Web.ContentResults
{
	using System;
	using System.Collections.Generic;
	using System.Drawing;
	using System.IO;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	/// <summary>
	/// Action result para retornar excel
	/// </summary>
	public class ExcelResult : ActionResult
	{
		private readonly string _fileName;
		private readonly string _htmlContent;

		/// <summary>
		/// Initializes a new instance of the <see cref="ExcelResult"/> class.
		/// </summary>
		/// <param name="fileName">
		/// The file name.
		/// </param>
		/// <param name="htmlContent">
		/// Tabela html a ser renderizada
		/// </param>
		public ExcelResult(string fileName, string htmlContent)
		{
			_htmlContent = htmlContent;
			_fileName = fileName;
		}

		/// <summary>
		/// Nome do arquivo.
		/// </summary>
		public string FileName
		{
			get { return _fileName; }
		}

		/// <summary>
		/// Registros a serem gerados excel
		/// </summary>
		public string HtmlContent
		{
			get { return _htmlContent; }
		}

		/// <summary>
		/// Método de execução da action
		/// </summary>
		/// <param name="context">
		/// The context.
		/// </param>
		public override void ExecuteResult(ControllerContext context)
		{
			WriteFile(_fileName, "application/ms-excel", _htmlContent);
		}

		private static void WriteFile(string fileName, string contentType, string content)
		{
			HttpContext context = HttpContext.Current;
			context.Response.Clear();
			context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
			context.Response.Charset = string.Empty;
			context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			context.Response.ContentType = contentType;
			context.Response.Write(content);
			context.Response.End();
		}
	}
}