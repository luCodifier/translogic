namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using System.Web.Mvc;
	using Castle.Core.Logging;
	using Filters;
	using NHibernate.Validator.Engine;
	using Settings;
	using Validation;

	/// <summary>
	/// Controller base para os m�dulos, para acesso p�blico.
	/// </summary>
	[ModuleInitFilter(Order = 0)]
	[JsonRequestBehavior]
	[HandleError]
	public class BaseModuleController : Controller
	{
		#region ATRIBUTOS

		private ILogger _logger = NullLogger.Instance;

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Serializador JSON
		/// </summary>
		public IJsonSerializer JsonSerializer { get; set; }

		/// <summary>
		/// Seta/Retorna o Logger
		/// </summary>
		public ILogger Logger
		{
			get { return _logger; }
			set { _logger = value; }
		}

		/// <summary>
		/// Configura��es do server
		/// </summary>
		public ServerSettings Settings { get; set; }

		/// <summary>
		/// Nome do m�dulo
		/// </summary>
		public string ModuleName { get; set; }

		/// <summary>
		/// Caminho do m�dulo
		/// </summary>
		public string ModuleSourcePath { get; set; }

		/// <summary>
		/// Engine de valida��o
		/// </summary>
		public IValidatorEngine ValidatorEngine { get; set; }

		#endregion

		#region M�TODOS

		/// <summary>
		/// Valida se a entidade est� vazia
		/// </summary>
		/// <param name="entity">Entidade a ser validada</param>
		public void Validate(object entity)
		{
			Validate(entity, string.Empty);
		}

		/// <summary>
		/// Valida a entindade atrav�s do ValidatorEngine
		/// </summary>
		/// <param name="entity">Entidade a ser validada</param>
		/// <param name="prefix">Prefixo do erro.</param>
		/// <returns>Valor booleano</returns>
		public bool Validate(object entity, string prefix)
		{
			if (entity == null)
			{
				throw new ArgumentNullException("entity");
			}

			if (!string.IsNullOrEmpty(prefix))
			{
				prefix += ".";
			}

			InvalidValue[] errors = ValidatorEngine.Validate(entity);

			foreach (InvalidValue error in errors)
			{
				ModelState.AddModelError(prefix + error.PropertyName, error.Message);
			}

			return ModelState.IsValid;
		}

		#endregion
	}
}