namespace Translogic.Core.Infrastructure.Web
{
	using System.Globalization;
	using System.Web.Mvc;

	/// <summary>
	/// Custom view engine
	/// </summary>
	public class CustomViewEngine : WebFormViewEngine
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomViewEngine"/> class.
		/// </summary>
		public CustomViewEngine()
		{
			/*MasterLocationFormats = new[] 
			{ 
				"~/{0}.master", 
                "~/Views/{1}/{0}.master", 
                "~/Views/Shared/{0}.master" 
            };
			
			ViewLocationFormats = new[] 
			{ 
				"~/{0}.aspx",
				"~/{0}.ascx",
				"~/Views/{1}/{0}.aspx",
                "~/Views/{1}/{0}.ascx",
				"~/Views/Shared/{0}.aspx",
                "~/Views/Shared/{0}.ascx"
            };
			PartialViewLocationFormats = ViewLocationFormats;*/
		}

		/// <summary>
		/// Finds the view.
		/// </summary>
		/// <param name="controllerContext">The controller context.</param><param name="viewName">Name of the view.</param><param name="masterName">Name of the master.</param><param name="useCache">if set to <c>true</c> [use cache].</param>
		/// <returns>
		/// The page view.
		/// </returns>
		public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
		{
			// var request = controllerContext.RequestContext;
			
			ViewEngineResult result = base.FindView(controllerContext, viewName, masterName, useCache);
			
			return result;
		}

		/// <summary>
		/// Finds the partial view.
		/// </summary>
		/// <param name="controllerContext">The controller context.</param><param name="partialViewName">Partial name of the view.</param><param name="useCache">if set to <c>true</c> [use cache].</param>
		/// <returns>
		/// The partial view.
		/// </returns>
		public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
		{
			ViewEngineResult result = base.FindPartialView(controllerContext, partialViewName, useCache);

			return result;
		}
	}
}