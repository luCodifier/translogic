﻿namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Text;
	using System.Web;

	/// <summary>
	/// Response capture
	/// </summary>
	public class ResponseCapture : IDisposable
	{
		private readonly HttpResponseBase response;
		private readonly TextWriter originalWriter;
		private StringWriter localWriter;

		/// <summary>
		/// Initializes a new instance of the <see cref="ResponseCapture"/> class.
		/// </summary>
		/// <param name="response"> The response. </param>
		public ResponseCapture(HttpResponseBase response)
		{
			this.response = response;
			originalWriter = response.Output;
			localWriter = new StringWriter();
			response.Output = localWriter;
		}

		/// <summary>
		/// The to string.
		/// </summary>
		/// <returns>
		/// The <see cref="string"/>. </returns>
		public override string ToString()
		{
			localWriter.Flush();
			return localWriter.ToString();
		}

		/// <summary>
		/// The dispose.
		/// </summary>
		public void Dispose()
		{
			if (localWriter != null)
			{
				localWriter.Dispose();
				localWriter = null;
				response.Output = originalWriter;
			}
		}
	}
}
