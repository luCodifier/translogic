namespace Translogic.Core.Infrastructure.Web
{
	using System.Web;
	using System.Web.Mvc;

	/// <summary>
	/// Classe de extens�o para retornar paths de URL corretamente
	/// </summary>
	public static class UrlHelperExtension
	{
		#region M�TODOS EST�TICOS
		/// <summary>
		/// Retorna o path absoluto de uma string de URL
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <param name="url">String da URL</param>
		/// <returns>Path absoluto do arquivo</returns>
		public static string FullRoot(this UrlHelper helper, string url)
		{
			var basePath = VirtualPathUtility.ToAbsolute("~/");

			if (url.StartsWith(basePath))
			{
				url = url.Remove(0, basePath.Length);
			}

			return FullRoot(helper) + url;
		}

		/// <summary>
		/// Retorna a string do caminho absoluto da pasta "Content"
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <returns>String do caminho absoluto da pasta</returns>
		public static string ContentBase(this UrlHelper helper)
		{
			return helper.Content("~/Content");
		}

		/// <summary>
		/// Retorna a string do caminho absoluto referente da aplica��o do arquivo CSS
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <param name="fileName">Nome do arquivo CSS</param>
		/// <returns>String do caminho absoluto</returns>
		public static string CSS(this UrlHelper helper, string fileName)
		{
			return helper.Content(string.Format("~/Content/CSS/{0}", fileName));
		}

		/// <summary>
		/// Retorna o path absoluto da URL atual do contexto
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <returns>Path absoluto do arquivo</returns>
		public static string FullRoot(this UrlHelper helper)
		{
			HttpRequest request = HttpContext.Current.Request;

			return VirtualPathUtility.AppendTrailingSlash(request.Url.AbsoluteUri.Replace(request.Url.AbsolutePath, string.Empty) + request.ApplicationPath);
		}

		/// <summary>
		/// Retorna a string do caminho absoluto da imagem que est� na pasta "Content/Images"
		/// </summary>
		/// <param name="helper"> Objeto de extens�o </param>
		/// <param name="fileName"> Nome do arquivo de imagem. </param>
		/// <returns>String do caminho absoluto da imagem</returns>
		public static string Images(this UrlHelper helper, string fileName)
		{
			return helper.Content(@"/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/" + fileName);
		}

		/// <summary>
		/// Retorna a string do caminho absoluto da imagem que est� na pasta "Content/Images"
		/// </summary>
		/// <param name="helper"> Objeto de extens�o </param>
		/// <param name="folders"> Pasta ap�s as imagens </param>
		/// <param name="fileName"> Nome do arquivo de imagem. </param>
		/// <returns>String do caminho absoluto da imagem</returns>
		public static string Images(this UrlHelper helper, string folders, string fileName)
		{
			if (folders.StartsWith("/"))
			{
				folders = folders.Substring(1, folders.Length);
			}

			if (folders.EndsWith("/"))
			{
				folders = folders.Substring(0, folders.Length - 1);
			}

			return helper.Content(string.Format("~/Content/Images/{0}/{1}", folders, fileName));
		}

		/// <summary>
		/// Retorna a string do caminho relativo referente da aplica��o do arquivo Javascript
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <param name="fileName">Nome do arquivo JS</param>
		/// <returns>String do caminho absoluto do arquivo JS</returns>
		public static string Javascript(this UrlHelper helper, string fileName)
		{
			return helper.Content(string.Format("~/Content/Javascript/{0}", fileName));
		}

		/// <summary>
		/// Retorna o caminho do root
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <returns>String do caminho do root</returns>
		public static string Root(this UrlHelper helper)
		{
			return helper.Content("~/");
		}

		/// <summary>
		/// Retorna o caminho absoluto da url
		/// </summary>
		/// <param name="helper">Objeto de extens�o</param>
		/// <param name="url">String da URL</param>
		/// <returns>String do caminho absoluto</returns>
		public static string ToAbsolute(this UrlHelper helper, string url)
		{
			return helper.Content(url);
		}

        public static string LegadoPage(this UrlHelper helper)
        {
#if DEBUG
            return "";
#else
            return helper.ToAbsolute("~/legado.html");
#endif
        }


		#endregion
	}
}