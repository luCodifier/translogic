namespace Translogic.Core.Infrastructure.Web
{
	/// <summary>
	/// Interface de serializa��o de objetos no formato JSON
	/// </summary>
	public interface IJsonSerializer
	{
		#region M�TODOS
		/// <summary>
		/// Decodifica uma string JSON em objeto
		/// </summary>
		/// <typeparam name="T">Tipo do objeto que ser� feito o cast</typeparam>
		/// <param name="json">String JSON</param>
		/// <returns>Objeto convertido</returns>
		T Decode<T>(string json);

		/// <summary>
		/// Codifica um objeto em string JSON
		/// </summary>
		/// <param name="data">Objeto a ser transformado</param>
		/// <returns>String JSON</returns>
		string Encode(object data);

		#endregion
	}
}