namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using System.Web;
	using Castle.Windsor;

	/// <summary>
	/// GlobalApplication abstrata
	/// </summary>
	public abstract class AbstractGlobalApplication : HttpApplication, IContainerAccessor
	{
		/// <summary>
		/// Retorna o Container.
		/// </summary>
		public IWindsorContainer Container
		{
			get { return TranslogicStarter.Container; }
		}

		#region M�TODOS

		/// <summary>
		/// Instalar componentes web
		/// </summary>
		protected abstract void InstallWebComponents();

		/// <summary>
		/// Evento acionado ao terminar a aplica��o
		/// </summary>
		/// <param name="sender"> Disparador do evento. </param>
		/// <param name="e"> Argumentos do evento. </param>
		protected void Application_End(object sender, EventArgs e)
		{
		}

		/// <summary>
		/// Evento acionado ao iniciar a aplica��o
		/// </summary>
		/// <param name="sender"> Disparador do evento. </param>
		/// <param name="e"> Argumentos do evento. </param>
		protected void Application_Start(object sender, EventArgs e)
		{
			TranslogicStarter.Initialize();
			InstallWebComponents();
		}

		#endregion
	}
}