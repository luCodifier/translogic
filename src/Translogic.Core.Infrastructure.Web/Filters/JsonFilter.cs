﻿using System.Threading;
using Newtonsoft.Json.Converters;

namespace Translogic.Core.Infrastructure.Web.Filters
{
    using System;
    using System.IO;
    using System.Web.Mvc;
    using Newtonsoft.Json;

    /// <summary>
    /// Filtro para receber json
    /// </summary>
    public class JsonFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Gets or sets Param.
        /// </summary>
        public string Param { get; set; }

        /// <summary>
        /// Gets or sets JsonDataType.
        /// </summary>
        public Type JsonDataType { get; set; }

        /// <summary>
        /// Gets or sets DateTimeFormat.
        /// </summary>
        public string DateTimeFormat { get; set; }

        /// <summary>
        /// evento de execução da action
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.ContentType.Contains("application/json"))
            {
                string inputContent;
                using (var sr = new StreamReader(filterContext.HttpContext.Request.InputStream))
                {
                    sr.BaseStream.Seek(0, SeekOrigin.Begin);
                    inputContent = sr.ReadToEnd();
                }

                var result = GetObject(inputContent);
                //var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
                filterContext.ActionParameters[Param] = result;
            }
        }

        private object GetObject(string inputContent)
        {
            return string.IsNullOrEmpty(DateTimeFormat) ? JsonConvert.DeserializeObject(inputContent, JsonDataType) : JsonConvert.DeserializeObject(inputContent, JsonDataType, new IsoDateTimeConverter { DateTimeFormat = DateTimeFormat });
        }
    }
}