namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System;
	using System.Web;
	using System.Web.Mvc;

	/// <summary>
	/// Atributo de filtro de cache da a��o de um controller
	/// </summary>
	public class CacheFilterAttribute : ActionFilterAttribute
	{
		#region CONSTRUTORES
		/// <summary>
		/// Inicializa uma nova inst�ncia da classe
		/// </summary>
		public CacheFilterAttribute()
		{
			Duration = TimeSpan.FromSeconds(10);
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Gets or sets the cache duration in seconds. The default is 10 seconds.
		/// </summary>
		/// <value>The cache duration in seconds.</value>
		public TimeSpan Duration { get; set; }

		/// <summary>
		/// Tempo de dura��o em dias
		/// </summary>
		public int DurationInDays
		{
			get { return Duration.Days; }
			set { Duration = TimeSpan.FromDays(value); }
		}

		#endregion

		#region M�TODOS
		/// <summary>
		/// Adiciona ao cache ap�s a a��o ser executada
		/// </summary>
		/// <param name="filterContext">Contexto de filtro</param>
		public override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			if (Duration <= TimeSpan.Zero)
			{
				return;
			}

			HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
			TimeSpan cacheDuration = Duration;

			cache.SetCacheability(HttpCacheability.Public);
			cache.SetExpires(DateTime.Now.Add(cacheDuration));
			cache.SetMaxAge(cacheDuration);
			cache.AppendCacheExtension("must-revalidate, proxy-revalidate");
		}

		#endregion
	}
}