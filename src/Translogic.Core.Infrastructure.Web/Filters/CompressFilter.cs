namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System.IO.Compression;
	using System.Web;
	using System.Web.Mvc;

	/// <summary>
	/// Filtro de compress�o de uma a��o do controller
	/// </summary>
	public class CompressFilter : ActionFilterAttribute
	{
		#region M�TODOS
		/// <summary>
		/// Faz a compress�o ap�s a a��o ser executada
		/// </summary>
		/// <param name="filterContext">Contexto de filtro</param>
		public override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			if (filterContext.Exception != null)
			{
				return;
			}

			HttpRequestBase request = filterContext.HttpContext.Request;

			string acceptEncoding = request.Headers["Accept-Encoding"];

			if (string.IsNullOrEmpty(acceptEncoding))
			{
				return;
			}

			acceptEncoding = acceptEncoding.ToUpperInvariant();

			HttpResponseBase response = filterContext.HttpContext.Response;

			if (acceptEncoding.Contains("GZIP"))
			{
				response.AppendHeader("Content-encoding", "gzip");
				response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
			}
			else if (acceptEncoding.Contains("DEFLATE"))
			{
				response.AppendHeader("Content-encoding", "deflate");
				response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
			}
		}

		#endregion
	}
}