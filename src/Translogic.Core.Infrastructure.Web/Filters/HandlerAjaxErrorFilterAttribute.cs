namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System;
	using System.Net;
	using System.Web;
	using System.Web.Mvc;

	/// <summary>
	/// Tratamento de exce��es qndo � uma requisi��o ajax
	/// </summary>
	public class HandlerAjaxErrorFilterAttribute : HandleErrorAttribute
	{
		#region M�TODOS

		/// <summary>
		/// Executado ap�s o resultado ser executado
		/// </summary>
		/// <param name="filterContext">Contexto do filtro</param>
		public override void OnException(ExceptionContext filterContext)
		{
			HttpContextBase context = filterContext.RequestContext.HttpContext;

			Exception exception = filterContext.Exception;

			if (exception != null && context.Request.IsAjaxRequest())
			{
				context.Response.Clear();

				filterContext.Result = new JsonResult
				                       	{
				                       		Data =
				                       			new
				                       				{
														type = "exception",
				                       					success = false,
				                       					statusCode = (int) HttpStatusCode.InternalServerError,
				                       					message = exception.Message
				                       				}
				                       	};

				filterContext.RequestContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

				filterContext.ExceptionHandled = true;
			}
		}

		#endregion
	}
}