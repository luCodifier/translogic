namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System.Web.Mvc;

	/// <summary>
	/// Atributo JsonRequestBehaviorAttribute
	/// </summary>
	public class JsonRequestBehaviorAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JsonRequestBehaviorAttribute"/> class.
		/// </summary>
		public JsonRequestBehaviorAttribute()
		{
			Behavior = JsonRequestBehavior.AllowGet;
		}

		private JsonRequestBehavior Behavior { get; set; }

		/// <summary>
		/// Called before the action result executes.
		/// </summary>
		/// <param name="filterContext">The filter context.</param>
		public override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			var result = filterContext.Result as JsonResult;

			if (result != null)
			{
				result.JsonRequestBehavior = Behavior;
			}
		}
	}
}