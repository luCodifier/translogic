namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System;
	using System.Configuration;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Web.Mvc;

	/// <summary>
	/// Atributo de filtro de inicializa��o de um m�dulo
	/// </summary>
	public class ModuleInitFilterAttribute : FilterAttribute, IActionFilter
	{
		#region M�TODOS

		/// <summary>
		/// Aplica o filtro durante a execu��o da a��o
		/// </summary>
		/// <param name="filterContext">Contexto de filtro</param>
		public void OnActionExecuting(ActionExecutingContext filterContext)
		{
			var moduleController = filterContext.Controller as BaseModuleController;

			if (moduleController == null)
			{
				return;
			}

			SetModuleContext(filterContext, moduleController);
		}

		/// <summary>
		/// Aplica o fultro ap�s a execu��o da a��o
		/// </summary>
		/// <param name="filterContext">Contexto de filtro</param>
		public void OnActionExecuted(ActionExecutedContext filterContext)
		{
		}

		private void SetModuleContext(ActionExecutingContext filterContext, BaseModuleController moduleController)
		{
			moduleController.ModuleName = filterContext.RouteData.Values["module"].ToString();

			if (IsAssemblyDebugBuild(moduleController.ControllerContext.Controller.GetType().Assembly))
			{
				SetModuleSourcePath(moduleController, filterContext);	
			}

			filterContext.HttpContext.Items["ControllerContext"] = moduleController.ControllerContext;
		}

		/// <summary>
		/// Verifica se o assembly foi compilado em Debug
		/// </summary>
		/// <param name="assembly"> Assembly a ser verificado. </param>
		/// <returns> Valor booleano </returns>
		private bool IsAssemblyDebugBuild(Assembly assembly)
		{
			return assembly.GetCustomAttributes(false).Any(x =>
			(x as DebuggableAttribute) != null ?
			(x as DebuggableAttribute).IsJITTrackingEnabled : false);
		}

		private void SetModuleSourcePath(BaseModuleController moduleController, ActionExecutingContext filterContext)
		{
			string physicalPath = filterContext.HttpContext.Server.MapPath("~");
			/*string nomeProjetoWeb = ConfigurationManager.AppSettings["NomeProjetoWeb"];
			string pastaPrefixoModulos = ConfigurationManager.AppSettings["PastaPrefixoModulos"];

			// if (string.IsNullOrEmpty(nomeProjetoWeb))
			// {
				nomeProjetoWeb = "Translogic.Modules.CopaDiesel";
			// }

			// if (string.IsNullOrEmpty(pastaPrefixoModulos))
			// {
				pastaPrefixoModulos = @"Modules\Translogic.Modules.";
			// }
			*/

			if (physicalPath.IndexOf(moduleController.ModuleName) > 0)
			{
				moduleController.ModuleSourcePath = physicalPath;
			}
		}

		#endregion
	}
}