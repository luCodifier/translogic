namespace Translogic.Core.Infrastructure.Web.Filters
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using System.Net.Mail;
	using System.Text;
	using System.Web;
	using System.Web.Mvc;
	using System.Web.Security;
	using Microsoft.Practices.ServiceLocation;
	using Services;

	/// <summary>
	/// Atributo de filtro de autentica��o
	/// </summary>
	public class AuthenticationFilterAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Evento de autoriza��o
		/// </summary>
		/// <param name="filterContext">Contexto da autoriza��o</param>
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			/*
			string controllerName = filterContext.Controller.GetType().Name.ToUpper();

			string[] controllersBloqueados = 
											{
												 "CLASSIFICACAODIESELMAQPORCHAVECONTROLLER",
			                                 	"CLASSIFICACAODIESELMAQPORTIMECONTROLLER",
			                                 	"CLASSIFICACAODIESELMAQPORUPCONTROLLER",
			                                 	"CLASSIFICACAODIESELTIMESCONTROLLER",
			                                 	"CLASSIFICACAODIESELUPSCONTROLLER",
			                                 	"CLASSIFICACAORALLYMAQPORCHAVECONTROLLER",
			                                 	"CLASSIFICACAORALLYMAQPORTIMECONTROLLER",
			                                 	"CLASSIFICACAORALLYMAQPORUPCONTROLLER",
			                                 	"CLASSIFICACAORALLYTIMESCONTROLLER",
			                                 	"CLASSIFICACAORALLYUPSCONTROLLER",
			                                 	"CONSULTACONSUMOMANOBRASCONTROLLER",
			                                 	"CONSULTADESEMPENHOSECOESCONTROLLER",
			                                 	"CONSULTAMETADIESELCONTROLLER",
			                                 	"CONSULTAMETARALLYCONTROLLER",
			                                 	"CONSULTAPENALIDADEDIESELCONTROLLER",
			                                 	"CONSULTAPENALIDADERALLYCONTROLLER",
			                                 	"CONSULTATRECHODIESELCONTROLLER",
			                                 	"CONSULTATRECHORALLYCONTROLLER",
			                                 	"CONSULTAVIAGEMDIESELMAQUINISTACONTROLLER",
			                                 	"CONSULTAVIAGEMRALLYCONTROLLER",
			                                 	"EXPORTAVIAGEMDIESELCONTROLLER",
			                                 	"EXPORTAVIAGEMRALLYCONTROLLER"
			                                 };
			IEnumerable<string> matchedResult = controllersBloqueados.Where(
				c => c.Equals(controllerName)
				);
			if (matchedResult != null)
			{
				if (matchedResult.Count() > 0)
				{
					filterContext.Result = new ViewResult
					                       	{
					                       		ViewName = "BloqueadoManutencao",
					                       		MasterName = null
					                       	};
					return;
				}
			}
			*/

			if (filterContext.HttpContext.User.Identity.IsAuthenticated)
			{
				return;
			}

			if (filterContext.HttpContext.Request.IsAjaxRequest())
			{
				IControleAcessoService controleAcessoService = ServiceLocator.Current.GetInstance<IControleAcessoService>();
				HttpContextBase context = filterContext.HttpContext;

				int tempoEmSegundos;
				int.TryParse(context.Request.Headers["TempoRestanteEmSegundos"], out tempoEmSegundos);

				controleAcessoService.InserirLogSessaoExpirada(
					context.Request.Headers["UsuarioLogado"],
					context.Request.Headers["IpClient"],
					context.Request.ServerVariables["LOCAL_ADDR"],
					tempoEmSegundos);

				if (tempoEmSegundos > 0)
				{
					SendErrorMail(context);
				}

				filterContext.Result = new JsonResult { Data = new { success = false, statusCode = (int) HttpStatusCode.Unauthorized } };
				filterContext.RequestContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;
				return;
			}

			filterContext.Result = new HttpUnauthorizedResult();
		}

		private void SendErrorMail(HttpContextBase context)
		{
			if (!string.IsNullOrEmpty(context.Request.Headers["IpClient"]) && !string.IsNullOrEmpty(context.Request.Headers["TempoRestanteEmSegundos"]))
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("Usu�rio:");
				sb.Append(context.Request.Headers["UsuarioLogado"]);
				sb.Append("<br /><br />");
				sb.Append("IP Client:");
				sb.Append(context.Request.Headers["IpClient"]);
				sb.Append("<br /><br />");
				sb.Append("IP Server:");
				sb.Append(context.Request.ServerVariables["LOCAL_ADDR"]);
				sb.Append("<br /><br />");
				sb.Append("Segundos Restantes:");
				sb.Append(context.Request.Headers["TempoRestanteEmSegundos"]);

				MailMessage mailMessage = new MailMessage { Body = sb.ToString() };
				mailMessage.To.Add("marcos.yano@all-logistica.com");
				mailMessage.CC.Add("diegorn@all-logistica.com");
				// mailMessage.CC.Add("marcelo.vieira@all-logistica.com");
				// mailMessage.CC.Add("cesar.schork@addvalue.com.br");
				mailMessage.Subject = "[Sessao Expirada do Translogic]";
                mailMessage.From = new MailAddress("noreply@all-logistica.com");
				mailMessage.IsBodyHtml = true;
				mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
				mailMessage.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

				SmtpClient client = new SmtpClient();
				client.Send(mailMessage);
			}
		}
	}
}