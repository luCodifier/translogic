﻿namespace Translogic.Core.Infrastructure.Web.Filters
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Web.Mvc;

    /// <summary>
    /// Filtro para receber datas para as actions
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class DateTimeFormatFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Gets or sets Param.
        /// </summary>
        public string Param { get; set; }

        /// <summary>
        /// Gets or sets DateFormat.
        /// </summary>
        public string DateFormat { get; set; }

        /// <summary>
        /// evento de execução da action
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.ContentType.Contains("application/json"))
            {
                var value = filterContext.HttpContext.Request[Param];
                DateTime valueCnv;

                if (DateTime.TryParseExact(value, DateFormat, Thread.CurrentThread.CurrentUICulture, DateTimeStyles.None, out valueCnv))
                {
                    filterContext.ActionParameters[Param] = valueCnv;
                }
                else
                {
                    filterContext.ActionParameters[Param] = null;
                }
            }
        }
    }
}
