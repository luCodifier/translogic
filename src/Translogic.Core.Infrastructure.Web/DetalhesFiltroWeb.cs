namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using ALL.Core.Dominio;

	/// <summary>
	/// Detalhes de pagina��o quando for web
	/// </summary>
	public class DetalhesFiltroWeb : DetalhesFiltro<string>
	{
		/// <summary>
		/// Tipo do Campo
		/// </summary>
		public Type TipoCampo { get; set; }
		
		/// <summary>
		/// dasdsad asd asda
		/// </summary>
		/// <param name="campo">
		/// The campo.
		/// </param>
		/// <param name="formaPesquisaFiltro">
		/// The forma pesquisa filtro.
		/// </param>
		/// <typeparam name="T">
		/// dsadsad asdsa dsa
		/// </typeparam>
		/// <returns>
		/// dsadas dsadsada
		/// </returns>
		public static DetalhesFiltroWeb Create<T>(string campo, FormaPesquisaFiltro formaPesquisaFiltro)
		{
			return new DetalhesFiltroWeb
				{
					Campo = campo,
					FormaPesquisa = formaPesquisaFiltro,
					TipoCampo = typeof(T)
				};
		}

		/*
		/// <summary>
		/// Converte uma lista de web em objects
		/// </summary>
		/// <param name="detalhesFiltroWeb">
		/// The detalhes filtro web.
		/// </param>
		/// <returns>
		/// Lista de dasdas 
		/// </returns>
		public static IDetalhesFiltro<object> Converter(DetalhesFiltroWeb[] detalhesFiltroWeb)
		{
			
		}*/
	}
}