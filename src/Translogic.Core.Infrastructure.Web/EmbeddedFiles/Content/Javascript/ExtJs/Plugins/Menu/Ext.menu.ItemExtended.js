﻿Ext.ns('Ext.menu');

Ext.menu.ItemExtended = function(config) {
	Ext.menu.ItemExtended.superclass.constructor.call(this, config);
	this.addEvents("contextmenu");
};

Ext.extend(Ext.menu.ItemExtended, Ext.menu.Item, {
	ctype: "Ext.menu.ItemExtended",
	// private
	onRender: function(ct) {
		Ext.menu.ItemExtended.superclass.onRender.apply(this, arguments);
		this.mon(this.el, {
			"contextmenu": this.onContextMenu,
			scope: this
		});
	},
	onContextMenu: function(e) {
		e.preventDefault();
		this.fireEvent("contextmenu", this, e);
	},
	destroy: function() {
		if (this.childNodes) {
			for (var i = 0, l = this.childNodes.length; i < l; i++) {
				this.childNodes[i].destroy();
			}
			this.childNodes = null;
		}
	},
	menuContexto: null,
	setMenuContexto: function (obj){this.menuContexto = obj;}
}
);