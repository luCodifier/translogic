// vim: ts=4:sw=4:nu:fdc=4:nospell
/*global Ext, console */
/**
* @class Ext.ux.form.ServerValidator
* @extends Ext.util.Observable
*
* Server-validates field value
*
* @author    Ing. Jozef Sakáloš
* @copyright (c) 2008, by Ing. Jozef Sakáloš
* @date      8. February 2008
* @version   1.0
* @revision  $Id: Ext.ux.form.ServerValidator.html 148 2009-04-13 15:29:20Z jozo $
* 
* @license Ext.ux.form.ServerValidator is licensed under the terms of
* the Open Source LGPL 3.0 license.  Commercial use is permitted to the extent
* that the code/component(s) do NOT become part of another Open Source or Commercially
* licensed development library or toolkit without explicit permission.
* 
* 

License details: http://www.gnu.org/licenses/lgpl.html

*
* @donate
* 

*
*
*
*
*

*/

Ext.ns('Ext.ux.form');

/**
* Creates new ServerValidator
* @constructor
* @param {Object} config A config object
*/
Ext.ux.form.ServerValidator = function(config)
{
	Ext.apply(this, config, {

		/**
		* @cfg {String} url URL to send validation requests to
		*/
		url: '/request.php'

		/**
		* @cfg {String} method Http method of requests. Valid
		* values are 'GET' and 'POST' (Defaults to 'POST')
		*/
		, method: 'POST'

		/**
		* @cfg {String} cmd This value is posted together with validation request.
		*/
		, cmd: 'validateField'

		/**
		* @cfg {Object} paramNames Names of 'valid' and 'reason' properties. Change
		* them if your server return results with another names (default to {valid:'valid',
		* reason:'reason'
		*/
		, paramNames: {
			valid: 'valid'
				, reason: 'reason'
		}

		/**
		* @cfg {Number} validationDelay Number of milliseconds to wait before the validation
		* request is sent to server (defaults to 500).
		*/
		, validationDelay: 500


		/**
		* @cfg {String} validationEvent Name of the event that triggers the validation
		* (defaults to 'keyup').
		*/
		, validationEvent: 'keyup'

		, logFailure: false
		, logSuccess: false
	});
	Ext.ux.form.ServerValidator.superclass.constructor.apply(this, arguments);
}; // eo constructor

// extend
Ext.extend(Ext.ux.form.ServerValidator, Ext.util.Observable, {

	// {{{
	init: function(field)
	{
		this.field = field;
		// save original functions
		var isValid = field.isValid;
		var validate = field.validate;

		Ext.apply(field, {
			// is field validated by server flag
			serverValid: undefined !== this.serverValid ? this.serverValid : true
			//                       serverValid: true

			// private
			, isValid: function(preventMark)
			{
				if (this.disabled)
				{
					return true;
				}
				return isValid.call(this, preventMark) && this.serverValid;
			}

			// private
			, validate: function()
			{
        		var clientValid = validate.call(this);

        		// return false if client validation failed
        		if (!this.disabled && !clientValid)
        		{
        			return false;
        		}

        		// return true if both client valid and server valid
        		if (this.disabled || (clientValid && this.serverValid))
        		{
        			this.clearInvalid();
        			return true;
        		}

        		// mark invalid and return false if server invalid
        		if (!this.serverValid)
        		{
        			this.markInvalid(this.reason);
        			return false;
        		}

        		return false;
			} // eo function validate

		}); // eo apply

		// install listeners
		this.field.on({
			render: { single: true, scope: this, fn: function()
			{
				this.serverValidationTask = new Ext.util.DelayedTask(this.serverValidate, this);
				this.field.el.on(this.validationEvent, function(e)
				{
					this.field.serverValid = false;
					this.filterServerValidation(e);
				}, this);
				//                              this.field.el.on({
				//                                      keyup:{scope:this, fn:function(e) {
				//                                              this.field.serverValid = false;
				//                                              this.filterServerValidation(e);
				//                                      }}
				////                                    ,blur:{scope:this, fn:function(e) {
				////                                            this.field.serverValid = false;
				////                                            this.filterServerValidation(e);
				////                                    }}
				//                              });
			}
			}
		});
	} // eo function init
	// }}}

        , serverValidate: function()
        {
        	var options = {
        		url: this.url + '?#' + (this.name || this.field.name)
                        , method: this.method
                        , scope: this
                        , success: this.handleSuccess
                        , failure: this.handleFailure
                        , params: this.params || {}
        	};
        	Ext.applyIf(options.params, {
        		cmd: this.cmd
                        , field: this.name || this.field.name
                        , value: this.field.getValue()
                        , table: this.table
        	});
        	Ext.Ajax.request(options);
        } // eo function serverValidate

	// {{{
        , filterServerValidation: function(e)
        {
        	if (this.field.value === this.field.getValue())
        	{
        		this.serverValidationTask.cancel();
        		this.field.serverValid = true;
        		return;
        	}
        	if (!e.isNavKeyPress())
        	{
        		this.serverValidationTask.delay(this.validationDelay);
        	}
        } // eo function filterServerValidation
	// }}}
	// {{{
        , handleSuccess: function(response, options)
        {
        	var o;
        	try { o = Ext.decode(response.responseText); }
        	catch (e)
        	{
        		if (this.logFailure)
        		{
        			this.log(response.responseText);
        		}
        	}
        	if (true !== o.success)
        	{
        		if (this.logFailure)
        		{
        			this.log(response.responseText);
        		}
        	}
        	this.field.serverValid = true === o[this.paramNames.valid];
        	this.field.reason = o[this.paramNames.reason];
        	this.field.validate();
        } // eo function handleSuccess
	// }}}
	// {{{
        , handleFailure: function(response, options)
        {
        	if (this.logFailure)
        	{
        		this.log(response.responseText);
        	}
        } // eo function handleFailure
	// }}}
	// {{{
        , log: function(msg)
        {
        	if (console && console.log)
        	{
        		console.log(msg);
        	}
        } // eo function log
	// }}}

});

// shortcut
Ext.ux.ServerValidator = Ext.ux.form.ServerValidator;

// register xtype
Ext.reg('servervalidator', Ext.ux.form.ServerValidator);

// eof