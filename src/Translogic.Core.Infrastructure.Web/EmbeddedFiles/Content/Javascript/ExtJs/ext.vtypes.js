﻿// var cteFieldExpr = /^\d{44}$/;
var cteFieldExpr = /^\d{44}$/;
var cteEstacaoExpr = /^[a-zA-Z]{3}$/;
var cteFluxoExpr = /^[0-9]+$/;
var cteVagaoExpr = /^[0-9]+$/;
var cteSerieDespExpr = /^[a-zA-Z0-9]+$/;
var cteDespachoExpr = /^[0-9]+$/;

Ext.apply(Ext.form.VTypes, {
	ctevtype: function (v) {
		if (v == '')
			return true;
		return cteFieldExpr.test(v);
	},
	ctevtypeText: 'Campo do CT-e incompleto.',
	ctevtypeMask: /[0-9]/
});

Ext.apply(Ext.form.VTypes, {
	ctefluxovtype: function (v) {
		if (v == '')
			return true;
		return cteFluxoExpr.test(v);
	},
	ctefluxovtypeText: 'Campo de Fluxo inv&aacute;lido.',
	ctefluxovtypeMask: /[0-9]/
});

Ext.apply(Ext.form.VTypes, {
	ctevagaovtype: function (v) {
		if (v == '')
			return true;
		return cteVagaoExpr.test(v);
	},
	ctevagaovtypeText: 'Campo de Vag&atildeo inv&aacute;lido.',
	ctevagaovtypeMask: /[0-9]/
});

Ext.apply(Ext.form.VTypes, {
	cteestacaovtype: function (v) {
		if (v == '')
			return true;
		return cteEstacaoExpr.test(v);
	},
	cteestacaovtypeText: 'Campo inv&aacute;lido.',
	cteestacaovtypeMask: /[a-zA-Z]/
});

Ext.apply(Ext.form.VTypes, {
	ctesdvtype: function (v) {
		if (v == '')
			return true;
		return cteSerieDespExpr.test(v);
	},
	ctesdvtypeText: 'Campo Série Despacho inv&aacute;lido.',
	ctesdvtypeMask: /[a-zA-Z0-9]/
});

Ext.apply(Ext.form.VTypes, {
	ctedespachovtype: function (v) {
		if (v == '')
			return true;
		return cteDespachoExpr.test(v);
	},
	ctedespachovtypeText: 'Campo Despacho inv&aacute;lido.',
	ctedespachovtypeMask: /[0-9]/
});

Ext.apply(Ext.form.VTypes, {
	daterange: function (val, field) {

		var date = field.parseDate(val);

		if (!date) {
			return;
		}
		if (this.dateRangeMax == undefined) {
			this.dateRangeMax = date;
		}

		if (this.dateRangeMin == undefined) {
			this.dateRangeMin = date;
		}

		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = Ext.getCmp(field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
		}
		else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = Ext.getCmp(field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
		}
		/*
		* Always return true since we're only using this vtype to set the
		* min/max allowed values (these are tested for after the vtype test)
		*/
		return true;
	},

	password: function (val, field) {
		if (field.initialPassField) {
			var pwd = Ext.getCmp(field.initialPassField);
			return (val == pwd.getValue());
		}
		return true;
	},

	passwordText: 'Passwords do not match'
});