﻿//Valida se valor é texto
function isText(n) {
    return isNaN(parseFloat(n)) && isFinite(n);
};

//Valida se valor é numero
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

//Valida se data Inicial é Maior que data Final
function comparaDatas(dataIni, dataFim) {

    var arrDtIni = dataIni.split('/');
    var arrDtFim = dataFim.split('/');

    var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
    var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

    return dtIni > dtFim;
};

//Valida se datas inicial e final informadas pelo usuario são periodo de 1 ano
function validarPeriodoUmAno(dtIni, dtFim) {

    var diasAno = 365;
    var arrDtIni = dtIni.split('/');
    var arrDtFim = dtFim.split('/');

    var anoIni = arrDtIni[2];
    var anoFim = arrDtFim[2];

    var dtIni = new Date(arrDtIni[2] + '/' + arrDtIni[1] + '/' + arrDtIni[0]);
    var dtFim = new Date(arrDtFim[2] + '/' + arrDtFim[1] + '/' + arrDtFim[0]);

    var diferenca = Math.abs(dtIni.getTime() - dtFim.getTime());
    var dia = 1000 * 60 * 60 * 24;
    var total = Math.round(diferenca / dia);

    //verifica se é ano bissexto
    if ((anoIni % 4 == 0) && ((anoIni % 100 != 0) || (anoIni % 400 == 0)))
        diasAno = 366;

    if ((anoFim % 4 == 0) && ((anoFim % 100 != 0) || (anoFim % 400 == 0)))
        diasAno = 366;
    //fim de verificação de ano bissexto

    if (total > diasAno) {
        return true;
    }
    return false;
};

// Validar se extensão do arquivo é do tipo requerido
function validateFileExtension(file, extensao) {
    var extension = file.split('.').pop();

    if (extension == extensao)
        return true;
    else
        return false;
};

//Valida data valida d/m/y
function isValidDate(date) {
    var bits = s.split('/');
    var d = new Date(bits[2], bits[1] - 1, bits[0]);
    return d && (d.getMonth() + 1) == bits[1];
}

function alerta(msg) {
    Ext.Msg.show({
        title: "Aviso",
        msg: msg,
        buttons: Ext.Msg.OK,
        icon: Ext.MessageBox.QUESTION,
        minWidth: 200
    });
}

function alerta(msg, icon) {
    Ext.Msg.show({
        title: "Aviso",
        msg: msg,
        buttons: Ext.Msg.OK,
        icon: icon,
        minWidth: 200
    });
}

function alerta(title, msg, icon) {
    Ext.Msg.show({
        title: title,
        msg: msg,
        buttons: Ext.Msg.OK,
        icon: icon,
        minWidth: 200
    });
}

function loader(msg) {
    Ext.getBody().mask(msg, "x-mask-loading");
}

function closeLoader() {
    Ext.getBody().unmask();
}


// Setar valueField em combos (ID). Default: executaLoad = false, loaderParams = null 
// executaLoad = true - irá executar o load do store da combo. Se houver loaderParams irá usar os parâmetros no load
// Ex. setValue(Ext.getCmp('cmbSubDivisao'), subdivisaoId, true, { idSubDiv: subdivisaoId });
function setarValorCombo(combo, value, executaLoad, loaderParams) {
    combo.setValue(value);
    var store = combo.getStore();
    store.on("load", function () {
        combo.setValue(value);
    });
    if (executaLoad) {
        if (loaderParams) {
            store.load({ params: loaderParams });
        }
        else {
            store.load();
        }
    }
}

function sleepFechaProgressBar(milisegundos) 
{
    setTimeout(function () { closeLoader(); }, milisegundos);
}

// Retorna true se a aba já estiver aberta ou false se não estiver
function validarAbaAberta(tituloAba) {
    var conteudo_principal = parent.Ext.getCmp("conteudo-principal");
    var aberta = false;
    for (var i = 0; i < conteudo_principal.items.items.length; i++) {
        if (conteudo_principal.items.items[i].title == tituloAba) {
            aberta = true;
            break;
        }
    }
    return aberta;
}

