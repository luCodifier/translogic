Ext.namespace('Translogic');

var meuTranslogic;
var contextMenuMeuTranslogic = null;

var DynamicKeyMap = new Ext.KeyMap(document);
DynamicKeyMap.accessKey = function(key, handler, scope) {
	var dynamicHandler = function(keycd, dynamicEvent) {
		dynamicEvent.preventDefault();
		handler.call(scope || this, keycd, dynamicEvent);
	};
	this.on(key, dynamicHandler, scope);
};

Ext.override(Ext.form.TextField, {
	afterRender: function() {
		Ext.form.TextField.superclass.afterRender.call(this);
		if (this.title) {
			this.container.dom.title = this.title;
		}

		if (this.accessKey) {
			this.container.dom.accessKey = this.accessKey;
		}
	}
});


Translogic.MenuPrincipal = Ext.extend(Ext.Toolbar, {

    id: 'menu-principal',
    border: true,
    region: 'south',
    height: 24,

    onRender: function () {

        var toolbar = this;

        toolbar.add({ xtype: 'tbspacer', width: 10 });

        toolbar.addText(App.Resources.Web.Tela + ":");

        toolbar.add(new Ext.form.TextField({
            id: 'codigo-tela',
            width: 38,
            maskRe: /[a-zA-Z0-9]/,
            selectOnFocus: true,
            title: App.Resources.Web.DigiteNumeroTelaPressioneAlt_a,
			autoCreate: { tag: 'input', type: 'text', maxlength: '5', autocomplete: 'off' },
            accessKey: "a",
            listeners: {
                specialKey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        var codigoTela = Ext.getCmp("codigo-tela").getValue();
                        layoutPrincipal.menuLateral.abrirTela(codigoTela);
                    }
                },
                render: function () {
                    DynamicKeyMap.accessKey({ key: 'a', alt: true }, this.focus, this);
                }
            }
        }));

        toolbar.add({ xtype: 'tbspacer', width: 5 });


        toolbar.add({
            iconCls: 'icon-tab-nova',
            tooltip: App.Resources.Web.AbrirEmNovaAba,
            handler: function () {
                var codigoTela = Ext.getCmp("codigo-tela").getValue();
                layoutPrincipal.menuLateral.abrirTela(codigoTela, true, true);
            }
        });

        toolbar.addSeparator();

        toolbar.add({
            iconCls: 'icon-map-giz',
            tooltip: "Acesso ao Gis",
            handler: function () {
                layoutPrincipal.menuLateral.abrirTela(0, true, true, "GIS");
            }
        });

        toolbar.addSeparator();

        toolbar.add({
            iconCls: 'icon-manual-translogic',
            tooltip: "Manual",
            handler: function () {
                var win = window.open('http://manualtranslogic.rumolog.net', '_blank');
                win.focus();
            }
        });

        toolbar.addFill();

        toolbar.addSeparator();

        toolbar.addButton({
            id: 'meu-translogic',
            iconCls: 'icon-house',
            text: App.Resources.Web.MeuTranslogic,
            tooltip: App.Resources.Web.MeuTranslogic,
            menu: new Ext.menu.Menu()
        });

        toolbar.addSeparator();

        Ext.each(this.itensMenu, function () {

            var menu = new Ext.menu.Menu();

            Ext.each(this.Filhos, function () {

                if (this.Filhos != null && this.Filhos.length > 0) {
                    var items2 = new Array();
                    Ext.each(this.Filhos, function () {
                        // alert(this.Titulo);
                        items2.push({
                            text: this.Titulo,
                            data: this,
                            handler: toolbar.onItemClick
                        });
                    });

                    var submenu = {
                        text: this.Titulo,
                        menu: items2
                    };
                } else {
                    var submenu = {
                        text: this.Titulo,
                        data: this,
                        handler: toolbar.onItemClick
                    };
                }

                menu.add(submenu);

            });

            toolbar.addButton({
                text: this.Titulo,
                menu: menu
            });
        });

        Translogic.MenuPrincipal.superclass.onRender.apply(this, arguments);
    },
    carregarMeuTranslogic: function () {
        Ext.Ajax.request({
            url: App.Paths.Route("Core", "Menu", "CarregarMeuTranslogic"),
            success: function (data) {
                var json = Ext.decode(data.responseText);

                layoutPrincipal.menuPrincipal.criarMeuTranslogic(json);
            }
        });
    },
    criarMeuTranslogic: function (data) {
        meuTranslogic = Ext.getCmp("meu-translogic");
        var items = [];

        Ext.each(data, function (e) {
            items.push(new Ext.menu.ItemExtended({
                text: e.CodigoTela + ' - ' + e.Titulo,
                isLeaf: !e.EhPasta,
                data: e,
                handler: function (e) {
                    layoutPrincipal.menuLateral.abrirTela(e.data.CodigoTela, true, false, e.data.SistemaOrigem);
                },
                listeners: {
                    'contextmenu': function (e, o) {

                        if (e.data.EhPasta) {
                            return false;
                        }
                        var contextCodigoTela = e.data.CodigoTela;

                        var menuItem = new Ext.menu.Item({
                            text: App.Resources.Web.ExcluirMeuTranslogic,
                            iconCls: 'icon-del',
                            handler: function () {
                                Ext.Ajax.request({
                                    url: App.Paths.Route("Core", "Menu", "RemoverItemMeuTranslogic"),
                                    success: function (data) {
                                        var menuMeuTL = Ext.getCmp("meu-translogic-menu");
                                        menuMeuTL.hide();
                                        menuMeuTL.destroy();

                                        layoutPrincipal.menuPrincipal.carregarMeuTranslogic();
                                    },
                                    params: { id: e.data.Id }
                                });
                            }
                        });

                        if (contextMenuMeuTranslogic == null) {
                            contextMenuMeuTranslogic = new Ext.menu.Menu({});
                        }
                        contextMenuMeuTranslogic.removeAll();
                        contextMenuMeuTranslogic.add(menuItem);

                        contextMenuMeuTranslogic.showAt(o.getPoint());
                    }
                }
            }));

        });

        meuTranslogic.menu = new Ext.menu.Menu({
            id: 'meu-translogic-menu',
            items: items
        });
    }
});