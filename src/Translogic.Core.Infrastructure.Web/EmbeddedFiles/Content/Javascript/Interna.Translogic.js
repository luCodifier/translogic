Ext.BLANK_IMAGE_URL = "/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/blank.gif";

$.ajaxSetup({ error: ValidarRetornoErro });

Ext.override(Ext.data.Store, {
    listeners: {
        'loadexception': function (proxy, options, response) {
            ValidarRetornoErro(response);
        }
    }
});

$(function () {
    Ext.QuickTips.init();

    // timeout
    var min5 = 5 * 60 * 1000;
    try {
        Ext.Ajax.timeout = min5;
        Ext.override(Ext.data.proxy.Ajax, { timeout: min5 });
    }
    catch (e) { }

    $("FORM.ext-form").transformToExtForm();
    Ext.Ajax.on('beforerequest', function (conn, options) {
        try {
            // alert(1);
            parent.$(".ajax-loader").show();
            parent.layoutPrincipal.menuUsuario.ativaContador();
        } catch (e) { }
        if (options.headers == null) options.headers = {};
        options.headers.IpClient = App.Context.ServerAddress;
        options.headers.TempoRestanteEmSegundos = getTimer();
        options.headers.UsuarioLogado = App.User.Name;
    }, this);
    Ext.Ajax.on('requestcomplete', function () { parent.$(".ajax-loader").hide(); }, this);
    Ext.Ajax.on('requestexception', function (conn, response, options) {
        parent.$(".ajax-loader").hide();
        ValidarRetornoErro(response);
    }, this);

});

function ValidarRetornoErro(response) {

    try {
        //salvarLog(response);
        var responseObj = Ext.decode(response.responseText);
        if (responseObj && responseObj.statusCode == 401) {
            parent.Ext.Msg.show({
                title: App.Resources.Web.TituloErro,
                msg: App.Resources.Web.UsuarioNaoLogado,
                buttons: Ext.Msg.OK,
                fn: function () { parent.location = parent.location; },
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        } else if (responseObj && responseObj.statusCode == 403) {
            parent.Ext.Msg.show({
                title: App.Resources.Web.TituloErro,
                msg: App.Resources.Web.OperacaoNaoPermitida,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR,
                minWidth: 200
            });
        } else if (responseObj && (responseObj.statusCode == 500 && responseObj.type == 'exception')) {
            parent.Ext.Msg.show({
                title: App.Resources.Web.TituloErro,
                msg: responseObj.message,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
        else {
            throw responseObj;
        }

    } catch (e) {
        parent.Ext.Msg.show({
            title: App.Resources.Web.TituloErro,
            msg: App.Resources.Web.FalhaAjax,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
        });
    }

}

function salvarLog(response) {
    try {
        $.ajax
            (
                {
                    url: '/Logs/SalvarWebException',
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: $.toJSON(response),
                    contentType: "application/json; charset=utf-8",
                    failure: function (result) {
                        // n�o faz nada
                    }
                }
            );
    } catch (e) { }
}
