﻿
// Remove caracteres de uma string
function removeCaracteres(str, sub) {
    i = str.indexOf(sub);
    r = "";
    if (i == -1) return str;
    r += str.substring(0, i) + removeCaracteres(str.substring(i + sub.length), sub);
    return r;
} // Remove caracteres de uma string


// Validar numero do CPF
function validarCPF(cpf) {

    cpf = removeCaracteres(cpf, ".");
    cpf = removeCaracteres(cpf, "-");

    if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" ||
		  cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" ||
		  cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
		  cpf == "88888888888" || cpf == "99999999999") {
        return false;
    }

    soma = 0;
    for (i = 0; i < 9; i++)
        soma += parseInt(cpf.charAt(i)) * (10 - i);
    resto = 11 - (soma % 11);
    if (resto == 10 || resto == 11)
        resto = 0;
    if (resto != parseInt(cpf.charAt(9))) {
        return false;
    }
    soma = 0;
    for (i = 0; i < 10; i++)
        soma += parseInt(cpf.charAt(i)) * (11 - i);
    resto = 11 - (soma % 11);
    if (resto == 10 || resto == 11)
        resto = 0;
    if (resto != parseInt(cpf.charAt(10))) {
        return false;
    }
    return true;
} // Validar numero do CPF

// Validar Numero da DANFE
function validarDanfe(danfe) {
    var i = 0;
    var total = 0;
    var pos = 1;
    var dv = 0;

    dv = danfe.charAt(danfe.length - 1);

    for (i = danfe.length - 2; i >= 0; i--) {

        pos++;
        if (pos > 9) {
            pos = 2;
        }

        total = total + (danfe.charAt(i) * pos);
    }

    var resto = (11 - Math.round(total % 11));

    if ((resto == 0 || resto == 1 || resto == 10 || resto == 11) && dv == 0) {
        return true;
    } else if (resto == dv) {
        return true;
    } else {
        return false;
    }
      
} // Validar Numero da DANFE

//Tipos de arquivos permitidos para upload - Patrimonios
function IsFileAllowed(value){
	var validExtensions = new Array();
		var ext = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
       
	validExtensions[0] = 'pdf';
	validExtensions[1] = 'gif';
	validExtensions[2] = 'tiff';
	validExtensions[3] = 'png';

	for(var i = 0; i < validExtensions.length; i++) 
		{
		if(ext == validExtensions[i])
      		return true;
	}

	alert('A extensão ' + ext.toUpperCase() + ' não é permitida!');
	return false;
}	