Ext.namespace('Translogic');

Translogic.PaginatedEditorGrid = Ext.extend(Ext.grid.EditorGridPanel, {
	autoLoadGrid: true,
	initComponent: function () {
		this.init();
		
		if (this.viewConfig == undefined) {
			this.viewConfig = {
				forceFit: true
			}
		}

		Ext.apply(this, {
			store: this.overviewStore,
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					sortable: true
				},
				columns: this.columns
			}),
            clicksToEdit: 1,
			tbar: this.filteringToolbar,
			bbar: this.pagingToolbar,
			sm: this.sm,
			viewConfig: this.viewConfig,
			stripeRows: true,
			height: this.height,
			width: this.width,
			loadMask: { msg: App.Resources.Web.Carregando },
			plugins: this.plugins
		});

		this.on('render', function () {
			if (this.autoLoadGrid)
				this.overviewStore.load({ params: { 'pagination.Start': 0, 'pagination.Limit': this.pagingToolbar.pageSize} });
		});

		// call the superclass's constructor with the right scope

		Translogic.PaginatedEditorGrid.superclass.initComponent.call(this);
	},

	init: function () {

		this.overviewStore = new Ext.data.JsonStore({
			url: this.url,
			root: 'Items',
			totalProperty: 'Total',
			remoteSort: true,
			paramNames: {
				sort: "pagination.Sort",
				dir: "pagination.Dir",
				start: "pagination.Start",
				limit: "pagination.Limit"
			},
			fields: this.fields		// defined by user
		});

		this.pagingToolbar = new Ext.PagingToolbar({
			pageSize: 50,
			store: this.overviewStore,
			displayInfo: true,
			displayMsg: App.Resources.Web.GridExibindoRegistros,
			emptyMsg: App.Resources.Web.GridSemRegistros,
			paramNames: {
				start: "pagination.Start",
				limit: "pagination.Limit"
			}
		});
	}
});

Ext.reg('paginatedEditorGrid', Translogic.PaginatedEditorGrid);