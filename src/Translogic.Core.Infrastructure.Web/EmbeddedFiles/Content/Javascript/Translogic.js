Ext.BLANK_IMAGE_URL = "/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/Images/blank.gif";

$(function () {
    Ext.QuickTips.init();

    $("FORM.ext-form").transformToExtForm();
    Ext.Ajax.on('beforerequest', function (conn, options) {
        $(".ajax-loader").show();
        if (options.headers == null) options.headers = {};
        options.headers.IpClient = App.Context.ServerAddress;
        options.headers.TempoRestanteEmSegundos = getTimer();
        options.headers.UsuarioLogado = App.User.Name
    }, this);

    Ext.Ajax.on('requestcomplete', function () { $(".ajax-loader").hide() }, this);
    Ext.Ajax.on('requestexception', function () { $(".ajax-loader").hide() }, this);
    Ext.Ajax.timeout = 120000;
    Ext.Ajax.on('requestexception', function (conn, response, options) {
        try {
            //salvarLog(response);
            var responseObj = Ext.decode(response.responseText);
            if (responseObj && responseObj.statusCode == 401) {
                Ext.Msg.show({
                    title: App.Resources.Web.TituloErro,
                    msg: App.Resources.Web.UsuarioNaoLogado,
                    buttons: Ext.Msg.OK,
                    fn: function () { window.location = window.location; },
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            } else if (responseObj && responseObj.statusCode == 403) {
                Ext.Msg.show({
                    title: App.Resources.Web.TituloErro,
                    msg: App.Resources.Web.OperacaoNaoPermitida,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR,
                    minWidth: 200
                });
            } else if (responseObj && (responseObj.statusCode == 500 && responseObj.type == 'exception')) {
                Ext.Msg.show({
                    title: App.Resources.Web.TituloErro,
                    msg: responseObj.message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
            else {
                throw responseObj;
            }

        } catch (e) {
            Ext.Msg.show({
                title: App.Resources.Web.TituloErro,
                msg: App.Resources.Web.FalhaAjax,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }



    }, this);

});

function salvarLog(response) {
    try {
        $.ajax
            (
                {
                    url: '/Logs/SalvarWebException',
                    type: "POST",
                    async: false,
                    dataType: 'json',
                    data: $.toJSON(response),
                    contentType: "application/json; charset=utf-8",
                    failure: function (result) {
                        // n�o faz nada
                    }
                }
            );
    } catch (e) {}
}
