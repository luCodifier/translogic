Ext.namespace('Translogic');

Translogic.ConteudoPrincipal = Ext.extend(Ext.TabPanel, {
	id: 'conteudo-principal',
	region: 'center',
	plugins: new Ext.ux.TabCloseMenu(),
	enableTabScroll: true,
	activeTab: 0,
	onStripMouseDown: function(e) {

		if (e.button !== 0) {
			return;
		}

		e.preventDefault();

		var t = this.findTargets(e);

		if (t.item && t.item.blankTab) {
			this.criaNovaAba();
		}
		else {
			Translogic.ConteudoPrincipal.superclass.onStripMouseDown.apply(this, arguments);
		}
	},
	criaNovaAba: function() {
		var blankTab = new Ext.Panel({
			title: 'Nova aba',
			closable: true
		});

		this.insert(this.items.length - 1, blankTab);

		this.setActiveTab(this.items.length - 2);

		$("#codigo-tela").focus();
	}
});