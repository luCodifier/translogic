var blankImage = '/GetFile.efh?file={Translogic.Core.Infrastructure.Web;}/Content/images/blank.gif';

// Atribui��o de console.debug para quando esse n�o existir (Internet Explorer)
if (typeof (console) == 'undefined') {
	console = {
		debug : function(txt) { }
	}
}

$(function()
{

	$("form").submit(function()
	{
		$(".loading").css("display", "inline");
	});

	$("input, select").focus(function()
	{
		$(this).addClass("highlight");
	}).blur(function()
	{
		$(this).removeClass("highlight");
	});

});