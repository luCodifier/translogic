Ext.namespace('Translogic');



Translogic.StatusBar = Ext.extend(Ext.Toolbar, {
	id: 'status-bar',
	border: false,
	region: 'south',
	height: 28,
	onRender: function() {
		this.addFill();
		
			Translogic.StatusBar.superclass.onRender.apply(this, arguments);
		},
		
	});