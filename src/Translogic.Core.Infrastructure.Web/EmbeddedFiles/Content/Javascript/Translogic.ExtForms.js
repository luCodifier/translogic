jQuery.fn.extend({
	transformToExtForm: function()
	{
		return this.each(function()
		{

			var jForm = $(this);
			var formId = jForm.attr("id");
			var action = jForm.attr("action");

			var successCallback = eval(jForm.attr("onSuccess")) || eval("FormSuccess");
			var errorCallback = eval(jForm.attr("onError")) || eval("FormError");

			var extForm = new Ext.form.BasicForm(formId, {
				onSubmit: function(e)
				{
					e.stopEvent();

					if (!this.isValid())
					{
						return;
					}

					this.submit({
						success: successCallback,
						failure: errorCallback
					});
				}
			});

			$(":submit", jForm).each(function()
			{
				var btn = new Ext.Button({
					applyTo: this,
					type: 'submit',
					text: $(this).attr("value"),
					handler: function()
					{
						jForm.submit();
					}
				});

				extForm.on("actioncomplete", function()
				{
					btn.enable();
				});

				extForm.on("actionfailed", function()
				{
					btn.enable();
				});

				extForm.on("beforeaction", function()
				{
					btn.disable();
				});

				$(this).remove();
			});

			$(":input, select, textarea", jForm).each(function()
			{
				var domElem = this;
				var jElem = $(this);

				var elemId = jElem.attr("id");

				var elem = Ext.get(elemId);

				var allowBlank = jElem.attr("required") != "true";
				var readOnly = jElem.attr("READONLY") == "READONLY";

				var vtype = jElem.attr("vtype");
				var emptyText = jElem.attr("emptyText");

				var minLength = jElem.attr("minLength") | 0;
				var maxLength = jElem.attr("maxLength");

				if (!maxLength || (maxLength && maxLength <= 0))
				{
					maxLength = Number.MAX_VALUE;
				}

				var fld = null;

				if (jElem.is(":text"))
				{
					if (jElem.attr("behavior") == 'date-picker')
					{
						fld = new Ext.form.DateField({
							applyTo: domElem,
							format: 'm/d/Y',
							allowBlank: allowBlank,
							vtype: vtype,
							emptyText: emptyText,
							readOnly: readOnly
						});
					}
					else if (jElem.attr("behavior") == 'numeric')
					{
						fld = new Ext.form.NumberField({
							applyTo: domElem,
							allowBlank: allowBlank,
							emptyText: emptyText,
							readOnly: readOnly
						});

						var decimalPrecision = jElem.attr("decimalPrecision");
						var allowDecimals = jElem.attr("allowDecimals");
						var allowNegative = jElem.attr("allowNegative");

						fld.decimalPrecision = decimalPrecision || fld.decimalPrecision;
						fld.allowDecimals = allowDecimals || fld.allowDecimals;
						fld.allowNegative = allowNegative || fld.allowNegative;
					}
					else
					{
						fld = new Ext.form.TextField({
							applyTo: domElem,
							allowBlank: allowBlank,
							vtype: vtype,
							minLength: minLength,
							maxLength: maxLength,
							emptyText: emptyText,
							readOnly: readOnly
						});
					}
					
					if (readOnly){
						fld.addClass('x-item-disabled');
					}
				}
				else if (jElem.is("SELECT"))
				{
					fld = new Ext.form.ComboBox({
						transform: domElem,
						typeAhead: true,
						triggerAction: 'all',
						width: elem.getWidth(),
						forceSelection: true,
						allowBlank: allowBlank,
						vtype: vtype,
						emptyText: emptyText,
						readOnly: readOnly
					});
				}
				else if (jElem.is("TEXTAREA"))
				{
					fld = new Ext.form.TextArea({
						applyTo: domElem,
						allowBlank: allowBlank,
						vtype: vtype,
						minLength: minLength,
						maxLength: maxLength,
						emptyText: emptyText,
						readOnly: readOnly
					});
				}
				else if (jElem.is(":password"))
				{
					fld = new Ext.form.TextField({
						applyTo: domElem,
						allowBlank: allowBlank,
						vtype: vtype,
						inputType: 'password',
						minLength: minLength,
						maxLength: maxLength,
						emptyText: emptyText,
						readOnly: readOnly
					});
				}

				if (jElem.is('.resizeable'))
				{
					new Ext.Resizable(elem, {
						wrap: true,
						pinned: true,
						width: 400,
						height: 150,
						minWidth: 200,
						minHeight: 50,
						dynamic: true
					});
				}

				if (fld)
				{
					extForm.add(fld);
				}

			});

		});
	}
});