﻿//função para permitir somente números
(function($){
 $.fn.onlyNumbers = function() {
    return this.each(function() {
        obj = $(this);
        obj.keypress(function(e){
            if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
                return false;
        });
    });
 };
})(jQuery);

//função para buscar no migre.me a URL encurtada; pode "ir e vir", depende da booleana Shorten
(function($) {
    $.fn.migreMe = function(bShorten) {
        return this.each(function() {
            obj = $(this);
            if (bShorten) {
                sLongURL = obj.val();
                obj.attr('longURL', sLongURL);
                if (!obj.attr('shortURL')) {
                    obj.val("Aguarde...");
                    $.get("MigreMe.ashx", "url=" + escape(sLongURL), function(dados) {
                        obj.attr('shortURL', dados);
                        obj.val(dados);
                    });
                }
                else
                    obj.val(obj.attr('shortURL'));
            }
            else {
                if (obj.attr('longURL') != '')
                    obj.val(obj.attr('longURL'));
            }
        });
    };
})(jQuery);

function migreMe(objName, bShorten) {
    $("#" + objName).migreMe(bShorten);
}

//função para alternar as hashtags do twitter
(function($) {
    $.fn.buscaTwHash = function(twHashTag, twQtd, twCallback) {
        return this.each(function() {
            obj = $(this);
            var twurl = 'http://search.twitter.com/search.json';
            var btRemoverHash = '<a href="javascript:void(0);" class="removerHash btPagina" rel="' + twHashTag + '">remover esta hashtag </a><div class="clear"></div>';
            obj.html("");
            obj.addClass("divCarregando");

            $.getJSON(
                twurl + '?callback=?&rpp=' + twQtd + '&q=' + escape(twHashTag) + '&i=' + Math.random(),
                function(data) {
                    obj.html("");
                    obj.removeClass("divCarregando");
                    obj.append(btRemoverHash);
                    $.each(data.results, function(i, tweet) {
                        if (tweet.text !== undefined) {
                            var tweet_html = '<div class="tweet"><a href="http://www.twitter.com/' + tweet.from_user + '" target="_blank"><img src="' + tweet.profile_image_url + '" width="48" heigh="48"/></a><p>@' + tweet.from_user + ' ' + tweet.text + '<br/><span>via ' + HtmlDecode(tweet.source) + '</span></p><div class="clear"></div><div>';
                            obj.append(tweet_html).linkify();
                        }
                    });

                    if (twCallback)
                        twCallback();
                }
            );
        });
    }
})(jQuery);

(function(b) { var v = /(^|["'(]|&lt;|\s)(www\..+?\..+?)(([:?]|\.+)?(\s|$)|&gt;|[)"',])/g, w = /(^|["'(]|&lt;|\s)(((https?|ftp):\/\/|mailto:).+?)(([:?]|\.+)?(\s|$)|&gt;|[)"',])/g, x = function(h) { return h.replace(v, '$1<a href="<``>://$2">$2</a>$3').replace(w, '$1<a href="$2">$2</a>$5').replace(/"<``>/g, '"http') }, q = b.fn.linkify = function(e) { e = e || {}; if (typeof e == 'string') { e = { use: e} } var c = e.use, k = q.plugins || {}, l = [x], f; if (c) { c = b.isArray(c) ? c : b.trim(c).split(/ *, */); var m, i; for (var n = 0, y = c.length; n < y; n++) { i = c[n]; m = k[i]; if (m) { l.push(m) } } } else { for (var i in k) { l.push(k[i]) } } return this.each(function() { var h = this.childNodes, r = h.length; while (r--) { var d = h[r]; if (d.nodeType == 3) { var a = d.nodeValue; if (a.length > 1 && /\S/.test(a)) { var o, p; f = f || b('<div/>')[0]; f.innerHTML = ''; f.appendChild(d.cloneNode(false)); var s = f.childNodes; for (var t = 0, g; (g = l[t]); t++) { var u = s.length, j; while (u--) { j = s[u]; if (j.nodeType == 3) { a = j.nodeValue; if (a.length > 1 && /\S/.test(a)) { p = a; a = a.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;'); a = b.isFunction(g) ? g(a) : a.replace(g.re, g.tmpl); o = o || p != a; p != a && b(j).after(a).remove() } } } } a = f.innerHTML; o && b(d).after(a).remove() } } else if (d.nodeType == 1 && !/^(a|button|textarea)$/i.test(d.tagName)) { arguments.callee.call(d) } } }) }; q.plugins = {} })(jQuery);

function HtmlDecode(text) {
    return $('<div/>').html(text).text();
}

(function ($) {
    $.fn.timeStamp = function () {
        return this.each(function () {
            obj = $(this);

            obj.keypress(function (e) {
                if (e.which != 36 && e.which != 35 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 9 && e.which != 58)
                    return false;
                if ((e.which >= 48 && e.which <= 57) && ($(this).getSelection().text == "")) {
                    sVal = $(this).val();
                    if (((sVal.length == 2 && (sVal.indexOf(":") == -1)) || (sVal.length == (sVal.indexOf(":") + 3))) && (($(this).attr("maxlength") == -1) || ($(this).attr("maxlength") > $(this).val().length)))
                        $(this).val($(this).val() + ":");
                }
            });

            obj.keyup(function (e) {
                if (e.which == 46 || e.which == 36 || e.which == 35 || e.which == 16 || e.which == 8)
                    return true;

                sVal = $(this).val();
                bShallSet = false;
                if ((sVal.length > 0) && (sVal.indexOf(":") == 0)) {
                    sRetorno = "00:";
                    bShallSet = true;
                    sVal = sVal.substring(1, sVal.length);
                    $(this).val(sVal);
                }

                if (sVal.length > 8)
                    $(this).val(sVal.substring(0, 8));
            });
            obj.blur(function (e) {
                sVal = $(this).val();
                while (sVal.indexOf("::") >= 0) {
                    bShallSet = true;
                    sVal = sVal.replace("::", ":");
                }

                // parse
                iNums = new Array();
                i = 0;
                sAux = sVal;
                while (sAux.indexOf(":") >= 0) {
                    iNums[i++] = parseInt(sAux.substring(0, sAux.indexOf(":")));
                    sAux = sAux.substring(sAux.indexOf(":") + 1, sAux.length);
                }
                if (sAux.length > 0)
                    iNums[i++] = parseInt(sAux);

                i--;
                // soma valores maiores que 60 na casa anterior
                iAux = i;
                while (iAux > 0) {
                    if (iNums[iAux] > 60) {
                        iNums[iAux - 1] += Math.floor(iNums[iAux] / 60);
                        iNums[iAux] = iNums[iAux] % 60;
                    }
                    iAux--;
                }

                if (i == 0)
                    iNums[++i] = 0;
                // bota o retorno, com no mínimo 2 trecos
                sRetorno = "";
                while (i >= 0)
                    sRetorno = (iNums[i] <= 9 ? "0" : "") + iNums[i--] + (sRetorno != "" ? ":" : "") + sRetorno;

                $(this).val(sRetorno);
            });
        });
    }
})(jQuery);

//função para parsear uma hora e transformá-la em segundos (somados); aceita hh:mm:ss ou mm:ss
(function($){
 $.fn.timeToSecond = function(AOnlyLongFormat) {
    var iSuperTotal = 0;
    
    this.each(function() {
        obj = $(this);
        sValor = obj.val();
        iTotal = 0;
        
        if ((sValor.length < 5) || ((AOnlyLongFormat) && (sValor.length < 8)))
            return 0;
        iPrimeiro = parseInt(sValor.substring(0, 2), 10);
        iSegundo = parseInt(sValor.substring(3, 5), 10);
        
        if (sValor.length == 8)
            iTerceiro = parseInt(sValor.substring(6, 8), 10);
        else
            iTerceiro = -1;
        
        if (iTerceiro >= 0) // horas, minutos e segundos
        {
            iPrimeiro *= 60;
            iSegundo *= 60;
            iTotal = iTerceiro;
        }
        else
        {
            if (AOnlyLongFormat)
                return 0;
        }
           
        iPrimeiro *= 60;
        
        iTotal += iPrimeiro + iSegundo;
        
        iSuperTotal += iTotal;
    });
    
    return iSuperTotal;
 };
})(jQuery);
// Nova função para numéricos.
(function ($) {
    $.fn.numeric = function (ASize, APrecision) {
        return this.each(function () {
            obj = $(this);
            obj.keydown(function (e) {
                //$(this).val($(this).val());
            });
            obj.keypress(function (e) {
                if (e.which != 36 && e.which != 35 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 44 && e.which != 46 && e.which != 9)
                    return false;
                if (e.which == 44 || e.which == 46) {
                    if (($(this).val().indexOf(",") > 0) || ($(this).val().indexOf(".") > 0))
                        return false;
                }
            });
            obj.keyup(function (e) {
                if (e.which == 46 || e.which == 36 || e.which == 35 || e.which == 16 || e.which == 8)
                    return true;

                sVal = $(this).val();
                bShallSet = false;
                if (sVal.indexOf(".") > -1) {
                    sVal = sVal.replace(".", ",");
                    bShallSet = true;
                }

                sInteiro = sVal;
                sDecimal = "";
                // verifica parse inteira ;)
                if (sVal.indexOf(",") > -1) {
                    sInteiro = sVal.substring(0, sVal.indexOf(","));

                    if (sInteiro == "")
                        sInteiro = "0";

                    // ...e decimal
                    sDecimal = sVal.substring(sVal.indexOf(",") + 1, sVal.length);
                }

                sRetorno = sInteiro.substring(0, ASize - APrecision);
                if ((sDecimal != "") || (sInteiro.length == ASize - APrecision) || (sVal.indexOf(",") > -1))
                    sRetorno += ",";
                sRetorno += sDecimal.substring(0, APrecision);
                if ((sRetorno != sVal) || (bShallSet))
                    $(this).val(sRetorno);
                return true;
            });
        });
    };
})(jQuery);

//box
(function($){
 $.fn.info = function(Title, Content) {
    return this.each(function() {
        obj = $(this);
        obj.focus(function(e){
            var PosTop = $(this).offset().top;
            var PosLeft = $(this).offset().left + $(this).width() + 30;
            HelpBox(PosLeft, PosTop, Title, Content, $(this).attr("id"), "Interrogacao");
            $("#helpbox_" + $(this).attr("id")).fadeIn(100);
        });
        obj.blur(function(e){
            $("#helpbox_" + $(this).attr("id")).fadeOut(100, function() { $(this).remove()} );
        });
    });
 };
})(jQuery);

(function($){
$.fn.help = function(PosLeft, PosTop, Title, Content) {
    return this.each(function() {
        obj = $(this);
        obj.click(function(e){
            HelpBox(PosLeft, PosTop, Title, Content, "help", "Exclamacao");
            $("#helpbox_help").fadeIn(100);
        });
        obj.blur(function(e){
            $("#helpbox_help").fadeOut(100, function() { $(this).remove()} );
        });
    });
 };
})(jQuery);

function HelpBox(PosLeft, PosTop, Title, Content, ID, Icon)
{
    var BoxWidth = Title.length * 11 + 45;
    if (PosLeft == "centralized") { PosLeft = $(window).width() / 2  - (BoxWidth / 2); }
    if (PosTop == "centralized") { PosTop = ($(window).height() - BoxWidth) / 2; }

    $("body").append( 
    '<div id="helpbox_' + ID + '" class="helpbox" style="position:absolute;top:' + PosTop + 'px;left:' + PosLeft + 'px;width:' + BoxWidth + 'px;display:none;"><div class="helpbox_ec"></div><div class="helpbox_dc"></div><div class="helpbox_e"></div><div class="helpbox_d"></div><div class="helpbox_eb"></div><div class="helpbox_db"></div><h3 id="titulo">' + Title + '</h3><ul><li id="Conteudo">' + Content + '</li></ul><div class="helpbox_c"></div><div class="helpbox_b"></div></div>'
    );
    $("#helpbox_help").fadeIn(100, function() {
        $("body").click(function() {
            if ($("#helpbox_help") != null)
                $("#helpbox_help").fadeOut(100, function() { $(this).remove()} )
            $(this).unbind("click");
        })
    });
}

//função para dar get numa página
(function($){
 $.fn.getPage = function(Page, PosLeft, PosTop, Options) {
    var id = $(this).attr("id");
    var defaults = {  
       trigger: "click",
       hideOnMouseOut: "false",
       closerElement: "",
       fadingTimeOut: 0,
       position: "absolute",
       toSelf: false,
       zIndex: "auto"
    };  
    var Options = $.extend(defaults, Options);
    var style; 
    return this.each(function() {
        obj = $(this);
        if (Options.trigger == "click") { obj.click(function(e) { getFromPage(); }); }
        if (Options.trigger == "load") { obj.ready(function(e) { getFromPage(); }); }
        if (Options.trigger == "focus")  { obj.focus(function(e) { getFromPage(); }); }
        if (Options.trigger == "mouseover") 
        { 
            obj.mouseover(function(e) { getPage(); });
            if (Options.hideOnMouseOut == "true")
                obj.mouseout(function(e) { hideDiv(); });
        }
        if (Options.position != "auto")
            style = "style=\"position:" + Options.position + ";left: " + PosLeft + "px; top: " + PosTop + "px; z-index: " + Options.zIndex + "\""
    });
    
    function getFromPage()
    {
        if ($("#" + id + "div").html() != null)
            return;
            
        $.get(Page, function(dados) {
            if (Options.toSelf)
                $("#" + id).append("<div id=\"" + id + "div\" " + style + " class=\"portlet\">" + dados + "</div>");
            else
                $("body").append("<div id=\"" + id + "div\" " + style + " class=\"portlet\">" + dados + "</div>");
                //$("#" + id).parent().append("<div id=\"" + id + "div\" " + style + " class=\"portlet\">" + dados + "</div>");
            if (Options.closerElement != "")
            {
                $(Options.closerElement).click(function()
                {
                    hideDiv();
                });
            }
            if (Options.fadingTimeOut > 0)
            {
                setTimeout(function() 
                { 
                    $("#" + id + "div").fadeOut("fast"); },
                    Options.fadingTimeOut 
                ); 
            }
        });
    }
    function hideDiv()
    {
        $("#" + id + "div").remove();
    }
 };
})(jQuery);