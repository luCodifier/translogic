Ext.namespace('Translogic');

var _countDowncontainer = 0;
var _currentSeconds = 0;
var windowFaleComEdinho;
var _currentSeconds = 0;
var timerTl3;
var changeColorTimeOut = false;

Translogic.MenuUsuario = Ext.extend(Ext.Toolbar, {

	id: 'menu-usuario',
	border: false,
	region: 'north',
	height: 24,
	onRender: function() {

		var toolbar = this;

		toolbar.addFill();

		var themes = [];
		Ext.each(App.Context.Themes, function (item) {
		    if (item.Codigo != App.Context.Theme.Codigo) {
		        themes.push({
		            iconCls: 'icon-theme-' + item.Codigo, text: item.DisplayName, data: item
		        });
		    }
		});

        // themes.push({ iconCls: 'icon-bullet-black', text: 'Dark', data: { Name: 'slickness'} });
        // themes.push({ iconCls: 'icon-bullet-white', text: 'Padr&atilde;o', data: { Name: 'default'} });

		this.addButton({
		    iconCls: 'icon-theme-' + App.Context.Theme.Codigo,
		    text: 'Tema ' + App.Context.Theme.DisplayName,
			// tooltip: App.Context.Culture.DisplayName,
			menu: new Ext.menu.Menu({
				items: themes,
				defaults: {
					handler: function(e) {
					    Ext.getBody().mask("Carregando...");
                        window.location = App.Paths.Route("core", "home", "AlterarTema",
					    {
						    tema: e.data.Codigo,
						    returnUrl: window.location
					    }
				        );
					}
				}
			})
		});

        this.addSeparator();

		this.addItem(
			new Ext.Toolbar.TextItem({
				// id: 'context-info',
				cls: 'text-gray', //'statusbar-icon-user',
				text: App.User.Name + "@" + App.Context.ServerAddress + ";" + App.Context.LegadoAddress
			}));

		this.addSeparator();

		this.add(
			new Ext.Toolbar.TextItem({
			cls: 'text-gray', // 'icon-calendar',
			// style: {backgroundRepeat:"no-repeat", padding: "0 0 0 20px !important" },
			text: App.User.LastLogin,
			tooltip: App.Resources.Web.UltimoLoginEm + App.User.LastLogin
		}));

		this.addSeparator();
		
		this.addItem(
			new Ext.Toolbar.TextItem({
				id: 'timeout-info',
				cls: 'text-gray', //'statusbar-icon-clock',
				text: '<span id="spanTimeOutTexto">' + App.Resources.Web.SessaoExpiraEm + ' <span id="spanTimeOut">00:00</span></span>'
			}));

		this.addSeparator();
		
		toolbar.addButton({
			text: App.Resources.Web.Sair,
			tooltip: App.Resources.Web.Sair,
			iconCls: 'icon-door-in',
			iconAlign: 'right',
			handler: function() {
				layoutPrincipal.menuUsuario.sairSistema();
			}
		});

		Translogic.MenuUsuario.superclass.onRender.apply(this, arguments);
	},
	sairSistema: function() {
		if (window.confirm(App.Resources.Web.DesejaSair)) {
			window.location = App.Paths.Logout;
		}
	},
	ativaContador: function() {
		clearTimeout(timerTl3);
		var segundosTimeout = 2400;

		layoutPrincipal.menuUsuario.setaValorTimeout(segundosTimeout);
		timerTl3 = window.setTimeout("layoutPrincipal.menuUsuario.atualizarTimeout()", 1000);
		layoutPrincipal.doLayout(false);
			
	},
	atualizarTimeout: function() {
		if (_currentSeconds <= 0) {
			Ext.getBody().mask();
			Ext.Ajax.request({                
			    url: App.Paths.Route("Core", "Acesso", "ForcarLogout"),
			    timeout: 150000,
                success: function(response) {
			    },
                failure: function(conn, data){
                },
			    method: "POST"
		    });

			Ext.Msg.show({
				title: App.Resources.Web.Mensagem,
				msg: App.Resources.Web.SessaoExpiradaPorTempo,
				buttons: Ext.Msg.OK,
				fn: function() { window.location = App.Paths.Logout; },
				icon: Ext.MessageBox.ERROR,
				minWidth: 200
			});
			return;
		}
		this.setaValorTimeout(_currentSeconds - 1);
		timerTl3 = window.setTimeout("layoutPrincipal.menuUsuario.atualizarTimeout()", 1000);
	},
	setaValorTimeout: function(segundosTimeout) {
		_currentSeconds = segundosTimeout;
		var minutes = parseInt(segundosTimeout / 60);
		seconds = (segundosTimeout % 60);
		var hours = parseInt(minutes / 60);
		minutes = (minutes % 60);

		var objTextTimeout = Ext.getDom("spanTimeOutTexto");
		if (segundosTimeout < 60) {
			if (changeColorTimeOut) {
				objTextTimeout.style.color = "#FF0000";

			} else {
				objTextTimeout.style.color = "";

			}
			changeColorTimeOut = !changeColorTimeOut;
		} else {
			objTextTimeout.style.color = "";

		}
		var strText = this.addZero(minutes) + ":" + this.addZero(seconds);
		var objTimeoutInfo = Ext.getDom("spanTimeOut");
		objTimeoutInfo.innerHTML = strText;

	},
	getSegundosTimeout:function(){
		return _currentSeconds;
	},
	addZero: function(num) {
		return ((num >= 0) && (num < 10)) ? "0" + num : num + "";
	}
});
