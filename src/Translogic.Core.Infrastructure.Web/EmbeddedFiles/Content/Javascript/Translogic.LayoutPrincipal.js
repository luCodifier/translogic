Ext.namespace('Translogic');

Translogic.LayoutPrincipal = Ext.extend(Ext.Viewport, {

    id: 'layout-principal',
    border: true,
    layout: 'border',

    menuPrincipal: null,
    menuLateral: null,
    menuUsuario: null,
    conteudo: null,
    statusBar: null,

    onRender: function () {

        var header = new Ext.Panel({
            region: 'north',
            layout: 'border',
            height: 50,
            items: [
				new Ext.Panel({
				    el: 'logo',
				    region: 'west',
				    width: 185,
				    border: false
				}),
				new Ext.Panel({
				    region: 'center',
				    border: false,
				    layout: 'border',
				    items: [
						this.menuUsuario,
						new Ext.Panel({
						    region: 'center',
						    border: false
						}),
						this.menuPrincipal
					]
				})
			]
        });

        this.add(header);

        var pesquisaTelaStore = new Ext.data.JsonStore({
            autoDestroy: true,
            url: App.Paths.Route("Core", "Menu", "PesquisaTela"),
            root: 'items',
            idProperty: 'Id',
            fields: ['Id', 'CodigoTela', 'Titulo', 'Filhos', 'Path', 'SistemaOrigem']
        });

        var pesquisaTelaField = new Ext.form.ComboBox({
            emptyText: App.Resources.Web.PesquisaDeTela,
            width: 195,
            store: pesquisaTelaStore,
            displayField: 'CodigoTela',
            valueField: 'Path',
            typeAhead: false,
            loadingText: App.Resources.Web.Pesquisando,
            hideTrigger: true,
            minChars: 1,
            tpl: '<tpl for="."><div class="x-combo-list-item">{CodigoTela} - {Titulo}</div></tpl>',
            listeners: {
                scope: this,
                select: function (combo, record, index) {

                    this.menuLateral.abrirTela(record.data.CodigoTela, true, false, record.data.SistemaOrigem);
                },
                specialKey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        var codigoTela = Number(field.el.dom.value);

                        this.menuLateral.abrirTela(codigoTela);
                    }
                }
            }
        });

        /*var menuEsquerdaArvore = new Ext.Panel({
        id: 'menu-esquerda-arvore',
        ,
        layout: 'fit',
        border: false,
        items: [this.menuLateral]
        });*/


        this.add(new Ext.Panel({
            id: 'layout-esquerda',
            region: 'west',
            layout: 'border',
            width: 200,
            collapsible: true,
            title: App.Resources.Web.Menu,
            header: true,
            tbar: {
                items: [pesquisaTelaField]
            },
            items: [this.menuLateral/*, this.montaPropriedades()*/],
            listeners: {
                scope: this,
                collapse: function (panel) {
                    // Ext.getCmp('statusbar-when-collapsed').show();
                    // Ext.getCmp('layout-conteudo').doLayout(false, true);
                },
                expand: function (panel) {
                    // Ext.getCmp('statusbar-when-collapsed').hide();
                    // Ext.getCmp('layout-conteudo').doLayout(false, true);
                }
            }
        }));

        this.add(this.conteudo);
        // this.add(this.statusBar);

        Translogic.LayoutPrincipal.superclass.onRender.apply(this, arguments);
    },

    montaPropriedades: function () {

        var oHtml = "<div id='statusbar-logon-info'><div class='statusbar-icon-calendar' style='border-bottom:1px solid #CCCCCC;border-top:1px solid #CCCCCC;'>" + App.User.LastLogin + "</div>" +
					"<div class='statusbar-icon-server' style=''>" + App.Context.ServerAddress + ";" + App.Context.LegadoAddress + "</div><div>";

        var form = {
            xtype: 'panel',
            region: 'south',
            border: false,
            labelWidth: 65,
            width: 200,
            autoHeight: true,
            html: oHtml
        };
        return form;

    }

});