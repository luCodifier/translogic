Ext.namespace('Translogic');

Translogic.MenuLateral = Ext.extend(Ext.Panel, {

    id: 'menu-lateral',
    layout: 'fit',
    region: 'center',
    autoScroll: true,
    lines: false,
    border: false,

    carregarSubMenu: function (pai) {
        // alert(pai.data);

        if (pai.data.Filhos != null && pai.data.Filhos.length > 0) {
            return;
        }

        var menu = Ext.getCmp('menu-lateral');

        var myMask = new Ext.LoadMask(menu.body);

        myMask.show();

        Ext.Ajax.request({
            url: App.Paths.Route("Core", "Menu", "ObterSubMenu"),
            success: function (data) {
                var json = Ext.decode(data.responseText);

                menu.construirTree(json);

                myMask.hide();
                layoutPrincipal.menuUsuario.ativaContador();
            },
            params: { pai: pai.data.Id, sistemaOrigem: pai.data.SistemaOrigem, itemMenu: false }
        });

    },

    carregarMenuParaTela: function (codigoTela, sistemaOrigem) {
        if (typeof codigoTela != "number") {
            return;
        }

        var menu = Ext.getCmp('menu-lateral');

        var myMask = new Ext.LoadMask(menu.body);

        myMask.show();

        Ext.Ajax.request({
            url: App.Paths.Route("Core", "Menu", "ObterMenuParaTela"),
            success: function (data) {
                var json = Ext.decode(data.responseText);

                menu.construirTree(json);

                myMask.hide();
            },
            failure: function (data) {
                myMask.hide();
            },
            params: { codigoTela: codigoTela, sistemaOrigem: sistemaOrigem }
        });
    },

    construirTree: function (json) {
        var menu = Ext.getCmp('menu-lateral');

        var tree = new Ext.tree.TreePanel({
            id: 'menu-tree',
            autoScroll: true,
            animate: false,
            rootVisible: false,
            lines: false
        });

        var root = new Ext.tree.TreeNode({
            expanded: true
        });

        menu.popularTree(json, root);

        tree.setRootNode(root);

        tree.on('click', function (e, evt) {
            var codigoTela = e.attributes.data.CodigoTela;
            var sistemaOrigem = e.attributes.data.SistemaOrigem;

            if (codigoTela == null) {
                e.isExpanded() ? e.collapse() : e.expand();
            } else {
                menu.abrirTela(codigoTela, false, evt.ctrlKey, sistemaOrigem);
            }

            return false;
        });

        if (menu.items.length > 0) {
            menu.remove(0);
        }

        menu.add(tree);

        menu.doLayout();
        if (root.childNodes.length > 0) {
            root.childNodes[0].expand();
        }
    },

    popularTree: function (json, node) {
        var menu = Ext.getCmp('menu-lateral');
        Ext.each(json, function (e) {

            var subNode = new Ext.tree.TreeNode({
                text: e.Titulo,
                expanded: true,
                qtip: e.SistemaOrigem == "SIV" ? "S" + e.CodigoTela : e.CodigoTela,
                data: e,
                leaf: (e.Filhos.length == 0 && e.Path != null && e.Path != "") ? true : false,
                listeners: {
                    'contextmenu': function (a, b) {
                        if (!a.leaf) {
                            return false;
                        }

                        var contextCodigoTela = a.attributes.data.CodigoTela;
                        var sistema = a.attributes.data.SistemaOrigem;

                        var contextMenu = new Ext.menu.Menu({
                            items: [{
                                text: App.Resources.Web.AbrirEmNovaAba,
                                iconCls: 'icon-tab-nova',
                                handler: function () {
                                    layoutPrincipal.menuLateral.abrirTela(contextCodigoTela, false, true, sistema);
                                }
                            },
                            {
                                text: App.Resources.Web.SalvarMeuTranslogic,
                                iconCls: 'icon-house',
                                handler: function () {
                                    Ext.Ajax.request({
                                        url: App.Paths.Route("Core", "Menu", "AdicionarItemMeuTranslogic"),
                                        success: function (data) {
                                            layoutPrincipal.menuPrincipal.carregarMeuTranslogic();
                                        },
                                        params: { codigoTela: contextCodigoTela, sistemaOrigem: sistema }
                                    });
                                }
                            }
							]
                        });

                        contextMenu.showAt(b.getPoint());
                    }
                }
            });

            if ((e.Filhos.length > 0) || (e.Path != "" && e.Path != null)) {
                node.appendChild(subNode);
                menu.popularTree(e.Filhos, subNode);
            }

        });
    },
    abrirTelaEmAba: function (tituloAba, linkAba) {
        var tabs = Ext.getCmp("conteudo-principal");

        var tab = new Ext.ux.ManagedIFrame.Panel(
						{
						    autoScroll: true,
						    closable: true,
						    title: tituloAba,
						    loadMask: {
						        msg: tituloAba
						    },
						    defaultSrc: linkAba,
						    listeners:
							{
							    documentloaded: function (frame) {
							        layoutPrincipal.menuUsuario.ativaContador();
							        layoutPrincipal.doLayout(false);
							    }
							}
						});
        tabs.insert(tabs.items.length - 1, tab);
        tab.show();
        tab.focus(true);
        tabs.setActiveTab(tabs.items.length - 2);
    },
    abrirTela: function (codigoTela, carregarMenu, tabNova, SistemaOrigem) {
        //if (typeof codigoTela != 'number') {
        //	return;
        //}

        if (carregarMenu == undefined) {
            carregarMenu = true;
        }

        if (tabNova == undefined) {
            tabNova = false;
        }

        Ext.Ajax.request({
            url: App.Paths.Route("core", "Menu", "ObterTela"),
            success: function (e) {
                var json = Ext.decode(e.responseText);

                if (!json.success) {
                    Ext.Msg.show({
                        title: 'Mensagem de Informa&ccedil;&atilde;o',
                        msg: json.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    return;
                }

                var tela = json.tela;

                var codigoTela = tela.CodigoTela;

                var path = tela.Path;

                var tabs = Ext.getCmp("conteudo-principal");

                var tab = new Ext.ux.ManagedIFrame.Panel(
					{
					    autoScroll: true,
					    closable: true,
					    codigoTela: codigoTela,
					    title: App.Resources.Web.Tela + " - " + (tela.SistemaOrigem == null ? "" : tela.SistemaOrigem + " - ") + codigoTela,
					    loadMask: {
					        msg: App.Resources.Web.Tela + " - " + (tela.SistemaOrigem == null ? "" : tela.SistemaOrigem + " - ") + codigoTela
					    },
					    defaultSrc: path, //App.Paths.Route("core", "home", "ExibirTela", { codigoTela: codigoTela }),
					    listeners:
						{
						    documentloaded: function (frame) {
						        layoutPrincipal.menuUsuario.ativaContador();
						        layoutPrincipal.doLayout(false);
						    }
						}
					});

                if (!tabNova) {
                    var existente = null;
                    var idxExistente = 0;

                    for (var i = 0; i < tabs.items.length; i++) {
                        var item = tabs.items.items[i];
                        if (item.codigoTela === codigoTela) {
                            existente = item;
                            break;
                        }
                        idxExistente++;
                    }

                    if (existente) {

                        existente.setSrc(existente.defaultSrc);
                        existente.show();
                        tabs.setActiveTab(idxExistente);
                    }
                    else {
                        var activeTab = tabs.getActiveTab();

                        if (!activeTab.closable) {
                            tabs.insert(tabs.items.length - 1, tab);
                            tab.show();
                            tabs.setActiveTab(tabs.items.length - 2);
                        } else {
                            var idx;

                            Ext.each(tabs.items.items, function (item, i) {
                                if (item.id === activeTab.id) {
                                    idx = i;
                                }
                            });

                            activeTab.hide();

                            tabs.insert(idx, tab);
                            tab.show();
                            tabs.setActiveTab(idx);
                            tabs.remove(activeTab.id, true);
                        }


                    }
                } else {
                    tabs.insert(tabs.items.length - 1, tab);
                    tab.show();
                    tabs.setActiveTab(tabs.items.length - 2);
                }

                if (carregarMenu === true) {
                    SistemaOrigem = tela.SistemaOrigem;
                    layoutPrincipal.menuLateral.carregarMenuParaTela(codigoTela, SistemaOrigem);
                }
            },
            params: { codigoTela: codigoTela, sistemaOrigem: SistemaOrigem }
        });
    },
    abrirTelaPath: function (codigoTela, carregarMenu, tabNova, SistemaOrigem,
                             controllerPath, actionPath, param1, param2) {
        //if (typeof codigoTela != 'number') {
        //	return;
        //}

        if (carregarMenu == undefined) {
            carregarMenu = true;
        }

        if (tabNova == undefined) {
            tabNova = false;
        }

        Ext.Ajax.request({
            url: App.Paths.Route("core", "Menu", "ObterTela"),
            success: function (e) {

                var json = Ext.decode(e.responseText);

                if (!json.success) {
                    Ext.Msg.show({
                        title: 'Mensagem de Informa&ccedil;&atilde;o',
                        msg: json.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    return;
                }

                var tela = json.tela;

                var codigoTela = tela.CodigoTela;

                var path = tela.Path;

                var tabs = Ext.getCmp("conteudo-principal");

                var tab = new Ext.ux.ManagedIFrame.Panel(
					{
					    autoScroll: true,
					    closable: true,
					    codigoTela: codigoTela,
					    title: App.Resources.Web.Tela + " - " + (tela.SistemaOrigem == null ? "" : tela.SistemaOrigem + " - ") + codigoTela,
					    loadMask: {
					        msg: App.Resources.Web.Tela + " - " + (tela.SistemaOrigem == null ? "" : tela.SistemaOrigem + " - ") + codigoTela
					    },
					    defaultSrc: App.Paths.Route("core", controllerPath, actionPath, { param1: param1, param2: param2 }),
					    listeners:
						{
						    documentloaded: function (frame) {
						        layoutPrincipal.menuUsuario.ativaContador();
						        layoutPrincipal.doLayout(false);
						    }
						}
					});

                if (!tabNova) {
                    var existente = null;
                    var idxExistente = 0;

                    for (var i = 0; i < tabs.items.length; i++) {
                        var item = tabs.items.items[i];
                        if (item.codigoTela === codigoTela) {
                            existente = item;
                            break;
                        }
                        idxExistente++;
                    }

                    if (existente) {

                        existente.setSrc(existente.defaultSrc);
                        existente.show();
                        tabs.setActiveTab(idxExistente);
                    }
                    else {
                        var activeTab = tabs.getActiveTab();

                        if (!activeTab.closable) {
                            tabs.insert(tabs.items.length - 1, tab);
                            tab.show();
                            tabs.setActiveTab(tabs.items.length - 2);
                        } else {
                            var idx;

                            Ext.each(tabs.items.items, function (item, i) {
                                if (item.id === activeTab.id) {
                                    idx = i;
                                }
                            });

                            activeTab.hide();

                            tabs.insert(idx, tab);
                            tab.show();
                            tabs.setActiveTab(idx);
                            tabs.remove(activeTab.id, true);
                        }


                    }
                } else {
                    tabs.insert(tabs.items.length - 1, tab);
                    tab.show();
                    tabs.setActiveTab(tabs.items.length - 2);
                }

                if (carregarMenu === true) {
                    SistemaOrigem = tela.SistemaOrigem;
                    layoutPrincipal.menuLateral.carregarMenuParaTela(codigoTela, SistemaOrigem);
                }
            },
            params: { codigoTela: codigoTela, sistemaOrigem: SistemaOrigem }
        });
    }
});