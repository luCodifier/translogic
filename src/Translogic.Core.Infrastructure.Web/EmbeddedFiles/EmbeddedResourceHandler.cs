namespace Translogic.Core.Infrastructure.Web.EmbeddedFiles
{
	using System;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Security.Permissions;
	using System.Text;
	using System.Web;

	using Remotion.Linq.Utilities;

	/// <summary>
	/// EmbeddedResource Handler
	/// </summary>
	[AspNetHostingPermission(SecurityAction.LinkDemand, Level = AspNetHostingPermissionLevel.Minimal)]
	public class EmbeddedFileHandler : IHttpHandler
	{
		/// <summary>
		/// Gets a value indicating whether IsReusable.
		/// </summary>
		public bool IsReusable
		{
			get { return true; }
		}

		/// <summary>
		/// Processa requisição
		/// </summary>
		/// <param name="context"> Http context. </param>
		public void ProcessRequest(HttpContext context)
		{
			// http://localhost:60672/{Translogic.Core}/teste/das.gif/GetObject.efh
			// string file = context.Request.RawUrl.Replace("/GetObject.efh", string.Empty).Substring(1);
			string file = context.Request.QueryString["file"];
			if (file.Contains(";}"))
			{
				file = file.Replace(";}", "}");
			}

			string fileName = file.Substring(file.LastIndexOf("/") + 1);

			string assemblyName = file.Substring(1, file.IndexOf("}") - 1);
			string filePath = string.Concat(assemblyName, ".EmbeddedFiles", file.Replace(assemblyName, string.Empty).Substring(2).Replace("/", "."));
			filePath = filePath.Replace(fileName, string.Empty).Replace("-", "_");
			filePath = string.Concat(filePath, fileName);

			Assembly assembly = Assembly.Load(assemblyName);
			string fileNameInAssembly = assembly.GetManifestResourceNames().Where(r => r.EndsWith(filePath, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
			if (fileNameInAssembly == null)
			{
				throw new ArgumentException("Nome do arquivo nao encontrado no assembly" + filePath);
			}

			Stream stream = assembly.GetManifestResourceStream(fileNameInAssembly);
			if (stream == null)
			{
				throw new ArgumentException("Arquivo nao encontrado: " + fileNameInAssembly);
			}

			var extensao = filePath.Substring(filePath.LastIndexOf(".") + 1);
			switch (extensao)
			{
				case "js":
					WriteFile("text/javascript", stream);
					break;
				case "css":
					WriteFile("text/css", stream);
					break;
				case "gif":
					WriteImage("image/gif", stream);
					break;
				case "png":
					WriteImage("image/png", stream);
					break;
				case "jpg":
				case "jpeg":
					WriteImage("image/jpg", stream);
					break;
			}
		}

		private void WriteFile(string responseType, Stream stream)
		{
			StringBuilder sb = new StringBuilder(4096);

			using (stream)
			{
				StreamReader reader = new StreamReader(stream);
				sb.Append(reader.ReadToEnd());
				reader.Close();
			}

			string data = sb.ToString();
			byte[] content = Encoding.UTF8.GetBytes(data);
			Send(content, responseType);
		}

		private void WriteImage(string responseType, Stream stream)
		{
			var length = Convert.ToInt32(stream.Length);
			var output = new byte[length];
			stream.Read(output, 0, length);
			Send(output, responseType);
		}

		private void Send(byte[] data, string responseType)
		{
			HttpResponse response = HttpContext.Current.Response;

			response.Charset = "utf-8";
			response.ContentType = responseType;
			response.BinaryWrite(data);	
		}
	}
}