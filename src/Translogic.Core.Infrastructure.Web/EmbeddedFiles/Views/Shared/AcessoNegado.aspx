<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AcessoNegado</title>
</head>
<body leftmargin=25>
<br>
<table cellspacing=0 cellpadding=0 border=0 width=100% style="border:1px; border-style: dashed; height:200px;">
	<tr align=center class=BodyEscuro>
		<td height=30 align=center colspan=2><font face=arial size=4 color=Red><b>ACESSO NEGADO<b></font></td>
	</tr>
	<tr><td colspan=2></td></tr>
	<tr><td colspan=2 align=center height=40 valign=bottom><span class="TITULO_TEXTO_geral" style="font-size:15px;"><b>TRANSLOGIC<b></span></td></tr>
	<tr><td colspan=2><img src="imagens/LinhaSubmenu.gif" width=100% height=1 color="#6E6E6E"></td></tr>
	<tr>
		<td><span class="TITULO_TEXTO_geral"><b>Data:</b></span></td>
		<td><span class="TITULO_TEXTO_geral"><%=DateTime.Now.ToShortDateString()%>&nbsp;�s&nbsp;<%=DateTime.Now.ToShortTimeString()%></span></td>
	</tr>
	<tr>
		<td><span class="TITULO_TEXTO_geral"><b>Usu�rio:</b></span></td>
		<td><span class="TITULO_TEXTO_geral"><%=ViewData["usuario"]%></span></td>
	</tr>
	<tr>
		<td><span class="TITULO_TEXTO_geral"><b>Transa��o:</b></span></td>
		<td><span class="TITULO_TEXTO_geral"><%=ViewData["transacao"]%></span></td>
	</tr>
	<tr height=80>
		<td valign=top width=20%><span class="TITULO_TEXTO_geral"><b>Descri��o:</b></span></td>
		<td valign=top>
			<span class="TITULO_TEXTO_geral">
			O usu�rio atual n�o tem acesso a esta transa��o.
			</span>
		</td>
	</tr>
	<tr><td height=2 colspan=2><img src="imagens/LinhaSubmenu.gif" width=100% height=1 color="#6E6E6E"></td></tr>
</table>
</body>
</html>
