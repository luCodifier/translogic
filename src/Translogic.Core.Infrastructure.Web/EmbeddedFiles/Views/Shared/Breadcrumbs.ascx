<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%
	var tela = ViewData["tela"] as Translogic.Modules.Core.Domain.Model.Acesso.Menu;
	
	if(tela == null)
	{
		throw new Exception("� necess�rio informar o c�digo da tela.");
	}

%> 

	<table width="100%">
		<tr>
			<td align="left">
				<div style="font-size: 18px; font-weight: bold">
				<%= tela.Titulo %>
				</div>
				<strong><%= Resources.Web.VoceEstaEm %> </strong>
				<%
					var breadCrumbs = tela.Breadcrumbs;

					for(var i = 0; i < breadCrumbs.Count - 1; i++)
					{
					%>
						<%= breadCrumbs[i].Titulo %><%= (i + 2 < breadCrumbs.Count) ? " > " : string.Empty %>
					<%
					}
				%>
			
			</td>
		</tr>
	</table>