<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Core.Infrastructure.Web" %>

<%
using (var combiner = new ResourceCombiner("main"))
{
	// CSS
	combiner.Add(Url.Javascript("ExtJs/resources/css/ext-all.css"));
	combiner.Add(Url.Javascript("ExtJs/resources/css/xtheme-gray-extend.css"));
    // combiner.Add(Url.Javascript("ExtJs/resources/css/xtheme-slickness.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/RowActions/Ext.ux.grid.RowActions.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GroupHeader/GroupHeaderPlugin.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Multiselect/Multiselect.css"));
	
	combiner.Add(Url.CSS("Common.css"));
	combiner.Add(Url.CSS("Translogic.css"));
	combiner.Add(Url.CSS("Icons.css"));
	combiner.Add(Url.CSS("Messages.css"));
	combiner.Add(Url.CSS("TipoVagao.css"));
	// Javascripts
	combiner.Add(Url.Javascript("jQuery/jquery-1.3.2.min.js"));
	combiner.Add(Url.Javascript("jQuery/Plugins/jquery.toJson.js"));
	combiner.Add(Url.Javascript("ExtJs/adapter/jquery/ext-jquery-adapter.js"));
	combiner.Add(Url.Javascript("ExtJs/ext-all-debug.js"));
	combiner.Add(Url.Javascript("ExtJs/ext.vtypes.js"));

	combiner.Add(Url.Javascript("ExtJs/Plugins/Tabs/TabCloseMenu.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Tabs/TabScrollerMenu.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/ManagedIFrame/miframe.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Menu/Ext.menu.ItemExtended.js"));

	combiner.Add(Url.Javascript("ExtJs/Plugins/Ext.ux.form.SearchField.js"));
	/*combiner.Add(Url.Javascript("ExtJs/Plugins/RowActions/Ext.ux.grid.RowActions.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GroupHeader/GroupHeaderPlugin.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Ext.ux.form.ServerValidator.js"));
	//
	combiner.Add(Url.Javascript("ExtJs/Plugins/Multiselect/Multiselect.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Multiselect/DDView.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Ext.DateTimeField.js"));*/

	// combiner.AddLocalizable(Url.Javascript("ExtJs/locale/ext-lang.js"));
	combiner.Add(Url.Javascript("ExtJs/locale/ext-lang.js"));
	
	combiner.Add(Url.Javascript("Translogic.PaginatedGrid.js"));
    combiner.Add(Url.Javascript("Translogic.PaginatedEditorGrid.js"));    
	combiner.Add(Url.Javascript("Translogic.MenuPrincipal.js"));
	combiner.Add(Url.Javascript("Translogic.MenuLateral.js"));
	combiner.Add(Url.Javascript("Translogic.LayoutPrincipal.js"));
	combiner.Add(Url.Javascript("Translogic.ConteudoPrincipal.js"));
	// combiner.Add(Url.Javascript("Translogic.StatusBar.js"));
	combiner.Add(Url.Javascript("Translogic.MenuUsuario.js"));
	combiner.Add(Url.Javascript("Translogic.ExtForms.js"));
    combiner.Add(Url.Javascript("Translogic.LinkButton.js"));
	combiner.Add(Url.Javascript("Translogic.js"));

} %>