<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Translogic.Modules.Core.Domain.Model.Acesso" %>
<%@ Import Namespace="Translogic.Modules.Core.App_GlobalResources" %>
<%@ Import Namespace="Translogic.Core.Infrastructure.Web" %>


    <script type="text/javascript">
		var App = {
			User : {
				Name : '<%: Page.User.Identity.Name %>'
				<%
					UsuarioIdentity usuarioIdentity = Context.User.Identity as UsuarioIdentity;
					if(usuarioIdentity != null)
					{
						%>
						,LastLogin : '<%= usuarioIdentity.Usuario.UltimoAcesso %>'
						<%
					}
				%>
			},
			Paths : {
				Root: '<%= Url.Root() %>',
				Logout: '<%= Url.Logout() %>',
				Legado: '<%= Url.Legado() %>',
				Route : function(modulo, controller, action, querystring)
				{
					var url = "/Modules/" + modulo + "/" + controller + "/" + action + ".all";
				
					if(App.Paths.Root != "/")
					{
						url = App.Paths.Root + url;
					}
					
					url = url.replace("\/\/", "\/");
					
					if(typeof querystring === "string")
					{
						url += "?" + querystring;
					}
					else if(typeof querystring === "object")
					{
						var strQs = "";
						
						for(var e in querystring)
						{
							strQs += e + "=" + querystring[e] + "&";
						}
						
						url += "?" + strQs;
					}

					return url;
				}
			},
			Context : {
				Now: '<%= DateTime.Now %>',
				ServerAddress : '<% = Request.ServerVariables["LOCAL_ADDR"] %>',
				SetLegadoAddress : function(legado){
				
					if(legado.length > 20)
					{
						return;
					}
				
					App.Context.LegadoAddress = legado;
				},
				LegadoAddress : '',
				Culture : {
						<%var currentCulture = Thread.CurrentThread.CurrentUICulture; %>
						Name: '<%= currentCulture.Name %>',
						DisplayName: '<%= currentCulture.DisplayName %>',
						LCID:  <%= currentCulture.LCID %>
				},
				Cultures: [
				<%
				for(var i = 0; i < Cultura.Todas.Length; i++){
					var cultura = Cultura.Todas[i];
				%>
					{
						Name: '<%= cultura.Name %>',
						DisplayName: '<%= cultura.DisplayName %>',
						LCID:  <%= cultura.LCID %>
					}<%if(i + 1 < Cultura.Todas.Length){%>,<%} %>
				<%}%>
				],
                Theme: {
						<%string codigoTemaAtual = (string) Session["TEMA_TELA"];
                          var temaAtual = string.IsNullOrEmpty(codigoTemaAtual) ? Tema.Default : new Tema(codigoTemaAtual);%>
						Codigo: '<%= temaAtual.Codigo %>',
						DisplayName: '<%= temaAtual.DisplayName %>',
                        Css: '<%= temaAtual.Css %>'
				},
				Themes: [
				<%
				for(var i = 0; i < Tema.Todos.Length; i++){
					var tema = Tema.Todos[i];
				%>
					{
						Codigo: '<%= tema.Codigo %>',
						DisplayName: '<%= tema.DisplayName %>'
					}<%if(i + 1 < Tema.Todos.Length){%>,<%} %>
				<%}%>
				]
			},
			Resources : {
				Web : {
					UsuarioNaoLogado: '<%= Web.UsuarioNaoLogado %>',
					TituloErro: '<%= Web.TituloErro %>',
					FalhaAjax: '<%= Web.FalhaAjax %>',
					PesquisaDeTela: '<%= Web.PesquisaDeTela %>',
					Pesquisando: '<%= Web.Pesquisando %>',
					Menu: '<%= Web.Menu %>',
					Tela: '<%= Web.Tela %>',
					MeuTranslogic: '<%= Web.MeuTranslogic %>',
					Sair: '<%= Web.Sair %>',
					Carregando: '<%= Web.Carregando %>',
					GridExibindoRegistros: '<%= Web.GridExibindoRegistros %>',
					GridSemRegistros: '<%= Web.GridSemRegistros %>',
					Usuario: '<%= Web.Usuario %>',
					Servidor: '<%= Web.Servidor %>',
					UltimoLoginEm: '<%= Web.UltimoLoginEm %>',
					AbrirEmNovaAba: '<%= Web.AbrirEmNovaAba %>',
					AbrirUmaAba: '<%= Web.AbrirUmaAba %>',
					SalvarMeuTranslogic: '<%= Web.SalvarMeuTranslogic %>',
					SessaoExpiraEm: '<%= Web.SessaoExpiraEm %>',
					ExcluirMeuTranslogic: '<%= Web.ExcluirMeuTranslogic %>',
					DigiteNumeroTelaPressioneAlt_a: "<%= Web.DigiteNumeroTelaPressioneAlt_a %>",
					Mensagem: "<%= Web.Mensagem %>",
					MensagemEnviadaSucesso: "<%= Web.MensagemEnviadaSucesso %>",
					Assunto: "<%= Web.Assunto %>",
					DesejaSair: "<%= Web.DesejaSair %>",
					SessaoExpiradaPorTempo: "<%= Web.SessaoExpiradaPorTempo %>",
					OperacaoNaoPermitida: "<%= Web.OperacaoNaoPermitida %>"
				}
			}
		}
    </script>
    