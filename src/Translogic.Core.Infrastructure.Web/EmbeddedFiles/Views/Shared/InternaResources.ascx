<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Translogic.Core.Infrastructure.Web" %>
<%
using (var combiner = new ResourceCombiner("interna"))
{
	// CSS
	combiner.Add(Url.Javascript("ExtJs/resources/css/ext-all.css"));
	combiner.Add(Url.Javascript("ExtJs/resources/css/xtheme-gray-extend.css"));
    // combiner.Add(Url.Javascript("ExtJs/resources/css/xtheme-slickness.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/RowActions/Ext.ux.grid.RowActions.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GroupHeader/GroupHeaderPlugin.css"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GridSummary/Ext.ux.grid.GridSummary.css"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/GroupSummary/GroupSummary.css"));
    combiner.Add(Url.Javascript("ExtJs/resources/css/Ext.ux.form.SuperBoxSelect.css"));
    combiner.Add(Url.Javascript("ExtJs/resources/css/Ext.ux.Andrie.Select.css"));
    combiner.Add(Url.Javascript("ExtJs/resources/css/Fileuploadfield.css"));
   
	combiner.Add(Url.CSS("Common.css"));
	combiner.Add(Url.CSS("Translogic.css"));
	combiner.Add(Url.CSS("Icons.css"));
	combiner.Add(Url.CSS("Messages.css"));
    combiner.Add(Url.CSS("TipoVagao.css"));
	// Javascripts
	combiner.Add(Url.Javascript("jQuery/jquery-1.3.2.min.js"));
	combiner.Add(Url.Javascript("jQuery/Plugins/jquery.toJson.js"));
	combiner.Add(Url.Javascript("ExtJs/adapter/jquery/ext-jquery-adapter.js"));
	combiner.Add(Url.Javascript("ExtJs/ext-all.js"));
	combiner.Add(Url.Javascript("ExtJs/ext.vtypes.js"));

	combiner.Add(Url.Javascript("ExtJs/Plugins/Tabs/TabCloseMenu.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Tabs/TabScrollerMenu.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/ManagedIFrame/miframe.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Menu/Ext.menu.ItemExtended.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/InputTextMask/InputTextMask.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/TextMask/Ext.ux.TextMask.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/MaskFormatter/MaskFormatter.js"));
	
	combiner.Add(Url.Javascript("ExtJs/Plugins/RowActions/Ext.ux.grid.RowActions.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GroupHeader/GroupHeaderPlugin.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/GridSummary/Ext.ux.grid.GridSummary.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/GroupSummary/GroupSummary.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/MultGroupingGrid/MultiGrouping.js"));
	
	//combiner.Add(Url.Javascript("ExtJs/Plugins/Multiselect/Multiselect.js"));
	//combiner.Add(Url.Javascript("ExtJs/Plugins/Multiselect/DDView.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/Ext.DateTimeField.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/ComboMultiSelect/Ext.ux.form.SuperBoxSelect.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/ComboMultiSelect/Ext.ux.Andrie.Select.js"));
	combiner.Add(Url.Javascript("ExtJs/Plugins/CheckColumn/CheckColumn.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/Fields/FileUploadField.js"));
    combiner.Add(Url.Javascript("ExtJs/Plugins/Fields/MoneyField.js"));
     

	combiner.Add(Url.Javascript("ExtJs/locale/ext-lang.js"));
	
	combiner.Add(Url.Javascript("Translogic.PaginatedGrid.js"));
    combiner.Add(Url.Javascript("Translogic.PaginatedEditorGrid.js"));    
	combiner.Add(Url.Javascript("Translogic.ExtForms.js"));
    combiner.Add(Url.Javascript("Translogic.LinkButton.js"));
	combiner.Add(Url.Javascript("Interna.Translogic.js"));

    
} %>
