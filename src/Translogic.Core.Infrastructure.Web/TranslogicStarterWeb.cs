namespace Translogic.Core.Infrastructure.Web
{
	using System.Web.Mvc;
	using Castle.MicroKernel.Registration;
	using HibernatingRhinos.NHibernate.Profiler.Appender;
	using Infrastructure.Modules.Steps;
	using Infrastructure.Settings;
	using Infrastructure.Validation;
	using Infrastructure.Web.ControllerFactories;
	using NHibernate.Validator.Cfg.Loquacious;
	using NHibernate.Validator.Engine;

	using Translogic.Core.Infrastructure.Web.Services;

	/// <summary>
	/// Classe que tem defini��es de inicializa��o da aplica��o
	/// </summary>
	public class TranslogicStarterWeb : TranslogicStarter
	{
		#region ATRIBUTOS READONLY & EST�TICOS


		#endregion

		#region M�TODOS EST�TICOS
		/// <summary>
		/// Executa m�todo de configura��o para projetos tipo Web
		/// </summary>
		public static void Setup()
		{
			BaseSetup();

			Container.Register(
				Component.For<IJsonSerializer>().ImplementedBy<JavascriptJsonSerializer>(),
				Component.For<WebAuthenticationService>(),
				Component.For<IControllerFactory>().ImplementedBy<WindsorControllerFactory>(),
				Component.For<IModuleSetupStep>().ImplementedBy<RoutesSetupStep>().Named("routes.step"),
				Component.For<ServerSettings>().Named("server.settings")
			);

			RunModulesSetup();
			SetupValidation();

			SetupDone = true;
		}
		#endregion
	}
}