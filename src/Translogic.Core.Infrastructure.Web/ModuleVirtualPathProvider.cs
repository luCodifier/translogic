namespace Translogic.Core.Infrastructure.Web
{
	using System;
	using System.Collections;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Web;
	using System.Web.Caching;
	using System.Web.Hosting;
	using System.Web.Mvc;
	using Castle.Core.Logging;
	using Microsoft.Practices.ServiceLocation;

	/// <summary>
	/// Classe responsável por carregar as views do Assembly (modo Release) 
	/// e do diretório de desenvolvimento (modo debug)
	/// </summary>
	public class ModuleVirtualPathProvider : VirtualPathProvider
	{
		#region MÉTODOS
		/// <summary>
		/// Verifica se o arquivo existe
		/// </summary>
		/// <param name="virtualPath">Caminho do arquivo</param>
		/// <returns>Valor booleano</returns>
		public override bool FileExists(string virtualPath)
		{
			return base.FileExists(virtualPath) || IsAppResourcePath(virtualPath);
		}

		/// <summary>
		/// Retorna a dependencia do cache
		/// </summary>
		/// <param name="virtualPath">Caminho do arquivo de cache</param>
		/// <param name="virtualPathDependencies">Dependencias do caminho</param>
		/// <param name="utcStart">Data do cache</param>
		/// <returns>Objeto de dependencia do cache - <see cref="CacheDependency"/></returns>
		public override CacheDependency GetCacheDependency(
			string virtualPath,
			IEnumerable virtualPathDependencies,
			DateTime utcStart)
		{
			if (IsAppResourcePath(virtualPath))
			{
				return null;
			}

			return base.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
		}

		/// <summary>
		/// Retorna o arquivo virtual
		/// </summary>
		/// <param name="virtualPath">Caminho virtual</param>
		/// <returns>Arquivo virtual - <see cref="VirtualFile"/></returns>
		public override VirtualFile GetFile(string virtualPath)
		{
			ControllerContext context = GetControllerContext();

			if (!base.FileExists(virtualPath))
			{
				return new AssemblyResourceFile(virtualPath, context);
			}

			return base.GetFile(virtualPath);
		}

		/// <summary>
		/// Retorna o hash do arquivo
		/// </summary>
		/// <param name="virtualPath">Caminho virtual</param>
		/// <param name="virtualPathDependencies">Dependencias do caminho virtual</param>
		/// <returns>String do hash</returns>
		public override string GetFileHash(string virtualPath, IEnumerable virtualPathDependencies)
		{
			ControllerContext controllerContext = GetControllerContext();
			if (controllerContext != null)
			{
				if (!AssemblyResourceFile.IsAssemblyDebugBuild(controllerContext.Controller.GetType().Assembly))
				{
					return base.GetFileHash(virtualPath, virtualPathDependencies);
				}
			}
			else
			{
#if !DEBUG
			return base.GetFileHash(virtualPath, virtualPathDependencies);
#endif
			}

			if (!IsAppResourcePath(virtualPath))
			{
				return virtualPath;
			}

			string sourceViewPath = AssemblyResourceFile.GetSourceViewPath(GetControllerContext(), virtualPath);

			if (!File.Exists(sourceViewPath))
			{
				return base.GetFileHash(virtualPath, virtualPathDependencies);
			}

			return (File.GetLastWriteTimeUtc(sourceViewPath).Ticks * 37).ToString();
		}

		private ControllerContext GetControllerContext()
		{
			return HttpContext.Current.Items["ControllerContext"] as ControllerContext;
		}

		private bool IsAppResourcePath(string virtualPath)
		{
			return AssemblyResourceFile.FileExists(virtualPath, GetControllerContext());
		}

		#endregion

		#region TIPO ANINHADO: AssemblyResourceFile
		/// <summary>
		/// Arquivo de recurso de um assembly
		/// </summary>
		private class AssemblyResourceFile : VirtualFile
		{
			#region ATRIBUTOS READONLY & ESTÁTICOS

			private readonly string _assemblyPath;

			#endregion

			#region ATRIBUTOS

			private readonly ControllerContext _context;
			private readonly ILogger _logger = ServiceLocator.Current.GetInstance<ILogger>();

			#endregion

			#region CONSTRUTORES
			/// <summary>
			/// Inicializa uma nova instancia da classe
			/// </summary>
			/// <param name="virtualPath">Caminho virtual</param>
			/// <param name="context">Contexto do controller</param>
			public AssemblyResourceFile(string virtualPath, ControllerContext context) :
				base(virtualPath)
			{
				_assemblyPath = VirtualPathUtility.ToAppRelative(virtualPath);
				_context = context;
			}

			#endregion

			#region MÉTODOS
			/// <summary>
			/// Retorna o caminho o assembly
			/// </summary>
			/// <param name="context"> Contexto do controller. </param>
			/// <param name="assemblyPath"> Caminho do assembly. </param>
			/// <returns>String do caminho do arquivo </returns>
			public static string GetSourceViewPath(ControllerContext context, string assemblyPath)
			{
				if (context == null)
				{
					return assemblyPath.Replace("~", string.Empty).Replace('/', '\\');
				}

				var moduleController = context.Controller as BaseModuleController;

				string absolute = VirtualPathUtility.ToAbsolute("~");

				if (!absolute.Equals("/") && assemblyPath.StartsWith(absolute))
				{
					assemblyPath = assemblyPath.Substring(absolute.Length, assemblyPath.Length - absolute.Length);
				}

				return moduleController.ModuleSourcePath + assemblyPath.Replace("~", string.Empty).Replace('/', '\\');
			}

			/// <summary>
			/// Verifica se o arquivo existe
			/// </summary>
			/// <param name="path">Path a ser verificado</param>
			/// <param name="context">Contexto do controller</param>
			/// <returns>Valor booleano</returns>
			public static bool FileExists(string path, ControllerContext context)
			{
				if (context != null)
				{
					return FileExistsFromSource(path, context) || FileExistsFromAssembly(path, context) || FileExistsFromBaseAssembly(path);
				}

				return FileExistsFromBaseAssembly(path);
			}

			/// <summary>
			/// Verifica se o assembly foi compilado em Debug
			/// </summary>
			/// <param name="assembly"> Assembly a ser verificado. </param>
			/// <returns> Valor booleano </returns>
			public static bool IsAssemblyDebugBuild(Assembly assembly)
			{
				return assembly.GetCustomAttributes(false).Any(x =>
				(x as DebuggableAttribute) != null ?
				(x as DebuggableAttribute).IsJITTrackingEnabled : false);
			}

			/// <summary>
			/// Abre o arquivo
			/// </summary>
			/// <returns>Retorna o Stream do arquivo</returns>
			public override Stream Open()
			{
				Stream stream = null;
				if (_context != null && FileExistsFromSource(_assemblyPath, _context))
				{
					stream = OpenFromSource(_context, _assemblyPath);
				}

				return stream ?? (OpenFromAssembly(_context, _assemblyPath) ?? OpenFromBaseAssembly(_assemblyPath));
			}

			private static bool FileExistsFromSource(string path, ControllerContext context)
			{
				string viewPath = GetSourceViewPath(context, path);
				return File.Exists(viewPath);
			}

			private static bool FileExistsFromAssembly(string path, ControllerContext context)
			{
				Assembly assembly = context.Controller.GetType().Assembly;

				string resourceName = path.Replace('/', '.').Replace("~", string.Empty);

				string resourceAssemblyName = assembly.GetManifestResourceNames().
					Where(r => r.EndsWith(resourceName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

				if (string.IsNullOrEmpty(resourceAssemblyName))
				{
					return false;
				}

				Stream resourceStream = assembly.GetManifestResourceStream(resourceAssemblyName);
				return resourceStream != null;
			}

			private static bool FileExistsFromBaseAssembly(string path)
			{
				Assembly assembly = Assembly.GetAssembly(typeof(ModuleVirtualPathProvider));

				string resourceName = path.Replace('/', '.').Replace("~", string.Empty);

				string resourceAssemblyName = assembly.GetManifestResourceNames().
					Where(r => r.EndsWith(resourceName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

				if (string.IsNullOrEmpty(resourceAssemblyName))
				{
					return false;
				}

				Stream resourceStream = assembly.GetManifestResourceStream(resourceAssemblyName);
				return resourceStream != null;
			}

			private Stream OpenFromAssembly(ControllerContext context, string assemblyPath)
			{
				if (context != null)
				{
					Assembly assembly = context.Controller.GetType().Assembly;
					Stream stream = OpenFromAssembly(assembly, assemblyPath);
					if (stream != null)
					{
						return stream;
					}
				}

				return null; // OpenFromBaseAssembly(assemblyPath);
			}

			private Stream OpenFromBaseAssembly(string assemblyPath)
			{
				Assembly assembly = Assembly.GetAssembly(typeof(ModuleVirtualPathProvider));
				return OpenFromAssembly(assembly, assemblyPath);
			}

			private Stream OpenFromAssembly(Assembly assembly, string assemblyPath)
			{
				string resourceName = assemblyPath.Replace('/', '.').Replace("~", string.Empty);
				string resourceAssemblyName = assembly.GetManifestResourceNames().
					Where(r => r.EndsWith(resourceName, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

				if (string.IsNullOrEmpty(resourceAssemblyName))
				{
					return null;
				}

				return assembly.GetManifestResourceStream(resourceAssemblyName);
			}

			private Stream OpenFromSource(ControllerContext context, string assemblyPath)
			{
				if (context != null)
				{
					string viewPath = GetSourceViewPath(context, assemblyPath);

					if (File.Exists(viewPath))
					{
						return File.OpenRead(viewPath);
					}
				}

				return null; // OpenFromBaseAssembly();
			}
			#endregion
		}

		#endregion
	}
}