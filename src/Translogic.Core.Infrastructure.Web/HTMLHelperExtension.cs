namespace Translogic.Core.Infrastructure.Web
{
	using System.Web.Mvc;

	/// <summary>
	/// Extens�o de Helper de html
	/// </summary>
	public static class HTMLHelperExtension
	{
		#region M�TODOS EST�TICOS

		/// <summary>
		/// String de include de um arquivo javascript
		/// </summary>
		/// <param name="helper">Helper da extens�o</param>
		/// <param name="fileName">Caminho do arquivo javascript</param>
		/// <returns>String de html para incluir javascript</returns>
		public static string ScriptInclude(this HtmlHelper helper, string fileName)
		{
			return string.Format("<script type=\"text/javascript\" src=\"{0}\" ></script>", fileName);
		}

		/// <summary>
		/// String de include de um arquivo css
		/// </summary>
		/// <param name="helper">Helper da Extens�o</param>
		/// <param name="fileName">Caminho do arquivo css</param>
		/// <returns>String html para incluir css</returns>
		public static string Stylesheet(this HtmlHelper helper, string fileName)
		{
			return string.Format("<link type=\"text/css\" rel=\"stylesheet\" href=\"{0}\" />", fileName);
		}

		#endregion
	}
}