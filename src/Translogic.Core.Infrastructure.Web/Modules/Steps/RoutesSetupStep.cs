namespace Translogic.Core.Infrastructure.Modules.Steps
{
	using System.Web.Mvc;
	using System.Web.Routing;
	using Web;

    /// <summary>
    /// Implementa��o de "Passo" de configura��o de rotas
    /// </summary>
	public class RoutesSetupStep : IModuleSetupStep
	{
		#region IModuleSetupStep MEMBROS

		/// <summary>
		/// Executa a insta��o do m�dulo
		/// </summary>
		/// <param name="module">M�dulo a ser instalado</param>
		public void Execute(IModuleInstaller module)
		{
			RouteTable.Routes.CreateModule(
				module.Name,
				module.GetType().Assembly.GetName().Name + ".Controllers",
				RouteTable.Routes.MapRoute(null, "Modules/" + module.Name + "/{controller}.all/{action}"),
				RouteTable.Routes.MapRoute(null, "Modules/" + module.Name + "/{controller}/{action}.all"),
				RouteTable.Routes.MapRoute(null, "Modules/" + module.Name + "/{controller}/{action}"),
				RouteTable.Routes.MapRoute(null, "Modules/" + module.Name + "/{controller}/{action}/{id}")
				);

			module.RegisterRoutes(RouteTable.Routes);
		}

		#endregion
	}
}