﻿namespace Translogic.JobRunner
{
	using System;

	using Topshelf;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe que inicia o programa
    /// </summary>
    public static class Program
	{
		#region MÉTODOS ESTÁTICOS

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		public static void Main(string[] args)
		{
			try
			{
				Sis.Inicializar(EnumSistema.JobRunner, "Job: " + string.Join(", ", args));
			}
			catch (Exception ex)
			{
				Sis.LogException(ex, "Sis.Inicializar");
			}

            var m = new Translogic.Modules.EDI.Domain.Models.Edi.ConfiguracaoEmpresaEdi();

            HostFactory.Run(x =>
			{
				x.Service<JobRunnerSetupService>(s =>
				{
					s.ConstructUsing(name => new JobRunnerSetupService());
					s.WhenStarted(tc => tc.Start());
					s.WhenStopped(tc => tc.Stop());
				});

#if DEBUG2
                x.RunAsPrompt();
#else
                x.RunAsLocalSystem();
#endif

                x.SetDescription("Serviço de JobRunner");
				x.SetDisplayName("Translogic.JobRunner");
				x.SetServiceName("Translogic.JobRunner");
			}); 
		}

#endregion
	}
}