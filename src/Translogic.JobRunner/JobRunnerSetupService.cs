﻿namespace Translogic.JobRunner
{
    using System;
    using System.Diagnostics;
    using System.ServiceModel;
    using Castle.Facilities.WcfIntegration;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.EDI.Domain.DataAccess.Repositories.Edi;
    using Translogic.Modules.EDI.Domain.Models.Edi.Repositories;

    /// <summary>
    /// Serviço de configuração do jobrunner
    /// </summary>
    public class JobRunnerSetupService
    {
        ServiceHostBase _jobRunnerHost;
        private DefaultServiceHostFactory _hostFactory;

        /// <summary>
        /// Método chamado na inicialização do serviço
        /// </summary>
        public void Start()
        {
            try
            {
                TranslogicStarter.Initialize();
                TranslogicStarter.SetupForJobRunner();

                TranslogicContainer container = TranslogicStarter.Container;
                RegistrarComponentes(TranslogicStarter.Container);

                _hostFactory = new DefaultServiceHostFactory(TranslogicStarter.Container.Kernel);

                AbrirServico();
            }
            catch (Exception ex)
            {
                Sis.LogException(ex);
            }
        }

        /// <summary>
        /// Método chamado no término do serviço
        /// </summary>
        public void Stop()
        {
            GarantirServicoFechado();

            TranslogicStarter.Container.Dispose();
        }

        private void RegistrarComponentes(TranslogicContainer container)
        {
            try
            {
                container.Register(Component.For<IConfiguracaoEmpresaEdiRepository>().ImplementedBy<ConfiguracaoEmpresaEdiRepository>());
            }
            catch { }
        }

        private void AbrirServico()
        {
            GarantirServicoFechado();

            _jobRunnerHost = _hostFactory.CreateServiceHost("core.jobrunnerservice", new Uri[0]);
            _jobRunnerHost.Open();
        }

        private void GarantirServicoFechado()
        {
            if (_jobRunnerHost != null)
            {
                _jobRunnerHost.Close();
            }
        }
    }
}