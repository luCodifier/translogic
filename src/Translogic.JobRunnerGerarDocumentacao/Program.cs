﻿namespace Translogic.JobRunnerGerarDocumentacao
{
    using Speed.Common;
    using System;
    using System.Linq;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.Core.Jobs.JobRunner;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe que inicia o programa
    /// </summary>
    public static class Program
    {

        const string key = "core.job.GerenciamentoDocumentacao";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static int Main(string[] args) 
        {
            return JobRunnerProgram.Execute(EnumSistema.JobRunnerGerarDocumentacao, key, args);
        }
    }
}


