﻿namespace Translogic.Web
{
	using System;
	using System.Web.UI;

	/// <summary>
	/// Página padrão que irá redirecionar para a primeira pagina do sistema
	/// </summary>
	public class _Default : Page
	{
		#region MÉTODOS
		/// <summary>
		/// Evento de load da pagina
		/// </summary>
		/// <param name="sender">Objeto sender</param>
		/// <param name="e">Argumentos do evento</param>
		public void Page_Load(object sender, EventArgs e)
		{
			Response.Redirect("~/home.all");

			// HttpContext.Current.RewritePath(Request.ApplicationPath, false);
			// IHttpHandler httpHandler = new MvcHttpHandler();
			// httpHandler.ProcessRequest(HttpContext.Current);
		}

		#endregion
	}
}