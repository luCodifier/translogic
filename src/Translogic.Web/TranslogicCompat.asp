﻿<!-- #include file="translogicII/xmlBib.asp" -->
<%
Sub HCTI_Setup(ByVal Language)

	Session("Idioma") = language
	Session("Linguagem") = Left(Ucase(language),2)

	Select Case Language
		Case "pt-BR"
			Session.LCID = 1046
		Case "es-ES"
			Session.LCID = 11274
		Case "es-AR"
			Session.LCID = 11274
		Case "en-US"
			Session.LCID = 1033
	End Select
	
End Sub

'on error resume next
dim strToken, xmlEntrada, xmlSaida, codErro
strToken = Request("token")

xmlEntrada = "<XML><TOKEN>"& strToken &"</TOKEN></XML>"

set oControleAcesso = Server.CreateObject("ControleAcesso.IServControleAcesso")

if err.number<>0 then
	response.write "Erro: " & err.description
end if

oControleAcesso.recDadosToken xmlEntrada, xmlSaida, codErro
if err.number<>0 then
	response.write "Erro: " & err.description
end if

if codErro<>"" then
	response.write "Erro: " & codErro
end if
'response.write server.htmlEncode(xmlSaida)

criaDom	  xmlSaida, domLista
set regs  = domLista.selectNodes("//REGISTRO")
if regs.length>0 then

	Dim idioma
	
	idioma = regs(0).getAttribute("IDIOMA")
	
	Session("Auth")		= "S"

	call HCTI_Setup(idioma)

	Session("UserID")   = regs(0).getAttribute("USUARIO")
	Session("password")	= regs(0).getAttribute("SENHA")
	Session("TranslogicIII") = "S"

	response.write "ok - "  & Session("UserId")
else
	response.write "nok"
end if

%>