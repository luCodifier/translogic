﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Translogic.Modules.Core.Spd
{

    /// <summary>
    /// Classe de validações
    /// </summary>
    public static class Validators
    {

        static Regex ValidEmailRegex = CreateValidEmailRegex();

        /// <summary>
        /// Taken from http://haacked.com/archive/2007/08/21/i-knew-how-to-validate-an-email-address-until-i.aspx
        /// </summary>
        /// <returns></returns>
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^([\w\-\.]+)@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([\w\-]+\.)+)([a-zA-Z]{2,4}))$";

            // string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
            //    + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
            //    + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Checa se um e-mail é válido
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool EmailIsValid(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress) || !emailAddress.Contains('@'))
                return false;

            try
            {
                bool isValid = ValidEmailRegex.IsMatch(emailAddress);

                return isValid;
            }
            catch
            {
                return false;
            }
        }
    }
}
