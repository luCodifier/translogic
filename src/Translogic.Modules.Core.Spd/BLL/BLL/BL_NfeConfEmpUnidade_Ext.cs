using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_NfeConfEmpUnidade
    {

        public static NfeConfEmpUnidade ObterPorCnpj(string cnpj)
        {
            return SelectSingle(new NfeConfEmpUnidade { Cnpj = cnpj });
        }

        public static NfeConfEmpUnidade ObterPorCnpj(Database db, string cnpj)
        {
            return SelectSingle(db, new NfeConfEmpUnidade { Cnpj = cnpj });
        }

    }

}
