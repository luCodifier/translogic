using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Speed;
using System.Linq;
using Translogic.Modules.Core.Spd.Data;
using Speed.Common;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_ItemDespacho
    {

        public static List<ItemDespacho> SelectPorDespachos(Database db, List<Despacho> despachos)
        {
            return SelectPorDespachos(db, despachos.Select(p => p.IdDespacho));
        }

        public static List<ItemDespacho> SelectPorDespachos(Database db, IEnumerable<decimal?> FxIdFlxs)
        {
            string where = string.Format("DP_ID_DP in ({0})", Conv.GetIn(FxIdFlxs));
            return Select(db, where);
        }

        public static List<ItemDespacho> ObterDespachosPorItensDespacho(int[] itensDespachoId)
        {
            var ids = string.Join(",", itensDespachoId);
            return Select(string.Format("ID_ID_ITD in ({0})", ids));
        }

    }

}
