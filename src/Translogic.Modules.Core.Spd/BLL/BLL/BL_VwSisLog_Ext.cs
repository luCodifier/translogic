using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwSisLog
    {

        public static List<VwSisLog> SelectRelatorio(int? inicio, int? limite, string ordem, DateTime? dataIni, DateTime? dataFim, string mensagem, string exception, string sistema, string tipo)
        {
            if (dataIni == null)
                dataIni = DateTime.Today;
            if (dataFim == null)
                dataFim = DateTime.Today;
            if (inicio == null)
                inicio = 0;

            dataIni = dataIni.Value.Date;
            dataFim = dataFim.Value.Date.AddDays(1).AddMilliseconds(-1);

            limite = 50;

            string sort;
            if (ordem == null)
                ordem = "DATA DESC";

            var filters = new List<string>();

            filters.Add($"DATA BETWEEN {dataIni.OracleDate()} AND {dataFim.OracleDate()}");
            
            if (!string.IsNullOrWhiteSpace(mensagem))
                filters.Add($"MENSAGEM like '%{mensagem}%'");
            
            if (!string.IsNullOrWhiteSpace(exception))
                filters.Add($"EXCEPTION like '%{exception}%'");
            
            if (!string.IsNullOrWhiteSpace(sistema))
                filters.Add($"SISTEMA_NOME = '{sistema}'");
            
            if (!string.IsNullOrWhiteSpace(tipo))
                filters.Add($"TIPO_NOME = '{tipo}'");

            string where = string.Join(" AND ", filters);

            return BL_VwSisLog.SelectPage(where, inicio.Value, limite.Value, ordem, 300);
        }

    }

}
