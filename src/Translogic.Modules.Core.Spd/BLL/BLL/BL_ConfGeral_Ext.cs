using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_ConfGeral
    {

        public static ConfGeralInfo GetInfo()
        {
            return Sys.RunInDb((db) => GetInfo(db));
        }

        public static ConfGeralInfo GetInfo(Database dbT)
        {
            ConfGeralInfo info = new ConfGeralInfo();

            //var x = generateInfo(dbT);

            var confs = BL_ConfGeral.Select(dbT);

            var props = info.GetType().GetProperties().GroupByToDictionaryDistinct(p => p.Name, StringComparer.OrdinalIgnoreCase);

            foreach (var conf in confs)
            {
                string propName = conf.Chave.ToPascalCase();
                var prop = props.GetValue(propName);
                if (prop != null)
                    prop.SetValue(info, conf.Valor, null);
            }

            return info;

        }

        /// <summary>
        /// Gera as propriedades da classe ConfGeralInfo
        /// </summary>
        /// <returns></returns>
        public static string GenerateClass()
        {
            var b = new StringBuilder();
            var confs = Select().OrderBy(p => p.Chave.ToLower());
            foreach (var conf in confs)
            {
                var desc = conf.Descricao == null ? "" : conf.Descricao.Replace("\r", " ").Replace("\n", " ");
                b.AppendLine("/// <summary>");
                b.AppendLine("/// " + conf.Chave + " - " + desc);
                b.AppendLine("/// </summary>");
                b.AppendLine("public string " + conf.Chave.ToPascalCase() + " {get; set;}");
            }
            return b.ToString();
        }

        //public static ConfGeral GetPorChave(string chave, string valorDefault = null, string descricao = null)
        //{
        //    var ret = SelectByPk(chave);
        //    if (ret == null && valorDefault != null && descricao != null)
        //    {
        //        ret = new ConfGeral { Chave = chave, Valor = valorDefault, Descricao = descricao };
        //        Insert(ret, EnumSaveMode.Requery);
        //    }
        //    return ret;
        //}

    }

}
