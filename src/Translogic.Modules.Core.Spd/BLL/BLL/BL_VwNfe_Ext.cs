using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfe
    {
        public static VwNfe ObterPorChaveNfe(string chaveNfe, string tipo = null)
        {
            return SelectSingle(new VwNfe { ChaveNfe = chaveNfe, Tipo = tipo });
        }
        public static VwNfe ObterPorChaveNfe(Database db, string chaveNfe, string tipo = null)
        {
            return SelectSingle(db, new VwNfe { ChaveNfe = chaveNfe, Tipo = tipo });
        }

        public static List<VwNfe> ObterPorChaveNfes(string chavesNfe)
        {
            var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
            return Select("NFE_CHAVE IN (" + chaves + ")");
        }

        public static List<VwNfe> ObterPorChaveNfes(List<string> chavesNfe)
        {
            var chaves = string.Join(",", chavesNfe.Select(p => "'" + p + "'"));
            return Select("NFE_CHAVE IN (" + chaves + ")");
        }

    }

}
