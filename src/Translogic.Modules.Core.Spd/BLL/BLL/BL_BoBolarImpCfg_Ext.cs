using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_BoBolarImpCfg
    {

        public static BoBolarImpCfg ObterPorCnpjAtivo(string cnpj)
        {
            return SelectSingle(new BoBolarImpCfg { Cnpj = cnpj, IndAtivo = "S" });
        }

        public static BoBolarImpCfg ObterPorCnpjAtivo(Database db, string cnpj)
        {
            return SelectSingle(db, new BoBolarImpCfg { Cnpj = cnpj, IndAtivo = "S" });
        }

    }

}
