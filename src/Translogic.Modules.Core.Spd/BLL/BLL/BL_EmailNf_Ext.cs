using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_EmailNf
    {

        public static EmailNf ObterPorChaveNfe(string chave)
        {
            return SelectSingle(new EmailNf { ChaveAcesso = chave });
        }

    }

}
