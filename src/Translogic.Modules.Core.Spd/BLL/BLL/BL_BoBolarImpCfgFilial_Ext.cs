using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_BoBolarImpCfgFilial
    {

        public static BoBolarImpCfgFilial ObterPorCnpj(string cnpj)
        {
            return SelectSingle(new BoBolarImpCfgFilial { Cnpj = cnpj });
        }

        public static BoBolarImpCfgFilial ObterPorCnpj(Database db, string cnpj)
        {
            return SelectSingle(db, new BoBolarImpCfgFilial { Cnpj = cnpj });
        }

    }

}
