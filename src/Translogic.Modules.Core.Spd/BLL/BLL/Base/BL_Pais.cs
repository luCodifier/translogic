// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_Pais : BLClass<Translogic.Modules.Core.Spd.Data.Pais>
    {

        public static Translogic.Modules.Core.Spd.Data.Pais SelectByPk(Database db, Decimal? _PiIdPas)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.Pais>(string.Format("PI_ID_PAS={0}", _PiIdPas));
        }
        public static Translogic.Modules.Core.Spd.Data.Pais SelectByPk(Decimal? _PiIdPas)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.Pais>(string.Format("PI_ID_PAS={0}", _PiIdPas));
        }

        public static int DeleteByPk(Database db, Decimal? _PiIdPas)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.Pais>(string.Format("PI_ID_PAS={0}", _PiIdPas));
        }
        public static int DeleteByPk(Decimal? _PiIdPas)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.Pais>(string.Format("PI_ID_PAS={0}", _PiIdPas));
        }



    }

}
