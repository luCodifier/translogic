// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_ConfGeral : BLClass<Translogic.Modules.Core.Spd.Data.ConfGeral>
    {

        public static Translogic.Modules.Core.Spd.Data.ConfGeral SelectByPk(Database db, String _Chave)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.ConfGeral>(string.Format("CHAVE={0}", Q(_Chave)));
        }
        public static Translogic.Modules.Core.Spd.Data.ConfGeral SelectByPk(String _Chave)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.ConfGeral>(string.Format("CHAVE={0}", Q(_Chave)));
        }

        public static int DeleteByPk(Database db, String _Chave)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.ConfGeral>(string.Format("CHAVE={0}", Q(_Chave)));
        }
        public static int DeleteByPk(String _Chave)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.ConfGeral>(string.Format("CHAVE={0}", Q(_Chave)));
        }



    }

}
