// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_LogCteVirtualSemVinculo : BLClass<Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo>
    {

        public static Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo SelectByPk(Database db, Decimal? _IdCteVirtualSemVinculo)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo>(string.Format("ID_CTE_VIRTUAL_SEM_VINCULO={0}", _IdCteVirtualSemVinculo));
        }
        public static Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo SelectByPk(Decimal? _IdCteVirtualSemVinculo)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo>(string.Format("ID_CTE_VIRTUAL_SEM_VINCULO={0}", _IdCteVirtualSemVinculo));
        }

        public static int DeleteByPk(Database db, Decimal? _IdCteVirtualSemVinculo)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo>(string.Format("ID_CTE_VIRTUAL_SEM_VINCULO={0}", _IdCteVirtualSemVinculo));
        }
        public static int DeleteByPk(Decimal? _IdCteVirtualSemVinculo)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.LogCteVirtualSemVinculo>(string.Format("ID_CTE_VIRTUAL_SEM_VINCULO={0}", _IdCteVirtualSemVinculo));
        }



    }

}
