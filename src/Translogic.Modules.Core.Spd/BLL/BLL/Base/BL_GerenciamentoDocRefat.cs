// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_GerenciamentoDocRefat : BLClass<Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat>
    {

        public static Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat SelectByPk(Database db, Decimal? _IdGerenciamentoDocRefat)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat>(string.Format("ID_GERENCIAMENTO_DOC_REFAT={0}", _IdGerenciamentoDocRefat));
        }
        public static Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat SelectByPk(Decimal? _IdGerenciamentoDocRefat)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat>(string.Format("ID_GERENCIAMENTO_DOC_REFAT={0}", _IdGerenciamentoDocRefat));
        }

        public static int DeleteByPk(Database db, Decimal? _IdGerenciamentoDocRefat)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat>(string.Format("ID_GERENCIAMENTO_DOC_REFAT={0}", _IdGerenciamentoDocRefat));
        }
        public static int DeleteByPk(Decimal? _IdGerenciamentoDocRefat)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.GerenciamentoDocRefat>(string.Format("ID_GERENCIAMENTO_DOC_REFAT={0}", _IdGerenciamentoDocRefat));
        }



    }

}
