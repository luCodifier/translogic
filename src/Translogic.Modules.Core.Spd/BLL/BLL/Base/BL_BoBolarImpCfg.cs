// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_BoBolarImpCfg : BLClass<Translogic.Modules.Core.Spd.Data.BoBolarImpCfg>
    {

        public static Translogic.Modules.Core.Spd.Data.BoBolarImpCfg SelectByPk(Database db, String _Cliente)
        {
            return db.SelectSingle<Translogic.Modules.Core.Spd.Data.BoBolarImpCfg>(string.Format("CLIENTE={0}", Q(_Cliente)));
        }
        public static Translogic.Modules.Core.Spd.Data.BoBolarImpCfg SelectByPk(String _Cliente)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Core.Spd.Data.BoBolarImpCfg>(string.Format("CLIENTE={0}", Q(_Cliente)));
        }

        public static int DeleteByPk(Database db, String _Cliente)
        {
            return db.Delete<Translogic.Modules.Core.Spd.Data.BoBolarImpCfg>(string.Format("CLIENTE={0}", Q(_Cliente)));
        }
        public static int DeleteByPk(String _Cliente)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Core.Spd.Data.BoBolarImpCfg>(string.Format("CLIENTE={0}", Q(_Cliente)));
        }



    }

}
