using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_GerenciamentoDocRefat
    {

        /// <summary>
        /// Retorna todos as documenta��es de refaturamento que est�o pendentes de atualiza��o
        /// </summary>
        /// <returns>Todos os registros cuja documenta��o est� pendente de atualiza��o</returns>
        public static List<GerenciamentoDocRefat> ObterPendentesAtualizacao()
        {
            string sql =
@"select
    r.*
from
    GERENCIAMENTO_DOC_REFAT r
    INNER JOIN GERENCIAMENTO_DOC d ON r.id_gerenciamento_doc_refat = d.id_gerenciamento_doc_refat
where
    d.Criado_Em is not null and
    d.numero_tentativas <= 5 and
    d.excluido_em is null and
    r.processado = 'N'
";

            return SelectSql(sql);
        }

        public static List<GerenciamentoDocRefat> ObterPendentesGeracao(decimal recebedorId)
        {
            string sql =
@"select
    r.*
from
    GERENCIAMENTO_DOC_REFAT r
    LEFT OUTER JOIN GERENCIAMENTO_DOC d ON r.id_gerenciamento_doc_refat = d.id_gerenciamento_doc_refat
where
    r.id_recebedor = {0}
    and d.id_gerenciamento_doc is null
order by
      r.id_gerenciamento_doc_refat desc";

            return SelectSql(string.Format(sql, recebedorId));
        }

    }

}
