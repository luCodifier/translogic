using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfeFluxosAssociados
    {

        /// <summary>
        /// Fluxos associados das Notas Fiscais
        /// </summary>
        /// <param name="chavesNfe"> chaves nfe, separadas por v�rgula ou ponto e v�rgula</param>
        /// <param name="somenteVigentes">Se retorna somente os vigentes: FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)</param>
        /// <param name="tomadorBloqueado">Status do Tomador: null, S ou N</param>
        /// <returns> Retorna lista de Objetos do tipo VwNfeFluxosAssociados </returns>
        public static IList<VwNfeFluxosAssociados> ObterFluxosAssociadosChaveNfe(string chavesNfe, bool somenteVigentes, string cnpjRaiz = null, string tomadorBloqueado = null)
        {
            var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
            var where = new List<string>();

            where.Add("CHAVE_NFE IN (" + chaves + ")");

            if (somenteVigentes)
                where.Add("FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)");

            if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                where.Add("USUARIO_CNPJ LIKE '%" + cnpjRaiz + "%'");

            if (!string.IsNullOrWhiteSpace(tomadorBloqueado))
                where.Add("TOMADOR_BLOQUEADO = '" + tomadorBloqueado + "'");

            var listSpd = BL_VwNfeFluxosAssociados.Select(string.Join(" AND ", where));
            return listSpd;
        }

        /// <summary>
        /// Obt�m a lista de fluxos. Usado no CAALL
        /// </summary>
        /// <param name="cnpjRaiz"></param>
        /// <param name="numDias">N�mero de dias, de hora para tr�s que se deseja o relat�rio</param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="mercadoria"></param>
        /// <param name="chavesNfe"></param>
        /// <param name="pagador"></param>
        /// <param name="remetFiscal"></param>
        /// <param name="destFiscal"></param>
        /// <param name="somenteVigentes"></param>
        /// <param name="tomadorBloqueado"></param>
        /// <returns></returns>
        public static List<VwNfeFluxosAssociados> ObterFluxosAssociados(
            string cnpjRaiz,
            DateTime dataInicial,
            string origem,
            string destino,
            string fluxo,
            string chavesNfe,
            string mercadoria,
            string pagador = null,
            string remetFiscal = null,
            string destFiscal = null,
            bool somenteVigentes = false,
            bool tomadorBloqueado = false)
        {
            var where = new List<string>();

            if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                where.Add("USUARIO_CNPJ LIKE '" + cnpjRaiz + "%'");

            if (!string.IsNullOrWhiteSpace(origem))
                where.Add("ORIGEM = '" + origem + "'");

            if (!string.IsNullOrWhiteSpace(destino))
                where.Add("DESTINO = '" + destino + "'");

            if (!string.IsNullOrWhiteSpace(fluxo))
                where.Add("FLUXO = '" + fluxo + "'");

            if (!string.IsNullOrWhiteSpace(chavesNfe))
            {
                var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
                where.Add("CHAVE_NFE IN (" + chaves + ")");
            }

            if (!string.IsNullOrWhiteSpace(mercadoria))
                where.Add("MERCADORIA = '" + mercadoria + "'");

            if (!string.IsNullOrWhiteSpace(pagador))
                where.Add("(TOMADOR_CNPJ LIKE '%" + pagador + "%' OR TOMADOR_CNPJ LIKE '%" + pagador + "%')");

            if (!string.IsNullOrWhiteSpace(remetFiscal))
                where.Add("(REMETENTE_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR REMETENTE_FISCAL LIKE '%" + remetFiscal + "%')");

            if (!string.IsNullOrWhiteSpace(destFiscal))
                where.Add("(DESTINATARIO_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR DESTINATARIO_FISCAL LIKE '%" + remetFiscal + "%')");

            where.Add("FLUXO_VIGENCIA_FINAL >= TO_DATE('" + dataInicial.ToString("ddMMyyyy") + "', 'DDMMYYYY')");

            if (somenteVigentes)
                where.Add("FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)");

            if (tomadorBloqueado)
                where.Add("TOMADOR_BLOQUEADO = 'S'");

            return BL_VwNfeFluxosAssociados.Select(string.Join(" AND ", where), 500);
        }

    }

}
