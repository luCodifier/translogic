using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwNfeProd
    {

        /// <summary>
        /// Obt�m os produtos dado o id da nfe e o tipo se � edi ou simconsultas
        /// </summary>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se � edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        public static List<VwNfeProd> ObterPorTipoId(int idNfe, string tipo)
        {
            return Select(new VwNfeProd { IdNfe = idNfe, Tipo = tipo });
        }

        /// <summary>
        /// Obt�m os produtos dado o id da nfe e o tipo se � edi ou simconsultas
        /// </summary>
        /// <param name="db"> Database </param>
        /// <param name="idNfe"> Id da nota fiscal eletronica. </param>
        /// <param name="tipo"> Tipo que indica se � edi ou simconsultas</param>
        /// <returns> Objeto Lista de produtos </returns>
        public static List<VwNfeProd> ObterPorTipoId(Database db, int idNfe, string tipo)
        {
            return Select(db, new VwNfeProd { IdNfe = idNfe, Tipo = tipo });
        }

    }

}
