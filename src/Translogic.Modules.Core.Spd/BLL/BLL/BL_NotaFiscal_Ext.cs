using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_NotaFiscal
    {

        public static List<NotaFiscal> SelectPorDespachos(Database db, List<Despacho> despachos)
        {
            var inStr = Conv.GetIn(despachos.Select(p => p.IdDespacho));
            string where = $"DP_ID_DP in ({inStr})";
            return Select(db, where);
        }

        public static List<NotaFiscal> SelectPorDespachos(List<decimal?> idDespachos)
        {
            var inStr = Conv.GetIn(idDespachos);
            string where = $"DP_ID_DP in ({inStr})";
            return Select(where);
        }

        public static List<NotaFiscal> SelectPorDespacho(Despacho despacho)
        {
            string where = $"DP_ID_DP = {despacho.IdDespacho}";
            return Select(where);
        }

    }

}
