using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwEdiNfeNoPdf
    {
        public static List<VwEdiNfeNoPdf> ObterPorChavesNfe(string[] chavesNfe)
        {
            var chaves = string.Join(",", chavesNfe.Select(p => "'" + p + "'"));
            return Select("NFE_CHAVE in (" + chaves + ")");
        }
    }
}
