using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_Cte
    {

        public static List<Cte> ObterCtesPorDespacho(decimal? idDespacho)
        {
            return Select(new Cte { IdDespacho = idDespacho });
        }

        public static List<Cte> ObterCtesPorDespachos(List<decimal?> idsDespachos)
        {
            var ids = string.Join(",", idsDespachos.Where(p => p != null));
            return Select($"DP_ID_DP IN ({ids})");
        }

    }

}
