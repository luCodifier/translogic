using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_StatusNfe
    {

        public static StatusNfe ObterPorChaveNfe(string chave)
        {
            return SelectSingle(new StatusNfe { ChaveNfe = chave });
        }

        public static StatusNfe ObterPorChaveNfe(Database db, string chave)
        {
            return SelectSingle(db, new StatusNfe { ChaveNfe = chave });
        }

    }

}
