using System.Collections.Generic;
using System.Linq;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{
    public partial class BL_VwEdiNfePdf
    {
        public static List<VwEdiNfePdf> ObterPorChavesNfe(string[] chavesNfe)
        {
            var chaves = string.Join(",", chavesNfe.Select(p => "'" + p + "'"));
            return Select("NFE_CHAVE in (" + chaves + ")");
        }
    }
}
