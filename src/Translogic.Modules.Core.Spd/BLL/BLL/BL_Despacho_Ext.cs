using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_Despacho
    {

        public static List<Despacho> ObterDespachosPorItensDespacho(int[] itensDespachoId)
        {
            var ids = string.Join(",", itensDespachoId);
            return Select(string.Format("DP_ID_DP in (select DP_ID_DP from item_despacho where ID_ID_ITD in ({0}))", ids))
                .Distinct(p => p.IdDespacho).ToList();
        }

    }

}
