using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_PedidoDerivado
    {

        public static List<PedidoDerivado> ObterPorIds(List<decimal?> ids)
        {
            var _ids = string.Join(",", ids.Where(p => p != null));
            return Select($"PX_IDT_PDR IN ({ids})");
        }

    }

}
