using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwConsultaFluxo
    {

        /// <summary>
        /// Obt�m a lista de fluxos. Usado no CAALL
        /// </summary>
        /// <param name="pagina">N�mero da p�gina. Se for negativo tras tudo</param>
        /// <param name="cnpjRaiz"></param>
        /// <param name="numDias">N�mero de dias, de hora para tr�s que se deseja o relat�rio</param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="mercadoria"></param>
        /// <param name="chavesNfe"></param>
        /// <param name="pagador"></param>
        /// <param name="remetFiscal"></param>
        /// <param name="destFiscal"></param>
        /// <param name="somenteVigentes"></param>
        /// <param name="tomadorBloqueado"></param>
        /// <returns></returns>
        public static List<VwConsultaFluxo> ObterFluxosAssociados(
            int pagina, int numRegistros, string ordem,
            string cnpjRaiz,
            DateTime dataInicial,
            string origem,
            string destino,
            string fluxo,
            string chavesNfe,
            string mercadoria,
            string pagador = null,
            string remetFiscal = null,
            string destFiscal = null,
            bool somenteVigentes = false,
            bool tomadorBloqueado = false)
        {

            /*            
                        #region Sql

                        string sql =
            @"    SELECT 
                    DISTINCT
                    USUA.EP_CGC_EMP USUARIO_CNPJ,
                    NFE.NFE_CHAVE CHAVE_NFE,        
                    F.FX_COD_FLX as FLUXO, 
                    F.FX_ID_FLX ID,
                    F.FX_COD_SEGMENTO AS FLUXO_SEGMENTO,
                    M.MC_COD_MRC as MERCADORIA_CODIGO,
                    M.MC_DRS_PT as MERCADORIA,

                    REME.EP_DSC_RSM AS REMETENTE_FISCAL,
                    F.FX_CNPJ_REM_FISCAL AS REMETENTE_FISCAL_CNPJ,      
                    DEST.EP_DSC_RSM AS DESTINATARIO_FISCAL,
                    F.FX_CNPJ_DES_FISCAL AS DESTINATARIO_FISCAL_CNPJ,

                    TOMA.EP_CGC_EMP AS TOMADOR_CNPJ,
                    TOMA.EP_DSC_RSM AS TOMADOR,
                    TOMA.EP_UF_EMP AS TOMADOR_UF,
                    TOMA.FATBLOQUEADO AS TOMADOR_BLOQUEADO,

                    A_ORI.AO_COD_AOP AS ORIGEM,
                    A_ORI.AO_DSC_AOP AS ORIGEM_DESC,

                    A_DES.AO_COD_AOP AS DESTINO,
                    A_DES.AO_DSC_AOP AS DESTINO_DESC,

                    F.FX_DAT_VIG as FLUXO_VIGENCIA_FINAL,
                    F.FX_MODELO_NF as MODELO_NOTA,

                    RECE.EP_DSC_RSM AS RECEBEDOR,
                    RECE.EP_CGC_EMP AS RECEBEDOR_CNPJ

                FROM
                    VW_NFE             NFE
                    JOIN FLUXO_COMERCIAL F ON F.FX_CNPJ_REM_FISCAL = NFE.NFE_CNPJ_ORI
                         AND F.FX_CNPJ_DES_FISCAL = NFE.NFE_CNPJ_DST
                         AND F.FX_MODELO_NF = '55'
                         AND EXISTS (
                                     SELECT 1
                                     FROM ASSOCIA_FLUXO_VIG AFV
                                     WHERE AFV.FX_ID_FLX = F.FX_ID_FLX
                                     AND ROWNUM = 1
                                    ),
                    EMPRESA REME,
                    EMPRESA DEST,
                    EMPRESA TOMA,
                    EMPRESA USUA,
                    EMPRESA RECE,
                    CIDADE_IBGE C_REME,
                    CIDADE_IBGE C_DEST,
                    AREA_OPERACIONAL A_ORI,
                    AREA_OPERACIONAL A_DES,      
                    MERCADORIA M,
                    CONTRATO C,
                    MUNICIPIO M_ORI,
                    MUNICIPIO M_DES       
                WHERE        
                    F.FX_DAT_VIG >= SYSDATE
                    AND F.FX_CNPJ_REM_FISCAL = REME.EP_CGC_EMP
                    AND REME.CD_ID_CD = C_REME.CD_ID_CD
                    AND F.FX_CNPJ_DES_FISCAL = DEST.EP_CGC_EMP
                    AND DEST.CD_ID_CD = C_DEST.CD_ID_CD
                    AND F.AO_ID_EST_OR = A_ORI.AO_ID_AO
                    AND A_ORI.MN_ID_MNC = M_ORI.MN_ID_MNC
                    AND F.AO_ID_EST_DS = A_DES.AO_ID_AO
                    AND A_DES.MN_ID_MNC = M_DES.MN_ID_MNC
                    AND F.MC_ID_MRC = M.MC_ID_MRC
                    AND F.CZ_ID_CTT = C.CZ_ID_CTT
                    AND C.EP_ID_EMP_COR = TOMA.EP_ID_EMP 
                    AND C.EP_ID_EMP_REM = USUA.EP_ID_EMP
                    AND F.EP_ID_EMP_DST = RECE.EP_ID_EMP
                    AND TOMA.FATBLOQUEADO = 'N'
                    AND F.FX_CNPJ_REM_FISCAL <> ' '
                    AND F.FX_CNPJ_DES_FISCAL <> ' '
                    AND INSTR(REME.EP_SIG_EMP, '-')> 0 
                    AND INSTR(DEST.EP_SIG_EMP, '-')> 0 
                    {0}
            UNION
                SELECT 
                    DISTINCT
                    USUA.EP_CGC_EMP USUARIO_CNPJ,
                    NFE.NFE_CHAVE CHAVE_NFE,
                    F.FX_COD_FLX as FLUXO, 
                    F.FX_ID_FLX ID,
                    F.FX_COD_SEGMENTO AS FLUXO_SEGMENTO,
                    M.MC_COD_MRC as MERCADORIA_CODIGO,
                    M.MC_DRS_PT as MERCADORIA,

                    REME.EP_DSC_RSM AS REMETENTE_FISCAL,
                    F.FX_CNPJ_REM_FISCAL AS REMETENTE_FISCAL_CNPJ,      
                    NULL AS DESTINATARIO_FISCAL,
                    F.FX_CNPJ_DES_FISCAL AS DESTINATARIO_FISCAL_CNPJ,

                    TOMA.EP_CGC_EMP AS TOMADOR_CNPJ,
                    TOMA.EP_DSC_RSM AS TOMADOR,
                    TOMA.EP_UF_EMP AS TOMADOR_UF,
                    TOMA.FATBLOQUEADO AS TOMADOR_BLOQUEADO,

                    A_ORI.AO_COD_AOP AS ORIGEM,
                    A_ORI.AO_DSC_AOP AS ORIGEM_DESC,

                    A_DES.AO_COD_AOP AS DESTINO,
                    A_DES.AO_DSC_AOP AS DESTINO_DESC,

                    F.FX_DAT_VIG as FLUXO_VIGENCIA_FINAL,
                    F.FX_MODELO_NF as MODELO_NOTA,

                    RECE.EP_DSC_RSM AS RECEBEDOR,
                    RECE.EP_CGC_EMP AS RECEBEDOR_CNPJ
                FROM
                    VW_NFE             NFE
                    JOIN FLUXO_COMERCIAL F ON F.FX_CNPJ_REM_FISCAL = NFE.NFE_CNPJ_ORI
                         AND F.FX_CNPJ_DES_FISCAL = NFE.NFE_CNPJ_DST
                         AND F.FX_MODELO_NF = '55'
                         AND EXISTS (
                                     SELECT 1
                                     FROM ASSOCIA_FLUXO_VIG AFV
                                     WHERE AFV.FX_ID_FLX = F.FX_ID_FLX
                                     AND ROWNUM = 1
                                    ),
                    EMPRESA REME,
                    EMPRESA DEST,
                    EMPRESA TOMA,
                    EMPRESA USUA,
                    EMPRESA RECE,
                    CIDADE_IBGE C_REME,
                    CIDADE_IBGE C_DEST,
                    AREA_OPERACIONAL A_ORI,
                    AREA_OPERACIONAL A_DES,      
                    MERCADORIA M,
                    CONTRATO C,
                    MUNICIPIO M_ORI,
                    MUNICIPIO M_DES       
                WHERE        
                    F.FX_DAT_VIG >= SYSDATE
                    AND F.FX_CNPJ_REM_FISCAL = REME.EP_CGC_EMP
                    AND REME.CD_ID_CD = C_REME.CD_ID_CD
                    AND F.FX_CNPJ_DES_FISCAL = DEST.EP_CGC_EMP
                    AND DEST.CD_ID_CD = C_DEST.CD_ID_CD
                    AND F.AO_ID_EST_OR = A_ORI.AO_ID_AO
                    AND A_ORI.MN_ID_MNC = M_ORI.MN_ID_MNC
                    AND F.AO_ID_EST_DS = A_DES.AO_ID_AO
                    AND A_DES.MN_ID_MNC = M_DES.MN_ID_MNC
                    AND F.MC_ID_MRC = M.MC_ID_MRC
                    AND F.CZ_ID_CTT = C.CZ_ID_CTT
                    AND C.EP_ID_EMP_COR = TOMA.EP_ID_EMP 
                    AND C.EP_ID_EMP_REM = USUA.EP_ID_EMP
                    AND F.EP_ID_EMP_DST = RECE.EP_ID_EMP
                    AND TOMA.FATBLOQUEADO = 'N'
                    AND F.FX_CNPJ_DES_FISCAL = ' '
                    AND INSTR(REME.EP_SIG_EMP, '-')> 0 
                    {0}
            UNION
                SELECT 
                    DISTINCT
                    USUA.EP_CGC_EMP USUARIO_CNPJ,
                    NFE.NFE_CHAVE CHAVE_NFE,
                    F.FX_COD_FLX as FLUXO, 
                    F.FX_ID_FLX ID,
                    F.FX_COD_SEGMENTO AS FLUXO_SEGMENTO,
                    M.MC_COD_MRC as MERCADORIA_CODIGO,
                    M.MC_DRS_PT as MERCADORIA,

                    NULL AS REMETENTE_FISCAL,
                    F.FX_CNPJ_REM_FISCAL AS REMETENTE_FISCAL_CNPJ,      
                    DEST.EP_DSC_RSM AS DESTINATARIO_FISCAL,
                    F.FX_CNPJ_DES_FISCAL AS DESTINATARIO_FISCAL_CNPJ,

                    TOMA.EP_CGC_EMP AS TOMADOR_CNPJ,
                    TOMA.EP_DSC_RSM AS TOMADOR,
                    TOMA.EP_UF_EMP AS TOMADOR_UF,
                    TOMA.FATBLOQUEADO AS TOMADOR_BLOQUEADO,

                    A_ORI.AO_COD_AOP AS ORIGEM,
                    A_ORI.AO_DSC_AOP AS ORIGEM_DESC,

                    A_DES.AO_COD_AOP AS DESTINO,
                    A_DES.AO_DSC_AOP AS DESTINO_DESC,

                    F.FX_DAT_VIG as FLUXO_VIGENCIA_FINAL,
                    F.FX_MODELO_NF as MODELO_NOTA,

                    RECE.EP_DSC_RSM AS RECEBEDOR,
                    RECE.EP_CGC_EMP AS RECEBEDOR_CNPJ
                FROM
                    VW_NFE             NFE
                    JOIN FLUXO_COMERCIAL F ON F.FX_CNPJ_REM_FISCAL = NFE.NFE_CNPJ_ORI
                         AND F.FX_CNPJ_DES_FISCAL = NFE.NFE_CNPJ_DST
                         AND F.FX_MODELO_NF = '55'
                         AND EXISTS (
                                     SELECT 1
                                     FROM ASSOCIA_FLUXO_VIG AFV
                                     WHERE AFV.FX_ID_FLX = F.FX_ID_FLX
                                     AND ROWNUM = 1
                                    ),
                    EMPRESA REME,
                    EMPRESA DEST,
                    EMPRESA TOMA,
                    EMPRESA USUA,
                    EMPRESA RECE,
                    CIDADE_IBGE C_REME,
                    CIDADE_IBGE C_DEST,
                    AREA_OPERACIONAL A_ORI,
                    AREA_OPERACIONAL A_DES,      
                    MERCADORIA M,
                    CONTRATO C,
                    MUNICIPIO M_ORI,
                    MUNICIPIO M_DES       
                WHERE        
                    F.FX_DAT_VIG >= SYSDATE
                    AND F.FX_CNPJ_REM_FISCAL = REME.EP_CGC_EMP
                    AND REME.CD_ID_CD = C_REME.CD_ID_CD
                    AND F.FX_CNPJ_DES_FISCAL = DEST.EP_CGC_EMP
                    AND DEST.CD_ID_CD = C_DEST.CD_ID_CD
                    AND F.AO_ID_EST_OR = A_ORI.AO_ID_AO
                    AND A_ORI.MN_ID_MNC = M_ORI.MN_ID_MNC
                    AND F.AO_ID_EST_DS = A_DES.AO_ID_AO
                    AND A_DES.MN_ID_MNC = M_DES.MN_ID_MNC
                    AND F.MC_ID_MRC = M.MC_ID_MRC
                    AND F.CZ_ID_CTT = C.CZ_ID_CTT
                    AND C.EP_ID_EMP_COR = TOMA.EP_ID_EMP 
                    AND C.EP_ID_EMP_REM = USUA.EP_ID_EMP
                    AND F.EP_ID_EMP_DST = RECE.EP_ID_EMP
                    AND TOMA.FATBLOQUEADO = 'N'
                    AND F.FX_CNPJ_REM_FISCAL = ' '
                    AND INSTR(DEST.EP_SIG_EMP, '-') > 0 
                    {0}
            ";
                        #endregion Sql

                        if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                            where.Add("USUA.EP_CGC_EMP LIKE '" + cnpjRaiz + "%'");

                        if (!string.IsNullOrWhiteSpace(origem))
                            where.Add("A_ORI.AO_COD_AOP = '" + origem + "'");

                        if (!string.IsNullOrWhiteSpace(destino))
                            where.Add("A_DES.AO_COD_AOP = '" + destino + "'");

                        if (!string.IsNullOrWhiteSpace(fluxo))
                            where.Add("F.FX_COD_FLX = '" + fluxo + "'");

                        if (!string.IsNullOrWhiteSpace(chavesNfe))
                        {
                            var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
                            where.Add("NFE.NFE_CHAVE IN (" + chaves + ")");
                        }

                        if (!string.IsNullOrWhiteSpace(mercadoria))
                            where.Add("M.MC_DRS_PT = '" + mercadoria + "'");

                        if (!string.IsNullOrWhiteSpace(pagador))
                            where.Add("(TOMA.EP_CGC_EMP LIKE '%" + pagador + "%' OR TOMA.EP_DSC_RSM LIKE '%" + pagador + "%')");

                        if (!string.IsNullOrWhiteSpace(remetFiscal))
                            where.Add("(F.FX_CNPJ_REM_FISCAL LIKE '%" + remetFiscal + "%' OR REME.EP_DSC_RSM LIKE '%" + remetFiscal + "%')");

                        if (!string.IsNullOrWhiteSpace(destFiscal))
                            where.Add("(F.FX_CNPJ_DES_FISCAL LIKE '%" + remetFiscal + "%' OR DEST.EP_DSC_RSM LIKE '%" + remetFiscal + "%')");

                        where.Add("F.FX_DAT_VIG >= TO_DATE('" + dataInicial.ToString("ddMMyyyy") + "', 'DDMMYYYY')");

                        if (somenteVigentes)
                            where.Add("F.FX_DAT_VIG >= TRUNC(SYSDATE-1)");

                        if (tomadorBloqueado)
                            where.Add("TOMA.FATBLOQUEADO = 'S'");

            string _where = "AND " + string.Join("\r\nAND ", where);
             sql = string.Format(sql, _where);

            return BL_VwConsultaFluxo.SelectSql(sql, 500);
           */

            var where = new List<string>();

            if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                where.Add("USUARIO_CNPJ LIKE '" + cnpjRaiz + "%'");

            if (!string.IsNullOrWhiteSpace(origem))
                where.Add("ORIGEM = '" + origem + "'");

            if (!string.IsNullOrWhiteSpace(destino))
                where.Add("DESTINO = '" + destino + "'");

            if (!string.IsNullOrWhiteSpace(fluxo))
                where.Add("FLUXO = '" + fluxo + "'");

            if (!string.IsNullOrWhiteSpace(chavesNfe))
            {
                var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
                where.Add("CHAVE_NFE IN (" + chaves + ")");
            }

            if (!string.IsNullOrWhiteSpace(mercadoria))
                where.Add("MERCADORIA = '" + mercadoria + "'");

            if (!string.IsNullOrWhiteSpace(pagador))
                where.Add("(TOMADOR_CNPJ LIKE '%" + pagador + "%' OR TOMADOR_CNPJ LIKE '%" + pagador + "%')");

            if (!string.IsNullOrWhiteSpace(remetFiscal))
                where.Add("(REMETENTE_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR REMETENTE_FISCAL LIKE '%" + remetFiscal + "%')");

            if (!string.IsNullOrWhiteSpace(destFiscal))
                where.Add("(DESTINATARIO_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR DESTINATARIO_FISCAL LIKE '%" + remetFiscal + "%')");

            where.Add("FLUXO_VIGENCIA_FINAL >= TO_DATE('" + dataInicial.ToString("ddMMyyyy") + "', 'DDMMYYYY')");

            if (somenteVigentes)
                where.Add("FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)");

            string _where = string.Join("\r\nAND ", where);

            return BL_VwConsultaFluxo.Select(_where, 500);

            //if (pagina >= 0)
            //    return BL_VwConsultaFluxo.SelectPage(_where, pagina, numRegistros, ordem, 500);
            //else
            //    return BL_VwConsultaFluxo.Select(_where, 500);
        }

    }

}
