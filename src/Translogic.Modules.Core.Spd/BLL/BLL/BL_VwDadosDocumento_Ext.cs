using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Common;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwDadosDocumento
    {

        public static void PrepararCache()
        {
            using (var db = Sys.NewDb())
            {
                db.ExecuteNonQuery("TRUNCATE TABLE TMP_ULT_COMP_VAGAO_OS");
                db.ExecuteNonQuery("TRUNCATE TABLE TMP_CACHE_LAUDO_MERCADORIA");
                db.ExecuteNonQuery("P_CACHE_GERAR_DOCUMENTACAO", CommandType.StoredProcedure, 0);
            }
        }

        public static List<VwDadosDocumento> ObterDadosDocumento(decimal? idOs, decimal? idRecebedor)
        {
            return Select(new VwDadosDocumento { IdOs = idOs.GetValueOrDefault(), IdRecebedor = idRecebedor.GetValueOrDefault() });
        }

        public static List<VwDadosDocumento> ObterDadosDocumentoRefaturamento(decimal? idOs, decimal? idRecebedor)
        {
            var list = BL_VwDadosDocumentoRefat.Select(new VwDadosDocumentoRefat { IdOs = idOs.GetValueOrDefault(), IdRecebedor = idRecebedor.GetValueOrDefault() });
            // essa convers�o s� � possivel pq os 2 views possuem os mesmos nomes de colunas
            return list.Select(p => ReflectionUtil.Clone<VwDadosDocumento>(p)).ToList();
        }

        public static List<VwDadosDocumento> ObterDadosDocumentoHistorico(decimal? idOs, decimal? idRecebedor)
        {
            var list = BL_VwDadosDocumentoHist.Select(new VwDadosDocumentoHist { IdOs = idOs.GetValueOrDefault(), IdRecebedor = idRecebedor.GetValueOrDefault() });
            // essa convers�o s� � possivel pq os 2 views possuem os mesmos nomes de colunas
            return list.Select(p => ReflectionUtil.Clone<VwDadosDocumento>(p)).ToList();
        }

    }

}
