using System;
using System.Collections.Generic;
using System.Data;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_VwConsultaFluxo
    {

        /// <summary>
        /// Obt�m a lista de fluxos. Usado no CAALL
        /// </summary>
        /// <param name="pagina">N�mero da p�gina. Se for negativo tras tudo</param>
        /// <param name="cnpjRaiz"></param>
        /// <param name="numDias">N�mero de dias, de hora para tr�s que se deseja o relat�rio</param>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        /// <param name="mercadoria"></param>
        /// <param name="chavesNfe"></param>
        /// <param name="pagador"></param>
        /// <param name="remetFiscal"></param>
        /// <param name="destFiscal"></param>
        /// <param name="somenteVigentes"></param>
        /// <param name="tomadorBloqueado"></param>
        /// <returns></returns>
        public static List<VwConsultaFluxo> ObterFluxosAssociados(
            int pagina, int numRegistros, string ordem,
            string cnpjRaiz,
            int? id,
            DateTime? dataInicial,
            string origem,
            string destino,
            string fluxo,
            string chavesNfe,
            string mercadoria,
            string pagador = null,
            string remetFiscal = null,
            string destFiscal = null,
            bool somenteVigentes = false,
            bool tomadorBloqueado = false)
        {
            var where = new List<string>();

            if (id != null)
                where.Add("ID = " + id);

            if (!string.IsNullOrWhiteSpace(cnpjRaiz))
                where.Add("USUARIO_CNPJ LIKE '" + cnpjRaiz + "%'");

            if (!string.IsNullOrWhiteSpace(origem))
                where.Add("ORIGEM = '" + origem + "'");

            if (!string.IsNullOrWhiteSpace(destino))
                where.Add("DESTINO = '" + destino + "'");

            if (!string.IsNullOrWhiteSpace(fluxo))
                where.Add("FLUXO = '" + fluxo + "'");

            if (!string.IsNullOrWhiteSpace(chavesNfe))
            {
                var chaves = Helper.FormataListaStr(chavesNfe, ",", true);
                where.Add("CHAVE_NFE IN (" + chaves + ")");
            }

            if (!string.IsNullOrWhiteSpace(mercadoria))
                where.Add("MERCADORIA = '" + mercadoria + "'");

            if (!string.IsNullOrWhiteSpace(pagador))
                where.Add("(TOMADOR_CNPJ LIKE '%" + pagador + "%' OR TOMADOR_CNPJ LIKE '%" + pagador + "%')");

            if (!string.IsNullOrWhiteSpace(remetFiscal))
                where.Add("(REMETENTE_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR REMETENTE_FISCAL LIKE '%" + remetFiscal + "%')");

            if (!string.IsNullOrWhiteSpace(destFiscal))
                where.Add("(DESTINATARIO_FISCAL_CNPJ LIKE '%" + remetFiscal + "%' OR DESTINATARIO_FISCAL LIKE '%" + remetFiscal + "%')");

            if (dataInicial != null)
            where.Add("FLUXO_VIGENCIA_FINAL >= TO_DATE('" + dataInicial.Value.ToString("ddMMyyyy") + "', 'DDMMYYYY')");

            if (somenteVigentes)
                where.Add("FLUXO_VIGENCIA_FINAL >= TRUNC(SYSDATE-1)");

            string _where = string.Join("\r\nAND ", where);

            _where += "\r\nORDER BY " + ordem;

            return BL_VwConsultaFluxo.Select(_where, 500);

            //if (pagina >= 0)
            //    return BL_VwConsultaFluxo.SelectPage(_where, pagina, numRegistros, ordem, 500);
            //else
            //    return BL_VwConsultaFluxo.Select(_where, 500);
        }

    }

}
