using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_GerenciamentoDoc
    {

        public static List<GerenciamentoDoc> ObterNaoVigentes()
        {
            string where =
@"
                EXCLUIDO_EM IS     NULL
                AND CRIADO_EM   IS NOT NULL 
                AND CRIADO_EM + DIAS_VALIDADE < SYSDATE";

            return Select(where);
        }

        public static List<GerenciamentoDoc> ObterNaoGeradosInicialmente()
        {
            return Select("CRIADO_EM IS NULL AND NUMERO_TENTATIVAS <= 5");
        }

        public static GerenciamentoDoc ObterPorOs(decimal? idOs, decimal? idRecebedor, bool refaturamento)
        {
            var b = new List<string>();
            b.Add("ID_OS=" + idOs);
            b.Add("ID_RECEBEDOR=" + idRecebedor);
            if (refaturamento)
                b.Add("ID_GERENCIAMENTO_DOC_REFAT IS NOT NULL");
            else
                b.Add("ID_GERENCIAMENTO_DOC_REFAT IS NULL");

            string where = string.Join(" AND ", b) + " ORDER BY ID_GERENCIAMENTO_DOC DESC";

            return SelectSingle(where);
        }

    }

}
