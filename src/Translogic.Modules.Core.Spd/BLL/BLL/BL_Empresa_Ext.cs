using System;
using System.Collections.Generic;
using System.Data;
using Speed;
using Speed.Data;
using Translogic.Modules.Core.Spd.Data;

namespace Translogic.Modules.Core.Spd.BLL
{

    public partial class BL_Empresa
    {

        public static Empresa ObterPorId(Database db, int? id)
        {
            return SelectByPk(db, id.GetValueOrDefault());
        }

        public static Empresa GetEmpresaMrs(Database db, decimal? ediIdEmpresaMrs)
        {
            return SelectByPk(db, ediIdEmpresaMrs);
        }

        public static Empresa ObterPorSiglaSap(Database db, string codigoSap)
        {
            return SelectSingle(db, new Empresa { Sigla = codigoSap });
        }

        public static Empresa ObterPorCnpj(Database db, string cnpj)
        {
            return SelectSingle(db, new Empresa { Cgc = cnpj });
        }
    }

}
