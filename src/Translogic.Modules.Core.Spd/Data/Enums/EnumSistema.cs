﻿namespace Translogic.Modules.Core.Spd.Data.Enums
{
    public enum EnumSistema
    {
        Indefinido = 0,

        Translogic = 1,

        Caall = 2,

        EdiRunnerNotaExpedicao = 3,

        EdiRunnerShippingNote = 4,

        EdiRunnerAcompanhamentoTremProcessor = 5,

        EdiRunnerAcompanhamentoTremSender = 6,

        EdiRunnerShippingNoteJ = 7,

        CteRunner = 8,

        Testes = 9,

        EdiSoaWebService = 10,

        /// <summary>
        /// Base de dados do translogic. Usa o PKG_LOG
        /// </summary>
        DbTranslogic = 11,

        JobRunner = 12,

        TranslogicSoaWebService = 13,

        EdiWebSite = 14,
        
        CteRunnerSenderInterfaceSap = 15,
        
        JobIntegracaoSeguradora = 16,
        
        EdiRunner = 17,

        Tools = 18,

        MdfeRunner = 19,

        
        // JobRunner
        JobRunnerEnvioDocumentacao = 30,
        JobRunnerGerarDocumentacao = 31,
        JobRunnerEdiDescarga = 32,
        // JobRunnerBaixarNfesDoEmail = 33, / /não é mais usado
        JobRunnerEmailDownloader = 34,
        JobRunnerAdmNfe = 35,
        JobEdiRunnerEdiNotas = 36,


    }

}
