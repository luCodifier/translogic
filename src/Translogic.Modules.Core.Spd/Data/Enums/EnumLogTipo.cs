﻿using System.ComponentModel;

namespace Translogic.Modules.Core.Spd.Data.Enums
{
    public enum EnumLogTipo
    {
        [Description("Indefinido")]
        Undefined = 0,

        [Description("Mensagem")]
        Message = 1,

        [Description("Exceção de erro no sistema")]
        Exception = 2,

        [Description("Mensagem de erro que o programador quer registrar, mas nao e uma exception")]
        Error = 3,

        [Description("Mensagem que o programador quer registrar pra propostas de debug")]
        Debug = 4,

        [Description("Mensagem de aviso")]
        Warning = 5,

        [Description("Trace")]
        Trace = 6,

        [Description("Info")]
        Info = 7,

        [Description("WebException")]
        WebException = 8,

        /// <summary>
        /// Log de quando acessa uma pagina. Usado pra auditoria
        /// </summary>
        [Description("Página")]
        Page = 9,

        [Description("FatalError")]
        FatalError = 10,

    }
}
