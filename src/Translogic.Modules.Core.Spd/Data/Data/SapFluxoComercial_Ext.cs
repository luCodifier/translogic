using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;

namespace Translogic.Modules.Core.Spd.Data
{

    public partial class SapFluxoComercial
    {

        public string Cor { get; set; }
        public string TimeStampFmt { get { return Timestamp.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm"); } }

        /// <summary>
        /// Status de aprova��o: N=Criado pelo CAALL; L=Lido; S=sucesso; E=erro
        /// </summary>
        public string StatusAprovacaoDesc
        {
            get
            {

                switch (StatusAprovacao)
                {
                    case "N":
                        return "Aguardando Integracao";
                    case "L":
                        return "Integrando";
                    case "S":
                        return "Integrado";
                    case "E":
                        return "Erro";
                    case "F":
                        return "Fluxo Existente";
                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// Tipo de Plataforma: L=long; S=Single; D=Double
        /// </summary>
        public string TipoPlataformaDesc
        {
            get
            {

                switch (TipoPlataforma)
                {
                    case "L":
                        return "Long";
                    case "S":
                        return "Single";
                    case "D":
                        return "Double";
                    default:
                        return null;
                }
            }
        }

        /// <summary>
        /// Formata todos os campos de cnpj
        /// </summary>
        public void FormataCnpjs()
        {
            this.CliExpCgc = Helper.FormataCnpj(this.CliExpCgc);
            this.CliFatCgc = Helper.FormataCnpj(this.CliFatCgc);
            this.CliRecCgc = Helper.FormataCnpj(this.CliRecCgc);
            this.DesFisCgc = Helper.FormataCnpj(this.DesFisCgc);
            this.RemFisCgc = Helper.FormataCnpj(this.RemFisCgc);
        }

    }

}
