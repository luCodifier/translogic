using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;

using Speed;
using Speed.Data;

namespace Translogic.Modules.Core.Spd.Data
{

    public partial class VwNfe
    {

        /// <summary>
        /// Campo Xml. Este campo � adicional e tem que ser carregado usando BL_VwNfeXml;
        /// </summary>
        public string Xml { get; set; }

    }

}
