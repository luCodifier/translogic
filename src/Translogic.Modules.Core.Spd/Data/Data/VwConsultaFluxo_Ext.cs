using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;

namespace Translogic.Modules.Core.Spd.Data
{

    public partial class VwConsultaFluxo
    {
        public string Cor { get; set; }

        public string FluxoVigenciaFinalFormatado
        {
            get
            {
                return FluxoVigenciaFinal == null || FluxoVigenciaFinal == DateTime.MinValue
                    ? "" :
                      FluxoVigenciaFinal.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm");
            }
        }

        public bool IsVigente
        {
            get
            {
                return FluxoVigenciaFinal.GetValueOrDefault().AddDays(-1) >= DateTime.Now.Date;
            }
        }
    }

}
