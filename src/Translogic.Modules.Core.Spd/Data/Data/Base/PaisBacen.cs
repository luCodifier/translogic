// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;

using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "PAIS_BACEN", "", "")]
    [Serializable]
    [DataContract(Name = "PaisBacen", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PaisBacen : Record, ICloneable, INotifyPropertyChanged
    {

        private Decimal? z_IdPais;
        [DbColumn("ID_PAIS")]
        [DataMember]
        
        public Decimal? IdPais
        {
            get { return z_IdPais; }
            set
            {
                if (z_IdPais != value)
                {
                    z_IdPais = value;
                    this.RaisePropertyChanged("IdPais");
                }
            }
        }

        private String z_CodigoBacen;
        [DbColumn("CODIGO_BACEN")]
        [DataMember]
        
        public String CodigoBacen
        {
            get { return z_CodigoBacen; }
            set
            {
                if (z_CodigoBacen != value)
                {
                    z_CodigoBacen = value;
                    this.RaisePropertyChanged("CodigoBacen");
                }
            }
        }

        private String z_NomePais;
        [DbColumn("NOME_PAIS")]
        [DataMember]
        
        public String NomePais
        {
            get { return z_NomePais; }
            set
            {
                if (z_NomePais != value)
                {
                    z_NomePais = value;
                    this.RaisePropertyChanged("NomePais");
                }
            }
        }

        private String z_SiglaResumida;
        [DbColumn("SIGLA_RESUMIDA")]
        [DataMember]
        
        public String SiglaResumida
        {
            get { return z_SiglaResumida; }
            set
            {
                if (z_SiglaResumida != value)
                {
                    z_SiglaResumida = value;
                    this.RaisePropertyChanged("SiglaResumida");
                }
            }
        }

        private String z_SiglaPais;
        [DbColumn("SIGLA_PAIS")]
        [DataMember]
        
        public String SiglaPais
        {
            get { return z_SiglaPais; }
            set
            {
                if (z_SiglaPais != value)
                {
                    z_SiglaPais = value;
                    this.RaisePropertyChanged("SiglaPais");
                }
            }
        }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PaisBacen value)
        {
            if (value == null)
                return false;
            return
				this.IdPais == value.IdPais &&
				this.CodigoBacen == value.CodigoBacen &&
				this.NomePais == value.NomePais &&
				this.SiglaResumida == value.SiglaResumida &&
				this.SiglaPais == value.SiglaPais;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PaisBacen CloneT()
        {
            PaisBacen value = new PaisBacen();
            value.RecordStatus = this.RecordStatus;
            value.RecordTag = this.RecordTag;

			value.IdPais = this.IdPais;
			value.CodigoBacen = this.CodigoBacen;
			value.NomePais = this.NomePais;
			value.SiglaResumida = this.SiglaResumida;
			value.SiglaPais = this.SiglaPais;

            return value;
        }

        #endregion Clone

        #region Create

        public static PaisBacen Create(Decimal _IdPais, String _CodigoBacen, String _NomePais, String _SiglaResumida, String _SiglaPais)
        {
            PaisBacen __value = new PaisBacen();

			__value.IdPais = _IdPais;
			__value.CodigoBacen = _CodigoBacen;
			__value.NomePais = _NomePais;
			__value.SiglaResumida = _SiglaResumida;
			__value.SiglaPais = _SiglaPais;

            return __value;
        }

        #endregion Create

   }

}
