// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "T2_OS", "X1_ID_OS", "SEQ_X1_ID_OS")]
    [Serializable]
    [DataContract(Name = "OrdemServico", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class OrdemServico : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// ColumnName: X1_ID_OS
        /// </summary>
        [DbColumn("X1_ID_OS")]
        
        public Decimal? Id { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_SIT_OS
        /// </summary>
        [DbColumn("X1_IDT_SIT_OS")]
        
        public Decimal? Situacao { get; set; }

        /// <summary>
        /// ColumnName: X1_PFX_TRE
        /// </summary>
        [DbColumn("X1_PFX_TRE")]
        
        public String Prefixo { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_PAR_PRV_GRA
        /// </summary>
        [DbColumn("X1_DAT_PAR_PRV_GRA")]
        
        public DateTime? DataPartidaPrevistaGrade { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_CHE_PRV
        /// </summary>
        [DbColumn("X1_DAT_CHE_PRV")]
        
        public DateTime? DataChegadaPrevistaOficial { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_DUR
        /// </summary>
        [DbColumn("X1_DAT_DUR")]
        
        public DateTime? Duracao { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_PAR_PRV_OFI
        /// </summary>
        [DbColumn("X1_DAT_PAR_PRV_OFI")]
        
        public DateTime? DataPartidaPrevistaOficial { get; set; }

        /// <summary>
        /// ColumnName: X1_QTD_VAG_VAZ
        /// </summary>
        [DbColumn("X1_QTD_VAG_VAZ")]
        
        public Int32? QtdeVagoesVazios { get; set; }

        /// <summary>
        /// ColumnName: X1_QTD_VAG_CAR
        /// </summary>
        [DbColumn("X1_QTD_VAG_CAR")]
        
        public Int32? QtdeVagoesCarregados { get; set; }

        /// <summary>
        /// ColumnName: X1_IND_GAR
        /// </summary>
        [DbColumn("X1_IND_GAR")]
        
        public String Garantido { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_FEC
        /// </summary>
        [DbColumn("X1_DAT_FEC")]
        
        public DateTime? DataFechamento { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_SUP
        /// </summary>
        [DbColumn("X1_DAT_SUP")]
        
        public DateTime? DataSupressao { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_USR_FEC
        /// </summary>
        [DbColumn("X1_IDT_USR_FEC")]
        
        public Decimal? X1IdtUsrFec { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_USR_SUP
        /// </summary>
        [DbColumn("X1_IDT_USR_SUP")]
        
        public Decimal? X1IdtUsrSup { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_EMP
        /// </summary>
        [DbColumn("X1_IDT_EMP")]
        
        public Decimal? X1IdtEmp { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_ROT
        /// </summary>
        [DbColumn("X1_IDT_ROT")]
        
        public Decimal? IdRota { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_EST_ORI
        /// </summary>
        [DbColumn("X1_IDT_EST_ORI")]
        
        public Decimal? IdOrigem { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_EST_DES
        /// </summary>
        [DbColumn("X1_IDT_EST_DES")]
        
        public Decimal? IdDestino { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_MOT_SUP
        /// </summary>
        [DbColumn("X1_IDT_MOT_SUP")]
        
        public Decimal? X1IdtMotSup { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_OBS
        /// </summary>
        [DbColumn("X1_IDT_OBS")]
        
        public Decimal? X1IdtObs { get; set; }

        /// <summary>
        /// ColumnName: X1_TIP_OS
        /// </summary>
        [DbColumn("X1_TIP_OS")]
        
        public Decimal? X1TipOs { get; set; }

        /// <summary>
        /// ColumnName: X1_NRO_OS
        /// </summary>
        [DbColumn("X1_NRO_OS")]
        
        public Decimal? Numero { get; set; }

        /// <summary>
        /// ColumnName: X1_CAT_TRE
        /// </summary>
        [DbColumn("X1_CAT_TRE")]
        
        public Decimal? X1CatTre { get; set; }

        /// <summary>
        /// ColumnName: X1_NRO_MAQ
        /// </summary>
        [DbColumn("X1_NRO_MAQ")]
        
        public Decimal? X1NroMaq { get; set; }

        /// <summary>
        /// ColumnName: X1_SIT_CTR
        /// </summary>
        [DbColumn("X1_SIT_CTR")]
        
        public String X1SitCtr { get; set; }

        /// <summary>
        /// ColumnName: X1_CLS_TRE
        /// </summary>
        [DbColumn("X1_CLS_TRE")]
        
        public Decimal? X1ClsTre { get; set; }

        /// <summary>
        /// ColumnName: X1_DAT_CHE_PRV_GRA
        /// </summary>
        [DbColumn("X1_DAT_CHE_PRV_GRA")]
        
        public DateTime? DataChegadaPrevistaGrade { get; set; }

        /// <summary>
        /// ColumnName: X1_IDT_OS_PRO
        /// </summary>
        [DbColumn("X1_IDT_OS_PRO")]
        
        public Decimal? IdPreOs { get; set; }

        /// <summary>
        /// ColumnName: X1_USR_CADASTRO
        /// </summary>
        [DbColumn("X1_USR_CADASTRO")]
        
        public String X1UsrCadastro { get; set; }

        /// <summary>
        /// ColumnName: X1_DT_CADASTRO
        /// </summary>
        [DbColumn("X1_DT_CADASTRO")]
        
        public DateTime? DataCadastro { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(OrdemServico value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.Situacao == value.Situacao &&
				this.Prefixo == value.Prefixo &&
				this.DataPartidaPrevistaGrade == value.DataPartidaPrevistaGrade &&
				this.DataChegadaPrevistaOficial == value.DataChegadaPrevistaOficial &&
				this.Duracao == value.Duracao &&
				this.DataPartidaPrevistaOficial == value.DataPartidaPrevistaOficial &&
				this.QtdeVagoesVazios == value.QtdeVagoesVazios &&
				this.QtdeVagoesCarregados == value.QtdeVagoesCarregados &&
				this.Garantido == value.Garantido &&
				this.DataFechamento == value.DataFechamento &&
				this.DataSupressao == value.DataSupressao &&
				this.X1IdtUsrFec == value.X1IdtUsrFec &&
				this.X1IdtUsrSup == value.X1IdtUsrSup &&
				this.X1IdtEmp == value.X1IdtEmp &&
				this.IdRota == value.IdRota &&
				this.IdOrigem == value.IdOrigem &&
				this.IdDestino == value.IdDestino &&
				this.X1IdtMotSup == value.X1IdtMotSup &&
				this.X1IdtObs == value.X1IdtObs &&
				this.X1TipOs == value.X1TipOs &&
				this.Numero == value.Numero &&
				this.X1CatTre == value.X1CatTre &&
				this.X1NroMaq == value.X1NroMaq &&
				this.X1SitCtr == value.X1SitCtr &&
				this.X1ClsTre == value.X1ClsTre &&
				this.DataChegadaPrevistaGrade == value.DataChegadaPrevistaGrade &&
				this.IdPreOs == value.IdPreOs &&
				this.X1UsrCadastro == value.X1UsrCadastro &&
				this.DataCadastro == value.DataCadastro;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public OrdemServico CloneT()
        {
            OrdemServico value = new OrdemServico();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.Situacao = this.Situacao;
			value.Prefixo = this.Prefixo;
			value.DataPartidaPrevistaGrade = this.DataPartidaPrevistaGrade;
			value.DataChegadaPrevistaOficial = this.DataChegadaPrevistaOficial;
			value.Duracao = this.Duracao;
			value.DataPartidaPrevistaOficial = this.DataPartidaPrevistaOficial;
			value.QtdeVagoesVazios = this.QtdeVagoesVazios;
			value.QtdeVagoesCarregados = this.QtdeVagoesCarregados;
			value.Garantido = this.Garantido;
			value.DataFechamento = this.DataFechamento;
			value.DataSupressao = this.DataSupressao;
			value.X1IdtUsrFec = this.X1IdtUsrFec;
			value.X1IdtUsrSup = this.X1IdtUsrSup;
			value.X1IdtEmp = this.X1IdtEmp;
			value.IdRota = this.IdRota;
			value.IdOrigem = this.IdOrigem;
			value.IdDestino = this.IdDestino;
			value.X1IdtMotSup = this.X1IdtMotSup;
			value.X1IdtObs = this.X1IdtObs;
			value.X1TipOs = this.X1TipOs;
			value.Numero = this.Numero;
			value.X1CatTre = this.X1CatTre;
			value.X1NroMaq = this.X1NroMaq;
			value.X1SitCtr = this.X1SitCtr;
			value.X1ClsTre = this.X1ClsTre;
			value.DataChegadaPrevistaGrade = this.DataChegadaPrevistaGrade;
			value.IdPreOs = this.IdPreOs;
			value.X1UsrCadastro = this.X1UsrCadastro;
			value.DataCadastro = this.DataCadastro;

            return value;
        }

        #endregion Clone

        #region Create

        public static OrdemServico Create(Decimal _Id, Decimal? _Situacao, String _Prefixo, DateTime? _DataPartidaPrevistaGrade, DateTime? _DataChegadaPrevistaOficial, DateTime? _Duracao, DateTime? _DataPartidaPrevistaOficial, Int32? _QtdeVagoesVazios, Int32? _QtdeVagoesCarregados, String _Garantido, DateTime? _DataFechamento, DateTime? _DataSupressao, Decimal? _X1IdtUsrFec, Decimal? _X1IdtUsrSup, Decimal? _X1IdtEmp, Decimal? _IdRota, Decimal? _IdOrigem, Decimal? _IdDestino, Decimal? _X1IdtMotSup, Decimal? _X1IdtObs, Decimal? _X1TipOs, Decimal? _Numero, Decimal? _X1CatTre, Decimal? _X1NroMaq, String _X1SitCtr, Decimal? _X1ClsTre, DateTime? _DataChegadaPrevistaGrade, Decimal? _IdPreOs, String _X1UsrCadastro, DateTime? _DataCadastro)
        {
            OrdemServico __value = new OrdemServico();

			__value.Id = _Id;
			__value.Situacao = _Situacao;
			__value.Prefixo = _Prefixo;
			__value.DataPartidaPrevistaGrade = _DataPartidaPrevistaGrade;
			__value.DataChegadaPrevistaOficial = _DataChegadaPrevistaOficial;
			__value.Duracao = _Duracao;
			__value.DataPartidaPrevistaOficial = _DataPartidaPrevistaOficial;
			__value.QtdeVagoesVazios = _QtdeVagoesVazios;
			__value.QtdeVagoesCarregados = _QtdeVagoesCarregados;
			__value.Garantido = _Garantido;
			__value.DataFechamento = _DataFechamento;
			__value.DataSupressao = _DataSupressao;
			__value.X1IdtUsrFec = _X1IdtUsrFec;
			__value.X1IdtUsrSup = _X1IdtUsrSup;
			__value.X1IdtEmp = _X1IdtEmp;
			__value.IdRota = _IdRota;
			__value.IdOrigem = _IdOrigem;
			__value.IdDestino = _IdDestino;
			__value.X1IdtMotSup = _X1IdtMotSup;
			__value.X1IdtObs = _X1IdtObs;
			__value.X1TipOs = _X1TipOs;
			__value.Numero = _Numero;
			__value.X1CatTre = _X1CatTre;
			__value.X1NroMaq = _X1NroMaq;
			__value.X1SitCtr = _X1SitCtr;
			__value.X1ClsTre = _X1ClsTre;
			__value.DataChegadaPrevistaGrade = _DataChegadaPrevistaGrade;
			__value.IdPreOs = _IdPreOs;
			__value.X1UsrCadastro = _X1UsrCadastro;
			__value.DataCadastro = _DataCadastro;

            return __value;
        }

        #endregion Create

   }

}
