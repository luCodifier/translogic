// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "TRECHOS_SB_RESTRITOS_FROTAS", "", "")]
    [Serializable]
    [DataContract(Name = "TrechosSbRestritosFrotas", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class TrechosSbRestritosFrotas : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ID_SB_FROTA
        /// </summary>
        [DbColumn("ID_SB_FROTA")]
        
        public Decimal? IdSbFrota { get; set; }

        /// <summary>
        /// Column ID_TRECHO
        /// </summary>
        [DbColumn("ID_TRECHO")]
        
        public Decimal? IdTrecho { get; set; }

        /// <summary>
        /// Column AO_ID_AO
        /// </summary>
        [DbColumn("AO_ID_AO")]
        
        public Decimal? AoIdAo { get; set; }

        /// <summary>
        /// Column AO_COD_AOP
        /// </summary>
        [DbColumn("AO_COD_AOP")]
        
        public String AoCodAop { get; set; }

        /// <summary>
        /// Column ID_FROTA
        /// </summary>
        [DbColumn("ID_FROTA")]
        
        public Decimal? IdFrota { get; set; }

        /// <summary>
        /// Column DT_TIMESTAMP
        /// </summary>
        [DbColumn("DT_TIMESTAMP")]
        
        public DateTime? DtTimestamp { get; set; }

        /// <summary>
        /// Column IND_ATIVO
        /// </summary>
        [DbColumn("IND_ATIVO")]
        
        public String IndAtivo { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(TrechosSbRestritosFrotas value)
        {
            if (value == null)
                return false;
            return
				this.IdSbFrota == value.IdSbFrota &&
				this.IdTrecho == value.IdTrecho &&
				this.AoIdAo == value.AoIdAo &&
				this.AoCodAop == value.AoCodAop &&
				this.IdFrota == value.IdFrota &&
				this.DtTimestamp == value.DtTimestamp &&
				this.IndAtivo == value.IndAtivo;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public TrechosSbRestritosFrotas CloneT()
        {
            TrechosSbRestritosFrotas value = new TrechosSbRestritosFrotas();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.IdSbFrota = this.IdSbFrota;
			value.IdTrecho = this.IdTrecho;
			value.AoIdAo = this.AoIdAo;
			value.AoCodAop = this.AoCodAop;
			value.IdFrota = this.IdFrota;
			value.DtTimestamp = this.DtTimestamp;
			value.IndAtivo = this.IndAtivo;

            return value;
        }

        #endregion Clone

        #region Create

        public static TrechosSbRestritosFrotas Create(Decimal? _IdSbFrota, Decimal? _IdTrecho, Decimal? _AoIdAo, String _AoCodAop, Decimal? _IdFrota, DateTime? _DtTimestamp, String _IndAtivo)
        {
            TrechosSbRestritosFrotas __value = new TrechosSbRestritosFrotas();

			__value.IdSbFrota = _IdSbFrota;
			__value.IdTrecho = _IdTrecho;
			__value.AoIdAo = _AoIdAo;
			__value.AoCodAop = _AoCodAop;
			__value.IdFrota = _IdFrota;
			__value.DtTimestamp = _DtTimestamp;
			__value.IndAtivo = _IndAtivo;

            return __value;
        }

        #endregion Create

   }

}
