// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "SERIE_DESP_UF", "SD_IDT_SUF", "SERIE_DESP_UF_SEQ_ID")]
    [Serializable]
    [DataContract(Name = "SerieDespUf", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class SerieDespUf : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SD_IDT_SUF
        /// </summary>
        [DbColumn("SD_IDT_SUF")]
        
        public Decimal? Id { get; set; }

        /// <summary>
        /// Column EP_ID_EMP
        /// </summary>
        [DbColumn("EP_ID_EMP")]
        
        public Decimal? IdEmpresa { get; set; }

        /// <summary>
        /// Column SD_NUM_SUF
        /// </summary>
        [DbColumn("SD_NUM_SUF")]
        
        public Decimal? NumeroSerieDespacho { get; set; }

        /// <summary>
        /// Column SD_COD_SUF
        /// </summary>
        [DbColumn("SD_COD_SUF")]
        
        public String CodigoSerieDespacho { get; set; }

        /// <summary>
        /// Column SD_IND_ATIV
        /// </summary>
        [DbColumn("SD_IND_ATIV")]
        
        public String IndicadorSerie { get; set; }

        /// <summary>
        /// Column SD_DAT_INI
        /// </summary>
        [DbColumn("SD_DAT_INI")]
        
        public DateTime? DataInicioSerie { get; set; }

        /// <summary>
        /// Column SD_COD_UF
        /// </summary>
        [DbColumn("SD_COD_UF")]
        
        public String CodUnidadeFederativaFerrovia { get; set; }

        /// <summary>
        /// Column SD_COD_CTRL
        /// </summary>
        [DbColumn("SD_COD_CTRL")]
        
        public String CodigoControle { get; set; }

        /// <summary>
        /// Column SD_TIMESTAMP
        /// </summary>
        [DbColumn("SD_TIMESTAMP")]
        
        public DateTime? DataCadastro { get; set; }

        /// <summary>
        /// Column CNPJ_FERROVIA
        /// </summary>
        [DbColumn("CNPJ_FERROVIA")]
        
        public String CnpjFerrovia { get; set; }

        /// <summary>
        /// Column CNPJ_HOLDING
        /// </summary>
        [DbColumn("CNPJ_HOLDING")]
        
        public String CnpjHolding { get; set; }

        /// <summary>
        /// Column LETRA_FERROVIA
        /// </summary>
        [DbColumn("LETRA_FERROVIA")]
        
        public String LetraFerrovia { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(SerieDespUf value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.IdEmpresa == value.IdEmpresa &&
				this.NumeroSerieDespacho == value.NumeroSerieDespacho &&
				this.CodigoSerieDespacho == value.CodigoSerieDespacho &&
				this.IndicadorSerie == value.IndicadorSerie &&
				this.DataInicioSerie == value.DataInicioSerie &&
				this.CodUnidadeFederativaFerrovia == value.CodUnidadeFederativaFerrovia &&
				this.CodigoControle == value.CodigoControle &&
				this.DataCadastro == value.DataCadastro &&
				this.CnpjFerrovia == value.CnpjFerrovia &&
				this.CnpjHolding == value.CnpjHolding &&
				this.LetraFerrovia == value.LetraFerrovia;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public SerieDespUf CloneT()
        {
            SerieDespUf value = new SerieDespUf();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.IdEmpresa = this.IdEmpresa;
			value.NumeroSerieDespacho = this.NumeroSerieDespacho;
			value.CodigoSerieDespacho = this.CodigoSerieDespacho;
			value.IndicadorSerie = this.IndicadorSerie;
			value.DataInicioSerie = this.DataInicioSerie;
			value.CodUnidadeFederativaFerrovia = this.CodUnidadeFederativaFerrovia;
			value.CodigoControle = this.CodigoControle;
			value.DataCadastro = this.DataCadastro;
			value.CnpjFerrovia = this.CnpjFerrovia;
			value.CnpjHolding = this.CnpjHolding;
			value.LetraFerrovia = this.LetraFerrovia;

            return value;
        }

        #endregion Clone

        #region Create

        public static SerieDespUf Create(Decimal _Id, Decimal _IdEmpresa, Decimal? _NumeroSerieDespacho, String _CodigoSerieDespacho, String _IndicadorSerie, DateTime? _DataInicioSerie, String _CodUnidadeFederativaFerrovia, String _CodigoControle, DateTime? _DataCadastro, String _CnpjFerrovia, String _CnpjHolding, String _LetraFerrovia)
        {
            SerieDespUf __value = new SerieDespUf();

			__value.Id = _Id;
			__value.IdEmpresa = _IdEmpresa;
			__value.NumeroSerieDespacho = _NumeroSerieDespacho;
			__value.CodigoSerieDespacho = _CodigoSerieDespacho;
			__value.IndicadorSerie = _IndicadorSerie;
			__value.DataInicioSerie = _DataInicioSerie;
			__value.CodUnidadeFederativaFerrovia = _CodUnidadeFederativaFerrovia;
			__value.CodigoControle = _CodigoControle;
			__value.DataCadastro = _DataCadastro;
			__value.CnpjFerrovia = _CnpjFerrovia;
			__value.CnpjHolding = _CnpjHolding;
			__value.LetraFerrovia = _LetraFerrovia;

            return __value;
        }

        #endregion Create

   }

}
