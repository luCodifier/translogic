// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "GERENCIAMENTO_DOC_COMPVAG", "ID_GERENCIAMENTO_DOC_COMPVAG", "GER_DOC_COMPVAG_SEQ_ID")]
    [Serializable]
    [DataContract(Name = "GerenciamentoDocCompvag", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class GerenciamentoDocCompvag : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// ColumnName: ID_GERENCIAMENTO_DOC_COMPVAG
        /// </summary>
        [DbColumn("ID_GERENCIAMENTO_DOC_COMPVAG")]
        
        public Decimal? Id { get; set; }

        /// <summary>
        /// ColumnName: ID_GERENCIAMENTO_DOC
        /// </summary>
        [DbColumn("ID_GERENCIAMENTO_DOC")]
        
        public Decimal? GerenciamentoDocId { get; set; }

        /// <summary>
        /// ColumnName: ID_OS
        /// </summary>
        [DbColumn("ID_OS")]
        
        public Decimal? OsId { get; set; }

        /// <summary>
        /// ColumnName: ID_COMPOSICAO
        /// </summary>
        [DbColumn("ID_COMPOSICAO")]
        
        public Decimal? ComposicaoId { get; set; }

        /// <summary>
        /// ColumnName: ID_RECEBEDOR
        /// </summary>
        [DbColumn("ID_RECEBEDOR")]
        
        public Decimal? RecebedorId { get; set; }

        /// <summary>
        /// ColumnName: CODIGO_VAGAO
        /// </summary>
        [DbColumn("CODIGO_VAGAO")]
        
        public String CodigoVagao { get; set; }

        /// <summary>
        /// ColumnName: CRIADO_EM
        /// </summary>
        [DbColumn("CRIADO_EM")]
        
        public DateTime? CriadoEm { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(GerenciamentoDocCompvag value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.GerenciamentoDocId == value.GerenciamentoDocId &&
				this.OsId == value.OsId &&
				this.ComposicaoId == value.ComposicaoId &&
				this.RecebedorId == value.RecebedorId &&
				this.CodigoVagao == value.CodigoVagao &&
				this.CriadoEm == value.CriadoEm;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public GerenciamentoDocCompvag CloneT()
        {
            GerenciamentoDocCompvag value = new GerenciamentoDocCompvag();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.GerenciamentoDocId = this.GerenciamentoDocId;
			value.OsId = this.OsId;
			value.ComposicaoId = this.ComposicaoId;
			value.RecebedorId = this.RecebedorId;
			value.CodigoVagao = this.CodigoVagao;
			value.CriadoEm = this.CriadoEm;

            return value;
        }

        #endregion Clone

        #region Create

        public static GerenciamentoDocCompvag Create(Decimal _Id, Decimal _GerenciamentoDocId, Decimal _OsId, Decimal _ComposicaoId, Decimal _RecebedorId, String _CodigoVagao, DateTime _CriadoEm)
        {
            GerenciamentoDocCompvag __value = new GerenciamentoDocCompvag();

			__value.Id = _Id;
			__value.GerenciamentoDocId = _GerenciamentoDocId;
			__value.OsId = _OsId;
			__value.ComposicaoId = _ComposicaoId;
			__value.RecebedorId = _RecebedorId;
			__value.CodigoVagao = _CodigoVagao;
			__value.CriadoEm = _CriadoEm;

            return __value;
        }

        #endregion Create

   }

}
