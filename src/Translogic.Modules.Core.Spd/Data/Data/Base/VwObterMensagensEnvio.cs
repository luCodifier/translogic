// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Core.Spd.Data
{

    [DbTable("", "VW_OBTER_MENSAGENS_ENVIO", "", "")]
    [Serializable]
    [DataContract(Name = "VwObterMensagensEnvio", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwObterMensagensEnvio : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// ColumnName: ID_MENSAGEM_ENVIO
        /// </summary>
        [DbColumn("ID_MENSAGEM_ENVIO")]
        
        public Decimal? IdMensagemEnvio { get; set; }

        /// <summary>
        /// ColumnName: TIPO_LOG_MENSAGEM
        /// </summary>
        [DbColumn("TIPO_LOG_MENSAGEM")]
        
        public String TipoLogMensagem { get; set; }

        /// <summary>
        /// ColumnName: STATUS_RETORNO
        /// </summary>
        [DbColumn("STATUS_RETORNO")]
        
        public String StatusRetorno { get; set; }

        /// <summary>
        /// ColumnName: STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public Decimal? Status { get; set; }

        /// <summary>
        /// ColumnName: STATUS_DESCRICAO
        /// </summary>
        [DbColumn("STATUS_DESCRICAO")]
        
        public String StatusDescricao { get; set; }

        /// <summary>
        /// ColumnName: ID_CTE
        /// </summary>
        [DbColumn("ID_CTE")]
        
        public Decimal? IdCte { get; set; }

        /// <summary>
        /// ColumnName: SITUACAO_ATUAL
        /// </summary>
        [DbColumn("SITUACAO_ATUAL")]
        
        public String SituacaoAtual { get; set; }

        /// <summary>
        /// ColumnName: SERIE_CTE
        /// </summary>
        [DbColumn("SERIE_CTE")]
        
        public String SerieCte { get; set; }

        /// <summary>
        /// ColumnName: NUMERO_CTE
        /// </summary>
        [DbColumn("NUMERO_CTE")]
        
        public String NumeroCte { get; set; }

        /// <summary>
        /// ColumnName: CHAVE_CTE
        /// </summary>
        [DbColumn("CHAVE_CTE")]
        
        public String ChaveCte { get; set; }

        /// <summary>
        /// ColumnName: PESO_PARA_CALCULO
        /// </summary>
        [DbColumn("PESO_PARA_CALCULO")]
        
        public Double? PesoParaCalculo { get; set; }

        /// <summary>
        /// ColumnName: PROTOCOLO
        /// </summary>
        [DbColumn("PROTOCOLO")]
        
        public String Protocolo { get; set; }

        /// <summary>
        /// ColumnName: PROTOCOLO_MRS
        /// </summary>
        [DbColumn("PROTOCOLO_MRS")]
        
        public String ProtocoloMrs { get; set; }

        /// <summary>
        /// ColumnName: ORIGEM
        /// </summary>
        [DbColumn("ORIGEM")]
        
        public String Origem { get; set; }

        /// <summary>
        /// ColumnName: DESTINO
        /// </summary>
        [DbColumn("DESTINO")]
        
        public String Destino { get; set; }

        /// <summary>
        /// ColumnName: VAGAO
        /// </summary>
        [DbColumn("VAGAO")]
        
        public String Vagao { get; set; }

        /// <summary>
        /// ColumnName: ID_VAGAO
        /// </summary>
        [DbColumn("ID_VAGAO")]
        
        public Decimal? IdVagao { get; set; }

        /// <summary>
        /// ColumnName: SERIE_VAGAO
        /// </summary>
        [DbColumn("SERIE_VAGAO")]
        
        public String SerieVagao { get; set; }

        /// <summary>
        /// ColumnName: CODIGO_FLUXO
        /// </summary>
        [DbColumn("CODIGO_FLUXO")]
        
        public String CodigoFluxo { get; set; }

        /// <summary>
        /// ColumnName: ID_DESPACHO_TRANSLOGIC
        /// </summary>
        [DbColumn("ID_DESPACHO_TRANSLOGIC")]
        
        public Decimal? IdDespachoTranslogic { get; set; }

        /// <summary>
        /// ColumnName: PREFIXO
        /// </summary>
        [DbColumn("PREFIXO")]
        
        public String Prefixo { get; set; }

        /// <summary>
        /// ColumnName: SEQUENCIA_VAGAO
        /// </summary>
        [DbColumn("SEQUENCIA_VAGAO")]
        
        public Int32? SequenciaVagao { get; set; }

        /// <summary>
        /// ColumnName: MERCADORIA
        /// </summary>
        [DbColumn("MERCADORIA")]
        
        public String Mercadoria { get; set; }

        /// <summary>
        /// ColumnName: EMPRESA_REMETENTE
        /// </summary>
        [DbColumn("EMPRESA_REMETENTE")]
        
        public String EmpresaRemetente { get; set; }

        /// <summary>
        /// ColumnName: EMPRESA_DESTINATARIA
        /// </summary>
        [DbColumn("EMPRESA_DESTINATARIA")]
        
        public String EmpresaDestinataria { get; set; }

        /// <summary>
        /// ColumnName: EMPRESA_PAGADORA
        /// </summary>
        [DbColumn("EMPRESA_PAGADORA")]
        
        public String EmpresaPagadora { get; set; }

        /// <summary>
        /// ColumnName: SERIE_DESPACHO
        /// </summary>
        [DbColumn("SERIE_DESPACHO")]
        
        public String SerieDespacho { get; set; }

        /// <summary>
        /// ColumnName: DESPACHO
        /// </summary>
        [DbColumn("DESPACHO")]
        
        public Decimal? Despacho { get; set; }

        /// <summary>
        /// ColumnName: DATA_DESPACHO
        /// </summary>
        [DbColumn("DATA_DESPACHO")]
        
        public DateTime? DataDespacho { get; set; }

        /// <summary>
        /// ColumnName: DATA_ENVIO
        /// </summary>
        [DbColumn("DATA_ENVIO")]
        
        public DateTime? DataEnvio { get; set; }

        /// <summary>
        /// ColumnName: STATUS_RECIBO
        /// </summary>
        [DbColumn("STATUS_RECIBO")]
        
        public String StatusRecibo { get; set; }

        /// <summary>
        /// ColumnName: DATA_CADASTRO
        /// </summary>
        [DbColumn("DATA_CADASTRO")]
        
        public DateTime? DataCadastro { get; set; }

        /// <summary>
        /// ColumnName: VAGAO_CODIGO
        /// </summary>
        [DbColumn("VAGAO_CODIGO")]
        
        public String VagaoCodigo { get; set; }

        /// <summary>
        /// ColumnName: OS_NUMERO
        /// </summary>
        [DbColumn("OS_NUMERO")]
        
        public Decimal? OsNumero { get; set; }

        /// <summary>
        /// ColumnName: INTERF_ENVIO_ID
        /// </summary>
        [DbColumn("INTERF_ENVIO_ID")]
        
        public Decimal? InterfEnvioId { get; set; }

        /// <summary>
        /// ColumnName: NUMERO_DESPACHO
        /// </summary>
        [DbColumn("NUMERO_DESPACHO")]
        
        public Int32? NumeroDespacho { get; set; }

        /// <summary>
        /// ColumnName: VALORES
        /// </summary>
        [DbColumn("VALORES")]

        public string Valores { get; set; }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwObterMensagensEnvio value)
        {
            if (value == null)
                return false;
            return
				this.IdMensagemEnvio == value.IdMensagemEnvio &&
				this.TipoLogMensagem == value.TipoLogMensagem &&
				this.StatusRetorno == value.StatusRetorno &&
				this.Status == value.Status &&
				this.StatusDescricao == value.StatusDescricao &&
				this.IdCte == value.IdCte &&
				this.SituacaoAtual == value.SituacaoAtual &&
				this.SerieCte == value.SerieCte &&
				this.NumeroCte == value.NumeroCte &&
				this.ChaveCte == value.ChaveCte &&
				this.PesoParaCalculo == value.PesoParaCalculo &&
				this.Protocolo == value.Protocolo &&
				this.ProtocoloMrs == value.ProtocoloMrs &&
				this.Origem == value.Origem &&
				this.Destino == value.Destino &&
				this.Vagao == value.Vagao &&
				this.IdVagao == value.IdVagao &&
				this.SerieVagao == value.SerieVagao &&
				this.CodigoFluxo == value.CodigoFluxo &&
				this.IdDespachoTranslogic == value.IdDespachoTranslogic &&
				this.Prefixo == value.Prefixo &&
				this.SequenciaVagao == value.SequenciaVagao &&
				this.Mercadoria == value.Mercadoria &&
				this.EmpresaRemetente == value.EmpresaRemetente &&
				this.EmpresaDestinataria == value.EmpresaDestinataria &&
				this.EmpresaPagadora == value.EmpresaPagadora &&
				this.SerieDespacho == value.SerieDespacho &&
				this.Despacho == value.Despacho &&
				this.DataDespacho == value.DataDespacho &&
				this.DataEnvio == value.DataEnvio &&
				this.StatusRecibo == value.StatusRecibo &&
				this.DataCadastro == value.DataCadastro &&
				this.VagaoCodigo == value.VagaoCodigo &&
				this.OsNumero == value.OsNumero &&
				this.InterfEnvioId == value.InterfEnvioId &&
				this.NumeroDespacho == value.NumeroDespacho;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwObterMensagensEnvio CloneT()
        {
            VwObterMensagensEnvio value = new VwObterMensagensEnvio();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.IdMensagemEnvio = this.IdMensagemEnvio;
			value.TipoLogMensagem = this.TipoLogMensagem;
			value.StatusRetorno = this.StatusRetorno;
			value.Status = this.Status;
			value.StatusDescricao = this.StatusDescricao;
			value.IdCte = this.IdCte;
			value.SituacaoAtual = this.SituacaoAtual;
			value.SerieCte = this.SerieCte;
			value.NumeroCte = this.NumeroCte;
			value.ChaveCte = this.ChaveCte;
			value.PesoParaCalculo = this.PesoParaCalculo;
			value.Protocolo = this.Protocolo;
			value.ProtocoloMrs = this.ProtocoloMrs;
			value.Origem = this.Origem;
			value.Destino = this.Destino;
			value.Vagao = this.Vagao;
			value.IdVagao = this.IdVagao;
			value.SerieVagao = this.SerieVagao;
			value.CodigoFluxo = this.CodigoFluxo;
			value.IdDespachoTranslogic = this.IdDespachoTranslogic;
			value.Prefixo = this.Prefixo;
			value.SequenciaVagao = this.SequenciaVagao;
			value.Mercadoria = this.Mercadoria;
			value.EmpresaRemetente = this.EmpresaRemetente;
			value.EmpresaDestinataria = this.EmpresaDestinataria;
			value.EmpresaPagadora = this.EmpresaPagadora;
			value.SerieDespacho = this.SerieDespacho;
			value.Despacho = this.Despacho;
			value.DataDespacho = this.DataDespacho;
			value.DataEnvio = this.DataEnvio;
			value.StatusRecibo = this.StatusRecibo;
			value.DataCadastro = this.DataCadastro;
			value.VagaoCodigo = this.VagaoCodigo;
			value.OsNumero = this.OsNumero;
			value.InterfEnvioId = this.InterfEnvioId;
			value.NumeroDespacho = this.NumeroDespacho;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwObterMensagensEnvio Create(Decimal _IdMensagemEnvio, String _TipoLogMensagem, String _StatusRetorno, Decimal? _Status, String _StatusDescricao, Decimal? _IdCte, String _SituacaoAtual, String _SerieCte, String _NumeroCte, String _ChaveCte, Double? _PesoParaCalculo, String _Protocolo, String _ProtocoloMrs, String _Origem, String _Destino, String _Vagao, Decimal? _IdVagao, String _SerieVagao, String _CodigoFluxo, Decimal? _IdDespachoTranslogic, String _Prefixo, Int32? _SequenciaVagao, String _Mercadoria, String _EmpresaRemetente, String _EmpresaDestinataria, String _EmpresaPagadora, String _SerieDespacho, Decimal? _Despacho, DateTime? _DataDespacho, DateTime? _DataEnvio, String _StatusRecibo, DateTime? _DataCadastro, String _VagaoCodigo, Decimal? _OsNumero, Decimal? _InterfEnvioId, Int32? _NumeroDespacho)
        {
            VwObterMensagensEnvio __value = new VwObterMensagensEnvio();

			__value.IdMensagemEnvio = _IdMensagemEnvio;
			__value.TipoLogMensagem = _TipoLogMensagem;
			__value.StatusRetorno = _StatusRetorno;
			__value.Status = _Status;
			__value.StatusDescricao = _StatusDescricao;
			__value.IdCte = _IdCte;
			__value.SituacaoAtual = _SituacaoAtual;
			__value.SerieCte = _SerieCte;
			__value.NumeroCte = _NumeroCte;
			__value.ChaveCte = _ChaveCte;
			__value.PesoParaCalculo = _PesoParaCalculo;
			__value.Protocolo = _Protocolo;
			__value.ProtocoloMrs = _ProtocoloMrs;
			__value.Origem = _Origem;
			__value.Destino = _Destino;
			__value.Vagao = _Vagao;
			__value.IdVagao = _IdVagao;
			__value.SerieVagao = _SerieVagao;
			__value.CodigoFluxo = _CodigoFluxo;
			__value.IdDespachoTranslogic = _IdDespachoTranslogic;
			__value.Prefixo = _Prefixo;
			__value.SequenciaVagao = _SequenciaVagao;
			__value.Mercadoria = _Mercadoria;
			__value.EmpresaRemetente = _EmpresaRemetente;
			__value.EmpresaDestinataria = _EmpresaDestinataria;
			__value.EmpresaPagadora = _EmpresaPagadora;
			__value.SerieDespacho = _SerieDespacho;
			__value.Despacho = _Despacho;
			__value.DataDespacho = _DataDespacho;
			__value.DataEnvio = _DataEnvio;
			__value.StatusRecibo = _StatusRecibo;
			__value.DataCadastro = _DataCadastro;
			__value.VagaoCodigo = _VagaoCodigo;
			__value.OsNumero = _OsNumero;
			__value.InterfEnvioId = _InterfEnvioId;
			__value.NumeroDespacho = _NumeroDespacho;

            return __value;
        }

        #endregion Create

   }

}
