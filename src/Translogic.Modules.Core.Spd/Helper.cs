﻿using Speed.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Translogic.Modules.Core.Spd
{

    /// <summary>
    /// Classe de métodos úteis
    /// </summary>
    public static class Helper
    {

        /// <summary>
        /// Formata uma lista e retorna um List<string>
        /// </summary>
        /// <param name="valores"></param>
        /// <returns>Lista de valores</returns>
        public static List<string> FormataLista(string valores)
        {
            var vals = valores.Trim().ToUpper()
                             .Split(new char[] { ';', ',', ' ', '|', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries)
                             .ToList();
            return vals;
        }

        /// <summary>
        /// Formata uma lista e retorna um List<string>
        /// </summary>
        /// <param name="valores"></param>
        /// <returns>Valores formatados separados por ','</returns>
        public static string FormataListaStr(string valores, string separador = ",", bool addAspas = false, bool distinct = true)
        {
            var lista = FormataLista(valores);
            if (distinct)
                lista = lista.Distinct(p => p).ToList();
            if (addAspas)
                return string.Join(separador, lista.Select(p => "'" + p + "'"));
            else
                return string.Join(separador, lista);
        }

        /// <summary>
        /// Formata CNPJ
        /// </summary>
        /// <param name="cnpj"></param>
        /// <returns></returns>
        public static string FormataCnpj(string cnpj)
        {
            var resultado = String.Empty;
            if (!String.IsNullOrEmpty(cnpj))
            {
                cnpj = String.Format("{0, 14:D14}", cnpj.Replace(".", String.Empty).Replace("/", String.Empty).Replace("-", String.Empty));
                resultado = String.Format(
                    "{0}.{1}.{2}/{3}-{4}",
                    cnpj.Substring(0, 2),
                    cnpj.Substring(2, 3),
                    cnpj.Substring(5, 3),
                    cnpj.Substring(8, 4),
                    cnpj.Substring(12, 2));
            }

            return resultado;
        }

        /// <summary>
        /// passa todas as propriedades string pra maiúsculas
        /// </summary>
        /// <param name="obj"></param>
        public static void ToUpperCase(object obj)
        {
            if (obj == null)
                return;

            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.DeclaringType == typeof(string))
                    prop.SetValue(obj, ((string)prop.GetValue(obj, null)).ToUpper(), null);
            }

        }

        public static string OracleDate(this DateTime date)
        {
            return $"TO_DATE('{date.ToString("yyyy-MM-dd HH:mm:ss")}', 'YYYY-MM-DD HH24:MI:SS')";
        }
        public static string OracleDate(this DateTime? date)
        {
            return $"TO_DATE('{date.Value.ToString("yyyy-MM-dd HH:mm:ss")}', 'YYYY-MM-DD HH24:MI:SS')";
        }

        public static string ToISO_8859_1(string text)
        {
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(text);
            byte[] isoBytes = Encoding.Convert(utf8, iso, utfBytes);
            string msg = iso.GetString(isoBytes);
            return msg;
        }

        public static T DeserializeXml<T>(string xml)
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (XmlReader reader = XmlReader.Create(xml))
            {
                return (T)ser.Deserialize(reader);
            }
        }

        public static dynamic DeserializeXml(string xml)
        {
            return DeserializeXml<dynamic>(xml);
        }

        public static string SerializeXml(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            Encoding Utf8 = new UTF8Encoding(false);
            XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
            XmlSerializer xsSubmit = new XmlSerializer(obj.GetType());
            using (MemoryStream output = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(output, settings))
                {
                    xsSubmit.Serialize(writer, obj);
                }
                output.Position = 0;
                return Utf8.GetString(output.ToArray());
            }
        }

        public static List<TSource> FilterArray<TSource>(this IEnumerable<TSource> source, Func<TSource, string> keySelector, string[] values)
        {
            List<TSource> list = new List<TSource>();

            if (source == null || !source.Any())
            {
                return list;
            }

            if (values == null || !values.Any())
            {
                return source.ToList();
            }

            values = values.Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim().ToUpper()).ToArray();

            foreach (var item in source)
            {
                var v = keySelector(item);
                if (v != null)
                {
                    if (values.Contains(v))
                    {
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public static string[] ParseCommandLine(string[] args)
        {
            if (args != null || args.Any())
            {
                args = args.Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Replace(",", " ").Replace(";", " "))
                    .Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim().ToUpperInvariant()).ToArray();
            }

            return args;
        }

        public static List<TSource> FilterArray<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, TKey[] values)
        {
            List<TSource> list = new List<TSource>();

            if (source == null || !source.Any())
            {
                return list;
            }

            if (values == null || !values.Any())
            {
                return source.ToList();
            }

            values = values.Where(p => p != null).ToArray();

            foreach (var item in source)
            {
                var v = keySelector(item);
                if (v != null)
                {
                    if (values.Contains(v))
                    {
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public static bool IsRunningInstance()
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);
            return processes.Any(p => p.Id != current.Id && current.ProcessName == p.ProcessName);
        }

        public static string ChangePath(string oldDir, string newDir, string filename)
        {
            string newFilename = filename.Remove(0, oldDir.Length + 1);
            newFilename = Path.Combine(newDir, newFilename);
            return newFilename;
        }

        public static void CreateDirectory(params string[] dirs)
        {
            foreach (var dir in dirs)
            {
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
        }

        /// <summary>
        /// Apaga sub-diretórios vazios
        /// </summary>
        /// <param name="dir"></param>
        public static void ApagarSubdiretoriosVazios(string dir)
        {
            var dirs = Directory.GetDirectories(dir, "*.*", SearchOption.AllDirectories).ToList();
            // apaga os diretórios vazios
            foreach (var _dir in dirs)
            {
                try
                {
                    if (!Directory.GetDirectories(_dir).Any() && !Directory.GetFiles(_dir).Any())
                    {
                        Directory.Delete(_dir);
                    }
                }
                catch { }
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
