// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrTerminalDescarga : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga SelectByPk(Database db, Decimal? _PtdId)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga>(string.Format("PTD_ID={0}", _PtdId));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga SelectByPk(Decimal? _PtdId)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga>(string.Format("PTD_ID={0}", _PtdId));
        }

        public static int DeleteByPk(Database db, Decimal? _PtdId)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga>(string.Format("PTD_ID={0}", _PtdId));
        }
        public static int DeleteByPk(Decimal? _PtdId)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrTerminalDescarga>(string.Format("PTD_ID={0}", _PtdId));
        }



    }

}
