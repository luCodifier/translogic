// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_CaallXrefProduto : BLClass<Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto>
    {

        public static Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto SelectByPk(Database db, String _CdProdutoCaall, Decimal? _CdProdutoFammpatio)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto>(string.Format("CD_PRODUTO_CAALL={0} AND CD_PRODUTO_FAMMPATIO={1}", Q(_CdProdutoCaall), _CdProdutoFammpatio));
        }
        public static Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto SelectByPk(String _CdProdutoCaall, Decimal? _CdProdutoFammpatio)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto>(string.Format("CD_PRODUTO_CAALL={0} AND CD_PRODUTO_FAMMPATIO={1}", Q(_CdProdutoCaall), _CdProdutoFammpatio));
        }

        public static int DeleteByPk(Database db, String _CdProdutoCaall, Decimal? _CdProdutoFammpatio)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto>(string.Format("CD_PRODUTO_CAALL={0} AND CD_PRODUTO_FAMMPATIO={1}", Q(_CdProdutoCaall), _CdProdutoFammpatio));
        }
        public static int DeleteByPk(String _CdProdutoCaall, Decimal? _CdProdutoFammpatio)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.CaallXrefProduto>(string.Format("CD_PRODUTO_CAALL={0} AND CD_PRODUTO_FAMMPATIO={1}", Q(_CdProdutoCaall), _CdProdutoFammpatio));
        }



    }

}
