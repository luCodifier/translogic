// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_Role : BLClass<Translogic.Modules.Sispat.Spd.Data.Role>
    {

        public static Translogic.Modules.Sispat.Spd.Data.Role SelectByPk(Database db, Int64? _Roleid)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.Role>(string.Format("ROLEID={0}", _Roleid));
        }
        public static Translogic.Modules.Sispat.Spd.Data.Role SelectByPk(Int64? _Roleid)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.Role>(string.Format("ROLEID={0}", _Roleid));
        }

        public static int DeleteByPk(Database db, Int64? _Roleid)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.Role>(string.Format("ROLEID={0}", _Roleid));
        }
        public static int DeleteByPk(Int64? _Roleid)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.Role>(string.Format("ROLEID={0}", _Roleid));
        }



    }

}
