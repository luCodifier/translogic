// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrImpostos : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrImpostos>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrImpostos SelectByPk(Database db, String _ImpostoSigla)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrImpostos>(string.Format("IMPOSTO_SIGLA={0}", Q(_ImpostoSigla)));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrImpostos SelectByPk(String _ImpostoSigla)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrImpostos>(string.Format("IMPOSTO_SIGLA={0}", Q(_ImpostoSigla)));
        }

        public static int DeleteByPk(Database db, String _ImpostoSigla)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrImpostos>(string.Format("IMPOSTO_SIGLA={0}", Q(_ImpostoSigla)));
        }
        public static int DeleteByPk(String _ImpostoSigla)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrImpostos>(string.Format("IMPOSTO_SIGLA={0}", Q(_ImpostoSigla)));
        }



    }

}
