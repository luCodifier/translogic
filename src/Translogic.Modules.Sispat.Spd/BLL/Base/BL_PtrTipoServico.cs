// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrTipoServico : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrTipoServico>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrTipoServico SelectByPk(Database db, String _CodServico)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrTipoServico>(string.Format("COD_SERVICO={0}", Q(_CodServico)));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrTipoServico SelectByPk(String _CodServico)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrTipoServico>(string.Format("COD_SERVICO={0}", Q(_CodServico)));
        }

        public static int DeleteByPk(Database db, String _CodServico)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrTipoServico>(string.Format("COD_SERVICO={0}", Q(_CodServico)));
        }
        public static int DeleteByPk(String _CodServico)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrTipoServico>(string.Format("COD_SERVICO={0}", Q(_CodServico)));
        }



    }

}
