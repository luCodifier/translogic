// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrCadClienteFilial : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial SelectByPk(Database db, String _CodCliente, String _CodFilial)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial>(string.Format("COD_CLIENTE={0} AND COD_FILIAL={1}", Q(_CodCliente), Q(_CodFilial)));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial SelectByPk(String _CodCliente, String _CodFilial)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial>(string.Format("COD_CLIENTE={0} AND COD_FILIAL={1}", Q(_CodCliente), Q(_CodFilial)));
        }

        public static int DeleteByPk(Database db, String _CodCliente, String _CodFilial)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial>(string.Format("COD_CLIENTE={0} AND COD_FILIAL={1}", Q(_CodCliente), Q(_CodFilial)));
        }
        public static int DeleteByPk(String _CodCliente, String _CodFilial)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrCadClienteFilial>(string.Format("COD_CLIENTE={0} AND COD_FILIAL={1}", Q(_CodCliente), Q(_CodFilial)));
        }



    }

}
