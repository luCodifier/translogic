// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_UsuarioAcesso : BLClass<Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso>
    {

        public static Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso SelectByPk(Database db, String _UsuLogin, Decimal? _AceId, String _CodTerminal)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso>(string.Format("USU_LOGIN={0} AND ACE_ID={1} AND COD_TERMINAL={2}", Q(_UsuLogin), _AceId, Q(_CodTerminal)));
        }
        public static Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso SelectByPk(String _UsuLogin, Decimal? _AceId, String _CodTerminal)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso>(string.Format("USU_LOGIN={0} AND ACE_ID={1} AND COD_TERMINAL={2}", Q(_UsuLogin), _AceId, Q(_CodTerminal)));
        }

        public static int DeleteByPk(Database db, String _UsuLogin, Decimal? _AceId, String _CodTerminal)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso>(string.Format("USU_LOGIN={0} AND ACE_ID={1} AND COD_TERMINAL={2}", Q(_UsuLogin), _AceId, Q(_CodTerminal)));
        }
        public static int DeleteByPk(String _UsuLogin, Decimal? _AceId, String _CodTerminal)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.UsuarioAcesso>(string.Format("USU_LOGIN={0} AND ACE_ID={1} AND COD_TERMINAL={2}", Q(_UsuLogin), _AceId, Q(_CodTerminal)));
        }



    }

}
