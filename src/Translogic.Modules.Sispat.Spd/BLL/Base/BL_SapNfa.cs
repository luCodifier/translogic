// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_SapNfa : BLClass<Translogic.Modules.Sispat.Spd.Data.SapNfa>
    {

        public static Translogic.Modules.Sispat.Spd.Data.SapNfa SelectByPk(Database db, Decimal? _IDocSispat)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.SapNfa>(string.Format("I_DOC_SISPAT={0}", _IDocSispat));
        }
        public static Translogic.Modules.Sispat.Spd.Data.SapNfa SelectByPk(Decimal? _IDocSispat)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.SapNfa>(string.Format("I_DOC_SISPAT={0}", _IDocSispat));
        }

        public static int DeleteByPk(Database db, Decimal? _IDocSispat)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.SapNfa>(string.Format("I_DOC_SISPAT={0}", _IDocSispat));
        }
        public static int DeleteByPk(Decimal? _IDocSispat)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.SapNfa>(string.Format("I_DOC_SISPAT={0}", _IDocSispat));
        }



    }

}
