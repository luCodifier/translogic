// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_ChamadaRondopatioRetorno : BLClass<Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno>
    {

        public static Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno SelectByPk(Database db, Decimal? _CrrId)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno>(string.Format("CRR_ID={0}", _CrrId));
        }
        public static Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno SelectByPk(Decimal? _CrrId)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno>(string.Format("CRR_ID={0}", _CrrId));
        }

        public static int DeleteByPk(Database db, Decimal? _CrrId)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno>(string.Format("CRR_ID={0}", _CrrId));
        }
        public static int DeleteByPk(Decimal? _CrrId)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.ChamadaRondopatioRetorno>(string.Format("CRR_ID={0}", _CrrId));
        }



    }

}
