// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_OffCarregamentoLog : BLClass<Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog>
    {

        public static Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog SelectByPk(Database db, Decimal? _IdCarregamentoLog)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog>(string.Format("ID_CARREGAMENTO_LOG={0}", _IdCarregamentoLog));
        }
        public static Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog SelectByPk(Decimal? _IdCarregamentoLog)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog>(string.Format("ID_CARREGAMENTO_LOG={0}", _IdCarregamentoLog));
        }

        public static int DeleteByPk(Database db, Decimal? _IdCarregamentoLog)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog>(string.Format("ID_CARREGAMENTO_LOG={0}", _IdCarregamentoLog));
        }
        public static int DeleteByPk(Decimal? _IdCarregamentoLog)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.OffCarregamentoLog>(string.Format("ID_CARREGAMENTO_LOG={0}", _IdCarregamentoLog));
        }



    }

}
