// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrLote : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrLote>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrLote SelectByPk(Database db, String _CodContrato, Int16? _NumLote)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrLote>(string.Format("COD_CONTRATO={0} AND NUM_LOTE={1}", Q(_CodContrato), _NumLote));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrLote SelectByPk(String _CodContrato, Int16? _NumLote)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrLote>(string.Format("COD_CONTRATO={0} AND NUM_LOTE={1}", Q(_CodContrato), _NumLote));
        }

        public static int DeleteByPk(Database db, String _CodContrato, Int16? _NumLote)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrLote>(string.Format("COD_CONTRATO={0} AND NUM_LOTE={1}", Q(_CodContrato), _NumLote));
        }
        public static int DeleteByPk(String _CodContrato, Int16? _NumLote)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrLote>(string.Format("COD_CONTRATO={0} AND NUM_LOTE={1}", Q(_CodContrato), _NumLote));
        }



    }

}
