// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrCargaVagaoAltFisica : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica SelectByPk(Database db, Decimal? _CvafId)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica>(string.Format("CVAF_ID={0}", _CvafId));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica SelectByPk(Decimal? _CvafId)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica>(string.Format("CVAF_ID={0}", _CvafId));
        }

        public static int DeleteByPk(Database db, Decimal? _CvafId)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica>(string.Format("CVAF_ID={0}", _CvafId));
        }
        public static int DeleteByPk(Decimal? _CvafId)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrCargaVagaoAltFisica>(string.Format("CVAF_ID={0}", _CvafId));
        }



    }

}
