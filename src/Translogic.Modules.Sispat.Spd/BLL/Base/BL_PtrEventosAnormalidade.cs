// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Speed.Data;
using Translogic.Modules.Sispat.Spd.Data;

namespace Translogic.Modules.Sispat.Spd.BLL
{

    //[System.Diagnostics.DebuggerStepThrough]
    public partial class BL_PtrEventosAnormalidade : BLClass<Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade>
    {

        public static Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade SelectByPk(Database db, Int64? _SqAnormalidade)
        {
            return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade>(string.Format("SQ_ANORMALIDADE={0}", _SqAnormalidade));
        }
        public static Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade SelectByPk(Int64? _SqAnormalidade)
        {
            using (var db = Sys.NewDb())
                return db.SelectSingle<Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade>(string.Format("SQ_ANORMALIDADE={0}", _SqAnormalidade));
        }

        public static int DeleteByPk(Database db, Int64? _SqAnormalidade)
        {
            return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade>(string.Format("SQ_ANORMALIDADE={0}", _SqAnormalidade));
        }
        public static int DeleteByPk(Int64? _SqAnormalidade)
        {
            using (var db = Sys.NewDb())
                return db.Delete<Translogic.Modules.Sispat.Spd.Data.PtrEventosAnormalidade>(string.Format("SQ_ANORMALIDADE={0}", _SqAnormalidade));
        }



    }

}
