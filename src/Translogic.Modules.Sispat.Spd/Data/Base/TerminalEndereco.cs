// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "TERMINAL_ENDERECO", "", "")]
    [Serializable]
    [DataContract(Name = "TerminalEndereco", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class TerminalEndereco : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TEN_ID
        /// </summary>
        [DbColumn("TEN_ID")]
        
        public Decimal? TenId { get; set; }

        /// <summary>
        /// Column TEN_TSP
        /// </summary>
        [DbColumn("TEN_TSP")]
        
        public DateTime? TenTsp { get; set; }

        /// <summary>
        /// Column TEN_VER
        /// </summary>
        [DbColumn("TEN_VER")]
        
        public Decimal? TenVer { get; set; }

        /// <summary>
        /// Column TEN_END
        /// </summary>
        [DbColumn("TEN_END")]
        
        public String TenEnd { get; set; }

        /// <summary>
        /// Column TEN_CEP
        /// </summary>
        [DbColumn("TEN_CEP")]
        
        public String TenCep { get; set; }

        /// <summary>
        /// Column COD_TER
        /// </summary>
        [DbColumn("COD_TER")]
        
        public String CodTer { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(TerminalEndereco value)
        {
            if (value == null)
                return false;
            return
				this.TenId == value.TenId &&
				this.TenTsp == value.TenTsp &&
				this.TenVer == value.TenVer &&
				this.TenEnd == value.TenEnd &&
				this.TenCep == value.TenCep &&
				this.CodTer == value.CodTer;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public TerminalEndereco CloneT()
        {
            TerminalEndereco value = new TerminalEndereco();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.TenId = this.TenId;
			value.TenTsp = this.TenTsp;
			value.TenVer = this.TenVer;
			value.TenEnd = this.TenEnd;
			value.TenCep = this.TenCep;
			value.CodTer = this.CodTer;

            return value;
        }

        #endregion Clone

        #region Create

        public static TerminalEndereco Create(Decimal _TenId, DateTime _TenTsp, Decimal _TenVer, String _TenEnd, String _TenCep, String _CodTer)
        {
            TerminalEndereco __value = new TerminalEndereco();

			__value.TenId = _TenId;
			__value.TenTsp = _TenTsp;
			__value.TenVer = _TenVer;
			__value.TenEnd = _TenEnd;
			__value.TenCep = _TenCep;
			__value.CodTer = _CodTer;

            return __value;
        }

        #endregion Create

   }

}
