// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "TMP_CHAVE_NFE", "", "")]
    [Serializable]
    [DataContract(Name = "TmpChaveNfe", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class TmpChaveNfe : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column CHAVE
        /// </summary>
        [DbColumn("CHAVE")]
        
        public String Chave { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(TmpChaveNfe value)
        {
            if (value == null)
                return false;
            return
				this.Chave == value.Chave;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public TmpChaveNfe CloneT()
        {
            TmpChaveNfe value = new TmpChaveNfe();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Chave = this.Chave;

            return value;
        }

        #endregion Clone

        #region Create

        public static TmpChaveNfe Create(String _Chave)
        {
            TmpChaveNfe __value = new TmpChaveNfe();

			__value.Chave = _Chave;

            return __value;
        }

        #endregion Create

   }

}
