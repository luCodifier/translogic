// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "NFS_ABERTAS_FILIAL_DESCARGA", "", "")]
    [Serializable]
    [DataContract(Name = "NfsAbertasFilialDescarga", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class NfsAbertasFilialDescarga : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column NUM_VAGAO
        /// </summary>
        [DbColumn("NUM_VAGAO")]
        
        public Int32? NumVagao { get; set; }

        /// <summary>
        /// Column FLAG_ARMAZENADO
        /// </summary>
        [DbColumn("FLAG_ARMAZENADO")]
        
        public String FlagArmazenado { get; set; }

        /// <summary>
        /// Column FLUXOSOL
        /// </summary>
        [DbColumn("FLUXOSOL")]
        
        public String Fluxosol { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column SERIE_NF
        /// </summary>
        [DbColumn("SERIE_NF")]
        
        public String SerieNf { get; set; }

        /// <summary>
        /// Column NUM_NOTA_FISCAL
        /// </summary>
        [DbColumn("NUM_NOTA_FISCAL")]
        
        public Int64? NumNotaFiscal { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column PESO_DECLARADO
        /// </summary>
        [DbColumn("PESO_DECLARADO")]
        
        public Double? PesoDeclarado { get; set; }

        /// <summary>
        /// Column SALDO_DECLARADO
        /// </summary>
        [DbColumn("SALDO_DECLARADO")]
        
        public Decimal? SaldoDeclarado { get; set; }

        /// <summary>
        /// Column SALDO_LIQUIDO
        /// </summary>
        [DbColumn("SALDO_LIQUIDO")]
        
        public Decimal? SaldoLiquido { get; set; }

        /// <summary>
        /// Column CLIENTE_NOTA_FISCAL
        /// </summary>
        [DbColumn("CLIENTE_NOTA_FISCAL")]
        
        public Int16? ClienteNotaFiscal { get; set; }

        /// <summary>
        /// Column COD_FILIAL
        /// </summary>
        [DbColumn("COD_FILIAL")]
        
        public String CodFilial { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column FLG_DESCARGA_ARMAZENA
        /// </summary>
        [DbColumn("FLG_DESCARGA_ARMAZENA")]
        
        public String FlgDescargaArmazena { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(NfsAbertasFilialDescarga value)
        {
            if (value == null)
                return false;
            return
				this.NumVagao == value.NumVagao &&
				this.FlagArmazenado == value.FlagArmazenado &&
				this.Fluxosol == value.Fluxosol &&
				this.SqNf == value.SqNf &&
				this.SerieNf == value.SerieNf &&
				this.NumNotaFiscal == value.NumNotaFiscal &&
				this.CodContrato == value.CodContrato &&
				this.PesoDeclarado == value.PesoDeclarado &&
				this.SaldoDeclarado == value.SaldoDeclarado &&
				this.SaldoLiquido == value.SaldoLiquido &&
				this.ClienteNotaFiscal == value.ClienteNotaFiscal &&
				this.CodFilial == value.CodFilial &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.FlgDescargaArmazena == value.FlgDescargaArmazena;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public NfsAbertasFilialDescarga CloneT()
        {
            NfsAbertasFilialDescarga value = new NfsAbertasFilialDescarga();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.NumVagao = this.NumVagao;
			value.FlagArmazenado = this.FlagArmazenado;
			value.Fluxosol = this.Fluxosol;
			value.SqNf = this.SqNf;
			value.SerieNf = this.SerieNf;
			value.NumNotaFiscal = this.NumNotaFiscal;
			value.CodContrato = this.CodContrato;
			value.PesoDeclarado = this.PesoDeclarado;
			value.SaldoDeclarado = this.SaldoDeclarado;
			value.SaldoLiquido = this.SaldoLiquido;
			value.ClienteNotaFiscal = this.ClienteNotaFiscal;
			value.CodFilial = this.CodFilial;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.FlgDescargaArmazena = this.FlgDescargaArmazena;

            return value;
        }

        #endregion Clone

        #region Create

        public static NfsAbertasFilialDescarga Create(Int32 _NumVagao, String _FlagArmazenado, String _Fluxosol, Int64 _SqNf, String _SerieNf, Int64 _NumNotaFiscal, String _CodContrato, Double? _PesoDeclarado, Decimal? _SaldoDeclarado, Decimal? _SaldoLiquido, Int16? _ClienteNotaFiscal, String _CodFilial, String _ClienteDestino, String _FilialDestino, String _FlgDescargaArmazena)
        {
            NfsAbertasFilialDescarga __value = new NfsAbertasFilialDescarga();

			__value.NumVagao = _NumVagao;
			__value.FlagArmazenado = _FlagArmazenado;
			__value.Fluxosol = _Fluxosol;
			__value.SqNf = _SqNf;
			__value.SerieNf = _SerieNf;
			__value.NumNotaFiscal = _NumNotaFiscal;
			__value.CodContrato = _CodContrato;
			__value.PesoDeclarado = _PesoDeclarado;
			__value.SaldoDeclarado = _SaldoDeclarado;
			__value.SaldoLiquido = _SaldoLiquido;
			__value.ClienteNotaFiscal = _ClienteNotaFiscal;
			__value.CodFilial = _CodFilial;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.FlgDescargaArmazena = _FlgDescargaArmazena;

            return __value;
        }

        #endregion Create

   }

}
