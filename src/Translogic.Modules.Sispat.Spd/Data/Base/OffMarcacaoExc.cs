// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "OFF_MARCACAO_EXC", "", "")]
    [Serializable]
    [DataContract(Name = "OffMarcacaoExc", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class OffMarcacaoExc : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column AGE_ID
        /// </summary>
        [DbColumn("AGE_ID")]
        
        public Decimal? AgeId { get; set; }

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Decimal? SqCaminhao { get; set; }

        /// <summary>
        /// Column NUM_PLACA
        /// </summary>
        [DbColumn("NUM_PLACA")]
        
        public String NumPlaca { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        /// <summary>
        /// Column TSP_EXCLUSAO
        /// </summary>
        [DbColumn("TSP_EXCLUSAO")]
        
        public DateTime? TspExclusao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(OffMarcacaoExc value)
        {
            if (value == null)
                return false;
            return
				this.AgeId == value.AgeId &&
				this.SqCaminhao == value.SqCaminhao &&
				this.NumPlaca == value.NumPlaca &&
				this.Status == value.Status &&
				this.TspExclusao == value.TspExclusao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public OffMarcacaoExc CloneT()
        {
            OffMarcacaoExc value = new OffMarcacaoExc();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.AgeId = this.AgeId;
			value.SqCaminhao = this.SqCaminhao;
			value.NumPlaca = this.NumPlaca;
			value.Status = this.Status;
			value.TspExclusao = this.TspExclusao;

            return value;
        }

        #endregion Clone

        #region Create

        public static OffMarcacaoExc Create(Decimal? _AgeId, Decimal? _SqCaminhao, String _NumPlaca, String _Status, DateTime? _TspExclusao)
        {
            OffMarcacaoExc __value = new OffMarcacaoExc();

			__value.AgeId = _AgeId;
			__value.SqCaminhao = _SqCaminhao;
			__value.NumPlaca = _NumPlaca;
			__value.Status = _Status;
			__value.TspExclusao = _TspExclusao;

            return __value;
        }

        #endregion Create

   }

}
