// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_SERIE_TBMAX", "", "")]
    [Serializable]
    [DataContract(Name = "PtrSerieTbmax", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrSerieTbmax : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SERIE
        /// </summary>
        [DbColumn("SERIE")]
        
        public String Serie { get; set; }

        /// <summary>
        /// Column TB_MAX
        /// </summary>
        [DbColumn("TB_MAX")]
        
        public Single? TbMax { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrSerieTbmax value)
        {
            if (value == null)
                return false;
            return
				this.Serie == value.Serie &&
				this.TbMax == value.TbMax;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrSerieTbmax CloneT()
        {
            PtrSerieTbmax value = new PtrSerieTbmax();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Serie = this.Serie;
			value.TbMax = this.TbMax;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrSerieTbmax Create(String _Serie, Single? _TbMax)
        {
            PtrSerieTbmax __value = new PtrSerieTbmax();

			__value.Serie = _Serie;
			__value.TbMax = _TbMax;

            return __value;
        }

        #endregion Create

   }

}
