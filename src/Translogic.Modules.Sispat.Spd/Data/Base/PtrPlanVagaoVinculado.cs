// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_PLAN_VAGAO_VINCULADO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrPlanVagaoVinculado", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrPlanVagaoVinculado : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column NUM_OS_COMPOSICAO
        /// </summary>
        [DbColumn("NUM_OS_COMPOSICAO")]
        
        public Decimal? NumOsComposicao { get; set; }

        /// <summary>
        /// Column COD_LOCAL_COMPOSICAO
        /// </summary>
        [DbColumn("COD_LOCAL_COMPOSICAO")]
        
        public String CodLocalComposicao { get; set; }

        /// <summary>
        /// Column COD_COMPOSICAO
        /// </summary>
        [DbColumn("COD_COMPOSICAO")]
        
        public String CodComposicao { get; set; }

        /// <summary>
        /// Column SEQ_COMPOSICAO
        /// </summary>
        [DbColumn("SEQ_COMPOSICAO")]
        
        public Decimal? SeqComposicao { get; set; }

        /// <summary>
        /// Column NUM_VAGAO
        /// </summary>
        [DbColumn("NUM_VAGAO")]
        
        public String NumVagao { get; set; }

        /// <summary>
        /// Column DATA_CARGA_VAGAO
        /// </summary>
        [DbColumn("DATA_CARGA_VAGAO")]
        
        public DateTime? DataCargaVagao { get; set; }

        /// <summary>
        /// Column PESO_BRUTO_VAGAO
        /// </summary>
        [DbColumn("PESO_BRUTO_VAGAO")]
        
        public Decimal? PesoBrutoVagao { get; set; }

        /// <summary>
        /// Column PESO_TARA_VAGAO
        /// </summary>
        [DbColumn("PESO_TARA_VAGAO")]
        
        public Decimal? PesoTaraVagao { get; set; }

        /// <summary>
        /// Column PESO_LIQUIDO_VAGAO
        /// </summary>
        [DbColumn("PESO_LIQUIDO_VAGAO")]
        
        public Decimal? PesoLiquidoVagao { get; set; }

        /// <summary>
        /// Column COD_PRODUTO
        /// </summary>
        [DbColumn("COD_PRODUTO")]
        
        public String CodProduto { get; set; }

        /// <summary>
        /// Column DSC_PRODUTO
        /// </summary>
        [DbColumn("DSC_PRODUTO")]
        
        public String DscProduto { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column DSC_CONTRATO
        /// </summary>
        [DbColumn("DSC_CONTRATO")]
        
        public String DscContrato { get; set; }

        /// <summary>
        /// Column LOCAL_PREV_CHEGADA_1
        /// </summary>
        [DbColumn("LOCAL_PREV_CHEGADA_1")]
        
        public String LocalPrevChegada1 { get; set; }

        /// <summary>
        /// Column DATA_PREV_CHEGADA_1
        /// </summary>
        [DbColumn("DATA_PREV_CHEGADA_1")]
        
        public DateTime? DataPrevChegada1 { get; set; }

        /// <summary>
        /// Column LOCAL_PREV_CHEGADA_2
        /// </summary>
        [DbColumn("LOCAL_PREV_CHEGADA_2")]
        
        public String LocalPrevChegada2 { get; set; }

        /// <summary>
        /// Column DATA_PREV_CHEGADA_2
        /// </summary>
        [DbColumn("DATA_PREV_CHEGADA_2")]
        
        public DateTime? DataPrevChegada2 { get; set; }

        /// <summary>
        /// Column NOME_CLIENTE
        /// </summary>
        [DbColumn("NOME_CLIENTE")]
        
        public String NomeCliente { get; set; }

        /// <summary>
        /// Column TERMINAL_DESCARGA
        /// </summary>
        [DbColumn("TERMINAL_DESCARGA")]
        
        public String TerminalDescarga { get; set; }

        /// <summary>
        /// Column TERMINAL_DESTINO
        /// </summary>
        [DbColumn("TERMINAL_DESTINO")]
        
        public String TerminalDestino { get; set; }

        /// <summary>
        /// Column USUARIO_PLANEJAMENTO
        /// </summary>
        [DbColumn("USUARIO_PLANEJAMENTO")]
        
        public String UsuarioPlanejamento { get; set; }

        /// <summary>
        /// Column DATA_PLANEJAMENTO
        /// </summary>
        [DbColumn("DATA_PLANEJAMENTO")]
        
        public DateTime? DataPlanejamento { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrPlanVagaoVinculado value)
        {
            if (value == null)
                return false;
            return
				this.CodTerminal == value.CodTerminal &&
				this.NumOsComposicao == value.NumOsComposicao &&
				this.CodLocalComposicao == value.CodLocalComposicao &&
				this.CodComposicao == value.CodComposicao &&
				this.SeqComposicao == value.SeqComposicao &&
				this.NumVagao == value.NumVagao &&
				this.DataCargaVagao == value.DataCargaVagao &&
				this.PesoBrutoVagao == value.PesoBrutoVagao &&
				this.PesoTaraVagao == value.PesoTaraVagao &&
				this.PesoLiquidoVagao == value.PesoLiquidoVagao &&
				this.CodProduto == value.CodProduto &&
				this.DscProduto == value.DscProduto &&
				this.CodContrato == value.CodContrato &&
				this.DscContrato == value.DscContrato &&
				this.LocalPrevChegada1 == value.LocalPrevChegada1 &&
				this.DataPrevChegada1 == value.DataPrevChegada1 &&
				this.LocalPrevChegada2 == value.LocalPrevChegada2 &&
				this.DataPrevChegada2 == value.DataPrevChegada2 &&
				this.NomeCliente == value.NomeCliente &&
				this.TerminalDescarga == value.TerminalDescarga &&
				this.TerminalDestino == value.TerminalDestino &&
				this.UsuarioPlanejamento == value.UsuarioPlanejamento &&
				this.DataPlanejamento == value.DataPlanejamento;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrPlanVagaoVinculado CloneT()
        {
            PtrPlanVagaoVinculado value = new PtrPlanVagaoVinculado();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.CodTerminal = this.CodTerminal;
			value.NumOsComposicao = this.NumOsComposicao;
			value.CodLocalComposicao = this.CodLocalComposicao;
			value.CodComposicao = this.CodComposicao;
			value.SeqComposicao = this.SeqComposicao;
			value.NumVagao = this.NumVagao;
			value.DataCargaVagao = this.DataCargaVagao;
			value.PesoBrutoVagao = this.PesoBrutoVagao;
			value.PesoTaraVagao = this.PesoTaraVagao;
			value.PesoLiquidoVagao = this.PesoLiquidoVagao;
			value.CodProduto = this.CodProduto;
			value.DscProduto = this.DscProduto;
			value.CodContrato = this.CodContrato;
			value.DscContrato = this.DscContrato;
			value.LocalPrevChegada1 = this.LocalPrevChegada1;
			value.DataPrevChegada1 = this.DataPrevChegada1;
			value.LocalPrevChegada2 = this.LocalPrevChegada2;
			value.DataPrevChegada2 = this.DataPrevChegada2;
			value.NomeCliente = this.NomeCliente;
			value.TerminalDescarga = this.TerminalDescarga;
			value.TerminalDestino = this.TerminalDestino;
			value.UsuarioPlanejamento = this.UsuarioPlanejamento;
			value.DataPlanejamento = this.DataPlanejamento;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrPlanVagaoVinculado Create(String _CodTerminal, Decimal? _NumOsComposicao, String _CodLocalComposicao, String _CodComposicao, Decimal? _SeqComposicao, String _NumVagao, DateTime? _DataCargaVagao, Decimal? _PesoBrutoVagao, Decimal? _PesoTaraVagao, Decimal? _PesoLiquidoVagao, String _CodProduto, String _DscProduto, String _CodContrato, String _DscContrato, String _LocalPrevChegada1, DateTime? _DataPrevChegada1, String _LocalPrevChegada2, DateTime? _DataPrevChegada2, String _NomeCliente, String _TerminalDescarga, String _TerminalDestino, String _UsuarioPlanejamento, DateTime? _DataPlanejamento)
        {
            PtrPlanVagaoVinculado __value = new PtrPlanVagaoVinculado();

			__value.CodTerminal = _CodTerminal;
			__value.NumOsComposicao = _NumOsComposicao;
			__value.CodLocalComposicao = _CodLocalComposicao;
			__value.CodComposicao = _CodComposicao;
			__value.SeqComposicao = _SeqComposicao;
			__value.NumVagao = _NumVagao;
			__value.DataCargaVagao = _DataCargaVagao;
			__value.PesoBrutoVagao = _PesoBrutoVagao;
			__value.PesoTaraVagao = _PesoTaraVagao;
			__value.PesoLiquidoVagao = _PesoLiquidoVagao;
			__value.CodProduto = _CodProduto;
			__value.DscProduto = _DscProduto;
			__value.CodContrato = _CodContrato;
			__value.DscContrato = _DscContrato;
			__value.LocalPrevChegada1 = _LocalPrevChegada1;
			__value.DataPrevChegada1 = _DataPrevChegada1;
			__value.LocalPrevChegada2 = _LocalPrevChegada2;
			__value.DataPrevChegada2 = _DataPrevChegada2;
			__value.NomeCliente = _NomeCliente;
			__value.TerminalDescarga = _TerminalDescarga;
			__value.TerminalDestino = _TerminalDestino;
			__value.UsuarioPlanejamento = _UsuarioPlanejamento;
			__value.DataPlanejamento = _DataPlanejamento;

            return __value;
        }

        #endregion Create

   }

}
