// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "NFS_ABERTAS_FILIAL", "", "")]
    [Serializable]
    [DataContract(Name = "NfsAbertasFilial", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class NfsAbertasFilial : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column NUM_PLACA
        /// </summary>
        [DbColumn("NUM_PLACA")]
        
        public String NumPlaca { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column SERIE_NF
        /// </summary>
        [DbColumn("SERIE_NF")]
        
        public String SerieNf { get; set; }

        /// <summary>
        /// Column NUM_NOTA_FISCAL
        /// </summary>
        [DbColumn("NUM_NOTA_FISCAL")]
        
        public String NumNotaFiscal { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column PESO_DECLARADO
        /// </summary>
        [DbColumn("PESO_DECLARADO")]
        
        public Double? PesoDeclarado { get; set; }

        /// <summary>
        /// Column SALDO_DECLARADO
        /// </summary>
        [DbColumn("SALDO_DECLARADO")]
        
        public Decimal? SaldoDeclarado { get; set; }

        /// <summary>
        /// Column SALDO_LIQUIDO
        /// </summary>
        [DbColumn("SALDO_LIQUIDO")]
        
        public Decimal? SaldoLiquido { get; set; }

        /// <summary>
        /// Column CLIENTE_NOTA_FISCAL
        /// </summary>
        [DbColumn("CLIENTE_NOTA_FISCAL")]
        
        public Int16? ClienteNotaFiscal { get; set; }

        /// <summary>
        /// Column COD_FILIAL
        /// </summary>
        [DbColumn("COD_FILIAL")]
        
        public String CodFilial { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column DESC_AVARIAS_TOTAIS
        /// </summary>
        [DbColumn("DESC_AVARIAS_TOTAIS")]
        
        public Decimal? DescAvariasTotais { get; set; }

        /// <summary>
        /// Column DESC_UMIDADE
        /// </summary>
        [DbColumn("DESC_UMIDADE")]
        
        public Decimal? DescUmidade { get; set; }

        /// <summary>
        /// Column DESC_IMPUREZA
        /// </summary>
        [DbColumn("DESC_IMPUREZA")]
        
        public Decimal? DescImpureza { get; set; }

        /// <summary>
        /// Column LIQUIDO_PERC_ADM
        /// </summary>
        [DbColumn("LIQUIDO_PERC_ADM")]
        
        public Decimal? LiquidoPercAdm { get; set; }

        /// <summary>
        /// Column DECLARADO_PERC_ADM
        /// </summary>
        [DbColumn("DECLARADO_PERC_ADM")]
        
        public Decimal? DeclaradoPercAdm { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(NfsAbertasFilial value)
        {
            if (value == null)
                return false;
            return
				this.NumPlaca == value.NumPlaca &&
				this.SqNf == value.SqNf &&
				this.SerieNf == value.SerieNf &&
				this.NumNotaFiscal == value.NumNotaFiscal &&
				this.CodContrato == value.CodContrato &&
				this.PesoDeclarado == value.PesoDeclarado &&
				this.SaldoDeclarado == value.SaldoDeclarado &&
				this.SaldoLiquido == value.SaldoLiquido &&
				this.ClienteNotaFiscal == value.ClienteNotaFiscal &&
				this.CodFilial == value.CodFilial &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.DescAvariasTotais == value.DescAvariasTotais &&
				this.DescUmidade == value.DescUmidade &&
				this.DescImpureza == value.DescImpureza &&
				this.LiquidoPercAdm == value.LiquidoPercAdm &&
				this.DeclaradoPercAdm == value.DeclaradoPercAdm;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public NfsAbertasFilial CloneT()
        {
            NfsAbertasFilial value = new NfsAbertasFilial();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.NumPlaca = this.NumPlaca;
			value.SqNf = this.SqNf;
			value.SerieNf = this.SerieNf;
			value.NumNotaFiscal = this.NumNotaFiscal;
			value.CodContrato = this.CodContrato;
			value.PesoDeclarado = this.PesoDeclarado;
			value.SaldoDeclarado = this.SaldoDeclarado;
			value.SaldoLiquido = this.SaldoLiquido;
			value.ClienteNotaFiscal = this.ClienteNotaFiscal;
			value.CodFilial = this.CodFilial;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.DescAvariasTotais = this.DescAvariasTotais;
			value.DescUmidade = this.DescUmidade;
			value.DescImpureza = this.DescImpureza;
			value.LiquidoPercAdm = this.LiquidoPercAdm;
			value.DeclaradoPercAdm = this.DeclaradoPercAdm;

            return value;
        }

        #endregion Clone

        #region Create

        public static NfsAbertasFilial Create(String _NumPlaca, Int64 _SqNf, String _SerieNf, String _NumNotaFiscal, String _CodContrato, Double? _PesoDeclarado, Decimal? _SaldoDeclarado, Decimal? _SaldoLiquido, Int16? _ClienteNotaFiscal, String _CodFilial, String _ClienteDestino, String _FilialDestino, Decimal? _DescAvariasTotais, Decimal? _DescUmidade, Decimal? _DescImpureza, Decimal? _LiquidoPercAdm, Decimal? _DeclaradoPercAdm)
        {
            NfsAbertasFilial __value = new NfsAbertasFilial();

			__value.NumPlaca = _NumPlaca;
			__value.SqNf = _SqNf;
			__value.SerieNf = _SerieNf;
			__value.NumNotaFiscal = _NumNotaFiscal;
			__value.CodContrato = _CodContrato;
			__value.PesoDeclarado = _PesoDeclarado;
			__value.SaldoDeclarado = _SaldoDeclarado;
			__value.SaldoLiquido = _SaldoLiquido;
			__value.ClienteNotaFiscal = _ClienteNotaFiscal;
			__value.CodFilial = _CodFilial;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.DescAvariasTotais = _DescAvariasTotais;
			__value.DescUmidade = _DescUmidade;
			__value.DescImpureza = _DescImpureza;
			__value.LiquidoPercAdm = _LiquidoPercAdm;
			__value.DeclaradoPercAdm = _DeclaradoPercAdm;

            return __value;
        }

        #endregion Create

   }

}
