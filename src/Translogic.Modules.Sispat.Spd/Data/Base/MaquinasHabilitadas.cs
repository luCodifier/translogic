// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "MAQUINAS_HABILITADAS", "", "")]
    [Serializable]
    [DataContract(Name = "MaquinasHabilitadas", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class MaquinasHabilitadas : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column MQH_ID
        /// </summary>
        [DbColumn("MQH_ID")]
        
        public Decimal? MqhId { get; set; }

        /// <summary>
        /// Column MQH_SERIAL_HD
        /// </summary>
        [DbColumn("MQH_SERIAL_HD")]
        
        public String MqhSerialHd { get; set; }

        /// <summary>
        /// Column MQH_MAC_ADDRESS
        /// </summary>
        [DbColumn("MQH_MAC_ADDRESS")]
        
        public String MqhMacAddress { get; set; }

        /// <summary>
        /// Column MQH_STA
        /// </summary>
        [DbColumn("MQH_STA")]
        
        public Decimal? MqhSta { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(MaquinasHabilitadas value)
        {
            if (value == null)
                return false;
            return
				this.MqhId == value.MqhId &&
				this.MqhSerialHd == value.MqhSerialHd &&
				this.MqhMacAddress == value.MqhMacAddress &&
				this.MqhSta == value.MqhSta;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public MaquinasHabilitadas CloneT()
        {
            MaquinasHabilitadas value = new MaquinasHabilitadas();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.MqhId = this.MqhId;
			value.MqhSerialHd = this.MqhSerialHd;
			value.MqhMacAddress = this.MqhMacAddress;
			value.MqhSta = this.MqhSta;

            return value;
        }

        #endregion Clone

        #region Create

        public static MaquinasHabilitadas Create(Decimal _MqhId, String _MqhSerialHd, String _MqhMacAddress, Decimal _MqhSta)
        {
            MaquinasHabilitadas __value = new MaquinasHabilitadas();

			__value.MqhId = _MqhId;
			__value.MqhSerialHd = _MqhSerialHd;
			__value.MqhMacAddress = _MqhMacAddress;
			__value.MqhSta = _MqhSta;

            return __value;
        }

        #endregion Create

   }

}
