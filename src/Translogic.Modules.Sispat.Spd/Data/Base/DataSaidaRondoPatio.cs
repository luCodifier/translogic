// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "DATA_SAIDA_RONDO_PATIO", "", "")]
    [Serializable]
    [DataContract(Name = "DataSaidaRondoPatio", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class DataSaidaRondoPatio : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column DSR_ID
        /// </summary>
        [DbColumn("DSR_ID")]
        
        public Decimal? DsrId { get; set; }

        /// <summary>
        /// Column DSR_TSP
        /// </summary>
        [DbColumn("DSR_TSP")]
        
        public DateTime? DsrTsp { get; set; }

        /// <summary>
        /// Column DSR_VER
        /// </summary>
        [DbColumn("DSR_VER")]
        
        public Decimal? DsrVer { get; set; }

        /// <summary>
        /// Column DSR_COD
        /// </summary>
        [DbColumn("DSR_COD")]
        
        public String DsrCod { get; set; }

        /// <summary>
        /// Column DSR_DSC
        /// </summary>
        [DbColumn("DSR_DSC")]
        
        public String DsrDsc { get; set; }

        /// <summary>
        /// Column PAT_ID_AGE
        /// </summary>
        [DbColumn("PAT_ID_AGE")]
        
        public String PatIdAge { get; set; }

        /// <summary>
        /// Column DSR_DTS
        /// </summary>
        [DbColumn("DSR_DTS")]
        
        public DateTime? DsrDts { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(DataSaidaRondoPatio value)
        {
            if (value == null)
                return false;
            return
				this.DsrId == value.DsrId &&
				this.DsrTsp == value.DsrTsp &&
				this.DsrVer == value.DsrVer &&
				this.DsrCod == value.DsrCod &&
				this.DsrDsc == value.DsrDsc &&
				this.PatIdAge == value.PatIdAge &&
				this.DsrDts == value.DsrDts;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public DataSaidaRondoPatio CloneT()
        {
            DataSaidaRondoPatio value = new DataSaidaRondoPatio();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.DsrId = this.DsrId;
			value.DsrTsp = this.DsrTsp;
			value.DsrVer = this.DsrVer;
			value.DsrCod = this.DsrCod;
			value.DsrDsc = this.DsrDsc;
			value.PatIdAge = this.PatIdAge;
			value.DsrDts = this.DsrDts;

            return value;
        }

        #endregion Clone

        #region Create

        public static DataSaidaRondoPatio Create(Decimal _DsrId, DateTime _DsrTsp, Decimal _DsrVer, String _DsrCod, String _DsrDsc, String _PatIdAge, DateTime _DsrDts)
        {
            DataSaidaRondoPatio __value = new DataSaidaRondoPatio();

			__value.DsrId = _DsrId;
			__value.DsrTsp = _DsrTsp;
			__value.DsrVer = _DsrVer;
			__value.DsrCod = _DsrCod;
			__value.DsrDsc = _DsrDsc;
			__value.PatIdAge = _PatIdAge;
			__value.DsrDts = _DsrDts;

            return __value;
        }

        #endregion Create

   }

}
