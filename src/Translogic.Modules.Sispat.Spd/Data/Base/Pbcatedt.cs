// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PBCATEDT", "", "")]
    [Serializable]
    [DataContract(Name = "Pbcatedt", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class Pbcatedt : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column PBE_NAME
        /// </summary>
        [DbColumn("PBE_NAME")]
        
        public String PbeName { get; set; }

        /// <summary>
        /// Column PBE_EDIT
        /// </summary>
        [DbColumn("PBE_EDIT")]
        
        public String PbeEdit { get; set; }

        /// <summary>
        /// Column PBE_TYPE
        /// </summary>
        [DbColumn("PBE_TYPE")]
        
        public Int32? PbeType { get; set; }

        /// <summary>
        /// Column PBE_CNTR
        /// </summary>
        [DbColumn("PBE_CNTR")]
        
        public Int64? PbeCntr { get; set; }

        /// <summary>
        /// Column PBE_SEQN
        /// </summary>
        [DbColumn("PBE_SEQN")]
        
        public Int32? PbeSeqn { get; set; }

        /// <summary>
        /// Column PBE_FLAG
        /// </summary>
        [DbColumn("PBE_FLAG")]
        
        public Int64? PbeFlag { get; set; }

        /// <summary>
        /// Column PBE_WORK
        /// </summary>
        [DbColumn("PBE_WORK")]
        
        public String PbeWork { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(Pbcatedt value)
        {
            if (value == null)
                return false;
            return
				this.PbeName == value.PbeName &&
				this.PbeEdit == value.PbeEdit &&
				this.PbeType == value.PbeType &&
				this.PbeCntr == value.PbeCntr &&
				this.PbeSeqn == value.PbeSeqn &&
				this.PbeFlag == value.PbeFlag &&
				this.PbeWork == value.PbeWork;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public Pbcatedt CloneT()
        {
            Pbcatedt value = new Pbcatedt();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.PbeName = this.PbeName;
			value.PbeEdit = this.PbeEdit;
			value.PbeType = this.PbeType;
			value.PbeCntr = this.PbeCntr;
			value.PbeSeqn = this.PbeSeqn;
			value.PbeFlag = this.PbeFlag;
			value.PbeWork = this.PbeWork;

            return value;
        }

        #endregion Clone

        #region Create

        public static Pbcatedt Create(String _PbeName, String _PbeEdit, Int32? _PbeType, Int64? _PbeCntr, Int32 _PbeSeqn, Int64? _PbeFlag, String _PbeWork)
        {
            Pbcatedt __value = new Pbcatedt();

			__value.PbeName = _PbeName;
			__value.PbeEdit = _PbeEdit;
			__value.PbeType = _PbeType;
			__value.PbeCntr = _PbeCntr;
			__value.PbeSeqn = _PbeSeqn;
			__value.PbeFlag = _PbeFlag;
			__value.PbeWork = _PbeWork;

            return __value;
        }

        #endregion Create

   }

}
