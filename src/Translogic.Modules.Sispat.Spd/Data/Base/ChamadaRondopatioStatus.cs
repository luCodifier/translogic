// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "CHAMADA_RONDOPATIO_STATUS", "", "")]
    [Serializable]
    [DataContract(Name = "ChamadaRondopatioStatus", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class ChamadaRondopatioStatus : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column CRS_ID
        /// </summary>
        [DbColumn("CRS_ID")]
        
        public Decimal? CrsId { get; set; }

        /// <summary>
        /// Column CRS_TSP
        /// </summary>
        [DbColumn("CRS_TSP")]
        
        public DateTime? CrsTsp { get; set; }

        /// <summary>
        /// Column CRS_VER
        /// </summary>
        [DbColumn("CRS_VER")]
        
        public Decimal? CrsVer { get; set; }

        /// <summary>
        /// Column CRP_ID
        /// </summary>
        [DbColumn("CRP_ID")]
        
        public Decimal? CrpId { get; set; }

        /// <summary>
        /// Column CRS_DAT_INI
        /// </summary>
        [DbColumn("CRS_DAT_INI")]
        
        public DateTime? CrsDatIni { get; set; }

        /// <summary>
        /// Column CRS_DAT_FIM
        /// </summary>
        [DbColumn("CRS_DAT_FIM")]
        
        public DateTime? CrsDatFim { get; set; }

        /// <summary>
        /// Column CRS_QTD_SOL
        /// </summary>
        [DbColumn("CRS_QTD_SOL")]
        
        public Decimal? CrsQtdSol { get; set; }

        /// <summary>
        /// Column CRS_QTD_REC
        /// </summary>
        [DbColumn("CRS_QTD_REC")]
        
        public Decimal? CrsQtdRec { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(ChamadaRondopatioStatus value)
        {
            if (value == null)
                return false;
            return
				this.CrsId == value.CrsId &&
				this.CrsTsp == value.CrsTsp &&
				this.CrsVer == value.CrsVer &&
				this.CrpId == value.CrpId &&
				this.CrsDatIni == value.CrsDatIni &&
				this.CrsDatFim == value.CrsDatFim &&
				this.CrsQtdSol == value.CrsQtdSol &&
				this.CrsQtdRec == value.CrsQtdRec;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public ChamadaRondopatioStatus CloneT()
        {
            ChamadaRondopatioStatus value = new ChamadaRondopatioStatus();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.CrsId = this.CrsId;
			value.CrsTsp = this.CrsTsp;
			value.CrsVer = this.CrsVer;
			value.CrpId = this.CrpId;
			value.CrsDatIni = this.CrsDatIni;
			value.CrsDatFim = this.CrsDatFim;
			value.CrsQtdSol = this.CrsQtdSol;
			value.CrsQtdRec = this.CrsQtdRec;

            return value;
        }

        #endregion Clone

        #region Create

        public static ChamadaRondopatioStatus Create(Decimal _CrsId, DateTime _CrsTsp, Decimal _CrsVer, Decimal _CrpId, DateTime? _CrsDatIni, DateTime? _CrsDatFim, Decimal? _CrsQtdSol, Decimal? _CrsQtdRec)
        {
            ChamadaRondopatioStatus __value = new ChamadaRondopatioStatus();

			__value.CrsId = _CrsId;
			__value.CrsTsp = _CrsTsp;
			__value.CrsVer = _CrsVer;
			__value.CrpId = _CrpId;
			__value.CrsDatIni = _CrsDatIni;
			__value.CrsDatFim = _CrsDatFim;
			__value.CrsQtdSol = _CrsQtdSol;
			__value.CrsQtdRec = _CrsQtdRec;

            return __value;
        }

        #endregion Create

   }

}
