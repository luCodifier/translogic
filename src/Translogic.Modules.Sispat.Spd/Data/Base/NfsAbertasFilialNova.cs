// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "NFS_ABERTAS_FILIAL_NOVA", "", "")]
    [Serializable]
    [DataContract(Name = "NfsAbertasFilialNova", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class NfsAbertasFilialNova : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column PLACA
        /// </summary>
        [DbColumn("PLACA")]
        
        public String Placa { get; set; }

        /// <summary>
        /// Column NF_NUMERO
        /// </summary>
        [DbColumn("NF_NUMERO")]
        
        public String NfNumero { get; set; }

        /// <summary>
        /// Column NF_SERIE
        /// </summary>
        [DbColumn("NF_SERIE")]
        
        public String NfSerie { get; set; }

        /// <summary>
        /// Column NF_DATA
        /// </summary>
        [DbColumn("NF_DATA")]
        
        public DateTime? NfData { get; set; }

        /// <summary>
        /// Column CLIENTE_ORIGEM
        /// </summary>
        [DbColumn("CLIENTE_ORIGEM")]
        
        public Int16? ClienteOrigem { get; set; }

        /// <summary>
        /// Column FILIAL_ORIGEM
        /// </summary>
        [DbColumn("FILIAL_ORIGEM")]
        
        public String FilialOrigem { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column PESO_DECLARADO
        /// </summary>
        [DbColumn("PESO_DECLARADO")]
        
        public Decimal? PesoDeclarado { get; set; }

        /// <summary>
        /// Column PESO_DESCARREGADO
        /// </summary>
        [DbColumn("PESO_DESCARREGADO")]
        
        public Decimal? PesoDescarregado { get; set; }

        /// <summary>
        /// Column PESO_SELECIONADO
        /// </summary>
        [DbColumn("PESO_SELECIONADO")]
        
        public Decimal? PesoSelecionado { get; set; }

        /// <summary>
        /// Column PESO_CARREGADO
        /// </summary>
        [DbColumn("PESO_CARREGADO")]
        
        public Decimal? PesoCarregado { get; set; }

        /// <summary>
        /// Column SALDO_NOTA
        /// </summary>
        [DbColumn("SALDO_NOTA")]
        
        public Decimal? SaldoNota { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(NfsAbertasFilialNova value)
        {
            if (value == null)
                return false;
            return
				this.SqNf == value.SqNf &&
				this.Contrato == value.Contrato &&
				this.Terminal == value.Terminal &&
				this.Placa == value.Placa &&
				this.NfNumero == value.NfNumero &&
				this.NfSerie == value.NfSerie &&
				this.NfData == value.NfData &&
				this.ClienteOrigem == value.ClienteOrigem &&
				this.FilialOrigem == value.FilialOrigem &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.PesoDeclarado == value.PesoDeclarado &&
				this.PesoDescarregado == value.PesoDescarregado &&
				this.PesoSelecionado == value.PesoSelecionado &&
				this.PesoCarregado == value.PesoCarregado &&
				this.SaldoNota == value.SaldoNota;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public NfsAbertasFilialNova CloneT()
        {
            NfsAbertasFilialNova value = new NfsAbertasFilialNova();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqNf = this.SqNf;
			value.Contrato = this.Contrato;
			value.Terminal = this.Terminal;
			value.Placa = this.Placa;
			value.NfNumero = this.NfNumero;
			value.NfSerie = this.NfSerie;
			value.NfData = this.NfData;
			value.ClienteOrigem = this.ClienteOrigem;
			value.FilialOrigem = this.FilialOrigem;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.PesoDeclarado = this.PesoDeclarado;
			value.PesoDescarregado = this.PesoDescarregado;
			value.PesoSelecionado = this.PesoSelecionado;
			value.PesoCarregado = this.PesoCarregado;
			value.SaldoNota = this.SaldoNota;

            return value;
        }

        #endregion Clone

        #region Create

        public static NfsAbertasFilialNova Create(Int64 _SqNf, String _Contrato, String _Terminal, String _Placa, String _NfNumero, String _NfSerie, DateTime? _NfData, Int16? _ClienteOrigem, String _FilialOrigem, String _ClienteDestino, String _FilialDestino, Decimal? _PesoDeclarado, Decimal? _PesoDescarregado, Decimal? _PesoSelecionado, Decimal? _PesoCarregado, Decimal? _SaldoNota)
        {
            NfsAbertasFilialNova __value = new NfsAbertasFilialNova();

			__value.SqNf = _SqNf;
			__value.Contrato = _Contrato;
			__value.Terminal = _Terminal;
			__value.Placa = _Placa;
			__value.NfNumero = _NfNumero;
			__value.NfSerie = _NfSerie;
			__value.NfData = _NfData;
			__value.ClienteOrigem = _ClienteOrigem;
			__value.FilialOrigem = _FilialOrigem;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.PesoDeclarado = _PesoDeclarado;
			__value.PesoDescarregado = _PesoDescarregado;
			__value.PesoSelecionado = _PesoSelecionado;
			__value.PesoCarregado = _PesoCarregado;
			__value.SaldoNota = _SaldoNota;

            return __value;
        }

        #endregion Create

   }

}
