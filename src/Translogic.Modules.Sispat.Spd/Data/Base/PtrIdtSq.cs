// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_IDT_SQ", "", "")]
    [Serializable]
    [DataContract(Name = "PtrIdtSq", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrIdtSq : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column IDT_FATURA
        /// </summary>
        [DbColumn("IDT_FATURA")]
        
        public Decimal? IdtFatura { get; set; }

        /// <summary>
        /// Column SQ_FATURA
        /// </summary>
        [DbColumn("SQ_FATURA")]
        
        public Decimal? SqFatura { get; set; }

        /// <summary>
        /// Column P_OPER
        /// </summary>
        [DbColumn("P_OPER")]
        
        public String POper { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrIdtSq value)
        {
            if (value == null)
                return false;
            return
				this.IdtFatura == value.IdtFatura &&
				this.SqFatura == value.SqFatura &&
				this.POper == value.POper;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrIdtSq CloneT()
        {
            PtrIdtSq value = new PtrIdtSq();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.IdtFatura = this.IdtFatura;
			value.SqFatura = this.SqFatura;
			value.POper = this.POper;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrIdtSq Create(Decimal? _IdtFatura, Decimal? _SqFatura, String _POper)
        {
            PtrIdtSq __value = new PtrIdtSq();

			__value.IdtFatura = _IdtFatura;
			__value.SqFatura = _SqFatura;
			__value.POper = _POper;

            return __value;
        }

        #endregion Create

   }

}
