// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_VAGAO_EXPEDICAO_NF", "", "")]
    [Serializable]
    [DataContract(Name = "PtrVagaoExpedicaoNf", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrVagaoExpedicaoNf : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column NUM_VAGAO
        /// </summary>
        [DbColumn("NUM_VAGAO")]
        
        public Int32? NumVagao { get; set; }

        /// <summary>
        /// Column NUM_NF
        /// </summary>
        [DbColumn("NUM_NF")]
        
        public String NumNf { get; set; }

        /// <summary>
        /// Column VAL_NF
        /// </summary>
        [DbColumn("VAL_NF")]
        
        public Decimal? ValNf { get; set; }

        /// <summary>
        /// Column PESO_NF
        /// </summary>
        [DbColumn("PESO_NF")]
        
        public Int64? PesoNf { get; set; }

        /// <summary>
        /// Column PESO_CARREGADO
        /// </summary>
        [DbColumn("PESO_CARREGADO")]
        
        public Int64? PesoCarregado { get; set; }

        /// <summary>
        /// Column CLIENTE_ORIGEM
        /// </summary>
        [DbColumn("CLIENTE_ORIGEM")]
        
        public String ClienteOrigem { get; set; }

        /// <summary>
        /// Column FILIAL_ORIGEM
        /// </summary>
        [DbColumn("FILIAL_ORIGEM")]
        
        public String FilialOrigem { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column DATA_NOTA_FISCAL
        /// </summary>
        [DbColumn("DATA_NOTA_FISCAL")]
        
        public DateTime? DataNotaFiscal { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrVagaoExpedicaoNf value)
        {
            if (value == null)
                return false;
            return
				this.NumVagao == value.NumVagao &&
				this.NumNf == value.NumNf &&
				this.ValNf == value.ValNf &&
				this.PesoNf == value.PesoNf &&
				this.PesoCarregado == value.PesoCarregado &&
				this.ClienteOrigem == value.ClienteOrigem &&
				this.FilialOrigem == value.FilialOrigem &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.DataNotaFiscal == value.DataNotaFiscal &&
				this.SqNf == value.SqNf;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrVagaoExpedicaoNf CloneT()
        {
            PtrVagaoExpedicaoNf value = new PtrVagaoExpedicaoNf();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.NumVagao = this.NumVagao;
			value.NumNf = this.NumNf;
			value.ValNf = this.ValNf;
			value.PesoNf = this.PesoNf;
			value.PesoCarregado = this.PesoCarregado;
			value.ClienteOrigem = this.ClienteOrigem;
			value.FilialOrigem = this.FilialOrigem;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.DataNotaFiscal = this.DataNotaFiscal;
			value.SqNf = this.SqNf;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrVagaoExpedicaoNf Create(Int32? _NumVagao, String _NumNf, Decimal? _ValNf, Int64? _PesoNf, Int64? _PesoCarregado, String _ClienteOrigem, String _FilialOrigem, String _ClienteDestino, String _FilialDestino, DateTime? _DataNotaFiscal, Int64? _SqNf)
        {
            PtrVagaoExpedicaoNf __value = new PtrVagaoExpedicaoNf();

			__value.NumVagao = _NumVagao;
			__value.NumNf = _NumNf;
			__value.ValNf = _ValNf;
			__value.PesoNf = _PesoNf;
			__value.PesoCarregado = _PesoCarregado;
			__value.ClienteOrigem = _ClienteOrigem;
			__value.FilialOrigem = _FilialOrigem;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.DataNotaFiscal = _DataNotaFiscal;
			__value.SqNf = _SqNf;

            return __value;
        }

        #endregion Create

   }

}
