// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_ENVIO_XML_DESCARGA_BUNGE", "", "")]
    [Serializable]
    [DataContract(Name = "PtrEnvioXmlDescargaBunge", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrEnvioXmlDescargaBunge : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column EXD_ID
        /// </summary>
        [DbColumn("EXD_ID")]
        
        public Decimal? ExdId { get; set; }

        /// <summary>
        /// Column EXD_TIMESTAMP
        /// </summary>
        [DbColumn("EXD_TIMESTAMP")]
        
        public DateTime? ExdTimestamp { get; set; }

        /// <summary>
        /// Column EXD_SQ_CAM
        /// </summary>
        [DbColumn("EXD_SQ_CAM")]
        
        public Decimal? ExdSqCam { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrEnvioXmlDescargaBunge value)
        {
            if (value == null)
                return false;
            return
				this.ExdId == value.ExdId &&
				this.ExdTimestamp == value.ExdTimestamp &&
				this.ExdSqCam == value.ExdSqCam;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrEnvioXmlDescargaBunge CloneT()
        {
            PtrEnvioXmlDescargaBunge value = new PtrEnvioXmlDescargaBunge();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.ExdId = this.ExdId;
			value.ExdTimestamp = this.ExdTimestamp;
			value.ExdSqCam = this.ExdSqCam;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrEnvioXmlDescargaBunge Create(Decimal _ExdId, DateTime _ExdTimestamp, Decimal _ExdSqCam)
        {
            PtrEnvioXmlDescargaBunge __value = new PtrEnvioXmlDescargaBunge();

			__value.ExdId = _ExdId;
			__value.ExdTimestamp = _ExdTimestamp;
			__value.ExdSqCam = _ExdSqCam;

            return __value;
        }

        #endregion Create

   }

}
