// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "V_NOTA_FISCAL_OK", "", "")]
    [Serializable]
    [DataContract(Name = "VNotaFiscalOk", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VNotaFiscalOk : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column SQ_CAM
        /// </summary>
        [DbColumn("SQ_CAM")]
        
        public Decimal? SqCam { get; set; }

        /// <summary>
        /// Column COD_TER
        /// </summary>
        [DbColumn("COD_TER")]
        
        public String CodTer { get; set; }

        /// <summary>
        /// Column COD_CON
        /// </summary>
        [DbColumn("COD_CON")]
        
        public String CodCon { get; set; }

        /// <summary>
        /// Column NUM_PLA
        /// </summary>
        [DbColumn("NUM_PLA")]
        
        public String NumPla { get; set; }

        /// <summary>
        /// Column DAT_MAR
        /// </summary>
        [DbColumn("DAT_MAR")]
        
        public DateTime? DatMar { get; set; }

        /// <summary>
        /// Column DAT_ENT
        /// </summary>
        [DbColumn("DAT_ENT")]
        
        public DateTime? DatEnt { get; set; }

        /// <summary>
        /// Column NF_NUM
        /// </summary>
        [DbColumn("NF_NUM")]
        
        public String NfNum { get; set; }

        /// <summary>
        /// Column NF_SER
        /// </summary>
        [DbColumn("NF_SER")]
        
        public String NfSer { get; set; }

        /// <summary>
        /// Column NF_DAT
        /// </summary>
        [DbColumn("NF_DAT")]
        
        public DateTime? NfDat { get; set; }

        /// <summary>
        /// Column NF_VAL
        /// </summary>
        [DbColumn("NF_VAL")]
        
        public Double? NfVal { get; set; }

        /// <summary>
        /// Column NF_CHA
        /// </summary>
        [DbColumn("NF_CHA")]
        
        public String NfCha { get; set; }

        /// <summary>
        /// Column NF_IS_VAL
        /// </summary>
        [DbColumn("NF_IS_VAL")]
        
        public String NfIsVal { get; set; }

        /// <summary>
        /// Column NF_IS_ARM
        /// </summary>
        [DbColumn("NF_IS_ARM")]
        
        public String NfIsArm { get; set; }

        /// <summary>
        /// Column NF_IS_APR
        /// </summary>
        [DbColumn("NF_IS_APR")]
        
        public String NfIsApr { get; set; }

        /// <summary>
        /// Column NF_IS_EDI
        /// </summary>
        [DbColumn("NF_IS_EDI")]
        
        public String NfIsEdi { get; set; }

        /// <summary>
        /// Column NF_PES_DEC
        /// </summary>
        [DbColumn("NF_PES_DEC")]
        
        public Decimal? NfPesDec { get; set; }

        /// <summary>
        /// Column NF_PES_DES
        /// </summary>
        [DbColumn("NF_PES_DES")]
        
        public Decimal? NfPesDes { get; set; }

        /// <summary>
        /// Column NF_PES_SEL
        /// </summary>
        [DbColumn("NF_PES_SEL")]
        
        public Decimal? NfPesSel { get; set; }

        /// <summary>
        /// Column NF_PES_EXP
        /// </summary>
        [DbColumn("NF_PES_EXP")]
        
        public Decimal? NfPesExp { get; set; }

        /// <summary>
        /// Column NF_PES_SAL
        /// </summary>
        [DbColumn("NF_PES_SAL")]
        
        public Decimal? NfPesSal { get; set; }

        /// <summary>
        /// Column CLI_ORI
        /// </summary>
        [DbColumn("CLI_ORI")]
        
        public String CliOri { get; set; }

        /// <summary>
        /// Column FIL_ORI
        /// </summary>
        [DbColumn("FIL_ORI")]
        
        public String FilOri { get; set; }

        /// <summary>
        /// Column CNPJ_ORI
        /// </summary>
        [DbColumn("CNPJ_ORI")]
        
        public String CnpjOri { get; set; }

        /// <summary>
        /// Column CLI_DST
        /// </summary>
        [DbColumn("CLI_DST")]
        
        public String CliDst { get; set; }

        /// <summary>
        /// Column FIL_DST
        /// </summary>
        [DbColumn("FIL_DST")]
        
        public String FilDst { get; set; }

        /// <summary>
        /// Column CNPJ_DST
        /// </summary>
        [DbColumn("CNPJ_DST")]
        
        public String CnpjDst { get; set; }

        /// <summary>
        /// Column FLX_COD
        /// </summary>
        [DbColumn("FLX_COD")]
        
        public String FlxCod { get; set; }

        /// <summary>
        /// Column FLX_DAT_VIG
        /// </summary>
        [DbColumn("FLX_DAT_VIG")]
        
        public DateTime? FlxDatVig { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VNotaFiscalOk value)
        {
            if (value == null)
                return false;
            return
				this.SqNf == value.SqNf &&
				this.SqCam == value.SqCam &&
				this.CodTer == value.CodTer &&
				this.CodCon == value.CodCon &&
				this.NumPla == value.NumPla &&
				this.DatMar == value.DatMar &&
				this.DatEnt == value.DatEnt &&
				this.NfNum == value.NfNum &&
				this.NfSer == value.NfSer &&
				this.NfDat == value.NfDat &&
				this.NfVal == value.NfVal &&
				this.NfCha == value.NfCha &&
				this.NfIsVal == value.NfIsVal &&
				this.NfIsArm == value.NfIsArm &&
				this.NfIsApr == value.NfIsApr &&
				this.NfIsEdi == value.NfIsEdi &&
				this.NfPesDec == value.NfPesDec &&
				this.NfPesDes == value.NfPesDes &&
				this.NfPesSel == value.NfPesSel &&
				this.NfPesExp == value.NfPesExp &&
				this.NfPesSal == value.NfPesSal &&
				this.CliOri == value.CliOri &&
				this.FilOri == value.FilOri &&
				this.CnpjOri == value.CnpjOri &&
				this.CliDst == value.CliDst &&
				this.FilDst == value.FilDst &&
				this.CnpjDst == value.CnpjDst &&
				this.FlxCod == value.FlxCod &&
				this.FlxDatVig == value.FlxDatVig;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VNotaFiscalOk CloneT()
        {
            VNotaFiscalOk value = new VNotaFiscalOk();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqNf = this.SqNf;
			value.SqCam = this.SqCam;
			value.CodTer = this.CodTer;
			value.CodCon = this.CodCon;
			value.NumPla = this.NumPla;
			value.DatMar = this.DatMar;
			value.DatEnt = this.DatEnt;
			value.NfNum = this.NfNum;
			value.NfSer = this.NfSer;
			value.NfDat = this.NfDat;
			value.NfVal = this.NfVal;
			value.NfCha = this.NfCha;
			value.NfIsVal = this.NfIsVal;
			value.NfIsArm = this.NfIsArm;
			value.NfIsApr = this.NfIsApr;
			value.NfIsEdi = this.NfIsEdi;
			value.NfPesDec = this.NfPesDec;
			value.NfPesDes = this.NfPesDes;
			value.NfPesSel = this.NfPesSel;
			value.NfPesExp = this.NfPesExp;
			value.NfPesSal = this.NfPesSal;
			value.CliOri = this.CliOri;
			value.FilOri = this.FilOri;
			value.CnpjOri = this.CnpjOri;
			value.CliDst = this.CliDst;
			value.FilDst = this.FilDst;
			value.CnpjDst = this.CnpjDst;
			value.FlxCod = this.FlxCod;
			value.FlxDatVig = this.FlxDatVig;

            return value;
        }

        #endregion Clone

        #region Create

        public static VNotaFiscalOk Create(Int64 _SqNf, Decimal? _SqCam, String _CodTer, String _CodCon, String _NumPla, DateTime? _DatMar, DateTime _DatEnt, String _NfNum, String _NfSer, DateTime? _NfDat, Double? _NfVal, String _NfCha, String _NfIsVal, String _NfIsArm, String _NfIsApr, String _NfIsEdi, Decimal? _NfPesDec, Decimal? _NfPesDes, Decimal? _NfPesSel, Decimal? _NfPesExp, Decimal? _NfPesSal, String _CliOri, String _FilOri, String _CnpjOri, String _CliDst, String _FilDst, String _CnpjDst, String _FlxCod, DateTime? _FlxDatVig)
        {
            VNotaFiscalOk __value = new VNotaFiscalOk();

			__value.SqNf = _SqNf;
			__value.SqCam = _SqCam;
			__value.CodTer = _CodTer;
			__value.CodCon = _CodCon;
			__value.NumPla = _NumPla;
			__value.DatMar = _DatMar;
			__value.DatEnt = _DatEnt;
			__value.NfNum = _NfNum;
			__value.NfSer = _NfSer;
			__value.NfDat = _NfDat;
			__value.NfVal = _NfVal;
			__value.NfCha = _NfCha;
			__value.NfIsVal = _NfIsVal;
			__value.NfIsArm = _NfIsArm;
			__value.NfIsApr = _NfIsApr;
			__value.NfIsEdi = _NfIsEdi;
			__value.NfPesDec = _NfPesDec;
			__value.NfPesDes = _NfPesDes;
			__value.NfPesSel = _NfPesSel;
			__value.NfPesExp = _NfPesExp;
			__value.NfPesSal = _NfPesSal;
			__value.CliOri = _CliOri;
			__value.FilOri = _FilOri;
			__value.CnpjOri = _CnpjOri;
			__value.CliDst = _CliDst;
			__value.FilDst = _FilDst;
			__value.CnpjDst = _CnpjDst;
			__value.FlxCod = _FlxCod;
			__value.FlxDatVig = _FlxDatVig;

            return __value;
        }

        #endregion Create

   }

}
