// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CONT_RC_CIDADE_FLUXO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrContRcCidadeFluxo", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrContRcCidadeFluxo : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column CON_RC_ID
        /// </summary>
        [DbColumn("CON_RC_ID")]
        
        public Decimal? ConRcId { get; set; }

        /// <summary>
        /// Column CD_ID_CD
        /// </summary>
        [DbColumn("CD_ID_CD")]
        
        public Decimal? CdIdCd { get; set; }

        /// <summary>
        /// Column FX_ID_FLX
        /// </summary>
        [DbColumn("FX_ID_FLX")]
        
        public Decimal? FxIdFlx { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrContRcCidadeFluxo value)
        {
            if (value == null)
                return false;
            return
				this.ConRcId == value.ConRcId &&
				this.CdIdCd == value.CdIdCd &&
				this.FxIdFlx == value.FxIdFlx;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrContRcCidadeFluxo CloneT()
        {
            PtrContRcCidadeFluxo value = new PtrContRcCidadeFluxo();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.ConRcId = this.ConRcId;
			value.CdIdCd = this.CdIdCd;
			value.FxIdFlx = this.FxIdFlx;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrContRcCidadeFluxo Create(Decimal _ConRcId, Decimal _CdIdCd, Decimal _FxIdFlx)
        {
            PtrContRcCidadeFluxo __value = new PtrContRcCidadeFluxo();

			__value.ConRcId = _ConRcId;
			__value.CdIdCd = _CdIdCd;
			__value.FxIdFlx = _FxIdFlx;

            return __value;
        }

        #endregion Create

   }

}
