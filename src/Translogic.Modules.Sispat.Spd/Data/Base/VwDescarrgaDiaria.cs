// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_DESCARRGA_DIARIA", "", "")]
    [Serializable]
    [DataContract(Name = "VwDescarrgaDiaria", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwDescarrgaDiaria : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column PRODUTO
        /// </summary>
        [DbColumn("PRODUTO")]
        
        public String Produto { get; set; }

        /// <summary>
        /// Column DATA_MARCACAO
        /// </summary>
        [DbColumn("DATA_MARCACAO")]
        
        public DateTime? DataMarcacao { get; set; }

        /// <summary>
        /// Column DATA_ENTRADA
        /// </summary>
        [DbColumn("DATA_ENTRADA")]
        
        public DateTime? DataEntrada { get; set; }

        /// <summary>
        /// Column DATA_SAIDA
        /// </summary>
        [DbColumn("DATA_SAIDA")]
        
        public DateTime? DataSaida { get; set; }

        /// <summary>
        /// Column HORAS_NA_FILA
        /// </summary>
        [DbColumn("HORAS_NA_FILA")]
        
        public Decimal? HorasNaFila { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        /// <summary>
        /// Column PLACA
        /// </summary>
        [DbColumn("PLACA")]
        
        public String Placa { get; set; }

        /// <summary>
        /// Column MOTORISTA
        /// </summary>
        [DbColumn("MOTORISTA")]
        
        public String Motorista { get; set; }

        /// <summary>
        /// Column PESO_BRUTO
        /// </summary>
        [DbColumn("PESO_BRUTO")]
        
        public Decimal? PesoBruto { get; set; }

        /// <summary>
        /// Column PESO_TARA
        /// </summary>
        [DbColumn("PESO_TARA")]
        
        public Decimal? PesoTara { get; set; }

        /// <summary>
        /// Column PESO_LIQUIDO
        /// </summary>
        [DbColumn("PESO_LIQUIDO")]
        
        public Decimal? PesoLiquido { get; set; }

        /// <summary>
        /// Column CTE_RODO
        /// </summary>
        [DbColumn("CTE_RODO")]
        
        public String CteRodo { get; set; }

        /// <summary>
        /// Column CLIENTE_CNPJ
        /// </summary>
        [DbColumn("CLIENTE_CNPJ")]
        
        public String ClienteCnpj { get; set; }

        /// <summary>
        /// Column CLIENTE_NOME
        /// </summary>
        [DbColumn("CLIENTE_NOME")]
        
        public String ClienteNome { get; set; }

        /// <summary>
        /// Column NF_NUMERO
        /// </summary>
        [DbColumn("NF_NUMERO")]
        
        public String NfNumero { get; set; }

        /// <summary>
        /// Column NF_SERIE
        /// </summary>
        [DbColumn("NF_SERIE")]
        
        public String NfSerie { get; set; }

        /// <summary>
        /// Column NF_DATA
        /// </summary>
        [DbColumn("NF_DATA")]
        
        public DateTime? NfData { get; set; }

        /// <summary>
        /// Column NF_VALOR
        /// </summary>
        [DbColumn("NF_VALOR")]
        
        public Double? NfValor { get; set; }

        /// <summary>
        /// Column NF_PESO_DECLARADO
        /// </summary>
        [DbColumn("NF_PESO_DECLARADO")]
        
        public Decimal? NfPesoDeclarado { get; set; }

        /// <summary>
        /// Column NF_PESO_DESCARREGADO
        /// </summary>
        [DbColumn("NF_PESO_DESCARREGADO")]
        
        public Int64? NfPesoDescarregado { get; set; }

        /// <summary>
        /// Column CHAVE
        /// </summary>
        [DbColumn("CHAVE")]
        
        public String Chave { get; set; }

        /// <summary>
        /// Column CIDADE_ORIGEM
        /// </summary>
        [DbColumn("CIDADE_ORIGEM")]
        
        public String CidadeOrigem { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwDescarrgaDiaria value)
        {
            if (value == null)
                return false;
            return
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.Produto == value.Produto &&
				this.DataMarcacao == value.DataMarcacao &&
				this.DataEntrada == value.DataEntrada &&
				this.DataSaida == value.DataSaida &&
				this.HorasNaFila == value.HorasNaFila &&
				this.Status == value.Status &&
				this.Placa == value.Placa &&
				this.Motorista == value.Motorista &&
				this.PesoBruto == value.PesoBruto &&
				this.PesoTara == value.PesoTara &&
				this.PesoLiquido == value.PesoLiquido &&
				this.CteRodo == value.CteRodo &&
				this.ClienteCnpj == value.ClienteCnpj &&
				this.ClienteNome == value.ClienteNome &&
				this.NfNumero == value.NfNumero &&
				this.NfSerie == value.NfSerie &&
				this.NfData == value.NfData &&
				this.NfValor == value.NfValor &&
				this.NfPesoDeclarado == value.NfPesoDeclarado &&
				this.NfPesoDescarregado == value.NfPesoDescarregado &&
				this.Chave == value.Chave &&
				this.CidadeOrigem == value.CidadeOrigem;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwDescarrgaDiaria CloneT()
        {
            VwDescarrgaDiaria value = new VwDescarrgaDiaria();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.Produto = this.Produto;
			value.DataMarcacao = this.DataMarcacao;
			value.DataEntrada = this.DataEntrada;
			value.DataSaida = this.DataSaida;
			value.HorasNaFila = this.HorasNaFila;
			value.Status = this.Status;
			value.Placa = this.Placa;
			value.Motorista = this.Motorista;
			value.PesoBruto = this.PesoBruto;
			value.PesoTara = this.PesoTara;
			value.PesoLiquido = this.PesoLiquido;
			value.CteRodo = this.CteRodo;
			value.ClienteCnpj = this.ClienteCnpj;
			value.ClienteNome = this.ClienteNome;
			value.NfNumero = this.NfNumero;
			value.NfSerie = this.NfSerie;
			value.NfData = this.NfData;
			value.NfValor = this.NfValor;
			value.NfPesoDeclarado = this.NfPesoDeclarado;
			value.NfPesoDescarregado = this.NfPesoDescarregado;
			value.Chave = this.Chave;
			value.CidadeOrigem = this.CidadeOrigem;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwDescarrgaDiaria Create(String _Terminal, String _Contrato, String _Produto, DateTime _DataMarcacao, DateTime? _DataEntrada, DateTime? _DataSaida, Decimal? _HorasNaFila, String _Status, String _Placa, String _Motorista, Decimal? _PesoBruto, Decimal? _PesoTara, Decimal? _PesoLiquido, String _CteRodo, String _ClienteCnpj, String _ClienteNome, String _NfNumero, String _NfSerie, DateTime? _NfData, Double? _NfValor, Decimal? _NfPesoDeclarado, Int64? _NfPesoDescarregado, String _Chave, String _CidadeOrigem)
        {
            VwDescarrgaDiaria __value = new VwDescarrgaDiaria();

			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.Produto = _Produto;
			__value.DataMarcacao = _DataMarcacao;
			__value.DataEntrada = _DataEntrada;
			__value.DataSaida = _DataSaida;
			__value.HorasNaFila = _HorasNaFila;
			__value.Status = _Status;
			__value.Placa = _Placa;
			__value.Motorista = _Motorista;
			__value.PesoBruto = _PesoBruto;
			__value.PesoTara = _PesoTara;
			__value.PesoLiquido = _PesoLiquido;
			__value.CteRodo = _CteRodo;
			__value.ClienteCnpj = _ClienteCnpj;
			__value.ClienteNome = _ClienteNome;
			__value.NfNumero = _NfNumero;
			__value.NfSerie = _NfSerie;
			__value.NfData = _NfData;
			__value.NfValor = _NfValor;
			__value.NfPesoDeclarado = _NfPesoDeclarado;
			__value.NfPesoDescarregado = _NfPesoDescarregado;
			__value.Chave = _Chave;
			__value.CidadeOrigem = _CidadeOrigem;

            return __value;
        }

        #endregion Create

   }

}
