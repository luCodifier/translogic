// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CAD_MUNICIPIO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrCadMunicipio", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrCadMunicipio : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column COD_MUNICIPIO
        /// </summary>
        [DbColumn("COD_MUNICIPIO")]
        
        public Int32? CodMunicipio { get; set; }

        /// <summary>
        /// Column NOM_MUNICIPIO
        /// </summary>
        [DbColumn("NOM_MUNICIPIO")]
        
        public String NomMunicipio { get; set; }

        /// <summary>
        /// Column COD_UF_MUNICIPIO
        /// </summary>
        [DbColumn("COD_UF_MUNICIPIO")]
        
        public String CodUfMunicipio { get; set; }

        /// <summary>
        /// Column MUN_TSP
        /// </summary>
        [DbColumn("MUN_TSP")]
        
        public DateTime? MunTsp { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrCadMunicipio value)
        {
            if (value == null)
                return false;
            return
				this.CodMunicipio == value.CodMunicipio &&
				this.NomMunicipio == value.NomMunicipio &&
				this.CodUfMunicipio == value.CodUfMunicipio &&
				this.MunTsp == value.MunTsp;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrCadMunicipio CloneT()
        {
            PtrCadMunicipio value = new PtrCadMunicipio();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.CodMunicipio = this.CodMunicipio;
			value.NomMunicipio = this.NomMunicipio;
			value.CodUfMunicipio = this.CodUfMunicipio;
			value.MunTsp = this.MunTsp;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrCadMunicipio Create(Int32 _CodMunicipio, String _NomMunicipio, String _CodUfMunicipio, DateTime? _MunTsp)
        {
            PtrCadMunicipio __value = new PtrCadMunicipio();

			__value.CodMunicipio = _CodMunicipio;
			__value.NomMunicipio = _NomMunicipio;
			__value.CodUfMunicipio = _CodUfMunicipio;
			__value.MunTsp = _MunTsp;

            return __value;
        }

        #endregion Create

   }

}
