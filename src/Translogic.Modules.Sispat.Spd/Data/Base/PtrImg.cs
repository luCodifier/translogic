// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_IMG", "", "")]
    [Serializable]
    [DataContract(Name = "PtrImg", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrImg : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SEQ_PESAGEM
        /// </summary>
        [DbColumn("SEQ_PESAGEM")]
        
        public String SeqPesagem { get; set; }

        /// <summary>
        /// Column IMG
        /// </summary>
        [DbColumn("IMG")]
        
        public Byte[] Img { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrImg value)
        {
            if (value == null)
                return false;
            return
				this.SeqPesagem == value.SeqPesagem &&
				this.Img == value.Img;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrImg CloneT()
        {
            PtrImg value = new PtrImg();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SeqPesagem = this.SeqPesagem;
			value.Img = this.Img;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrImg Create(String _SeqPesagem, Byte[] _Img)
        {
            PtrImg __value = new PtrImg();

			__value.SeqPesagem = _SeqPesagem;
			__value.Img = _Img;

            return __value;
        }

        #endregion Create

   }

}
