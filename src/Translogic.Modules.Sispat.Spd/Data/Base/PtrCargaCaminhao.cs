// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CARGA_CAMINHAO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrCargaCaminhao", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrCargaCaminhao : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Int64? SqCaminhao { get; set; }

        /// <summary>
        /// Column NUM_PLACA
        /// </summary>
        [DbColumn("NUM_PLACA")]
        
        public String NumPlaca { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column NUM_NF_SUBSTITUIDA
        /// </summary>
        [DbColumn("NUM_NF_SUBSTITUIDA")]
        
        public Int64? NumNfSubstituida { get; set; }

        /// <summary>
        /// Column SERIE_NF
        /// </summary>
        [DbColumn("SERIE_NF")]
        
        public Int64? SerieNf { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_VAZIO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_VAZIO")]
        
        public DateTime? DataHoraPesoVazio { get; set; }

        /// <summary>
        /// Column TARA
        /// </summary>
        [DbColumn("TARA")]
        
        public Int32? Tara { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARREGADO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARREGADO")]
        
        public DateTime? DataHoraPesoCarregado { get; set; }

        /// <summary>
        /// Column PESO_BRUTO
        /// </summary>
        [DbColumn("PESO_BRUTO")]
        
        public Int32? PesoBruto { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARR_ANTERIOR
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARR_ANTERIOR")]
        
        public DateTime? DataHoraPesoCarrAnterior { get; set; }

        /// <summary>
        /// Column FLAG_ABERTO
        /// </summary>
        [DbColumn("FLAG_ABERTO")]
        
        public String FlagAberto { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column FLAG_MEIA_CARGA
        /// </summary>
        [DbColumn("FLAG_MEIA_CARGA")]
        
        public String FlagMeiaCarga { get; set; }

        /// <summary>
        /// Column VR_FATURA
        /// </summary>
        [DbColumn("VR_FATURA")]
        
        public Decimal? VrFatura { get; set; }

        /// <summary>
        /// Column PB1
        /// </summary>
        [DbColumn("PB1")]
        
        public Int64? Pb1 { get; set; }

        /// <summary>
        /// Column PB2
        /// </summary>
        [DbColumn("PB2")]
        
        public Int64? Pb2 { get; set; }

        /// <summary>
        /// Column PB3
        /// </summary>
        [DbColumn("PB3")]
        
        public Int64? Pb3 { get; set; }

        /// <summary>
        /// Column PB4
        /// </summary>
        [DbColumn("PB4")]
        
        public Int64? Pb4 { get; set; }

        /// <summary>
        /// Column T1
        /// </summary>
        [DbColumn("T1")]
        
        public Int64? T1 { get; set; }

        /// <summary>
        /// Column T2
        /// </summary>
        [DbColumn("T2")]
        
        public Int64? T2 { get; set; }

        /// <summary>
        /// Column T3
        /// </summary>
        [DbColumn("T3")]
        
        public Int64? T3 { get; set; }

        /// <summary>
        /// Column T4
        /// </summary>
        [DbColumn("T4")]
        
        public Int64? T4 { get; set; }

        /// <summary>
        /// Column TU_REFERENCIA
        /// </summary>
        [DbColumn("TU_REFERENCIA")]
        
        public Int64? TuReferencia { get; set; }

        /// <summary>
        /// Column MD
        /// </summary>
        [DbColumn("MD")]
        
        public String Md { get; set; }

        /// <summary>
        /// Column FLUXO_CARREGADO
        /// </summary>
        [DbColumn("FLUXO_CARREGADO")]
        
        public String FluxoCarregado { get; set; }

        /// <summary>
        /// Column FLAG
        /// </summary>
        [DbColumn("FLAG")]
        
        public String Flag { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrCargaCaminhao value)
        {
            if (value == null)
                return false;
            return
				this.SqCaminhao == value.SqCaminhao &&
				this.NumPlaca == value.NumPlaca &&
				this.SqNf == value.SqNf &&
				this.NumNfSubstituida == value.NumNfSubstituida &&
				this.SerieNf == value.SerieNf &&
				this.CodContrato == value.CodContrato &&
				this.DataHoraPesoVazio == value.DataHoraPesoVazio &&
				this.Tara == value.Tara &&
				this.DataHoraPesoCarregado == value.DataHoraPesoCarregado &&
				this.PesoBruto == value.PesoBruto &&
				this.DataHoraPesoCarrAnterior == value.DataHoraPesoCarrAnterior &&
				this.FlagAberto == value.FlagAberto &&
				this.CodTerminal == value.CodTerminal &&
				this.FlagMeiaCarga == value.FlagMeiaCarga &&
				this.VrFatura == value.VrFatura &&
				this.Pb1 == value.Pb1 &&
				this.Pb2 == value.Pb2 &&
				this.Pb3 == value.Pb3 &&
				this.Pb4 == value.Pb4 &&
				this.T1 == value.T1 &&
				this.T2 == value.T2 &&
				this.T3 == value.T3 &&
				this.T4 == value.T4 &&
				this.TuReferencia == value.TuReferencia &&
				this.Md == value.Md &&
				this.FluxoCarregado == value.FluxoCarregado &&
				this.Flag == value.Flag;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrCargaCaminhao CloneT()
        {
            PtrCargaCaminhao value = new PtrCargaCaminhao();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqCaminhao = this.SqCaminhao;
			value.NumPlaca = this.NumPlaca;
			value.SqNf = this.SqNf;
			value.NumNfSubstituida = this.NumNfSubstituida;
			value.SerieNf = this.SerieNf;
			value.CodContrato = this.CodContrato;
			value.DataHoraPesoVazio = this.DataHoraPesoVazio;
			value.Tara = this.Tara;
			value.DataHoraPesoCarregado = this.DataHoraPesoCarregado;
			value.PesoBruto = this.PesoBruto;
			value.DataHoraPesoCarrAnterior = this.DataHoraPesoCarrAnterior;
			value.FlagAberto = this.FlagAberto;
			value.CodTerminal = this.CodTerminal;
			value.FlagMeiaCarga = this.FlagMeiaCarga;
			value.VrFatura = this.VrFatura;
			value.Pb1 = this.Pb1;
			value.Pb2 = this.Pb2;
			value.Pb3 = this.Pb3;
			value.Pb4 = this.Pb4;
			value.T1 = this.T1;
			value.T2 = this.T2;
			value.T3 = this.T3;
			value.T4 = this.T4;
			value.TuReferencia = this.TuReferencia;
			value.Md = this.Md;
			value.FluxoCarregado = this.FluxoCarregado;
			value.Flag = this.Flag;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrCargaCaminhao Create(Int64 _SqCaminhao, String _NumPlaca, Int64? _SqNf, Int64? _NumNfSubstituida, Int64? _SerieNf, String _CodContrato, DateTime _DataHoraPesoVazio, Int32? _Tara, DateTime? _DataHoraPesoCarregado, Int32? _PesoBruto, DateTime? _DataHoraPesoCarrAnterior, String _FlagAberto, String _CodTerminal, String _FlagMeiaCarga, Decimal? _VrFatura, Int64? _Pb1, Int64? _Pb2, Int64? _Pb3, Int64? _Pb4, Int64? _T1, Int64? _T2, Int64? _T3, Int64? _T4, Int64? _TuReferencia, String _Md, String _FluxoCarregado, String _Flag)
        {
            PtrCargaCaminhao __value = new PtrCargaCaminhao();

			__value.SqCaminhao = _SqCaminhao;
			__value.NumPlaca = _NumPlaca;
			__value.SqNf = _SqNf;
			__value.NumNfSubstituida = _NumNfSubstituida;
			__value.SerieNf = _SerieNf;
			__value.CodContrato = _CodContrato;
			__value.DataHoraPesoVazio = _DataHoraPesoVazio;
			__value.Tara = _Tara;
			__value.DataHoraPesoCarregado = _DataHoraPesoCarregado;
			__value.PesoBruto = _PesoBruto;
			__value.DataHoraPesoCarrAnterior = _DataHoraPesoCarrAnterior;
			__value.FlagAberto = _FlagAberto;
			__value.CodTerminal = _CodTerminal;
			__value.FlagMeiaCarga = _FlagMeiaCarga;
			__value.VrFatura = _VrFatura;
			__value.Pb1 = _Pb1;
			__value.Pb2 = _Pb2;
			__value.Pb3 = _Pb3;
			__value.Pb4 = _Pb4;
			__value.T1 = _T1;
			__value.T2 = _T2;
			__value.T3 = _T3;
			__value.T4 = _T4;
			__value.TuReferencia = _TuReferencia;
			__value.Md = _Md;
			__value.FluxoCarregado = _FluxoCarregado;
			__value.Flag = _Flag;

            return __value;
        }

        #endregion Create

   }

}
