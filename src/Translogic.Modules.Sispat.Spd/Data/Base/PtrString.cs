// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_STRING", "", "")]
    [Serializable]
    [DataContract(Name = "PtrString", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrString : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column USUARIO
        /// </summary>
        [DbColumn("USUARIO")]
        
        public Decimal? Usuario { get; set; }

        /// <summary>
        /// Column PDATA_HORA_EVENTO_INICIAL
        /// </summary>
        [DbColumn("PDATA_HORA_EVENTO_INICIAL")]
        
        public DateTime? PdataHoraEventoInicial { get; set; }

        /// <summary>
        /// Column PNUM_PLACA
        /// </summary>
        [DbColumn("PNUM_PLACA")]
        
        public String PnumPlaca { get; set; }

        /// <summary>
        /// Column SENTIDO
        /// </summary>
        [DbColumn("SENTIDO")]
        
        public String Sentido { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrString value)
        {
            if (value == null)
                return false;
            return
				this.Usuario == value.Usuario &&
				this.PdataHoraEventoInicial == value.PdataHoraEventoInicial &&
				this.PnumPlaca == value.PnumPlaca &&
				this.Sentido == value.Sentido;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrString CloneT()
        {
            PtrString value = new PtrString();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Usuario = this.Usuario;
			value.PdataHoraEventoInicial = this.PdataHoraEventoInicial;
			value.PnumPlaca = this.PnumPlaca;
			value.Sentido = this.Sentido;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrString Create(Decimal? _Usuario, DateTime? _PdataHoraEventoInicial, String _PnumPlaca, String _Sentido)
        {
            PtrString __value = new PtrString();

			__value.Usuario = _Usuario;
			__value.PdataHoraEventoInicial = _PdataHoraEventoInicial;
			__value.PnumPlaca = _PnumPlaca;
			__value.Sentido = _Sentido;

            return __value;
        }

        #endregion Create

   }

}
