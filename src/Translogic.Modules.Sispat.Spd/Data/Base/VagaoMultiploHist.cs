// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VAGAO_MULTIPLO_HIST", "", "")]
    [Serializable]
    [DataContract(Name = "VagaoMultiploHist", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VagaoMultiploHist : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column VMH_ID
        /// </summary>
        [DbColumn("VMH_ID")]
        
        public Decimal? VmhId { get; set; }

        /// <summary>
        /// Column VAG_ID_ORI
        /// </summary>
        [DbColumn("VAG_ID_ORI")]
        
        public Decimal? VagIdOri { get; set; }

        /// <summary>
        /// Column VAG_ID_DES
        /// </summary>
        [DbColumn("VAG_ID_DES")]
        
        public Decimal? VagIdDes { get; set; }

        /// <summary>
        /// Column VMH_TIMESTAMP
        /// </summary>
        [DbColumn("VMH_TIMESTAMP")]
        
        public DateTime? VmhTimestamp { get; set; }

        /// <summary>
        /// Column USU_ID
        /// </summary>
        [DbColumn("USU_ID")]
        
        public Decimal? UsuId { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VagaoMultiploHist value)
        {
            if (value == null)
                return false;
            return
				this.VmhId == value.VmhId &&
				this.VagIdOri == value.VagIdOri &&
				this.VagIdDes == value.VagIdDes &&
				this.VmhTimestamp == value.VmhTimestamp &&
				this.UsuId == value.UsuId;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VagaoMultiploHist CloneT()
        {
            VagaoMultiploHist value = new VagaoMultiploHist();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.VmhId = this.VmhId;
			value.VagIdOri = this.VagIdOri;
			value.VagIdDes = this.VagIdDes;
			value.VmhTimestamp = this.VmhTimestamp;
			value.UsuId = this.UsuId;

            return value;
        }

        #endregion Clone

        #region Create

        public static VagaoMultiploHist Create(Decimal _VmhId, Decimal _VagIdOri, Decimal _VagIdDes, DateTime _VmhTimestamp, Decimal _UsuId)
        {
            VagaoMultiploHist __value = new VagaoMultiploHist();

			__value.VmhId = _VmhId;
			__value.VagIdOri = _VagIdOri;
			__value.VagIdDes = _VagIdDes;
			__value.VmhTimestamp = _VmhTimestamp;
			__value.UsuId = _UsuId;

            return __value;
        }

        #endregion Create

   }

}
