// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "ROLE", "", "")]
    [Serializable]
    [DataContract(Name = "Role", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class Role : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ROLEID
        /// </summary>
        [DbColumn("ROLEID")]
        
        public Int64? Roleid { get; set; }

        /// <summary>
        /// Column DESCR
        /// </summary>
        [DbColumn("DESCR")]
        
        public String Descr { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(Role value)
        {
            if (value == null)
                return false;
            return
				this.Roleid == value.Roleid &&
				this.Descr == value.Descr;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public Role CloneT()
        {
            Role value = new Role();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Roleid = this.Roleid;
			value.Descr = this.Descr;

            return value;
        }

        #endregion Clone

        #region Create

        public static Role Create(Int64 _Roleid, String _Descr)
        {
            Role __value = new Role();

			__value.Roleid = _Roleid;
			__value.Descr = _Descr;

            return __value;
        }

        #endregion Create

   }

}
