// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_CONSULTA_AGENDAMENTO_NOVO", "", "")]
    [Serializable]
    [DataContract(Name = "VwConsultaAgendamentoNovo", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwConsultaAgendamentoNovo : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column MERCADORIA
        /// </summary>
        [DbColumn("MERCADORIA")]
        
        public String Mercadoria { get; set; }

        /// <summary>
        /// Column DATA_PREVISAO_DESCARGA
        /// </summary>
        [DbColumn("DATA_PREVISAO_DESCARGA")]
        
        public DateTime? DataPrevisaoDescarga { get; set; }

        /// <summary>
        /// Column QTD_COTAS_LIBERADAS
        /// </summary>
        [DbColumn("QTD_COTAS_LIBERADAS")]
        
        public Decimal? QtdCotasLiberadas { get; set; }

        /// <summary>
        /// Column QTD_AGENDADO
        /// </summary>
        [DbColumn("QTD_AGENDADO")]
        
        public Decimal? QtdAgendado { get; set; }

        /// <summary>
        /// Column QTD_MARCADO
        /// </summary>
        [DbColumn("QTD_MARCADO")]
        
        public Decimal? QtdMarcado { get; set; }

        /// <summary>
        /// Column DIFERENCA
        /// </summary>
        [DbColumn("DIFERENCA")]
        
        public Decimal? Diferenca { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwConsultaAgendamentoNovo value)
        {
            if (value == null)
                return false;
            return
				this.Terminal == value.Terminal &&
				this.Mercadoria == value.Mercadoria &&
				this.DataPrevisaoDescarga == value.DataPrevisaoDescarga &&
				this.QtdCotasLiberadas == value.QtdCotasLiberadas &&
				this.QtdAgendado == value.QtdAgendado &&
				this.QtdMarcado == value.QtdMarcado &&
				this.Diferenca == value.Diferenca;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwConsultaAgendamentoNovo CloneT()
        {
            VwConsultaAgendamentoNovo value = new VwConsultaAgendamentoNovo();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Terminal = this.Terminal;
			value.Mercadoria = this.Mercadoria;
			value.DataPrevisaoDescarga = this.DataPrevisaoDescarga;
			value.QtdCotasLiberadas = this.QtdCotasLiberadas;
			value.QtdAgendado = this.QtdAgendado;
			value.QtdMarcado = this.QtdMarcado;
			value.Diferenca = this.Diferenca;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwConsultaAgendamentoNovo Create(String _Terminal, String _Mercadoria, DateTime _DataPrevisaoDescarga, Decimal? _QtdCotasLiberadas, Decimal? _QtdAgendado, Decimal? _QtdMarcado, Decimal? _Diferenca)
        {
            VwConsultaAgendamentoNovo __value = new VwConsultaAgendamentoNovo();

			__value.Terminal = _Terminal;
			__value.Mercadoria = _Mercadoria;
			__value.DataPrevisaoDescarga = _DataPrevisaoDescarga;
			__value.QtdCotasLiberadas = _QtdCotasLiberadas;
			__value.QtdAgendado = _QtdAgendado;
			__value.QtdMarcado = _QtdMarcado;
			__value.Diferenca = _Diferenca;

            return __value;
        }

        #endregion Create

   }

}
