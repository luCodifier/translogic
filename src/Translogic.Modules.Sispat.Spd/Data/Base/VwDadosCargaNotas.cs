// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_DADOS_CARGA_NOTAS", "", "")]
    [Serializable]
    [DataContract(Name = "VwDadosCargaNotas", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwDadosCargaNotas : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column NUM_NOTA_FISCAL
        /// </summary>
        [DbColumn("NUM_NOTA_FISCAL")]
        
        public String NumNotaFiscal { get; set; }

        /// <summary>
        /// Column PESO_DECLARADO
        /// </summary>
        [DbColumn("PESO_DECLARADO")]
        
        public Double? PesoDeclarado { get; set; }

        /// <summary>
        /// Column PESO_CARREGADO
        /// </summary>
        [DbColumn("PESO_CARREGADO")]
        
        public Int64? PesoCarregado { get; set; }

        /// <summary>
        /// Column NFE_CHAVE
        /// </summary>
        [DbColumn("NFE_CHAVE")]
        
        public String NfeChave { get; set; }

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwDadosCargaNotas value)
        {
            if (value == null)
                return false;
            return
				this.SqNf == value.SqNf &&
				this.NumNotaFiscal == value.NumNotaFiscal &&
				this.PesoDeclarado == value.PesoDeclarado &&
				this.PesoCarregado == value.PesoCarregado &&
				this.NfeChave == value.NfeChave &&
				this.SqVagao == value.SqVagao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwDadosCargaNotas CloneT()
        {
            VwDadosCargaNotas value = new VwDadosCargaNotas();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqNf = this.SqNf;
			value.NumNotaFiscal = this.NumNotaFiscal;
			value.PesoDeclarado = this.PesoDeclarado;
			value.PesoCarregado = this.PesoCarregado;
			value.NfeChave = this.NfeChave;
			value.SqVagao = this.SqVagao;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwDadosCargaNotas Create(Int64 _SqNf, String _NumNotaFiscal, Double? _PesoDeclarado, Int64? _PesoCarregado, String _NfeChave, Int64 _SqVagao)
        {
            VwDadosCargaNotas __value = new VwDadosCargaNotas();

			__value.SqNf = _SqNf;
			__value.NumNotaFiscal = _NumNotaFiscal;
			__value.PesoDeclarado = _PesoDeclarado;
			__value.PesoCarregado = _PesoCarregado;
			__value.NfeChave = _NfeChave;
			__value.SqVagao = _SqVagao;

            return __value;
        }

        #endregion Create

   }

}
