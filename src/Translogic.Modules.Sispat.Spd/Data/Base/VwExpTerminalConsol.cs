// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_EXP_TERMINAL_CONSOL", "", "")]
    [Serializable]
    [DataContract(Name = "VwExpTerminalConsol", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwExpTerminalConsol : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column CLIENTE
        /// </summary>
        [DbColumn("CLIENTE")]
        
        public String Cliente { get; set; }

        /// <summary>
        /// Column GRUPO_CLIENTE
        /// </summary>
        [DbColumn("GRUPO_CLIENTE")]
        
        public String GrupoCliente { get; set; }

        /// <summary>
        /// Column VAGAO
        /// </summary>
        [DbColumn("VAGAO")]
        
        public String Vagao { get; set; }

        /// <summary>
        /// Column SERIE_VAGAO
        /// </summary>
        [DbColumn("SERIE_VAGAO")]
        
        public String SerieVagao { get; set; }

        /// <summary>
        /// Column DATA_CADASTRO
        /// </summary>
        [DbColumn("DATA_CADASTRO")]
        
        public DateTime? DataCadastro { get; set; }

        /// <summary>
        /// Column DATA_CARREGAMENTO
        /// </summary>
        [DbColumn("DATA_CARREGAMENTO")]
        
        public DateTime? DataCarregamento { get; set; }

        /// <summary>
        /// Column MULTIPLO
        /// </summary>
        [DbColumn("MULTIPLO")]
        
        public String Multiplo { get; set; }

        /// <summary>
        /// Column EXPEDIDO
        /// </summary>
        [DbColumn("EXPEDIDO")]
        
        public String Expedido { get; set; }

        /// <summary>
        /// Column AUTOMATICA
        /// </summary>
        [DbColumn("AUTOMATICA")]
        
        public String Automatica { get; set; }

        /// <summary>
        /// Column NOTAS_FISCAIS
        /// </summary>
        [DbColumn("NOTAS_FISCAIS")]
        
        public String NotasFiscais { get; set; }

        /// <summary>
        /// Column DCL_SERIE
        /// </summary>
        [DbColumn("DCL_SERIE")]
        
        public String DclSerie { get; set; }

        /// <summary>
        /// Column DCL_NUMERO
        /// </summary>
        [DbColumn("DCL_NUMERO")]
        
        public String DclNumero { get; set; }

        /// <summary>
        /// Column FLUXOS
        /// </summary>
        [DbColumn("FLUXOS")]
        
        public String Fluxos { get; set; }

        /// <summary>
        /// Column CTE
        /// </summary>
        [DbColumn("CTE")]
        
        public String Cte { get; set; }

        /// <summary>
        /// Column PESO_LIQUIDO
        /// </summary>
        [DbColumn("PESO_LIQUIDO")]
        
        public String PesoLiquido { get; set; }

        /// <summary>
        /// Column PESO_TARA
        /// </summary>
        [DbColumn("PESO_TARA")]
        
        public Int32? PesoTara { get; set; }

        /// <summary>
        /// Column DATA_DESCARGA_PORTO
        /// </summary>
        [DbColumn("DATA_DESCARGA_PORTO")]
        
        public String DataDescargaPorto { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwExpTerminalConsol value)
        {
            if (value == null)
                return false;
            return
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.Cliente == value.Cliente &&
				this.GrupoCliente == value.GrupoCliente &&
				this.Vagao == value.Vagao &&
				this.SerieVagao == value.SerieVagao &&
				this.DataCadastro == value.DataCadastro &&
				this.DataCarregamento == value.DataCarregamento &&
				this.Multiplo == value.Multiplo &&
				this.Expedido == value.Expedido &&
				this.Automatica == value.Automatica &&
				this.NotasFiscais == value.NotasFiscais &&
				this.DclSerie == value.DclSerie &&
				this.DclNumero == value.DclNumero &&
				this.Fluxos == value.Fluxos &&
				this.Cte == value.Cte &&
				this.PesoLiquido == value.PesoLiquido &&
				this.PesoTara == value.PesoTara &&
				this.DataDescargaPorto == value.DataDescargaPorto;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwExpTerminalConsol CloneT()
        {
            VwExpTerminalConsol value = new VwExpTerminalConsol();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.Cliente = this.Cliente;
			value.GrupoCliente = this.GrupoCliente;
			value.Vagao = this.Vagao;
			value.SerieVagao = this.SerieVagao;
			value.DataCadastro = this.DataCadastro;
			value.DataCarregamento = this.DataCarregamento;
			value.Multiplo = this.Multiplo;
			value.Expedido = this.Expedido;
			value.Automatica = this.Automatica;
			value.NotasFiscais = this.NotasFiscais;
			value.DclSerie = this.DclSerie;
			value.DclNumero = this.DclNumero;
			value.Fluxos = this.Fluxos;
			value.Cte = this.Cte;
			value.PesoLiquido = this.PesoLiquido;
			value.PesoTara = this.PesoTara;
			value.DataDescargaPorto = this.DataDescargaPorto;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwExpTerminalConsol Create(String _Terminal, String _Contrato, String _Cliente, String _GrupoCliente, String _Vagao, String _SerieVagao, DateTime _DataCadastro, DateTime? _DataCarregamento, String _Multiplo, String _Expedido, String _Automatica, String _NotasFiscais, String _DclSerie, String _DclNumero, String _Fluxos, String _Cte, String _PesoLiquido, Int32? _PesoTara, String _DataDescargaPorto)
        {
            VwExpTerminalConsol __value = new VwExpTerminalConsol();

			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.Cliente = _Cliente;
			__value.GrupoCliente = _GrupoCliente;
			__value.Vagao = _Vagao;
			__value.SerieVagao = _SerieVagao;
			__value.DataCadastro = _DataCadastro;
			__value.DataCarregamento = _DataCarregamento;
			__value.Multiplo = _Multiplo;
			__value.Expedido = _Expedido;
			__value.Automatica = _Automatica;
			__value.NotasFiscais = _NotasFiscais;
			__value.DclSerie = _DclSerie;
			__value.DclNumero = _DclNumero;
			__value.Fluxos = _Fluxos;
			__value.Cte = _Cte;
			__value.PesoLiquido = _PesoLiquido;
			__value.PesoTara = _PesoTara;
			__value.DataDescargaPorto = _DataDescargaPorto;

            return __value;
        }

        #endregion Create

   }

}
