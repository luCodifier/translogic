// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "CAALL_AGENDAMENTO_VEICULO", "", "")]
    [Serializable]
    [DataContract(Name = "CaallAgendamentoVeiculo", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class CaallAgendamentoVeiculo : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column AGV_ID
        /// </summary>
        [DbColumn("AGV_ID")]
        
        public Decimal? AgvId { get; set; }

        /// <summary>
        /// Column AGV_TSP
        /// </summary>
        [DbColumn("AGV_TSP")]
        
        public DateTime? AgvTsp { get; set; }

        /// <summary>
        /// Column AGE_ID
        /// </summary>
        [DbColumn("AGE_ID")]
        
        public Decimal? AgeId { get; set; }

        /// <summary>
        /// Column VEI_ID
        /// </summary>
        [DbColumn("VEI_ID")]
        
        public Decimal? VeiId { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(CaallAgendamentoVeiculo value)
        {
            if (value == null)
                return false;
            return
				this.AgvId == value.AgvId &&
				this.AgvTsp == value.AgvTsp &&
				this.AgeId == value.AgeId &&
				this.VeiId == value.VeiId;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public CaallAgendamentoVeiculo CloneT()
        {
            CaallAgendamentoVeiculo value = new CaallAgendamentoVeiculo();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.AgvId = this.AgvId;
			value.AgvTsp = this.AgvTsp;
			value.AgeId = this.AgeId;
			value.VeiId = this.VeiId;

            return value;
        }

        #endregion Clone

        #region Create

        public static CaallAgendamentoVeiculo Create(Decimal _AgvId, DateTime _AgvTsp, Decimal _AgeId, Decimal _VeiId)
        {
            CaallAgendamentoVeiculo __value = new CaallAgendamentoVeiculo();

			__value.AgvId = _AgvId;
			__value.AgvTsp = _AgvTsp;
			__value.AgeId = _AgeId;
			__value.VeiId = _VeiId;

            return __value;
        }

        #endregion Create

   }

}
