// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_IMP_TICKET_AUTO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrImpTicketAuto", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrImpTicketAuto : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column NUM_PLACA
        /// </summary>
        [DbColumn("NUM_PLACA")]
        
        public String NumPlaca { get; set; }

        /// <summary>
        /// Column DATA_HORA_EVENTO_INICIAL
        /// </summary>
        [DbColumn("DATA_HORA_EVENTO_INICIAL")]
        
        public DateTime? DataHoraEventoInicial { get; set; }

        /// <summary>
        /// Column DATA_HORA_PRINT
        /// </summary>
        [DbColumn("DATA_HORA_PRINT")]
        
        public DateTime? DataHoraPrint { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrImpTicketAuto value)
        {
            if (value == null)
                return false;
            return
				this.NumPlaca == value.NumPlaca &&
				this.DataHoraEventoInicial == value.DataHoraEventoInicial &&
				this.DataHoraPrint == value.DataHoraPrint &&
				this.CodTerminal == value.CodTerminal;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrImpTicketAuto CloneT()
        {
            PtrImpTicketAuto value = new PtrImpTicketAuto();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.NumPlaca = this.NumPlaca;
			value.DataHoraEventoInicial = this.DataHoraEventoInicial;
			value.DataHoraPrint = this.DataHoraPrint;
			value.CodTerminal = this.CodTerminal;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrImpTicketAuto Create(String _NumPlaca, DateTime? _DataHoraEventoInicial, DateTime? _DataHoraPrint, String _CodTerminal)
        {
            PtrImpTicketAuto __value = new PtrImpTicketAuto();

			__value.NumPlaca = _NumPlaca;
			__value.DataHoraEventoInicial = _DataHoraEventoInicial;
			__value.DataHoraPrint = _DataHoraPrint;
			__value.CodTerminal = _CodTerminal;

            return __value;
        }

        #endregion Create

   }

}
