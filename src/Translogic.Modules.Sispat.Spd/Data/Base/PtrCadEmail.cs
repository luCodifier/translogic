// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CAD_EMAIL", "", "")]
    [Serializable]
    [DataContract(Name = "PtrCadEmail", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrCadEmail : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column PCE_ID
        /// </summary>
        [DbColumn("PCE_ID")]
        
        public Decimal? PceId { get; set; }

        /// <summary>
        /// Column PCE_TIMESTAMP
        /// </summary>
        [DbColumn("PCE_TIMESTAMP")]
        
        public DateTime? PceTimestamp { get; set; }

        /// <summary>
        /// Column PCE_NOM
        /// </summary>
        [DbColumn("PCE_NOM")]
        
        public String PceNom { get; set; }

        /// <summary>
        /// Column PCE_EML
        /// </summary>
        [DbColumn("PCE_EML")]
        
        public String PceEml { get; set; }

        /// <summary>
        /// Column PCE_ATV
        /// </summary>
        [DbColumn("PCE_ATV")]
        
        public String PceAtv { get; set; }

        /// <summary>
        /// Column GPE_CODIGO
        /// </summary>
        [DbColumn("GPE_CODIGO")]
        
        public Decimal? GpeCodigo { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrCadEmail value)
        {
            if (value == null)
                return false;
            return
				this.PceId == value.PceId &&
				this.PceTimestamp == value.PceTimestamp &&
				this.PceNom == value.PceNom &&
				this.PceEml == value.PceEml &&
				this.PceAtv == value.PceAtv &&
				this.GpeCodigo == value.GpeCodigo;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrCadEmail CloneT()
        {
            PtrCadEmail value = new PtrCadEmail();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.PceId = this.PceId;
			value.PceTimestamp = this.PceTimestamp;
			value.PceNom = this.PceNom;
			value.PceEml = this.PceEml;
			value.PceAtv = this.PceAtv;
			value.GpeCodigo = this.GpeCodigo;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrCadEmail Create(Decimal _PceId, DateTime _PceTimestamp, String _PceNom, String _PceEml, String _PceAtv, Decimal _GpeCodigo)
        {
            PtrCadEmail __value = new PtrCadEmail();

			__value.PceId = _PceId;
			__value.PceTimestamp = _PceTimestamp;
			__value.PceNom = _PceNom;
			__value.PceEml = _PceEml;
			__value.PceAtv = _PceAtv;
			__value.GpeCodigo = _GpeCodigo;

            return __value;
        }

        #endregion Create

   }

}
