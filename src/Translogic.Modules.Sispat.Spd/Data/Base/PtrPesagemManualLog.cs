// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_PESAGEM_MANUAL_LOG", "", "")]
    [Serializable]
    [DataContract(Name = "PtrPesagemManualLog", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrPesagemManualLog : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column PML_ID
        /// </summary>
        [DbColumn("PML_ID")]
        
        public Decimal? PmlId { get; set; }

        /// <summary>
        /// Column PML_TIMESTAMP
        /// </summary>
        [DbColumn("PML_TIMESTAMP")]
        
        public DateTime? PmlTimestamp { get; set; }

        /// <summary>
        /// Column USU_LOG_ID
        /// </summary>
        [DbColumn("USU_LOG_ID")]
        
        public Decimal? UsuLogId { get; set; }

        /// <summary>
        /// Column USU_SUP_ID
        /// </summary>
        [DbColumn("USU_SUP_ID")]
        
        public Decimal? UsuSupId { get; set; }

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Decimal? SqCaminhao { get; set; }

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Decimal? SqVagao { get; set; }

        /// <summary>
        /// Column PLM_JUS
        /// </summary>
        [DbColumn("PLM_JUS")]
        
        public String PlmJus { get; set; }

        /// <summary>
        /// Column PLM_OBS
        /// </summary>
        [DbColumn("PLM_OBS")]
        
        public String PlmObs { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrPesagemManualLog value)
        {
            if (value == null)
                return false;
            return
				this.PmlId == value.PmlId &&
				this.PmlTimestamp == value.PmlTimestamp &&
				this.UsuLogId == value.UsuLogId &&
				this.UsuSupId == value.UsuSupId &&
				this.SqCaminhao == value.SqCaminhao &&
				this.SqVagao == value.SqVagao &&
				this.PlmJus == value.PlmJus &&
				this.PlmObs == value.PlmObs;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrPesagemManualLog CloneT()
        {
            PtrPesagemManualLog value = new PtrPesagemManualLog();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.PmlId = this.PmlId;
			value.PmlTimestamp = this.PmlTimestamp;
			value.UsuLogId = this.UsuLogId;
			value.UsuSupId = this.UsuSupId;
			value.SqCaminhao = this.SqCaminhao;
			value.SqVagao = this.SqVagao;
			value.PlmJus = this.PlmJus;
			value.PlmObs = this.PlmObs;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrPesagemManualLog Create(Decimal _PmlId, DateTime _PmlTimestamp, Decimal _UsuLogId, Decimal _UsuSupId, Decimal? _SqCaminhao, Decimal? _SqVagao, String _PlmJus, String _PlmObs)
        {
            PtrPesagemManualLog __value = new PtrPesagemManualLog();

			__value.PmlId = _PmlId;
			__value.PmlTimestamp = _PmlTimestamp;
			__value.UsuLogId = _UsuLogId;
			__value.UsuSupId = _UsuSupId;
			__value.SqCaminhao = _SqCaminhao;
			__value.SqVagao = _SqVagao;
			__value.PlmJus = _PlmJus;
			__value.PlmObs = _PlmObs;

            return __value;
        }

        #endregion Create

   }

}
