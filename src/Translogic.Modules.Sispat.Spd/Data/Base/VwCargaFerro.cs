// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_CARGA_FERRO", "", "")]
    [Serializable]
    [DataContract(Name = "VwCargaFerro", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwCargaFerro : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column CON_DSC
        /// </summary>
        [DbColumn("CON_DSC")]
        
        public String ConDsc { get; set; }

        /// <summary>
        /// Column DATA_CARGA
        /// </summary>
        [DbColumn("DATA_CARGA")]
        
        public DateTime? DataCarga { get; set; }

        /// <summary>
        /// Column VAGAO
        /// </summary>
        [DbColumn("VAGAO")]
        
        public String Vagao { get; set; }

        /// <summary>
        /// Column SERIE
        /// </summary>
        [DbColumn("SERIE")]
        
        public String Serie { get; set; }

        /// <summary>
        /// Column PESO_BRUTO
        /// </summary>
        [DbColumn("PESO_BRUTO")]
        
        public Int32? PesoBruto { get; set; }

        /// <summary>
        /// Column PESO_TARA
        /// </summary>
        [DbColumn("PESO_TARA")]
        
        public Int32? PesoTara { get; set; }

        /// <summary>
        /// Column PESO_LIQUIDO
        /// </summary>
        [DbColumn("PESO_LIQUIDO")]
        
        public Decimal? PesoLiquido { get; set; }

        /// <summary>
        /// Column FLAG_ABERTO
        /// </summary>
        [DbColumn("FLAG_ABERTO")]
        
        public String FlagAberto { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        /// <summary>
        /// Column STATUS_ID
        /// </summary>
        [DbColumn("STATUS_ID")]
        
        public String StatusId { get; set; }

        /// <summary>
        /// Column IS_MULTIPLO
        /// </summary>
        [DbColumn("IS_MULTIPLO")]
        
        public String IsMultiplo { get; set; }

        /// <summary>
        /// Column BALANCA
        /// </summary>
        [DbColumn("BALANCA")]
        
        public String Balanca { get; set; }

        /// <summary>
        /// Column DESPACHO_SERIE
        /// </summary>
        [DbColumn("DESPACHO_SERIE")]
        
        public String DespachoSerie { get; set; }

        /// <summary>
        /// Column DESPACHO_NUMERO
        /// </summary>
        [DbColumn("DESPACHO_NUMERO")]
        
        public Int32? DespachoNumero { get; set; }

        /// <summary>
        /// Column CTE_SERIE
        /// </summary>
        [DbColumn("CTE_SERIE")]
        
        public String CteSerie { get; set; }

        /// <summary>
        /// Column CTE_NUMERO
        /// </summary>
        [DbColumn("CTE_NUMERO")]
        
        public String CteNumero { get; set; }

        /// <summary>
        /// Column DT_DESC_PORTO
        /// </summary>
        [DbColumn("DT_DESC_PORTO")]
        
        public DateTime? DtDescPorto { get; set; }

        /// <summary>
        /// Column CLIENTE
        /// </summary>
        [DbColumn("CLIENTE")]
        
        public String Cliente { get; set; }

        /// <summary>
        /// Column COD_PRODUTO
        /// </summary>
        [DbColumn("COD_PRODUTO")]
        
        public String CodProduto { get; set; }

        /// <summary>
        /// Column PRODUTO
        /// </summary>
        [DbColumn("PRODUTO")]
        
        public String Produto { get; set; }

        /// <summary>
        /// Column MERCADORIA
        /// </summary>
        [DbColumn("MERCADORIA")]
        
        public String Mercadoria { get; set; }

        /// <summary>
        /// Column FLUXO
        /// </summary>
        [DbColumn("FLUXO")]
        
        public String Fluxo { get; set; }

        /// <summary>
        /// Column TB_ALVO
        /// </summary>
        [DbColumn("TB_ALVO")]
        
        public Int64? TbAlvo { get; set; }

        /// <summary>
        /// Column ID_BO
        /// </summary>
        [DbColumn("ID_BO")]
        
        public Decimal? IdBo { get; set; }

        /// <summary>
        /// Column DATA_CARREGAMENTO_REAL
        /// </summary>
        [DbColumn("DATA_CARREGAMENTO_REAL")]
        
        public DateTime? DataCarregamentoReal { get; set; }

        /// <summary>
        /// Column SEGUNDA_MELHOR_PESAGEM
        /// </summary>
        [DbColumn("SEGUNDA_MELHOR_PESAGEM")]
        
        public Decimal? SegundaMelhorPesagem { get; set; }

        /// <summary>
        /// Column PERC_LOTACAO
        /// </summary>
        [DbColumn("PERC_LOTACAO")]
        
        public Decimal? PercLotacao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwCargaFerro value)
        {
            if (value == null)
                return false;
            return
				this.SqVagao == value.SqVagao &&
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.ConDsc == value.ConDsc &&
				this.DataCarga == value.DataCarga &&
				this.Vagao == value.Vagao &&
				this.Serie == value.Serie &&
				this.PesoBruto == value.PesoBruto &&
				this.PesoTara == value.PesoTara &&
				this.PesoLiquido == value.PesoLiquido &&
				this.FlagAberto == value.FlagAberto &&
				this.Status == value.Status &&
				this.StatusId == value.StatusId &&
				this.IsMultiplo == value.IsMultiplo &&
				this.Balanca == value.Balanca &&
				this.DespachoSerie == value.DespachoSerie &&
				this.DespachoNumero == value.DespachoNumero &&
				this.CteSerie == value.CteSerie &&
				this.CteNumero == value.CteNumero &&
				this.DtDescPorto == value.DtDescPorto &&
				this.Cliente == value.Cliente &&
				this.CodProduto == value.CodProduto &&
				this.Produto == value.Produto &&
				this.Mercadoria == value.Mercadoria &&
				this.Fluxo == value.Fluxo &&
				this.TbAlvo == value.TbAlvo &&
				this.IdBo == value.IdBo &&
				this.DataCarregamentoReal == value.DataCarregamentoReal &&
				this.SegundaMelhorPesagem == value.SegundaMelhorPesagem &&
				this.PercLotacao == value.PercLotacao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwCargaFerro CloneT()
        {
            VwCargaFerro value = new VwCargaFerro();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqVagao = this.SqVagao;
			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.ConDsc = this.ConDsc;
			value.DataCarga = this.DataCarga;
			value.Vagao = this.Vagao;
			value.Serie = this.Serie;
			value.PesoBruto = this.PesoBruto;
			value.PesoTara = this.PesoTara;
			value.PesoLiquido = this.PesoLiquido;
			value.FlagAberto = this.FlagAberto;
			value.Status = this.Status;
			value.StatusId = this.StatusId;
			value.IsMultiplo = this.IsMultiplo;
			value.Balanca = this.Balanca;
			value.DespachoSerie = this.DespachoSerie;
			value.DespachoNumero = this.DespachoNumero;
			value.CteSerie = this.CteSerie;
			value.CteNumero = this.CteNumero;
			value.DtDescPorto = this.DtDescPorto;
			value.Cliente = this.Cliente;
			value.CodProduto = this.CodProduto;
			value.Produto = this.Produto;
			value.Mercadoria = this.Mercadoria;
			value.Fluxo = this.Fluxo;
			value.TbAlvo = this.TbAlvo;
			value.IdBo = this.IdBo;
			value.DataCarregamentoReal = this.DataCarregamentoReal;
			value.SegundaMelhorPesagem = this.SegundaMelhorPesagem;
			value.PercLotacao = this.PercLotacao;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwCargaFerro Create(Int64 _SqVagao, String _Terminal, String _Contrato, String _ConDsc, DateTime _DataCarga, String _Vagao, String _Serie, Int32? _PesoBruto, Int32? _PesoTara, Decimal? _PesoLiquido, String _FlagAberto, String _Status, String _StatusId, String _IsMultiplo, String _Balanca, String _DespachoSerie, Int32? _DespachoNumero, String _CteSerie, String _CteNumero, DateTime? _DtDescPorto, String _Cliente, String _CodProduto, String _Produto, String _Mercadoria, String _Fluxo, Int64? _TbAlvo, Decimal? _IdBo, DateTime? _DataCarregamentoReal, Decimal? _SegundaMelhorPesagem, Decimal? _PercLotacao)
        {
            VwCargaFerro __value = new VwCargaFerro();

			__value.SqVagao = _SqVagao;
			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.ConDsc = _ConDsc;
			__value.DataCarga = _DataCarga;
			__value.Vagao = _Vagao;
			__value.Serie = _Serie;
			__value.PesoBruto = _PesoBruto;
			__value.PesoTara = _PesoTara;
			__value.PesoLiquido = _PesoLiquido;
			__value.FlagAberto = _FlagAberto;
			__value.Status = _Status;
			__value.StatusId = _StatusId;
			__value.IsMultiplo = _IsMultiplo;
			__value.Balanca = _Balanca;
			__value.DespachoSerie = _DespachoSerie;
			__value.DespachoNumero = _DespachoNumero;
			__value.CteSerie = _CteSerie;
			__value.CteNumero = _CteNumero;
			__value.DtDescPorto = _DtDescPorto;
			__value.Cliente = _Cliente;
			__value.CodProduto = _CodProduto;
			__value.Produto = _Produto;
			__value.Mercadoria = _Mercadoria;
			__value.Fluxo = _Fluxo;
			__value.TbAlvo = _TbAlvo;
			__value.IdBo = _IdBo;
			__value.DataCarregamentoReal = _DataCarregamentoReal;
			__value.SegundaMelhorPesagem = _SegundaMelhorPesagem;
			__value.PercLotacao = _PercLotacao;

            return __value;
        }

        #endregion Create

   }

}
