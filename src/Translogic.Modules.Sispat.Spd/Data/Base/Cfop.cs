// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "CFOP", "", "")]
    [Serializable]
    [DataContract(Name = "Cfop", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class Cfop : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column CODIGO
        /// </summary>
        [DbColumn("CODIGO")]
        
        public Int16? Codigo { get; set; }

        /// <summary>
        /// Column DESCRICAO
        /// </summary>
        [DbColumn("DESCRICAO")]
        
        public String Descricao { get; set; }

        /// <summary>
        /// Column EXPORTACAO
        /// </summary>
        [DbColumn("EXPORTACAO")]
        
        public Int16? Exportacao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(Cfop value)
        {
            if (value == null)
                return false;
            return
				this.Codigo == value.Codigo &&
				this.Descricao == value.Descricao &&
				this.Exportacao == value.Exportacao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public Cfop CloneT()
        {
            Cfop value = new Cfop();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Codigo = this.Codigo;
			value.Descricao = this.Descricao;
			value.Exportacao = this.Exportacao;

            return value;
        }

        #endregion Clone

        #region Create

        public static Cfop Create(Int16 _Codigo, String _Descricao, Int16? _Exportacao)
        {
            Cfop __value = new Cfop();

			__value.Codigo = _Codigo;
			__value.Descricao = _Descricao;
			__value.Exportacao = _Exportacao;

            return __value;
        }

        #endregion Create

   }

}
