// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "LOG_PTR_CONTRATO", "", "")]
    [Serializable]
    [DataContract(Name = "LogPtrContrato", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class LogPtrContrato : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column DESC_CONTRATO
        /// </summary>
        [DbColumn("DESC_CONTRATO")]
        
        public String DescContrato { get; set; }

        /// <summary>
        /// Column PRODUTO_CONTRATO
        /// </summary>
        [DbColumn("PRODUTO_CONTRATO")]
        
        public String ProdutoContrato { get; set; }

        /// <summary>
        /// Column CLIENTE_ORIGEM
        /// </summary>
        [DbColumn("CLIENTE_ORIGEM")]
        
        public String ClienteOrigem { get; set; }

        /// <summary>
        /// Column FILIAL_ORIGEM
        /// </summary>
        [DbColumn("FILIAL_ORIGEM")]
        
        public String FilialOrigem { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column FLUXO_SIGEFER
        /// </summary>
        [DbColumn("FLUXO_SIGEFER")]
        
        public String FluxoSigefer { get; set; }

        /// <summary>
        /// Column MOEDA_CONTRATO
        /// </summary>
        [DbColumn("MOEDA_CONTRATO")]
        
        public String MoedaContrato { get; set; }

        /// <summary>
        /// Column ALIQ_ICMS
        /// </summary>
        [DbColumn("ALIQ_ICMS")]
        
        public Int16? AliqIcms { get; set; }

        /// <summary>
        /// Column PESO_CONTRATADO
        /// </summary>
        [DbColumn("PESO_CONTRATADO")]
        
        public Int64? PesoContratado { get; set; }

        /// <summary>
        /// Column FLAG_ATIVO
        /// </summary>
        [DbColumn("FLAG_ATIVO")]
        
        public String FlagAtivo { get; set; }

        /// <summary>
        /// Column FLAG_CLASSIFICACAO
        /// </summary>
        [DbColumn("FLAG_CLASSIFICACAO")]
        
        public String FlagClassificacao { get; set; }

        /// <summary>
        /// Column END_EMAIL
        /// </summary>
        [DbColumn("END_EMAIL")]
        
        public String EndEmail { get; set; }

        /// <summary>
        /// Column COD_CLIENTE
        /// </summary>
        [DbColumn("COD_CLIENTE")]
        
        public String CodCliente { get; set; }

        /// <summary>
        /// Column COD_FILIAL
        /// </summary>
        [DbColumn("COD_FILIAL")]
        
        public String CodFilial { get; set; }

        /// <summary>
        /// Column PERC_ADM_QUEBRA
        /// </summary>
        [DbColumn("PERC_ADM_QUEBRA")]
        
        public Single? PercAdmQuebra { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column DIAS_VCTO_FATURA
        /// </summary>
        [DbColumn("DIAS_VCTO_FATURA")]
        
        public Int16? DiasVctoFatura { get; set; }

        /// <summary>
        /// Column COD_SIGEFER
        /// </summary>
        [DbColumn("COD_SIGEFER")]
        
        public String CodSigefer { get; set; }

        /// <summary>
        /// Column FLAG_RATEIO
        /// </summary>
        [DbColumn("FLAG_RATEIO")]
        
        public String FlagRateio { get; set; }

        /// <summary>
        /// Column FLAG_TRANSBORDO
        /// </summary>
        [DbColumn("FLAG_TRANSBORDO")]
        
        public String FlagTransbordo { get; set; }

        /// <summary>
        /// Column FLAG_VIGENTE
        /// </summary>
        [DbColumn("FLAG_VIGENTE")]
        
        public String FlagVigente { get; set; }

        /// <summary>
        /// Column LOG_CONTRATO_ID
        /// </summary>
        [DbColumn("LOG_CONTRATO_ID")]
        
        public Decimal? LogContratoId { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(LogPtrContrato value)
        {
            if (value == null)
                return false;
            return
				this.CodContrato == value.CodContrato &&
				this.DescContrato == value.DescContrato &&
				this.ProdutoContrato == value.ProdutoContrato &&
				this.ClienteOrigem == value.ClienteOrigem &&
				this.FilialOrigem == value.FilialOrigem &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.FluxoSigefer == value.FluxoSigefer &&
				this.MoedaContrato == value.MoedaContrato &&
				this.AliqIcms == value.AliqIcms &&
				this.PesoContratado == value.PesoContratado &&
				this.FlagAtivo == value.FlagAtivo &&
				this.FlagClassificacao == value.FlagClassificacao &&
				this.EndEmail == value.EndEmail &&
				this.CodCliente == value.CodCliente &&
				this.CodFilial == value.CodFilial &&
				this.PercAdmQuebra == value.PercAdmQuebra &&
				this.CodTerminal == value.CodTerminal &&
				this.DiasVctoFatura == value.DiasVctoFatura &&
				this.CodSigefer == value.CodSigefer &&
				this.FlagRateio == value.FlagRateio &&
				this.FlagTransbordo == value.FlagTransbordo &&
				this.FlagVigente == value.FlagVigente &&
				this.LogContratoId == value.LogContratoId &&
				this.Status == value.Status;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public LogPtrContrato CloneT()
        {
            LogPtrContrato value = new LogPtrContrato();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.CodContrato = this.CodContrato;
			value.DescContrato = this.DescContrato;
			value.ProdutoContrato = this.ProdutoContrato;
			value.ClienteOrigem = this.ClienteOrigem;
			value.FilialOrigem = this.FilialOrigem;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.FluxoSigefer = this.FluxoSigefer;
			value.MoedaContrato = this.MoedaContrato;
			value.AliqIcms = this.AliqIcms;
			value.PesoContratado = this.PesoContratado;
			value.FlagAtivo = this.FlagAtivo;
			value.FlagClassificacao = this.FlagClassificacao;
			value.EndEmail = this.EndEmail;
			value.CodCliente = this.CodCliente;
			value.CodFilial = this.CodFilial;
			value.PercAdmQuebra = this.PercAdmQuebra;
			value.CodTerminal = this.CodTerminal;
			value.DiasVctoFatura = this.DiasVctoFatura;
			value.CodSigefer = this.CodSigefer;
			value.FlagRateio = this.FlagRateio;
			value.FlagTransbordo = this.FlagTransbordo;
			value.FlagVigente = this.FlagVigente;
			value.LogContratoId = this.LogContratoId;
			value.Status = this.Status;

            return value;
        }

        #endregion Clone

        #region Create

        public static LogPtrContrato Create(String _CodContrato, String _DescContrato, String _ProdutoContrato, String _ClienteOrigem, String _FilialOrigem, String _ClienteDestino, String _FilialDestino, String _FluxoSigefer, String _MoedaContrato, Int16? _AliqIcms, Int64? _PesoContratado, String _FlagAtivo, String _FlagClassificacao, String _EndEmail, String _CodCliente, String _CodFilial, Single? _PercAdmQuebra, String _CodTerminal, Int16? _DiasVctoFatura, String _CodSigefer, String _FlagRateio, String _FlagTransbordo, String _FlagVigente, Decimal _LogContratoId, String _Status)
        {
            LogPtrContrato __value = new LogPtrContrato();

			__value.CodContrato = _CodContrato;
			__value.DescContrato = _DescContrato;
			__value.ProdutoContrato = _ProdutoContrato;
			__value.ClienteOrigem = _ClienteOrigem;
			__value.FilialOrigem = _FilialOrigem;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.FluxoSigefer = _FluxoSigefer;
			__value.MoedaContrato = _MoedaContrato;
			__value.AliqIcms = _AliqIcms;
			__value.PesoContratado = _PesoContratado;
			__value.FlagAtivo = _FlagAtivo;
			__value.FlagClassificacao = _FlagClassificacao;
			__value.EndEmail = _EndEmail;
			__value.CodCliente = _CodCliente;
			__value.CodFilial = _CodFilial;
			__value.PercAdmQuebra = _PercAdmQuebra;
			__value.CodTerminal = _CodTerminal;
			__value.DiasVctoFatura = _DiasVctoFatura;
			__value.CodSigefer = _CodSigefer;
			__value.FlagRateio = _FlagRateio;
			__value.FlagTransbordo = _FlagTransbordo;
			__value.FlagVigente = _FlagVigente;
			__value.LogContratoId = _LogContratoId;
			__value.Status = _Status;

            return __value;
        }

        #endregion Create

   }

}
