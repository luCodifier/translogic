// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VIEW_SALDO_POR_FLUXO_CIDADE", "", "")]
    [Serializable]
    [DataContract(Name = "ViewSaldoPorFluxoCidade", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class ViewSaldoPorFluxoCidade : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column FLUXO
        /// </summary>
        [DbColumn("FLUXO")]
        
        public String Fluxo { get; set; }

        /// <summary>
        /// Column ORI_CNPJ_SPT
        /// </summary>
        [DbColumn("ORI_CNPJ_SPT")]
        
        public String OriCnpjSpt { get; set; }

        /// <summary>
        /// Column PESO_DEC
        /// </summary>
        [DbColumn("PESO_DEC")]
        
        public Decimal? PesoDec { get; set; }

        /// <summary>
        /// Column PESO_DES
        /// </summary>
        [DbColumn("PESO_DES")]
        
        public Decimal? PesoDes { get; set; }

        /// <summary>
        /// Column PER_RET
        /// </summary>
        [DbColumn("PER_RET")]
        
        public Decimal? PerRet { get; set; }

        /// <summary>
        /// Column PESO_SEL
        /// </summary>
        [DbColumn("PESO_SEL")]
        
        public Decimal? PesoSel { get; set; }

        /// <summary>
        /// Column PESO_EXP
        /// </summary>
        [DbColumn("PESO_EXP")]
        
        public Decimal? PesoExp { get; set; }

        /// <summary>
        /// Column PESO_RET
        /// </summary>
        [DbColumn("PESO_RET")]
        
        public Decimal? PesoRet { get; set; }

        /// <summary>
        /// Column PESO_SALDO
        /// </summary>
        [DbColumn("PESO_SALDO")]
        
        public Decimal? PesoSaldo { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(ViewSaldoPorFluxoCidade value)
        {
            if (value == null)
                return false;
            return
				this.Contrato == value.Contrato &&
				this.Terminal == value.Terminal &&
				this.Fluxo == value.Fluxo &&
				this.OriCnpjSpt == value.OriCnpjSpt &&
				this.PesoDec == value.PesoDec &&
				this.PesoDes == value.PesoDes &&
				this.PerRet == value.PerRet &&
				this.PesoSel == value.PesoSel &&
				this.PesoExp == value.PesoExp &&
				this.PesoRet == value.PesoRet &&
				this.PesoSaldo == value.PesoSaldo;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public ViewSaldoPorFluxoCidade CloneT()
        {
            ViewSaldoPorFluxoCidade value = new ViewSaldoPorFluxoCidade();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Contrato = this.Contrato;
			value.Terminal = this.Terminal;
			value.Fluxo = this.Fluxo;
			value.OriCnpjSpt = this.OriCnpjSpt;
			value.PesoDec = this.PesoDec;
			value.PesoDes = this.PesoDes;
			value.PerRet = this.PerRet;
			value.PesoSel = this.PesoSel;
			value.PesoExp = this.PesoExp;
			value.PesoRet = this.PesoRet;
			value.PesoSaldo = this.PesoSaldo;

            return value;
        }

        #endregion Clone

        #region Create

        public static ViewSaldoPorFluxoCidade Create(String _Contrato, String _Terminal, String _Fluxo, String _OriCnpjSpt, Decimal? _PesoDec, Decimal? _PesoDes, Decimal? _PerRet, Decimal? _PesoSel, Decimal? _PesoExp, Decimal? _PesoRet, Decimal? _PesoSaldo)
        {
            ViewSaldoPorFluxoCidade __value = new ViewSaldoPorFluxoCidade();

			__value.Contrato = _Contrato;
			__value.Terminal = _Terminal;
			__value.Fluxo = _Fluxo;
			__value.OriCnpjSpt = _OriCnpjSpt;
			__value.PesoDec = _PesoDec;
			__value.PesoDes = _PesoDes;
			__value.PerRet = _PerRet;
			__value.PesoSel = _PesoSel;
			__value.PesoExp = _PesoExp;
			__value.PesoRet = _PesoRet;
			__value.PesoSaldo = _PesoSaldo;

            return __value;
        }

        #endregion Create

   }

}
