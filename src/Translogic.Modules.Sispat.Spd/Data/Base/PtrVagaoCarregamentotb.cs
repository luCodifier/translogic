// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_VAGAO_CARREGAMENTOTB", "", "")]
    [Serializable]
    [DataContract(Name = "PtrVagaoCarregamentotb", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrVagaoCarregamentotb : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        /// <summary>
        /// Column SITUACAO
        /// </summary>
        [DbColumn("SITUACAO")]
        
        public String Situacao { get; set; }

        /// <summary>
        /// Column TB_REF
        /// </summary>
        [DbColumn("TB_REF")]
        
        public Decimal? TbRef { get; set; }

        /// <summary>
        /// Column TB_MAX
        /// </summary>
        [DbColumn("TB_MAX")]
        
        public Decimal? TbMax { get; set; }

        /// <summary>
        /// Column TB_TOL
        /// </summary>
        [DbColumn("TB_TOL")]
        
        public Decimal? TbTol { get; set; }

        /// <summary>
        /// Column PERC_APROV
        /// </summary>
        [DbColumn("PERC_APROV")]
        
        public Decimal? PercAprov { get; set; }

        /// <summary>
        /// Column ST_TARA
        /// </summary>
        [DbColumn("ST_TARA")]
        
        public String StTara { get; set; }

        /// <summary>
        /// Column ST_BRUTO
        /// </summary>
        [DbColumn("ST_BRUTO")]
        
        public String StBruto { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrVagaoCarregamentotb value)
        {
            if (value == null)
                return false;
            return
				this.SqVagao == value.SqVagao &&
				this.Situacao == value.Situacao &&
				this.TbRef == value.TbRef &&
				this.TbMax == value.TbMax &&
				this.TbTol == value.TbTol &&
				this.PercAprov == value.PercAprov &&
				this.StTara == value.StTara &&
				this.StBruto == value.StBruto;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrVagaoCarregamentotb CloneT()
        {
            PtrVagaoCarregamentotb value = new PtrVagaoCarregamentotb();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqVagao = this.SqVagao;
			value.Situacao = this.Situacao;
			value.TbRef = this.TbRef;
			value.TbMax = this.TbMax;
			value.TbTol = this.TbTol;
			value.PercAprov = this.PercAprov;
			value.StTara = this.StTara;
			value.StBruto = this.StBruto;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrVagaoCarregamentotb Create(Int64? _SqVagao, String _Situacao, Decimal? _TbRef, Decimal? _TbMax, Decimal? _TbTol, Decimal? _PercAprov, String _StTara, String _StBruto)
        {
            PtrVagaoCarregamentotb __value = new PtrVagaoCarregamentotb();

			__value.SqVagao = _SqVagao;
			__value.Situacao = _Situacao;
			__value.TbRef = _TbRef;
			__value.TbMax = _TbMax;
			__value.TbTol = _TbTol;
			__value.PercAprov = _PercAprov;
			__value.StTara = _StTara;
			__value.StBruto = _StBruto;

            return __value;
        }

        #endregion Create

   }

}
