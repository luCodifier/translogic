// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_VEICULO", "", "")]
    [Serializable]
    [DataContract(Name = "PtrVeiculo", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrVeiculo : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column VEI_ID
        /// </summary>
        [DbColumn("VEI_ID")]
        
        public Decimal? VeiId { get; set; }

        /// <summary>
        /// Column VEI_TIMESTAMP
        /// </summary>
        [DbColumn("VEI_TIMESTAMP")]
        
        public DateTime? VeiTimestamp { get; set; }

        /// <summary>
        /// Column VEI_PLA
        /// </summary>
        [DbColumn("VEI_PLA")]
        
        public String VeiPla { get; set; }

        /// <summary>
        /// Column VEI_BLQ
        /// </summary>
        [DbColumn("VEI_BLQ")]
        
        public String VeiBlq { get; set; }

        /// <summary>
        /// Column PTV_ID
        /// </summary>
        [DbColumn("PTV_ID")]
        
        public Decimal? PtvId { get; set; }

        /// <summary>
        /// Column VEI_UPD_DAT
        /// </summary>
        [DbColumn("VEI_UPD_DAT")]
        
        public DateTime? VeiUpdDat { get; set; }

        /// <summary>
        /// Column VEI_VER
        /// </summary>
        [DbColumn("VEI_VER")]
        
        public Decimal? VeiVer { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrVeiculo value)
        {
            if (value == null)
                return false;
            return
				this.VeiId == value.VeiId &&
				this.VeiTimestamp == value.VeiTimestamp &&
				this.VeiPla == value.VeiPla &&
				this.VeiBlq == value.VeiBlq &&
				this.PtvId == value.PtvId &&
				this.VeiUpdDat == value.VeiUpdDat &&
				this.VeiVer == value.VeiVer;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrVeiculo CloneT()
        {
            PtrVeiculo value = new PtrVeiculo();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.VeiId = this.VeiId;
			value.VeiTimestamp = this.VeiTimestamp;
			value.VeiPla = this.VeiPla;
			value.VeiBlq = this.VeiBlq;
			value.PtvId = this.PtvId;
			value.VeiUpdDat = this.VeiUpdDat;
			value.VeiVer = this.VeiVer;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrVeiculo Create(Decimal _VeiId, DateTime _VeiTimestamp, String _VeiPla, String _VeiBlq, Decimal? _PtvId, DateTime? _VeiUpdDat, Decimal? _VeiVer)
        {
            PtrVeiculo __value = new PtrVeiculo();

			__value.VeiId = _VeiId;
			__value.VeiTimestamp = _VeiTimestamp;
			__value.VeiPla = _VeiPla;
			__value.VeiBlq = _VeiBlq;
			__value.PtvId = _PtvId;
			__value.VeiUpdDat = _VeiUpdDat;
			__value.VeiVer = _VeiVer;

            return __value;
        }

        #endregion Create

   }

}
