// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_ROLE_ROTINA", "", "")]
    [Serializable]
    [DataContract(Name = "PtrRoleRotina", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrRoleRotina : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ROLEID
        /// </summary>
        [DbColumn("ROLEID")]
        
        public Int64? Roleid { get; set; }

        /// <summary>
        /// Column SQ_ROTINA
        /// </summary>
        [DbColumn("SQ_ROTINA")]
        
        public Int64? SqRotina { get; set; }

        /// <summary>
        /// Column ACESSO
        /// </summary>
        [DbColumn("ACESSO")]
        
        public String Acesso { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrRoleRotina value)
        {
            if (value == null)
                return false;
            return
				this.Roleid == value.Roleid &&
				this.SqRotina == value.SqRotina &&
				this.Acesso == value.Acesso;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrRoleRotina CloneT()
        {
            PtrRoleRotina value = new PtrRoleRotina();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Roleid = this.Roleid;
			value.SqRotina = this.SqRotina;
			value.Acesso = this.Acesso;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrRoleRotina Create(Int64 _Roleid, Int64 _SqRotina, String _Acesso)
        {
            PtrRoleRotina __value = new PtrRoleRotina();

			__value.Roleid = _Roleid;
			__value.SqRotina = _SqRotina;
			__value.Acesso = _Acesso;

            return __value;
        }

        #endregion Create

   }

}
