// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_PCO_DESCARGA_DIARIA", "", "")]
    [Serializable]
    [DataContract(Name = "VwPcoDescargaDiaria", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwPcoDescargaDiaria : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column DATA
        /// </summary>
        [DbColumn("DATA")]
        
        public DateTime? Data { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column PRODUTO
        /// </summary>
        [DbColumn("PRODUTO")]
        
        public String Produto { get; set; }

        /// <summary>
        /// Column CLIENTE
        /// </summary>
        [DbColumn("CLIENTE")]
        
        public String Cliente { get; set; }

        /// <summary>
        /// Column HORA_INICIAL
        /// </summary>
        [DbColumn("HORA_INICIAL")]
        
        public String HoraInicial { get; set; }

        /// <summary>
        /// Column HORA_FINAL
        /// </summary>
        [DbColumn("HORA_FINAL")]
        
        public String HoraFinal { get; set; }

        /// <summary>
        /// Column QTD_VEIC_SAIDA
        /// </summary>
        [DbColumn("QTD_VEIC_SAIDA")]
        
        public Decimal? QtdVeicSaida { get; set; }

        /// <summary>
        /// Column PESO
        /// </summary>
        [DbColumn("PESO")]
        
        public Decimal? Peso { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwPcoDescargaDiaria value)
        {
            if (value == null)
                return false;
            return
				this.Data == value.Data &&
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.Produto == value.Produto &&
				this.Cliente == value.Cliente &&
				this.HoraInicial == value.HoraInicial &&
				this.HoraFinal == value.HoraFinal &&
				this.QtdVeicSaida == value.QtdVeicSaida &&
				this.Peso == value.Peso;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwPcoDescargaDiaria CloneT()
        {
            VwPcoDescargaDiaria value = new VwPcoDescargaDiaria();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Data = this.Data;
			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.Produto = this.Produto;
			value.Cliente = this.Cliente;
			value.HoraInicial = this.HoraInicial;
			value.HoraFinal = this.HoraFinal;
			value.QtdVeicSaida = this.QtdVeicSaida;
			value.Peso = this.Peso;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwPcoDescargaDiaria Create(DateTime? _Data, String _Terminal, String _Contrato, String _Produto, String _Cliente, String _HoraInicial, String _HoraFinal, Decimal? _QtdVeicSaida, Decimal? _Peso)
        {
            VwPcoDescargaDiaria __value = new VwPcoDescargaDiaria();

			__value.Data = _Data;
			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.Produto = _Produto;
			__value.Cliente = _Cliente;
			__value.HoraInicial = _HoraInicial;
			__value.HoraFinal = _HoraFinal;
			__value.QtdVeicSaida = _QtdVeicSaida;
			__value.Peso = _Peso;

            return __value;
        }

        #endregion Create

   }

}
