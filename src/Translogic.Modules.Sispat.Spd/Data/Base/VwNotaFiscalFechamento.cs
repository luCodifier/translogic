// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_NOTA_FISCAL_FECHAMENTO", "", "")]
    [Serializable]
    [DataContract(Name = "VwNotaFiscalFechamento", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwNotaFiscalFechamento : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column DESC_CONTRATO
        /// </summary>
        [DbColumn("DESC_CONTRATO")]
        
        public String DescContrato { get; set; }

        /// <summary>
        /// Column RAIZ_REMETENTE
        /// </summary>
        [DbColumn("RAIZ_REMETENTE")]
        
        public String RaizRemetente { get; set; }

        /// <summary>
        /// Column NOME_REMETENTE
        /// </summary>
        [DbColumn("NOME_REMETENTE")]
        
        public String NomeRemetente { get; set; }

        /// <summary>
        /// Column COD_TER_DESTINO
        /// </summary>
        [DbColumn("COD_TER_DESTINO")]
        
        public String CodTerDestino { get; set; }

        /// <summary>
        /// Column NOME_TER_DESTINO
        /// </summary>
        [DbColumn("NOME_TER_DESTINO")]
        
        public String NomeTerDestino { get; set; }

        /// <summary>
        /// Column COD_DESTINO
        /// </summary>
        [DbColumn("COD_DESTINO")]
        
        public String CodDestino { get; set; }

        /// <summary>
        /// Column DESC_DESTINO
        /// </summary>
        [DbColumn("DESC_DESTINO")]
        
        public String DescDestino { get; set; }

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Decimal? SqCaminhao { get; set; }

        /// <summary>
        /// Column VEICULO
        /// </summary>
        [DbColumn("VEICULO")]
        
        public String Veiculo { get; set; }

        /// <summary>
        /// Column DATA_MARCACAO
        /// </summary>
        [DbColumn("DATA_MARCACAO")]
        
        public DateTime? DataMarcacao { get; set; }

        /// <summary>
        /// Column DATA_ENTRADA
        /// </summary>
        [DbColumn("DATA_ENTRADA")]
        
        public DateTime? DataEntrada { get; set; }

        /// <summary>
        /// Column DATA_SAIDA
        /// </summary>
        [DbColumn("DATA_SAIDA")]
        
        public DateTime? DataSaida { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column NF_NUMERO
        /// </summary>
        [DbColumn("NF_NUMERO")]
        
        public String NfNumero { get; set; }

        /// <summary>
        /// Column NF_SERIE
        /// </summary>
        [DbColumn("NF_SERIE")]
        
        public String NfSerie { get; set; }

        /// <summary>
        /// Column NF_DATA
        /// </summary>
        [DbColumn("NF_DATA")]
        
        public DateTime? NfData { get; set; }

        /// <summary>
        /// Column NF_VALOR
        /// </summary>
        [DbColumn("NF_VALOR")]
        
        public Double? NfValor { get; set; }

        /// <summary>
        /// Column NF_CHAVE
        /// </summary>
        [DbColumn("NF_CHAVE")]
        
        public String NfChave { get; set; }

        /// <summary>
        /// Column NF_PES_DEC
        /// </summary>
        [DbColumn("NF_PES_DEC")]
        
        public Decimal? NfPesDec { get; set; }

        /// <summary>
        /// Column NF_PES_DES
        /// </summary>
        [DbColumn("NF_PES_DES")]
        
        public Decimal? NfPesDes { get; set; }

        /// <summary>
        /// Column NF_PES_SEL
        /// </summary>
        [DbColumn("NF_PES_SEL")]
        
        public Decimal? NfPesSel { get; set; }

        /// <summary>
        /// Column NF_PES_EXP
        /// </summary>
        [DbColumn("NF_PES_EXP")]
        
        public Decimal? NfPesExp { get; set; }

        /// <summary>
        /// Column NF_SALDO
        /// </summary>
        [DbColumn("NF_SALDO")]
        
        public Decimal? NfSaldo { get; set; }

        /// <summary>
        /// Column CLIENTE_ORIGEM
        /// </summary>
        [DbColumn("CLIENTE_ORIGEM")]
        
        public String ClienteOrigem { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column COD_CIDADE
        /// </summary>
        [DbColumn("COD_CIDADE")]
        
        public Int32? CodCidade { get; set; }

        /// <summary>
        /// Column NOME_CIDADE
        /// </summary>
        [DbColumn("NOME_CIDADE")]
        
        public String NomeCidade { get; set; }

        /// <summary>
        /// Column COD_PRACA
        /// </summary>
        [DbColumn("COD_PRACA")]
        
        public Int32? CodPraca { get; set; }

        /// <summary>
        /// Column NOME_PRACA
        /// </summary>
        [DbColumn("NOME_PRACA")]
        
        public String NomePraca { get; set; }

        /// <summary>
        /// Column FLUXO_ATUAL
        /// </summary>
        [DbColumn("FLUXO_ATUAL")]
        
        public String FluxoAtual { get; set; }

        /// <summary>
        /// Column VIGENCIA_FLUXO_ATUAL
        /// </summary>
        [DbColumn("VIGENCIA_FLUXO_ATUAL")]
        
        public DateTime? VigenciaFluxoAtual { get; set; }

        /// <summary>
        /// Column PRIMEIRA_VIGENCIA
        /// </summary>
        [DbColumn("PRIMEIRA_VIGENCIA")]
        
        public DateTime? PrimeiraVigencia { get; set; }

        /// <summary>
        /// Column FLUXO_VIGENTE
        /// </summary>
        [DbColumn("FLUXO_VIGENTE")]
        
        public String FluxoVigente { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwNotaFiscalFechamento value)
        {
            if (value == null)
                return false;
            return
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.DescContrato == value.DescContrato &&
				this.RaizRemetente == value.RaizRemetente &&
				this.NomeRemetente == value.NomeRemetente &&
				this.CodTerDestino == value.CodTerDestino &&
				this.NomeTerDestino == value.NomeTerDestino &&
				this.CodDestino == value.CodDestino &&
				this.DescDestino == value.DescDestino &&
				this.SqCaminhao == value.SqCaminhao &&
				this.Veiculo == value.Veiculo &&
				this.DataMarcacao == value.DataMarcacao &&
				this.DataEntrada == value.DataEntrada &&
				this.DataSaida == value.DataSaida &&
				this.SqNf == value.SqNf &&
				this.NfNumero == value.NfNumero &&
				this.NfSerie == value.NfSerie &&
				this.NfData == value.NfData &&
				this.NfValor == value.NfValor &&
				this.NfChave == value.NfChave &&
				this.NfPesDec == value.NfPesDec &&
				this.NfPesDes == value.NfPesDes &&
				this.NfPesSel == value.NfPesSel &&
				this.NfPesExp == value.NfPesExp &&
				this.NfSaldo == value.NfSaldo &&
				this.ClienteOrigem == value.ClienteOrigem &&
				this.ClienteDestino == value.ClienteDestino &&
				this.CodCidade == value.CodCidade &&
				this.NomeCidade == value.NomeCidade &&
				this.CodPraca == value.CodPraca &&
				this.NomePraca == value.NomePraca &&
				this.FluxoAtual == value.FluxoAtual &&
				this.VigenciaFluxoAtual == value.VigenciaFluxoAtual &&
				this.PrimeiraVigencia == value.PrimeiraVigencia &&
				this.FluxoVigente == value.FluxoVigente;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwNotaFiscalFechamento CloneT()
        {
            VwNotaFiscalFechamento value = new VwNotaFiscalFechamento();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.DescContrato = this.DescContrato;
			value.RaizRemetente = this.RaizRemetente;
			value.NomeRemetente = this.NomeRemetente;
			value.CodTerDestino = this.CodTerDestino;
			value.NomeTerDestino = this.NomeTerDestino;
			value.CodDestino = this.CodDestino;
			value.DescDestino = this.DescDestino;
			value.SqCaminhao = this.SqCaminhao;
			value.Veiculo = this.Veiculo;
			value.DataMarcacao = this.DataMarcacao;
			value.DataEntrada = this.DataEntrada;
			value.DataSaida = this.DataSaida;
			value.SqNf = this.SqNf;
			value.NfNumero = this.NfNumero;
			value.NfSerie = this.NfSerie;
			value.NfData = this.NfData;
			value.NfValor = this.NfValor;
			value.NfChave = this.NfChave;
			value.NfPesDec = this.NfPesDec;
			value.NfPesDes = this.NfPesDes;
			value.NfPesSel = this.NfPesSel;
			value.NfPesExp = this.NfPesExp;
			value.NfSaldo = this.NfSaldo;
			value.ClienteOrigem = this.ClienteOrigem;
			value.ClienteDestino = this.ClienteDestino;
			value.CodCidade = this.CodCidade;
			value.NomeCidade = this.NomeCidade;
			value.CodPraca = this.CodPraca;
			value.NomePraca = this.NomePraca;
			value.FluxoAtual = this.FluxoAtual;
			value.VigenciaFluxoAtual = this.VigenciaFluxoAtual;
			value.PrimeiraVigencia = this.PrimeiraVigencia;
			value.FluxoVigente = this.FluxoVigente;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwNotaFiscalFechamento Create(String _Terminal, String _Contrato, String _DescContrato, String _RaizRemetente, String _NomeRemetente, String _CodTerDestino, String _NomeTerDestino, String _CodDestino, String _DescDestino, Decimal? _SqCaminhao, String _Veiculo, DateTime _DataMarcacao, DateTime? _DataEntrada, DateTime? _DataSaida, Int64 _SqNf, String _NfNumero, String _NfSerie, DateTime? _NfData, Double? _NfValor, String _NfChave, Decimal? _NfPesDec, Decimal? _NfPesDes, Decimal? _NfPesSel, Decimal? _NfPesExp, Decimal? _NfSaldo, String _ClienteOrigem, String _ClienteDestino, Int32? _CodCidade, String _NomeCidade, Int32? _CodPraca, String _NomePraca, String _FluxoAtual, DateTime? _VigenciaFluxoAtual, DateTime? _PrimeiraVigencia, String _FluxoVigente)
        {
            VwNotaFiscalFechamento __value = new VwNotaFiscalFechamento();

			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.DescContrato = _DescContrato;
			__value.RaizRemetente = _RaizRemetente;
			__value.NomeRemetente = _NomeRemetente;
			__value.CodTerDestino = _CodTerDestino;
			__value.NomeTerDestino = _NomeTerDestino;
			__value.CodDestino = _CodDestino;
			__value.DescDestino = _DescDestino;
			__value.SqCaminhao = _SqCaminhao;
			__value.Veiculo = _Veiculo;
			__value.DataMarcacao = _DataMarcacao;
			__value.DataEntrada = _DataEntrada;
			__value.DataSaida = _DataSaida;
			__value.SqNf = _SqNf;
			__value.NfNumero = _NfNumero;
			__value.NfSerie = _NfSerie;
			__value.NfData = _NfData;
			__value.NfValor = _NfValor;
			__value.NfChave = _NfChave;
			__value.NfPesDec = _NfPesDec;
			__value.NfPesDes = _NfPesDes;
			__value.NfPesSel = _NfPesSel;
			__value.NfPesExp = _NfPesExp;
			__value.NfSaldo = _NfSaldo;
			__value.ClienteOrigem = _ClienteOrigem;
			__value.ClienteDestino = _ClienteDestino;
			__value.CodCidade = _CodCidade;
			__value.NomeCidade = _NomeCidade;
			__value.CodPraca = _CodPraca;
			__value.NomePraca = _NomePraca;
			__value.FluxoAtual = _FluxoAtual;
			__value.VigenciaFluxoAtual = _VigenciaFluxoAtual;
			__value.PrimeiraVigencia = _PrimeiraVigencia;
			__value.FluxoVigente = _FluxoVigente;

            return __value;
        }

        #endregion Create

   }

}
