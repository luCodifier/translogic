// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VIEW_PENDENCIA_EM_TRANSITO", "", "")]
    [Serializable]
    [DataContract(Name = "ViewPendenciaEmTransito", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class ViewPendenciaEmTransito : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column FLUXO
        /// </summary>
        [DbColumn("FLUXO")]
        
        public String Fluxo { get; set; }

        /// <summary>
        /// Column DATA_CADASTRO
        /// </summary>
        [DbColumn("DATA_CADASTRO")]
        
        public DateTime? DataCadastro { get; set; }

        /// <summary>
        /// Column AGE_ID
        /// </summary>
        [DbColumn("AGE_ID")]
        
        public Decimal? AgeId { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Decimal? SqCaminhao { get; set; }

        /// <summary>
        /// Column MOT_NOM
        /// </summary>
        [DbColumn("MOT_NOM")]
        
        public String MotNom { get; set; }

        /// <summary>
        /// Column MOT_CPF
        /// </summary>
        [DbColumn("MOT_CPF")]
        
        public String MotCpf { get; set; }

        /// <summary>
        /// Column DATA_DESCARGA_PREV
        /// </summary>
        [DbColumn("DATA_DESCARGA_PREV")]
        
        public DateTime? DataDescargaPrev { get; set; }

        /// <summary>
        /// Column EXCLUIDO
        /// </summary>
        [DbColumn("EXCLUIDO")]
        
        public String Excluido { get; set; }

        /// <summary>
        /// Column PLACA_TRACAO
        /// </summary>
        [DbColumn("PLACA_TRACAO")]
        
        public String PlacaTracao { get; set; }

        /// <summary>
        /// Column PLACA_CARRETAS
        /// </summary>
        [DbColumn("PLACA_CARRETAS")]
        
        public String PlacaCarretas { get; set; }

        /// <summary>
        /// Column DATA_CLASSIF
        /// </summary>
        [DbColumn("DATA_CLASSIF")]
        
        public DateTime? DataClassif { get; set; }

        /// <summary>
        /// Column JUSTIF_CLASSIF
        /// </summary>
        [DbColumn("JUSTIF_CLASSIF")]
        
        public String JustifClassif { get; set; }

        /// <summary>
        /// Column APROVADO_CLASSIF
        /// </summary>
        [DbColumn("APROVADO_CLASSIF")]
        
        public String AprovadoClassif { get; set; }

        /// <summary>
        /// Column DATA_MARCACAO
        /// </summary>
        [DbColumn("DATA_MARCACAO")]
        
        public DateTime? DataMarcacao { get; set; }

        /// <summary>
        /// Column DATA_ENTRADA
        /// </summary>
        [DbColumn("DATA_ENTRADA")]
        
        public DateTime? DataEntrada { get; set; }

        /// <summary>
        /// Column DATA_SAIDA
        /// </summary>
        [DbColumn("DATA_SAIDA")]
        
        public DateTime? DataSaida { get; set; }

        /// <summary>
        /// Column NF_NUMERO
        /// </summary>
        [DbColumn("NF_NUMERO")]
        
        public String NfNumero { get; set; }

        /// <summary>
        /// Column NF_SERIE
        /// </summary>
        [DbColumn("NF_SERIE")]
        
        public String NfSerie { get; set; }

        /// <summary>
        /// Column NF_DATA
        /// </summary>
        [DbColumn("NF_DATA")]
        
        public DateTime? NfData { get; set; }

        /// <summary>
        /// Column NF_VALOR
        /// </summary>
        [DbColumn("NF_VALOR")]
        
        public Double? NfValor { get; set; }

        /// <summary>
        /// Column NF_PES_DEC
        /// </summary>
        [DbColumn("NF_PES_DEC")]
        
        public Decimal? NfPesDec { get; set; }

        /// <summary>
        /// Column NF_PES_EXP
        /// </summary>
        [DbColumn("NF_PES_EXP")]
        
        public Decimal? NfPesExp { get; set; }

        /// <summary>
        /// Column NF_PES_SEL
        /// </summary>
        [DbColumn("NF_PES_SEL")]
        
        public Decimal? NfPesSel { get; set; }

        /// <summary>
        /// Column NF_CHAVE
        /// </summary>
        [DbColumn("NF_CHAVE")]
        
        public String NfChave { get; set; }

        /// <summary>
        /// Column NF_ARMAZENADO
        /// </summary>
        [DbColumn("NF_ARMAZENADO")]
        
        public String NfArmazenado { get; set; }

        /// <summary>
        /// Column ORI_CLI_NOME
        /// </summary>
        [DbColumn("ORI_CLI_NOME")]
        
        public String OriCliNome { get; set; }

        /// <summary>
        /// Column ORI_CLI
        /// </summary>
        [DbColumn("ORI_CLI")]
        
        public Int16? OriCli { get; set; }

        /// <summary>
        /// Column ORI_FIL
        /// </summary>
        [DbColumn("ORI_FIL")]
        
        public String OriFil { get; set; }

        /// <summary>
        /// Column ORI_CLI_NOM
        /// </summary>
        [DbColumn("ORI_CLI_NOM")]
        
        public String OriCliNom { get; set; }

        /// <summary>
        /// Column ORI_CLI_RAZ
        /// </summary>
        [DbColumn("ORI_CLI_RAZ")]
        
        public String OriCliRaz { get; set; }

        /// <summary>
        /// Column ORI_CNPJ_SPT
        /// </summary>
        [DbColumn("ORI_CNPJ_SPT")]
        
        public String OriCnpjSpt { get; set; }

        /// <summary>
        /// Column ORI_CNPJ_TL
        /// </summary>
        [DbColumn("ORI_CNPJ_TL")]
        
        public String OriCnpjTl { get; set; }

        /// <summary>
        /// Column DES_CLI_NOME
        /// </summary>
        [DbColumn("DES_CLI_NOME")]
        
        public String DesCliNome { get; set; }

        /// <summary>
        /// Column DES_CLI
        /// </summary>
        [DbColumn("DES_CLI")]
        
        public String DesCli { get; set; }

        /// <summary>
        /// Column DES_FIL
        /// </summary>
        [DbColumn("DES_FIL")]
        
        public String DesFil { get; set; }

        /// <summary>
        /// Column DES_CNPJ_SPT
        /// </summary>
        [DbColumn("DES_CNPJ_SPT")]
        
        public String DesCnpjSpt { get; set; }

        /// <summary>
        /// Column DES_CNPJ_TL
        /// </summary>
        [DbColumn("DES_CNPJ_TL")]
        
        public String DesCnpjTl { get; set; }

        /// <summary>
        /// Column CLI_RAT_PES_NF
        /// </summary>
        [DbColumn("CLI_RAT_PES_NF")]
        
        public String CliRatPesNf { get; set; }

        /// <summary>
        /// Column MERCADORIA
        /// </summary>
        [DbColumn("MERCADORIA")]
        
        public String Mercadoria { get; set; }

        /// <summary>
        /// Column PRODUTO
        /// </summary>
        [DbColumn("PRODUTO")]
        
        public String Produto { get; set; }

        /// <summary>
        /// Column DATA_VIGENCIA
        /// </summary>
        [DbColumn("DATA_VIGENCIA")]
        
        public DateTime? DataVigencia { get; set; }

        /// <summary>
        /// Column STS_CAMINHAO
        /// </summary>
        [DbColumn("STS_CAMINHAO")]
        
        public String StsCaminhao { get; set; }

        /// <summary>
        /// Column NUM_VAGAO
        /// </summary>
        [DbColumn("NUM_VAGAO")]
        
        public String NumVagao { get; set; }

        /// <summary>
        /// Column SER_VAGAO
        /// </summary>
        [DbColumn("SER_VAGAO")]
        
        public String SerVagao { get; set; }

        /// <summary>
        /// Column DIAS_EM_ATRASO
        /// </summary>
        [DbColumn("DIAS_EM_ATRASO")]
        
        public Decimal? DiasEmAtraso { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(ViewPendenciaEmTransito value)
        {
            if (value == null)
                return false;
            return
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.Fluxo == value.Fluxo &&
				this.DataCadastro == value.DataCadastro &&
				this.AgeId == value.AgeId &&
				this.SqNf == value.SqNf &&
				this.SqCaminhao == value.SqCaminhao &&
				this.MotNom == value.MotNom &&
				this.MotCpf == value.MotCpf &&
				this.DataDescargaPrev == value.DataDescargaPrev &&
				this.Excluido == value.Excluido &&
				this.PlacaTracao == value.PlacaTracao &&
				this.PlacaCarretas == value.PlacaCarretas &&
				this.DataClassif == value.DataClassif &&
				this.JustifClassif == value.JustifClassif &&
				this.AprovadoClassif == value.AprovadoClassif &&
				this.DataMarcacao == value.DataMarcacao &&
				this.DataEntrada == value.DataEntrada &&
				this.DataSaida == value.DataSaida &&
				this.NfNumero == value.NfNumero &&
				this.NfSerie == value.NfSerie &&
				this.NfData == value.NfData &&
				this.NfValor == value.NfValor &&
				this.NfPesDec == value.NfPesDec &&
				this.NfPesExp == value.NfPesExp &&
				this.NfPesSel == value.NfPesSel &&
				this.NfChave == value.NfChave &&
				this.NfArmazenado == value.NfArmazenado &&
				this.OriCliNome == value.OriCliNome &&
				this.OriCli == value.OriCli &&
				this.OriFil == value.OriFil &&
				this.OriCliNom == value.OriCliNom &&
				this.OriCliRaz == value.OriCliRaz &&
				this.OriCnpjSpt == value.OriCnpjSpt &&
				this.OriCnpjTl == value.OriCnpjTl &&
				this.DesCliNome == value.DesCliNome &&
				this.DesCli == value.DesCli &&
				this.DesFil == value.DesFil &&
				this.DesCnpjSpt == value.DesCnpjSpt &&
				this.DesCnpjTl == value.DesCnpjTl &&
				this.CliRatPesNf == value.CliRatPesNf &&
				this.Mercadoria == value.Mercadoria &&
				this.Produto == value.Produto &&
				this.DataVigencia == value.DataVigencia &&
				this.StsCaminhao == value.StsCaminhao &&
				this.NumVagao == value.NumVagao &&
				this.SerVagao == value.SerVagao &&
				this.DiasEmAtraso == value.DiasEmAtraso;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public ViewPendenciaEmTransito CloneT()
        {
            ViewPendenciaEmTransito value = new ViewPendenciaEmTransito();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.Fluxo = this.Fluxo;
			value.DataCadastro = this.DataCadastro;
			value.AgeId = this.AgeId;
			value.SqNf = this.SqNf;
			value.SqCaminhao = this.SqCaminhao;
			value.MotNom = this.MotNom;
			value.MotCpf = this.MotCpf;
			value.DataDescargaPrev = this.DataDescargaPrev;
			value.Excluido = this.Excluido;
			value.PlacaTracao = this.PlacaTracao;
			value.PlacaCarretas = this.PlacaCarretas;
			value.DataClassif = this.DataClassif;
			value.JustifClassif = this.JustifClassif;
			value.AprovadoClassif = this.AprovadoClassif;
			value.DataMarcacao = this.DataMarcacao;
			value.DataEntrada = this.DataEntrada;
			value.DataSaida = this.DataSaida;
			value.NfNumero = this.NfNumero;
			value.NfSerie = this.NfSerie;
			value.NfData = this.NfData;
			value.NfValor = this.NfValor;
			value.NfPesDec = this.NfPesDec;
			value.NfPesExp = this.NfPesExp;
			value.NfPesSel = this.NfPesSel;
			value.NfChave = this.NfChave;
			value.NfArmazenado = this.NfArmazenado;
			value.OriCliNome = this.OriCliNome;
			value.OriCli = this.OriCli;
			value.OriFil = this.OriFil;
			value.OriCliNom = this.OriCliNom;
			value.OriCliRaz = this.OriCliRaz;
			value.OriCnpjSpt = this.OriCnpjSpt;
			value.OriCnpjTl = this.OriCnpjTl;
			value.DesCliNome = this.DesCliNome;
			value.DesCli = this.DesCli;
			value.DesFil = this.DesFil;
			value.DesCnpjSpt = this.DesCnpjSpt;
			value.DesCnpjTl = this.DesCnpjTl;
			value.CliRatPesNf = this.CliRatPesNf;
			value.Mercadoria = this.Mercadoria;
			value.Produto = this.Produto;
			value.DataVigencia = this.DataVigencia;
			value.StsCaminhao = this.StsCaminhao;
			value.NumVagao = this.NumVagao;
			value.SerVagao = this.SerVagao;
			value.DiasEmAtraso = this.DiasEmAtraso;

            return value;
        }

        #endregion Clone

        #region Create

        public static ViewPendenciaEmTransito Create(String _Terminal, String _Contrato, String _Fluxo, DateTime _DataCadastro, Decimal _AgeId, Int64 _SqNf, Decimal? _SqCaminhao, String _MotNom, String _MotCpf, DateTime _DataDescargaPrev, String _Excluido, String _PlacaTracao, String _PlacaCarretas, DateTime? _DataClassif, String _JustifClassif, String _AprovadoClassif, DateTime _DataMarcacao, DateTime? _DataEntrada, DateTime? _DataSaida, String _NfNumero, String _NfSerie, DateTime? _NfData, Double? _NfValor, Decimal? _NfPesDec, Decimal? _NfPesExp, Decimal? _NfPesSel, String _NfChave, String _NfArmazenado, String _OriCliNome, Int16? _OriCli, String _OriFil, String _OriCliNom, String _OriCliRaz, String _OriCnpjSpt, String _OriCnpjTl, String _DesCliNome, String _DesCli, String _DesFil, String _DesCnpjSpt, String _DesCnpjTl, String _CliRatPesNf, String _Mercadoria, String _Produto, DateTime? _DataVigencia, String _StsCaminhao, String _NumVagao, String _SerVagao, Decimal? _DiasEmAtraso)
        {
            ViewPendenciaEmTransito __value = new ViewPendenciaEmTransito();

			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.Fluxo = _Fluxo;
			__value.DataCadastro = _DataCadastro;
			__value.AgeId = _AgeId;
			__value.SqNf = _SqNf;
			__value.SqCaminhao = _SqCaminhao;
			__value.MotNom = _MotNom;
			__value.MotCpf = _MotCpf;
			__value.DataDescargaPrev = _DataDescargaPrev;
			__value.Excluido = _Excluido;
			__value.PlacaTracao = _PlacaTracao;
			__value.PlacaCarretas = _PlacaCarretas;
			__value.DataClassif = _DataClassif;
			__value.JustifClassif = _JustifClassif;
			__value.AprovadoClassif = _AprovadoClassif;
			__value.DataMarcacao = _DataMarcacao;
			__value.DataEntrada = _DataEntrada;
			__value.DataSaida = _DataSaida;
			__value.NfNumero = _NfNumero;
			__value.NfSerie = _NfSerie;
			__value.NfData = _NfData;
			__value.NfValor = _NfValor;
			__value.NfPesDec = _NfPesDec;
			__value.NfPesExp = _NfPesExp;
			__value.NfPesSel = _NfPesSel;
			__value.NfChave = _NfChave;
			__value.NfArmazenado = _NfArmazenado;
			__value.OriCliNome = _OriCliNome;
			__value.OriCli = _OriCli;
			__value.OriFil = _OriFil;
			__value.OriCliNom = _OriCliNom;
			__value.OriCliRaz = _OriCliRaz;
			__value.OriCnpjSpt = _OriCnpjSpt;
			__value.OriCnpjTl = _OriCnpjTl;
			__value.DesCliNome = _DesCliNome;
			__value.DesCli = _DesCli;
			__value.DesFil = _DesFil;
			__value.DesCnpjSpt = _DesCnpjSpt;
			__value.DesCnpjTl = _DesCnpjTl;
			__value.CliRatPesNf = _CliRatPesNf;
			__value.Mercadoria = _Mercadoria;
			__value.Produto = _Produto;
			__value.DataVigencia = _DataVigencia;
			__value.StsCaminhao = _StsCaminhao;
			__value.NumVagao = _NumVagao;
			__value.SerVagao = _SerVagao;
			__value.DiasEmAtraso = _DiasEmAtraso;

            return __value;
        }

        #endregion Create

   }

}
