// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_EVENTOS_ANORMALIDADE", "", "")]
    [Serializable]
    [DataContract(Name = "PtrEventosAnormalidade", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrEventosAnormalidade : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_ANORMALIDADE
        /// </summary>
        [DbColumn("SQ_ANORMALIDADE")]
        
        public Int64? SqAnormalidade { get; set; }

        /// <summary>
        /// Column COD_EVENTO
        /// </summary>
        [DbColumn("COD_EVENTO")]
        
        public String CodEvento { get; set; }

        /// <summary>
        /// Column DATAHORA_EVENTO_ANORMAL
        /// </summary>
        [DbColumn("DATAHORA_EVENTO_ANORMAL")]
        
        public DateTime? DatahoraEventoAnormal { get; set; }

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrEventosAnormalidade value)
        {
            if (value == null)
                return false;
            return
				this.SqAnormalidade == value.SqAnormalidade &&
				this.CodEvento == value.CodEvento &&
				this.DatahoraEventoAnormal == value.DatahoraEventoAnormal &&
				this.SqVagao == value.SqVagao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrEventosAnormalidade CloneT()
        {
            PtrEventosAnormalidade value = new PtrEventosAnormalidade();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqAnormalidade = this.SqAnormalidade;
			value.CodEvento = this.CodEvento;
			value.DatahoraEventoAnormal = this.DatahoraEventoAnormal;
			value.SqVagao = this.SqVagao;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrEventosAnormalidade Create(Int64 _SqAnormalidade, String _CodEvento, DateTime _DatahoraEventoAnormal, Int64? _SqVagao)
        {
            PtrEventosAnormalidade __value = new PtrEventosAnormalidade();

			__value.SqAnormalidade = _SqAnormalidade;
			__value.CodEvento = _CodEvento;
			__value.DatahoraEventoAnormal = _DatahoraEventoAnormal;
			__value.SqVagao = _SqVagao;

            return __value;
        }

        #endregion Create

   }

}
