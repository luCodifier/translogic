// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CONTRATO_FILIAIS_ALL", "", "")]
    [Serializable]
    [DataContract(Name = "PtrContratoFiliaisAll", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrContratoFiliaisAll : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column PRODUTO_CONTRATO
        /// </summary>
        [DbColumn("PRODUTO_CONTRATO")]
        
        public String ProdutoContrato { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column CLI_PAGADO
        /// </summary>
        [DbColumn("CLI_PAGADO")]
        
        public String CliPagado { get; set; }

        /// <summary>
        /// Column FIL_PAGADO
        /// </summary>
        [DbColumn("FIL_PAGADO")]
        
        public String FilPagado { get; set; }

        /// <summary>
        /// Column RAZAO_PAGADORA
        /// </summary>
        [DbColumn("RAZAO_PAGADORA")]
        
        public String RazaoPagadora { get; set; }

        /// <summary>
        /// Column CNPJ_PAGADORA
        /// </summary>
        [DbColumn("CNPJ_PAGADORA")]
        
        public Int64? CnpjPagadora { get; set; }

        /// <summary>
        /// Column CLI_ORIGEM
        /// </summary>
        [DbColumn("CLI_ORIGEM")]
        
        public String CliOrigem { get; set; }

        /// <summary>
        /// Column FIL_ORIGEM
        /// </summary>
        [DbColumn("FIL_ORIGEM")]
        
        public String FilOrigem { get; set; }

        /// <summary>
        /// Column CLI_DESTINO
        /// </summary>
        [DbColumn("CLI_DESTINO")]
        
        public String CliDestino { get; set; }

        /// <summary>
        /// Column FIL_DESTINO
        /// </summary>
        [DbColumn("FIL_DESTINO")]
        
        public String FilDestino { get; set; }

        /// <summary>
        /// Column CNPJ_ORIGEM
        /// </summary>
        [DbColumn("CNPJ_ORIGEM")]
        
        public String CnpjOrigem { get; set; }

        /// <summary>
        /// Column RAZAO_ORIGEM
        /// </summary>
        [DbColumn("RAZAO_ORIGEM")]
        
        public String RazaoOrigem { get; set; }

        /// <summary>
        /// Column CNPJ_DESTINO
        /// </summary>
        [DbColumn("CNPJ_DESTINO")]
        
        public Int64? CnpjDestino { get; set; }

        /// <summary>
        /// Column RAZAO_DESTINO
        /// </summary>
        [DbColumn("RAZAO_DESTINO")]
        
        public String RazaoDestino { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrContratoFiliaisAll value)
        {
            if (value == null)
                return false;
            return
				this.CodContrato == value.CodContrato &&
				this.ProdutoContrato == value.ProdutoContrato &&
				this.CodTerminal == value.CodTerminal &&
				this.CliPagado == value.CliPagado &&
				this.FilPagado == value.FilPagado &&
				this.RazaoPagadora == value.RazaoPagadora &&
				this.CnpjPagadora == value.CnpjPagadora &&
				this.CliOrigem == value.CliOrigem &&
				this.FilOrigem == value.FilOrigem &&
				this.CliDestino == value.CliDestino &&
				this.FilDestino == value.FilDestino &&
				this.CnpjOrigem == value.CnpjOrigem &&
				this.RazaoOrigem == value.RazaoOrigem &&
				this.CnpjDestino == value.CnpjDestino &&
				this.RazaoDestino == value.RazaoDestino;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrContratoFiliaisAll CloneT()
        {
            PtrContratoFiliaisAll value = new PtrContratoFiliaisAll();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.CodContrato = this.CodContrato;
			value.ProdutoContrato = this.ProdutoContrato;
			value.CodTerminal = this.CodTerminal;
			value.CliPagado = this.CliPagado;
			value.FilPagado = this.FilPagado;
			value.RazaoPagadora = this.RazaoPagadora;
			value.CnpjPagadora = this.CnpjPagadora;
			value.CliOrigem = this.CliOrigem;
			value.FilOrigem = this.FilOrigem;
			value.CliDestino = this.CliDestino;
			value.FilDestino = this.FilDestino;
			value.CnpjOrigem = this.CnpjOrigem;
			value.RazaoOrigem = this.RazaoOrigem;
			value.CnpjDestino = this.CnpjDestino;
			value.RazaoDestino = this.RazaoDestino;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrContratoFiliaisAll Create(String _CodContrato, String _ProdutoContrato, String _CodTerminal, String _CliPagado, String _FilPagado, String _RazaoPagadora, Int64? _CnpjPagadora, String _CliOrigem, String _FilOrigem, String _CliDestino, String _FilDestino, String _CnpjOrigem, String _RazaoOrigem, Int64? _CnpjDestino, String _RazaoDestino)
        {
            PtrContratoFiliaisAll __value = new PtrContratoFiliaisAll();

			__value.CodContrato = _CodContrato;
			__value.ProdutoContrato = _ProdutoContrato;
			__value.CodTerminal = _CodTerminal;
			__value.CliPagado = _CliPagado;
			__value.FilPagado = _FilPagado;
			__value.RazaoPagadora = _RazaoPagadora;
			__value.CnpjPagadora = _CnpjPagadora;
			__value.CliOrigem = _CliOrigem;
			__value.FilOrigem = _FilOrigem;
			__value.CliDestino = _CliDestino;
			__value.FilDestino = _FilDestino;
			__value.CnpjOrigem = _CnpjOrigem;
			__value.RazaoOrigem = _RazaoOrigem;
			__value.CnpjDestino = _CnpjDestino;
			__value.RazaoDestino = _RazaoDestino;

            return __value;
        }

        #endregion Create

   }

}
