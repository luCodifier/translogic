// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_VERSAO_SISPAT", "", "")]
    [Serializable]
    [DataContract(Name = "PtrVersaoSispat", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrVersaoSispat : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column VERSAO
        /// </summary>
        [DbColumn("VERSAO")]
        
        public String Versao { get; set; }

        /// <summary>
        /// Column DATA_DEPLOY
        /// </summary>
        [DbColumn("DATA_DEPLOY")]
        
        public DateTime? DataDeploy { get; set; }

        /// <summary>
        /// Column DETALHES
        /// </summary>
        [DbColumn("DETALHES")]
        
        public String Detalhes { get; set; }

        /// <summary>
        /// Column RESPONSAVEL
        /// </summary>
        [DbColumn("RESPONSAVEL")]
        
        public String Responsavel { get; set; }

        /// <summary>
        /// Column FLAG_DEPLOY
        /// </summary>
        [DbColumn("FLAG_DEPLOY")]
        
        public String FlagDeploy { get; set; }

        /// <summary>
        /// Column DETALHES_USER
        /// </summary>
        [DbColumn("DETALHES_USER")]
        
        public String DetalhesUser { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrVersaoSispat value)
        {
            if (value == null)
                return false;
            return
				this.Versao == value.Versao &&
				this.DataDeploy == value.DataDeploy &&
				this.Detalhes == value.Detalhes &&
				this.Responsavel == value.Responsavel &&
				this.FlagDeploy == value.FlagDeploy &&
				this.DetalhesUser == value.DetalhesUser;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrVersaoSispat CloneT()
        {
            PtrVersaoSispat value = new PtrVersaoSispat();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Versao = this.Versao;
			value.DataDeploy = this.DataDeploy;
			value.Detalhes = this.Detalhes;
			value.Responsavel = this.Responsavel;
			value.FlagDeploy = this.FlagDeploy;
			value.DetalhesUser = this.DetalhesUser;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrVersaoSispat Create(String _Versao, DateTime? _DataDeploy, String _Detalhes, String _Responsavel, String _FlagDeploy, String _DetalhesUser)
        {
            PtrVersaoSispat __value = new PtrVersaoSispat();

			__value.Versao = _Versao;
			__value.DataDeploy = _DataDeploy;
			__value.Detalhes = _Detalhes;
			__value.Responsavel = _Responsavel;
			__value.FlagDeploy = _FlagDeploy;
			__value.DetalhesUser = _DetalhesUser;

            return __value;
        }

        #endregion Create

   }

}
