// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_OPERACAO_CAMINHAO_CARGA", "", "")]
    [Serializable]
    [DataContract(Name = "PtrOperacaoCaminhaoCarga", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrOperacaoCaminhaoCarga : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_CAMINHAO
        /// </summary>
        [DbColumn("SQ_CAMINHAO")]
        
        public Decimal? SqCaminhao { get; set; }

        /// <summary>
        /// Column NUM_PLACA
        /// </summary>
        [DbColumn("NUM_PLACA")]
        
        public String NumPlaca { get; set; }

        /// <summary>
        /// Column DATA_HORA_EVENTO_INICIAL
        /// </summary>
        [DbColumn("DATA_HORA_EVENTO_INICIAL")]
        
        public DateTime? DataHoraEventoInicial { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column NUM_LOTE
        /// </summary>
        [DbColumn("NUM_LOTE")]
        
        public Int16? NumLote { get; set; }

        /// <summary>
        /// Column COD_LOCAL_ARMAZENAGEM
        /// </summary>
        [DbColumn("COD_LOCAL_ARMAZENAGEM")]
        
        public String CodLocalArmazenagem { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column LOCAL_DESCARGA
        /// </summary>
        [DbColumn("LOCAL_DESCARGA")]
        
        public String LocalDescarga { get; set; }

        /// <summary>
        /// Column COD_FILA
        /// </summary>
        [DbColumn("COD_FILA")]
        
        public String CodFila { get; set; }

        /// <summary>
        /// Column DATA_HORA_PES_CAR
        /// </summary>
        [DbColumn("DATA_HORA_PES_CAR")]
        
        public DateTime? DataHoraPesCar { get; set; }

        /// <summary>
        /// Column PESO_CARREGADO
        /// </summary>
        [DbColumn("PESO_CARREGADO")]
        
        public Int64? PesoCarregado { get; set; }

        /// <summary>
        /// Column DATA_HORA_PES_VAZ
        /// </summary>
        [DbColumn("DATA_HORA_PES_VAZ")]
        
        public DateTime? DataHoraPesVaz { get; set; }

        /// <summary>
        /// Column PESO_VAZIO
        /// </summary>
        [DbColumn("PESO_VAZIO")]
        
        public Int64? PesoVazio { get; set; }

        /// <summary>
        /// Column NOME_MOTORISTA
        /// </summary>
        [DbColumn("NOME_MOTORISTA")]
        
        public String NomeMotorista { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        /// <summary>
        /// Column CLIENTE_ORIGEM
        /// </summary>
        [DbColumn("CLIENTE_ORIGEM")]
        
        public String ClienteOrigem { get; set; }

        /// <summary>
        /// Column FILIAL_ORIGEM
        /// </summary>
        [DbColumn("FILIAL_ORIGEM")]
        
        public String FilialOrigem { get; set; }

        /// <summary>
        /// Column CLIENTE_DESTINO
        /// </summary>
        [DbColumn("CLIENTE_DESTINO")]
        
        public String ClienteDestino { get; set; }

        /// <summary>
        /// Column FILIAL_DESTINO
        /// </summary>
        [DbColumn("FILIAL_DESTINO")]
        
        public String FilialDestino { get; set; }

        /// <summary>
        /// Column VALOR_FRETE
        /// </summary>
        [DbColumn("VALOR_FRETE")]
        
        public Double? ValorFrete { get; set; }

        /// <summary>
        /// Column FLAG
        /// </summary>
        [DbColumn("FLAG")]
        
        public String Flag { get; set; }

        /// <summary>
        /// Column MSG
        /// </summary>
        [DbColumn("MSG")]
        
        public String Msg { get; set; }

        /// <summary>
        /// Column TIPO_CARGA
        /// </summary>
        [DbColumn("TIPO_CARGA")]
        
        public String TipoCarga { get; set; }

        /// <summary>
        /// Column COD_TRANSPORTADORA
        /// </summary>
        [DbColumn("COD_TRANSPORTADORA")]
        
        public Decimal? CodTransportadora { get; set; }

        /// <summary>
        /// Column COD_TAG
        /// </summary>
        [DbColumn("COD_TAG")]
        
        public String CodTag { get; set; }

        /// <summary>
        /// Column OCC_NF_SAP
        /// </summary>
        [DbColumn("OCC_NF_SAP")]
        
        public String OccNfSap { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrOperacaoCaminhaoCarga value)
        {
            if (value == null)
                return false;
            return
				this.SqCaminhao == value.SqCaminhao &&
				this.NumPlaca == value.NumPlaca &&
				this.DataHoraEventoInicial == value.DataHoraEventoInicial &&
				this.CodTerminal == value.CodTerminal &&
				this.NumLote == value.NumLote &&
				this.CodLocalArmazenagem == value.CodLocalArmazenagem &&
				this.CodContrato == value.CodContrato &&
				this.LocalDescarga == value.LocalDescarga &&
				this.CodFila == value.CodFila &&
				this.DataHoraPesCar == value.DataHoraPesCar &&
				this.PesoCarregado == value.PesoCarregado &&
				this.DataHoraPesVaz == value.DataHoraPesVaz &&
				this.PesoVazio == value.PesoVazio &&
				this.NomeMotorista == value.NomeMotorista &&
				this.Status == value.Status &&
				this.ClienteOrigem == value.ClienteOrigem &&
				this.FilialOrigem == value.FilialOrigem &&
				this.ClienteDestino == value.ClienteDestino &&
				this.FilialDestino == value.FilialDestino &&
				this.ValorFrete == value.ValorFrete &&
				this.Flag == value.Flag &&
				this.Msg == value.Msg &&
				this.TipoCarga == value.TipoCarga &&
				this.CodTransportadora == value.CodTransportadora &&
				this.CodTag == value.CodTag &&
				this.OccNfSap == value.OccNfSap;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrOperacaoCaminhaoCarga CloneT()
        {
            PtrOperacaoCaminhaoCarga value = new PtrOperacaoCaminhaoCarga();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqCaminhao = this.SqCaminhao;
			value.NumPlaca = this.NumPlaca;
			value.DataHoraEventoInicial = this.DataHoraEventoInicial;
			value.CodTerminal = this.CodTerminal;
			value.NumLote = this.NumLote;
			value.CodLocalArmazenagem = this.CodLocalArmazenagem;
			value.CodContrato = this.CodContrato;
			value.LocalDescarga = this.LocalDescarga;
			value.CodFila = this.CodFila;
			value.DataHoraPesCar = this.DataHoraPesCar;
			value.PesoCarregado = this.PesoCarregado;
			value.DataHoraPesVaz = this.DataHoraPesVaz;
			value.PesoVazio = this.PesoVazio;
			value.NomeMotorista = this.NomeMotorista;
			value.Status = this.Status;
			value.ClienteOrigem = this.ClienteOrigem;
			value.FilialOrigem = this.FilialOrigem;
			value.ClienteDestino = this.ClienteDestino;
			value.FilialDestino = this.FilialDestino;
			value.ValorFrete = this.ValorFrete;
			value.Flag = this.Flag;
			value.Msg = this.Msg;
			value.TipoCarga = this.TipoCarga;
			value.CodTransportadora = this.CodTransportadora;
			value.CodTag = this.CodTag;
			value.OccNfSap = this.OccNfSap;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrOperacaoCaminhaoCarga Create(Decimal _SqCaminhao, String _NumPlaca, DateTime _DataHoraEventoInicial, String _CodTerminal, Int16 _NumLote, String _CodLocalArmazenagem, String _CodContrato, String _LocalDescarga, String _CodFila, DateTime? _DataHoraPesCar, Int64? _PesoCarregado, DateTime? _DataHoraPesVaz, Int64? _PesoVazio, String _NomeMotorista, String _Status, String _ClienteOrigem, String _FilialOrigem, String _ClienteDestino, String _FilialDestino, Double? _ValorFrete, String _Flag, String _Msg, String _TipoCarga, Decimal? _CodTransportadora, String _CodTag, String _OccNfSap)
        {
            PtrOperacaoCaminhaoCarga __value = new PtrOperacaoCaminhaoCarga();

			__value.SqCaminhao = _SqCaminhao;
			__value.NumPlaca = _NumPlaca;
			__value.DataHoraEventoInicial = _DataHoraEventoInicial;
			__value.CodTerminal = _CodTerminal;
			__value.NumLote = _NumLote;
			__value.CodLocalArmazenagem = _CodLocalArmazenagem;
			__value.CodContrato = _CodContrato;
			__value.LocalDescarga = _LocalDescarga;
			__value.CodFila = _CodFila;
			__value.DataHoraPesCar = _DataHoraPesCar;
			__value.PesoCarregado = _PesoCarregado;
			__value.DataHoraPesVaz = _DataHoraPesVaz;
			__value.PesoVazio = _PesoVazio;
			__value.NomeMotorista = _NomeMotorista;
			__value.Status = _Status;
			__value.ClienteOrigem = _ClienteOrigem;
			__value.FilialOrigem = _FilialOrigem;
			__value.ClienteDestino = _ClienteDestino;
			__value.FilialDestino = _FilialDestino;
			__value.ValorFrete = _ValorFrete;
			__value.Flag = _Flag;
			__value.Msg = _Msg;
			__value.TipoCarga = _TipoCarga;
			__value.CodTransportadora = _CodTransportadora;
			__value.CodTag = _CodTag;
			__value.OccNfSap = _OccNfSap;

            return __value;
        }

        #endregion Create

   }

}
