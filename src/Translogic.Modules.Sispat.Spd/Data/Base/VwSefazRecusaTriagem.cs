// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_SEFAZ_RECUSA_TRIAGEM", "", "")]
    [Serializable]
    [DataContract(Name = "VwSefazRecusaTriagem", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwSefazRecusaTriagem : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ID
        /// </summary>
        [DbColumn("ID")]
        
        public Decimal? Id { get; set; }

        /// <summary>
        /// Column DATACADASTRO
        /// </summary>
        [DbColumn("DATACADASTRO")]
        
        public DateTime? Datacadastro { get; set; }

        /// <summary>
        /// Column CLIENTE
        /// </summary>
        [DbColumn("CLIENTE")]
        
        public String Cliente { get; set; }

        /// <summary>
        /// Column PRODUTO
        /// </summary>
        [DbColumn("PRODUTO")]
        
        public String Produto { get; set; }

        /// <summary>
        /// Column DATAPREVISAODESCARGA
        /// </summary>
        [DbColumn("DATAPREVISAODESCARGA")]
        
        public DateTime? Dataprevisaodescarga { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CTE
        /// </summary>
        [DbColumn("CTE")]
        
        public String Cte { get; set; }

        /// <summary>
        /// Column MDFE
        /// </summary>
        [DbColumn("MDFE")]
        
        public String Mdfe { get; set; }

        /// <summary>
        /// Column VEICULO
        /// </summary>
        [DbColumn("VEICULO")]
        
        public String Veiculo { get; set; }

        /// <summary>
        /// Column CPF
        /// </summary>
        [DbColumn("CPF")]
        
        public String Cpf { get; set; }

        /// <summary>
        /// Column MOTORISTA
        /// </summary>
        [DbColumn("MOTORISTA")]
        
        public String Motorista { get; set; }

        /// <summary>
        /// Column DATALANCAMENTO
        /// </summary>
        [DbColumn("DATALANCAMENTO")]
        
        public DateTime? Datalancamento { get; set; }

        /// <summary>
        /// Column USUARIO
        /// </summary>
        [DbColumn("USUARIO")]
        
        public String Usuario { get; set; }

        /// <summary>
        /// Column APROVADO
        /// </summary>
        [DbColumn("APROVADO")]
        
        public String Aprovado { get; set; }

        /// <summary>
        /// Column IMPUREZA
        /// </summary>
        [DbColumn("IMPUREZA")]
        
        public Decimal? Impureza { get; set; }

        /// <summary>
        /// Column UMIDADE
        /// </summary>
        [DbColumn("UMIDADE")]
        
        public Decimal? Umidade { get; set; }

        /// <summary>
        /// Column AVARIADO
        /// </summary>
        [DbColumn("AVARIADO")]
        
        public Decimal? Avariado { get; set; }

        /// <summary>
        /// Column VERDE
        /// </summary>
        [DbColumn("VERDE")]
        
        public Decimal? Verde { get; set; }

        /// <summary>
        /// Column TEMPERATURA
        /// </summary>
        [DbColumn("TEMPERATURA")]
        
        public Decimal? Temperatura { get; set; }

        /// <summary>
        /// Column ARDIDO
        /// </summary>
        [DbColumn("ARDIDO")]
        
        public Decimal? Ardido { get; set; }

        /// <summary>
        /// Column QUEBRADO
        /// </summary>
        [DbColumn("QUEBRADO")]
        
        public Decimal? Quebrado { get; set; }

        /// <summary>
        /// Column CARBONIZADO
        /// </summary>
        [DbColumn("CARBONIZADO")]
        
        public Decimal? Carbonizado { get; set; }

        /// <summary>
        /// Column CARUNCHADO
        /// </summary>
        [DbColumn("CARUNCHADO")]
        
        public Decimal? Carunchado { get; set; }

        /// <summary>
        /// Column AFLATOXINA
        /// </summary>
        [DbColumn("AFLATOXINA")]
        
        public String Aflatoxina { get; set; }

        /// <summary>
        /// Column DOSAGEM
        /// </summary>
        [DbColumn("DOSAGEM")]
        
        public Decimal? Dosagem { get; set; }

        /// <summary>
        /// Column JUSTIFICATIVA
        /// </summary>
        [DbColumn("JUSTIFICATIVA")]
        
        public String Justificativa { get; set; }

        /// <summary>
        /// Column OBSERVACAO
        /// </summary>
        [DbColumn("OBSERVACAO")]
        
        public String Observacao { get; set; }

        /// <summary>
        /// Column NOTAS_FISCAIS
        /// </summary>
        [DbColumn("NOTAS_FISCAIS")]
        
        public String NotasFiscais { get; set; }

        /// <summary>
        /// Column CARRETAS
        /// </summary>
        [DbColumn("CARRETAS")]
        
        public String Carretas { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwSefazRecusaTriagem value)
        {
            if (value == null)
                return false;
            return
				this.Id == value.Id &&
				this.Datacadastro == value.Datacadastro &&
				this.Cliente == value.Cliente &&
				this.Produto == value.Produto &&
				this.Dataprevisaodescarga == value.Dataprevisaodescarga &&
				this.Terminal == value.Terminal &&
				this.Cte == value.Cte &&
				this.Mdfe == value.Mdfe &&
				this.Veiculo == value.Veiculo &&
				this.Cpf == value.Cpf &&
				this.Motorista == value.Motorista &&
				this.Datalancamento == value.Datalancamento &&
				this.Usuario == value.Usuario &&
				this.Aprovado == value.Aprovado &&
				this.Impureza == value.Impureza &&
				this.Umidade == value.Umidade &&
				this.Avariado == value.Avariado &&
				this.Verde == value.Verde &&
				this.Temperatura == value.Temperatura &&
				this.Ardido == value.Ardido &&
				this.Quebrado == value.Quebrado &&
				this.Carbonizado == value.Carbonizado &&
				this.Carunchado == value.Carunchado &&
				this.Aflatoxina == value.Aflatoxina &&
				this.Dosagem == value.Dosagem &&
				this.Justificativa == value.Justificativa &&
				this.Observacao == value.Observacao &&
				this.NotasFiscais == value.NotasFiscais &&
				this.Carretas == value.Carretas;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwSefazRecusaTriagem CloneT()
        {
            VwSefazRecusaTriagem value = new VwSefazRecusaTriagem();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Id = this.Id;
			value.Datacadastro = this.Datacadastro;
			value.Cliente = this.Cliente;
			value.Produto = this.Produto;
			value.Dataprevisaodescarga = this.Dataprevisaodescarga;
			value.Terminal = this.Terminal;
			value.Cte = this.Cte;
			value.Mdfe = this.Mdfe;
			value.Veiculo = this.Veiculo;
			value.Cpf = this.Cpf;
			value.Motorista = this.Motorista;
			value.Datalancamento = this.Datalancamento;
			value.Usuario = this.Usuario;
			value.Aprovado = this.Aprovado;
			value.Impureza = this.Impureza;
			value.Umidade = this.Umidade;
			value.Avariado = this.Avariado;
			value.Verde = this.Verde;
			value.Temperatura = this.Temperatura;
			value.Ardido = this.Ardido;
			value.Quebrado = this.Quebrado;
			value.Carbonizado = this.Carbonizado;
			value.Carunchado = this.Carunchado;
			value.Aflatoxina = this.Aflatoxina;
			value.Dosagem = this.Dosagem;
			value.Justificativa = this.Justificativa;
			value.Observacao = this.Observacao;
			value.NotasFiscais = this.NotasFiscais;
			value.Carretas = this.Carretas;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwSefazRecusaTriagem Create(Decimal _Id, DateTime _Datacadastro, String _Cliente, String _Produto, DateTime _Dataprevisaodescarga, String _Terminal, String _Cte, String _Mdfe, String _Veiculo, String _Cpf, String _Motorista, DateTime? _Datalancamento, String _Usuario, String _Aprovado, Decimal _Impureza, Decimal _Umidade, Decimal _Avariado, Decimal _Verde, Decimal _Temperatura, Decimal _Ardido, Decimal _Quebrado, Decimal _Carbonizado, Decimal? _Carunchado, String _Aflatoxina, Decimal? _Dosagem, String _Justificativa, String _Observacao, String _NotasFiscais, String _Carretas)
        {
            VwSefazRecusaTriagem __value = new VwSefazRecusaTriagem();

			__value.Id = _Id;
			__value.Datacadastro = _Datacadastro;
			__value.Cliente = _Cliente;
			__value.Produto = _Produto;
			__value.Dataprevisaodescarga = _Dataprevisaodescarga;
			__value.Terminal = _Terminal;
			__value.Cte = _Cte;
			__value.Mdfe = _Mdfe;
			__value.Veiculo = _Veiculo;
			__value.Cpf = _Cpf;
			__value.Motorista = _Motorista;
			__value.Datalancamento = _Datalancamento;
			__value.Usuario = _Usuario;
			__value.Aprovado = _Aprovado;
			__value.Impureza = _Impureza;
			__value.Umidade = _Umidade;
			__value.Avariado = _Avariado;
			__value.Verde = _Verde;
			__value.Temperatura = _Temperatura;
			__value.Ardido = _Ardido;
			__value.Quebrado = _Quebrado;
			__value.Carbonizado = _Carbonizado;
			__value.Carunchado = _Carunchado;
			__value.Aflatoxina = _Aflatoxina;
			__value.Dosagem = _Dosagem;
			__value.Justificativa = _Justificativa;
			__value.Observacao = _Observacao;
			__value.NotasFiscais = _NotasFiscais;
			__value.Carretas = _Carretas;

            return __value;
        }

        #endregion Create

   }

}
