// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "PTR_CARGA_VAGAO_LOG", "", "")]
    [Serializable]
    [DataContract(Name = "PtrCargaVagaoLog", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class PtrCargaVagaoLog : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        /// <summary>
        /// Column NUM_VAGAO
        /// </summary>
        [DbColumn("NUM_VAGAO")]
        
        public Int32? NumVagao { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_VAZIO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_VAZIO")]
        
        public DateTime? DataHoraPesoVazio { get; set; }

        /// <summary>
        /// Column COD_CONTRATO
        /// </summary>
        [DbColumn("COD_CONTRATO")]
        
        public String CodContrato { get; set; }

        /// <summary>
        /// Column TARA
        /// </summary>
        [DbColumn("TARA")]
        
        public Int32? Tara { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARREGADO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARREGADO")]
        
        public DateTime? DataHoraPesoCarregado { get; set; }

        /// <summary>
        /// Column PESO_BRUTO
        /// </summary>
        [DbColumn("PESO_BRUTO")]
        
        public Int32? PesoBruto { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARR_ANTERIOR
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARR_ANTERIOR")]
        
        public DateTime? DataHoraPesoCarrAnterior { get; set; }

        /// <summary>
        /// Column FLAG_ABERTO
        /// </summary>
        [DbColumn("FLAG_ABERTO")]
        
        public String FlagAberto { get; set; }

        /// <summary>
        /// Column COD_TERMINAL
        /// </summary>
        [DbColumn("COD_TERMINAL")]
        
        public String CodTerminal { get; set; }

        /// <summary>
        /// Column FLAG_MEIA_CARGA
        /// </summary>
        [DbColumn("FLAG_MEIA_CARGA")]
        
        public String FlagMeiaCarga { get; set; }

        /// <summary>
        /// Column SQ_FATURA
        /// </summary>
        [DbColumn("SQ_FATURA")]
        
        public Int64? SqFatura { get; set; }

        /// <summary>
        /// Column VR_FATURA
        /// </summary>
        [DbColumn("VR_FATURA")]
        
        public Decimal? VrFatura { get; set; }

        /// <summary>
        /// Column SER_DCL
        /// </summary>
        [DbColumn("SER_DCL")]
        
        public String SerDcl { get; set; }

        /// <summary>
        /// Column NUM_DCL
        /// </summary>
        [DbColumn("NUM_DCL")]
        
        public Int32? NumDcl { get; set; }

        /// <summary>
        /// Column PB1
        /// </summary>
        [DbColumn("PB1")]
        
        public Int64? Pb1 { get; set; }

        /// <summary>
        /// Column PB2
        /// </summary>
        [DbColumn("PB2")]
        
        public Int64? Pb2 { get; set; }

        /// <summary>
        /// Column PB3
        /// </summary>
        [DbColumn("PB3")]
        
        public Int64? Pb3 { get; set; }

        /// <summary>
        /// Column PB4
        /// </summary>
        [DbColumn("PB4")]
        
        public Int64? Pb4 { get; set; }

        /// <summary>
        /// Column T1
        /// </summary>
        [DbColumn("T1")]
        
        public Int64? T1 { get; set; }

        /// <summary>
        /// Column T2
        /// </summary>
        [DbColumn("T2")]
        
        public Int64? T2 { get; set; }

        /// <summary>
        /// Column T3
        /// </summary>
        [DbColumn("T3")]
        
        public Int64? T3 { get; set; }

        /// <summary>
        /// Column T4
        /// </summary>
        [DbColumn("T4")]
        
        public Int64? T4 { get; set; }

        /// <summary>
        /// Column TU_REFERENCIA
        /// </summary>
        [DbColumn("TU_REFERENCIA")]
        
        public Int64? TuReferencia { get; set; }

        /// <summary>
        /// Column USUARIO
        /// </summary>
        [DbColumn("USUARIO")]
        
        public String Usuario { get; set; }

        /// <summary>
        /// Column DESCRICAO
        /// </summary>
        [DbColumn("DESCRICAO")]
        
        public String Descricao { get; set; }

        /// <summary>
        /// Column DATA_EVENTO_EXCLUSAO
        /// </summary>
        [DbColumn("DATA_EVENTO_EXCLUSAO")]
        
        public DateTime? DataEventoExclusao { get; set; }

        /// <summary>
        /// Column TP_EXCLUSAO
        /// </summary>
        [DbColumn("TP_EXCLUSAO")]
        
        public String TpExclusao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(PtrCargaVagaoLog value)
        {
            if (value == null)
                return false;
            return
				this.SqVagao == value.SqVagao &&
				this.NumVagao == value.NumVagao &&
				this.DataHoraPesoVazio == value.DataHoraPesoVazio &&
				this.CodContrato == value.CodContrato &&
				this.Tara == value.Tara &&
				this.DataHoraPesoCarregado == value.DataHoraPesoCarregado &&
				this.PesoBruto == value.PesoBruto &&
				this.DataHoraPesoCarrAnterior == value.DataHoraPesoCarrAnterior &&
				this.FlagAberto == value.FlagAberto &&
				this.CodTerminal == value.CodTerminal &&
				this.FlagMeiaCarga == value.FlagMeiaCarga &&
				this.SqFatura == value.SqFatura &&
				this.VrFatura == value.VrFatura &&
				this.SerDcl == value.SerDcl &&
				this.NumDcl == value.NumDcl &&
				this.Pb1 == value.Pb1 &&
				this.Pb2 == value.Pb2 &&
				this.Pb3 == value.Pb3 &&
				this.Pb4 == value.Pb4 &&
				this.T1 == value.T1 &&
				this.T2 == value.T2 &&
				this.T3 == value.T3 &&
				this.T4 == value.T4 &&
				this.TuReferencia == value.TuReferencia &&
				this.Usuario == value.Usuario &&
				this.Descricao == value.Descricao &&
				this.DataEventoExclusao == value.DataEventoExclusao &&
				this.TpExclusao == value.TpExclusao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public PtrCargaVagaoLog CloneT()
        {
            PtrCargaVagaoLog value = new PtrCargaVagaoLog();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.SqVagao = this.SqVagao;
			value.NumVagao = this.NumVagao;
			value.DataHoraPesoVazio = this.DataHoraPesoVazio;
			value.CodContrato = this.CodContrato;
			value.Tara = this.Tara;
			value.DataHoraPesoCarregado = this.DataHoraPesoCarregado;
			value.PesoBruto = this.PesoBruto;
			value.DataHoraPesoCarrAnterior = this.DataHoraPesoCarrAnterior;
			value.FlagAberto = this.FlagAberto;
			value.CodTerminal = this.CodTerminal;
			value.FlagMeiaCarga = this.FlagMeiaCarga;
			value.SqFatura = this.SqFatura;
			value.VrFatura = this.VrFatura;
			value.SerDcl = this.SerDcl;
			value.NumDcl = this.NumDcl;
			value.Pb1 = this.Pb1;
			value.Pb2 = this.Pb2;
			value.Pb3 = this.Pb3;
			value.Pb4 = this.Pb4;
			value.T1 = this.T1;
			value.T2 = this.T2;
			value.T3 = this.T3;
			value.T4 = this.T4;
			value.TuReferencia = this.TuReferencia;
			value.Usuario = this.Usuario;
			value.Descricao = this.Descricao;
			value.DataEventoExclusao = this.DataEventoExclusao;
			value.TpExclusao = this.TpExclusao;

            return value;
        }

        #endregion Clone

        #region Create

        public static PtrCargaVagaoLog Create(Int64 _SqVagao, Int32 _NumVagao, DateTime _DataHoraPesoVazio, String _CodContrato, Int32? _Tara, DateTime? _DataHoraPesoCarregado, Int32? _PesoBruto, DateTime? _DataHoraPesoCarrAnterior, String _FlagAberto, String _CodTerminal, String _FlagMeiaCarga, Int64? _SqFatura, Decimal? _VrFatura, String _SerDcl, Int32? _NumDcl, Int64? _Pb1, Int64? _Pb2, Int64? _Pb3, Int64? _Pb4, Int64? _T1, Int64? _T2, Int64? _T3, Int64? _T4, Int64? _TuReferencia, String _Usuario, String _Descricao, DateTime? _DataEventoExclusao, String _TpExclusao)
        {
            PtrCargaVagaoLog __value = new PtrCargaVagaoLog();

			__value.SqVagao = _SqVagao;
			__value.NumVagao = _NumVagao;
			__value.DataHoraPesoVazio = _DataHoraPesoVazio;
			__value.CodContrato = _CodContrato;
			__value.Tara = _Tara;
			__value.DataHoraPesoCarregado = _DataHoraPesoCarregado;
			__value.PesoBruto = _PesoBruto;
			__value.DataHoraPesoCarrAnterior = _DataHoraPesoCarrAnterior;
			__value.FlagAberto = _FlagAberto;
			__value.CodTerminal = _CodTerminal;
			__value.FlagMeiaCarga = _FlagMeiaCarga;
			__value.SqFatura = _SqFatura;
			__value.VrFatura = _VrFatura;
			__value.SerDcl = _SerDcl;
			__value.NumDcl = _NumDcl;
			__value.Pb1 = _Pb1;
			__value.Pb2 = _Pb2;
			__value.Pb3 = _Pb3;
			__value.Pb4 = _Pb4;
			__value.T1 = _T1;
			__value.T2 = _T2;
			__value.T3 = _T3;
			__value.T4 = _T4;
			__value.TuReferencia = _TuReferencia;
			__value.Usuario = _Usuario;
			__value.Descricao = _Descricao;
			__value.DataEventoExclusao = _DataEventoExclusao;
			__value.TpExclusao = _TpExclusao;

            return __value;
        }

        #endregion Create

   }

}
