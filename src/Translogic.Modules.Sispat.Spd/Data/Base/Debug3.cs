// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "DEBUG3", "", "")]
    [Serializable]
    [DataContract(Name = "Debug3", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class Debug3 : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column SQ
        /// </summary>
        [DbColumn("SQ")]
        
        public Int64? Sq { get; set; }

        /// <summary>
        /// Column MSG
        /// </summary>
        [DbColumn("MSG")]
        
        public String Msg { get; set; }

        /// <summary>
        /// Column DT
        /// </summary>
        [DbColumn("DT")]
        
        public DateTime? Dt { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(Debug3 value)
        {
            if (value == null)
                return false;
            return
				this.Sq == value.Sq &&
				this.Msg == value.Msg &&
				this.Dt == value.Dt;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public Debug3 CloneT()
        {
            Debug3 value = new Debug3();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Sq = this.Sq;
			value.Msg = this.Msg;
			value.Dt = this.Dt;

            return value;
        }

        #endregion Clone

        #region Create

        public static Debug3 Create(Int64? _Sq, String _Msg, DateTime? _Dt)
        {
            Debug3 __value = new Debug3();

			__value.Sq = _Sq;
			__value.Msg = _Msg;
			__value.Dt = _Dt;

            return __value;
        }

        #endregion Create

   }

}
