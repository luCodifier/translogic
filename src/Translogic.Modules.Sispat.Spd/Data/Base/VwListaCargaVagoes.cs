// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_LISTA_CARGA_VAGOES", "", "")]
    [Serializable]
    [DataContract(Name = "VwListaCargaVagoes", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwListaCargaVagoes : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column ATIVO
        /// </summary>
        [DbColumn("ATIVO")]
        
        public String Ativo { get; set; }

        /// <summary>
        /// Column SERIE_VAGAO
        /// </summary>
        [DbColumn("SERIE_VAGAO")]
        
        public String SerieVagao { get; set; }

        /// <summary>
        /// Column CODIGO_VAGAO
        /// </summary>
        [DbColumn("CODIGO_VAGAO")]
        
        public String CodigoVagao { get; set; }

        /// <summary>
        /// Column TERMINAL
        /// </summary>
        [DbColumn("TERMINAL")]
        
        public String Terminal { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column PESO_BRUTO
        /// </summary>
        [DbColumn("PESO_BRUTO")]
        
        public Int32? PesoBruto { get; set; }

        /// <summary>
        /// Column PESO_TARA
        /// </summary>
        [DbColumn("PESO_TARA")]
        
        public Int32? PesoTara { get; set; }

        /// <summary>
        /// Column PESO_LIQUIDO
        /// </summary>
        [DbColumn("PESO_LIQUIDO")]
        
        public Decimal? PesoLiquido { get; set; }

        /// <summary>
        /// Column STATUS
        /// </summary>
        [DbColumn("STATUS")]
        
        public String Status { get; set; }

        /// <summary>
        /// Column ID_STATUS
        /// </summary>
        [DbColumn("ID_STATUS")]
        
        public String IdStatus { get; set; }

        /// <summary>
        /// Column DCL_SERIE
        /// </summary>
        [DbColumn("DCL_SERIE")]
        
        public String DclSerie { get; set; }

        /// <summary>
        /// Column DCL_NUMERO
        /// </summary>
        [DbColumn("DCL_NUMERO")]
        
        public Int32? DclNumero { get; set; }

        /// <summary>
        /// Column MULTIPLO
        /// </summary>
        [DbColumn("MULTIPLO")]
        
        public String Multiplo { get; set; }

        /// <summary>
        /// Column FLUXO
        /// </summary>
        [DbColumn("FLUXO")]
        
        public String Fluxo { get; set; }

        /// <summary>
        /// Column BALANCA
        /// </summary>
        [DbColumn("BALANCA")]
        
        public String Balanca { get; set; }

        /// <summary>
        /// Column LIMITE_VAGAO
        /// </summary>
        [DbColumn("LIMITE_VAGAO")]
        
        public Int64? LimiteVagao { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARREGADO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARREGADO")]
        
        public DateTime? DataHoraPesoCarregado { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_VAZIO
        /// </summary>
        [DbColumn("DATA_HORA_PESO_VAZIO")]
        
        public DateTime? DataHoraPesoVazio { get; set; }

        /// <summary>
        /// Column DATA_HORA_PESO_CARR_ANTERIOR
        /// </summary>
        [DbColumn("DATA_HORA_PESO_CARR_ANTERIOR")]
        
        public DateTime? DataHoraPesoCarrAnterior { get; set; }

        /// <summary>
        /// Column DATA_ENVIO_EMAIL
        /// </summary>
        [DbColumn("DATA_ENVIO_EMAIL")]
        
        public DateTime? DataEnvioEmail { get; set; }

        /// <summary>
        /// Column SQ_VAGAO
        /// </summary>
        [DbColumn("SQ_VAGAO")]
        
        public Int64? SqVagao { get; set; }

        /// <summary>
        /// Column COD_PRODUTO
        /// </summary>
        [DbColumn("COD_PRODUTO")]
        
        public String CodProduto { get; set; }

        /// <summary>
        /// Column DSC_PRODUTO
        /// </summary>
        [DbColumn("DSC_PRODUTO")]
        
        public String DscProduto { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwListaCargaVagoes value)
        {
            if (value == null)
                return false;
            return
				this.Ativo == value.Ativo &&
				this.SerieVagao == value.SerieVagao &&
				this.CodigoVagao == value.CodigoVagao &&
				this.Terminal == value.Terminal &&
				this.Contrato == value.Contrato &&
				this.PesoBruto == value.PesoBruto &&
				this.PesoTara == value.PesoTara &&
				this.PesoLiquido == value.PesoLiquido &&
				this.Status == value.Status &&
				this.IdStatus == value.IdStatus &&
				this.DclSerie == value.DclSerie &&
				this.DclNumero == value.DclNumero &&
				this.Multiplo == value.Multiplo &&
				this.Fluxo == value.Fluxo &&
				this.Balanca == value.Balanca &&
				this.LimiteVagao == value.LimiteVagao &&
				this.DataHoraPesoCarregado == value.DataHoraPesoCarregado &&
				this.DataHoraPesoVazio == value.DataHoraPesoVazio &&
				this.DataHoraPesoCarrAnterior == value.DataHoraPesoCarrAnterior &&
				this.DataEnvioEmail == value.DataEnvioEmail &&
				this.SqVagao == value.SqVagao &&
				this.CodProduto == value.CodProduto &&
				this.DscProduto == value.DscProduto;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwListaCargaVagoes CloneT()
        {
            VwListaCargaVagoes value = new VwListaCargaVagoes();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.Ativo = this.Ativo;
			value.SerieVagao = this.SerieVagao;
			value.CodigoVagao = this.CodigoVagao;
			value.Terminal = this.Terminal;
			value.Contrato = this.Contrato;
			value.PesoBruto = this.PesoBruto;
			value.PesoTara = this.PesoTara;
			value.PesoLiquido = this.PesoLiquido;
			value.Status = this.Status;
			value.IdStatus = this.IdStatus;
			value.DclSerie = this.DclSerie;
			value.DclNumero = this.DclNumero;
			value.Multiplo = this.Multiplo;
			value.Fluxo = this.Fluxo;
			value.Balanca = this.Balanca;
			value.LimiteVagao = this.LimiteVagao;
			value.DataHoraPesoCarregado = this.DataHoraPesoCarregado;
			value.DataHoraPesoVazio = this.DataHoraPesoVazio;
			value.DataHoraPesoCarrAnterior = this.DataHoraPesoCarrAnterior;
			value.DataEnvioEmail = this.DataEnvioEmail;
			value.SqVagao = this.SqVagao;
			value.CodProduto = this.CodProduto;
			value.DscProduto = this.DscProduto;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwListaCargaVagoes Create(String _Ativo, String _SerieVagao, String _CodigoVagao, String _Terminal, String _Contrato, Int32? _PesoBruto, Int32? _PesoTara, Decimal? _PesoLiquido, String _Status, String _IdStatus, String _DclSerie, Int32? _DclNumero, String _Multiplo, String _Fluxo, String _Balanca, Int64? _LimiteVagao, DateTime? _DataHoraPesoCarregado, DateTime _DataHoraPesoVazio, DateTime? _DataHoraPesoCarrAnterior, DateTime? _DataEnvioEmail, Int64 _SqVagao, String _CodProduto, String _DscProduto)
        {
            VwListaCargaVagoes __value = new VwListaCargaVagoes();

			__value.Ativo = _Ativo;
			__value.SerieVagao = _SerieVagao;
			__value.CodigoVagao = _CodigoVagao;
			__value.Terminal = _Terminal;
			__value.Contrato = _Contrato;
			__value.PesoBruto = _PesoBruto;
			__value.PesoTara = _PesoTara;
			__value.PesoLiquido = _PesoLiquido;
			__value.Status = _Status;
			__value.IdStatus = _IdStatus;
			__value.DclSerie = _DclSerie;
			__value.DclNumero = _DclNumero;
			__value.Multiplo = _Multiplo;
			__value.Fluxo = _Fluxo;
			__value.Balanca = _Balanca;
			__value.LimiteVagao = _LimiteVagao;
			__value.DataHoraPesoCarregado = _DataHoraPesoCarregado;
			__value.DataHoraPesoVazio = _DataHoraPesoVazio;
			__value.DataHoraPesoCarrAnterior = _DataHoraPesoCarrAnterior;
			__value.DataEnvioEmail = _DataEnvioEmail;
			__value.SqVagao = _SqVagao;
			__value.CodProduto = _CodProduto;
			__value.DscProduto = _DscProduto;

            return __value;
        }

        #endregion Create

   }

}
