// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_TERMINAL_EMPRESA", "", "")]
    [Serializable]
    [DataContract(Name = "VwTerminalEmpresa", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwTerminalEmpresa : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column EMP_CNPJ
        /// </summary>
        [DbColumn("EMP_CNPJ")]
        
        public String EmpCnpj { get; set; }

        /// <summary>
        /// Column EMP_RAZAO_SOCIAL
        /// </summary>
        [DbColumn("EMP_RAZAO_SOCIAL")]
        
        public String EmpRazaoSocial { get; set; }

        /// <summary>
        /// Column EMP_FAT_BLOQUEADO
        /// </summary>
        [DbColumn("EMP_FAT_BLOQUEADO")]
        
        public String EmpFatBloqueado { get; set; }

        /// <summary>
        /// Column TER_SIG
        /// </summary>
        [DbColumn("TER_SIG")]
        
        public String TerSig { get; set; }

        /// <summary>
        /// Column TER_DESCRICAO
        /// </summary>
        [DbColumn("TER_DESCRICAO")]
        
        public String TerDescricao { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwTerminalEmpresa value)
        {
            if (value == null)
                return false;
            return
				this.EmpCnpj == value.EmpCnpj &&
				this.EmpRazaoSocial == value.EmpRazaoSocial &&
				this.EmpFatBloqueado == value.EmpFatBloqueado &&
				this.TerSig == value.TerSig &&
				this.TerDescricao == value.TerDescricao;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwTerminalEmpresa CloneT()
        {
            VwTerminalEmpresa value = new VwTerminalEmpresa();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.EmpCnpj = this.EmpCnpj;
			value.EmpRazaoSocial = this.EmpRazaoSocial;
			value.EmpFatBloqueado = this.EmpFatBloqueado;
			value.TerSig = this.TerSig;
			value.TerDescricao = this.TerDescricao;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwTerminalEmpresa Create(String _EmpCnpj, String _EmpRazaoSocial, String _EmpFatBloqueado, String _TerSig, String _TerDescricao)
        {
            VwTerminalEmpresa __value = new VwTerminalEmpresa();

			__value.EmpCnpj = _EmpCnpj;
			__value.EmpRazaoSocial = _EmpRazaoSocial;
			__value.EmpFatBloqueado = _EmpFatBloqueado;
			__value.TerSig = _TerSig;
			__value.TerDescricao = _TerDescricao;

            return __value;
        }

        #endregion Create

   }

}
