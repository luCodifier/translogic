// ****** SPEED ******
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Speed;
using Speed.Data;


namespace Translogic.Modules.Sispat.Spd.Data
{

    [DbTable("", "VW_DADOS_NF", "", "")]
    [Serializable]
    [DataContract(Name = "VwDadosNf", Namespace = "")]
//    [System.Diagnostics.DebuggerStepThrough]
    
    public partial class VwDadosNf : Record, ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// Column PES_DEC
        /// </summary>
        [DbColumn("PES_DEC")]
        
        public Decimal? PesDec { get; set; }

        /// <summary>
        /// Column PES_DES
        /// </summary>
        [DbColumn("PES_DES")]
        
        public Int64? PesDes { get; set; }

        /// <summary>
        /// Column EXP_SPT
        /// </summary>
        [DbColumn("EXP_SPT")]
        
        public Decimal? ExpSpt { get; set; }

        /// <summary>
        /// Column EXP_TL
        /// </summary>
        [DbColumn("EXP_TL")]
        
        public Decimal? ExpTl { get; set; }

        /// <summary>
        /// Column FLG_ARM
        /// </summary>
        [DbColumn("FLG_ARM")]
        
        public String FlgArm { get; set; }

        /// <summary>
        /// Column FLG_APR
        /// </summary>
        [DbColumn("FLG_APR")]
        
        public String FlgApr { get; set; }

        /// <summary>
        /// Column STS_CAM
        /// </summary>
        [DbColumn("STS_CAM")]
        
        public String StsCam { get; set; }

        /// <summary>
        /// Column NFE_CHAVE
        /// </summary>
        [DbColumn("NFE_CHAVE")]
        
        public String NfeChave { get; set; }

        /// <summary>
        /// Column CLI_ORI_SPT
        /// </summary>
        [DbColumn("CLI_ORI_SPT")]
        
        public String CliOriSpt { get; set; }

        /// <summary>
        /// Column CLI_ORI_TL
        /// </summary>
        [DbColumn("CLI_ORI_TL")]
        
        public String CliOriTl { get; set; }

        /// <summary>
        /// Column CLI_DST_SPT
        /// </summary>
        [DbColumn("CLI_DST_SPT")]
        
        public String CliDstSpt { get; set; }

        /// <summary>
        /// Column CLI_DST_TL
        /// </summary>
        [DbColumn("CLI_DST_TL")]
        
        public String CliDstTl { get; set; }

        /// <summary>
        /// Column SQ_NF
        /// </summary>
        [DbColumn("SQ_NF")]
        
        public Int64? SqNf { get; set; }

        /// <summary>
        /// Column TERM
        /// </summary>
        [DbColumn("TERM")]
        
        public String Term { get; set; }

        /// <summary>
        /// Column CONTRATO
        /// </summary>
        [DbColumn("CONTRATO")]
        
        public String Contrato { get; set; }

        /// <summary>
        /// Column CLIENTE
        /// </summary>
        [DbColumn("CLIENTE")]
        
        public String Cliente { get; set; }

        /// <summary>
        /// Column NF_NUM
        /// </summary>
        [DbColumn("NF_NUM")]
        
        public Decimal? NfNum { get; set; }

        /// <summary>
        /// Column NF_SERIE
        /// </summary>
        [DbColumn("NF_SERIE")]
        
        public String NfSerie { get; set; }

        /// <summary>
        /// Column NF_DATA
        /// </summary>
        [DbColumn("NF_DATA")]
        
        public DateTime? NfData { get; set; }

        /// <summary>
        /// Column DT_MARCACAO
        /// </summary>
        [DbColumn("DT_MARCACAO")]
        
        public DateTime? DtMarcacao { get; set; }

        /// <summary>
        /// Column DSC_MERCADORIA
        /// </summary>
        [DbColumn("DSC_MERCADORIA")]
        
        public String DscMercadoria { get; set; }

        /// <summary>
        /// Column FLUXOSOL
        /// </summary>
        [DbColumn("FLUXOSOL")]
        
        public String Fluxosol { get; set; }

        
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IsEqual

        public bool IsEqual(VwDadosNf value)
        {
            if (value == null)
                return false;
            return
				this.PesDec == value.PesDec &&
				this.PesDes == value.PesDes &&
				this.ExpSpt == value.ExpSpt &&
				this.ExpTl == value.ExpTl &&
				this.FlgArm == value.FlgArm &&
				this.FlgApr == value.FlgApr &&
				this.StsCam == value.StsCam &&
				this.NfeChave == value.NfeChave &&
				this.CliOriSpt == value.CliOriSpt &&
				this.CliOriTl == value.CliOriTl &&
				this.CliDstSpt == value.CliDstSpt &&
				this.CliDstTl == value.CliDstTl &&
				this.SqNf == value.SqNf &&
				this.Term == value.Term &&
				this.Contrato == value.Contrato &&
				this.Cliente == value.Cliente &&
				this.NfNum == value.NfNum &&
				this.NfSerie == value.NfSerie &&
				this.NfData == value.NfData &&
				this.DtMarcacao == value.DtMarcacao &&
				this.DscMercadoria == value.DscMercadoria &&
				this.Fluxosol == value.Fluxosol;

        }

        #endregion IsEqual

        #region Clone

        public override object Clone()
        {
            return CloneT();
        }

        public VwDadosNf CloneT()
        {
            VwDadosNf value = new VwDadosNf();
            //value.RecordStatus = this.RecordStatus;
            //value.RecordTag = this.RecordTag;

			value.PesDec = this.PesDec;
			value.PesDes = this.PesDes;
			value.ExpSpt = this.ExpSpt;
			value.ExpTl = this.ExpTl;
			value.FlgArm = this.FlgArm;
			value.FlgApr = this.FlgApr;
			value.StsCam = this.StsCam;
			value.NfeChave = this.NfeChave;
			value.CliOriSpt = this.CliOriSpt;
			value.CliOriTl = this.CliOriTl;
			value.CliDstSpt = this.CliDstSpt;
			value.CliDstTl = this.CliDstTl;
			value.SqNf = this.SqNf;
			value.Term = this.Term;
			value.Contrato = this.Contrato;
			value.Cliente = this.Cliente;
			value.NfNum = this.NfNum;
			value.NfSerie = this.NfSerie;
			value.NfData = this.NfData;
			value.DtMarcacao = this.DtMarcacao;
			value.DscMercadoria = this.DscMercadoria;
			value.Fluxosol = this.Fluxosol;

            return value;
        }

        #endregion Clone

        #region Create

        public static VwDadosNf Create(Decimal? _PesDec, Int64? _PesDes, Decimal? _ExpSpt, Decimal? _ExpTl, String _FlgArm, String _FlgApr, String _StsCam, String _NfeChave, String _CliOriSpt, String _CliOriTl, String _CliDstSpt, String _CliDstTl, Int64 _SqNf, String _Term, String _Contrato, String _Cliente, Decimal? _NfNum, String _NfSerie, DateTime? _NfData, DateTime? _DtMarcacao, String _DscMercadoria, String _Fluxosol)
        {
            VwDadosNf __value = new VwDadosNf();

			__value.PesDec = _PesDec;
			__value.PesDes = _PesDes;
			__value.ExpSpt = _ExpSpt;
			__value.ExpTl = _ExpTl;
			__value.FlgArm = _FlgArm;
			__value.FlgApr = _FlgApr;
			__value.StsCam = _StsCam;
			__value.NfeChave = _NfeChave;
			__value.CliOriSpt = _CliOriSpt;
			__value.CliOriTl = _CliOriTl;
			__value.CliDstSpt = _CliDstSpt;
			__value.CliDstTl = _CliDstTl;
			__value.SqNf = _SqNf;
			__value.Term = _Term;
			__value.Contrato = _Contrato;
			__value.Cliente = _Cliente;
			__value.NfNum = _NfNum;
			__value.NfSerie = _NfSerie;
			__value.NfData = _NfData;
			__value.DtMarcacao = _DtMarcacao;
			__value.DscMercadoria = _DscMercadoria;
			__value.Fluxosol = _Fluxosol;

            return __value;
        }

        #endregion Create

   }

}
