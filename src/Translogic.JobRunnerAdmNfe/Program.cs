﻿namespace Translogic.JobRunnerBaixarNfesDoEmail
{
    using Speed.Common;
    using System;
    using System.Linq;
    using System.Threading;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.Core.Jobs.JobRunner;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe que inicia o programa
    /// </summary>
    public static class Program
    {

        const string key = "core.job.NfeEmailEdiJob1Asc";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static int Main(string[] args)
        {
            return JobRunnerProgram.Execute(EnumSistema.JobRunnerAdmNfe, key, args);
        }
    }
}
