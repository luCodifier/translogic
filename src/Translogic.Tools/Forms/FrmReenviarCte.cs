﻿namespace Translogic.Tools.Forms
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Windows.Forms;
    using Speed.Common;
    using Translogic.Core;
    using Translogic.Modules.Core.Domain.Services.Ctes;
    using Translogic.Modules.Core.Spd;

    /// <summary>
    /// Form de Reenvio de e-mails de ctes
    /// </summary>
    public partial class FrmReenviarCte : Form
    {
        /// <summary>
        /// Constructor da classe
        /// </summary>
        public FrmReenviarCte()
        {
            InitializeComponent();
        }

        private void BtnReenviarPorIds_Click(object sender, EventArgs e)
        {
            Program.Execute(
                this,
                () =>
                {
                    var ids = GetIds();

                    if (!ids.Any())
                    {
                        Program.ShowError("Nenhum id válido");
                        return;
                    }

                    var srv = TranslogicStarter.Container.Resolve<CteService>();
                    srv.ReenviarEmailporCte(ids);
                    Program.ShowInformation("Reenvio de e-mails finalizado");
                });
        }

        private void BtnCarregarIdsPorSelect_Click(object sender, EventArgs e)
        {
            Program.Execute(
                this,
                () =>
                {
                    using (var db = Dbs.NewDb(Db.Translogic))
                    {
                        string sql = txtSelect.Text.Trim();
                        if (sql.EndsWith(";"))
                        {
                            sql = sql.Substring(0, sql.Length - 1);
                        }

                        var ids = db.ExecuteArray1D<object>(sql).Select(p => p.ToInt32()).OrderBy(p => p);
                        txtIds.Text = string.Join(" ", ids);
                    }
                });
        }

        private void BtnValidar_Click(object sender, EventArgs e)
        {
            var ids = GetIds();

            txtIds.Text = string.Join(" ", ids);
        }

        private int[] GetIds()
        {
            var ids = txtIds.Text.Trim()
                             .Split(new char[] { '\r', '\n', '\t', ';', ',', ' ', '|' }, StringSplitOptions.RemoveEmptyEntries)
                             .Select(p => Conv.ExtractOnlyNumbers(p).ToInt32())
                             .Where(p => p > 0)
                             .OrderBy(p => p)
                             .ToArray();
            return ids;
        }
    }
}
