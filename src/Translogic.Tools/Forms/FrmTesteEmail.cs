﻿namespace Translogic.Tools.Forms
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Windows.Forms;
    using Translogic.Tools.Classes;

    /// <summary>
    /// Form de teste de envio de e-mails
    /// </summary>
    public partial class FrmTesteEmail : Form
    {
        /// <summary>
        /// Constructor da classe
        /// </summary>
        public FrmTesteEmail()
        {
            InitializeComponent();
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                WinUtil.ClearTextBoxes(this);

                SmtpClient client = new SmtpClient();

                client.Host = txtSmtp.Text;
                if (txtPort.Text != String.Empty)
                {
                    client.Port = Convert.ToInt32(txtPort.Text);
                }
                
                if (txtPassword.Text != String.Empty)
                {
                    client.Credentials = new NetworkCredential(txtFrom.Text, txtPassword.Text);
                }

                if (chkSsl.Checked)
                {
                    client.EnableSsl = true;
                }

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(txtFrom.Text);
                msg.To.Add(txtTo.Text);
                msg.Subject = txtSubject.Text;
                msg.Body = txtBody.Text;

                client.Send(msg);

                Program.ShowInformation(this, "E-mail enviado com sucesso");
            }
            catch (Exception ex)
            {
                Program.ShowError(ex);
            }
        }
    }
}
