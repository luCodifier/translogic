﻿namespace Translogic.Tools.Forms
{
    partial class FrmReenviarCte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ctlHelp1 = new Translogic.Tools.UserControls.CtlHelp();
            this.btnValidar = new System.Windows.Forms.Button();
            this.btnReenviarPorIds = new System.Windows.Forms.Button();
            this.txtIds = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCarregarIdsPorSelect = new System.Windows.Forms.Button();
            this.txtSelect = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ctlHelp1);
            this.groupBox1.Controls.Add(this.btnValidar);
            this.groupBox1.Controls.Add(this.btnReenviarPorIds);
            this.groupBox1.Controls.Add(this.txtIds);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(760, 260);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Envio por ID\'s";
            // 
            // ctlHelp1
            // 
            this.ctlHelp1.Location = new System.Drawing.Point(187, 19);
            this.ctlHelp1.Message = "Entre a lista de ID\'s. Os valores podem ser serarados por \',\', \';\', linhas e espa" +
    "ços. Depois clique em\'Validar\' para formatar os valores";
            this.ctlHelp1.Name = "ctlHelp1";
            this.ctlHelp1.Size = new System.Drawing.Size(24, 21);
            this.ctlHelp1.TabIndex = 3;
            // 
            // btnValidar
            // 
            this.btnValidar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnValidar.Location = new System.Drawing.Point(594, 17);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(75, 23);
            this.btnValidar.TabIndex = 0;
            this.btnValidar.Text = "Validar";
            this.toolTip1.SetToolTip(this.btnValidar, "Analisa o texto, retira os números e os coloca em ordem numérica");
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.BtnValidar_Click);
            // 
            // btnReenviarPorIds
            // 
            this.btnReenviarPorIds.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReenviarPorIds.Location = new System.Drawing.Point(675, 16);
            this.btnReenviarPorIds.Name = "btnReenviarPorIds";
            this.btnReenviarPorIds.Size = new System.Drawing.Size(75, 23);
            this.btnReenviarPorIds.TabIndex = 1;
            this.btnReenviarPorIds.Text = "Reenviar";
            this.toolTip1.SetToolTip(this.btnReenviarPorIds, "Reenvia os E-mails dos Ctes");
            this.btnReenviarPorIds.UseVisualStyleBackColor = true;
            this.btnReenviarPorIds.Click += new System.EventHandler(this.BtnReenviarPorIds_Click);
            // 
            // txtIds
            // 
            this.txtIds.AcceptsReturn = true;
            this.txtIds.AcceptsTab = true;
            this.txtIds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIds.Location = new System.Drawing.Point(10, 47);
            this.txtIds.Multiline = true;
            this.txtIds.Name = "txtIds";
            this.txtIds.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtIds.Size = new System.Drawing.Size(740, 192);
            this.txtIds.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID\'s dos Ctes que serão reenviados";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnCarregarIdsPorSelect);
            this.groupBox2.Controls.Add(this.txtSelect);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 294);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(760, 260);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Carregar ID\'s por Select";
            // 
            // btnCarregarIdsPorSelect
            // 
            this.btnCarregarIdsPorSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCarregarIdsPorSelect.Location = new System.Drawing.Point(664, 16);
            this.btnCarregarIdsPorSelect.Name = "btnCarregarIdsPorSelect";
            this.btnCarregarIdsPorSelect.Size = new System.Drawing.Size(86, 23);
            this.btnCarregarIdsPorSelect.TabIndex = 0;
            this.btnCarregarIdsPorSelect.Text = "Carregar IDs";
            this.toolTip1.SetToolTip(this.btnCarregarIdsPorSelect, "EXecuta a query e colocar os ID\'s retornados na caixa de texto acima");
            this.btnCarregarIdsPorSelect.UseVisualStyleBackColor = true;
            this.btnCarregarIdsPorSelect.Click += new System.EventHandler(this.BtnCarregarIdsPorSelect_Click);
            // 
            // txtSelect
            // 
            this.txtSelect.AcceptsReturn = true;
            this.txtSelect.AcceptsTab = true;
            this.txtSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSelect.Location = new System.Drawing.Point(10, 47);
            this.txtSelect.Multiline = true;
            this.txtSelect.Name = "txtSelect";
            this.txtSelect.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSelect.Size = new System.Drawing.Size(740, 192);
            this.txtSelect.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 23);
            this.label2.TabIndex = 0;
            this.label2.Text = "Select que retorna na primeira coluna os ID\'s dos Ctes";
            // 
            // FrmReenviarCte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 560);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FrmReenviarCte";
            this.Text = "FrmReenviarCte";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReenviarPorIds;
        private System.Windows.Forms.TextBox txtIds;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCarregarIdsPorSelect;
        private System.Windows.Forms.TextBox txtSelect;
        private System.Windows.Forms.Label label2;
        private UserControls.CtlHelp ctlHelp1;
        private System.Windows.Forms.Button btnValidar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}