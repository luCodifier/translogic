﻿namespace Translogic.Tools.UserControls
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Controle de Tooltip
    /// </summary>
    public partial class CtlHelp : UserControl
    {
        /// <summary>
        /// Constructor da classe
        /// </summary>
        public CtlHelp()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Mensagem de help
        /// </summary>
        public string Message
        {
            get
            {
                return toolTip1.GetToolTip(pic);
            }

            set
            {
                toolTip1.SetToolTip(pic, value);
            }
        }

        /// <summary>
        /// Load do control
        /// </summary>
        /// <param name="sender">Sender do evento</param>
        /// <param name="e">Propriedade do evento</param>
        private void CtlHelp_Load(object sender, EventArgs e)
        {
        }
    }
}
