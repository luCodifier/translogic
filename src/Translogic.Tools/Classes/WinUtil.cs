﻿namespace Translogic.Tools.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// Classe de métodos úteis nessa Windows Application
    /// </summary>
    public static class WinUtil
    {
        /// <summary>
        /// Faz um Trim em todos os TextBoxes filhos de de parent, recursivamente
        /// </summary>
        /// <param name="parent">Controle parent dos textboxes</param>
        public static void ClearTextBoxes(Control parent)
        {
            ClearTextBoxes(parent, true);
        }

        /// <summary>
        /// Faz um Trim em todos os TextBoxes filhos de de parent, recursivamente
        /// </summary>
        /// <param name="parent">Controle parent dos textboxes</param>
        /// <param name="recursive">Se faz recursivo para cada fiho</param>
        public static void ClearTextBoxes(Control parent, bool recursive)
        {
            foreach (Control ctrl in parent.Controls)
            {
                var txt = ctrl as TextBox;
                if (txt != null)
                {
                    txt.Text = txt.Text.Trim();
                }
                else if (recursive)
                {
                    ClearTextBoxes(ctrl, recursive);
                }
            }
        }
    }
}
