﻿namespace Translogic.Tools
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    /// <summary>
    /// Form principal da aplicação
    /// </summary>
    public partial class FrmMain : Form
    {
        /// <summary>
        /// Constructor da classe
        /// </summary>
        public FrmMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Eventos do Menu
        /// </summary>
        /// <param name="sender">Objeto sender</param>
        /// <param name="e">Parâmetro enviado</param>
        private void MainMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                switch (e.ClickedItem.Name)
                {
                    // case "mnuToolsReenviarCtes":
                    //    ShowForm<FrmReenviarCte>();
                    //    break;
                }
            }
            catch (Exception ex)
            {
                Program.ShowError(this, ex);
            }
        }

        /// <summary>
        /// Carrega e exive um form
        /// </summary>
        /// <typeparam name="T">Tipo do form</typeparam>
        private void ShowForm<T>() where T : Form, new()
        {
            var f = new T();
            f.MdiParent = this;
            f.WindowState = FormWindowState.Maximized;
            f.Show();
        }

        private void MnuToolsReenviarCtes_Click(object sender, EventArgs e)
        {
            ShowForm<Forms.FrmReenviarCte>();
        }

        private void MnuToolsTestEmails_Click(object sender, EventArgs e)
        {
            ShowForm<Forms.FrmTesteEmail>();
        }
    }
}
