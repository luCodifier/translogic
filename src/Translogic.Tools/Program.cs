﻿namespace Translogic.Tools
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Text;
    using System.Threading;
    using System.Windows.Forms;
    using Speed.Common;
    using Topshelf;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe estática que inicia ao programa
    /// </summary>
    public static class Program
    {
        private const string ClassName = "Program";
        private static FrmMain mainForm;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Argumentos enviados ao programa</param>
        public static void Main(string[] args)
        {
            try
            {
                Sis.Inicializar(EnumSistema.Translogic, "Web");
                (new ToolsSetupService()).Start();

                mainForm = new FrmMain();
                Application.Run(mainForm);
            }
            catch (Exception ex)
            {
                ShowError(ex);
            }
        }

        /// <summary>
        /// Exibe uma informação
        /// </summary>
        /// <param name="message">Mensagem à ser exibida</param>
        public static void ShowInformation(string message)
        {
            MessageBox.Show(message, "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Exibe uma informação
        /// </summary>
        /// <param name="owner">Form owner</param>
        /// <param name="message">Mensagem à ser exibida</param>
        public static void ShowInformation(IWin32Window owner, string message)
        {
            MessageBox.Show(owner, message, "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Exibe um erro
        /// </summary>
        /// <param name="message">Mensagem à ser exibida</param>
        public static void ShowError(string message)
        {
            ShowError(mainForm, message);
        }

        /// <summary>
        /// Exibe um erro
        /// </summary>
        /// <param name="owner">Form owner</param>
        /// <param name="message">Mensagem à ser exibida</param>
        public static void ShowError(IWin32Window owner, string message)
        {
            MessageBox.Show(owner, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Exibe um erro
        /// </summary>
        /// <param name="ex">Excetpion à ser exibida</param>
        public static void ShowError(Exception ex)
        {
            ShowError(mainForm, ex);
        }

        /// <summary>
        /// Exibe um erro
        /// </summary>
        /// <param name="owner">Form owner</param>
        /// <param name="ex">Excetpion à ser exibida</param>
        public static void ShowError(IWin32Window owner, Exception ex)
        {
            string message = ex.ToString();
            MessageBox.Show(owner, message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        /// Execute uma action, com try/catch e Cursors
        /// </summary>
        /// <param name="owner">Form parent</param>
        /// <param name="action">Actiin à ser executada</param>
        public static void Execute(Form owner, Action action)
        {
            try
            {
                owner.Cursor = Cursors.WaitCursor;
                action();
                owner.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                owner.Cursor = Cursors.Default;
                ShowError(owner, ex);
            }
        }

        /// <summary>
        /// Execute uma func, com try/catch e Cursors
        /// </summary>
        /// <typeparam name="T">Toipo de retorno</typeparam>
        /// <param name="owner">Form parent</param>
        /// <param name="func">Func à ser executada</param>
        /// <returns>Tipo de retorno</returns>
        public static T Execute<T>(Form owner, Func<T> func)
        {
            T ret = default(T);
            try
            {
                owner.Cursor = Cursors.WaitCursor;
                ret = func();
                owner.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                owner.Cursor = Cursors.Default;
                ShowError(owner, ex);
            }

            return ret;
        }
    }
}