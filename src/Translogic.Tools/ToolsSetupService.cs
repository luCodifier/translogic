﻿namespace Translogic.Tools
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;

    /// <summary>
    /// Serviço de configuração do cterunner
    /// </summary>
    public class ToolsSetupService
    {
        private const string ClassName = "ToolsSetupService";

        /// <summary>
        /// Método chamado na inicialização do serviço
        /// </summary>
        public void Start()
        {
            try
            {
                TranslogicStarter.Initialize();
                TranslogicStarter.SetupForJobRunner();
                TranslogicContainer container = TranslogicStarter.Container;
                RegistrarComponentes(TranslogicStarter.Container);
            }
            catch (Exception ex)
            {
                Sis.LogException(ex);
            }
        }

        /////// <summary>
        /////// Inicializa com as variaveis do ambiente (arquivo de configuração)
        /////// </summary>
        ////protected void Initialize()
        ////{
        ////    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        ////    _senderConfigService = Boolean.Parse(config.AppSettings.Settings["SenderConfigService"].Value);
        ////    _receiverConfigService = Boolean.Parse(config.AppSettings.Settings["ReceiverConfigService"].Value);
        ////    _senderInterfaceSapService = Boolean.Parse(config.AppSettings.Settings["SenderInterfaceSapService"].Value);
        ////    _importerPdfFileService = Boolean.Parse(config.AppSettings.Settings["ImporterPdfFileService"].Value);
        ////    _importerXmlFileService = Boolean.Parse(config.AppSettings.Settings["ImporterXmlFileService"].Value);
        ////    _senderEmailService = Boolean.Parse(config.AppSettings.Settings["SenderEmailService"].Value);
        ////    _efetuarLimpezaDiretorioService = Boolean.Parse(config.AppSettings.Settings["EfetuarLimpezaDiretorioService"].Value);
        ////}

        private void RegistrarComponentes(TranslogicContainer container)
        {
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFA").LifeStyle.PerThread);
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteCancelamentoCommand>().Named("EFC").LifeStyle.PerThread);
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteInutilizacaoCommand>().Named("EFI").LifeStyle.PerThread);
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteComplementoCommand>().Named("EFT").LifeStyle.PerThread);
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCorrecaoCteCommand>().Named("ECC").LifeStyle.PerThread);

            // // // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoContribuinteCommand>().Named("EFN").LifeStyle.PerThread);
            // // // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoNContribuinteCommand>().Named("EFR").LifeStyle.PerThread);

            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFN").LifeStyle.PerThread);
            // container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFR").LifeStyle.PerThread);

            // container.Register(Component.For<SenderQueueManagerService>().LifeStyle.PerThread);
            // container.Register(Component.For<ReceiverManagerService>().LifeStyle.PerThread);
            // container.Register(Component.For<SenderManagerInterfaceSapService>().LifeStyle.PerThread);
            // container.Register(Component.For<ImporterPdfFileManagerService>().LifeStyle.PerThread);
            // container.Register(Component.For<ImporterXmlFileManagerService>().LifeStyle.PerThread);
            // container.Register(Component.For<SenderEmailManagerService>().LifeStyle.PerThread);
            // container.Register(Component.For<CteRunnerLogService>().ImplementedBy<CteRunnerLogService>());
            // container.Register(Component.For<ICteRunnerLogRepository>().ImplementedBy<CteRunnerLogRepository>());
            // container.Register(Component.For<DirectoryFileManagerService>());
        }
    }
}