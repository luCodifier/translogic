properties {
	$version = $Null;
	$environment = $Null;
	$platform = $Null;
	$startServices = $true;
	$company = "ALL - Am�rica Latina Log�stica S/A";
	$copyright = "Copyright � $company 2012"
	$config = 'Release'
	$rootDir = resolve-path '..\..'
	$srcDir = resolve-path '..'
	$toolsDir = join-path $rootDir 'tools'
	$framework_dir = "$env:systemroot\microsoft.net\framework\v4.0.30319"
	$assemblyFile = 'Properties\AssemblyInfo.cs'
	$environmentalConfigFile = join-path $srcDir 'BuildScripts\Environments.config'
	$applicationsConfigFile = join-path $srcDir 'BuildScripts\Applications.config'
}

task default -depends Init, BuildSolutionProjects, ConfigEnvironments, ExtractCompiledAssemblies

task Init -depends GenerateAssemblyInfos {
	Write-Host "Build Translogic - Vers�o $version - $environment - $platform"
	Write-Host "Path: $env:path"

	$outputDir = GetArtifactsDir;

	if(test-path $outputDir) {
		rm -re -fo $outputDir
	}

	mkdir $outputDir
}

task GenerateAssemblyInfos {
	<#-----------------------------------------------------------------------------------------------------------------
	DEVE-SE VARRER OS DIRETORIOS DENTRO DO ROOT(SRC) PARA PROCURAR OS PROJETOS QUE SER�O ALTERADOS OS AssemblyInfo'S 
	-------------------------------------------------------------------------------------------------------------------#>
	$folders = (Get-Childitem $srcDir | ? {$_.Attributes -eq "Directory"}) 
	foreach ($i in $folders)
	{
		$projectName =  $i.Name + ".csproj"
		$projectPath = join-path $i.FullName $projectName
		if (Test-Path $projectPath)
		{
			$projectAsmInfo = join-path $i.FullName $assemblyFile
			if (Test-Path $projectAsmInfo)
			{
				<#------------------------------------------------------------------------------
				QUANDO ENCONTRADO O ASSEMBLYINFO DEVE GERAR COM A VERS�O PASSADA POR PAR�METRO
				------------------------------------------------------------------------------#>
				Generate-Assembly-Info `
					-file $projectAsmInfo `
					-title $i.Name `
					-product 'TRANSLOGIC' `
					-version $version `
					-company $company `
					-copyright $copyright `
			}
		}
	}
}

task BuildSolutionProjects {
	$outputDir = GetArtifactsDir;
	
	<#-----------------------------------------------------------------------------------------------------------------
	DEVE-SE VARRER OS DIRETORIOS DENTRO DO ROOT(SRC) PARA PROCURAR OS PROJETOS QUE SER�O ALTERADOS OS AssemblyInfo'S 
	-------------------------------------------------------------------------------------------------------------------#>
	$folders = (Get-Childitem $srcDir | ? {$_.Attributes -eq "Directory"}) 
	foreach ($i in $folders)
	{
		$projectName =  $i.Name + ".csproj"
		$projectPath = join-path $i.FullName $projectName
		
		if (Test-Path $projectPath)
		{
			$output = join-path $outputDir $i.Name
			$projectWebConfig = join-path $i.FullName "web.config"
			
			if (Test-Path $projectWebConfig){
				$folderNameTemp = $i.Name + "-Temp"
				$outputTemp = join-path $outputDir $folderNameTemp
				$outputWeb = join-path $outputTemp ('_PublishedWebsites/' + $i.Name)
				
				$outputTemp = $outputTemp + "/"
				
				if(test-path $output) {
					rm -re -fo $output
				}

				mkdir $output
				
				exec { msbuild $projectPath /t:Rebuild /p:OutDir=$outputTemp /p:Configuration=$config /nologo /maxcpucount /clp:ErrorsOnly }
				exec { & $framework_dir\aspnet_compiler.exe -v / -u -p $outputWeb $output }

				rm -re -fo $outputTemp
			}else{
				<#$projectAppConfig = join-path $i.FullName "app.config"
				if (Test-Path $projectAppConfig){
				#>
				$output = $output + "/"
					exec { msbuild $projectPath /t:Rebuild /p:OutDir=$output /p:Configuration=$config /nologo /maxcpucount /clp:ErrorsOnly }
				<#}#>
			}
		}		
	}
}

task ConfigEnvironments{
	
	$artifactsDir = GetArtifactsDir;
	
	<#-----------------------------------------------------------------------------------------------------------------
	DEVE-SE VARRER OS DIRETORIOS DENTRO DO ROOT(SRC) PARA PROCURAR OS PROJETOS QUE SER�O ALTERADOS OS AssemblyInfo'S 
	-------------------------------------------------------------------------------------------------------------------#>
	$folders = (Get-Childitem $artifactsDir | ? {$_.Attributes -eq "Directory"}) 
	foreach ($i in $folders)
	{
		$configFileTmp = $Null;
		
		$configPath =  $i.Name + "*.config"
		$configFiles = Get-ChildItem $i.FullName | Where-Object {$_.name -eq "web.config" -or $_.name -like $configPath}
		if ($configFiles -ne $Null)
		{
			foreach($config in $configFiles)
			{
				if (($config -ne $Null))
				{
					Write-host "---------------------------------------------------------------"  -fore GREEN;
					Write-host "Verificando arquivo de configura��o: " + $config.FullName
					$configFile = join-path $i.FullName $config.Name
					ChangeConfigEnvironment -newEnvironment $environment -configFilePath $configFile
				}
			}
		}
	}
}

task ExtractCompiledAssemblies{
	
	$xmlApplications = New-Object XML
	$xmlApplications.Load($applicationsConfigFile)
	
	$artifactsDir = GetArtifactsDir;
	
	<#-----------------------------------------------------------------------------------------------------------------
	DEVE-SE VARRER OS DIRETORIOS DOS ARTEFATOS PARA PEGAR APENAS OS ASSEMBLY'S DOS PROJETOS COMPILADOS
	-------------------------------------------------------------------------------------------------------------------#>
	$compiledPath = join-path $artifactsDir 'DLL'
	mkdir $compiledPath
	
	$folders = (Get-Childitem $artifactsDir | ? {$_.Attributes -eq "Directory"}) 
	foreach ($i in $folders)
	{
		$dllName = $i.Name + ".dll"
		$dllPath =  join-path $i.FullName $dllName
		if (Test-Path $dllPath)
		{
			cp $dllPath $compiledPath -fo;
		}
		else
		{
			$dllName = "bin\" + $i.Name + ".dll"
			$dllPath =  join-path $i.FullName $dllName
			if (Test-Path $dllPath)
			{
				cp $dllPath $compiledPath -fo;
			}
		}
		
		<#-----------------------------------------------------------------------------------------------------------------
		VERIFICA SE O PROJETO � DA LISTA DE APLICA��ES, SE N�O FOR DEVE SER APAGADO
		-------------------------------------------------------------------------------------------------------------------#>
		$folderName = $i.Name
		if ($folderName -ne "DLL"){
			$app = $xmlApplications.SelectSingleNode("/applications/app[@name='$folderName']")
			if ($app -eq $Null)
			{
				Write-host "Apagando pasta $folderName"
				rm -re -fo $i.FullName
			}
		}
	}
}

function ChangeConfigEnvironment {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$configFilePath = $(throw "configFile is a required parameter")
	)
	
	$configFile = Get-Item $configFile
	
	Write-Host "Obtendo tipo de ambiente no arquivo $configFilePath"
	$result = GetEnvironmentTypeInConfig -configFile $configFilePath
	
	if ($result -ne $Null)
	{
		$generatedConfigFilePath = $Null
		Write-Host "Gerando arquivo de configura��o do ambiente: $result"  -fore GREEN;
		switch ($result)
		{
			"web" {
				$sampleFile = join-path $srcDir "Translogic.Config/Web/Web.config"
				Copy-Item $sampleFile $configFile 
				ChangeConfigWeb -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"soa" {
				$sampleFile = join-path $srcDir "Translogic.Config/Soa/Web.config"
				Copy-Item $sampleFile $configFile 
				ChangeConfigSoa -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"unittests" {
				$sampleFile = join-path $srcDir "Translogic.Config/Tests/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigUnitTests -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"jobrunner.Server" {
				$sampleFile = join-path $srcDir "Translogic.Config/JobRunner/Server/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigJobRunnerServer -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"jobrunner.Client" {
				$sampleFile = join-path $srcDir "Translogic.Config/JobRunner/Client/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigJobRunnerClient -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"cterunner" {
				$sampleFile = join-path $srcDir "Translogic.Config/CteRunner/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigCteRunner -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"antt.Client" {
				$sampleFile = join-path $srcDir "Translogic.Config/ANTT/Client/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigAnttClient -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"equipamentosCampo.recebimentoArquivoService" {
				$sampleFile = join-path $srcDir "Translogic.Config/EquipamentosCampo/RecebimentoArquivo/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigEquipCampoRecebimentoArquivoService -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
			"equipamentosCampo.socketClient" {
				$sampleFile = join-path $srcDir "Translogic.Config/EquipamentosCampo/SocketClient/App.config"
				Copy-Item $sampleFile $configFile
				ChangeConfigEquipCampoSocketClient -newEnvironment $newEnvironment -generatedConfigFilePath $configFile
			}
		}
	}
}

function ChangeConfigWeb {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para WEB no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	
	<#
	<castle>
		<include uri="file://bin/Config/container.config" />
		<components>
			<component id="server.settings">
				<parameters>
					<UrlTranslogicLegado>http://190.1.0.228/translogic/</UrlTranslogicLegado>
					<SufixoServidor>.all-logistica.net</SufixoServidor>
					<GarantirUsoDoSufixoServidor>true</GarantirUsoDoSufixoServidor>
					<UrlSiv>http://190.1.2.238/</UrlSiv>
					<!-- PRODU��O - <UrlSiv>http://190.1.1.3/</UrlSiv>-->
					<ServletSiv>login.do?perform=iniciar</ServletSiv>
                    <Gis>gis.do?perform=iniciar</Gis>
				</parameters>
			</component>
		</components>
	</castle>
	#>
	$web = $currentEnv.SelectSingleNode("web")
	
	if ($web -ne $null)
	{
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/UrlTranslogicLegado" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/UrlTranslogicLegado" 
		
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/SufixoServidor" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/SufixoServidor" 
						
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/GarantirUsoDoSufixoServidor" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/GarantirUsoDoSufixoServidor" 
		
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/UrlSiv" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/UrlSiv" 
		
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/ServletSiv" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/ServletSiv" 
		
		ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/Gis" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/Gis" 
	}
	
	ConfigureConnectionStrings -xmlElement $currentEnv -generatedConfigFilePath $generatedConfigFilePath
}

function ChangeConfigSoa {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para SOA no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$soa = $currentEnv.SelectSingleNode("soa")
	
	if ($soa -ne $null)
	{
		<#ChangeConfigXml -fromXmlNode $web -toXmlFile $generatedConfigFilePath `
						-fromXpath "//server_settings/UrlTranslogicLegado" `
						-toXpath "/configuration/castle/components/component[@id='server.settings']/parameters/UrlTranslogicLegado" #>
	}
	
	ConfigureConnectionStrings -xmlElement $currentEnv -generatedConfigFilePath $generatedConfigFilePath
}

function ChangeConfigUnitTests {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para testes unit�rios no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$unittests = $currentEnv.SelectSingleNode("unittests")
	
	if ($unittests -ne $null)
	{
		<#
		<configuration>
			<appSettings>
				<add key="CaminhoPlanilhaConducaoPadrao" value="c:\Temp\"/>
			</appSettings>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $unittests -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/CaminhoPlanilhaConducaoPadrao" `
						-toXpath "/configuration/appSettings/add[@key='CaminhoPlanilhaConducaoPadrao']" `
						-toAttr "value"
	}
	
	ConfigureConnectionStrings -xmlElement $currentEnv -generatedConfigFilePath $generatedConfigFilePath
}

function ChangeConfigJobRunnerServer {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para testes unit�rios no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$jobrunnerServer = $currentEnv.SelectSingleNode("jobrunner.Server")
	
	if ($jobrunnerServer -ne $null)
	{
		<#
		<configuration>
			<system.serviceModel>
				<services>
					<service name="Translogic.Modules.Core.Infrastructure.Services.JobRunnerService">
						<endpoint address="net.pipe://localhost/Translogic/JobRunner" binding="netNamedPipeBinding" bindingConfiguration="default" contract="Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService"/>
					</service>
				</services>
			</system.serviceModel>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $jobrunnerServer -toXmlFile $generatedConfigFilePath `
						-fromXpath "//system.serviceModel/services/service[@name='Translogic.Modules.Core.Infrastructure.Services.JobRunnerService']/endpoint[@contract='Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService']" `
						-toXpath "/configuration/system.serviceModel/services/service[@name='Translogic.Modules.Core.Infrastructure.Services.JobRunnerService']/endpoint[@contract='Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService']" `
						-toAttr "address"
	}
	
	ConfigureConnectionStrings -xmlElement $currentEnv -generatedConfigFilePath $generatedConfigFilePath
}

function ChangeConfigJobRunnerClient {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para testes unit�rios no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$jobrunnerClient = $currentEnv.SelectSingleNode("jobrunner.Client")
	
	if ($jobrunnerClient -ne $null)
	{
		<#
		<configuration>
			<system.serviceModel>
				<client>
					<endpoint address="net.pipe://localhost/Translogic/JobRunner" binding="netNamedPipeBinding" bindingConfiguration="default" contract="Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService"/>
				</client>
			</system.serviceModel>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $jobrunnerClient -toXmlFile $generatedConfigFilePath `
						-fromXpath "//system.serviceModel/client/endpoint[@contract='Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService']" `
						-toXpath "/configuration/system.serviceModel/client/endpoint[@contract='Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService']" `
						-toAttr "address"
	}
}

function ChangeConfigCteRunner {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para service do CteRunner no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$recArquivo = $currentEnv.SelectSingleNode("cterunner")
	
	if ($recArquivo -ne $null)
	{
		<#
		<configuration>
			<appSettings>
				<add key="DiretorioTag" value="C:\Temp\MonitorTag\"/>
				<add key="DiretorioTagProcessado" value="C:\Temp\MonitorTag\Processados\"/>
				<add key="DiretorioDetac" value="C:\Temp\MonitorDetac\"/>
				<add key="DiretorioDetacProcessado" value="C:\Temp\MonitorDetac\Processados\"/>
				<add key="DiretorioColdWheel" value="C:\Temp\MonitorColdWheel\"/>
				<add key="DiretorioColdWheelProcessado" value="C:\Temp\MonitorColdWheel\Processados\"/>
				<add key="IntervaloLeitura" value="15"/>
			</appSettings>
			<system.serviceModel>
				<client>
					<endpoint address="net.pipe://localhost/Translogic/JobRunner" binding="netNamedPipeBinding" bindingConfiguration="default" contract="Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService"/>
				</client>
			</system.serviceModel>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/QuantidadeDiasParaLimpeza" `
						-toXpath "/configuration/appSettings/add[@key='QuantidadeDiasParaLimpeza']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IndAmbienteSefaz" `
						-toXpath "/configuration/appSettings/add[@key='IndAmbienteSefaz']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimerFtpEmSegundos" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimerFtpEmSegundos']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimerEnvioEmSegundos" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimerEnvioEmSegundos']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimerRecebimentoEmSegundos" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimerRecebimentoEmSegundos']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimerImportadorEmSegundos" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimerImportadorEmSegundos']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimerInterfaceSap" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimerInterfaceSap']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloImportadorXMLEmSegundos" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloImportadorXMLEmSegundos']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloSenderEmail" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloSenderEmail']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/LogInformacao" `
						-toXpath "/configuration/appSettings/add[@key='LogInformacao']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/LogErro" `
						-toXpath "/configuration/appSettings/add[@key='LogErro']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/ClientSettingsProvider.ServiceUri" `
						-toXpath "/configuration/appSettings/add[@key='ClientSettingsProvider.ServiceUri']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/SenderConfigService" `
						-toXpath "/configuration/appSettings/add[@key='SenderConfigService']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/ReceiverConfigService" `
						-toXpath "/configuration/appSettings/add[@key='ReceiverConfigService']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/SenderInterfaceSapService" `
						-toXpath "/configuration/appSettings/add[@key='SenderInterfaceSapService']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/ImporterPdfFileService" `
						-toXpath "/configuration/appSettings/add[@key='ImporterPdfFileService']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/ImporterXmlFileService" `
						-toXpath "/configuration/appSettings/add[@key='ImporterXmlFileService']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/SenderEmailService" `
						-toXpath "/configuration/appSettings/add[@key='SenderEmailService']" `
						-toAttr "value"
		
		ConfigureConnectionStrings -xmlElement $currentEnv -generatedConfigFilePath $generatedConfigFilePath
	}
}

function ChangeConfigAnttClient{
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config do client da ANTT no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$recArquivo = $currentEnv.SelectSingleNode("antt.Client")
	
	if ($recArquivo -ne $null)
	{
		<#
		<configuration>
			<castle>
				<components>
					<component id="service.settings">
						<parameters>
							<DesempenhoService>http://localhost:2343/ANTT/Desempenho.svc</DesempenhoService>
							<DistanciaService>http://localhost:2343/ANTT/Distancia.svc</DistanciaService>
							<FluxoCalculoService>http://localhost:2343/ANTT/FluxoCalculo.svc</FluxoCalculoService>
							<FluxoDistanciaService>http://localhost:2343/ANTT/FluxoDistancia.svc</FluxoDistanciaService>
							<FluxoExportacaoService>http://localhost:2343/ANTT/FluxoExportacao.svc</FluxoExportacaoService>
							<FluxoImportacaoService>http://localhost:2343/ANTT/FluxoImportacao.svc</FluxoImportacaoService>
							<FluxoIntercambioService>http://localhost:2343/ANTT/FluxoIntercambio.svc</FluxoIntercambioService>
							<FluxoService>http://localhost:2343/ANTT/Fluxo.svc</FluxoService>
							<GrupoContaService>http://localhost:2343/ANTT/GrupoConta.svc</GrupoContaService>
							<IntercambioService>http://localhost:2343/ANTT/Intercambio.svc</IntercambioService>
							<PassagemService>http://localhost:2343/ANTT/Passagem.svc</PassagemService>
							<RelatorioTkuService>http://localhost:2343/ANTT/RelatorioTku.svc</RelatorioTkuService>
							<RotaService>http://localhost:2343/ANTT/Rota.svc</RotaService>
							<VersaoService>http://localhost:2343/ANTT/Versao.svc</VersaoService>
						</parameters>
					</component>
				</components>
			</castle>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/DesempenhoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/DesempenhoService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/DistanciaService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/DistanciaService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoCalculoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/FluxoCalculoService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoDistanciaService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/FluxoDistanciaService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoExportacaoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/DesempenhoService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoImportacaoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/FluxoImportacaoService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoIntercambioService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/FluxoIntercambioService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/FluxoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/FluxoService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/GrupoContaService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/GrupoContaService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/IntercambioService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/IntercambioService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/PassagemService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/PassagemService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/RelatorioTkuService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/RelatorioTkuService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/RotaService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/RotaService" 
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//castle/components/component[@id='service.settings']/parameters/VersaoService" `
						-toXpath "/configuration/castle/components/component[@id='service.settings']/parameters/VersaoService" 
		
	}
}

function ChangeConfigEquipCampoRecebimentoArquivoService {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para service de recebimento de arquivos do EquipamentosCampo no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$recArquivo = $currentEnv.SelectSingleNode("equipamentosCampo.recebimentoArquivoService")
	
	if ($recArquivo -ne $null)
	{
		<#
		<configuration>
			<appSettings>
				<add key="DiretorioTag" value="C:\Temp\MonitorTag\"/>
				<add key="DiretorioTagProcessado" value="C:\Temp\MonitorTag\Processados\"/>
				<add key="DiretorioDetac" value="C:\Temp\MonitorDetac\"/>
				<add key="DiretorioDetacProcessado" value="C:\Temp\MonitorDetac\Processados\"/>
				<add key="DiretorioColdWheel" value="C:\Temp\MonitorColdWheel\"/>
				<add key="DiretorioColdWheelProcessado" value="C:\Temp\MonitorColdWheel\Processados\"/>
				<add key="IntervaloLeitura" value="15"/>
			</appSettings>
			<system.serviceModel>
				<client>
					<endpoint address="net.pipe://localhost/Translogic/JobRunner" binding="netNamedPipeBinding" bindingConfiguration="default" contract="Translogic.Modules.Core.Interfaces.Jobs.Service.IJobRunnerService"/>
				</client>
			</system.serviceModel>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioTag" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioTag']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioTagProcessado" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioTagProcessado']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioDetac" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioDetac']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioDetacProcessado" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioDetacProcessado']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioColdWheel" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioColdWheel']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioColdWheelProcessado" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioColdWheelProcessado']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloLeitura" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloLeitura']" `
						-toAttr "value"
		
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//system.serviceModel/client/endpoint[@name='EquipamentoCampoService']" `
						-toXpath "/configuration/system.serviceModel/client/endpoint[@name='EquipamentoCampoService']" `
						-toAttr "address"
	}
}

function ChangeConfigEquipCampoSocketClient {
	param(
		[string]$newEnvironment = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	Write-Host "Configurando arquivo de config para service de recebimento de arquivos do EquipamentosCampo no ambiente $newEnvironment"
	$currentEnv = GetConfigEnvironment($newEnvironment)
	$recArquivo = $currentEnv.SelectSingleNode("equipamentosCampo.socketClient")
	
	if ($recArquivo -ne $null)
	{
		<#
		<configuration>
			<appSettings>
				<add key="DiretorioColdWheel" value="C:\Temp\MonitorColdWheel\"/>
				<add key="DiretorioColdWheelProcessado" value="C:\Temp\MonitorColdWheel\Processados\"/>
				<add key="DiretorioColdWheelBackup" value="C:\Temp\MonitorColdWheel\Backup\"/>
				<add key="IntervaloTimer" value="3600"/>
				<add key="EmailsErro" value="marcos.yano;cesar.tomba;gerson.perotoni"/>
				<add key="ClientSettingsProvider.ServiceUri" value=""/>
			</appSettings>
			<system.serviceModel>
				<client>
					<endpoint name="EquipamentoCampoService" address="http://localhost/Translogic.SOA/EquipamentosCampo/EquipamentosCampoService.svc" binding="basicHttpBinding" behaviorConfiguration="default" contract="Translogic.Modules.Core.Interfaces.EquipamentosCampo.Interfaces.IEquipamentosCampoService" bindingConfiguration="InsecureHttp"/>
				</client>
			</system.serviceModel>
		</configuration>
		#>
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioColdWheel" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioColdWheel']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioColdWheelProcessado" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioColdWheelProcessado']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/DiretorioColdWheelBackup" `
						-toXpath "/configuration/appSettings/add[@key='DiretorioColdWheelBackup']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/IntervaloTimer" `
						-toXpath "/configuration/appSettings/add[@key='IntervaloTimer']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/EmailsErro" `
						-toXpath "/configuration/appSettings/add[@key='EmailsErro']" `
						-toAttr "value"
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//appSettings/ClientSettingsProvider.ServiceUri" `
						-toXpath "/configuration/appSettings/add[@key='ClientSettingsProvider.ServiceUri']" `
						-toAttr "value"
		
		ChangeConfigXml -fromXmlNode $recArquivo -toXmlFile $generatedConfigFilePath `
						-fromXpath "//system.serviceModel/client/endpoint[@name='EquipamentoCampoService']" `
						-toXpath "/configuration/system.serviceModel/client/endpoint[@name='EquipamentoCampoService']" `
						-toAttr "address"
	}
}

function ConfigureConnectionStrings {
	param(
		[XML.XmlElement]$xmlElement = $(throw "newEnvironment is a required parameter."),
		[string]$generatedConfigFilePath = $(throw "generatedConfigFilePath is a required parameter.")
	)	
	<#
	<connectionStrings>
		<add name="tl_connection" connectionString="Data Source=translogic_hom;User ID=translogic;Password=engesis;"/>
		<add name="act_connection" connectionString="Data Source=acthom;User ID=restricaot;Password=restricaot;" />
		<add name="antt_connection" connectionString="Data Source=translogic_hom;User ID=antt;Password=ntt43;" />
		<add name="siv_connection" connectionString="Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=190.1.15.128)(PORT=1521))(CONNECT_DATA=(SERVER=dedicated)(SID=sivdev)));User ID=siv;Password=siv;"/>
		<add name="sispat_connection" connectionString="Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP) (HOST = 190.1.0.90) (PORT = 1521)))(CONNECT_DATA = (SID = alltrd))) ;User ID=sispat;Password=sispat;" />
		<add name="essbase_connection" connectionString="Data Source=essbase;User ID=essbase2;Password=essbase;" />
		<add name="sap_consulta_connection" connectionString="Data Source=sap_prd;User ID=sap_consulta;Password=c0ns1lt4;"/>
		<add name="sap_mpr_connection" connectionString="Data Source=biprd_149;User ID=sap_conesulta;Password=c0ns1lt4;" />
		<add name="config_connection" connectionString="Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=190.1.2.228)(PORT=1521))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=confighom)));User ID=config;Password=config;"/>
	</connectionStrings>
	#>
	$general = $xmlElement.SelectSingleNode("general")
	if ($general -ne $null)
	{
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/tl_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='tl_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/act_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='act_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/antt_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='antt_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/siv_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='siv_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/sispat_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='sispat_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/essbase_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='essbase_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/sap_consulta_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='sap_consulta_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/sap_mpr_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='sap_mpr_connection']" `
						-toAttr "connectionString"
		
		ChangeConfigXml -fromXmlNode $general -toXmlFile $generatedConfigFilePath `
						-fromXpath "//connectionStrings/config_connection" `
						-toXpath "/configuration/connectionStrings/add[@name='config_connection']" `
						-toAttr "connectionString"
	}
}

function Generate-Assembly-Info {
	param(
		[string]$clsCompliant = "true",
		[string]$title, 
		[string]$description, 
		[string]$company, 
		[string]$product, 
		[string]$copyright, 
		[string]$version,
		[string]$file = $(throw "file is a required parameter.")
	)
  
  $asmInfo = "using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#if !SILVERLIGHT
[assembly: SuppressIldasmAttribute()]
#endif
[assembly: CLSCompliantAttribute($clsCompliant )]
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute(""$title"")]
[assembly: AssemblyDescriptionAttribute(""$description"")]
[assembly: AssemblyCompanyAttribute(""$company"")]
[assembly: AssemblyProductAttribute(""$product"")]
[assembly: AssemblyCopyrightAttribute(""$copyright"")]
[assembly: AssemblyVersionAttribute(""$version"")]
[assembly: AssemblyInformationalVersionAttribute(""$version"")]
[assembly: AssemblyFileVersionAttribute(""$version"")]
[assembly: AssemblyDelaySignAttribute(false)]
"

	$dir = [System.IO.Path]::GetDirectoryName($file)
	
	if ([System.IO.Directory]::Exists($dir) -eq $false)
	{
		Write-Host "Creating directory $dir"
		
		[System.IO.Directory]::CreateDirectory($dir)
	}
	
	Write-Host "Generating assembly info file: $file"
	
	Write-Output $asmInfo > $file
}

function GetEnvironment {
	if ($environment -eq $Null) { return 'DEV'; }
	return $environment;
}

function GetArtifactsDir {
	$environment = GetEnvironment
	$output = join-path $rootDir "artifacts\$environment";
	return $output;
}

function GetConfigEnvironment ($newEnvironment) {
	
	$path = $environmentalConfigFile
	$xmlEnvironments = New-Object XML
	$xmlEnvironments.Load($path)
	
	$currentEnv = $xmlEnvironments.SelectSingleNode("/configuration/environment[@name='$newEnvironment']")
	
	if ($currentEnv -eq $null)
	{
		Write-Host "GetConfigEnvironment - N�o foi encontrado o ambiente '$newEnvironment'" -fore RED; 
		
		exit -1;
	}

	return $currentEnv;
}

function GetEnvironmentTypeInConfig {
	param(
		[string]$configFile = $(throw "configFile is a required parameter")
	)
	
	$xmlConfig = New-Object XML
	$xmlConfig.Load($configFile)
	
	$currentEnv = $xmlConfig.SelectSingleNode("/configuration/appSettings/add[@key='environmentType']")
	
	if ($currentEnv -eq $null)
	{
		Write-Host "GetEnvironmentTypeInConfig - N�o foi encontrado o tipo do ambiente no arquivo de configura��o" -fore RED; 
		return $Null;
	}

	return $currentEnv.GetAttribute("value");
}

function ChangeConfigXml {
	param(
		[XML.XmlElement]$fromXmlNode = $(throw "fromXmlNode is a required parameter."),
		[string]$toXmlFile = $(throw "toXmlFile is a required parameter."),
		[string]$fromXpath = $(throw "fromXpath is a required parameter."),
		[string]$toXpath = $(throw "toXpath is a required parameter."),
		[string]$toAttr
	)
	
	$configItem = $null
	$configItem = $fromXmlNode.SelectSingleNode($fromXpath)

	if ($configItem -eq $Null)
	{ 
		Write-Host $fromXmlNode.outerXml -fore yellow
		Write-Host "ChangeConfigXml - N�o foi encontrado o n� para '$fromXpath'" -fore RED; 
		
		exit -1;
	}
	
	<#Write-Host $configItem.InnerText -fore YELLOW; #>
	ChangeConfig `
		-file $toXmlFile `
		-xpath $toXpath `
		-attr $toAttr `
		-value $configItem.InnerText
}

function ChangeConfig {
	param(
		[string]$file = $(throw "file is a required parameter."),
		[string]$xpath = $(throw "xpath is a required parameter."), 
		[string]$attr, 
		[string]$value = $(throw "value is a required parameter.")
	)
	
	$xml = New-Object XML
	$xml.Load($file)
	
	$configItem = $null

	if ($file.EndsWith(".csproj"))
	{
		$ns = New-Object Xml.XmlNamespaceManager $xml.NameTable
		$ns.AddNamespace( "e", "http://schemas.microsoft.com/developer/msbuild/2003" )
		
		$configItem = $xml.SelectSingleNode($xpath, $ns)
	}
	else
	{
		$configItem = $xml.SelectSingleNode($xpath)
	}

	if ($configItem -eq $Null)
	{ 
		Write-Host "ChangeConfig - N�o foi encontrado o n� para '$xpath'" -fore RED; 
		
		exit -1;
	}

	if ($attr -ne '')
	{
		if ($configItem.HasAttribute($attr))
		{ 
			$configItem.SetAttribute($attr, $value);
		}
		else
		{
			Write-Host "ChangeConfig - O atributo '$attr' n�o foi encontrado" -fore RED; 
			
			exit -1;
		}
	}
	else
	{
		$configItem.InnerText = $value
	}
	
	$xml.Save($file)
}