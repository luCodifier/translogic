﻿namespace Translogic.Modules.Core.JobRunner
{
	using System;
	using System.Diagnostics;
	using System.Linq;
	using Interfaces.Jobs;

	internal class Program
	{
		#region MÉTODOS ESTÁTICOS

		private static void Main(string[] args)
		{
			if (args.Length == 0)
			{
				ExibirExemplos();

				return;
			}

			string jobKey = args[0];
			string[] jobArgs = args.Skip(1).ToArray();

			try
			{
				JobRunnerClient jobRunnerClient = new JobRunnerClient();

				LogarEvento(EventLogEntryType.Information, "Chamando job '{0}'", jobKey);
				
				try
				{
					jobRunnerClient.Execute(jobKey, jobArgs);

					LogarEvento(EventLogEntryType.Information, "Job '{0}' executado com sucesso", jobKey);
				}
				catch (Exception ex)
				{
					LogarEvento(EventLogEntryType.Error, "Falha na execução do job {0}. \n{1}\n{2}", jobKey, ex.Message, ex.StackTrace);
                    jobRunnerClient.Abort();
					Environment.Exit(101);
				}

                jobRunnerClient.Close();
			}
			catch (Exception ex)
			{
				LogarEvento(EventLogEntryType.Error, "Falha ao tentar executar job {0}. \n{1}\n{2}", jobKey, ex.Message, ex.StackTrace);

				Environment.Exit(101);
			}
		}

		private static void LogarEvento(EventLogEntryType logEntryType, string format, params string[] args)
		{
            if (!EventLog.SourceExists("Translogic"))
            {
                EventLog.CreateEventSource("Translogic", "Translogic");
            }

            EventLog.WriteEntry("Translogic", string.Format(format, args), logEntryType);
		}

		private static void ExibirExemplos()
		{
			Console.WriteLine("É necessário passar por argumentos a chave do job a ser executado.");
			Console.WriteLine("Exemplo: JobRunner.exe core.testejob");
			Console.WriteLine("Exemplo: JobRunner.exe core.testejob arg1 arg2");

			Environment.Exit(101);
		}

		#endregion
	}
}