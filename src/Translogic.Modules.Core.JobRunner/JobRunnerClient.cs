namespace Translogic.Modules.Core.JobRunner
{
	using System.ServiceModel;
	using Interfaces.Jobs.Service;

	public class JobRunnerClient : ClientBase<IJobRunnerService>, IJobRunnerService
	{
		#region IJobRunnerService MEMBROS

		public bool Execute(string jobKey, params string[] args)
		{
			return Channel.Execute(jobKey, args);
		}

		#endregion
	}
}