namespace Translogic.Modules.Core.Tests.Domain.Services.Edi
{
    using System;
    using NUnit.Framework;
    using Translogic.Tests;
    using Translogic.Modules.Core.Domain.Services.Edi;

    [TestFixture]
    public class EdiErroServiceTestCase : BaseTranslogicContainerTestCase
    {
        private EdiErroService _service;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<EdiErroService>();
        }
        #endregion

        [Test]
        public void CarregarErros()
        {
            var dataInicial = Convert.ToDateTime("21/01/2019");
            var dataFinal = Convert.ToDateTime("21/01/2019");
            _service.CarregarErros(dataInicial, dataFinal);
        }
    }
}
