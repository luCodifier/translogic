using NUnit.Framework;
using Translogic.Modules.Core.Domain.Services.Edi;
using Translogic.Tests;

namespace Translogic.Modules.Core.Tests.Domain.Services.Edi
{
    [TestFixture]
    public class EdiReadNfeServiceTestCase : BaseTranslogicContainerTestCase
    {
        private EdiReadNfeService _service;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<EdiReadNfeService>();
        }
        #endregion

        [Test]
        public void CarregarErros()
        {
            var hora = 10;
            var order = "ASC";
             
            _service.ObterEmailsRealizarProcessamento(hora, order);
          
        }
    }
}
