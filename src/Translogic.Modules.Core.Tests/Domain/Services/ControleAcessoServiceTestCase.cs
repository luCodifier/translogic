using Translogic.Modules.Core.Domain.Model.Diversos;

namespace Translogic.Modules.Core.Tests.Domain.Services
{
	using System;
	using System.Collections.Generic;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Acesso.Repositories;
	using Core.Domain.Model.Codificador.Repositories;
	using Core.Domain.Services.Acesso;
	using Moq;
	using NUnit.Framework;

	[TestFixture]
	public class ControleAcessoServiceTestCase
	{
		#region SETUP/TEARDOWN

		[SetUp]
		public void Setup()
		{
			usuarioRepositoryMock = new Mock<IUsuarioRepository>(MockBehavior.Strict);
			logRepositoryMock = new Mock<ILogUsuarioRepository>(MockBehavior.Strict);
			eventoRepositoryMock = new Mock<IEventoUsuarioRepository>(MockBehavior.Strict);
			tokenAcessoRepositoryMock = new Mock<ITokenAcessoRepository>(MockBehavior.Strict);
			configuracaoTranslogicRepositoryMock = new Mock<IConfiguracaoTranslogicRepository>(MockBehavior.Strict);
			sessaoExpiradaRepositoryMock = new Mock<ISessaoExpiradaRepository>(MockBehavior.Strict);
			grupoUsuarioRepositoryMock = new Mock<IGrupoUsuarioRepository>(MockBehavior.Strict);
			agrupamentoUsuarioRepositoryMock = new Mock<IAgrupamentoUsuarioRepository>(MockBehavior.Strict);
			usuarioConfiguracaoRepositoryMock = new Mock<IUsuarioConfiguracaoRepository>(MockBehavior.Strict);

			service = new ControleAcessoService(
												usuarioRepositoryMock.Object, 
												logRepositoryMock.Object,
			                                    eventoRepositoryMock.Object, 
												tokenAcessoRepositoryMock.Object, 
												configuracaoTranslogicRepositoryMock.Object,
												sessaoExpiradaRepositoryMock.Object,
												agrupamentoUsuarioRepositoryMock.Object,
												grupoUsuarioRepositoryMock.Object,
												usuarioConfiguracaoRepositoryMock.Object
												);
		}

		#endregion

		private ControleAcessoService service;
		private Mock<IUsuarioRepository> usuarioRepositoryMock;
		private Mock<ILogUsuarioRepository> logRepositoryMock;
		private Mock<IEventoUsuarioRepository> eventoRepositoryMock;
	    private Mock<ITokenAcessoRepository> tokenAcessoRepositoryMock;
		private Mock<IConfiguracaoTranslogicRepository> configuracaoTranslogicRepositoryMock;
		private Mock<ISessaoExpiradaRepository> sessaoExpiradaRepositoryMock;
		private Mock<IAgrupamentoUsuarioRepository> agrupamentoUsuarioRepositoryMock;
		private Mock<IUsuarioConfiguracaoRepository> usuarioConfiguracaoRepositoryMock;
		private Mock<IGrupoUsuarioRepository> grupoUsuarioRepositoryMock;
		
		

		[Test]
		public void ValidarUsuario_QuandoCredenciaisValidas_RetornarSucesso()
		{
			var eventoUsuario = new EventoUsuario {Codigo = EventoUsuarioCodigo.LoginEfetuadoComSucesso};
			var logUsuario = new LogUsuario {Evento = eventoUsuario};
			var usuario = new Usuario {Senha = "a1"};

			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(usuario);
			usuarioRepositoryMock.Setup(rep => rep.Atualizar(usuario)).Returns(usuario);

			eventoRepositoryMock.Setup(rep => rep.ObterPorCodigo(EventoUsuarioCodigo.LoginEfetuadoComSucesso)).Returns(
				eventoUsuario).AtMostOnce();

			logRepositoryMock.Setup(rep => rep.ObterUltimoLogPorUsuario(usuario)).Returns((LogUsuario) null);
			logRepositoryMock.Setup(
				rep => rep.Inserir(
				       	It.Is<LogUsuario>(
				       		log => log.Evento == eventoUsuario)
				       	)
				).Returns(logUsuario).AtMostOnce();
		    
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.Sucesso, resultado);
		}

		[Test]
		public void ValidarUsuario_QuandoSenhaIncorreta_RetornarSenhaIncorreta()
		{
			var eventoUsuario = new EventoUsuario {Codigo = EventoUsuarioCodigo.UsuarioErrouSenha};
			var logUsuario = new LogUsuario {Evento = eventoUsuario};
			var usuario = new Usuario {Senha = "a2"};

			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(usuario);
			usuarioRepositoryMock.Setup(rep => rep.Atualizar(usuario)).Returns(usuario);

			eventoRepositoryMock.Setup(rep => rep.ObterPorCodigo(EventoUsuarioCodigo.UsuarioErrouSenha)).Returns(eventoUsuario).
				AtMostOnce();

			logRepositoryMock.Setup(rep => rep.ObterUltimoLogPorUsuario(usuario)).Returns((LogUsuario) null);
			logRepositoryMock.Setup(rep => rep.ObterLogsPorUsuarioNoPeriodo(usuario, It.IsAny<DateTime>(), It.IsAny<DateTime>()))
				.Returns(new List<LogUsuario>());
			logRepositoryMock.Setup(
				rep => rep.Inserir(
				       	It.Is<LogUsuario>(
				       		log => log.Evento == eventoUsuario)
				       	)
				).Returns(logUsuario).AtMostOnce();
		    
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.SenhaIncorreta, resultado);
		}

		[Test]
		public void ValidarUsuario_QuandoUsuarioBloqueadoETempoMaiorQue10Minutos_DesbloquearEEfetuarTentativa()
		{
			var usuario = new Usuario {Senha = "a1"};
			var contaBloqueadaLog = new LogUsuario
			                        	{
			                        		VersionDate = DateTime.Now - TimeSpan.FromMinutes(20),
			                        		Evento = new EventoUsuario
			                        		         	{
			                        		         		Codigo = EventoUsuarioCodigo.ContaBloqueada
			                        		         	}
			                        	};

			var eventoUsuario = new EventoUsuario {Codigo = EventoUsuarioCodigo.ContaDesbloqueada};
			var eventoUsuarioSucesso = new EventoUsuario {Codigo = EventoUsuarioCodigo.LoginEfetuadoComSucesso};

			logRepositoryMock.Setup(
				log => log.Inserir(
						It.Is<LogUsuario>(lu =>
										  lu.Evento.Codigo == EventoUsuarioCodigo.ContaDesbloqueada)
						)).Returns((LogUsuario)null).AtMostOnce();

			logRepositoryMock.Setup(
				log => log.Inserir(
						It.Is<LogUsuario>(lu =>
										  lu.Evento.Codigo == EventoUsuarioCodigo.LoginEfetuadoComSucesso)
						)).Returns((LogUsuario)null).AtMostOnce();

			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(usuario);
			logRepositoryMock.Setup(rep => rep.ObterUltimoLogPorUsuario(usuario)).Returns(contaBloqueadaLog);

			eventoRepositoryMock.Setup(rep => 
					rep.ObterPorCodigo(EventoUsuarioCodigo.ContaDesbloqueada)
			).Returns(eventoUsuario).AtMostOnce();
			
			usuarioRepositoryMock.Setup(rep => rep.Atualizar(usuario)).Returns(usuario);

			eventoRepositoryMock.Setup(rep => 
					rep.ObterPorCodigo(EventoUsuarioCodigo.LoginEfetuadoComSucesso)
			).Returns(eventoUsuarioSucesso).AtMostOnce();
            
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.Sucesso, resultado);
		}

		[Test]
		public void ValidarUsuario_QuandoUsuarioBloqueadoETempoMenorQue10Minutos_RetornarUsuarioExcedeuTentativas()
		{
			var usuario = new Usuario {Senha = "a1"};
			var contaBloqueadaLog = new LogUsuario
			                        	{
			                        		VersionDate = DateTime.Now - TimeSpan.FromMinutes(5),
			                        		Evento = new EventoUsuario
			                        		         	{
			                        		         		Codigo = EventoUsuarioCodigo.ContaBloqueada
			                        		         	}
			                        	};

			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(usuario);
			logRepositoryMock.Setup(rep => rep.ObterUltimoLogPorUsuario(usuario)).Returns(contaBloqueadaLog);
            
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.UsuarioBloqueadoPorTentativas, resultado);
		}

		[Test]
		public void ValidarUsuario_QuandoUsuarioInativo_RetornarUsuarioInativo()
		{
			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(new Usuario {Ativo = false});
            
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.UsuarioInativo, resultado);
		}

		[Test]
		public void ValidarUsuario_QuandoUsuarioNaoExiste_RetornarUsuarioNaoExiste()
		{
			usuarioRepositoryMock.Setup(rep => rep.ObterPorCodigo(It.IsAny<string>())).Returns(null as Usuario);
            DateTime? lixo;
			AutenticacaoResultado resultado = service.Autenticar("MIGRA", "a1");

			Assert.AreEqual(AutenticacaoResultado.UsuarioNaoExiste, resultado);
		}

		[Test]
		public void ValidarAlgoritimoSoString_ValidarComposicaoSenha()
		{
			Assert.AreEqual(service.ValidarComposicaoSenha("a"), false);
		}

		[Test]
		public void ValidarAlgoritimoSoNumero_ValidarComposicaoSenha()
		{
			Assert.AreEqual(service.ValidarComposicaoSenha("123"), false);
		}

		[Test]
		public void ValidarAlgoritimoStringValida_ValidarComposicaoSenha()
		{
			Assert.AreEqual(service.ValidarComposicaoSenha("123das"), true);
		}
	}
}