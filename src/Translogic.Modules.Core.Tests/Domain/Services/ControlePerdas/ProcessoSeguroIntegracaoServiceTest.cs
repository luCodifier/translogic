﻿
namespace Translogic.Modules.Core.Tests.Domain.Services.ControlePerdas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Transactions;
    using System.Net;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;
    using Translogic.CteRunner.Services;
    using Translogic.CteRunner.Services.Commands;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;
    using Translogic.Modules.Core.Interfaces.ControlePerdas;
    using Translogic.Modules.Core.Domain.Services.Tfa;
    using Translogic.Modules.Core.Domain.Services.Tfa.Interface;
    using Translogic.Modules.Core.Domain.Services.Seguro.Interface;
    using Translogic.Modules.Core.Util;
using Translogic.Modules.Core.Domain.Model.Tfa.Repositories;
    using System.Diagnostics;

    [TestFixture]
    public class ProcessoSeguroIntegracaoServiceTest : BaseTranslogicContainerTestCase
    {
        private IProcessoSeguroIntegracaoService _service;
        private IProcessoSeguroService _processoSeguroService;
        private ITfaService _tfaService;
        private IEmpresaGrupoTfaRepository _empresaGrupoTfaRepository;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            //this.RealizarRollback = false;
            //this.TemTransacao = false;
            this._service = this.Container.Resolve<IProcessoSeguroIntegracaoService>();
            this._processoSeguroService = this.Container.Resolve<IProcessoSeguroService>();
            this._tfaService = this.Container.Resolve<ITfaService>();
            this._empresaGrupoTfaRepository = this.Container.Resolve<IEmpresaGrupoTfaRepository>();

        }
        #endregion

        [Test]
        public void GerarProcessoTest()
        {
            var payload = new ProcessoSeguroIntegracaoRequest
            {
                CodTipoGambito = 1,
                CodTipoLacre = 1, 
                CodTipoConclusaoTfa = 2,// 1 - Devido 2 - Indevido
                CodTipoLiberadorDescarga = 3,
                ConsideracoesAdicionais = "Vazamento Porta",
                DataHoraSinistro = "2019-03-11T22:13:00-0200",//DateTime.Now.ToString("o"),
                DataHoraVistoria = "2019-03-29T15:08:00-0200",//DateTime.Now.ToString("o"),
                DocumentoRepresentante = "04733389981",
                EdicaoManual = true,
                IdCausaDano = 230,
                //IdLocalVistoria = 40154, //40154 - Rocha Grão
                IdLocalVistoria = 22290, //22215 - INTERALLI
                ImagemAssinaturaRepresentante = "iVBORw0KGgoAAAANSUhEUgAAAVcAAACTCAMAAAAN4ao8AAAAgVBMVEX///8AAAD+/v76+vr19fXw8PDq6urn5+fz8/Pe3t7r6+vPz8/39/dsbGzu7u6RkZFgYGCgoKDW1tbGxsa/v7+JiYlHR0e1tbUNDQ1NTU1ZWVmAgIC4uLiqqqp6eno2NjYnJyc8PDwWFhYfHx80NDRBQUFycnIrKyuZmZlmZmYaGhq2JRZgAAAPNUlEQVR4nO1ciZqqOhIOQUUEF1BEUdy39v0fcEhVZQGXdh3O9OT/7u0+jSFUVWpPkDELCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwuLr4Pj//5smtdNylsgPvDfnN8b+l8BkODNnAKjuml5ByBL10/zYUcLuFZ6GI82QqxOUjctb4EzN98LNraLHvsHFJbFY5CqM4nrpuQdcBasHcKuWTctxbL2iZikVy8x74AbfAic6qaHtUhZu94/4ZVeROEDfgyxOv26yRkhHevg3wiiL4KzxtoUqzOsWUdOZDbCIfzvipWxxlFKNNuKH3WKtXh0F0g5+vUR8RtQPvyOLYmPQinWqT8Uv/wa5Voo6BRomdVGwu8QQosjkafckStjSxJryob1e4EGUvNPFwMU5vP7cs0oSDRZRBzV6NOaYDtbrzYCHoILrmp6V0wzFOueNX5IW78n1eszc+WwWG8HXt6lD769vhzwnH0W4z0M8/kNAmHGHMU6dlPxaxV/lRnOhstd1k2idBR3rjyoARXfgXujqLvOWl93SNgVeY7jooKaYG59c0QxX0x5QAzRIut9NV8s8uS9kc5NjuPsMEtOUZ6n8wJpiiFLJn37/4JcXfdpjkkTj7erUs7aWxizPch64KtpeEWuv+L7Jex8cnwyQvKEiAu0/7rEwOBiNSSH8za1d5A+IdYDeViDpGed4R2IWT1hNY+7m+KOJkV5J1IxgRoaJiKTi/blg7n8cXnrKyjiEi3kZLmd3JFo4SEWeaC6wlKuZpP4fWJkBfJ4/VFkrTui8EdSJUsEc1xssHKtLW8o+me0uLOWj/ObfhyM0vw0W3QH2XQ6nkKCtTzMTvko9kN6fi/vZv3mV2JpMWcgHrl4Yq3mkvxjS9PUHEXltLDUFdgNL58sfrTjKEnS8COK0hjfXsZwjOSWCMjPYuw0pD8LSczD98lQ0wv+t62HV025VpGNSoH4yZb+VkicEq7k4uHwsILPsrfZUWaH6Fc/g8gZ6EuiWah9GV3LRUJ4Y3blerlpoPcQ3bLTq9NrcjDAA9VtkuFODhKpJA3aUo/wVJmJhbmO3ukjj/+FNPTmh/WFXBnFs6g0PF7Jh2ckI0+qyuXsxi/B3yj/PZfoiNnGj+gqrNRwo4QxkD5/qBpWIQhaXGuRCz40MckqdzqKUenS0Uje9wNDWqD5pb6y3hbJ1YEfnR9i3cBLEPRubMt40c9gRLeKGnLZ+I2eQdlC7qEgzOj7rzyK5NrgB4qME2rDtJhYic5ErLX+QugvwYMEoDC7oKKagmxB4cQ3PR2K9QB6TLYP3e6sfKNEH2bv4B/5IxJLn2HLx4oF3L0zQjqbRpK66OZtHnrzLir1bi726OmzuTFPWy3FdAFTnt7RV2EgnaPkI6g+jCMJJz1cEpWi7ziABLGdGUhjl3YH2kQs4odgGJN7jqC4sScGHR8IG+IhOSaFh0yaWnHNq5Q4x+lapo7YNqLkYeJJegvNIB+xizwXi/a3+nYinwanAg0VUJRy0igC2jI0hwu/MAlYa4LiFZz0UV25MS0FqFDaViDu9beS+3sEgbY81mTyaNVysIMMs2jf8JKbSi6OnVaqd8nARJBbkNjzBiO3tnmrB1LICRbq2JKty02pCIml8BQJkJAVnhRC6hESWAhasufOk/VMOVB3KhnyRGkO965+iVt9tJ4HxMojlNoqjkkXwZ5UxCqY0S4Thp5wxanoUk5/SMqaNCgtWAq//HJqLu5DJ7BBBypkZbQuKf+SgRmeI8+5IGkRXIUkbUG1Sm5IhSu21uKvAd2D2bd3NS1DV728/lmZ9IAW7RDys9TFYpFl3QV6DMwdl1kyEqQMKN9KFU0CIXnWTFmqWKb59Qc/APEQH7w5HbCAaiQpfYzqKv0mbcdGMnZhZMd/t/Ee2JDrUg4JOj2Z4C3UTt7BPcPBarK76OrLQutawl4aJpwo5UqTYpHJuYrLLTNVCjBNcV0k/CynjUldYSJ5OkOly1xQemy/7Aa4FIlMkDxtHASxkqumYpo1JyA15mFwxYQ1I1nDlCM5RzEanBZ4uE1DbeXmQvhUhmRltRQeGMz1F10RhPTJcR6aNPMU7jfqxsIIQS9BUdpHPS2pCy6eL0nxdEeg47x5oADtQWVRIkxOOgb5oRDfzGhgiPVfuayHmWCCzgm0UN4ktGgd6vMk5w2qvGyX7QtRekqn1pVOabzRmncHXkJSFY0+VA6kmxsJ1qDdBgJCTpFVdru5tJw0yOX43OzNFdp07rAX+wOScVGG0wzicZnpXiEfCfT8lC80KcF24ZMMBxHH8AG4DBi8BUEdmCohC53xVLUGibzRkEPZd++LlQ/VSZW+SFTaIGM0nYWeuVjKKT5Qdh09yZbrVJAZfgcH918XK0cLGKskKlw7unkJEAwcZX7AeWMLT+xgx33VgSUOUBTG0mACgS4GeB7r/lxRRzSNeI2CJsl2kKABq6J4ipuOPJEP+WlXZU4HPMkE0hNy8CM9s0hJILRmcPsBOSPWTmWpTua9dNGdRUOSbjF423y5T9hET/SjrwDvZi3aLg1AYzqrYsVHywHlkSrfos9OvK2Vcueqe0QMxOfmoUsl/D4VzoDHtEt6KVYxNRjF2UxGBz6aLhjd2M9nhhUIiZtOVHC2dqWkgK2tGtx3Va97u5j3QCFyWs7ngZ6s5MoEhUZPito/uqzF3oo06BgTBKD+IGeZKQp1WDbFWjDXpUUxh6/WavjiKrVqAomuT4wPq58Ubh/0ODxr6oVKj5QDnQLfmE5sT82K+i63QgptP+o/5QhoLEURM/CCGygVxaAKgbpNOK8ulSpYPzHVIsHcNXYuMTWlElEHVLrjeXX01d4gBVDNeqROWHvVCdY5+TXQ8DGTOcpU7QRAZC1+d0b53L+2ZoSnzmzRmiF351LJChlS6aCSWNiJL8UKz9cnirEhB+uzo6L1gkkH3IiW9qYXmdLT+/1yDa5vvIjOpCzcJlnkKz78mVPBjJH9IocdIFNX+5w+6BlsXsxBuOKQfhFsG7OLadNs/oHmDUpeRQzC7FVVWgjZjmMeGBuVEph7bkvE9Vmpq4hzJGZXNlJ+cT+6EYNBiGE8HI0C7JTglbh0ghUwdqmPjmaVwkDhKvbSv4HR48EhZKGxvJjlFbmKrHgnl9b0zKq0Utea4soRG6ycdc7qiWL3BTijdBGtmk7GxrkmbR+o7uhUx+qB7svAJEH/Z5p1I+kxb8jV+AtIjM0NaxFxiBL4eEQcQggQHl2mOSdcbDpLo9tzV/B4X1tn/w70/kr0iqevSj0XeOK2R6uhxTXDC7p/IZSoQepcpKo+hoSNaGYHVK2PmyqW73Ut8MSBFhSmGu0bG0fOLglCEKXs2WIwQ+cK6rGhFCqS66pmvRoTnFU37bBHgUsku+vj5qW6los3dJcxssRVZ2pOZOl9g5R3qLNUeFBGKzdq6Bq+0PBQ3r78wBkkz8j/9ydMDWfgs0D4qK0rYnDkqOYUaKuoBpVIGPU7NZJ4GPjPUSi6p9onjZumOxPXt+U1QlFA9ReOpFjF0Sxaj5byjNpFiQNn0MUq8vGe2iDJXObSmL33vljNsxVAnphxQOrayjG2buT7DyJQYacTQtamustrrJED9kaiehxFWEXuFtBQXxvVG6he9aUQtOFBns9k722TU1+rgFveDiIui89AAxIvUbkR5KQo5G7T9OCvwfSIywZFIJG8ZHGQ/5D6bX25/oJMkeaEaDwBq0RHfXLCocKRVzz6PcC4VCoV9n5XMX5Ch7PW1f28Sr1XCIjCMdOZTAnIJfBtWFcE94SZM5neivhPAfzTGbUyVk6pFMactc/IffOjOKZX3IUWd4WClrbh51/0EzU2hpajWDFqqkcuigmW8uI0eLMiNl16cdrLSErOaeMzJVeFnawBXP9TR5JzZzPDzVOjZ1pa5h+ZQhZcTyDFT8+kUhfrKtqli3VhXKvB8Hlj4nLBMg+0nPbWxmknbGDK0q9qP68WieY+Km6Ju2tTrKQ8DbNjn4Ry74CrFOlteC3Mps09WyNN3mB5j9dF82rVz6RmXD4fyHM93+9VM7qHMETVSuBUqRHBNnv0uYeqs4ZmFVrbOeuLdN/oyYBH6ZYOIe59mQ0NZW2wWahrjCvH/DY4PXccSp8kfqj4s/B0glz8Z6xydD0gKbqepFCMxbAxMRsCZlh1Ftd3lNw4j/KiyoGdbDj6BJkMbsAKf5xLGfZdrQutaLBeTw/pl95FoG2FSfntIBcC5brvl4eyg+Lx7ZNBF6ADTGu/5EB0cj2J7twM9AnFzWVZDh5ljC9BN9PFdNqNVHOcEDZ+2/p7GRxP4mHH2kQvjr2wMlTVgbeaD++ADPPQVnZDiJP9xpns+951A5GkMWoiwr9bGCH2HSaPOrsgQvOM8cc5KAOca/LQVnihR+Agdrn7yPinQPk5bn8a3INMOn7LZexmQMHqkSzvJ/aHB0xLxyRW5cgqK3Zx4YMA5zq9fP1Ak2NcKv6fJ/1R+6WgdAPwmA76gNU7XxbAK3mMM/j17Np3AJ0SscTnGt8lxUOMWG3+vCuI0lHn5HMvEzwJTmdoRnURwHBxE51hvDmVr+rSVX0vPkrD6X/fid8D1QKbgL0nCXAocrJT+LlU9HlAeJ/WZy+C8TnWAoNPfXtJcJqdhjXaH6Oj3Jv6XtLV/YBbVcbTM74/xdsUcEaZa400uCoP+IzVckqdvpc//U6AOsRZCwFABLWVuveS0ydnlClgfVxBc2Vfp+XQrnNeW+T+MIAJbFzW+g0IsNe79z9YZNQMwcYUuye16mvx8JB9snirG3QUa4H/ro0KruLMnxAsna9w1rJhYfERcMxca/1Gpr+JMbWmrVw/ihk517/h1v4ZpJS5/p04XDvU8WWn9ftgi2eAL0X+0980978Irg441E3J3wK0srPfx1k8DPXFd6vqlzJZvANZZ936Mg6L1yC/tODiEIbFe8DjqTXvE/494K7HoW4y/hS4PLV2/Zu5LF4EpzfaV7bO+jD6lApY3/opQH8FX3D46vdf/79ByBUPPtsM65OQb4bg6VIr2Y8ApIjaemL1HQT5azB8q3hVzwr1Q9DnS22Z9Umol4bgPWjrWz+HmeFbLT4GTudL7SbhR4Hl6/wvHYL6J8BZYzQKyi9FWXwAVpoWFhYWFhYWFhYWFhYWFhYWFhYWFhYW7+A/+vmqxqGx8xoAAAAASUVORK5CYII=",
                MatriculaVistoriador = "TR018697",
                ModoEdicao = false,
                NomeRepresentante = "Paulo Faria",
                NomeVistoriador = "Nathanael de Vicente",
                NumeroDespacho = 5225,
                NumeroVagao = "3568334",
                PesoDestino = 54.080,
                PesoOrigem = 54.420,
                ProcessoGerado = false,
                ResponsavelCiente = true,
                Sindicancia = 0
            };
            var response = _service.GerarProcesso(payload);         
        }


        [Test]
        public void AtualizarProcessoTest()
        {
            var payload = new ProcessoSeguroIntegracaoRequest
            {
                CodTipoGambito = 1, /// Código do Tipo de Gambito (1 - Gambitos OK, 2 - Gambitos Parcial, 3 - Gambitos Ausentes)
                CodTipoLacre = 1, /// Código do Tipo de Lacre (1 - Lacres OK, 2 - Lacre Parcial, 3 - Lacres Ausentes)
                CodTipoConclusaoTfa = 2, /// Indicador se o Processo é 1 - Devido , 2 - Indevido, 3 - Cancelado
                CodTipoLiberadorDescarga = 3, /// Código do Tipo de Liberador da Descarga (1 - Terminal, 2 - Cliente, 3 - Rumo)
                ConsideracoesAdicionais = "Reprocessamento CINQ",
                DataHoraSinistro = "2019-06-26T00:00:00-0200",//DateTime.Now.ToString("o"),
                DataHoraVistoria = "2019-06-26T10:36:42-0200",//DateTime.Now.ToString("o"),
                DocumentoRepresentante = "",
                EdicaoManual = true,/// Indica se a Edição do Número de Despacho e Vagão foi feita manualmente
                IdCausaDano = 16,
                IdLocalVistoria = 22255,
                //ImagemAssinaturaRepresentante = "iVBORw0KGgoAAAANSUhEUgAAAVcAAACTCAMAAAAN4ao8AAAAgVBMVEX///8AAAD+/v76+vr19fXw8PDq6urn5+fz8/Pe3t7r6+vPz8/39/dsbGzu7u6RkZFgYGCgoKDW1tbGxsa/v7+JiYlHR0e1tbUNDQ1NTU1ZWVmAgIC4uLiqqqp6eno2NjYnJyc8PDwWFhYfHx80NDRBQUFycnIrKyuZmZlmZmYaGhq2JRZgAAAPNUlEQVR4nO1ciZqqOhIOQUUEF1BEUdy39v0fcEhVZQGXdh3O9OT/7u0+jSFUVWpPkDELCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwuLr4Pj//5smtdNylsgPvDfnN8b+l8BkODNnAKjuml5ByBL10/zYUcLuFZ6GI82QqxOUjctb4EzN98LNraLHvsHFJbFY5CqM4nrpuQdcBasHcKuWTctxbL2iZikVy8x74AbfAic6qaHtUhZu94/4ZVeROEDfgyxOv26yRkhHevg3wiiL4KzxtoUqzOsWUdOZDbCIfzvipWxxlFKNNuKH3WKtXh0F0g5+vUR8RtQPvyOLYmPQinWqT8Uv/wa5Voo6BRomdVGwu8QQosjkafckStjSxJryob1e4EGUvNPFwMU5vP7cs0oSDRZRBzV6NOaYDtbrzYCHoILrmp6V0wzFOueNX5IW78n1eszc+WwWG8HXt6lD769vhzwnH0W4z0M8/kNAmHGHMU6dlPxaxV/lRnOhstd1k2idBR3rjyoARXfgXujqLvOWl93SNgVeY7jooKaYG59c0QxX0x5QAzRIut9NV8s8uS9kc5NjuPsMEtOUZ6n8wJpiiFLJn37/4JcXfdpjkkTj7erUs7aWxizPch64KtpeEWuv+L7Jex8cnwyQvKEiAu0/7rEwOBiNSSH8za1d5A+IdYDeViDpGed4R2IWT1hNY+7m+KOJkV5J1IxgRoaJiKTi/blg7n8cXnrKyjiEi3kZLmd3JFo4SEWeaC6wlKuZpP4fWJkBfJ4/VFkrTui8EdSJUsEc1xssHKtLW8o+me0uLOWj/ObfhyM0vw0W3QH2XQ6nkKCtTzMTvko9kN6fi/vZv3mV2JpMWcgHrl4Yq3mkvxjS9PUHEXltLDUFdgNL58sfrTjKEnS8COK0hjfXsZwjOSWCMjPYuw0pD8LSczD98lQ0wv+t62HV025VpGNSoH4yZb+VkicEq7k4uHwsILPsrfZUWaH6Fc/g8gZ6EuiWah9GV3LRUJ4Y3blerlpoPcQ3bLTq9NrcjDAA9VtkuFODhKpJA3aUo/wVJmJhbmO3ukjj/+FNPTmh/WFXBnFs6g0PF7Jh2ckI0+qyuXsxi/B3yj/PZfoiNnGj+gqrNRwo4QxkD5/qBpWIQhaXGuRCz40MckqdzqKUenS0Uje9wNDWqD5pb6y3hbJ1YEfnR9i3cBLEPRubMt40c9gRLeKGnLZ+I2eQdlC7qEgzOj7rzyK5NrgB4qME2rDtJhYic5ErLX+QugvwYMEoDC7oKKagmxB4cQ3PR2K9QB6TLYP3e6sfKNEH2bv4B/5IxJLn2HLx4oF3L0zQjqbRpK66OZtHnrzLir1bi726OmzuTFPWy3FdAFTnt7RV2EgnaPkI6g+jCMJJz1cEpWi7ziABLGdGUhjl3YH2kQs4odgGJN7jqC4sScGHR8IG+IhOSaFh0yaWnHNq5Q4x+lapo7YNqLkYeJJegvNIB+xizwXi/a3+nYinwanAg0VUJRy0igC2jI0hwu/MAlYa4LiFZz0UV25MS0FqFDaViDu9beS+3sEgbY81mTyaNVysIMMs2jf8JKbSi6OnVaqd8nARJBbkNjzBiO3tnmrB1LICRbq2JKty02pCIml8BQJkJAVnhRC6hESWAhasufOk/VMOVB3KhnyRGkO965+iVt9tJ4HxMojlNoqjkkXwZ5UxCqY0S4Thp5wxanoUk5/SMqaNCgtWAq//HJqLu5DJ7BBBypkZbQuKf+SgRmeI8+5IGkRXIUkbUG1Sm5IhSu21uKvAd2D2bd3NS1DV728/lmZ9IAW7RDys9TFYpFl3QV6DMwdl1kyEqQMKN9KFU0CIXnWTFmqWKb59Qc/APEQH7w5HbCAaiQpfYzqKv0mbcdGMnZhZMd/t/Ee2JDrUg4JOj2Z4C3UTt7BPcPBarK76OrLQutawl4aJpwo5UqTYpHJuYrLLTNVCjBNcV0k/CynjUldYSJ5OkOly1xQemy/7Aa4FIlMkDxtHASxkqumYpo1JyA15mFwxYQ1I1nDlCM5RzEanBZ4uE1DbeXmQvhUhmRltRQeGMz1F10RhPTJcR6aNPMU7jfqxsIIQS9BUdpHPS2pCy6eL0nxdEeg47x5oADtQWVRIkxOOgb5oRDfzGhgiPVfuayHmWCCzgm0UN4ktGgd6vMk5w2qvGyX7QtRekqn1pVOabzRmncHXkJSFY0+VA6kmxsJ1qDdBgJCTpFVdru5tJw0yOX43OzNFdp07rAX+wOScVGG0wzicZnpXiEfCfT8lC80KcF24ZMMBxHH8AG4DBi8BUEdmCohC53xVLUGibzRkEPZd++LlQ/VSZW+SFTaIGM0nYWeuVjKKT5Qdh09yZbrVJAZfgcH918XK0cLGKskKlw7unkJEAwcZX7AeWMLT+xgx33VgSUOUBTG0mACgS4GeB7r/lxRRzSNeI2CJsl2kKABq6J4ipuOPJEP+WlXZU4HPMkE0hNy8CM9s0hJILRmcPsBOSPWTmWpTua9dNGdRUOSbjF423y5T9hET/SjrwDvZi3aLg1AYzqrYsVHywHlkSrfos9OvK2Vcueqe0QMxOfmoUsl/D4VzoDHtEt6KVYxNRjF2UxGBz6aLhjd2M9nhhUIiZtOVHC2dqWkgK2tGtx3Va97u5j3QCFyWs7ngZ6s5MoEhUZPito/uqzF3oo06BgTBKD+IGeZKQp1WDbFWjDXpUUxh6/WavjiKrVqAomuT4wPq58Ubh/0ODxr6oVKj5QDnQLfmE5sT82K+i63QgptP+o/5QhoLEURM/CCGygVxaAKgbpNOK8ulSpYPzHVIsHcNXYuMTWlElEHVLrjeXX01d4gBVDNeqROWHvVCdY5+TXQ8DGTOcpU7QRAZC1+d0b53L+2ZoSnzmzRmiF351LJChlS6aCSWNiJL8UKz9cnirEhB+uzo6L1gkkH3IiW9qYXmdLT+/1yDa5vvIjOpCzcJlnkKz78mVPBjJH9IocdIFNX+5w+6BlsXsxBuOKQfhFsG7OLadNs/oHmDUpeRQzC7FVVWgjZjmMeGBuVEph7bkvE9Vmpq4hzJGZXNlJ+cT+6EYNBiGE8HI0C7JTglbh0ghUwdqmPjmaVwkDhKvbSv4HR48EhZKGxvJjlFbmKrHgnl9b0zKq0Utea4soRG6ycdc7qiWL3BTijdBGtmk7GxrkmbR+o7uhUx+qB7svAJEH/Z5p1I+kxb8jV+AtIjM0NaxFxiBL4eEQcQggQHl2mOSdcbDpLo9tzV/B4X1tn/w70/kr0iqevSj0XeOK2R6uhxTXDC7p/IZSoQepcpKo+hoSNaGYHVK2PmyqW73Ut8MSBFhSmGu0bG0fOLglCEKXs2WIwQ+cK6rGhFCqS66pmvRoTnFU37bBHgUsku+vj5qW6los3dJcxssRVZ2pOZOl9g5R3qLNUeFBGKzdq6Bq+0PBQ3r78wBkkz8j/9ydMDWfgs0D4qK0rYnDkqOYUaKuoBpVIGPU7NZJ4GPjPUSi6p9onjZumOxPXt+U1QlFA9ReOpFjF0Sxaj5byjNpFiQNn0MUq8vGe2iDJXObSmL33vljNsxVAnphxQOrayjG2buT7DyJQYacTQtamustrrJED9kaiehxFWEXuFtBQXxvVG6he9aUQtOFBns9k722TU1+rgFveDiIui89AAxIvUbkR5KQo5G7T9OCvwfSIywZFIJG8ZHGQ/5D6bX25/oJMkeaEaDwBq0RHfXLCocKRVzz6PcC4VCoV9n5XMX5Ch7PW1f28Sr1XCIjCMdOZTAnIJfBtWFcE94SZM5neivhPAfzTGbUyVk6pFMactc/IffOjOKZX3IUWd4WClrbh51/0EzU2hpajWDFqqkcuigmW8uI0eLMiNl16cdrLSErOaeMzJVeFnawBXP9TR5JzZzPDzVOjZ1pa5h+ZQhZcTyDFT8+kUhfrKtqli3VhXKvB8Hlj4nLBMg+0nPbWxmknbGDK0q9qP68WieY+Km6Ju2tTrKQ8DbNjn4Ry74CrFOlteC3Mps09WyNN3mB5j9dF82rVz6RmXD4fyHM93+9VM7qHMETVSuBUqRHBNnv0uYeqs4ZmFVrbOeuLdN/oyYBH6ZYOIe59mQ0NZW2wWahrjCvH/DY4PXccSp8kfqj4s/B0glz8Z6xydD0gKbqepFCMxbAxMRsCZlh1Ftd3lNw4j/KiyoGdbDj6BJkMbsAKf5xLGfZdrQutaLBeTw/pl95FoG2FSfntIBcC5brvl4eyg+Lx7ZNBF6ADTGu/5EB0cj2J7twM9AnFzWVZDh5ljC9BN9PFdNqNVHOcEDZ+2/p7GRxP4mHH2kQvjr2wMlTVgbeaD++ADPPQVnZDiJP9xpns+951A5GkMWoiwr9bGCH2HSaPOrsgQvOM8cc5KAOca/LQVnihR+Agdrn7yPinQPk5bn8a3INMOn7LZexmQMHqkSzvJ/aHB0xLxyRW5cgqK3Zx4YMA5zq9fP1Ak2NcKv6fJ/1R+6WgdAPwmA76gNU7XxbAK3mMM/j17Np3AJ0SscTnGt8lxUOMWG3+vCuI0lHn5HMvEzwJTmdoRnURwHBxE51hvDmVr+rSVX0vPkrD6X/fid8D1QKbgL0nCXAocrJT+LlU9HlAeJ/WZy+C8TnWAoNPfXtJcJqdhjXaH6Oj3Jv6XtLV/YBbVcbTM74/xdsUcEaZa400uCoP+IzVckqdvpc//U6AOsRZCwFABLWVuveS0ydnlClgfVxBc2Vfp+XQrnNeW+T+MIAJbFzW+g0IsNe79z9YZNQMwcYUuye16mvx8JB9snirG3QUa4H/ro0KruLMnxAsna9w1rJhYfERcMxca/1Gpr+JMbWmrVw/ihk517/h1v4ZpJS5/p04XDvU8WWn9ftgi2eAL0X+0980978Irg441E3J3wK0srPfx1k8DPXFd6vqlzJZvANZZ936Mg6L1yC/tODiEIbFe8DjqTXvE/494K7HoW4y/hS4PLV2/Zu5LF4EpzfaV7bO+jD6lApY3/opQH8FX3D46vdf/79ByBUPPtsM65OQb4bg6VIr2Y8ApIjaemL1HQT5azB8q3hVzwr1Q9DnS22Z9Umol4bgPWjrWz+HmeFbLT4GTudL7SbhR4Hl6/wvHYL6J8BZYzQKyi9FWXwAVpoWFhYWFhYWFhYWFhYWFhYWFhYWFhYW7+A/+vmqxqGx8xoAAAAASUVORK5CYII=",
                ImagemAssinaturaRepresentante = "",
                MatriculaVistoriador = "CS303782",
                ModoEdicao = true,/// Indica se o contrato de serviço está sendo enviado em modo de Edição
                NomeRepresentante = "",
                NomeVistoriador = "ORIEL MOREIRA ALVES",
                NumeroDespacho = 5994,
                NumeroVagao = "0528935",
                PesoDestino = 0.260,
                PesoOrigem = 77.00,
                ProcessoGerado = true,/// Indica se o contrato de serviço está sendo enviado em modo de Edição e o Processo já foi gerado
                ResponsavelCiente = true,
                Sindicancia = 0,
                NumeroProcesso = 246583,
                Justificativa = "Reprocessamento CINQ"

            };
            var response = _service.AtualizarProcesso(payload);
        }

        [Test]
        public void UploadTfa()
        {
            _tfaService.UploadTFA(160348);
        }


        [Test]
        public void GerarDossie()
        {
            _processoSeguroService.GerarDossie(160348);
        }


        [Test]
        public void ObterListaEmailTfa()
        {
            _empresaGrupoTfaRepository.ObterListaEmailTfa(7024);
        }


        [Test]
        public void EnviarEmail()
        {
            EmailUtil.Send("labrecirino@gmail.com", "teste", "teste");
        }

        [Test]
        public void AtualizarDocumentoTfa()
        {
            var payload = new AtualizarDocumentoTfaDto
            {
                NumeroProcesso = 160541,
                EnviarEmail = true
            };
            var response = _service.AtualizarDocumentoTfa(payload);
        }

        [Test]
        public void AtualizarDossie()
        {
            var payload = new AtualizarDocumentoTfaDto();
            payload.EnviarEmail = false;

            payload.NumeroProcesso = 177388;
            var response = _service.AtualizarDossie(payload);
            
            //SemaphoreSlim maxThread = new SemaphoreSlim(50);

            //for (int i = 200890; i <= 200910; i++)
            //{
            //    maxThread.Wait();
            //    Task.Factory.StartNew(() =>
            //    {
            //        payload.NumeroProcesso = i;
            //        var response = _service.AtualizarDossie(payload);
            //        Debug.WriteLine(string.Format("Processo: {0} - atualizado em: {1:T}", i, DateTime.Now));
            //    }
            //        , TaskCreationOptions.LongRunning)
            //    .ContinueWith((task) => maxThread.Release());
            //}
        }

        [Test]
        public void CriarTfaProcesso()
        {
            var payload = new AtualizarDocumentoTfaDto();
            payload.EnviarEmail = false;

            //SemaphoreSlim maxThread = new SemaphoreSlim(50);

            for (int i = 193213; i < 200947; i++)
            {
                //maxThread.Wait();
                //Task.Factory.StartNew(() =>
                //{
                    payload.NumeroProcesso = i;
                    var tfa = _tfaService.ObterPorNumProcesso(i);

                    try
                    {
                        if (tfa == null)
                        {
                            var response = _service.CriarTfaProcesso(payload);
                            if (response != null && response.TfaAtualizado.GetValueOrDefault())
                            {
                                _tfaService.UploadTFA(i);
                                Debug.WriteLine(string.Format("Processo: {0} - atualizado em: {1:T}", i, DateTime.Now));
                            }
                        }
                        else
                        {
                            _tfaService.UploadTFA(i);
                            Debug.WriteLine(string.Format("Processo: {0} - atualizado em: {1:T}", i, DateTime.Now));
                        }

                        if (i >= 200910)
                        {
                            _service.AtualizarDossie(payload);
                            Debug.WriteLine(string.Format("Processo: {0} - atualizado em: {1:T}", i, DateTime.Now));
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("Erro ao gerar Processo: {0} - atualizado em: {1:T}\n{2}", i, DateTime.Now, ex.Message));
                    }
                //}
                //    , TaskCreationOptions.LongRunning)
                //.ContinueWith((task) => maxThread.Release());
            }
        }
    }
}
