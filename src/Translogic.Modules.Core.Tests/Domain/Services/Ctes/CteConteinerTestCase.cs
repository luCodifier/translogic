﻿namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using System;
    using Core.Domain.Model.CteRodoviario;
    using Core.Domain.Model.CteRodoviario.Inputs;
    using Core.Domain.Services.Ctes.Interface;
    using NUnit.Framework;
    using Translogic.Tests;

    public class CteConteinerTestCase : BaseTranslogicContainerTestCase
    {
        private ICteRodoviarioService _cteRodoviarioService;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._cteRodoviarioService = this.Container.Resolve<ICteRodoviarioService>();
        }
        #endregion

        [Test]
        public void ValidaDadosCtesRodoviarios()
        {

        }
    }
}
