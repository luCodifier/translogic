﻿using Translogic.Modules.Core.Domain.Model.Diversos.Cte;

namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Transactions;
    using System.Net;
    using Castle.MicroKernel.Registration;
    using Translogic.Core;
    using Translogic.CteRunner.Services;
    using Translogic.CteRunner.Services.Commands;
    using Translogic.Modules.Core.Domain.Model.Acesso;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Spd.Data.Enums;

    public class CteServiceTest : BaseTranslogicContainerTestCase
    {
        private CteService _service;
        private CarregamentoService _carregamentoService;
        private SenderQueueManagerService _senderService;
        private SenderManagerInterfaceSapService _sapSenderQueueManagerService;
        private ReceiverManagerService _receiverManagerService;
        private ICteRepository _cteRepository;
        private ICteMotivoCancelamentoRepository _cteMotivoCancelamentoRepository;
        private ICteConteinerRepository _cteConteinerRepository;
        private ICteVersaoRepository _cteVersaoRepository;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._service = this.Container.Resolve<CteService>();

            this._carregamentoService = this.Container.Resolve<CarregamentoService>();
            this._cteRepository = this.Container.Resolve<ICteRepository>();
            this._cteMotivoCancelamentoRepository = this.Container.Resolve<ICteMotivoCancelamentoRepository>();
            this._cteConteinerRepository = this.Container.Resolve<ICteConteinerRepository>();
            this._cteVersaoRepository = this.Container.Resolve<ICteVersaoRepository>();

            this.Container.Register(Component.For<SenderQueueManagerService>());
            this.Container.Register(Component.For<ICteRunnerLogRepository>().ImplementedBy<CteRunnerLogRepository>());
            this.Container.Register(Component.For<CteRunnerLogService>().ImplementedBy<CteRunnerLogService>()).Resolve<CteRunnerLogService>();
            ////this.Container.Register(Component.For<GravarConfigCteAnulacaoService>());
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFA").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteCancelamentoCommand>().Named("EFC").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteInutilizacaoCommand>().Named("EFI").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteComplementoCommand>().Named("EFT").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCorrecaoCteCommand>().Named("ECC").LifeStyle.PerThread);
            ////this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoContribuinteCommand>().Named("EFN").LifeStyle.PerThread);
            ////this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteAnulacaoNContribuinteCommand>().Named("EFR").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFN").LifeStyle.PerThread);
            this.Container.Register(Component.For<ICommandCte>().ImplementedBy<GravarConfigCteEnvioCommand>().Named("EFR").LifeStyle.PerThread);

            this.Container.Register(Component.For<SenderManagerInterfaceSapService>().LifeStyle.PerThread);
            this._senderService = this.Container.Resolve<SenderQueueManagerService>();
            this._sapSenderQueueManagerService = this.Container.Resolve<SenderManagerInterfaceSapService>();
            this.Container.Register(Component.For<ReceiverManagerService>().LifeStyle.PerThread);
            this._receiverManagerService = this.Container.Resolve<ReceiverManagerService>();

            this.Container.Register(Component.For<ImporterPdfFileManagerService>().LifeStyle.PerThread);
            this.Container.Register(Component.For<ImporterXmlFileManagerService>().LifeStyle.PerThread);
            this.Container.Register(Component.For<SenderEmailManagerService>().LifeStyle.PerThread);
            this.Container.Register(Component.For<DirectoryFileManagerService>());

            Sis.Inicializar(EnumSistema.Testes, "TESTES", true);
        }
        #endregion

        [Test]
        public void ValidarTrocaDeFluxoComercialParaRealizarUmaCorrecao()
        {
            const string novocodigoFluxoComercial = "80679";
            const string codigoFluxoComercial = "45897";
            const int idCte = 1982321;
            // this._service.ValidarAlteracaoDoFluxoComercial(novocodigoFluxoComercial, codigoFluxoComercial, idCte);
        }

        [Test]
        public void InserirCorrecaoCte()
        {
            var cteDto = new CorrecaoCteDto();
            cteDto.CteId = 1977803;
            cteDto.CodFluxo = "80679";
            cteDto.Conteiner = "GATU0791410, TTNU3395324";
            cteDto.Observacao = "teste";

            var usuario = new Usuario { Codigo = "teste" };
            _service.SalvarCorrecaoCte(cteDto, usuario);
        }

        [Test]
        public void RetornaCtesNaoConstamCteEmpresas()
        {
            IList<CteDto> listaCtesNaoConstamCteEmpresas;

            do
            {
                listaCtesNaoConstamCteEmpresas = null;
                GC.Collect();
                listaCtesNaoConstamCteEmpresas = _service.RetornaCtesNaoConstamCteEmpresas();
                var tasks = new List<Task<bool>>();

                foreach (var reg in listaCtesNaoConstamCteEmpresas)
                {
                    CteDto cteNaoConstaCteEmpresa = reg;
                    var teste = _service.InserirCteEmpresaPorCte(cteNaoConstaCteEmpresa.CteId);
                    //                    tasks.Add(new Task<bool>(() => _service.InserirCteEmpresaPorCte(cteNaoConstaCteEmpresa.CteId)));
                    //
                    //                    if (tasks.Count == 100)
                    //                    {
                    //                        tasks.ForEach(A => A.Start());
                    //                        Task.WaitAll(tasks.ToArray());
                    //                        tasks.Clear();
                    //                        GC.Collect();
                    //                    }
                }

                //                Task.WaitAll(tasks.ToArray());
                //
                //                tasks = null;

            } while (listaCtesNaoConstamCteEmpresas.Count > 0);
        }

        [Test]
        public void SenderServiceExecutarGeracaoSingleThreadTestCase()
        {
            _senderService.InicializarParametros();
            // _senderService.ExecutarGeracaoSingleThread();
            _receiverManagerService.ExecutarRecebimentoSingleThread();
        }

        [Test]
        //[Ignore]
        public void SenderManagerInterfaceSapExecutarGeracaoSingleThreadTestCase()
        {
            _sapSenderQueueManagerService.InicializarParametros();
            _sapSenderQueueManagerService.ExecutarEnvioSingleThread();
        }

        [Test]
        [Ignore]
        public void InserirPoolingEnvioSapCte()
        {
            var lista = new List<int>();

            foreach (var item in lista)
            {
                try
                {
                    var cte = _cteRepository.ObterPorId(item);
                    _service.InserirInterfaceEnvioSap(cte, new CteStatusRetorno { Id = 100 });
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            }


        }

        [Test]
        public void SalvarCancelamentoCte()
        {
            try
            {
                var usuario = new Usuario { Codigo = "teste" };
                var cte = _cteRepository.ObterPorChave("51170224962466000136570020006377601905806351");
                var cteMotivoCancelamento = _cteMotivoCancelamentoRepository.ObterPorId(4);
                var conteiner = _cteConteinerRepository.ObterPorCte(cte.Id ?? 0);
                bool retorno = false;
                _service.SalvarCancelamentoCte(usuario, cte, false, ref retorno, null, cteMotivoCancelamento, false);
            }
            catch (Exception ex)
            {
                string exMsg = ex.Message + ex.InnerException.Message;
                throw;
            }
        }

        [Test]
        public void InserirCteInterfaceSap()
        {
            //// 2911581
            //// 2911580  

            //Cte cte = _cteRepository.ObterPorChave("51170224962466000136570020006377431905806350");    2911543
            var cte = _cteRepository.ObterPorId(2911619);  //ObterPorChave("51170224962466000136570020006377441905806358");

            ////_service.Executar(cte);

            _service.InserirInterfaceEnvioSap(cte, new CteStatusRetorno { Id = 100 });
        }

        [Test]
        public void TestarMudarSituacaoCte()
        {
            var idCte = 4623216;
            var cte = _cteRepository.ObterPorId(4623216);

            cte.Versao = null;

            try
            {
                cte.Versao = _cteVersaoRepository.ObterTodos().FirstOrDefault(p => p.Ativo);
                _cteRepository.Atualizar(cte);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        [Test]
        public void InserirPoolingEnvioCteTest()
        {
            IList<Cte> lista = new List<Cte>();
            var cte = _cteRepository.ObterPorId(2911776);
            //var cte3 = _cteRepository.ObterPorId(2911401);
            //var cte2 = _cteRepository.ObterPorId(2911400);

            lista.Add(cte);
            //lista.Add(cte3);
            //lista.Add(cte2);

            _service.InserirPoolingEnvioCte(lista, Dns.GetHostName());
        }

        [Test]
        public void TestObterPorIdsHql()
        {
            var lista = new List<int>();
            lista.Add(5280991);
            lista.Add(5280990);
            lista.Add(5280989);
            lista.Add(5280988);
            lista.Add(5280987);
            lista.Add(5280986);
            lista.Add(5280985);
            lista.Add(5280984);
            lista.Add(5280983);
            lista.Add(5280982);
            lista.Add(5280981);
            lista.Add(5280980);
            var cte = _cteRepository.ObterPorIdsHql(lista);
        }

        [Test]
        public void EnviarSeguradoraTest()
        {
            var cte = _cteRepository.ObterPorId(151572);
            _service.EnviarSeguradora(@"d:\temp\Translogic\Cte\41191101258944000550570020027281011963654006.xml", cte, StatusEnvioSeguradora.SemPeso);
        }

        [Test]
        public void ObterCtesProcessarRecebimento()
        {
            _service.ObterCtesProcessarRecebimento(1);
        }
    }
}
