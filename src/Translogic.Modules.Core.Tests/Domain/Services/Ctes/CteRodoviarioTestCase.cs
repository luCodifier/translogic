﻿namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using System;
    using Core.Domain.Model.CteRodoviario;
    using Core.Domain.Model.CteRodoviario.Inputs;
    using Core.Domain.Services.Ctes.Interface;
    using NUnit.Framework;
    using Translogic.Tests;

    public class CteRodoviarioTestCase : BaseTranslogicContainerTestCase
    {
        private ICteRodoviarioService _cteRodoviarioService;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._cteRodoviarioService = this.Container.Resolve<ICteRodoviarioService>();
        }
        #endregion

        [Test]
        public void ValidaDadosCtesRodoviarios()
        {
            // Para DEBUG rodar todos como X86 
            // Para deploy no Servidor do CAAL efetuar [Release x86]

            var inicial = new DateTime(2019, 01, 01, 17, 54, 0);
            var final = new DateTime(2019, 01, 01, 17, 58, 0);

            var request = new InformationElectronicBillofLadingHighwayRequest
            {
                InformatioElectronicBillofLadingHighway = new InformatioElectronicBillofLadingHighway
                {
                    InformationElectronicBillofLadingHighwayType = new InformationElectronicBillofLadingHighwayType
                    {
                        ElectronicBillofLadingHighwayDetail = new ElectronicBillofLadingHighwayDetail
                        {
                            EndDate = final,
                            StartDate = inicial
                        }
                    }
                }
            };

            var response = _cteRodoviarioService.InformationCteRodoviario(request);
        }
    }
}
