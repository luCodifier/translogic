﻿namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Web;

    using NUnit.Framework;

    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Inputs;
    using Translogic.Modules.Core.Domain.Model.CteFerroviarioCancelado.Outputs;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;
    using Translogic.Tests;

    public class CteFerroviarioCancelamentoTestCase : BaseTranslogicContainerTestCase
    {
        #region Fields

        private ICteFerroviarioCanceladoService _cteFerroviarioService;

        #endregion

        #region Public Methods and Operators

        [Test]
        public void InformationCte()
        {
            var request = new EletronicBillofLadingCancelledRailRequest
                              {
                                  ElectronicBillofLadingCancelledRailType =
                                      new ElectronicBillofLadingCancelledRailType
                                          {
                                              ElectronicBillofLadingCancelledRailDetail
                                                  =
                                                  new ElectronicBillofLadingCancelledRailDetail
                                                      {
                                                          EndDate
                                                              =
                                                              new DateTime
                                                              (
                                                              2014,
                                                              2,
                                                              1),
                                                          StartDate
                                                              =
                                                              new DateTime
                                                              (
                                                              2014,
                                                              1,
                                                              1)
                                                      }
                                          }
                              };

            string message;
            EletronicBillofLadingCancelledRailResponse response = this._cteFerroviarioService.InformationCte(request);
        }

        [Test]
        public void ValidaDados()
        {
            var request = new EletronicBillofLadingCancelledRailRequest
                              {
                                  ElectronicBillofLadingCancelledRailType =
                                      new ElectronicBillofLadingCancelledRailType
                                          {
                                              ElectronicBillofLadingCancelledRailDetail
                                                  =
                                                  new ElectronicBillofLadingCancelledRailDetail
                                                      {
                                                          EndDate
                                                              =
                                                              DateTime
                                                              .Today,
                                                          StartDate
                                                              =
                                                              DateTime
                                                              .Today
                                                              .AddDays
                                                              (
                                                                  (-2))
                                                      }
                                          }
                              };

            string message;
            IList<EletronicBillofLadingCancelledData> response =
                this._cteFerroviarioService.ObterCtesCanceladosSemDespachoCancelado(request, out message);
        }

        [Test]
        public void ValidarCteCanceladoParaRefaturamento()
        {
            var request = new EletronicBillofLadingCancelledRailRequest
                              {
                                  ElectronicBillofLadingCancelledRailType =
                                      new ElectronicBillofLadingCancelledRailType
                                          {
                                              ElectronicBillofLadingCancelledRailDetail
                                                  =
                                                  new ElectronicBillofLadingCancelledRailDetail
                                                      {
                                                          EndDate
                                                              =
                                                              new DateTime
                                                              (
                                                              2012,
                                                              01,
                                                              31,
                                                              23,
                                                              59,
                                                              59),
                                                          StartDate
                                                              =
                                                              new DateTime
                                                              (
                                                              2012,
                                                              01,
                                                              01,
                                                              0,
                                                              0,
                                                              0)
                                                      }
                                          }
                              };

            try
            {
                EletronicBillofLadingCancelledRailResponse response =
                    this._cteFerroviarioService.ObterCtesCanceladosParaRefaturamento(request);

                using (var fs = new FileStream("C:\\Temp\\relatorio.csv", FileMode.CreateNew, FileAccess.ReadWrite))
                {
                    using (var sw = new StreamWriter(fs))
                    {
                        sw.WriteLine("sep=;");
                        List<EletronicBillofLadingCancelledData> lst =
                            response.EletronicBillofLadingCancelledRailType.EletronicBillOfLadingCancelledRailDetail
                                .EletronicBillofLadingCancelledData;

                        sw.WriteLine(
                            "CteCancelledId;CteLadingId;CteCancelledKey;CteLadingKey;WaybillInvoiceCancelledId;WaybillInvoiceId");

                        foreach (EletronicBillofLadingCancelledData data in lst)
                        {
                            sw.WriteLine(
                                "{0};{1};{2};{3};{4};{5}",
                                data.CteCancelledId,
                                data.CteLadingId,
                                data.CteCancelledKey,
                                data.CteLadingKey,
                                String.Join(",", data.WayBillsInfoCancelled.Select(a => a.WaybillId.ToString())),
                                String.Join(",", data.WayBillsInfoLading.Select(a => a.WaybillId.ToString())));
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        [Test]        
        public void EnviaCteCanceladoParaFilaConfig()
        {
            try
            {
                var _cteRepository = ((AbstractGlobalApplication)HttpContext.Current.ApplicationInstance).Container
                                     .Resolve<ICteRepository>();

                var _cteCancelRefatRepository = ((AbstractGlobalApplication)HttpContext.Current.ApplicationInstance).Container
                                                .Resolve<ICteCanceladoRefaturamentoRepository>();

                IList<CteCanceladoRefaturamento> listaCtesCancelRefat = _cteCancelRefatRepository.ObterTodos().Where(crf => !crf.Processado).ToList();
                IList<int?> listaIdCtes = new List<int?>();

                foreach (CteCanceladoRefaturamento cteCanceladoRefaturamento in listaCtesCancelRefat)
                {
                    if (listaIdCtes.Count < 10)
                    {
                        Cte cte = _cteRepository.ObterPorId((int?)cteCanceladoRefaturamento.CteCanceladoId);
                        cte.SituacaoAtual = SituacaoCteEnum.AguardandoCancelamento;
                        cteCanceladoRefaturamento.Processado = true;

                        _cteRepository.Atualizar(cte);
                        _cteCancelRefatRepository.Atualizar(cteCanceladoRefaturamento);

                        listaIdCtes.Add(cte.Id);
                    }
                    else
                    {
                        bool todosCtesCancelados = false;
                        do
                        {
                            Thread.Sleep(5000);

                            foreach (var idCte in listaIdCtes)
                            {
                                Cte cte = _cteRepository.ObterPorId(idCte);
                                if (cte.SituacaoAtual != SituacaoCteEnum.Cancelado)
                                {
                                    todosCtesCancelados = false;
                                    break;
                                }
                                todosCtesCancelados = true;
                            }
                            if (todosCtesCancelados)
                            {
                                listaIdCtes = new List<int?>();
                            }
                        }
                        while (!todosCtesCancelados);
                    }
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
            }            
        }

        #endregion

        #region Methods

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._cteFerroviarioService = this.Container.Resolve<ICteFerroviarioCanceladoService>();
        }

        #endregion
    }
}