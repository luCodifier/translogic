﻿using System;
using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao;
using Translogic.Modules.Core.Domain.Model.CteConclusaoExportacao.Inputs;

namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.Ctes.Interface;
    using Translogic.Tests;

    public class CteConslusaoExportacaoTestCase : BaseTranslogicContainerTestCase
    {
        private ICteConclusaoExportacaoService _cteConslusaoExportacaoService;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._cteConslusaoExportacaoService = this.Container.Resolve<ICteConclusaoExportacaoService>();
        }
        #endregion

        //[Test]
        public void ValidaDadosCtesConclusaoExportacao()
        {
            DateTime inicial = new DateTime(2014, 10, 29, 10, 55, 0);

            DateTime final = new DateTime(2014, 10, 29, 11, 0, 0);

            var request = new EletronicBillofLadingExportCompletedRailRequest
            {
                ElectronicBillofLadingExportCompletedRail =
                    new ElectronicBillofLadingExportCompletedRail
                        {
                            ElectronicBillofLadingExportCompletedRailType =
                                new ElectronicBillofLadingExportCompletedRailType
                                    {
                                        ElectronicBillofLadingExportCompletedRailDetail =
                                            new ElectronicBillofLadingExportCompletedRailDetail
                                                {
                                                    StartDate = inicial,
                                                    EndDate = final
                                                },
                                                OtherXml = "",
                                                VersionIdentifier = "1.0"
                                    }
                        }
            };

            EletronicBillofLadingExportCompletedRailResponse response =
                _cteConslusaoExportacaoService.InformationCteConclusaoExportacao(request);

        }

    }
}
