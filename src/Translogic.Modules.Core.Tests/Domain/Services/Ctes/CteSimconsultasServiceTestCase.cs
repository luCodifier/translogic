﻿namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
	using System;
	using System.Collections.Generic;

	using NUnit.Framework;

	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Ctes;
	using Translogic.Modules.Core.Domain.Services.Ctes;
	using Translogic.Tests;

	public class CteSimconsultasServiceTestCase : BaseTranslogicContainerTestCase
	{
		private CteSimconsultasService _service;
		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();
			this.RealizarRollback = false;
			this.TemTransacao = false;
			this._service = this.Container.Resolve<CteSimconsultasService>();
		}
		#endregion

		[Test]
		public void ObterCteSimconsultas()
		{
			CteSimconsultas cte = this._service.ObterXmlCteDoSimconsultas("35130376302157001296570010000420611005218321");
		} 

		[Test]
		public void ObterCtesSimconsultas()
		{
			IList<string> lista = _service.ObterNaoBaixados();

			foreach (string chaveCte in lista)
			{
				var cte = this._service.ObterXmlCteDoSimconsultas(chaveCte);
				if (cte == null)
				{
					Console.WriteLine("Erro ===>>> {0}", chaveCte);
				}
			}
		}

		[Test]
		public void ProcessarCtesSimconsultas()
		{
			IList<CteSimconsultas> lista = _service.ObterNaoProcessados();

			foreach (CteSimconsultas cte in lista)
			{
				try
				{
					this._service.TransformarXmlCteSimconsultas(cte);
				}
				catch (Exception)
				{
					Console.WriteLine("Erro ===>>> {0}", cte.ChaveCte);
				}
			}
		}
	}
}