﻿namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using ALL.Core.IoC;
    using ALL.TestsSupport.AcessoDados;
    using ALL.TestsSupport.IoC;
    using Core.Domain.DataAccess.Repositories.Diversos;
    using Core.Domain.DataAccess.Repositories.Diversos.CodigoBarras;
    using Core.Domain.Model.Diversos.CodigoBarras;
    using Core.Domain.Model.Diversos.CodigoBarras.Repositories;
    using Core.Domain.Services.Ctes;
    using MvcContrib.Services;
    using NUnit.Framework;
    using Translogic.Core;

    public class CodigoBarrasTestCase : ContainerBaseTestCase
    {
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();

            _codigoBarrasService = Container.Resolve<CodigoBarrasService>();
        }

        #endregion

        private CodigoBarrasService _codigoBarrasService;

        protected override ContainerBase ObterContainer()
        {
            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForTests();

            return TranslogicStarter.Container;
        }

        [Test]
        public void ObterCodigoDeBarras()
        {
            /*IList<bool> codigoBarras = _codigoBarrasService.GerarCodigoBarras("35090700546997000180550020000017320002614216", "CODE128C");
            string valorBarra = String.Empty;

            foreach (var v in codigoBarras)
            {
                valorBarra += (v ? "1" : "0");
            }

            valorBarra = valorBarra.Replace("0", "a");*/
            Console.WriteLine("Teste teste");
        }

        [Test]
        public void GerarPdf()
        {
            try
            {
                _codigoBarrasService.PreencheLayoutPdfCte("0", "35090700546997000180550020000017320002614216");
                MemoryStream filePdf = _codigoBarrasService.ArquivoPdfCte;
                string nomeArquivo = _codigoBarrasService.NomeArquivo;
                filePdf.Seek(0, SeekOrigin.Begin);

                if(!filePdf.Equals(null))
                {
                        MailMessage mailMessage = new MailMessage("henderix@gmail.com", "marcelosr@all-logistica.com");
                        string nmPdf = nomeArquivo + ".pdf";
                        Attachment data = new Attachment(filePdf, nmPdf);
                        mailMessage.Attachments.Add(data);

                        mailMessage.From = new MailAddress("noreply@all-logistica.com");
                        mailMessage.Subject = "CT-e";
                        mailMessage.IsBodyHtml = false;
                        mailMessage.Body = "Envio do CTE";
                        mailMessage.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
                        mailMessage.BodyEncoding      = Encoding.GetEncoding("ISO-8859-1");

                        SmtpClient client = new SmtpClient();
                        client.Send(mailMessage);
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }
    }
}
