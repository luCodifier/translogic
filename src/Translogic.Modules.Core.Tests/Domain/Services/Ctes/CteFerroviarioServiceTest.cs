﻿using System;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario;
using Translogic.Modules.Core.Domain.Model.WebServices.CteFerroviario.Inputs;
using Translogic.Modules.Core.Domain.Services.Ctes.Interface;
using Translogic.Tests;

namespace Translogic.Modules.Core.Tests.Domain.Services.Ctes
{
    public class CteFerroviarioServiceTest : BaseTranslogicContainerTestCase
    {
        private ICteFerroviarioService _cteFerroviarioService;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _cteFerroviarioService = Container.Resolve<ICteFerroviarioService>();
        }
        #endregion

        [Test]
        public void ValidarTrocaDeFluxoComercialParaRealizarUmaCorrecao()
        {
            var request = new InformationElectronicNotificationRequest
            {
                    InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                    {
                            ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                            {
                                StartDate = new DateTime(2019, 03, 01, 16, 00, 00),
                                EndDate = new DateTime(2019, 03, 01, 16, 50, 00)
                            }
                    }
            };

            InformationElectronicNotificationResponse obj = _cteFerroviarioService.InformationElectronicCtes(request);
        }

        [Test]
        public void Informacoes_Eletronicas_MalhaNorte_ComMDFE()
        {
            // Para DEBUG rodar todos como X86 
            // Para deploy no Servidor do CAAL efetuar [Release x86]

            var request = new InformationElectronicNotificationRequest
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        StartDate = new DateTime(2020, 07, 01, 01, 00, 00),
                        EndDate = new DateTime(2020, 07, 01, 10, 50, 00)
                    }
                }
            };

            InformationElectronicNotificationResponse obj = _cteFerroviarioService.InformationElectronicCtesMalhaNorteComMDFE(request);
        }


        [Test]
        public void InformacoesEletronicasMalhaNorteCteMdfeIncluindoMultiplos()
        {
            // Para DEBUG rodar todos como X86 
            // Para deploy no Servidor do CAAL efetuar [Release x86]

            var request = new InformationElectronicNotificationRequest
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        StartDate = new DateTime(2019, 07, 01, 01, 00, 00),
                        EndDate = new DateTime(2019, 07, 01, 10, 50, 00)
                    }
                }
            };

            InformationElectronicNotificationResponse obj = _cteFerroviarioService.InformationElectronicCtesMalhaNorteComMDFE(request);
        }


        [Test]
        public void Informacoes_Eletronicas_MalhaSul_ComMDFE()
        {
            // Para DEBUG rodar todos como X86 
            // Para deploy no Servidor do CAAL efetuar [Release x86]

            var request = new InformationElectronicNotificationRequest
            {
                InformationElectronicBillofLadingRailType = new InformationElectronicBillofLadingRailType
                {
                    ElectronicBillofLadingRailDetail = new ElectronicBillofLadingRailDetail
                    {
                        StartDate = new DateTime(2020, 07, 01, 01, 00, 00),
                        EndDate = new DateTime(2020, 07, 02, 10, 50, 00)
                    }
                }
            };

            InformationElectronicNotificationResponse obj = _cteFerroviarioService.InformationElectronicCtesMalhaSulComMdfe(request);
        }
    }
}