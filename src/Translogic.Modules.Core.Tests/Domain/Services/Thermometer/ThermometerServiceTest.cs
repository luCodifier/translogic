﻿using System;

namespace Translogic.Modules.Core.Tests.Domain.Services.Thermometer
{
    using Translogic.Modules.Core.Domain.Services.Thermometer;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;

    public class ThermometerServiceTest : BaseTranslogicContainerTestCase
    {
        private ThermometerService _service;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = false;
            this._service = this.Container.Resolve<ThermometerService>();
        }
        #endregion

        [Test]
        public void ObterTermometros()
        {
           var listaTermometros = _service.ObterTermometros();
        }

        [Test]
        public void ObterMedicoesTermometros()
        {
            var teste = DateTime.Now;
            var teste2 = teste.ToString("mm-dd-yy");
            var listaMedicoes = _service.ObterMedicoesTermometros(109, DateTime.Now.AddDays(-1), DateTime.Now);
        }
    }
}
