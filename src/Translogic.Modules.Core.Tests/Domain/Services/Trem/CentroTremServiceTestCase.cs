﻿using System.Globalization;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

namespace Translogic.Modules.Core.Tests.Domain.Services.Trem
{
    using Core.Domain.Services.Trem;
    using Interfaces.Trem;
    using Interfaces.Trem.OrdemServico;
    using NUnit.Framework;
    using Translogic.Tests;
    using System.Text;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Translogic.Modules.Core.Domain.Model.Trem;
    using Translogic.Modules.Core.Domain.Model.Trem.Repositories;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

    [TestFixture]
    public class CentroTremServiceTestCase : BaseTranslogicContainerTestCase
    {
        private IComposicaoLocomotivaRepository _composicaoLocomotivaRepository;
        private IComposicaoRepository _composicaoRepository;

        #region SETUP/TEARDOWN

        private ICentroTremService _service;
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;

            _service = Container.Resolve<ICentroTremService>();
            _composicaoLocomotivaRepository = Container.Resolve<IComposicaoLocomotivaRepository>();
            _composicaoRepository = Container.Resolve<IComposicaoRepository>();
        }
        #endregion

        [Test]
        public void TestarOnTimer()
        {
            var oss = _service.ObterOrdensServicoOnTime(new RequisicaoOrdemServicoOnTime
                                                  {
                                                      DataFinalChegada = DateTime.Now,
                                                      DataInicialChegada = DateTime.Now.AddDays(-30)
                                                  });

            foreach (var os in oss.OrdensServico)
            {
                
            }
        }


        [Test]
        public void AtualizarPartidaRealOS()
        {
            _service.AtualizarMovimentacaoTremReal();
        }

        [Test]
        public void ObterLocoPorCOmposicao()
        {
            Composicao composicao = _composicaoRepository.ObterPorId(3204111);
            var comp = _composicaoLocomotivaRepository.ObterPorComposicao(composicao);
            IList<ComposicaoLocomotiva> compAux = new List<ComposicaoLocomotiva>();

            foreach (var composicaoLocomotiva in comp.Where(composicaoLocomotiva => compAux.Count(a => a.Id == composicaoLocomotiva.Id) == 0))
                compAux.Add(composicaoLocomotiva);
            Console.Write(comp.Count.ToString(CultureInfo.InvariantCulture));
        }

    }
}
