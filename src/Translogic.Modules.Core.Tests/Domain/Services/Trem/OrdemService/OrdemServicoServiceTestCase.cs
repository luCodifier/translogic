namespace Translogic.Modules.Core.Tests.Domain.Services.Trem.OrdemService
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Runtime.Serialization;
	using System.Text;
	using ALL.Core.Extensions;
	using ALL.Core.IoC;
	using ALL.TestsSupport.IoC;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso.Repositories;
	using Core.Domain.Model.Trem.OrdemServico;
	using Core.Domain.Model.Trem.OrdemServico.Repositories;
	using Core.Domain.Model.Via;
	using Core.Domain.Model.Via.Circulacao.Repositories;
	using Core.Domain.Model.Via.Repositories;
	using Interfaces.Trem;
	using Interfaces.Trem.OrdemServico;
	using NUnit.Framework;
	using Translogic.Core;
	
    public class OrdemServicoServiceTestCase : ContainerBaseTestCase
	{
		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();

			svc = Container.Resolve<ICentroTremService>();
		    ordemServicoRepository = Container.Resolve<IOrdemServicoRepository>();
		    rotaRepository = Container.Resolve<IRotaRepository>();
		    situacaoOrdemServicoRepository = Container.Resolve<ISituacaoOrdemServicoRepository>();
            usuarioRepository = Container.Resolve<IUsuarioRepository>();
            areaOperacionalRepository = Container.Resolve<IAreaOperacionalRepository>();
		}

		#endregion

		private ICentroTremService svc;
        private IOrdemServicoRepository ordemServicoRepository;
        private IRotaRepository rotaRepository;
        private ISituacaoOrdemServicoRepository situacaoOrdemServicoRepository;
        private IUsuarioRepository usuarioRepository;
        private IAreaOperacionalRepository areaOperacionalRepository;

        protected override ContainerBase ObterContainer()
		{
			TranslogicStarter.Initialize();
			TranslogicStarter.SetupForTests();

			return TranslogicStarter.Container;
		}

		[Test]
        public void ObterPorSituacaoEDataPartidaPrevistaOficial()
		{
			DateTime inicial = DateTime.Now.Date;
			DateTime final = inicial.AddDays(1);

			IList<OrdemServicoDto> ordens = svc.ObterPorSituacaoEDataPartidaPrevistaOficial(SituacaoOrdemServicoEnum.EmPlanejamento, "L10", "LIC", null, inicial, final);

            DataContractSerializer dataContractSerializer = new DataContractSerializer(ordens.GetType());

            String text;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                try
                {
                    dataContractSerializer.WriteObject(memoryStream, ordens);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                byte[] data = new byte[memoryStream.Length];

                Array.Copy(memoryStream.GetBuffer(), data, data.Length);

                text = Encoding.UTF8.GetString(data);
            }

		    Console.WriteLine(text);
		}

        [Test]
        public void NovaOrdemServico()
        {
            OrdemServico ordemServico = new OrdemServico();

            ordemServico.Prefixo = "R01";
            ordemServico.UsuarioCadastro = usuarioRepository.ObterPorCodigo("63031410");
            ordemServico.DataCadastro = DateTime.Now;
            ordemServico.DataPartidaPrevistaGrade = DateTime.Now.AddMinutes(2.5);
            ordemServico.DataChegadaPrevistaGrade = DateTime.Now.AddHours(13.89);
            ordemServico.DataChegadaPrevistaOficial = null;
            ordemServico.DataPartidaPrevistaOficial = ordemServico.DataPartidaPrevistaGrade;
            ordemServico.Rota = rotaRepository.ObterPorId(573);
            ordemServico.Origem = ordemServico.Rota.Origem.EstacaoMae;
            ordemServico.Destino = ordemServico.Rota.Destino.EstacaoMae; 
            ordemServico.QtdeVagoesCarregados = 12;
            ordemServico.QtdeVagoesVazios = 7;
            ordemServico.Situacao =  situacaoOrdemServicoRepository.ObterPorId((int)SituacaoOrdemServicoEnum.EmPlanejamento);
            ordemServicoRepository.Inserir(ordemServico);
        }

        [Test]
        public void AtualizarOrdemServico()
        {
            OrdemServico ordemServico = ordemServicoRepository.ObterPorId(1394589);

            ordemServico.QtdeVagoesCarregados = 123;

            ordemServicoRepository.Atualizar(ordemServico);
        }

        [Test]
        public void ObterFichaTrem()
        {
            FichaTremDto fichaTremDto = svc.ObterFichaTrem(1394503);

            Console.WriteLine(fichaTremDto.NomeMaquinista);
        }
	}
}