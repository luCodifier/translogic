using Translogic.Modules.Core.Interfaces.EquipamentosCampo.HotBox;

namespace Translogic.Modules.Core.Tests.Domain.Services.Acesso
{
    using System;
    using System.IO;
    using System.Xml;

    using ALL.Core.Dominio;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel;
    using Translogic.Tests;

    [TestFixture]
    public class ColdWheelTestCase : BaseTranslogicContainerTestCase
    {
        //private VagaoMangaService _service;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            //_service = Container.Resolve<VagaoMangaService>();
        }
        #endregion

        [Test]
        public void GerarArquivoColdWheel()
        {
            var content = File.ReadAllText(@"c:\Temp\coldwheel.txt");
            var result = ColdWheelFile.GerarArquivoColdWheel(content);
        }

        [Test]
        public void GerarArquivoHotBox()
        {
            DirectoryInfo di = new DirectoryInfo(@"c:\Temp\");
            FileInfo[] arquivos = di.GetFiles("*.txt", SearchOption.AllDirectories);

            foreach (var fileInfo in arquivos)
            {
                // Get a StreamReader with OpenText.
                using (StreamReader reader = fileInfo.OpenText())
                {
                    string texto = reader.ReadToEnd();

                    var result = HotBoxFile.GerarArquivoColdWheel(texto);

                    if (result == null)
                    {
                    }
                }
            }
        }
    }
}
