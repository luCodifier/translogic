namespace Translogic.Modules.Core.Tests.Domain.Services.Acesso
{
    using System;
    using ALL.Core.Dominio;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.Trem;
    using Translogic.Tests;

    [TestFixture]
    public class VagaoMangaServiceTestCase : BaseTranslogicContainerTestCase
    {
        private VagaoLimitePesoService _service;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<VagaoLimitePesoService>();
        }
        #endregion

        [Test]
        public void ObterPorFiltro()
        {
            _service.ObterPorFiltro(new DetalhesPaginacao(), DateTime.Now.AddDays(-10), DateTime.Now, null, null, null);
        }
    }
}
