namespace Translogic.Modules.Core.Tests.Domain.Services.Acesso
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;
    using Core.Domain.Services.Acesso;
    using Translogic.Modules.Core.Domain.Model.Diversos.Cte;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using NUnit.Framework;
    using Translogic.Tests;
    using Translogic.Modules.Core.Util;

    [TestFixture]
	public class MenuServiceTestCase : BaseTranslogicContainerTestCase
	{
		private MenuService _service;
		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<MenuService>();
		}
		#endregion

		[Test]
		public void TesteString()
		{
		    _service.ObterItensAtivos("MIGRA");
		}
	}
}
