﻿namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
    using System;
    using Core.Domain.Model.FluxosComerciais.Dcls;
    using Core.Domain.Services.FluxosComerciais.NfePetrobras;
    using NUnit.Framework;
    using Translogic.Tests;

    public class NfePetrobrasTestCase : BaseTranslogicContainerTestCase
    {
        private NfePetrobrasService _service;
        
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            _service = Container.Resolve<NfePetrobrasService>();
        }

        [Test]
        public void TestarDownloadNotasBr()
        {
            var input = new NfePetrobrasRequest
            {
                Cnpj = "01258944003818",
                DataEmissao = "17032014",
                HoraEmissaoFinal = "2100",
                HoraEmissaoInicial = "1900",
                Token = "70e5673e3ea2fc93dfb342491637166b532f6a11cbc102941b",
                Url = "http://cnhomolog.br-petrobras.com.br/ws/iit/nf/consulta"
            };

            _service.ObterNotasBr(input);
        }
    }
}
