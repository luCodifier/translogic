namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Text;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class GerarArquivoCteEnvioServiceTestCase : BaseTranslogicContainerTestCase
	{
		private GerarArquivoCteEnvioService _service;
		private ICteRepository _cteRepository;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<GerarArquivoCteEnvioService>();
			_cteRepository = Container.Resolve<ICteRepository>();
		}
		#endregion

		[Test]
		public void GerarArquivo()
		{
            Cte cte = _cteRepository.ObterPorId(4860748);

			// Chamar a execucao do servico
			StringBuilder arquivo = _service.Executar(cte);


		}
	}
}