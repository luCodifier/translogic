﻿namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;

    [TestFixture]
    public class GravarConfigCorrecaoCteEnvioServiceTestCase : BaseTranslogicContainerTestCase
	{
        private GravarConfigCorrecaoCteService _service;
        private ICteRepository _cteRepository;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<GravarConfigCorrecaoCteService>();
            _cteRepository = Container.Resolve<ICteRepository>();
        }
        #endregion

        [Test]
        public void GeracaoDaCartaDeCorrecaoEletronica()
        {
            Cte cte = _cteRepository.ObterPorId(2010918);
            _service.Executar(cte);
        }
    }
}
