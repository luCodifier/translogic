namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class CteLogServiceTestCase : BaseTranslogicContainerTestCase
	{
		private CteLogService _service;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<CteLogService>();
		}
		#endregion

		[Test]
		public void GerarLog()
		{
			_service.InserirLogErro(null, "Inserindo a informação de teste com id nulo");

			Cte cte = new Cte {Id = 61};
			_service.InserirLogErro(cte, "Inserindo log de erro teste");
			_service.InserirLogInfo(cte, "Inserindo log de informação teste");
		}
	}
}