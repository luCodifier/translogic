namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;

    public class GerarArquivoCteCancelamentoServiceTestCase : BaseTranslogicContainerTestCase
    {
        private ICteRepository _cteRepository;
        private GerarArquivoCteComplementoService _service;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = true;
            TemTransacao = false;
            _cteRepository = Container.Resolve<ICteRepository>();
            _service = Container.Resolve<GerarArquivoCteComplementoService>();
        }
        #endregion

        [Test]
        public void InserirFilaEnvio()
        {
            Cte cteComplementar = _cteRepository.ObterPorId(5372182);
            _service.Executar(cteComplementar);
        }
    }
}