using System;
using System.Collections.Generic;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class GravarConfigCteComplementoServiceTestCase : BaseTranslogicContainerTestCase
	{
		private GravarConfigCteComplementoService _service;
		private ICteRepository _cteRepository;
        private CteService _cteService;
        private FilaProcessamentoService _serviceFilaProc;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<GravarConfigCteComplementoService>();
            _cteService = Container.Resolve<CteService>();
			_cteRepository = Container.Resolve<ICteRepository>();
            _serviceFilaProc = Container.Resolve<FilaProcessamentoService>();
		}
		#endregion

		[Test]
		public void TestarGeracaoCteComplementar()
		{
            var listaErro = new List<int>();
            try
            {
                List<int> lista = new List<int>();
                lista.Add(7048273); // 4590854 , 7121755, 7017931

                foreach (var idCte in lista)
                {
                    try
                    {
                        Cte cte = _cteRepository.ObterPorId(idCte);
                        ////Cte cte = _cteRepository.ObterPorId(3844883);
                        _service.Executar(cte);
                        ////_cteService.InserirConfigCteComplemento(cte);
                        _serviceFilaProc.InserirCteEnvioFilaProcessamento(cte);
                    }
                    catch (Exception)
                    {
                        listaErro.Add(idCte);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw;
            }

            Console.Write(listaErro.Count.ToString());
		}
	}
}