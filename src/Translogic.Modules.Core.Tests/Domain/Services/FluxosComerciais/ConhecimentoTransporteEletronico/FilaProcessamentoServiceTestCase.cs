namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	public class FilaProcessamentoServiceTestCase : BaseTranslogicContainerTestCase
	{
		private FilaProcessamentoService _service;
		private ICteRepository _cteRepository;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = true;
			TemTransacao = false;
            _cteRepository = Container.Resolve<ICteRepository>();
            _service = Container.Resolve<FilaProcessamentoService>();
            ////_senderQueueManagerService = Container.Resolve<SenderQueueManagerService>();
		}
		#endregion


		[Test]
		public void TestarInsert()
		{
			// Cte cte = _cteRepository.ObterPorId(454);
            var retorno = _service.ObterRetornoCteAutorizadoPorChave("41140701258944000550570020008052831909775940");
		}

		[Test]
		public void InserirFilaEnvio()
		{
            ////senderQueueManagerService.Inicializar();

            Cte cte = _cteRepository.ObterPorId(5353451);
			_service.InserirCteEnvioFilaProcessamento(cte);
		}

		[Test]
		public void TestarRecebimento()
		{
			Vw209ConsultaCte consultaCte = _service.ObterRetornoCteProcessadoPorIdLista(118);
		}

		[Test]
		public void TestarGeracaoPdf()
		{
			Cte cte = _cteRepository.ObterPorId(225456);
			_service.InserirGeracaArquivosCteNoServidorFtp(cte);
			// _service.InserirGeracaoArquivosCteNoServidorEmail(cte);
			
		}

		[Test]
		public void TestarGeracaoPdfPorImpressao()
		{
			Cte cte = _cteRepository.ObterPorId(260257);
			_service.InserirGeracaoPdfPorImpressao(cte);
		}
	}
}