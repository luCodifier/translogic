using System;
using System.Collections.Generic;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Core.Domain.Model.Diversos.Cte;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using Translogic.Tests;
    using Util;

    [TestFixture]
    public class GravarConfigCteEnvioServiceTestCase : BaseTranslogicContainerTestCase
    {
        private GravarConfigCteEnvioService _service;
        private CteService _cteService;
        private ICteRepository _cteRepository;
        private ICteLogRepository _cteLogRepository;
        private ICteEmpresasRepository _cteEmpresasRepository;
        private IDespachoTranslogicRepository _despachoTranslogicRepository;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<GravarConfigCteEnvioService>();
            _cteService = Container.Resolve<CteService>();
            _cteRepository = Container.Resolve<ICteRepository>();
            _cteLogRepository = Container.Resolve<ICteLogRepository>();
            _cteEmpresasRepository = Container.Resolve<ICteEmpresasRepository>();
            _despachoTranslogicRepository = Container.Resolve<IDespachoTranslogicRepository>();

            ////HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
        }
        #endregion

        [Test]
        public void TestarGeracaoCte()
        {
            var ctes = new List<int> { 7048261 };
            var listaCorrigidos = new List<int>();
            var listaComErros = new Dictionary<int, Exception>();
            foreach (var cteId in ctes)
            {
                try
                {
                    var cte = _cteRepository.ObterPorId(cteId);
                    _service.Executar(cte);
                    _cteService.InserirConfigCteEnvio(cte);
                    listaCorrigidos.Add(cteId);
                }
                catch (Exception ex)
                {
                    listaComErros.Add(cteId, ex);
                }
            }

            Assert.IsTrue(listaComErros.Count == 0);
            Assert.IsTrue(listaCorrigidos.Count == ctes.Count);
        }

        [Test]
        public void AtualizarValorCte()
        {
            Cte cte;
            cte = _cteRepository.ObterPorId(2911564); //// 1,079.07
            _service.CalcularValorCte(cte);

            cte = _cteRepository.ObterPorId(2911563);
            _service.CalcularValorCte(cte);

            cte = _cteRepository.ObterPorId(2911562);
            _service.CalcularValorCte(cte);
        }

        [Test]
        public void TestarParserEmail()
        {
            string teste = string.Empty;

            // string email = "marcelosr@all-logistica.com;   tianena@teste.com.br; outroteste@hotmail.com ";
            string email = "";
            string[] lista = _service.Dummy(email);
            foreach (string s in lista)
            {
                teste = s;
            }
        }

        [Test]
        public void TestarGeracaoLogCte()
        {
            Cte cte = _cteRepository.ObterPorId(2911386);
            _cteLogRepository.InserirLogInfo(cte, "teste", "teste");
        }

        [Test]
        public void TestarInclusaoCteEmpresa()
        {
            Cte cte = _cteRepository.ObterPorId(2911397);
            //var despacho = cte.Despacho;
            //Cte cte2 = _cteRepository.ObterPorIdCte(2911397);

            //var teste = cte.Despacho.Id;
            ////var despacho = _despachoTranslogicRepository.ObterPorId(cte.Despacho.Id ?? 0); // ObterDespachoPeloNumeroDespachoSerie(cte.Despacho.Id ?? 0, cte.Despacho.SerieDespacho.Id.ToString());  

            //var despacho = cte.Despacho;
            //var serieDespachoUf = cte.Despacho.SerieDespachoUf;
            ////var despachoData = despacho.DataDespacho;

            CteEmpresas cteEmp = new CteEmpresas() { Cte = cte };

            //var teste2 = teste;
            //var despacho2 = despacho;
            //var despachoData2 = despachoData;
            try
            {
                _cteEmpresasRepository.InserirOuAtualizar(cteEmp);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Test]
        public void TestarInserirComposicaoFreteCte()
        {
            var lista = new List<int>();

            foreach (var item in lista)
            {
                try
                {
                    Cte cte = _cteRepository.ObterPorId(item);
                    _service.InserirComposicaoFreteCteTeste(cte);
                }
                catch (Exception ex)
                {
                    Assert.Fail("erro no item: {0},{1}", item, ex.Message);
                }
            }


        }
    }
}
