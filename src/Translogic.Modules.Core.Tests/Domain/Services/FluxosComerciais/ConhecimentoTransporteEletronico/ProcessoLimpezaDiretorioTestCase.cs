namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	public class EfetuarLimpezaDiretorioCteServiceTestCase : BaseTranslogicContainerTestCase
	{
		private EfetuarLimpezaDiretorioCteService _service;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<EfetuarLimpezaDiretorioCteService>();
		}
		#endregion

		[Test]
		public void TestarGeracaoCte()
		{
			_service.Executar();
		}
	}
}