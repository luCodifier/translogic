namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using Core.Domain.Services.FluxosComerciais;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class NfeBoServiceTestCase : BaseTranslogicContainerTestCase
	{
		private NfeBoService _service;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<NfeBoService>();
			
		}
		#endregion

		[Test]
		public void ObtemNfeInserePooling()
		{
			_service.InserirNfeBoPooling();
		}

		[Test]
		public void ProcessarNfePooling()
		{
			_service.ProcessarNfePooling("31");
		}

		[Test]
		public void ProcessarNfePoolingOutrosEstados()
		{
			_service.ProcessarNfePooling("OE");
		}
	}
}