﻿namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using Core.Domain.Model.Diversos.Cte;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Core.Domain.Services.Ctes;
    using NUnit.Framework;
    using System;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
    using Translogic.Tests;
    using Util;

    [TestFixture]
    public class GravarConfigCteAnulacaoNContribuinteServiceTestCase : BaseTranslogicContainerTestCase
    {
        private GravarConfigCteAnulacaoNContribuinteService _service;
        private ICteRepository _cteRepository;
        private ICteLogRepository _cteLogRepository;
        private ICteEmpresasRepository _cteEmpresasRepository;
        private IDespachoTranslogicRepository _despachoTranslogicRepository;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<GravarConfigCteAnulacaoNContribuinteService>();
            _cteRepository = Container.Resolve<ICteRepository>();
            _cteLogRepository = Container.Resolve<ICteLogRepository>();
            _cteEmpresasRepository = Container.Resolve<ICteEmpresasRepository>();
            _despachoTranslogicRepository = Container.Resolve<IDespachoTranslogicRepository>();
        }
        #endregion

        [Test]
        public void TestarGeracaoCte()
        {
            //Cte cte = _cteRepository.ObterPorChave("51170224962466000136570020006377431905806350");
            var cte = _cteRepository.ObterPorId(2911447);  //ObterPorChave("51170224962466000136570020006377441905806358");
            _service.Executar(cte);
        }
    }
}
