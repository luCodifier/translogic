using ALL.Core.Dominio;
using Translogic.Modules.Core.Domain.Model.Estrutura;
using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva.Repositories;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using Core.Domain.Model.Acesso;
    using Core.Domain.Model.Diversos.Cte;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
    using Core.Domain.Services.Ctes;
    using Core.Domain.Services.FluxosComerciais;
    using NUnit.Framework;
    using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
    using Translogic.Tests;
    using Util;
    using Translogic.Modules.Core.Domain.Model.Codificador.Repositories;
    using Translogic.Core.Infrastructure.Web;

    [TestFixture]
    public class CteServiceTestCase : BaseTranslogicContainerTestCase
    {
        private CteService _service;
        private CteLogService _cteLogService;

        private ICteRepository _cteRepository;
        private ICteDetalheRepository _cteDetalheRepository;
        private ICteInterfacePdfConfigRepository _cteInterfacePdfConfigRepository;
        private ICteInterfaceXmlConfigRepository _cteInterfaceXmlConfigRepository;
        private ICteInterfaceEnvioEmailRepository _cteInterfaceEnvioEmailRepository;
        private ICteArquivoRepository _cteArquivoRepository;
        private ICteComunicadoRepository _cteComunicadoRepository;
        private ICteStatusRepository _cteStatusRepository;
        private ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
        private IConfiguracaoTranslogicRepository _configuracaoTranslogicRepository;
        private ICteEmpresasRepository _cteEmpresasRepository;
        private IEmpresaClienteRepository _empresaClienteRepository;
        private IComposicaoLocomotivaRepository _composicaoLocomotivaRepository;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<CteService>();
            _cteLogService = Container.Resolve<CteLogService>();

            _cteEmpresasRepository = Container.Resolve<ICteEmpresasRepository>();
            _empresaClienteRepository = Container.Resolve<IEmpresaClienteRepository>();
            _cteEmpresasRepository = Container.Resolve<ICteEmpresasRepository>();
            _cteRepository = Container.Resolve<ICteRepository>();
            _cteInterfacePdfConfigRepository = Container.Resolve<ICteInterfacePdfConfigRepository>();
            _cteInterfaceXmlConfigRepository = Container.Resolve<ICteInterfaceXmlConfigRepository>();
            _cteInterfaceEnvioEmailRepository = Container.Resolve<ICteInterfaceEnvioEmailRepository>();
            _cteArquivoRepository = Container.Resolve<ICteArquivoRepository>();
            _cteDetalheRepository = Container.Resolve<ICteDetalheRepository>();
            _cteComunicadoRepository = Container.Resolve<ICteComunicadoRepository>();
            _cteStatusRepository = Container.Resolve<ICteStatusRepository>();
            _cteInterfaceEnvioSapRepository = Container.Resolve<ICteInterfaceEnvioSapRepository>();
            _configuracaoTranslogicRepository = Container.Resolve<IConfiguracaoTranslogicRepository>();
            _composicaoLocomotivaRepository = Container.Resolve<IComposicaoLocomotivaRepository>();

            ////HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
        }

        #endregion

        [Test]
        public void TestarObterPorHost()
        {
            string hostname = Dns.GetHostName();
            IList<Cte> lista = _service.ObterPoolingEnvioComplementadoPorServidor(hostname);
            int count = lista.Count;
        }

        [Test]
        public void TestarInserirConfigCteEnvio()
        {
            var cte = _cteRepository.ObterCteVirtual(new DetalhesPaginacao(), Convert.ToDateTime("01/04/2014 16:06:16"),
                                                     Convert.ToDateTime("01/05/2014 16:06:16"), "", 0, "", "", "", "",
                                                     "", "", "");

            // Executa a grava��o do arquivo
            // _gravarConfigCteEnvioService.Executar(cte);

            // _service.InserirConfigCteEnvio(cte);
        }

        [Test]
        public void TestarEnvioFila()
        {
            IList<Cte> lista = new List<Cte>();
            var cte = _cteRepository.ObterPorId(5504805);
            //var cte3 = _cteRepository.ObterPorId(2911401);
            //var cte2 = _cteRepository.ObterPorId(2911400);

            lista.Add(cte);
            //lista.Add(cte3);
            //lista.Add(cte2);

            _service.InserirPoolingEnvioCte(lista, Dns.GetHostName());
        }

        [Test]
        public void TestarProcessamento()
        {
            // IList<Cte> listaParaProcessar = _service.ObterCtesProcessarEnvio(1);
            var config = _configuracaoTranslogicRepository.ObterPorId("CTE_XML_PATH");
            var interf = _cteInterfaceXmlConfigRepository.ObterPorId(4396464);
            _service.ImportarArquivoXml(interf, config.Valor);
            // _service.InserirPoolingEnvioCte(listaParaProcessar, Dns.GetHostName());
        }

        [Test]
        public void TestarInsertDatabaseInput()
        {
        }

        [Test]
        public void TestarDosPoolingEnvio()
        {
            string hostName = Dns.GetHostName();
            IList<Cte> lista = _service.ObterPoolingEnvioPorServidor(hostName);
        }

        [Test]
        public void TestarLeituraBinaria()
        {
            byte[] valor1 = Tools.ConvertBinaryFileToByteArray(@"C:\Temp\Comprovante_Luciana.pdf");
            byte[] valor2 = Tools.ConvertBinaryFileToByteArray(@"C:\Temp\TransfPoupanca.pdf");
        }

        [Test]
        public void TestarObtencaoCte()
        {
            var dataInicial = new DateTime(2012, 01, 13);
            var dataFinal = new DateTime(2012, 01, 13, 23, 59, 59);
            var result = _service.RetornaCteMonitoramento(new DetalhesPaginacao(), dataInicial, dataFinal, null, 0, null, "41130901258944000550570020005383531933583647", null, null, null, null, null, null, true);
        }
        // ObterCteVirtual

        [Test]
        public void TestarObtencaoCteVirtual()
        {
            var dataInicial = new DateTime(2012, 01, 13);
            var dataFinal = new DateTime(2012, 01, 13, 23, 59, 59);
            var result = _cteRepository.ObterCteVirtual(new DetalhesPaginacao(), dataInicial, dataFinal, null, 0, null, "35131202502844000166570020001596511935214274", null, null, null, null, null);
        }

        [Test]
        public void ObterCtesProcessarEnvioSap()
        {
            IList<CteInterfaceEnvioSap> lista = _service.ObterCtesProcessarEnvioSap(2);
        }

        [Test]
        public void ObterDadosPdf()
        {
            string hostName = "SIS-FWHS02";
            IList<CteInterfacePdfConfig> listaCtesNaoImportados = _service.ObterCtesImportarPdf(hostName);
            int count = listaCtesNaoImportados.Count;
        }

        [Test]
        public void InserirNaFilaConfig()
        {
            Cte cte = _cteRepository.ObterPorId(227468);

            _service.Dummy(cte);
        }

        [Test]
        public void TestarImportacaoArquivoXML()
        {
            string xmlPath = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\";

            IList<string> listaCteChave = ListaCteChave();
            string fileName;
            string processado;
            // CteArquivo arquivo;
            Cte cte;

            foreach (string chave in listaCteChave)
            {
                cte = _cteRepository.ObterPorChave(chave);
                fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\" + cte.Chave + ".xml";
                bool encontrado = false;
                try
                {
                    if (File.Exists(fileName))
                    {
                        _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                        processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                        // Move o arquivo para o dir de processados
                        File.Move(fileName, processado);
                        encontrado = true;
                    }

                    if (!encontrado)
                    {
                        fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\XML\" + cte.Chave + ".xml";

                        if (File.Exists(fileName))
                        {
                            _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                            // string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
                            processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                            // Move o arquivo para o dir de processados
                            File.Move(fileName, processado);
                            encontrado = true;
                        }
                    }

                    if (!encontrado)
                    {
                        fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\XML_PROCESSADOS\201204\" + cte.Chave + ".xml";

                        if (File.Exists(fileName))
                        {
                            _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                            // string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
                            processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                            // Move o arquivo para o dir de processados
                            File.Move(fileName, processado);
                            encontrado = true;
                        }
                    }

                    if (!encontrado)
                    {
                        fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\XML_PROCESSADOS\201203\" + cte.Chave + ".xml";

                        if (File.Exists(fileName))
                        {
                            _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                            // string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
                            processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                            // Move o arquivo para o dir de processados
                            File.Move(fileName, processado);
                            encontrado = true;
                        }
                    }

                    if (!encontrado)
                    {
                        fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\XML_PROCESSADOS\201205\" + cte.Chave + ".xml";

                        if (File.Exists(fileName))
                        {
                            _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                            // string processado = xmlPah + "XmlProcessados\\" + cte.Chave + ".xml";
                            processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Xml, cte.DataHora) + cte.Chave + ".xml";

                            // Move o arquivo para o dir de processados
                            File.Move(fileName, processado);
                            encontrado = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Erro: {0} - {1}", fileName, ex.Message));
                }
            }
        }

        [Test]
        public void TestarImportarArquivoPdf()
        {
            string xmlPath = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\";

            IList<string> listaCteChave = ListaCteChave();
            string fileName;
            string processado;
            CteArquivo arquivo;
            Cte cte;
            bool encontrado = false;

            foreach (string chave in listaCteChave)
            {
                cte = _cteRepository.ObterPorChave(chave);
                fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\" + cte.Chave + ".pdf";
                encontrado = false;

                if (File.Exists(fileName))
                {
                    _cteArquivoRepository.InserirOuAtualizarPdf(cte, fileName);
                    processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                    // Move o arquivo para o dir de processados
                    File.Move(fileName, processado);
                    encontrado = true;
                }

                if (!encontrado)
                {
                    fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\PDF_PROCESSADOS\" + cte.Chave + ".pdf";

                    if (File.Exists(fileName))
                    {
                        _cteArquivoRepository.InserirOuAtualizarPdf(cte, fileName);
                        processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                        // Move o arquivo para o dir de processados
                        File.Move(fileName, processado);
                        encontrado = true;
                    }
                }

                if (!encontrado)
                {
                    fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\PDF_PROCESSADOS\201203\" + cte.Chave + ".pdf";

                    if (File.Exists(fileName))
                    {
                        _cteArquivoRepository.InserirOuAtualizarPdf(cte, fileName);
                        processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                        // Move o arquivo para o dir de processados
                        File.Move(fileName, processado);
                        encontrado = true;
                    }
                }

                if (!encontrado)
                {
                    fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\PDF_PROCESSADOS\201204\" + cte.Chave + ".pdf";

                    if (File.Exists(fileName))
                    {
                        _cteArquivoRepository.InserirOuAtualizarPdf(cte, fileName);
                        processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                        // Move o arquivo para o dir de processados
                        File.Move(fileName, processado);
                        encontrado = true;
                    }
                }

                if (!encontrado)
                {
                    fileName = @"\\alltlbeta3\FTP_CONFIG\Arquivo_Pdf\XML_PROCESSADOS\201205\" + cte.Chave + "pdf";

                    if (File.Exists(fileName))
                    {
                        _cteArquivoRepository.InserirOuAtualizarXml(cte, fileName);
                        processado = _service.ObterDiretorioProcessado(xmlPath, TipoArquivoEnvioEnum.Pdf, cte.DataHora) + cte.Chave + ".pdf";

                        // Move o arquivo para o dir de processados
                        File.Move(fileName, processado);
                        encontrado = true;
                    }
                }
            }
        }

        [Test]
        public void TestarEnvioEmail()
        {
            CteInterfaceEnvioEmail envio = new CteInterfaceEnvioEmail(); // _cteInterfaceEnvioEmailRepository.ObterPorId(4944409);
            envio.Cte = _cteRepository.ObterCtePorIdHql(7419915);
            envio.Chave = envio.Cte.Chave;
            _service.EnviarEmail(envio, "mail.office365.com", 993, "admnfe@all-logistica.com", "123456@b");
            // _service.EnviarEmail(envio, "smtp.office365.com", 587, "allcte@all-logistica.com", "All@1234!");////"1q2w3e4r5t!Q@W#E$R%T"
        }

        [Test]
        public void GravarEnvioEmail()
        {
            IList<ImportacaoEmail> listaImportacao = ListaImportacaoEmail();

            foreach (ImportacaoEmail importacaoEmail in listaImportacao)
            {
                _service.GravarInformacoesEnvioEmail(importacaoEmail.Cte, importacaoEmail.EmailNfe, importacaoEmail.EmailXml);
            }
        }

        [Test]
        public void TestarInterfaceEnvioEmail()
        {
            int[] listaCte = new int[]
			           	{
52632
			           	};
            CteInterfaceEnvioEmail interfaceEnvioEmail = null;
            foreach (int idCte in listaCte)
            {
                interfaceEnvioEmail = _cteInterfaceEnvioEmailRepository.ObterPorId(idCte);
                // _service.EnviarEmail(interfaceEnvioEmail, "190.1.0.15", 25, "allcte@all-logistica.com");
                _service.EnviarEmail(interfaceEnvioEmail, "smtp.office365.com", 587, "allcte@all-logistica.com", "All@1234!");
            }
        }

        [Test]
        public void SalvarArquivoXml()
        {
            int[] listaCte = null;

            CteArquivo arquivo = null;
            Cte cte = null;
            string fileName;

            foreach (int idCte in listaCte)
            {
                cte = _cteRepository.ObterPorId(idCte);
                arquivo = _cteArquivoRepository.ObterPorId(idCte);

                fileName = @"C:\Temp\ArquivosXML\" + cte.Chave + ".xml";
                Tools.SaveStringToFile(fileName, arquivo.ArquivoXml);
            }
        }

        [Test]
        public void SalvarArquivoPdf()
        {
            int[] listaCte = null;

            CteArquivo arquivo = null;
            Cte cte = null;
            string fileName;

            foreach (int idCte in listaCte)
            {
                cte = _cteRepository.ObterPorId(idCte);
                arquivo = _cteArquivoRepository.ObterPorId(idCte);

                fileName = @"C:\Temp\ArquivosPDF\20120824 - Brado\" + cte.Chave + ".pdf";
                Tools.SaveByteArrayToFile(fileName, arquivo.ArquivoPdf);
            }
        }

        [Test]
        public void SalvarArquivoPdfPorChave()
        {
            List<string> chaveCte = ListaCteChave();

            CteArquivo arquivo = null;
            Cte cte = null;
            string fileName;

            foreach (string chave in chaveCte)
            {
                cte = _cteRepository.ObterPorChave(chave);
                if (cte != null)
                {
                    arquivo = _cteArquivoRepository.ObterPorId(cte.Id);
                    fileName = @"C:\Temp\ArquivosPDF\" + cte.Chave + ".pdf";
                    Tools.SaveByteArrayToFile(fileName, arquivo.ArquivoPdf);
                }
            }
        }

        [Test]
        public void SalvarArquivoXmlPorChave()
        {
            List<string> chaveCte = ListaCteChave();

            CteArquivo arquivo = null;
            Cte cte = null;
            string fileName;

            foreach (string chave in chaveCte)
            {
                cte = _cteRepository.ObterPorChave(chave);
                if (cte != null)
                {
                    arquivo = _cteArquivoRepository.ObterPorId(cte.Id);
                    fileName = @"C:\Temp\ArquivosXML\" + cte.Chave + ".xml";
                    Tools.SaveStringToFile(fileName, arquivo.ArquivoXml);
                }
            }
        }

        [Test]
        public void TestedoYano()
        {
            _service.SalvarManutencaoCte(302440, new Usuario { Id = 43680 });
        }

        [Test]
        public void TesteAnulacao()
        {
            var cte1 = _cteRepository.ObterPorChave("51131224962466000136570020003662091984120575");
            // _service.InserirInterfaceEnvioSap(cte1, new CteStatusRetorno { Id = 100 });
            // var cte2 = _cteRepository.ObterPorChave("50131024962466000560570020000197551903214014");
            _service.AutorizarCancelamentoRejeitado(cte1, new Usuario { Id = 43680 });
            // _service.AutorizarCancelamentoRejeitado(cte2, new Usuario { Id = 43680 });
        }

        [Test]
        public void EnviarNotaAnulacaoSAP()
        {
            Usuario usuario = new Usuario { Id = 51471 };
            foreach (var chave in ListaCteChave())
            {
                var cte = _cteRepository.ObterPorChave(chave);
                _service.EnviarNotaAnulacaoSAP(cte, new Usuario { Id = 43680 });
            }
        }

        [Test]
        public void TestarPdfMerger()
        {
            CteArquivo arquivo = _cteArquivoRepository.ObterPorId(420450);
            CteComunicado comunicado = _cteComunicadoRepository.ObterPorId(6);

            // Inst�ncia de objeto
            PdfMerge merge = new PdfMerge();
            merge.AddFile(arquivo.ArquivoPdf);
            merge.AddFile(comunicado.ComunicadoPdf);
            merge.AddFile(Tools.ByteArrayToStream(arquivo.ArquivoPdf));


            string fileName = @"C:\Temp\Resultado.pdf";

            Stream streamAux = merge.Execute();
            Tools.CopyStreamToFileStream(fileName, streamAux);
        }

        /// <summary>
        /// Closar CT-e = Enviar apenas para o SAP as informa��es de cancelamento. N�o manda para a SEFAZ
        /// </summary>
        [Test]
        public void GlosarCte()
        {
            IList<string> listaChave = ListaCteChave();
            foreach (string chave in listaChave)
            {
                Cte cte = _cteRepository.ObterPorChave(chave);

                Usuario usuario = new Usuario { Id = 24386 }; // Usuario Joel Barbosa
                CteMotivoCancelamento motivoCancelamento = new CteMotivoCancelamento { Id = 23 };
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 30 });
                _cteStatusRepository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 220 });
                cte.SituacaoAtual = SituacaoCteEnum.Cancelado;
                cte.CteMotivoCancelamento = motivoCancelamento;

                _cteLogService.InserirLogInfo(cte, "CteServiceTestCase", string.Format("Atualizando o CT-e para a glosa do SAP: {0}", cte.Id));
                _cteLogService.InserirLogInfo(cte, "CteService", string.Format("Atualizando a situa��o atual do cte para: {0}", cte.SituacaoAtual));
                _cteRepository.Atualizar(cte);

                // Envia as informa��es para o SAP
                CteInterfaceEnvioSap interfaceEnvioSap = new CteInterfaceEnvioSap();
                interfaceEnvioSap.Cte = cte;
                interfaceEnvioSap.DataGravacaoRetorno = DateTime.Now;
                interfaceEnvioSap.DataUltimaLeitura = DateTime.Now;
                interfaceEnvioSap.TipoOperacao = TipoOperacaoCteEnum.CancelamentoRejeitado;
                interfaceEnvioSap.DataEnvioSap = null;

                _cteInterfaceEnvioSapRepository.Inserir(interfaceEnvioSap);
            }
        }

        [Test]
        public void ImportarCteEmpresas()
        {
            using (var erros = new StreamWriter(@"C:\Temp\cte_clientes_jan_erros.txt"))
            {
                using (var sr = new StreamReader(@"C:\Temp\cte_clientes_jan.txt"))
                {
                    string line = string.Empty;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!ImportarCteEmpresa(line))
                        {
                            erros.Write(line);
                        }
                    }
                }
            }
        }

        private List<string> ListaCteChave()
        {
            List<string> lista = new List<string>();

            lista.Add("41140601258944000550570020007790781962907526");
            lista.Add("41140601258944000550570020007790861905760858");
            lista.Add("41140601258944000550570020007790871905735826");
            lista.Add("41140601258944000550570020007790881963138689");
            lista.Add("41140601258944000550570020007791011962914905");
            lista.Add("41140601258944000550570020007791021962860381");
            lista.Add("41140601258944000550570020007791031963334940");
            lista.Add("41140601258944000550570020007791041962840431");
            lista.Add("41140601258944000550570020007791051905756157");
            lista.Add("41140601258944000550570020007791061905735912");
            lista.Add("41140601258944000550570020007791131962846714");
            lista.Add("41140601258944000550570020007791201962855890");
            lista.Add("41140601258944000550570020007791211905760930");
            lista.Add("41140601258944000550570020007791251905756079");
            lista.Add("41140601258944000550570020007791681962854146");
            lista.Add("41140601258944000550570020007791761962858305");
            lista.Add("41140601258944000550570020007791831905758792");
            lista.Add("41140601258944000550570020007791961905758618");
            lista.Add("41140601258944000550570020007792041962845919");
            lista.Add("41140601258944000550570020007798211905760512");
            lista.Add("41140601258944000550570020007798271905760427");
            lista.Add("41140601258944000550570020007798281963596470");

            return lista;
        }


        private List<ImportacaoEmail> ListaImportacaoEmail()
        {
            List<ImportacaoEmail> lista = new List<ImportacaoEmail>();

            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 226860 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238347 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238348 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238349 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238350 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238351 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238352 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238353 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238354 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238355 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238356 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238357 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238358 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238359 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238360 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238361 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238362 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238363 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238364 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 238365 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 240199 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 240200 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 240288 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 240289 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 240290 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246556 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246557 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246558 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246559 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246560 }, EmailNfe = "alan.goncalves@valedoivai.com.br;claudio.santos@valedoivai.com.br;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246574 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246575 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246576 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246577 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });
            lista.Add(new ImportacaoEmail { Cte = new Cte { Id = 246578 }, EmailNfe = "alan.goncalves@valedoivai.com.br;;", EmailXml = "alan.goncalves@valedoivai.com.br" });

            return lista;
        }


        private bool ImportarCteEmpresa(string registro)
        {
            try
            {
                Cte cte;
                IEmpresa EmpresaRemetente;
                IEmpresa EmpresaExpedidora;
                IEmpresa EmpresaRecebedor;
                IEmpresa EmpresaDestinataria;
                IEmpresa EmpresaTomadora;

                var dados = registro.Split(';');
                var CteChave = dados[0].Trim();
                var siglaRemetente = dados[1].Trim();
                var siglaExpedidora = dados[2].Trim();
                var siglaRecebedor = dados[3].Trim();
                var siglaDestinataria = dados[4].Trim();
                var siglaTomadora = dados[5].Trim();

                cte = _cteRepository.ObterPorChave(CteChave);

                if (_cteEmpresasRepository.ObterPorCte(cte) == null)
                {
                    EmpresaRemetente = _empresaClienteRepository.ObterPorSiglaSap(siglaRemetente);
                    EmpresaExpedidora = _empresaClienteRepository.ObterPorCnpj(siglaExpedidora);
                    EmpresaRecebedor = _empresaClienteRepository.ObterPorCnpj(siglaRecebedor);
                    EmpresaDestinataria = _empresaClienteRepository.ObterPorCnpj(siglaDestinataria);
                    EmpresaTomadora = _empresaClienteRepository.ObterPorCnpj(siglaTomadora);

                    CteEmpresas cteEmpresa = new CteEmpresas();

                    cteEmpresa.Cte = cte;
                    cteEmpresa.EmpresaRemetente = EmpresaRemetente;
                    cteEmpresa.EmpresaExpedidor = EmpresaExpedidora;
                    cteEmpresa.EmpresaRecebedor = EmpresaRecebedor;
                    cteEmpresa.EmpresaDestinatario = EmpresaDestinataria;
                    cteEmpresa.EmpresaTomadora = EmpresaTomadora;

                    /*  0-Remetente
                        1-Expedidor
                        2-Recebedor
                        3-Destinat�rio
                        4-Outros
                    */
                    if (EmpresaTomadora == EmpresaRemetente)
                    {
                        cteEmpresa.CodigoTomador = 0;
                    }
                    else if (EmpresaTomadora == EmpresaExpedidora)
                    {
                        cteEmpresa.CodigoTomador = 1;
                    }
                    else if (EmpresaTomadora == EmpresaRecebedor)
                    {
                        cteEmpresa.CodigoTomador = 2;
                    }
                    else if (EmpresaTomadora == EmpresaDestinataria)
                    {
                        cteEmpresa.CodigoTomador = 3;
                    }
                    else
                    {
                        cteEmpresa.CodigoTomador = 4;
                    }
                    _cteEmpresasRepository.InserirOuAtualizar(cteEmpresa);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        [Test]
        public void TestarConsultaTela1146()
        {

            var result = _cteRepository.ObterCteManutencao(new DetalhesPaginacaoWeb(), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(+1), "", 0, "", "", "", "", "", "", "");

            var teste = result;
        }
    }

    internal class ImportacaoEmail
    {
        public Cte Cte { get; set; }
        public string EmailNfe { get; set; }
        public string EmailXml { get; set; }
    }
}
