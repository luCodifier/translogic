namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.Estrutura;
	using Core.Domain.Model.Estrutura.Repositories;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using Core.Domain.Model.FluxosComerciais.InterfaceSap;
	using Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class GravarSapCteEnvioServiceTestCase : BaseTranslogicContainerTestCase
	{
		private GravarSapCteEnvioService _service;
	    private CteService _cteService;
		private ICteRepository _cteRepository;
		private ICteAgrupamentoRepository _cteAgrupamentoRepository;
		private IEmpresaClienteRepository _empresaClienteRepository;
		private ICteInterfaceEnvioSapRepository _cteInterfaceEnvioSapRepository;
		private ICteDetalheRepository _cteDetalheRepository;
		private IVSapZSDV0203VRepository _vSapZSDV0203VRepository;
        private ISapConteinerCteRepository _sapConteinerCteRepository;
        private ISapDespCteRepository _sapDespCteRepository;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<GravarSapCteEnvioService>();
            _cteService = Container.Resolve<CteService>();
			_cteRepository = Container.Resolve<ICteRepository>();
			_cteAgrupamentoRepository = Container.Resolve<ICteAgrupamentoRepository>();
			_empresaClienteRepository = Container.Resolve<IEmpresaClienteRepository>();
			_cteInterfaceEnvioSapRepository = Container.Resolve<ICteInterfaceEnvioSapRepository>();
			_vSapZSDV0203VRepository = Container.Resolve<IVSapZSDV0203VRepository>();
            _sapConteinerCteRepository = Container.Resolve<ISapConteinerCteRepository>();
            _sapDespCteRepository = Container.Resolve<ISapDespCteRepository>();
		}
		#endregion

		[Test]
		public void GerarDados()
		{
            var idCte = 2911796;

            //// Remover CTE da interface antes
            var cteRemoverInterf = _cteRepository.ObterPorId(idCte);
            _cteInterfaceEnvioSapRepository.RemoverCteInterfaceEnvioSap(cteRemoverInterf);

            //////// Inserir CTE na interface antes
            var cteInserirInterf = _cteRepository.ObterPorId(idCte);
            cteInserirInterf.SituacaoAtual = SituacaoCteEnum.Autorizado;
            _cteService.InserirInterfaceEnvioSap(cteInserirInterf, new CteStatusRetorno { Id = 100 });
            
            //// Integrar com o SAP.
            CteInterfaceEnvioSap cies = _cteInterfaceEnvioSapRepository.ObterPorCte(idCte);
			_service.Executar(cies, new Usuario { Id = 597 });  
		}

		[Test]
		public void EmpresaCliente()
		{
			Empresa empresaCliente = _empresaClienteRepository.ObterEmpresaClientePorCnpjDaFerrovia("01258944000550");
		}

		[Test]
		public void VSapZSDV0203V()
		{
			VSapZSDV0203V vSapZSDV0203V = _vSapZSDV0203VRepository.ObterPorSerieDespachoTipo(821, 64236, "DES");
		}

		[Test]
		public void TestarExecutarPorCteRaiz()
		{
            //Cte cte = new Cte { Id = 2911549 };

		    var cte = _cteRepository.ObterPorIdCte(2911549);
			_service.ExecutarPorCteRaiz(cte, new Usuario { Id = 597 });
		}

		[Test]
		public void InterfaceSap()
		{
			string hostName = Dns.GetHostName();
			IList<CteInterfaceEnvioSap> listaCteInterfaceEnvioSap = _cteInterfaceEnvioSapRepository.ObterNaoProcessadosPorHost(hostName);

			foreach (CteInterfaceEnvioSap cteInterfaceEnvioSap in listaCteInterfaceEnvioSap)
			{
				if (cteInterfaceEnvioSap.DataEnvioSap == null)
				{
					bool agrupamentoExistente = false;

					IList<CteAgrupamento> listaCteAgrupamento = _cteAgrupamentoRepository.ObterAgrupamentoPorCteFilho(cteInterfaceEnvioSap.Cte);

					foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
					{
						foreach (CteInterfaceEnvioSap cteInterfaceEnvioSapAux in listaCteInterfaceEnvioSap)
						{
							if (cteInterfaceEnvioSapAux.Cte == cteAgrupamento.CteFilho)
							{
								agrupamentoExistente = true;
								break;
							}
							else
							{
								agrupamentoExistente = false;
							}
						}

						if (!agrupamentoExistente)
						{
							break;
						}
					}

					if (agrupamentoExistente)
					{
						// mandar para gravar envio

						foreach (CteAgrupamento cteAgrupamento in listaCteAgrupamento)
						{
							_cteInterfaceEnvioSapRepository.AtualizarEnvioSap(cteAgrupamento.CteFilho);

							for (int i = 0; i < listaCteInterfaceEnvioSap.Count; i++)
							{
								if (listaCteInterfaceEnvioSap[i].Cte == cteAgrupamento.CteFilho)
								{
									if (listaCteInterfaceEnvioSap[i].DataEnvioSap == null)
									{
										listaCteInterfaceEnvioSap[i].DataEnvioSap = DateTime.Now;
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		[Test]
		public void VerifcarDadosEnviadosSap()
		{
			string chaveCte = "41120601258944000550570020000737781963044945";
			TipoOperacaoCteEnum tipo = TipoOperacaoCteEnum.Exclusao;
			// bool ret = _service.VerifcarCteJaEnviadoSap(chaveCte, tipo);
		}
        
        [Test]
        public void Verificar_GravaSapConteinerCte()
        {
            //_sapConteinerCteRepository.Inserir();
        }
	}
}