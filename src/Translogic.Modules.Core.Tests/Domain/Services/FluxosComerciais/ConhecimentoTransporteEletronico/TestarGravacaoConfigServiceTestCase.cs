namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using Core.Domain.Services.Ctes;
	using NUnit.Framework;
	using Translogic.Tests;

	public class TestarGravacaoConfigServiceTestCase : BaseTranslogicContainerTestCase
	{
		private TestarGravacaoConfigService _service;
		#region SETUP/TEARDOWN
		
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<TestarGravacaoConfigService>();
		}
		#endregion

		[Test]
		public void TestarGravacao()
		{
			// _service.TestarGravacao();
			_service.GravarDadosIniciais();
		}
	}
}