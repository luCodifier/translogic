﻿using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao.Repositories;
using Translogic.Modules.Core.Interfaces.GestaoDocumentos;
using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.GestaoDocumentos;
    using Translogic.Tests;

    [TestFixture]
    public class GestaoDocumentosServiceTestCase : BaseTranslogicContainerTestCase
    {
        private IGestaoDocumentosService _service;
        private IWsTicketControleRecebimentoRepository _wsTicketControleRecebimentoRepository;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<IGestaoDocumentosService>();
            _wsTicketControleRecebimentoRepository = Container.Resolve<IWsTicketControleRecebimentoRepository>();
        }

        [Test]
        public void InformarTicketBalanca()
        {
            // WsTicketControleRecebimento mensagemRecebida = _wsTicketControleRecebimentoRepository.ObterPorId(161);

            var sr = new StreamReader(@"C:\Temp\tiket.xml");
            var ticket = SerializarMensagem(sr.ReadToEnd());

            _service.InformarTicketBalanca(ticket);
        }

        private TicketDto SerializarMensagem(string mensagemRecebida)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(TicketDto));
            return (TicketDto)serializer.ReadObject(new XmlTextReader(new StringReader(mensagemRecebida)));
        }

        #endregion

    }
}