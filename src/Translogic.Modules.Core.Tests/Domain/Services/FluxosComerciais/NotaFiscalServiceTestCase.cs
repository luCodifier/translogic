namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
    using System;
    using ALL.Core.IoC;
    using ALL.TestsSupport.IoC;
    using Core.Domain.Model.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.Dcls;
    using Core.Domain.Services.FluxosComerciais;
    using NUnit.Framework;
    using Translogic.Tests;

    [TestFixture]
    public class NotaFiscalServiceTestCase : BaseTranslogicContainerTestCase
	{
        private NotaFiscalService _service;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<NotaFiscalService>();
        }
        #endregion

        [Test]
        public void TestarAtualizacaoDomPedro()
        {
            _service.AtualizarDadosNotasFiscais();
        }

        [Test]
        public void TestarValidacaoPlaca_DeveSerValido()
        {
            var teste = _service.ValidarPlaca("ASD1899");
            Assert.IsTrue(teste);
        }

        [Test]
        public void TestarValidacaoPlaca_DeveSerValidoComTraco()
        {
            var teste = _service.ValidarPlaca("ASD-1899");
            Assert.IsTrue(teste);
        }

        [Test]
        public void TestarValidacaoPlaca_DeveSerInvalido()
        {
            var teste = _service.ValidarPlaca("1231899");
            Assert.IsFalse(teste);
        }
	}
}