using System.Collections.Generic;
using System.Linq;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
	using System;
	using System.Diagnostics;
	using System.IO;
	using System.Xml;
	using ALL.Core.IoC;
	using ALL.TestsSupport.IoC;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais.Dcls;
	using Core.Domain.Model.FluxosComerciais.Nfes;
	using Core.Domain.Model.FluxosComerciais.Repositories;
	using Core.Domain.Services.FluxosComerciais;
	using NUnit.Framework;
	using Translogic.Tests;
	using Translogic.Modules.Core.Domain.Model.Dto;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
	using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura;

	[TestFixture]
    public class DescarregamentoServiceTestCase : BaseTranslogicContainerTestCase
	{
		private DescarregamentoService _service;

		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
			_service = Container.Resolve<DescarregamentoService>();
		}
		#endregion

		[Test]
		public void TestarListaErros()
		{
		    var result = _service.ObterVagoesComErro("PCZ", null, null, null, null, null, "CARGILL",
		                                             DateTime.Today.AddMonths(-1), DateTime.Today.AddDays(1));
		}
	}
}