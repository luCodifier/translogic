using Translogic.Modules.Core.Domain.Services.Edi;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using ALL.Core.IoC;
    using ALL.TestsSupport.IoC;
    using Core.Domain.Model.Diversos.Cte;
    using Core.Domain.Model.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.Dcls;
    using Core.Domain.Model.FluxosComerciais.Nfes;
    using Core.Domain.Services.FluxosComerciais;
    using NUnit.Framework;
    using Translogic.Tests;
    using Util;

	[TestFixture]
	public class NfeServiceTestCase : BaseTranslogicContainerTestCase
	{
		private NfeService _service;
	    private EdiReadNfeService _ediReadNfeService;

		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = true;
			TemTransacao = false;
			_service = Container.Resolve<NfeService>();
		    _ediReadNfeService = Container.Resolve<EdiReadNfeService>();
		}
		#endregion

		[Test]
		public void TesteString()
		{
			var mensagem = "O Vag�o (0332968) deve ser carregado em um fluxo de conteiner de 20.$247449|0332968$";
			int idxIni = mensagem.IndexOf("$") + 1;
			int idxFim = mensagem.IndexOf("|");

			string idVagao = mensagem.Substring(idxIni, idxFim - idxIni);
			string codigoVagao = mensagem.Substring(idxFim + 1).Replace("$", string.Empty);
		}

		[Test]
		public void TestarObterNfe()
		{
            var dadosNfe = _service.ObterDadosNfe("31140861409892000840550010000506916921726719", TelaProcessamentoNfe.Pooling, false, false);
			var peso = dadosNfe.NotaFiscalEletronica.PesoBruto / 1000;
			double? pesoTotal = dadosNfe.NotaFiscalEletronica.PesoBruto > 0 ? Tools.TruncateValue(peso, TipoTruncateValueEnum.UnidadeMilhar) : (double?)null;

			Assert.IsNotNull(dadosNfe);
		}

		[Test]
		public void TestarSimConsultas()
		{
			// PARA RODAR ESTE TESTE MELHOR RODAR O SETUP PARA COLOCAR TRANSA��O E FAZER ROLLBACK
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			var dadosNfe = _service.ObterDadosNfe("51140247067525010766550080000627441267823706", TelaProcessamentoNfe.Pooling, false, false);
			stopwatch.Stop();

			// Write result
			Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);

			// Assert.IsNotNull(dadosNfe);
		}

		[Test]
		public void TestarParserNfe()
		{
			// FileInfo fileInfo = new FileInfo(@"C:\Temp\notas20110513\52110584046101005748550280000125031156992531.xml");
			FileInfo fileInfo = new FileInfo(@"C:\Temp\xml\nfe.xml");
			FileStream file = fileInfo.OpenRead();
			XmlDocument doc = new XmlDocument();
		    double peso_bruto = 0;
			doc.Load(file);
            var nfe = _service.ProcessarDadosNfeSimConsultas(doc, false, ref peso_bruto);

             var statusRetorno = new StatusRetornoNfe
                {
                    Codigo = "",
                    DataCadastro = DateTime.Now,
                    IndPermiteReenvioTela = false,
                    IndPermiteReenvioBo = false,
                    IndLiberarDigitacao = true,
                    Mensagem = ""
                };

		    _service.GravarLogStatusNfe(nfe, statusRetorno, "50150707401436000131550090000001141795116174",
                                        TelaProcessamentoNfe.Pooling, false, ref peso_bruto);

		}

		[Test]
		public void TestarValidacaoNfeDeveSerValido()
		{
			bool valido = false;
			try
			{
				_service.ValidarChaveNfe("41110708487254000198550010000000021771507543", TelaProcessamentoNfe.Pooling);
				valido = true;
			}
			catch (Exception ex)
			{
			}

			Assert.IsTrue(valido);
		}

		[Test]
		public void TestarValidacaoNfeDeveSerInvalidoPorCarateres()
		{
			bool valido = false;
			try
			{
				_service.ValidarChaveNfe("41110708487254000198550X10000000021771507543", TelaProcessamentoNfe.Pooling);
				valido = true;
			}
			catch (Exception ex)
			{
			}

			Assert.IsFalse(valido);
		}

		[Test]
		public void TestarValidacaoNfeDeveSerInvalidoPorTamanho()
		{
			bool valido = false;
			try
			{
				_service.ValidarChaveNfe("41110708487254000198550", TelaProcessamentoNfe.Pooling);
				valido = true;
			}
			catch (Exception ex)
			{
			}

			Assert.IsFalse(valido);
		}

		[Test]
		public void TestarValidacaoNfeDeveSerInvalidoPorDigito()
		{
			bool valido = false;
			try
			{
				_service.ValidarChaveNfe("51090684046101024700550010000779416842034971", TelaProcessamentoNfe.Pooling);
				valido = true;
			}
			catch (Exception ex)
			{
			}

			Assert.IsFalse(valido);
		}

		[Test]
		public void TestarValidacaoNfeDeveSerInvalidoPorData()
		{
			bool valido = false;
			try
			{
				_service.ValidarChaveNfe("51120084046101055095550010000013601931602484", TelaProcessamentoNfe.Pooling);
				valido = true;
			}
			catch (Exception ex)
			{
			}

			Assert.IsFalse(valido);
		}


		[Test]
		public void ObterNfes()
		{
            //IList<string> list = ObterListaNfes();
            //foreach (string nfe in list)
            //{
				
            //}
            var dadosNfe = _service.ObterDadosNfe("52150508517600000133550550000422341526754437", TelaProcessamentoNfe.Pooling, false, false);
		}

		[Test]
		public void ValidarNfes()
		{
			int contador = 0;
			IList<string> list = ObterListaNfes();
			foreach (string nfe in list)
			{
				string erro = string.Empty;
				var oNfe = _service.ObterDbNfeSimconsultasPorChave(nfe);
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(oNfe.Xml);
				if (!_service.ValidarXml(doc, out erro))
				{
					Console.WriteLine(nfe + " - " + erro);
					contador++;
				}
			}

			Console.WriteLine("Erros: " + contador);
		}

		[Test]
		public void AtualizarDadosNfeEdiTest()
		{
			_service.AtualizarDadosNfeEdi();
		}

		[Test]
		public void ObterNfeSimConsultas()
		{
            string chave = "51180384046101024700551010002122001323167561";
			int codigo = 0;
			_service.ObterNfeSimConsultas(chave, false,TelaProcessamentoNfe.Pooling, false);
		}

		private IList<string> ObterListaNfes()
		{
			List<string> lista = new List<string>
                                     {
                                        "35120205242560000176550010000363461526838311",
										"35120205242560000176550010000363471541144780",
										"35120205242560000176550010000363451503302370",
										"35120205242560000176550010000363421469675120",
										"35120205242560000176550010000363431480158176",
										"35120205242560000176550010000363271618949530",
										"35120205242560000176550010000363231571971667",
										"35120205242560000176550010000363411469252043",
										"35120205242560000176550010000363181525401217",
										"35120205242560000176550010000363281623634786",
										"35120205242560000176550010000363161506597530",
										"35120205242560000176550010000363171512285640",
										"35120205242560000176550010000363131491272537",
										"35120205242560000176550010000363121484518871",
										"35120205242560000176550010000363061307090464",
										"35120205242560000176550010000363091369597033",
										"35120205242560000176550010000362941166501592",
										"35120205242560000176550010000362721249320660",
										"35120205242560000176550010000362851097930834",
										"52120300012377000160550030000573461500782834",
										"52120300012377000160550030000573521522250379",
										"52120300012377000160550030000573491508727397",
										"52120300012377000160550030000573481506972389",
										"52120300012377000160550030000573231655259596",
										"35120205242560000176550010000362331743846343",
										"35120205242560000176550010000362691218372930",
										"35120205242560000176550010000362581144286420",
										"35120205242560000176550010000362451828917335",
										"35120205242560000176550010000362621176550410",
										"35120205242560000176550010000362401811711961",
										"35120205242560000176550010000362611161538802",
										"35120327184944001860550010000182101010182103",
										"35120327184944001860550010000182111010182119",
										"35120327184944001860550010000182081010182083",
										"35120327184944001860550010000182091010182099",
										"35120327184944001860550010000182071010182078",
										"35120205242560000176550010000362571139507158",
										"35120205242560000176550010000362671208548012",
										"35120205242560000176550010000362381801009530",
										"50120361247870000316550550000086021000026100",
										"50120361247870000316550550000086011000026090",
										"50120361247870000316550550000086041000026121",
										"50120361247870000316550550000086051000026137",
										"35120307024792000264550010000114111377919550",
										"50120361247870000316550550000086031000026116",
										"35120307024792000264550010000114101703395612",
										"35120307024792000264550010000114091631436465",
										"35120307024792000264550010000114081853523219",
										"35120307024792000264550010000114071904306557",
										"35120307024792000264550010000114041233831910",
										"35120307024792000264550010000114061401066664",
										"35120307024792000264550010000114051160622982",
										"35120307024792000264550010000114031450800803",
										"35120350290329000960550040000214081185606290",
										"35120350290329000960550040000214171341206761",
										"35120350290329000960550040000214131290906916",
										"35120350290329000960550040000214091227021403",
										"35120350290329000960550040000214161321462897",
										"35120350290329000960550040000214041071875358",
										"35120350290329000960550040000214191374144549",
										"35120350290329000960550040000214101243427614",
										"35120350290329000960550040000214181346393443",
										"35120350290329000960550040000214221404810229",
										"35120307024792000264550010000114021996217444",
										"35120350290329000960550040000214201389171823",
										"35120350290329000960550040000214121258470566",
										"35120307024792000264550010000114011395987710",
										"35120307024792000264550010000114001393781203",
										"35120307024792000264550010000113991275211951",
										"35120307024792000264550010000113981743357383",
										"50120384046101004261550010000032861999234725",
										"35120347067525014591550060000243331410164757",
										"35120347067525014591550060000243341430457064",
										"50120384046101004261550010000032111144078084",
										"50120384046101004261550010000032151202923704",
										"50120384046101004261550010000031761114391085",
										"50120384046101004261550010000032081124211085",
										"50120384046101004261550010000032131853709382",
										"50120384046101004261550010000032211664223298",
										"50120384046101004261550010000031801462262964",
										"35120347067525014591550060000243321398710184",
										"35120347067525014591550060000243311386660155",
										"35120347067525014591550060000243301379859491",
										"35120347067525014591550060000243291372322343",
										"35120347067525014591550060000243281364111404",
										"35120347067525014591550060000243271317775990",
										"35120347067525014591550060000243261305804321",
										"35120334274233006569550010003008801126965635",
										"35120334274233006569550010003008731124756201",
										"35120205242560000176550010000362341757604379",
										"35120205242560000176550010000362191847866554",
										"35120205242560000176550010000362361776502076",
										"52120300012377000160550030000573131609613653",
										"35120327184944001860550010000182041010182041",
										"35120327184944001860550010000181981010181984",
										"35120327184944001860550010000181951010181958",
										"35120327184944001860550010000182021010182020",
										"35120327184944001860550010000182031010182036",
										"52120300012377000160550030000573111603486795",
										"35120327184944001860550010000182001010182000",
										"35120327184944001860550010000182011010182015",
										"35120327184944001860550010000181961010181963",
										"35120327184944001860550010000181971010181979",
										"35120327184944001860550010000181991010181990",
										"52120300012377000160550030000573201645434679"
                                     };
			return lista;
		}

        [Test]
        public void TestarAlteracaoNfe()
        {
            _service.AlterarVolumePesoNfe("35120205242560000176550010000363091369597033", 100, 100, "910016640", null, 0, 0, false, "", "");
            
            var listaHistoricoAlteracaoNfe = _service.ListarHistoricoNfe();
            var historicoNfe = listaHistoricoAlteracaoNfe.Items.FirstOrDefault();

            _service.DarComoConferidoVolumePesoNfe(historicoNfe.Id);
            _service.RestaurarVolumePesoNfe(historicoNfe.Id, "910016640");
            //_service.RestaurarVolumePesoNfe(1, "910016640");
        }

        [Test]
        public void TestarEmail()
        {
            _ediReadNfeService.ObterEmailsRealizarProcessamento(1,"ASC");
            //_ediReadNfeService.TesteJhonatan();
        }

        [Test]
        public void TesteValidacaoGestaoFluxo()
        {
            _ediReadNfeService.ValidaExistenciaFluxoNota();
        }

        [Test]
        public void TestarParserNfeEdi()
        {
            // FileInfo fileInfo = new FileInfo(@"C:\Temp\notas20110513\52110584046101005748550280000125031156992531.xml");
            FileInfo fileInfo = new FileInfo(@"C:\Users\AS\Desktop\Rumo\nfe.xml");
            FileStream file = fileInfo.OpenRead();
            XmlDocument doc = new XmlDocument();
            doc.Load(file);
            _service.ProcessarDadosNfeEdi(doc);
        }

        [Test]
        public void TestarParserNfeEdiVariasNotas()
        {
            // FileInfo fileInfo = new FileInfo(@"C:\Temp\notas20110513\52110584046101005748550280000125031156992531.xml");

            var directory = new DirectoryInfo(@"C:\temp\xml");

            foreach (var xml in directory.GetFiles().Where(n=> n.FullName.ToString().Contains(".xml")))
            {
                FileStream file = xml.OpenRead();
                XmlDocument doc = new XmlDocument();
                
                doc.Load(file);
                _service.ProcessarDadosNfeEdi(doc);   
            }
        }
	}
}
