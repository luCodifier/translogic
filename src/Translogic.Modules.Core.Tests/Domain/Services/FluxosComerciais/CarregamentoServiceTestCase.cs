using System.Collections.Generic;
using System.Linq;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Carregamentos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos.Repositories;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Tests.Domain.Services.FluxosComerciais
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;
    using ALL.Core.IoC;
    using ALL.TestsSupport.IoC;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.Dcls;
    using Core.Domain.Model.FluxosComerciais.Nfes;
    using Core.Domain.Model.FluxosComerciais.Repositories;
    using Core.Domain.Model.FluxosComerciais.Simconsultas;
    using Core.Domain.Services.FluxosComerciais;
    using NUnit.Framework;
    using Translogic.Tests;
    using Translogic.Modules.Core.Domain.Model.Dto;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Simconsultas.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura.Repositories;
    using Translogic.Modules.Core.Domain.Model.Estrutura;

    [TestFixture]
    public class CarregamentoServiceTestCase : BaseTranslogicContainerTestCase
    {
        private CarregamentoService _service;
        private IFluxoComercialRepository _fluxoComercialRepository;
        private ICarregamentoRepository _carregamentoRepository;
        private ICteRepository _cteRepository;
        private INfeSimconsultasRepository _nfeSimconsultasRepository;
        private IEmpresaRepository _empresaRepository;
        private ISapDespCteRepository _sapDespCteRepository;
        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<CarregamentoService>();
            _nfeSimconsultasRepository = Container.Resolve<INfeSimconsultasRepository>();
            _fluxoComercialRepository = Container.Resolve<IFluxoComercialRepository>();
            _carregamentoRepository = Container.Resolve<ICarregamentoRepository>();
            _cteRepository = Container.Resolve<ICteRepository>();
            _empresaRepository = Container.Resolve<IEmpresaRepository>();
            _sapDespCteRepository = Container.Resolve<ISapDespCteRepository>();
        }
        #endregion

        [Test]
        public void TestarCarregamento()
        {
            DclTemp dclTemp = new DclTemp();
            dclTemp.DataDespacho = DateTime.Now;
            dclTemp.DclPorVagao = true;
            dclTemp.Id = -1;
            // _service.SalvarCarregamento(dclTemp);
        }

        [Test]
        public void TestarVerificacaoFluxoInternacional()
        {
            FluxoComercial fluxoComercial = new FluxoComercial { Id = 103584 };

            bool ret = _service.VerificarFluxoInternacional(fluxoComercial);
            Assert.AreEqual(true, ret);
        }

        [Test]
        public void MargemLiberacaoFaturamentoTestCase()
        {
            var ret = _service.VerificarMargemLiberacaoFaturamento(new EmpresaCliente { Id = 68939 });
        }

        [Test]
        public void TestarObtencaoStatusVagao()
        {
            Stopwatch sw = Stopwatch.StartNew();
            var x = _service.ObterDadosVagao("6383483");
            sw.Stop();

            decimal tempo = sw.ElapsedMilliseconds;
            Console.WriteLine(string.Format("Patio: {0} - Situacao:{1} - Tempo: {2}", x.Patio, x.Situacao, tempo));

            sw = Stopwatch.StartNew();
            x = _service.ObterDadosVagao("6408346");
            sw.Stop();
            tempo = sw.ElapsedMilliseconds;
            Console.WriteLine(string.Format("Trem: {0} - Situacao:{1} - Tempo: {2}", x.PrefixoTrem, x.Situacao, tempo));

            sw = Stopwatch.StartNew();
            x = _service.ObterDadosVagao("3271251");
            sw.Stop();
            tempo = sw.ElapsedMilliseconds;
            Console.WriteLine(string.Format("Intercambio: {0} - Situacao:{1} - Tempo: {2}", x.Intercambio, x.Situacao, tempo));
        }

        [Test]
        public void TestarPesoRemanescente()
        {
            double pesoTotal, pesoRemanescente;
            var peso = _service.ObterPesosMultiploDespacho(301285, out pesoTotal, out pesoRemanescente);
        }

        [Test]
        public void TestarVoarConteiner()
        {
            _service.VoarConteiner("480", 78, "MIGRA");
        }

        [Test]
        public void TestarVerificacaoContratoPeloFluxoCte()
        {
            FluxoComercial fluxoComercial = _fluxoComercialRepository.ObterPorId(112313);

            _service.VerificarContratoFluxoPossuiVigenciaPorCte(fluxoComercial, 325671);
        }

        [Test]
        public void Testar()
        {
            var carregamento = GerarCarregamentoDto("GL10282");
            var retorno = _service.SalvarCarregamento(carregamento, new Usuario() { Id = 597 });
        }

        [Test]
        public void ObterQtdeCtePorVagaoVigenteTestCase()
        {
            ////Cte cte1 = _cteRepository.ObterPorId(2911629);
            ////if (cte1.FluxoComercial.IndRateioCte == true)
            ////{
            ////    cte1.Qtde = (double)_service.ObterQtdeCtePorVagaoVigente(cte1.Chave);
            ////}
            ////_cteRepository.Atualizar(cte1);
            

            ////Cte cte2 = _cteRepository.ObterPorId(2911630);
            ////if (cte2.FluxoComercial.IndRateioCte == true)
            ////{
            ////    cte2.Qtde = (double)_service.ObterQtdeCtePorVagaoVigente(cte2.Chave);
            ////}
            ////_cteRepository.Atualizar(cte2);


            ////Cte cte3 = _cteRepository.ObterPorId(2911631);
            ////if (cte3.FluxoComercial.IndRateioCte == true)
            ////{
            ////    cte3.Qtde = (double)_service.ObterQtdeCtePorVagaoVigente(cte3.Chave);
            ////}
            ////_cteRepository.Atualizar(cte3);


            //Cte cte4 = _cteRepository.ObterPorId(2911617);
            //if (cte4.FluxoComercial.IndRateioCte == true)
            //{
            //    cte4.Qtde = (double)_service.ObterQtdeCtePorVagaoVigente(cte4.Chave);
            //}
            //_cteRepository.Atualizar(cte4);
        }


        private CarregamentoDto GerarCarregamentoDto(string codigoFluxo)
        {
            List<NotaFiscalVagaoCarregamentoDto> listaNotas = new List<NotaFiscalVagaoCarregamentoDto>();
            LogTempoCarregamento logDespacho = new LogTempoCarregamento();
            IList<VagaoCarregamentoDto> vagoes;

            FluxoComercial fluxo = _fluxoComercialRepository.ObterPorCodigo(codigoFluxo);
            IList<ElementoVia> linhas = null;
            var idOrigem = 0;
            var idDestino = 0;
            var idLinhaIntercambio = 0;
            string serieIntercambio = null;
            int? despIntercambio = null;
            int? IdFerroviaIntercambio = null;
            string regimeIntercambio = null;

            if (fluxo.OrigemIntercambio != null)
            {
                idOrigem = fluxo.OrigemIntercambio.Id.Value;
                idDestino = fluxo.DestinoIntercambio.Id.Value;

                linhas = _service.ObterLinhasRecebimentoIntercambio(idOrigem);

                int index = new Random().Next(0, linhas.Count - 1);
                despIntercambio = new Random().Next(11111, 99999);

                serieIntercambio = "F" + new Random().Next(10, 99);
                IdFerroviaIntercambio = 56;
                idLinhaIntercambio = linhas[index].Id;
                regimeIntercambio = "P";

                vagoes = _service.ObterVagaoCarregamentoIntercambio("0531499");
            }
            else
            {
                idOrigem = fluxo.Origem.Id.Value;
                idDestino = fluxo.Destino.Id.Value;

                vagoes = _service.ObterVagoesCarregamento(idOrigem, fluxo.Id, "0531499");
            }

            NfeSimconsultas nota = new NfeSimconsultas
            {
                Id = 1,
                ChaveNfe = "52150408322396000103550010004835031712602840",
                CnpjDestinatario = "33337122005358",
                CnpjEmitente = "08322396000103",
                DataEmissao = DateTime.Today,
                RazaoSocialDestinatario = "IPIRANGA PRODUTOS DE PETROLEO S.A.",
                InscricaoEstadualDestinatario = "513467885117",
                InscricaoEstadualEmitente = "104068477",
                NumeroNotaFiscal = 000483503,
                PesoBruto = 101.817,
                Peso = 101.817,
                RazaoSocialEmitente = "CERRADINHO BIOENERGIA S.A",
                SerieNotaFiscal = "001",
                UfDestinatario = "SP",
                UfEmitente = "GO",
                ValorTotalFrete = 50.806,
                Volume = 101.817
            };
            int idMercadoria = fluxo.Mercadoria.Id;

            /*
             
            int idMercadoria = fluxo.Mercadoria.Id;
            IList<string> notas = _nfeSimconsultasRepository.ObterChaveNfeParaTestes(codigoFluxo);
            int i = 0;foreach (var n in notas)
            {
                i++;
                var indexNota = new Random().Next(0, notas.Count - 1);
                var indexVagao = new Random().Next(0, vagoes.Count - 1);
                var nota = _nfeSimconsultasRepository.ObterPorChaveNfe(notas[indexNota]);

                if (listaNotas.Where(x => x.ChaveNfe == nota.ChaveNfe).Count() > 0)
                {
                    i--;
                    continue;
                }

                var vag = vagoes[indexVagao];
                //var remetente = _empresaRepository.obter;
                var remetente = _service.ObterEmpresaPorCnpj(nota.CnpjEmitente, codigoFluxo, idOrigem);
                var destinatario = _service.ObterEmpresaPorCnpj(nota.CnpjDestinatario, codigoFluxo, idOrigem);

                var notavagao = new NotaFiscalVagaoCarregamentoDto()
                                    {
                                        ChaveNfe = nota.ChaveNfe,
                                        CnpjDestinatario = nota.CnpjDestinatario,
                                        CnpjRemetente = nota.CnpjEmitente,
                                        CodigoVagao = vag.CodigoVagao,
                                        DataNotaFiscal = nota.DataEmissao,
                                        Destinatario = nota.RazaoSocialDestinatario,
                                        IdVagao = vag.IdVagao,
                                        InscricaoEstadualDestinatario = nota.InscricaoEstadualDestinatario,
                                        InscricaoEstadualRemetente = nota.InscricaoEstadualEmitente,
                                        MultiploDespacho = false,
                                        NumeroNotaFiscal = nota.NumeroNotaFiscal.ToString(),
                                        PesoDclVagao = nota.PesoBruto,
                                        PesoRateio = nota.Peso,
                                        PesoTotal = nota.PesoBruto,
                                        PesoTotalVagao = nota.PesoBruto,
                                        Remetente = nota.RazaoSocialEmitente,
                                        SerieNotaFiscal = nota.SerieNotaFiscal,
                                        SiglaDestinatario = destinatario.Sigla,
                                        SiglaRemetente = remetente.Sigla,
                                        UfDestinatario = nota.UfDestinatario,
                                        UfRemetente = nota.UfEmitente,
                                        ValorNotaFiscal = nota.ValorTotalFrete,
                                        ValorTotalNotaFiscal = nota.ValorTotalFrete,
                                        VolumeNotaFiscal = nota.Volume,
                                        VolumeVagao = nota.Volume,
                                        TaraVagao = nota.Peso
                                    };

                listaNotas.Add(notavagao);
                if (i > 3)
                {
                    break;
                }
            }
            */

            var vag = vagoes[0];
            var remetente = _service.ObterEmpresaPorCnpj(nota.CnpjEmitente, codigoFluxo, idOrigem);
            var destinatario = _service.ObterEmpresaPorCnpj(nota.CnpjDestinatario, codigoFluxo, idOrigem);

            var notavagao = new NotaFiscalVagaoCarregamentoDto()
            {
                ChaveNfe = nota.ChaveNfe,
                CnpjDestinatario = nota.CnpjDestinatario,
                CnpjRemetente = nota.CnpjEmitente,
                CodigoVagao = vag.CodigoVagao,
                DataNotaFiscal = nota.DataEmissao,
                Destinatario = nota.RazaoSocialDestinatario,
                IdVagao = vag.IdVagao,
                InscricaoEstadualDestinatario = nota.InscricaoEstadualDestinatario,
                InscricaoEstadualRemetente = nota.InscricaoEstadualEmitente,
                MultiploDespacho = false,
                NumeroNotaFiscal = nota.NumeroNotaFiscal.ToString(),
                PesoDclVagao = nota.PesoBruto,
                PesoRateio = nota.Peso,
                PesoTotal = nota.PesoBruto,
                PesoTotalVagao = nota.PesoBruto,
                Remetente = nota.RazaoSocialEmitente,
                SerieNotaFiscal = nota.SerieNotaFiscal,
                SiglaDestinatario = destinatario.Sigla,
                SiglaRemetente = remetente.Sigla,
                UfDestinatario = nota.UfDestinatario,
                UfRemetente = nota.UfEmitente,
                ValorNotaFiscal = nota.ValorTotalFrete,
                ValorTotalNotaFiscal = nota.ValorTotalFrete,
                VolumeNotaFiscal = nota.Volume,
                VolumeVagao = nota.Volume,
                TaraVagao = nota.Peso
            };

            listaNotas.Add(notavagao);

            CarregamentoDto carregamentoDto = new CarregamentoDto()
                                                {
                                                    CodigoTransacao = "BOI",
                                                    IdFluxoComercial = fluxo.Id,
                                                    IdAreaOperacionalSede = idOrigem,
                                                    IdAreaOperacionalDestino = idDestino,
                                                    IdMercadoria = idMercadoria,
                                                    SerieIntercambio = serieIntercambio,
                                                    NumeroIntercambio = despIntercambio,
                                                    IdFerroviaIntercambio = IdFerroviaIntercambio,
                                                    RegimeIntercambio = regimeIntercambio,
                                                    IdLinhaIntercambio = idLinhaIntercambio,
                                                    DataIntercambioString = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"),
                                                    DataCarregamentoString = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"),
                                                    DclPorVagao = true,
                                                    ListaNotasFiscais = listaNotas,
                                                    LogDespacho = logDespacho
                                                };

            return carregamentoDto;
        }


        [Test]
        public void AtualizaSapDespCtePesoCalculo()
        {
            var sapDespCte1 = _sapDespCteRepository.ObterPorId(4865954);
            sapDespCte1.PesoCalculo = (double)_service.ObterQtdeCtePorVagaoVigente("51170324962466000136570020006378281963565320");
            _sapDespCteRepository.Atualizar(sapDespCte1);

            var sapDespCte2 = _sapDespCteRepository.ObterPorId(4865953);
            sapDespCte2.PesoCalculo = (double)_service.ObterQtdeCtePorVagaoVigente("51170324962466000136570020006378291963565328");
            _sapDespCteRepository.Atualizar(sapDespCte2);

            var sapDespCte3 = _sapDespCteRepository.ObterPorId(4865952);
            sapDespCte3.PesoCalculo = (double)_service.ObterQtdeCtePorVagaoVigente("51170324962466000136570020006378271963565323");
            _sapDespCteRepository.Atualizar(sapDespCte3);
        }

        [Test]
        public void ObterPesoCalculoSapDespCte()
        {
            var sapDespCte1 = _sapDespCteRepository.ObterPorId(2911653);
            sapDespCte1.PesoCalculo = (double)_service.ObterPesoCalculoSapDespCte("41170301258944000550570020011074341933203756");
            _sapDespCteRepository.Atualizar(sapDespCte1);

            var sapDespCte2 = _sapDespCteRepository.ObterPorId(2911654);
            sapDespCte2.PesoCalculo = (double)_service.ObterPesoCalculoSapDespCte("41170301258944000550570020011074351933203753");
            _sapDespCteRepository.Atualizar(sapDespCte2);

            var sapDespCte3 = _sapDespCteRepository.ObterPorId(2911655);
            sapDespCte3.PesoCalculo = (double)_service.ObterPesoCalculoSapDespCte("41170301258944000550570020011074361933203750");
            _sapDespCteRepository.Atualizar(sapDespCte3);

            var sapDespCte4 = _sapDespCteRepository.ObterPorId(2911656);
            sapDespCte4.PesoCalculo = (double)_service.ObterPesoCalculoSapDespCte("41170301258944000550570020011074371933203758");
            _sapDespCteRepository.Atualizar(sapDespCte4);
        }
    }
}