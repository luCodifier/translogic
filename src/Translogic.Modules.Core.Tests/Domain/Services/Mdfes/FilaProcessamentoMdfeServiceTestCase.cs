using NUnit.Framework;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Tests;

namespace Translogic.Modules.Core.Tests.Domain.Services.Mdfes
{
	public class FilaProcessamentoMdfeServiceTestCase : BaseTranslogicContainerTestCase
	{
		private FilaProcessamentoMdfeService _service;
		private IMdfeRepository _mdfeRepository;


		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = true;
			TemTransacao = false;
			_service = Container.Resolve<FilaProcessamentoMdfeService>();
			_mdfeRepository = Container.Resolve<IMdfeRepository>();
		}
		#endregion


		[Test]
		public void TestarInsert()
		{
            Mdfe mdfe = _mdfeRepository.ObterPorId(313073);

			_service.InserirGeracaoPdfPorImpressao(mdfe);
			_service.InserirMdfeEnvioFilaProcessamento(mdfe);
		}

		[Test]
		public void ObterRetornoCteProcessadoPorIdLista()
		{
		    var mdfe = _mdfeRepository.ObterPorId(38945);
		    var returno = _service.InserirMdfeEnvioFilaProcessamento(mdfe);
		}
        
        /// <summary>  
        /// Testar a inser��o do MDFe na fila de processamento de envio 
        /// </summary>
        [Test]
        public void InserirMdfeFilaProcessamentoEnvio()
        {
            var mdfe = _mdfeRepository.ObterPorId(724132);
            var returno = _service.InserirMdfeEnvioFilaProcessamento(mdfe);
        }
	}
}