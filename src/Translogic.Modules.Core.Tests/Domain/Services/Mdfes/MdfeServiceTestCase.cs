﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.Model.Acesso;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Tests;
using System.Net;
using Translogic.Modules.Core.Spd.Data.Enums;

namespace Translogic.Modules.Core.Tests.Domain.Services.Mdfes
{
    [TestFixture]
    public class MdfeServiceTestCase : BaseTranslogicContainerTestCase
    {

        private MdfeService _service;
        private IMdfeRepository _mdfeRepository;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            TemTransacao = false;
            _service = Container.Resolve<MdfeService>();
            _mdfeRepository = Container.Resolve<IMdfeRepository>();
            Sis.Inicializar(EnumSistema.MdfeRunner, "Job");
        }

        #endregion


        [Test]
        public void TestarObterPorHost()
        {
            var mdfe = _mdfeRepository.ObterPorId(722024);
            _service.InserirMdfeEnvioFilaProcessamentoConfig(mdfe);
        }

        [Test]
        public void VerificarComposicaoMdfeEnviarAprovacao()
        {
            Usuario u = _service.ObterUsuarioRobo();

            // _service.VerificarComposicaoMdfeEnviarAprovacao(1980508, u);
            _service.VerificarComposicaoMdfeEnviarAprovacao(11111111, u);
        }

        [Test]
        public void ProcessoExecucaoEnvioMdfes()
        {
            string hostName = Dns.GetHostName();

            //Buscar lista de não processados
            IList<Mdfe> listaCtesNaoProcessados = _service.ObterMdfesProcessarEnvio(2);

            //Inserir no pooling de envio
            //_mdfeService.InserirPoolingEnvioMdfe(listaCtesNaoProcessados, hostName);
        }
        
    }
}
