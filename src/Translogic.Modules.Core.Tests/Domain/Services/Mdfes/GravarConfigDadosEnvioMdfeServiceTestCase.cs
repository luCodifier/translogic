using NUnit.Framework;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Tests;

namespace Translogic.Modules.Core.Tests.Domain.Services.Mdfes
{
	public class GravarConfigDadosEnvioMdfeServiceTestCase : BaseTranslogicContainerTestCase
	{
		private GravarConfigDadosEnvioMdfeService _service;
		private IMdfeRepository _mdfeRepository;

        const int idMdfe = 670125;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = true;
			TemTransacao = false;
			_service = Container.Resolve<GravarConfigDadosEnvioMdfeService>();
			_mdfeRepository = Container.Resolve<IMdfeRepository>();
		}
		#endregion

        /// <summary>
        /// Inserir MDFE em Pooling de processamento de envio e enviar para config
        /// </summary>
        [Test]
        public void InserirPoolingEnvioProcessar()
        {
            Mdfe mdfe = _mdfeRepository.ObterPorId(idMdfe);

            // _service.InserirGeracaoPdfPorImpressao(mdfe);
            _service.Executar(mdfe);
        }
	}
}