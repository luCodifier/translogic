namespace Translogic.Modules.Core.Tests.Domain.Services.Trem.OrdemService
{
	/*using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;
	using System.Runtime.Serialization;
	using System.Text;
	using ALL.Core.IoC;
	using ALL.TestsSupport.IoC;
	using Core.Domain.Dto.Desnormalizacao;
	using Core.Domain.Model.Acesso.Repositories;
	using Core.Domain.Model.Trem.OrdemServico;
	using Core.Domain.Model.Trem.OrdemServico.Repositories;
	using Core.Domain.Model.Via.Circulacao.Repositories;
	using Core.Domain.Model.Via.Repositories;
	using Core.Domain.Services.Interfaces.Desnormalizacao;
	using Interfaces.Trem;
	using Interfaces.Trem.OrdemServico;
	using NUnit.Framework;
	using Translogic.Core;
	
    public class LocalizacaoVagaoServiceTestCase : ContainerBaseTestCase
	{
		#region SETUP/TEARDOWN

		protected override void Setup()
		{
			base.Setup();

			svc = Container.Resolve<ILocalizacaoVagaoService>();
		}

		#endregion

		private ILocalizacaoVagaoService svc;
        
        protected override ContainerBase ObterContainer()
		{
			TranslogicStarter.Initialize();
			TranslogicStarter.SetupForTests();

			return TranslogicStarter.Container;
		}

		[Test]
        public void ObterVagoes()
		{
			var s1 = Stopwatch.StartNew();
			IList<VagaoIntegracaoDto> retorno = svc.ObterVagoesPlataforma();
            s1.Stop();
            
            Console.WriteLine("T1 = {0}", s1.ElapsedMilliseconds);

            var itemsComDespacho = ObtercomDespacho(retorno).ToList();
            var itemsComEventos = ObtercomEventos(retorno).ToList();
            var itemsComConteiners = ObtercomConteiners(retorno).ToList();
		}

        private IEnumerable<VagaoIntegracaoDto> ObtercomDespacho(IList<VagaoIntegracaoDto> retorno)
        {
            return retorno.Where(c => c.ListaDespachos.Count > 0);
        }

        private IEnumerable<VagaoIntegracaoDto> ObtercomEventos(IList<VagaoIntegracaoDto> retorno)
        {
            return retorno.Where(c => c.ListaEventos.Count > 0);
        }

        private IEnumerable<VagaoIntegracaoDto> ObtercomConteiners(IList<VagaoIntegracaoDto> retorno)
        {
            return retorno.Where(c => c.ListaConteiners.Count > 0);
        }
	}*/
}