namespace Translogic.Modules.Core.Tests.Domain.Services
{
	using System.Security.Principal;
	using System.Threading;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class AuditoriaServiceTestCase : RepositoryTestCase<UsuarioRepository, Usuario, int?>
	{
		[Test]
		public void CRUD_DeveCriarLogAuditoria()
		{
			UsuarioIdentity.UsuarioRepository = Repository;
			
			Thread.CurrentPrincipal = new GenericPrincipal(new UsuarioIdentity("MIGRA"), new string[0]);

			Menu menu = new Menu
			           	{
			           		Ativo = false,
			           		CodigoTela = 123,
			           		Ordem = 0,
			           		Path = "/abc-123/",
							Chave = "ABC 123",
			           		Transacao = "ABC"
			           	};

			var menuRepository = Resolve<MenuRepository>();

			menuRepository.Inserir(menu);

			menu.Ordem++;

			menuRepository.Atualizar(menu);

			menuRepository.Remover(menu);
		}
	}
}