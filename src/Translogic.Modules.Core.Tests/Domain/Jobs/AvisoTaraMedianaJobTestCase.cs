﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using Translogic.Tests;

    class AvisoTaraMedianaJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void EnviarAvisoTarasMedianas()
        {
            _job = Container.Resolve<IJob>("core.job.enviarAvisoTarasMedianas");
            _job.Execute(string.Empty);
        }
    }
}
