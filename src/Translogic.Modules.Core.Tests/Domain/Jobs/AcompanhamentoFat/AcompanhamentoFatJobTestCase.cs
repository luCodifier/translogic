﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.AcompanhamentoFat
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using Translogic.Tests;

    class AcompanhamentoFatJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void EnviaMensagemAviso()
        {
            _job = Container.Resolve<IJob>("core.job.acompanhamentofat");
            _job.Execute(string.Empty);
        }
    }
}
