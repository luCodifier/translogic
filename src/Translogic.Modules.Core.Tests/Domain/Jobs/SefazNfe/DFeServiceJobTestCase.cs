﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.SefazNfe
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using Translogic.Tests;

    class DFeServiceJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void ObtemDfe()
        {
            // verificar se no Services client está apontando para pegar os certificados do usuario(local) ou do servidor(prd)
            // verificar se existem registrados os certificados no webconfig, tag <certificadosList>
            _job = Container.Resolve<IJob>("core.job.dfeservice");
            _job.Execute(string.Empty);
        }
    }
}
