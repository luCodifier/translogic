namespace Translogic.Modules.Core.Tests.Domain.Jobs.FluxosComerciais
{
	using Core.Domain.Jobs.FluxosComerciais;
	using Interfaces.Jobs;
	using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
	using NUnit.Framework;
	using Translogic.Tests;

	[TestFixture]
	public class NfeBoPoolingJobTestCase : BaseTranslogicContainerTestCase
	{
		private IJob _job;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			TemTransacao = false;
		}
		#endregion

		[Test]
		public void ObtemNfeInserePooling()
		{
			// _job = Container.Resolve<IJob>("core.job.NfeBoProcessarPooling42");
			// _job.Execute(string.Empty);
		}
	}
}