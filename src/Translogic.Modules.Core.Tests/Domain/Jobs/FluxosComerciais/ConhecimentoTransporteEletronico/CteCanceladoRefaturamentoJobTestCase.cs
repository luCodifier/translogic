namespace Translogic.Modules.Core.Tests.Domain.Jobs.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using NUnit.Framework;

    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Tests;

    internal class CteCanceladoRefaturamentoJobTestCase : BaseTranslogicContainerTestCase
    {
        #region Fields

        private IJob _job;

        #endregion

        #region Public Methods and Operators

        [Test]
        public void InserirCteCanceladoRefaturamento()
        {
            this._job = this.Container.Resolve<IJob>("core.job.CteCanceladoRefaturamentoJob");
            this._job.Execute(string.Empty);
        }

        #endregion

        #region Methods

        protected override void Setup()
        {
            // base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = true;
        }

        #endregion
    }
}