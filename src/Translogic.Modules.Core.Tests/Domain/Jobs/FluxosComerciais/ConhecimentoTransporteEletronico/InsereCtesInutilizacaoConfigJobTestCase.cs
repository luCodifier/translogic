namespace Translogic.Modules.Core.Tests.Domain.Jobs.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Threading;

    using NUnit.Framework;

    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Tests;

    internal class InsereCtesInutilizacaoConfigJobTestCase : BaseTranslogicContainerTestCase
    {
        #region Fields

        private IJob _job;

        #endregion

        #region Public Methods and Operators

        [Test]
        public void InserirCtesInutilizacaoConfig()
        {
            this._job = this.Container.Resolve<IJob>("core.job.InsereCtesInutilizacaoConfigJob");
            while (true)
            {
                this._job.Execute(string.Empty);
                Thread.Sleep(60000);
            }
        }

        #endregion

        #region Methods

        protected override void Setup()
        {
            // base.Setup();
            this.RealizarRollback = false;
            this.TemTransacao = true;
        }

        #endregion
    }
}