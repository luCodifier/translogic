﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.Edi
{
    using NUnit.Framework;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Tests;

    class AtualizacaoNfePdfIdTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void LerEmailNfe()
        {
            _job = Container.Resolve<IJob>("core.job.NfeEmailEdiJob1Desc");
            _job.Execute(string.Empty);
        }

        [Test]
        public void AtualizaNfePdf()
        {
            _job = Container.Resolve<IJob>("core.job.AtualizacaoNfePdfId");
            _job.Execute(string.Empty);
        }

        [Test]
        public void LimparCaixaEmailNfe()
        {
            _job = Container.Resolve<IJob>("core.job.ExclusaoEmailEdiJob");
            _job.Execute(string.Empty);
        }
    }
}
