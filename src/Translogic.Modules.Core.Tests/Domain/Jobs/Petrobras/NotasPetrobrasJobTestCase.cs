﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.Petrobras
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using Translogic.Tests;

    class NotasPetrobrasJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void ObtemNotasPetrobras()
        {
            _job = Container.Resolve<IJob>("core.job.notaspetrobras");
            _job.Execute(string.Empty);
        }
    }
}
