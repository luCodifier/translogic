﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.SefazNfe
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using Translogic.Tests;

    class AvisoFatJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void EnviaMensagemAviso()
        {
            _job = Container.Resolve<IJob>("core.job.avisovagoesfat");
            _job.Execute(string.Empty);
        }
    }
}
