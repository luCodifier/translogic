﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs
{
    using Interfaces.Jobs;
    using NUnit.Framework;
    using System;
    using System.IO;
    using Translogic.Tests;

    class GerarXmlNotasTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        #region SETUP/TEARDOWN
        protected override void Setup()
        {
            // base.Setup();
            RealizarRollback = false;
            TemTransacao = true;
        }
        #endregion

        [Test]
        public void GerarArquivosAldo()
        {
            _job = Container.Resolve<IJob>("core.job.gerararquivosxmlservice");
            _job.Execute(string.Empty);
        }

        [Test]
        public void testeArquivoXML()
        {
            try
            {

                //string[] lines = File.ReadAllLines(@"D:\Temp\Gerar Arquivos Xml\listaGeracao.txt");
                var files = Directory.GetFiles(Directory.GetDirectories(@"D:\Temp\Gerar Arquivos Xml\")[0]);

                var file = new StreamWriter(@"D:\Temp\Gerar Arquivos Xml\logGeracaoTeste.txt");
                foreach (string line in files)
                {
                    try
                    {
                        file.WriteLine(line);
                    }
                    catch (Exception e)
                    {
                        file.WriteLine(line + ";" + "ERRO");
                    }
                    finally
                    {
                        
                    }
                }
                file.Close();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
