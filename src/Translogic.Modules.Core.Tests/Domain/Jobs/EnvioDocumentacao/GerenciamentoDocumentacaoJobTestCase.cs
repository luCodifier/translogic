﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.EnvioDocumentacao
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Model.Documentacao;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Tests;

    class GerenciamentoDocumentacaoJobTestCase : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        private DocumentacaoVagaoService _documentacaoVagaoService;
        private IGerenciamentoDocumentacaoRepository _gerenciamentoDocumentacaoRepository;
        private IHistoricoEnvioDocumentacaoRepository _historicoEnvioRepository;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            RealizarRollback = false;
            TemTransacao = true;

            _documentacaoVagaoService = Container.Resolve<DocumentacaoVagaoService>();
            _gerenciamentoDocumentacaoRepository = Container.Resolve<IGerenciamentoDocumentacaoRepository>();
            _historicoEnvioRepository = Container.Resolve<IHistoricoEnvioDocumentacaoRepository>();
        }

        #endregion

        [Test]
        public void Executar()
        {
            _job = Container.Resolve<IJob>("core.job.GerenciamentoDocumentacao");
            _job.Execute(string.Empty);
        }

        [Test]
        public void GerarDocumentacaoPorId()
        {
            var ids = new int[] { 462 };

            foreach (var id in ids)
            {
                try
                {
                    var documento = Spd.BLL.BL_GerenciamentoDoc.SelectByPk(id);
                    // _gerenciamentoDocumentacaoRepository.ObterPorId(id);

                    if (documento != null)
                    {
                        var urlBase = documento.UrlAcesso;
                        var refaturamento = documento.IdGerenciamentoDocRefat != null;

                        // Caso desejar enviar a documentação para a pasta de homologação, descomentar a linha abaixo
                        // urlBase = urlBase.Replace("01. Producao", "02.l Homologacao");

                        // Caso desejar deixar em uma pasta local, setar abaixo
                        urlBase = @"C:\temp\Translogic\LaudoTemporario";

                        _documentacaoVagaoService.GerarPdfsDocumentos(documento, urlBase, refaturamento);
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        [Test]
        public void EnviarDocumentacao()
        {
            var histEnvioDoc = new HistoricoEnvioDocumentacao
            {
                OsId = 3888972,
                RecebedorId = 95008,
                Recomposicao = "N"
            };
            _documentacaoVagaoService.EnviarDocumentacao(histEnvioDoc);
        }

        [Test]
        public void MetodoEnviarDocumentacaoCompleto()
        {
            //_documentacaoVagaoService.ExecutarGerenciamentoDocumentacao();
            _documentacaoVagaoService.ExecutarEnvioDocumentacao();
        }

        [Test]
        public void GerarDocumentacaoGeral()
        {
            _documentacaoVagaoService.ExecutarGerenciamentoDocumentacao();
        }
    }
}
