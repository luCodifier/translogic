﻿namespace Translogic.Modules.Core.Tests.Domain.Jobs.EnvioDocumentacao
{
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Model.Documentacao.Repositories;
    using Translogic.Modules.Core.Domain.Services.Documentacao;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Tests;

    class EnvioDocumentacaoJob : BaseTranslogicContainerTestCase
    {
        private IJob _job;

        private DocumentacaoVagaoService _documentacaoVagaoService;
        private IGerenciamentoDocumentacaoRepository _gerenciamentoDocumentacaoRepository;
        private IHistoricoEnvioDocumentacaoRepository _historicoEnvioRepository;

        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            RealizarRollback = false;
            TemTransacao = true;

            _documentacaoVagaoService = Container.Resolve<DocumentacaoVagaoService>();
            _gerenciamentoDocumentacaoRepository = Container.Resolve<IGerenciamentoDocumentacaoRepository>();
            _historicoEnvioRepository = Container.Resolve<IHistoricoEnvioDocumentacaoRepository>();
        }

        #endregion

        [Test]
        public void Executar()
        {
            _job = Container.Resolve<IJob>("core.job.EnvioDocumentacaoEmail");
            _job.Execute(string.Empty);
        }

        [Test]
        public void GerarHtmlEmail()
        {
            var id = 4006;

            var html = string.Empty;

            var historicoEnvioDocumentacao = _historicoEnvioRepository.ObterPorId(id);
            if (historicoEnvioDocumentacao != null)
            {
                var documentacao = historicoEnvioDocumentacao.Documentacao ??
                                    _gerenciamentoDocumentacaoRepository.ObterPorOs(historicoEnvioDocumentacao.OsId, historicoEnvioDocumentacao.RecebedorId, false);

                if (documentacao != null)
                {
                    html = _documentacaoVagaoService.GerarHtmlEmail(historicoEnvioDocumentacao.Documentacao,
                                                                    historicoEnvioDocumentacao);
                }
            }

            // Pegue o valor da variável HTML e inserira em um arquivo html e abra em um navegador para 
            // visualizar o seu resultado.
            html = string.Format("<html><body>{0}</body></html>", html);
        }
    }
}
