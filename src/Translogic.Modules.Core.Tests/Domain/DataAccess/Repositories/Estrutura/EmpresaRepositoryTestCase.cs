namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Estrutura
{
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Estrutura;
	using Core.Domain.Model.Estrutura;
	using Core.Domain.Model.Estrutura.Repositories;
	using NHibernate.Criterion;
	using NUnit.Framework;

	[TestFixture]
	public class EmpresaRepositoryTestCase : RepositoryTestCase<EmpresaRepository, Empresa, int?>
	{
		[Test]
		public void ObterPaginado()
		{
			DetalhesPaginacao detalhes = new DetalhesPaginacao {Limite = 10};
			DetachedCriteria criteria = DetachedCriteria.For<Empresa>().
				AddOrder(Order.Asc("DescricaoDetalhada"));

			ResultadoPaginado<Empresa> paginado = Repository.ObterPaginado(criteria, detalhes);

			foreach (Empresa item in paginado.Items)
			{
				Assert.IsTrue(item is EmpresaCliente || item is EmpresaFerrovia);
			}
		}
	}
}