namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Estrutura
{
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Estrutura;
	using Core.Domain.Model.Estrutura;
	using NHibernate.Criterion;
	using NUnit.Framework;

	[TestFixture]
	public class EmpresaClienteRepositoryTestCase : RepositoryTestCase<EmpresaClienteRepository, EmpresaCliente, int?>
	{
		[Test]
		public void ObterPaginado()
		{
			var detalhes = new DetalhesPaginacao {Limite = 10};
			DetachedCriteria criteria = DetachedCriteria.For<EmpresaCliente>().
				AddOrder(Order.Asc("DescricaoDetalhada"));

			ResultadoPaginado<EmpresaCliente> paginado = Repository.ObterPaginado(criteria, detalhes);

			foreach (EmpresaCliente item in paginado.Items)
			{
				Assert.IsNotNull(item);
			}
		}

		[Test]
		public void ObterPorId()
		{
			EmpresaCliente empresa = Repository.ObterPorId(5087);
			string emailXml = empresa.EmailXml;
			string emailNfe = empresa.EmailNfe;
		}

		[Test]
		public void ObterPorCnpj()
		{
			string cnpj = "01258944003818";
			string uf = "RS";
			IEmpresa empresa = Repository.ObterEmpresaClientePorCnpjDaFerrovia(cnpj, uf);
		}

		[Test]
		public void ObterClientePorSigla()
		{
			string sigla = "7875-0";
			IEmpresa empresa = Repository.ObterPorSiglaSap(sigla);
			var retorno = RegraFerroesteBunge(empresa);
		}

		[Test]
		public void TestarLinq()
		{
		}

		private bool RegraFerroesteBunge(IEmpresa empresa)
		{
			string siglaEmpresa = empresa.Sigla;
			if (!string.IsNullOrEmpty(siglaEmpresa))
			{
				if (siglaEmpresa.Length >= 4)
				{
					// Pela sigla da empresa verifica se � a Bunge 7230 e 7875
					if ((siglaEmpresa.Substring(0, 4) == "7230") || (siglaEmpresa.Substring(0, 4) == "7875"))
					{
						return true;
					}
				}
			}

			return false;
		}
	}
}