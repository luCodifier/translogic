namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Estrutura
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Estrutura;
	using Core.Domain.Model.Estrutura;
	using NHibernate.Criterion;
	using NUnit.Framework;

	[TestFixture]
	public class EmpresaFerroviaRepositoryTestCase :
		CrudRepositoryTestCase<EmpresaFerroviaRepository, EmpresaFerrovia, int?>
	{
		protected override void PopularParaInsercao(EmpresaFerrovia entidade)
		{
			entidade.Apelido = "ABC";
			entidade.Celular = "8418-0299";
			entidade.Cep = "83702-090";
			entidade.Cgc = "0123456789001";
			entidade.Contato = "Usuario";
			entidade.Cpf = "123123";
			entidade.DescricaoDetalhada = "abc";
			entidade.DescricaoResumida = "321321";
			entidade.Email = "somebody@all-logistica.com";
			entidade.Endereco = "rua desconhecida";
		}

		protected override void PopularParaAtualizacao(EmpresaFerrovia entidade)
		{
			entidade.Apelido = "ABCD";
			entidade.Celular = "0299-8418";
			entidade.Cep = "83702-190";
			entidade.Cgc = "123456789";
			entidade.Contato = "Teste";
			entidade.Cpf = "123456789";
			entidade.DescricaoDetalhada = "abcDEF";
			entidade.DescricaoResumida = "ABC-123";
			entidade.Email = "somebody@all-logistica.com";
			entidade.Endereco = "rua teste";
		}

		protected override void VerificarIgualdade(EmpresaFerrovia entidade, EmpresaFerrovia recuperada)
		{
			Assert.AreEqual(entidade.Apelido, recuperada.Apelido);
			Assert.AreEqual(entidade.Celular, recuperada.Celular);
			Assert.AreEqual(entidade.Cep, recuperada.Cep);
			Assert.AreEqual(entidade.Cgc, recuperada.Cgc);
			Assert.AreEqual(entidade.Contato, recuperada.Contato);
			Assert.AreEqual(entidade.Cpf, recuperada.Cpf);
			Assert.AreEqual(entidade.DescricaoDetalhada, recuperada.DescricaoDetalhada);
			Assert.AreEqual(entidade.DescricaoResumida, recuperada.DescricaoResumida);
			Assert.AreEqual(entidade.Email, recuperada.Email);
			Assert.AreEqual(entidade.Endereco, recuperada.Endereco);
			Assert.AreEqual(entidade.Estado, recuperada.Estado);
			Assert.AreEqual(entidade.Fax, recuperada.Fax);
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.IndAtendimentoPreferencial, recuperada.IndAtendimentoPreferencial);
			Assert.AreEqual(entidade.IndEmpresaALL, recuperada.IndEmpresaALL);
			Assert.AreEqual(entidade.InscricaoEstadual, recuperada.InscricaoEstadual);
			Assert.AreEqual(entidade.InscricaoMunicipal, recuperada.InscricaoMunicipal);
			Assert.AreEqual(entidade.NomeFantasia, recuperada.NomeFantasia);
			Assert.AreEqual(entidade.Observacao, recuperada.Observacao);
			Assert.AreEqual(entidade.RazaoSocial, recuperada.RazaoSocial);
			Assert.AreEqual(entidade.Sigla, recuperada.Sigla);
			Assert.AreEqual(entidade.Telefone1, recuperada.Telefone1);
			Assert.AreEqual(entidade.Telefone2, recuperada.Telefone2);
			Assert.AreEqual(entidade.Telex, recuperada.Telex);
			Assert.AreEqual(entidade.Tipo, recuperada.Tipo);
			Assert.AreEqual(entidade.TipoPessoa, recuperada.TipoPessoa);
			Assert.AreEqual(entidade.VersionDate, recuperada.VersionDate);
		}

		[Test]
		public void ObterPaginado()
		{
			DetalhesPaginacao detalhes = new DetalhesPaginacao {Limite = 10};
			DetachedCriteria criteria = DetachedCriteria.For<EmpresaFerrovia>().
				AddOrder(Order.Asc("DescricaoDetalhada"));

			ResultadoPaginado<EmpresaFerrovia> paginado = Repository.ObterPaginado(criteria, detalhes);

			foreach (EmpresaFerrovia item in paginado.Items)
			{
				Assert.IsNotNull(item);
			}
		}

		[Test]
		public void ObterPorSigla()
		{
			EmpresaFerrovia f1 = Repository.ObterPorSigla("F");
			EmpresaFerrovia f2 = Repository.ObterPorSigla("T");
			EmpresaFerrovia f3 = Repository.ObterPorSigla("Z");
			EmpresaFerrovia f4 = Repository.ObterPorSigla("B");

			EmpresaFerrovia fx = Repository.ObterPorSiglaUf("L","RS");
		}
	}
}