namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Estrutura
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.Estrutura;
    using Core.Domain.Model.Estrutura;
    using NUnit.Framework;

    [TestFixture]
    public class UnidadeProducaoRepositoryTestCase :
        RepositoryTestCase<UnidadeProducaoRepository, UnidadeProducao, int?>
    {
        [Test]
        public void obter()
        {
            UnidadeProducao unidadeProducao = Repository.ObterPrimeiro();
            Assert.IsTrue(unidadeProducao != null);
        }
    }
}