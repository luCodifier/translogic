using System.Collections.Generic;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro;
using Translogic.Modules.Core.Domain.Model.Seguro;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Seguro
{
	[TestFixture]
	public class ModalSeguroRepositoryTestCase : RepositoryTestCase<ModalSeguroRepository, ModalSeguro, int>
	{
		public override void Setup()
		{
			base.Setup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<ModalSeguro> modalSeguroList = Repository.ObterTodos();

			Assert.IsTrue(modalSeguroList.Count > 1);
		}
	}
}