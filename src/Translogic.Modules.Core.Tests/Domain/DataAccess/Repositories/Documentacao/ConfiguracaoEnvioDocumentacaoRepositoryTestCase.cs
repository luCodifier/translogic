﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.Dominio;
using ALL.TestsSupport.AcessoDados;
using NHibernate.Criterion;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Documentacao;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Estrutura;
using Translogic.Modules.Core.Domain.Model.Documentacao;
using Translogic.Modules.Core.Domain.Model.Estrutura;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Documentacao
{
    [TestFixture]
    class ConfiguracaoEnvioDocumentacaoRepositoryTestCase : RepositoryTestCase<ConfiguracaoEnvioDocumentacaoRepository, ConfiguracaoEnvioDocumentacao, int>
    {
        [Test]
        public void ObterPaginado()
        {
            var t = Repository.ObterPorCnpjEmpresa("25278404000172");
            Assert.NotNull(t);
        }
    }
}
