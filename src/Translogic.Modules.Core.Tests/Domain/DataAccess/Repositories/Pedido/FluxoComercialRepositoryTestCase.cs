namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Pedido
{
	using System.Collections;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.Estrutura;
	using Core.Domain.Model.FluxosComerciais;
	using Core.Domain.Model.Via;
	using NUnit.Framework;

	[TestFixture]
	public class FluxoComercialRepositoryTestCase : RepositoryTestCase<FluxoComercialRepository, FluxoComercial, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<FluxoComercial> fluxos = Repository.ObterTodos();
		}

		[Test]
		public void ObterFluxoParaRateio()
		{
			IList<FluxoComercial> fluxos = Repository.ObterFluxoParaRateio(4532, "69623", "40033", "12010090");
		}

		[Test]
		public void ObterFluxoPorDespacho()
		{
			FluxoComercial fluxo = Repository.ObterFluxoPorDespachoFluxo(6028758, string.Empty);
			int? IdEmpresaOrigem = fluxo.Origem.EmpresaOperadora.Id;

			// FluxoComercial fluxo = Repository.ObterPorId(77841);
		}

		[Test]
		public void ObterFluxoIntermodalNaoTravado()
		{
			FluxoComercial fluxo = Repository.ObterFerroviarioPorCodigo("10227");
			fluxo = Repository.ObterFerroviarioPorCodigo("66867");
		}

		[Test]
		public void ObterFluxoTela1274()
		{
			FluxoComercial fluxoComercial = Repository.ObterFerroviarioPorCodigo("13223");
			
			var hh = fluxoComercial;
		}



		

		[Test]
		public void TestarFluxo()
		{
			FluxoComercial fc = Repository.ObterPorId(40193);
			EstacaoMae origemInterna = fc.OrigemIntercambio;
			EstacaoMae destinoInterno = fc.DestinoIntercambio;
			EstacaoMae origem = fc.Origem;
			EstacaoMae destino = fc.Destino;
			IEmpresa ecOrigem = origem.EmpresaConcessionaria;
			IEmpresa ecDestino = destino.EmpresaConcessionaria;
		}

		[Test]
		public void TestarObter()
		{
			FluxoComercial fc = Repository.ObterFerroviarioPorCodigo("64728");
			Contrato contrato = fc.Contrato;
			IEmpresa empresa = contrato.EmpresaPagadora;
			string descricao = empresa.DescricaoResumida.ToUpper();
			bool encontrado = (descricao.IndexOf("BRADO") > -1 ? true : false);

			FluxoComercial fc1 = Repository.ObterFerroviarioPorCodigo("62753");
			contrato = fc1.Contrato;
			empresa = contrato.EmpresaPagadora;
			descricao = empresa.DescricaoResumida.ToUpper();
			encontrado = (descricao.IndexOf("BRADO") > -1 ? true : false);
		}
	}
}