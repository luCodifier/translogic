﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Pedido
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	[TestFixture]
	public class FluxoExcluirCteRepositoryTestCase : RepositoryTestCase<FluxoExcluirCteRepository, FluxoExcluirCte, string>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorCodigo()
		{
			string idfluxo = "84354";
			FluxoExcluirCte fluxoExcluirCte = Repository.ObterPorId(idfluxo);
		}

	}
}