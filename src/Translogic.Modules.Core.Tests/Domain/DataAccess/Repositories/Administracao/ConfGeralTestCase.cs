﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Administracao
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.Administracao;
    using Core.Domain.Model.Administracao;
    using NUnit.Framework;

    [TestFixture]
    public class ConfGeralTestCase : RepositoryTestCase<ConfGeralRepository, ConfGeral, string>
    {
        [Test]
        public void ObterTodos()
        {
            var valor = Repository.ObterPorId("TRAVA_CTE");
            Assert.NotNull(valor);
        }

        [Test]
        public void SaveTeste()
        {
            var valor = new ConfGeral("teste") {Valor = "v1", Descricao = "descr"};
            Repository.Inserir(valor);
        }
    }
}
