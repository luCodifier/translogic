namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Bolar
{
    using System;
    using ALL.TestsSupport.AcessoDados;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
	using Translogic.Modules.Core.Domain.Model.Bolar;

	using NUnit.Framework;

	public class BolarBoCteRepositoryTestCase : RepositoryTestCase<BolarCteRepository, BolarCte, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
		    var lista = Repository.ObterTodos();
		    Console.Write(lista);
		}

		[Test]
        public void InserirNovo()
		{
		    BolarCte bo = new BolarCte();
		    bo.CnpjEmit = 1234;
		    bo.UfEmit = "PR";
		    bo.TipoServico = "1";
            bo.ChaveCte = "";
            bo.CnpjRem = 1234;
            bo.CnpjDest = 1234;
		    bo.PesoCte = 1;
            Repository.Inserir(bo);
        }
	}
}