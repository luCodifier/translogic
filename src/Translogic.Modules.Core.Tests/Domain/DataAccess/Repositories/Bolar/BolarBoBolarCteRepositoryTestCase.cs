﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Bolar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ALL.TestsSupport.AcessoDados;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
    using Translogic.Modules.Core.Domain.Model.Bolar;


    public class BolarBoBolarCteRepositoryTestCase : RepositoryTestCase<BolarBoBolarCteRepository, BolarBoBolarCte, int>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterTodos()
        {
            var lista = Repository.ObterTodos();
            Console.Write(lista);
        }

        [Test]
        public void InserirNovo()
        {
            var bo = new BolarBoBolarCte();
            var bolarCteRepository = Resolve<BolarCteRepository>();
            var boBolarRepository = Resolve<BolarBoRepository>();
            var BolarBoBolarCteRepository = Resolve<BolarBoBolarCteRepository>();

            bo.BolarBo = boBolarRepository.ObterPorId(346);
            bo.BolarCte = bolarCteRepository.ObterPorId(6); 

            try
            {
                BolarBoBolarCteRepository.Inserir(bo);
                Session.Flush();
                Session.Clear();
                Session.Transaction.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message));
                throw;
            }

        }

        [Test]
        public void ObterPorCte()
        {
            var BolarBoBolarCteRepository = Resolve<BolarBoBolarCteRepository>();
            try
            {
                var lista = BolarBoBolarCteRepository.ObterPorIdCte(5);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message));
                throw;
            }
        }

        [Test]
        public void ObterPorBolarBo()
        {
            var BolarBoBolarCteRepository = Resolve<BolarBoBolarCteRepository>();
            try
            {
                var lista = BolarBoBolarCteRepository.ObterPorIdBolar(344);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message));
                throw;
            }
        }
    }
}
