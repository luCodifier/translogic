﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.Core.Dominio;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Via.Circulacao;
using Translogic.Modules.Core.Domain.Model.Bolar;
using Translogic.Modules.Core.Domain.Model.Via;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Bolar
{
    [TestFixture]
    public class BoBolarRepositoryTestCase : RepositoryTestCase<BolarBoRepository, BolarBo, int>
    {
        [Test]
        public void PesquisarTestCase()
        {
            var resultado = Repository.Pesquisar(new DetalhesPaginacao(), DateTime.Parse("01/01/2017"), DateTime.Parse("29/11/2017"), null,
                                                 null, null, null, null);

            Assert.IsTrue(resultado.Items.Any());
        }

        [Test]
        public void ObterEmpresasBloqueadasFaturamentoManualTestCase()
        {
            var resultado = Repository.EmpresaBloqueadaFaturamentoManual("85693");

            Assert.IsTrue(resultado);
        }
    }
}
