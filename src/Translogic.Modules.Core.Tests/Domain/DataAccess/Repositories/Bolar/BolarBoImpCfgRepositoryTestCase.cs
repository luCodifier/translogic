namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Bolar
{
	using System.Collections.Generic;

	using ALL.TestsSupport.AcessoDados;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
	using Translogic.Modules.Core.Domain.Model.Bolar;
	using Translogic.Modules.Core.Domain.Model.Diversos;
	using Translogic.Modules.Core.Domain.Model.Estrutura;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais;

	using NUnit.Framework;

	public class BolarBoImpCfgRepositoryTestCase : RepositoryTestCase<BolarBoImpCfgRepository, BolarBoImpCfg, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorCnpj()
		{
			var obj = this.Repository.ObterPorCnpjAtivo("36785418001502");
		}
	}
}