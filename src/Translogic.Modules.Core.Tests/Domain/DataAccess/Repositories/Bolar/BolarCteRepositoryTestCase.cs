using Castle.Services.Transaction;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Bolar
{
    using System;
    using ALL.TestsSupport.AcessoDados;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Bolar;
	using Translogic.Modules.Core.Domain.Model.Bolar;

	using NUnit.Framework;

	public class BolarCteRepositoryTestCase : RepositoryTestCase<BolarCteRepository, BolarCte, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
		    var lista = Repository.ObterTodos();
		    Console.Write(lista);
		}

		[Test]
        public void InserirNovo()
		{
            BolarCte bo = new BolarCte();
		    ////bo.Id = 67;
            bo.CnpjEmit = 1234;
		    bo.UfEmit = "RJ";
		    bo.TipoServico = "1";
            bo.ChaveCte = "456234551";
            bo.CnpjRem = 1234;
            bo.CnpjDest = 1234;
		    bo.PesoCte = 1;

            var bolarCteRepository = Resolve<BolarCteRepository>();

		    try
		    {
                bolarCteRepository.Inserir(bo);
                Session.Flush();
		        Session.Clear();
                Session.Transaction.Commit();
            }
		    catch (Exception ex)
		    {
                Console.WriteLine(ex.Message + (ex.InnerException == null ? "" : ex.InnerException.Message));   
		        throw;
		    }
            
        }

	}
}