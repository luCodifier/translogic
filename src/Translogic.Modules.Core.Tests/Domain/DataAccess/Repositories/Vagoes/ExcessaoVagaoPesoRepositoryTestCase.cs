﻿using System.Linq;
using ALL.Core.Dominio;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Vagoes;
using Translogic.Modules.Core.Domain.Model.Vagoes;
using System;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Vagoes
{
    [TestFixture]
    public class ExcessaoVagaoPesoRepositoryTestCase : RepositoryTestCase<ExcessaoVagaoPesoRepository, ExcessaoVagaoPeso, int>
    {
        [Test]
        public void ObterDadosTestCase()
        {
            var todos = Repository.Obter(new DetalhesPaginacao(), DateTime.Parse("10/12/2017"),
                                         DateTime.Parse("11/12/2017"), null, null);
            Assert.IsTrue(todos.Items.Any());
        }

        [Test]
        public void CriarTestCase()
        {
            var item = new ExcessaoVagaoPeso
                           {
                               CodigoVagao = "0332941",
                               Peso = 10
                           };

            Repository.Inserir(item);
        }

        public System.DateTime DateTim { get; set; }
    }
}
