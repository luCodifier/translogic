namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Indicador.Vagao
{
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Indicador.Vagao;
	using Core.Domain.Model.Indicador.Vagao;
	using NUnit.Framework;

	[TestFixture]
	public class IndLotacaoVagoesTestCase : RepositoryTestCase<IndLotacaoVagoesRepository, IndLotacaoVagoes, int?>
	{
		[Test]
		public void ObterPaginado()
		{
			var detalhes = new DetalhesPaginacao {Limite = 10};
			ResultadoPaginado<IndLotacaoVagoes> paginado = Repository.ObterPaginado(detalhes);
			
			foreach (var indicador in paginado.Items	)
			{	
				Assert.IsNotNull(indicador);
			}
		}
	}
}