namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Indicador.TKB
{
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Indicador.TKB;
	using Core.Domain.Model.Indicador.TKB;
	using NUnit.Framework;

	[TestFixture]
	public class TLTKBCorredorTestCase : RepositoryTestCase<TLTKBCorredorRepository, TLTKBCorredor, int?>
	{
		[Test]
		public void ObterPaginado()
		{
			var detalhes = new DetalhesPaginacao {Limite = 10};
			ResultadoPaginado<TLTKBCorredor> paginado = Repository.ObterPaginado(detalhes);

			foreach (var indicador in paginado.Items)
			{
				Assert.IsNotNull(indicador);
			}
		}
	}
}