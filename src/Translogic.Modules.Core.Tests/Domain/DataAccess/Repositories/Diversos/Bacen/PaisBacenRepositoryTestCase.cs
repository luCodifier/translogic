﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Diversos.Bacen
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Diversos.Bacen;
	using Core.Domain.Model.Diversos.Bacen;
	using NUnit.Framework;

	class PaisBacenRepositoryTestCase : RepositoryTestCase<PaisBacenRepository, PaisBacen, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			PaisBacen bacen = Repository.ObterPorId(51);
		}

		[Test]
		public void ObterPaisPorSigla()
		{
			PaisBacen bacen = Repository.ObterPorSiglaResumida("cl");
		}

		[Test]
		public void ObterPorCodigoBacen()
		{
			PaisBacen bacen = Repository.ObterPorCodigoPais("639");
		}
	}
}
