namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Diversos.Ibge
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Diversos.Ibge;
	using Core.Domain.Model.Diversos.Ibge;
	using NUnit.Framework;

	[TestFixture]
	public class CidadeIbgeRepositoryTestCase : RepositoryTestCase<CidadeIbgeRepository, CidadeIbge, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			CidadeIbge cidade = Repository.ObterPorId(9449);
		}

		[Test]
		public void ObterPorCidade()
		{
			CidadeIbge cidade = Repository.ObterPorDescricaoUf("CANOAS", "RS");
		}
	}
}