namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Diversos.Ibge
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Diversos.Ibge;
	using Core.Domain.Model.Diversos.Ibge;
	using NUnit.Framework;

	[TestFixture]
	public class UfIbgeRepositoryTestCase : RepositoryTestCase<UfIbgeRepository, UfIbge, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			UfIbge uf = Repository.ObterPorId(41);
		}

		[Test]
		public void ObterPorSigla()
		{
			UfIbge uf = Repository.ObterPorSigla("SP");
		}
	}
}