﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Diversos
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Diversos;
	using Core.Domain.Model.Diversos;
	using NUnit.Framework;

	[TestFixture]
	public class CotacaoRepositoryTestCase : RepositoryTestCase<CotacaoRepository, Cotacao, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorData()
		{
			DateTime data = DateTime.Now;
			Cotacao cotacao = Repository.ObterCotacaoPorData(data, "USD");
		}
	}
}
