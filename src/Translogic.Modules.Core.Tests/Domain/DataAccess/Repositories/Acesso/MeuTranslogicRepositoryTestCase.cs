namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NHibernate.Criterion;
	using NUnit.Framework;

	public class MeuTranslogicRepositoryTestCase : CrudRepositoryTestCase<MeuTranslogicRepository, MeuTranslogic, int>
	{
		protected override void PopularParaInsercao(MeuTranslogic entidade)
		{
			entidade.Titulo = "Relatórios";
			entidade.Ordem = 0;
			entidade.Usuario = Resolve<UsuarioRepository>().ObterPrimeiro();
		}

		protected override void PopularParaAtualizacao(MeuTranslogic entidade)
		{
			entidade.Titulo = null;
			entidade.Menu = Resolve<MenuRepository>().ObterPrimeiro();
		}

		protected override void VerificarIgualdade(MeuTranslogic entidade, MeuTranslogic recuperada)
		{
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Ordem, recuperada.Ordem);
			Assert.AreEqual(entidade.Titulo, recuperada.Titulo);
			Assert.AreEqual(entidade.VersionDate, recuperada.VersionDate);
		}

		[Test]
		public void JaExiste_QuandoJaExiste_DeveRetornarTrue()
		{
			Usuario usuario = Resolve<UsuarioRepository>().ObterPrimeiro();
			Menu menu = Resolve<MenuRepository>().ObterPrimeiro();

			MeuTranslogic meuTranslogic = new MeuTranslogic { Usuario = usuario, Menu = menu };

			Repository.Inserir(meuTranslogic);

			Assert.IsTrue(Repository.JaExiste(usuario, menu));
		}

		[Test]
		public void JaExiste_QuandoNaoExiste_DeveRetornarFalse()
		{
			Usuario usuario = Resolve<UsuarioRepository>().ObterPrimeiro();
			Menu menu = Resolve<MenuRepository>().ObterPrimeiro();

			Assert.IsFalse(Repository.JaExiste(usuario, menu));
		}

		[Test]
		public void ObterUltimaOrdem()
		{
			Usuario usuario = Resolve<UsuarioRepository>().ObterPrimeiro();

			MeuTranslogic meuTranslogic = new MeuTranslogic { Usuario = usuario, Titulo = "Ultimo Item", Ordem = 999 };

			Repository.Inserir(meuTranslogic);

			Assert.AreEqual(999, Repository.ObterUltimaOrdem(usuario, null));
		}

		[Test]
		public void ObterUltimaOrdem_QuandoNaoExistemRegistros()
		{
			Usuario usuario = Resolve<UsuarioRepository>().ObterPrimeiro();

			Repository.Remover(Restrictions.Eq("Usuario", usuario));

			Assert.AreEqual(0, Repository.ObterUltimaOrdem(usuario, null));
		}
	}
}