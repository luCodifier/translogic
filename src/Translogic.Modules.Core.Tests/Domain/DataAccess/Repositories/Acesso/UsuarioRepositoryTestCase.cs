namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.DataAccess.Repositories.Estrutura;
	using Core.Domain.Model.Acesso;
	using Interfaces.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class UsuarioRepositoryTestCase : RepositoryTestCase<UsuarioRepository, Usuario, int?>
	{
		#region SETUP/TEARDOWN

		public override void Setup()
		{
			base.Setup();

			empresaRepository = Resolve<EmpresaFerroviaRepository>();
		}

		#endregion

		private EmpresaFerroviaRepository empresaRepository;

		[Test]
		public void Inserir()
		{
			var usuario = new Usuario
			              	{
			              		Ativo = true,
			              		Codigo = "TEST_" + Guid.NewGuid().ToString().Substring(0, 5),
			              		DataCriacao = DateTime.Now,
			              		Email = "somebody@all-logistica.com",
			              		Empresa = empresaRepository.ObterPrimeiro()
			              	};

			Repository.Inserir(usuario);

			Session.Flush();

			Assert.IsTrue(usuario.Id.HasValue);
		}

		[Test]
		public void ObterPorCodigo_QuandoExistir_DeveRetornar()
		{
			string codigo = "MIGRA";

			Usuario usuario = Repository.ObterPorCodigo(codigo);

			Assert.IsNotNull(usuario);
			Assert.AreEqual(codigo, usuario.Codigo);
		}

        [Test]
        public void ObterTodos()
        {
            IList<Usuario> usuarioList = Repository.ObterTodos();

            Assert.IsTrue(usuarioList.Count > 1);
        }

        [Test]
        public void ObterPorNome()
        {
            IList<UsuarioDto> usuarioList = Repository.ObterPorNome("Migr");

            Assert.IsTrue(usuarioList.Count > 1);
        }

	}
}