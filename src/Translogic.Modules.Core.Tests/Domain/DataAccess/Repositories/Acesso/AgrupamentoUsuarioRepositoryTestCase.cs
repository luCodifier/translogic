namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.DataAccess.Repositories.Estrutura;
	using Core.Domain.Model.Acesso;
	using Interfaces.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class AgrupamentoUsuarioRepositoryTestCase : RepositoryTestCase<AgrupamentoUsuarioRepository, AgrupamentoUsuario, int>
	{
        public override void TestSetup()
        {
            base.TestSetup();

            Configuration.AddAssembly(typeof(Usuario).Assembly);
        }

        [Test]
        public void ObterTodos()
        {
            Usuario usuario = new Usuario();
            usuario.Id = 347;

            IList<GrupoUsuario> grupos = Repository.ObterGruposPorUsuario(usuario);
        }
	}
}