namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Threading;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class MenuRepositoryTestCase : RepositoryTestCase<MenuRepository, Menu, int>
	{
		[Test]
		public void ConsegueInserir()
		{
			var item = new Menu
			           	{
			           		CodigoTela = 1,
			           		Path = "/",
			           		Chave = "Home",
			           		Ativo = false
			           	};

			IList<MenuI18N> i18n = new List<MenuI18N> {new MenuI18N {Menu = item, Titulo = "inicial"}};

			item.I18N = i18n;

			Repository.Inserir(item);
		}

		[Test]
		public void ConsegueSelecionar()
		{
			//Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");

			Session.EnableFilter("Culture").SetParameter("CultureName", Thread.CurrentThread.CurrentCulture.Name);

			IList<Menu> todos = Repository.ObterItensAtivos();

			foreach (Menu menu in todos)
			{
				Console.WriteLine(menu.Titulo);
			}
		}

		[Test]
		public void ConsegueCriarBreadcrumbs()
		{
			var tela = Repository.ObterItensAtivos().Where(menu => menu.CodigoTela.Equals(101)).First();

			var breadcrumbs = tela.Breadcrumbs;
		}
	}
}