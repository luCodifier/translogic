namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Acesso.Repositories;
	using NUnit.Framework;

	public class LogAuditoriaRepositoryTestCase :
		CrudRepositoryTestCase<LogAuditoriaRepository, LogAuditoria, int>
	{
		protected override void PopularParaInsercao(LogAuditoria entidade)
		{
			entidade.Quando = DateTime.Now;
			entidade.TipoEntidade = typeof (Usuario);
			entidade.EntidadeId = 1.ToString();
			entidade.Operacao = OperacaoAuditoria.Insercao;
			entidade.Usuario = Resolve<UsuarioRepository>().ObterPorCodigo("MIGRA");
		}

		protected override void PopularParaAtualizacao(LogAuditoria entidade)
		{
			entidade.Quando = DateTime.Now;
			entidade.TipoEntidade = typeof (Usuario);
			entidade.EntidadeId = 2.ToString();
			entidade.Operacao = OperacaoAuditoria.Atualizacao;
		}

		protected override void VerificarIgualdade(LogAuditoria entidade, LogAuditoria recuperada)
		{
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Operacao, recuperada.Operacao);
			Assert.AreEqual(entidade.Quando, recuperada.Quando);
			Assert.AreEqual(entidade.TipoEntidade, recuperada.TipoEntidade);
			Assert.AreEqual(entidade.Alteracoes, recuperada.Alteracoes);
		}
	}
}