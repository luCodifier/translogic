namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Adaptations;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NHibernate.Criterion;
	using NUnit.Framework;

	[TestFixture]
	public class LogUsuarioRepositoryTestCase : RepositoryTestCase<LogUsuarioRepository, LogUsuario, int>
	{
		private EventoUsuarioRepository eventoRepository;
		private UsuarioRepository usuarioRepository;

		public override void Setup()
		{
			base.Setup();
			eventoRepository = Resolve<EventoUsuarioRepository>();
			usuarioRepository = Resolve<UsuarioRepository>();
		}

		[Test, Ignore]
		public void Inserir()
		{
			var log = new LogUsuario();

			EventoUsuario eventoUsuario = eventoRepository.ObterPorCodigo(EventoUsuarioCodigo.SenhaAlterada);

			log.Usuario = usuarioRepository.ObterPorCodigo("MIGRA");

			log.Evento = eventoUsuario;

			Repository.Inserir(log);

			Assert.AreNotEqual(default(int), log.Id);
		}

		[Test]
		public void ObterLogsPorUsuarioNoPeriodo()
		{
			Usuario usuario = usuarioRepository.ObterPorCodigo("MIGRA");

			var final = new DateTime(2009, 07, 02, 17, 15, 00);
			DateTime inicial = final - TimeSpan.FromMinutes(10);

			IList<LogUsuario> logs = Repository.ObterLogsPorUsuarioNoPeriodo(usuario, inicial, final);
		}

		[Test]
		public void ObterPaginado_UsuarioAD_e_IP_NaoNulo()
		{
			ResultadoPaginado<LogUsuario> logs = Repository.ObterPaginado(new DetalhesPaginacao {Limite = 10},
			                                                              Restrictions.IsNotNull("UsuarioAD") ||
			                                                              Restrictions.IsNotNull("IP"));
		}

		[Test]
		public void ObterUltimoLogPorUsuario()
		{
			Usuario usuario = usuarioRepository.ObterPorCodigo("MIGRA");

			LogUsuario ultimoLog = Repository.ObterUltimoLogPorUsuario(usuario);
		}

		[Test]
		public void Vigente()
		{
			Console.WriteLine("Tabela full...");

			DetachedCriteria.For<LogUsuario>().GetExecutableCriteria(Session).List();

			Console.WriteLine("Agora apenas vigente...");

			DetachedCriteria.For<LogUsuarioVigente>().GetExecutableCriteria(Session).List();
		}
	}
}