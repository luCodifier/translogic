namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class TokenAcessoRepositoryTestCase :
		CrudRepositoryTestCase<TokenAcessoRepository, TokenAcesso, int>
	{
		private Usuario usuario;

		protected override void CriarDependencias()
		{
			base.CriarDependencias();

			usuario = Resolve<UsuarioRepository>().ObterPorCodigo("MIGRA");
		}

		protected override void PopularParaInsercao(TokenAcesso entidade)
		{
			entidade.Idioma = "PT";
			entidade.Token =
				"B42DCE03984D3C4D18A9A4D5DF472C778F2A08E066FC637015C8ABEDB6E36B2B022946A58951EC8990A716AC7A6D2318B1DCBB63C31CBD8A1D8F2E27DC66F07A73A9CCB3E50FA47C43BC981EB21F1857";
			entidade.Usuario = usuario;
		}

		protected override void PopularParaAtualizacao(TokenAcesso entidade)
		{
			entidade.Idioma = "EN";
			entidade.Token = "AAAAAABBBBBBBBCCCCCCCC";
		}

		protected override void VerificarIgualdade(TokenAcesso entidade, TokenAcesso recuperada)
		{
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Idioma, recuperada.Idioma);
			Assert.AreEqual(entidade.Token, recuperada.Token);
			Assert.AreEqual(entidade.Usuario, recuperada.Usuario);
		}
	}
}