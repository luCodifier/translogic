namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Acesso
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Acesso;
	using Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class EventoUsuarioRepositoryTestCase :
		CrudRepositoryTestCase<EventoUsuarioRepository, EventoUsuario, int>
	{
		protected override void PopularParaInsercao(EventoUsuario entidade)
		{
			entidade.Codigo = EventoUsuarioCodigo.ContaAtivada;
			entidade.Descricao = "ABCDE";
		}

		protected override void PopularParaAtualizacao(EventoUsuario entidade)
		{
			entidade.Codigo = EventoUsuarioCodigo.LoginEfetuadoComSucesso;
			entidade.Descricao = "ASDASDASD";
		}

		protected override void VerificarIgualdade(EventoUsuario entidade, EventoUsuario recuperada)
		{
			Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Codigo, recuperada.Codigo);
			Assert.AreEqual(entidade.Descricao, recuperada.Descricao);
		}

		[Test]
		public void ObterPorCodigo()
		{
			EventoUsuario eventoContaAtivada = Repository.ObterPorCodigo(EventoUsuarioCodigo.ContaAtivada);
			EventoUsuario eventoContaBloqueada = Repository.ObterPorCodigo(EventoUsuarioCodigo.ContaBloqueada);

			Assert.AreEqual(EventoUsuarioCodigo.ContaAtivada, eventoContaAtivada.Codigo);
			Assert.AreEqual(EventoUsuarioCodigo.ContaBloqueada, eventoContaBloqueada.Codigo);
		}
	}
}