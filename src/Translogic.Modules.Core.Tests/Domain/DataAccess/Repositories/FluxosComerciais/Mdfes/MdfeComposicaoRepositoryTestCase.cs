﻿using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
    using Core.Domain.Model.FluxosComerciais.Mdfes;
    using NUnit.Framework;

    [TestFixture]
    public class MdfeComposicaoRepositoryTestCase : RepositoryTestCase<MdfeComposicaoRepository, MdfeComposicao, int?>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorId()
        {
           var retorno = Repository.ListarDetalhesDaComposicao(14931);
        }

        [Test]
        public void Testar_BuscaOsDoCTEdoMDFE()
        {
            //var retorno = Repository.BuscarUfDosCteComposicaoMdfe(14931);
        }

    }
}