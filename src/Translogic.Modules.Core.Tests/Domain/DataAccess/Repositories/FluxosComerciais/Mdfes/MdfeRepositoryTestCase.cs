﻿using System;
using ALL.Core.Dominio;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
    using Core.Domain.Model.FluxosComerciais.Mdfes;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.Mdfes;

    [TestFixture]
    public class MdfeRepositoryTestCase : RepositoryTestCase<MdfeRepository, Mdfe, int?>
    {
        private readonly MdfeService _mdfeService;

        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void teste()
        {
            var lista = Repository.ObterListaEnvioPorHost("Maquina16", 60);
        }

        [Test]
        public void ObterStatusProcessamentoEnvio()
        {
            var lista = Repository.ObterStatusProcessamentoEnvio(1);
        }

        [Test]
        public void Listar()
        {
            var lista = Repository.Listar(new DetalhesPaginacao(), string.Empty, string.Empty, string.Empty, string.Empty, null, DateTime.Now.AddDays(-1), DateTime.Now, string.Empty, string.Empty, 0);
        }

        [Test]
        public void ListarDetalhesDaMdfe()
        {
            var lista = Repository.ListarDetalhesDaMdfe(1152799);
        }
    }
}