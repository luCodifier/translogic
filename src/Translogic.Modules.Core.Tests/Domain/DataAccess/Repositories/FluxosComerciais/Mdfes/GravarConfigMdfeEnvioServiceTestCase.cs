using NUnit.Framework;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.Repositories;
using Translogic.Modules.Core.Domain.Model.Via.Circulacao.Repositories;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Tests;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	public class GravarConfigMdfeEnvioServiceTestCase : BaseTranslogicContainerTestCase
	{
		private GravarConfigDadosEnvioMdfeService _service;
		private IMdfeRepository _mdfeRepository;

		#region SETUP/TEARDOWN
		protected override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
			_service = Container.Resolve<GravarConfigDadosEnvioMdfeService>();
			_mdfeRepository = Container.Resolve<IMdfeRepository>();
		}
		#endregion

		[Test]
		public void TestarGeracaoMdfe()
		{
            Mdfe mdfe = _mdfeRepository.ObterPorId(669885);

			_service.Executar(mdfe);
		}
	}
}