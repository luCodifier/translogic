namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using NUnit.Framework;
	using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

	[TestFixture]
	public class Mdfe06AgrupImpRepositoryTestCase : RepositoryTestCase<Mdfe06AgrupImpRepository, Mdfe06AgrupImp, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			var retorno = Repository.ObterTodos();
		}
	}
}