using System.Collections.Generic;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Mdfes.TriangulusConfig;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes.TriangulusConfig
{
	
	[TestFixture]
	public class Mdfe01ListaRepositoryTestCase : RepositoryTestCase<Mdfe01ListaRepository, Mdfe01Lista, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<Mdfe01Lista> retorno = Repository.ObterTodos();
		}
	}
}