﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Mdfes;
	using Core.Domain.Model.FluxosComerciais.Mdfes;
	using NUnit.Framework;

	[TestFixture]
	public class MdfeArvoveRepositoryTestCase : RepositoryTestCase<MdfeArvoreRepository, MdfeArvore, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}
	}
}