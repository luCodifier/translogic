﻿using Castle.Services.Transaction;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.InterfaceSap.Repositories;
using Translogic.Modules.Core.Domain.Services.Ctes.Interface;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Sap
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap;
    using Core.Domain.Model.FluxosComerciais.InterfaceSap;
    using NUnit.Framework;

    [TestFixture]
    public class SapConteinerCteRepositoryTestCase : RepositoryTestCase<SapConteinerCteRepository, SapConteinerCte, int>
    {
        private ISapDespCteRepository _sapDespCteRepository;

        public override void TestSetup()
        {
            base.TestSetup();
            ////HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
        }

        [Test]
        public void TestarInsert()
        {
            SapConteinerCte sap = new SapConteinerCte();
            sap.SapDespCte = new SapDespCte() { Id = 3791286, Ambiente = 2, SerDesp5 = "614", SerDesp6 = "1"};
            sap.CodConteiner = "GESU9491980";
            sap.Agrupamento = 1;

            Repository.Inserir(sap);
        }
    }
}