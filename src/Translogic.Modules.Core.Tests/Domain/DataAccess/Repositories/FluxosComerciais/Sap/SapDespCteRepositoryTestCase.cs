namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Sap
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap;
	using Core.Domain.Model.FluxosComerciais.InterfaceSap;
	using NUnit.Framework;

	[TestFixture]
	public class SapDespCteRepositoryTestCase : RepositoryTestCase<SapDespCteRepository, SapDespCte, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsert()
		{
			SapDespCte sap = new SapDespCte();
			sap.CteSubstituido = null;
			sap.Serie5CteSubstituido = null; 
			sap.Dcl5CteSubstituido = null;

			Repository.Inserir(sap);
		}
	}
}