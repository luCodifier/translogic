namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Sap
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.InterfaceSap;
	using Core.Domain.Model.FluxosComerciais.InterfaceSap;
	using NUnit.Framework;

	[TestFixture]
	public class SapChaveCteRepositoryTestCase : RepositoryTestCase<SapChaveCteRepository, SapChaveCte, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorChave()
		{
			string chaveCte = "51120324962466000136570020000006871905516276";
			SapChaveCte scc = Repository.ObterPorChaveCte(chaveCte);
			SapDespCte sdc = scc.SapDespCte;
		}
	}
}