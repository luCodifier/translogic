namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Sap
{
	using System.Collections.Generic;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteInterfaceEnvioSapRepositoryTestCase : RepositoryTestCase<CteInterfaceEnvioSapRepository, CteInterfaceEnvioSap, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarDadosEnvio()
		{
			string hostName = "TL-FWPS01";
			IList<CteInterfaceEnvioSap> lista = Repository.ObterNaoProcessadosPorHost(hostName);
			int i = lista.Count;
		}

		[Test]
		public void TestarAtualizacaoEnvio()
		{
			Cte cte = new Cte {Id = 358};
			Repository.AtualizarEnvioSap(cte);
		}

		[Test]
		public void ObterSemHost()
		{
			IList<CteInterfaceEnvioSap> lista = Repository.ObterNaoProcessadosSemHost();
		}
	}
}