﻿using System.Collections.Generic;
using System.Linq;
using Castle.Services.Transaction;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao
{
    using System;
    using ALL.TestsSupport.AcessoDados;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.DocumentosVagao;
    using Translogic.Modules.Core.Domain.Model.Estrutura;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.DocumentosVagao;

    [TestFixture]
    public class TicketBalancaRepositoryTestCase : RepositoryTestCase<TicketBalancaRepository, TicketBalanca, int?>
    {
        public override void TestSetup()
        {
            RealizarRollback = false;
            base.TestSetup();
        }

        [Test]
        public void Obter()
        {
            var ssretorno = Repository.ObterTodos();
            TicketBalanca retorno = Repository.ObterPorId(16);
        }

        [Test]
        public void ObterNotasTicketsPorVagao()
        {
            var ssretorno = Repository.ObterNotasTicketsPorVagao(2994051, new List<int>() { 321448 });
            // var ticket = ssretorno[0].Vagao.Ticket;
        }

        [Test]
        public void ObterNotasTicketItensDespacho()
        {
            var ssretorno = Repository.ObterNotasTicketItensDespacho(new List<int>() { 9677407 });
            var ticket = ssretorno[0].Vagao.Ticket;
        }
    }
}