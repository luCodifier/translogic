namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos;
	using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	[TestFixture]
	public class VagaoPedidoRepositoryTestCase : RepositoryTestCase<VagaoPedidoRepository, VagaoPedido, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			var composicao = new ComposicaoVagao { Vagao = new Vagao { Id = 299292 }, Pedido = new Pedido { Id = 6350938 } };

			var lista = Repository.ObterVagoesPedidosPorComposicaoVagao(composicao);
		}
	}
}