namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System.Collections.Generic;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos;
    using Core.Domain.Model.FluxosComerciais.Pedidos;
    using Core.Domain.Model.FluxosComerciais;
    using NUnit.Framework;

    [TestFixture]
    public class PedidoRepositoryTestCase : RepositoryTestCase<PedidoRepository, Pedido, int?>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterTodos()
        {
            IList<Pedido> pedidos = Repository.ObterTodos();
        }

        [Test]
        public void ObterPorId()
        {
					Pedido pedido = Repository.ObterPorId(5468297);
        }
    }
}