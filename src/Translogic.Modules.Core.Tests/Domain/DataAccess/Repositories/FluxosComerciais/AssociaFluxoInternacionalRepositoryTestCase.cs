namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	[TestFixture]
	public class AssociaFluxoInternacionalRepositoryTestCase : RepositoryTestCase<AssociaFluxoInternacionalRepository, AssociaFluxoInternacional, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			AssociaFluxoInternacional afi = Repository.ObterPorId(146404);
		}
	}
}