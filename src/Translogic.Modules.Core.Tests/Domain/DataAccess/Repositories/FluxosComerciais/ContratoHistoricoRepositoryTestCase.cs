namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.Diversos;
	using Core.Domain.Model.Estrutura;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	public class ContratoHistoricoRepositoryTestCase : RepositoryTestCase<ContratoHistoricoRepository, ContratoHistorico, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			ContratoHistorico contratoHistorico = Repository.ObterPorId(113098);
			string siglaFerrovia = ALL.Core.Util.Enum<FerroviaFaturamentoEnum>.GetDescriptionOf(contratoHistorico.FerroviaFaturamento.Value);
		}

		[Test]
		public void ObterHistoricoPorContrato()
		{
			Contrato contrato = new Contrato {Id = 215134};
			IList<ContratoHistorico> lista = Repository.ObterPorContrato(contrato);
		}

		[Test]
		public void ObterDadosContratoPorId()
		{
			ContratoHistorico historico = Repository.ObterPorId(10277);
			IEmpresa ep = historico.EmpresaCobranca;
			string unidade = historico.UnidadeMonetaria;
		}

	}
}