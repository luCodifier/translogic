namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System;
    using System.Linq;
    using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	[TestFixture]
    public class ControleCpbrRepositoryTestCase : RepositoryTestCase<ControleCpbrRepository, ControleCpbr, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterControleCpbr()
		{
            var controleCprb = Repository.ObterTodos().First(c => c.CnpjFerrovia == "24962466000136");
		    double percCprb = 0;

            if (controleCprb != null)
            {
                if (controleCprb.FlagAtivo.Equals("S") && DateTime.Now > controleCprb.InicioVigencia && percCprb != Convert.ToDouble(controleCprb.PercentualTributo))
                {
                    percCprb = Convert.ToDouble(controleCprb.PercentualTributo);
                }
            }

            var afi2 = Repository.ObterPorId(1);
		}
	}
}