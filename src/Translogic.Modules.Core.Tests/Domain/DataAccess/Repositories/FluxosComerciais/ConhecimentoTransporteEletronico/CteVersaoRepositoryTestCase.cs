namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteVersaoRepositoryTestCase : RepositoryTestCase<CteVersaoRepository, CteVersao, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<CteVersao> versoes = Repository.ObterTodos();
		}
	}
}