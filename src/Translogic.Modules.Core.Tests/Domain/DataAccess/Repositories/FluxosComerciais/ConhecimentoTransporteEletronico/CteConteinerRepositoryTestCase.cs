﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System.Collections.Generic;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using NUnit.Framework;

    [TestFixture]
    public class CteConteinerRepositoryTestCase : RepositoryTestCase<CteConteinerRepository, CteConteiner, int>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorCteTestCase()
        {
            var teste = Repository.ObterPorCte(2911448);
        }

        [Test]
        public void CancelarCarregPorChaveCteTestCase()
        {
            Repository.CancelarCarregPorChaveCte("41170301258944000550570020011073691933258658");
        }

    }
}
