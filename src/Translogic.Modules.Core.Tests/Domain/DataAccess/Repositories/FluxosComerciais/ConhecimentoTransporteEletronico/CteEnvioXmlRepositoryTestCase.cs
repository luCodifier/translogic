namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using System.Linq;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteEnvioXmlRepositoryTestCase : RepositoryTestCase<CteEnvioXmlRepository, CteEnvioXml, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = false;
		}

		[Test]
		public void TestarInsercao()
		{
			Cte c = new Cte { Id = 183229 };
			CteEnvioXml envio = new CteEnvioXml();
			envio.Cte = c;
			envio.EnvioPara = "marcelosr@all-logistica.com";
			envio.TipoEnvioCte = TipoEnvioCteEnum.Email;
			Repository.Inserir(envio);
		}

		[Test]
		public void ObterPorCte()
		{
			string email = "luciane.souza@capgemini.COM";
			Cte cte = new Cte { Id = 198206 };
			IList<CteEnvioXml> lista = Repository.ObterPorCte(cte);
			CteEnvioXml cteEnvioXml = lista.Where(g => g.EnvioPara.ToUpper() == email.ToUpper()).FirstOrDefault();

			int i = lista.Count;
		}

		[Test]
		public void RemoverPorCte()
		{
			Cte cte = new Cte {Id = 183229};
			Repository.RemoverPorCte(cte);
		}
	}
}