namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteArquivoFtpRepositoryTestCase : RepositoryTestCase<CteArquivoFtpRepository, CteArquivoFtp, string>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			Repository.AtualizarQuandoAcharCte();
		}

		[Test]
		public void TesteInsercao()
		{
			string nomeArquivo = "51249624660001361108570010000000021905272623_ARQUIVO.TXT";
			string chaveCte = nomeArquivo.Substring(0, 44);
			CteArquivoFtp cteArquivoFtp = new CteArquivoFtp();
			cteArquivoFtp.Id = nomeArquivo;
			cteArquivoFtp.DataCadastro = DateTime.Now;
			cteArquivoFtp.ChaveCte = chaveCte;

		}
	}
}