namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteEnvioPoolingRepositoryTestCase : RepositoryTestCase<CteEnvioPoolingRepository, CteEnvioPooling, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsercao()
		{
			CteEnvioPooling cep = new CteEnvioPooling { Id = 1, DataHora = DateTime.Now, HostName = Dns.GetHostName() };
			Repository.Inserir(cep);
		}
	}
}