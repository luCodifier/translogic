namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteInterfaceRecebimentoConfigRepositoryTestCase : RepositoryTestCase<CteInterfaceRecebimentoConfigRepository, CteInterfaceRecebimentoConfig, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsert()
		{
			Cte cte = new Cte { Id = 169 };
			CteInterfaceRecebimentoConfig interfaceConfig = new CteInterfaceRecebimentoConfig();
			interfaceConfig.Cte = cte;
			interfaceConfig.CfgUn = 4;
			interfaceConfig.CfgDoc = 50;
			interfaceConfig.CfgSerie = "001";
			interfaceConfig.DataHora = DateTime.Now;
			interfaceConfig.DataUltimaLeitura = DateTime.Now;
			Repository.Inserir(interfaceConfig);
		}

		[Test]
		public void ObterTodos()
		{
			IList<CteInterfaceRecebimentoConfig> lista = Repository.ObterTodos();
		}

		[Test]
		public void ObterNaoProcessados()
		{
			IList<CteInterfaceRecebimentoConfig> listaNaoProcessado = Repository.ObterNaoProcessados(2);
		}
	}
}