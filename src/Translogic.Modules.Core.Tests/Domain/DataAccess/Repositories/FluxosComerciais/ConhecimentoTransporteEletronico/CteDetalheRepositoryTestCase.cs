namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	public class CteDetalheRepositoryTestCase : RepositoryTestCase<CteDetalheRepository, CteDetalhe, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarPorCte()
		{
            Cte c = new Cte { Id = 5280987 };
			IList<CteDetalhe> lista = Repository.ObterPorCte(c);
		}

        [Test]
        public void MalhaEmitente()
        {
            var result = Repository.GetMalhaByEstacao("LIC");
        }

    }
}