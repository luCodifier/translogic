namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteStatusRepositoryTestCase : RepositoryTestCase<CteStatusRepository, CteStatus, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsercao()
		{
			Cte cte = new Cte {Id = 1};
			// Insere o status do cte
			Repository.InserirCteStatus(cte, new Usuario { Id = 29953 }, new CteStatusRetorno { Id = 21 });
		}

		[Test]
		public void ObterStatusPorCte()
		{
			Cte cte = new Cte { Id = 142627 };
			IList<CteStatus> lista = Repository.ObterPorCte(cte);
			int count = lista.Where(g => g.CteStatusRetorno.Id == 2012).Count();
		}
	}
}