namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteInterfacePdfConfigRepositoryTestCase : RepositoryTestCase<CteInterfacePdfConfigRepository, CteInterfacePdfConfig, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsert()
		{
			Cte cte = new Cte {Id = 244};
			CteInterfacePdfConfig interf = new CteInterfacePdfConfig();
			interf.Cte = cte;
			interf.Chave = "TESTE";
			interf.DataHora = DateTime.Now;
			Repository.Inserir(interf);
		}

		[Test]
		public void ObterPorHost()
		{
			// string hostName = Dns.GetHostName();
			string hostName = "SIS-FWHS02";
			IList<CteInterfacePdfConfig> lista = Repository.ObterListaInterfacePorHost(hostName);
			int x = lista.Count;
		}
	}
}