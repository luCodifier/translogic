namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteRunnerLogRepositoryTestCase : RepositoryTestCase<CteRunnerLogRepository, CteRunnerLog, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void InserirLog()
		{
			Repository.InserirLogInfo(Dns.GetHostName(), "CteRunnerLogRepositoryTestCase", "Dados de teste de inser��o de informa��o");
			Repository.InserirLogErro(Dns.GetHostName(), "CteRunnerLogRepositoryTestCase", "Dados de teste de inser��o de Erro");
			
			CteRunnerLog runnerLog = new CteRunnerLog();
			runnerLog.Descricao = "INSERINDO UM ERRO";
			runnerLog.TipoLog = TipoLogCteEnum.Erro;
			runnerLog.HostName = Dns.GetHostName();
			runnerLog.NomeServico = "TesteMarcelo";
			runnerLog.DataHora = DateTime.Now;
			Repository.Inserir(runnerLog);
		}
	}
}