namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.IO;
	using System.Xml;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;
	using Util;

	[TestFixture]
	public class CteArquivoRepositoryTestCase : RepositoryTestCase<CteArquivoRepository, CteArquivo, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = false;
		}

		[Test]
		public void TesteInserirDados()
		{ 
			string fileName = @"C:\Temp\CodigoBACEN.pdf";
			CteArquivo arquivo = new CteArquivo();
			
			byte[] binaryFile = Tools.ConvertBinaryFileToByteArray(fileName);
			arquivo.Id = 1;
			arquivo.ArquivoPdf = new byte[binaryFile.Length];
			binaryFile.CopyTo(arquivo.ArquivoPdf, 0);
			arquivo.TamanhoPdf = binaryFile.Length;
			arquivo.DataHora = DateTime.Now;

			Repository.Inserir(arquivo);
		}

		[Test]
		public void TestarObterArquivo()
		{
			string fileName = "C:\\Temp\\41120301258944000550570180000484531903831044.xml";
			CteArquivo arquivo = Repository.ObterPorId(276);
			arquivo.ArquivoXml = Tools.ConvertFileToString(fileName);
			arquivo.TamanhoXml = arquivo.ArquivoXml.Length;
			arquivo.DataHoraXml = DateTime.Now;
			Repository.Atualizar(arquivo);
		}

		[Test]
		public void TestarArquivoXml()
		{
			Cte cte = new Cte {Id = 307};
			string nomeArquivo = @"C:\Temp\TesteMarcelo.xml";
			CteArquivo cteArquivo = Repository.InserirOuAtualizarXml(cte, nomeArquivo);
		}

		[Test]
		public void TestarNoFileSystem()
		{
			// CteArquivo arquivo = Repository.ObterPorId(199475);
			Cte c = new Cte {Id = 199475};
			string fileName = @"C:\Temp\51120324962466000136570020000006871905516276.pdf";
			string fileNameXml = @"C:\Temp\51120324962466000136570020000006871905516276.xml";
			Repository.InserirOuAtualizarPdf(c, fileName);
			Repository.InserirOuAtualizarXml(c, fileNameXml);

			// Tools.SaveByteArrayToFile(fileName, arquivo.ArquivoPdf);
			// Tools.SaveStringToFile(fileNameXml, arquivo.ArquivoXml);
		}

		[Test]
		public void TestarGravacaoArquivo()
		{
			CteArquivo cteArquivo = Repository.ObterPorId(246988);
			string arquivo = @"C:\Temp\TiaNena.pdf";
			Tools.SaveByteArrayToFile(arquivo, cteArquivo.ArquivoPdf);
		}
	}
}