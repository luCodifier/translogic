namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteAgrupamentoRepositoryTestCase : RepositoryTestCase<CteAgrupamentoRepository, CteAgrupamento, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			Cte ctePai = new Cte {Id = 1};
			Cte cteFilho = new Cte {Id = 53};
			// IList<CteAgrupamento> listaTodos = Repository.ObterAgrupamentoPorCtePai(ctePai);
			IList<CteAgrupamento> lista = Repository.ObterAgrupamentoPorCteFilho(cteFilho);
		}
	}
}