namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.IO;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
	using NUnit.Framework;
	using Translogic.Core.Infrastructure.FileSystem.ExtensionMethods;
	using Util;

	[TestFixture]
	public class CteComunicadoRepositoryTestCase : RepositoryTestCase<CteComunicadoRepository, CteComunicado, int?>
	{
		public override void Setup()
		{
			base.Setup();
			RealizarRollback = false;
		}

		[Test]
		public void TesteInserirDados()
		{
			string fileName = @"C:\Temp\ModeloComunicado.pdf";
			CteComunicado comunicado = new CteComunicado();

			byte[] binaryFile = Tools.ConvertBinaryFileToByteArray(fileName);
			comunicado.SiglaUf = "PR";
			comunicado.DataCadastro = DateTime.Now;
			comunicado.ComunicadoPdf = new byte[binaryFile.Length];
			binaryFile.CopyTo(comunicado.ComunicadoPdf, 0);
			comunicado.Ativo = true;
			Repository.Inserir(comunicado);
		}

		[Test]
		public void ObterPorSiglaUf()
		{
			CteComunicado comunicado = Repository.ObterPorSiglaUf("PR");
		}

		[Test]
		public void InserirComunicados()
		{
			InserirDadosComunicado("PR");
			InserirDadosComunicado("SC");
			InserirDadosComunicado("RS");
			InserirDadosComunicado("MT");
			InserirDadosComunicado("MS");
			InserirDadosComunicado("SP");
		}


		private void InserirDadosComunicado(string siglaUf)
		{
			string fileName = @"C:\Work\CT-e\Comunicado aos Clientes\Comunicado Geral aos Clientes.pdf";
			CteComunicado comunicado = new CteComunicado();

			byte[] binaryFile = Tools.ConvertBinaryFileToByteArray(fileName);
			comunicado.SiglaUf = siglaUf.ToUpper();
			comunicado.DataCadastro = DateTime.Now;
			comunicado.ComunicadoPdf = new byte[binaryFile.Length];
			binaryFile.CopyTo(comunicado.ComunicadoPdf, 0);
			comunicado.Ativo = true;
			Repository.Inserir(comunicado);
		}
	}
}