namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteComplementadoRepositoryTestCase : RepositoryTestCase<CteComplementadoRepository, CteComplementado, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorCteComplementar()
		{
			Cte cteComplementar = new Cte { Id = 199475 };
			IList<CteComplementado> lista = Repository.ObterCteComplementadoPorCteComplementar(cteComplementar);
			int i = lista.Count;
		}
	}
}