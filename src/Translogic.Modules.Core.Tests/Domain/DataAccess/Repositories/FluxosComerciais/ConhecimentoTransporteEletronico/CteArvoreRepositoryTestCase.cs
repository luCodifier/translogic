namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteArvoreRepositoryTestCase : RepositoryTestCase<CteArvoreRepository, CteArvore, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorCteRaiz()
		{
			Cte cteRaiz = new Cte {Id = 51};
			IList<CteArvore> lista = Repository.ObterPorCteRaiz(cteRaiz);
		}

		[Test]
		public void ObterPorFilho()
		{
			Cte cteFilho = new Cte { Id = 166 };
			IList<CteArvore> lista = Repository.ObterArvorePorCteFilho(cteFilho);
			CteArvore arvore = lista[0];
		}

		[Test]
		public void ObterArvorePorCteFilho()
		{
			Cte cteFilho = new Cte {Id = 163};
			CteArvore arvore = Repository.ObterCteArvorePorCteFilho(cteFilho);
		}
	}
}