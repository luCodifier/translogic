namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	public class CteInterfaceXmlConfigRepositoryTestCase : RepositoryTestCase<CteInterfaceXmlConfigRepository, CteInterfaceXmlConfig, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = false;
		}

		[Test]
		public void TestarInsert()
		{
			Cte cte = new Cte { Id = 276 };
			CteInterfaceXmlConfig interf = new CteInterfaceXmlConfig();
			interf.Cte = cte;
			interf.Chave = "10101010101010101010101010101010101010101010";
			interf.HostName = "NBTICWB49";
			interf.DataHora = DateTime.Now;
			interf.DataUltimaLeitura = DateTime.Now;
			Repository.Inserir(interf);
		}

		[Test]
		public void ObterPorHost()
		{
			string hostName = Dns.GetHostName();
			IList<CteInterfaceXmlConfig> lista = Repository.ObterListaInterfacePorHost(hostName);
			int x = lista.Count;
		}
		
	}
}