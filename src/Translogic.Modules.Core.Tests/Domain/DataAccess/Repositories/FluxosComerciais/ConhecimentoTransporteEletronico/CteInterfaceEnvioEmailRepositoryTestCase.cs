namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteInterfaceEnvioEmailRepositoryTestCase : RepositoryTestCase<CteInterfaceEnvioEmailRepository, CteInterfaceEnvioEmail, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsert()
		{
		}

		[Test]
		public void ObterPorHost()
		{
			string hostName = Dns.GetHostName();
			IList<CteInterfaceEnvioEmail> lista = Repository.ObterListaInterfacePorHost(hostName);
			int x = lista.Count;
		}
	}
}