namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteEnvioEmailHistoricoRepositoryTestCase : RepositoryTestCase<CteEnvioEmailHistoricoRepository, CteEnvioEmailHistorico, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = false;
		}
		
		[Test]
		public void TestarObterArquivo()
		{
			CteEnvioEmailHistorico envio = Repository.ObterPorId(1);
			string str = envio.EmailPara;
		}

		[Test]
		public void TestarInsertArquivo()
		{
			CteEnvioEmailHistorico historico = new CteEnvioEmailHistorico();
			historico.Cte = new Cte {Id = 277};
			historico.DataHora = DateTime.Now;
			historico.EmailPara = "marcelosr@all-logistica.com";
			historico.EmailCopia = "marcelosr@all-logistica.com";
			historico.EmailCopiaOculta = "marcelosr@all-logistica.com";
			historico.TipoArquivoEnvio = TipoArquivoEnvioEnum.PdfXml;
			Repository.Inserir(historico);
		}
	}
}