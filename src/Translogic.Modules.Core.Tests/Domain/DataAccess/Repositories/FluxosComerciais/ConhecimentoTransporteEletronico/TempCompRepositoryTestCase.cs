namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class TempCompRepositoryTestCase : RepositoryTestCase<TempCompRepository, TempComp, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestInserir()
		{
			TempComp comp = new TempComp {IdPrincipal = 10, IdSecundario = 20, Dados = "Teste 2"};
			Repository.Inserir(comp);
		}

		[Test]
		public void TesteDados()
		{
			int idPrincipal = 1;
			IList<TempComp> lista = Repository.ObterPorPrimaria(idPrincipal);
		}
	}
}