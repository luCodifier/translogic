namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteLogRepositoryTestCase : RepositoryTestCase<CteLogRepository, CteLog, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void InserirLog()
		{
			Cte cte = new Cte { Id = 218673 };
			Repository.InserirLogInfo(cte, "Dados de teste de inser��o de informa��o");
			Repository.InserirLogErro(cte, "Dados de teste de inser��o de Erro");
		}

		[Test]
		public void InserirLogTeste()
		{
			CteLog cteLog = new CteLog();
			cteLog.Cte = null;
			cteLog.Descricao = "Teste dos Logs vers�o II";
			cteLog.NomeServico = "TiaNena";
			cteLog.TipoLog = TipoLogCteEnum.Erro;
			cteLog.DataHora = DateTime.Now;
			Repository.Inserir(cteLog);

		}
	}
}