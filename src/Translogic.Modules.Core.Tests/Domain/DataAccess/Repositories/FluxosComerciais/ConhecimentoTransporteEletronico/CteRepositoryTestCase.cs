namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using ALL.Core.Dominio;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.Diversos.Cte;
    using Core.Domain.Model.Dto;
    using Core.Domain.Model.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
    using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
    using Core.Domain.Model.Trem.Veiculo.Vagao;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;

    [TestFixture]
    public class CteRepositoryTestCase : RepositoryTestCase<CteRepository, Cte, int?>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorId()
        {
            Cte cte = Repository.ObterPorId(246456);
            IList<CteDetalhe> listaDetalhe = cte.ListaDetalhes.ToList();
            CteStatus status = cte.StatusVigente;
        }

        [Test]
        public void ObterPorIdEdi()
        {
            Cte cte = Repository.ObterPorIdEdi(246456);
            Console.Write(cte);
        }

        [Test]
        public void Obter()
        {
            // 43130701258944003818570020001056791963675423
            //  string[] chave = new string[] { "43130701258944003818570020001056791963675423" };
            // var listaCte = Repository.ObterCteVirtual(new DetalhesPaginacao(), new DateTime(2013, 09, 01, 0, 0, 0), new DateTime(2013, 09, 30, 23, 59, 59), "", 0, "", "", "", "", "", "", "");
            var listaCte = Repository.ObterCteAnulacao(new DetalhesPaginacao(), new DateTime(2014, 10, 10, 0, 0, 0), new DateTime(2014, 11, 05, 23, 59, 59), "", 0, "", null, "", "", "", 168);
            //  var listaCte = Repository.ObterCteAnulacao(new DetalhesPaginacao(), new DateTime(2014, 04, 01, 0, 0, 0), new DateTime(2014, 04, 10, 23, 59, 59), "", 0, "", "", "", "", "", "", "");
            // var lista = Repository.ObterCteCancelamento(new DetalhesPaginacao(), new DateTime(2013, 08, 21, 0, 0, 0), new DateTime(2013, 08, 21, 23, 59, 59), "", 0, "32944", "", "", "", "", "", 0, "");
        }


        [Test]
        public void RetornaListaCteVirtual()
        {
            var listaCte = Repository.ObterCteVirtual(new DetalhesPaginacao(), new DateTime(2020, 04, 13, 0, 0, 0), new DateTime(2020, 04, 13, 23, 59, 59), "", 0, "", "", "", "", "", "", "");
            var teste = listaCte;
        }

        [Test]
        public void ObterCte()
        {
            Repository.SalvarEmailCteReenvioArquivo("X", "marcelosr@all-logistica.com", "42933");
        }

        [Test]
        public void ObterCtePorFluxo()
        {
            IList<Cte> listaCte = Repository.Obter(new DateTime(2011, 05, 01, 0, 0, 0), new DateTime(2011, 07, 22, 23, 59, 59), 573, 63992, "35050976234556576787687777889089082342343239", "ZBV", 9090, "SP83093", true);
        }

        [Test]
        public void ObterParaRefaturamento()
        {
            IList<RefaturamentoDto> ret = Repository.ObterParaRefaturamento(string.Empty, "X96");
        }

        [Test]
        public void ObterPorHostName()
        {
            // IList<Cte> lista = Repository.ObterListaEnvioPorHost(Dns.GetHostName());
            IList<Cte> lista = Repository.ObterListaEnvioPorHost("TL-FWPS01");
            Cte cteAux = lista[0];
            CteVersao versao = cteAux.Versao;
            DespachoTranslogic despachoTranslogic = cteAux.Despacho;
            FluxoComercial fluxo = cteAux.FluxoComercial;
            Vagao vagao = cteAux.Vagao;
        }

        [Test]
        public void ObterPorStatusProcessamento()
        {
            IList<Cte> lista = Repository.ObterStatusProcessamentoEnvio(1);

            lista = Repository.ObterListaEnvioComplementarPorHost("ALL-RUNPW8AP101");
        }

        [Test]
        public void ObterListaEnvioPooling()
        {
            // string hostName = "VERIFICAR";
            // string hostName = "TL-FWPS01";
            string hostName = Dns.GetHostName();
            IList<Cte> lista = Repository.ObterListaEnvioPorHost(hostName);
            Cte cte = lista[0];
        }

        [Test]
        public void TestarAtualizaFlagGeracaoArquivo()
        {
            Cte cte = Repository.ObterPorId(272213);
            Repository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Pdf);
            Repository.AtualizarArquivoGeradoCte(cte, TipoArquivoEnvioEnum.Xml);
        }

        [Test]
        public void TestarObterCtePorDespacho()
        {
            DespachoTranslogic despacho = new DespachoTranslogic { Id = 16076554 };
            Cte cte1 = Repository.ObterPorDespacho(despacho);
            Cte cte2 = Repository.ObterPorDespacho(despacho, 336767);
        }

        [Test]
        public void TestarObterCtesFerroviariosDescarregadosPorData()
        {
            var lista = Repository.ObterCtesDescarregaosPorPeriodoSiglaFerrovia(DateTime.Now.AddHours(-3), DateTime.Now, "MT");
        }

        [Test]
        public void ExistemCtesRateioVagaoVigenteTestCase()
        {
            var lista = Repository.ExistemCtesRateioVagaoVigente("51170224962466000136570020006377261903013611");
        }

        [Test]
        public void ObterCtesPorVagaoPedidoVigenteTestCase()
        {
            var lista = Repository.ObterCtesPorVagaoPedidoVigente(1);
        }

        [Test]
        public void ObterCtesPorVagaoPedidoVigenteConteinerTestCase()
        {
            try
            {
                var lista = Repository.ObterCtesPorVagaoPedidoVigenteConteiner("51170224962466000136570020006377271908016071", "MRKU2989055");
            }
            catch (Exception ex)
            {
                var msg = ex.Message + ex.InnerException.Message;
                throw;
            }
        }
    }
}
