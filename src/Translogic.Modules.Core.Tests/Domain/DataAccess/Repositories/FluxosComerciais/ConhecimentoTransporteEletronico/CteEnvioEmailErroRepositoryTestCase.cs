namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.Diversos.Cte;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteEnvioEmailErroRepositoryTestCase : RepositoryTestCase<CteEnvioEmailErroRepository, CteEnvioEmailErro, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = false;
		}

		[Test]
		public void TestarInsert()
		{
			Cte cte = new Cte {Id = 196864};
			CteEnvioEmailErro cteEnvioEmailErro = new CteEnvioEmailErro();
			cteEnvioEmailErro.Cte = cte;
			cteEnvioEmailErro.DescricaoErro = "Email invalido";
			cteEnvioEmailErro.EmailPara = "marcelosr@all-logistica.com";
			cteEnvioEmailErro.TipoArquivoEnvio = TipoArquivoEnvioEnum.PdfXml;
			cteEnvioEmailErro.HostName = Dns.GetHostName();
			cteEnvioEmailErro.DataHora = DateTime.Now;
			Repository.Inserir(cteEnvioEmailErro);
		}
	}
}