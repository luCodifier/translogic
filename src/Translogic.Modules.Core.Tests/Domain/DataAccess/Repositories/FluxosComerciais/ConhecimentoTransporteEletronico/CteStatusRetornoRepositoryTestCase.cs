namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteStatusRetornoRepositoryTestCase : RepositoryTestCase<CteStatusRetornoRepository, CteStatusRetorno, int?>
	{
		public override void TestSetup()
		{
		    // this.RealizarRollback = false;
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<CteStatusRetorno> ListaStatus = Repository.ObterTodos();
		}

		[Test]
		public void TestarInsert()
		{
			CteStatusRetorno retorno = new CteStatusRetorno
			                           	{
			                           		Descricao = "Falha no Paser XML",
			                           		Id = 4002,
			                           		InformadoPelaReceita = false,
			                           		PermiteReenvio = false
			};

			Repository.Inserir(retorno);
		}
	}
}
