namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico
{
	using System;
	using System.Collections.Generic;
	using System.Net;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using NUnit.Framework;

	[TestFixture]
	public class CteRecebimentoPoolingRepositoryTestCase : RepositoryTestCase<CteRecebimentoPoolingRepository, CteRecebimentoPooling, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsercao()
		{
			CteRecebimentoPooling crp = new CteRecebimentoPooling { Id = 1, DataHora = DateTime.Now, HostName = Dns.GetHostName() };
			Repository.Inserir(crp);
		}

		[Test]
		public void TestarRecebimentoPorHost()
		{
			string hostName = Dns.GetHostName();
			IList<CteRecebimentoPooling> pooling = Repository.ObterListaRecebimentoPorHost(hostName);
		}
	}
}