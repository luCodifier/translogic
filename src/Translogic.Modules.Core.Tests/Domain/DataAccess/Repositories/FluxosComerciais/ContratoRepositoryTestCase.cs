namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais;
	using Core.Domain.Model.FluxosComerciais;
	using NUnit.Framework;

	[TestFixture]
	public class ContratoRepositoryTestCase : RepositoryTestCase<ContratoRepository, Contrato, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			Contrato contrato = Repository.ObterPorId(66215);
		}
	}
}