﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais;
    using Core.Domain.Model.FluxosComerciais;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class FluxoComercialRepositoryTestCase : RepositoryTestCase<FluxoComercialRepository, FluxoComercial, int>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorId()
        {
            FluxoComercial fluxoComercial = Repository.ObterPorId(102438);
        }

        [Test]
        public void ObterListaFluxoComercial()
        {
            IList<FluxoComercial> lst = new List<FluxoComercial>();
            int[] flxs = {
                             40451,
                             102438,
                             40453,
                             99139,
                             99416,
                         };
            foreach (var i in flxs)
            {
                lst.Add(Repository.ObterPorId(i));
            }
        }
    }
}
