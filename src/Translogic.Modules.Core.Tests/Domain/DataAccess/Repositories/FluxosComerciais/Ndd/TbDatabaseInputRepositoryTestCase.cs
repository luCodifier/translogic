namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Ndd
{
	using System;
	using System.Text;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Ndd;
	using Core.Domain.Model.FluxosComerciais.Ndd;
	using NUnit.Framework;

	[TestFixture]
	public class TbDatabaseInputRepositoryTestCase : RepositoryTestCase<TbDatabaseInputRepository, TbDatabaseInput, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorIdentificador()
		{
			TbDatabaseInput dados = Repository.ObterPorId(537997);
		}

		[Test]
		public void TestInsercao()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("01");
				
			UTF8Encoding utf8 = new UTF8Encoding();

			// ASCIIEncoding ASCII  = new ASCIIEncoding();
			Byte[] bytesMessage = utf8.GetBytes(builder.ToString());

			TbDatabaseInput dados = new TbDatabaseInput();
			dados.DataHora = DateTime.Now;
			dados.DocumentUser = "MARCELO";
			dados.Job = "TESTEJOB";
			dados.Kind = 0;
			dados.Status = 1;
			dados.NumeroDocumento = "1111";
			dados.DocumentData = new byte[bytesMessage.Length];
			bytesMessage.CopyTo(dados.DocumentData, 0);

			Repository.Inserir(dados);
		}
	}
}