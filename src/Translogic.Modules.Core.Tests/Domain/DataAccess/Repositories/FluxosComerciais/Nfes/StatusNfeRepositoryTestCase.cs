namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes;
	using Core.Domain.Model.FluxosComerciais.Nfes;
	using NUnit.Framework;

	[TestFixture]
	public class StatusNfeRepositoryTestCase : RepositoryTestCase<StatusNfeRepository, StatusNfe, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorChave()
		{
			string chave = "43120887548020000341550110000179021007111365";
			StatusNfe status = Repository.ObterPorChaveNfe(chave);
		}
	}
}