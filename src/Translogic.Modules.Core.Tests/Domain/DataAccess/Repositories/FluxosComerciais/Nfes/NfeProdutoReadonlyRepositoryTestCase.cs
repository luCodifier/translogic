namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Nfes
{
    using System.Collections.Generic;
    using ALL.TestsSupport.AcessoDados;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Nfes;
    using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Nfes;
    using NUnit.Framework;

    [TestFixture]
	public class NfeProdutoReadonlyRepositoryTestCase : RepositoryTestCase<NfeProdutoReadonlyRepository, NfeProdutoReadonly, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<NfeProdutoReadonly> produtos = Repository.ObterTodos();
		}

        [Test]
        public void ObterPorTipoId()
        {
            var produtos = Repository.ObterPorTipoId(1,"teste");
            
        }
	}
}