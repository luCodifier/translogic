﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.Pedido
{
    using System;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos;
    using Core.Domain.Model.FluxosComerciais.Pedidos;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class VagaoPedidoRepositoryTestCase : RepositoryTestCase<VagaoPedidoRepository, VagaoPedido, int?>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorId()
        {
            VagaoPedido vagaoPedido = Repository.ObterPorId(15037442);
        }

        [Test]
        public void ObterListaVagaoPedido()
        {
            IList<VagaoPedido> lst = new List<VagaoPedido>();
            int[] flxs = {
                             15037442
                         };
            foreach (var i in flxs)
            {
                lst.Add(Repository.ObterPorId(i));
            }
        }

    }
}
