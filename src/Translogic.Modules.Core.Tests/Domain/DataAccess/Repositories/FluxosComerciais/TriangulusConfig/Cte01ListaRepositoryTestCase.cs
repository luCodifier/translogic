namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using NHibernate.SqlCommand;
	using NUnit.Framework;

	[TestFixture]
	public class Cte01ListaRepositoryTestCase : RepositoryTestCase<Cte01ListaRepository,Cte01Lista, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarRecebimento()
		{
			// Obs: Tem que mudar para o banco da config para funcionar o teste case
			// Estudar uma maneira de fazer automaticamente isso
			Cte01Lista rec = Repository.ObterPorId(262);
		}
	}
}