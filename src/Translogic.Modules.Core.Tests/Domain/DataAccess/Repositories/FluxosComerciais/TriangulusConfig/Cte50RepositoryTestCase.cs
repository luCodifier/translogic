namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using NUnit.Framework;

	[TestFixture]
	public class Cte50RepositoryTestCase : RepositoryTestCase<Cte50Repository, Cte50, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
			RealizarRollback = true;
		}

		[Test]
		public void Testar()
		{
			/* Como essa tabela � por conex�o pelo Alias tem que testar por Test */
			
			Cte50 cte50 = new Cte50();
			cte50.IdFilial = 3;
			cte50.Numero = 1;
			cte50.Serie = "2";
			cte50.IdImp = 0;
			cte50.LocalPrt = "LOCAL_PADRAO";
			cte50.IdentPrt = "TESTE";
			cte50.NrCop = 1;
			cte50.ImpFrenteVerso = 0;
			cte50.Copia = 1;

			Repository.Inserir(cte50);
		}
	}
}