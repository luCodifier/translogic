namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig.Repositories;
	using NUnit.Framework;
	using Translogic.Tests;

	public class Nfe20ErroLogRepositoryTestCase : BaseTranslogicContainerTestCase
	{
		private INfe20ErroLogRepository _nfe20ErroLogRepository;

		protected override void Setup()
		{
			base.Setup();
			_nfe20ErroLogRepository = Container.Resolve<INfe20ErroLogRepository>();
		}
		
		[Test]
		public void TestarObter()
		{
			var log = _nfe20ErroLogRepository.ObterTodos();
		}
	}
}