namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using NUnit.Framework;

	[TestFixture]
	public class Nfe03FilialRepositoryTestCase : RepositoryTestCase<Nfe03FilialRepository, Nfe03Filial, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarInsert()
		{
			long cnpj;
			string cnpjAux = "39115514000390";
			Int64.TryParse(cnpjAux, out cnpj);
			Repository.ObterFilialPorCnpj(cnpj);
		}
	}
}