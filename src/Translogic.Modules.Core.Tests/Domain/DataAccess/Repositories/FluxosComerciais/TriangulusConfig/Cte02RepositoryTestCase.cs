namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using NUnit.Framework;

	[TestFixture]
    public class Cte02RepositoryTestCase : RepositoryTestCase<Cte02Repository, Cte02, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
            Configuration.AddAssembly(typeof(Usuario).Assembly);
		}
	
		[Test]
		public void TestarInsert()
		{
			Repository.Alias = "CONFIG";
            Cte02 cte = new Cte02 { CfgDoc = 4, CfgUn = 21, CfgSerie = "001", IdPass = 0 , PassXpass = "a"};
			Repository.Inserir(cte);
		}
	}
}