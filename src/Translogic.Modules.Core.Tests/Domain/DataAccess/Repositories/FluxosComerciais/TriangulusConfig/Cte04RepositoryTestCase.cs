namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.TriangulusConfig;
	using Core.Domain.Model.FluxosComerciais.TriangulusConfig;
	using NUnit.Framework;

	[TestFixture]
	public class Cte04RepositoryTestCase : RepositoryTestCase<Cte04Repository, Cte04, int>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}
	
		[Test]
		public void TestarInsert()
		{
			Repository.Alias = "CONFIG";
			Cte04 cte = new Cte04 {CfgDoc = 1, CfgUn = 1, CfgSerie = "XXX", IdObsFisco = 2, ObsFiscoXcampo = "wwww", ObsFiscoXtexto = "zzzz"};
			Repository.Inserir(cte);
			// IList<Cte04> lista = Repository.ObterTeste();
		}
	}
}