namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.FluxosComerciais
{
    using System.Collections.Generic;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais;
    using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos;
    using Core.Domain.Model.FluxosComerciais.Pedidos;
    using Core.Domain.Model.FluxosComerciais;
    using NUnit.Framework;

    [TestFixture]
    public class MercadoriaRepositoryTestCase : RepositoryTestCase<MercadoriaRepository, Mercadoria, int>
    {
        public override void TestSetup()
        {
            base.TestSetup();
        }

        [Test]
        public void ObterPorCodigo()
        {
            Mercadoria mercadoria = Repository.ObterPorCodigo("828");
        }
    }
}