﻿
using System.Collections.Generic;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa;
using Translogic.Modules.Core.Domain.Model.Seguro;
using Translogic.Modules.Core.Domain.Model.Tfa;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Tfa
{
	[TestFixture]
	public class TerminalTfaEmailRepositoryTestCase : RepositoryTestCase<TerminalTfaEmailRepository, TerminalTfaEmail, int>
	{
		[Test]
		public void ObterTodosPorIdTerminal()
		{
		    var lista = Repository.ObterTodosPorIdTerminalDto(21);

            Assert.IsTrue(lista.Count > 1);
		}
	}
}