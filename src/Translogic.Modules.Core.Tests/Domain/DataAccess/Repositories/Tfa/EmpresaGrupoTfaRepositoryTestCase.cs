﻿
//using System.Collections.Generic;
//using ALL.TestsSupport.AcessoDados;
//using NUnit.Framework;
//using Translogic.Modules.Core.Domain.DataAccess.Repositories.Seguro;
//using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa;
//using Translogic.Modules.Core.Domain.Model.Seguro;
//using Translogic.Modules.Core.Domain.Model.Tfa;

using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Tfa;
using Translogic.Modules.Core.Domain.Model.Tfa;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Tfa
{
	[TestFixture]
    public class EmpresaGrupoTfaRepositoryTestCase : RepositoryTestCase<EmpresaGrupoTfaRepository, EmpresaGrupoTfa, int?>
	{
		[Test]
		public void ObterTodos()
		{
            var lista = Repository.ObterListaEmailTfa(160749);
		    Assert.IsTrue(lista.Count > 1);
		}
	}
}