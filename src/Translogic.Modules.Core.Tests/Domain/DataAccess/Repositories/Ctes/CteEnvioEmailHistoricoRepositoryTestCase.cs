﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.ConhecimentoTransporteEletronico;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Ctes
{
    [TestFixture]
    public class CteEnvioEmailHistoricoRepositoryTestCase : RepositoryTestCase<CteEnvioEmailHistoricoRepository, CteEnvioEmailHistorico, int?>
    {
        [Test]
        public void ObterPorTestCase()
        {
            var resultado = Repository.ObterPor(new List<int> { 239167, 239169, 268836, 268836 });

            Assert.IsTrue(resultado.Any());
        }
    }
}
