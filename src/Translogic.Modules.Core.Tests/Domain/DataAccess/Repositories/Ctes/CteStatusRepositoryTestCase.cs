﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.Services.Mdfes;
using Translogic.Tests;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico.Repositories;
using Translogic.Modules.Core.Domain.Model.Acesso.Repositories;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Ctes
{
    [TestFixture]
    public class CteStatusRepositoryTestCase
        : BaseTranslogicContainerTestCase
    {
        private ICteStatusRepository _repository;
        private ICteRepository _cteRepository;
        private IUsuarioRepository _usuarioRepository;


        #region SETUP/TEARDOWN

        protected override void Setup()
        {
            base.Setup();
            RealizarRollback = false;
            _repository = Container.Resolve<ICteStatusRepository>();
            _cteRepository = Container.Resolve<ICteRepository>();
            _usuarioRepository = Container.Resolve<IUsuarioRepository>();
        }

        #endregion

        [Test]
        public void SaveTest()
        {
            var cte = _cteRepository.ObterPorId(4622746);
            var usuario = _usuarioRepository.ObterPorId(597);
            _repository.InserirCteStatus(cte, usuario, new CteStatusRetorno { Id = 38 });
        }
    }
}
