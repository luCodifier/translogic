using System;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Edi
{
    using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
    using Core.Domain.Model.Estrutura;
	using NHibernate.Criterion;
	using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Edi;
    using Translogic.Modules.Core.Domain.Model.Edi;

	[TestFixture]
    public class EdiErroRepositoryTestCase : RepositoryTestCase<EdiErroRepository, EdiErro, int>
	{
		[Test]
        public void ObterMensagensRecebimentoErro()
        {
            var dataInicial = Convert.ToDateTime("21/01/2019");
            var dataFinal = Convert.ToDateTime("22/01/2019");
            Repository.ObterMensagensRecebimentoErro(dataInicial, dataFinal);
		}
	}
}