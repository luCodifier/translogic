namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem
{
	using System;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Trem;
	using NUnit.Framework;

	[TestFixture]
	public class ComposicaoRepositoryTestCase : RepositoryTestCase<ComposicaoRepository, Composicao, int?>
	{
		[SetUp]
		public override void Setup()
		{
			base.Setup();
		}

		[Test]
		public void ConsegueObterComposicao()
		{
			Composicao composicao = Repository.ObterPorId(949005);
			Assert.IsNotNull(composicao.ListaVagoes);
		}

		[Test]
		public void ObterDadosComposicao()
		{
			Composicao composicao = Repository.ObterDadosComposicao(2700507);
		}
	}
}