namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem
{
	using System;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Trem;
	using NUnit.Framework;

	public class TremRapidoRepositoryTestCase : CrudRepositoryTestCase<TremRapidoRepository, TremRapido, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();

			Configuration.AddAssembly(typeof(Usuario).Assembly);
		}

		protected override void PopularParaInsercao(TremRapido tremRapido)
		{
			tremRapido.Trechos = "LAP-LRF;LRF-LUS;LUS-LIC;LUS-LRO";

			tremRapido.Prefixos = "C??;D??";

			tremRapido.DataInicial = Convert.ToDateTime("05/02/2010");

			tremRapido.DataFinal = Convert.ToDateTime("10/02/2010");
		}

		protected override void PopularParaAtualizacao(TremRapido tremRapido)
		{
			tremRapido.Trechos = "LUS-LIC;LUS-LRO";

			tremRapido.Prefixos = "D??;E??";

			tremRapido.DataInicial = Convert.ToDateTime("05/02/2010");

			tremRapido.DataFinal = Convert.ToDateTime("10/02/2010");
		}

		protected override void VerificarIgualdade(TremRapido entidade, TremRapido recuperada)
		{
			//Assert.AreEqual(entidade.Id, recuperada.Id);
			Assert.AreEqual(entidade.Trechos, recuperada.Trechos);
			Assert.AreEqual(entidade.Prefixos, recuperada.Prefixos);
			Assert.AreEqual(entidade.DataInicial, recuperada.DataInicial);
			Assert.AreEqual(entidade.DataFinal, recuperada.DataFinal);
		}
	}
}