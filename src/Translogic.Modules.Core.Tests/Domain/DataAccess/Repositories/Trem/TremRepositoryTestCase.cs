﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem
{
    using System;
    using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem;
	using NUnit.Framework;

	[TestFixture]
	public class TremRepositoryTestCase : RepositoryTestCase<TremRepository, Core.Domain.Model.Trem.Trem, int?>
	{
	    #region SETUP/TEARDOWN

        [SetUp]
        public override void Setup()
        {
            base.Setup();
	        this.RealizarRollback = true;
        }

        #endregion

		[Test]
		public void ObterPorIdComFetch()
		{
            var trem = Repository.ObterPorIdComFetch(1325039);
            Console.Write(trem);
		}
	}
}