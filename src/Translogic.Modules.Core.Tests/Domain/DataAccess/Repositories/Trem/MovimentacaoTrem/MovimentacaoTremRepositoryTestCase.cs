﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem
{
	using System;

	using ALL.TestsSupport.AcessoDados;

	using NUnit.Framework;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.MovimentacaoTrem;
	using Translogic.Modules.Core.Domain.Model.Trem.MovimentacaoTrem.Repositories;

	[TestFixture]
	public class MovimentacaoTremRepositoryTestCase : RepositoryTestCase<MovimentacaoTremRepository, Core.Domain.Model.Trem.MovimentacaoTrem.MovimentacaoTrem, int?>
	{
	    #region SETUP/TEARDOWN

        [SetUp]
        public override void Setup()
        {
            base.Setup();
	        this.RealizarRollback = true;
        }

        #endregion

		[Test]
		public void ObterPorId()
		{
			var movimentacao = Repository.ObterPorId(4237);
		}
	}
}