namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.OrdemServico
{
	using System;
	using System.Collections.Generic;
	using ALL.Core.Dominio;
	using ALL.Core.Extensions;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem.OrdemServico;
	using Core.Domain.DataAccess.Repositories.Via.Circulacao;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Trem;
	using Core.Domain.Model.Trem.OrdemServico;
	using Core.Domain.Model.Trem.OrdemServico.Repositories;
	using Core.Domain.Model.Via;
	using Core.Domain.Model.Via.Repositories;
	using Interfaces.Trem.OrdemServico;
	using NUnit.Framework;

    [TestFixture]
	public class OrdemServicoRepositoryTestCase : RepositoryTestCase<OrdemServicoRepository, OrdemServico, int?>
	{
	    private IAreaOperacionalRepository areaOperacionalRepository;

	    #region SETUP/TEARDOWN

        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        #endregion

	    [Test]
		public void ObterTodos()
		{
			ResultadoPaginado<OrdemServico> ordensServico = Repository.ObterTodosPaginado(new DetalhesPaginacao {Limite = 10, });

			foreach (OrdemServico os in ordensServico.Items)
			{
				Console.WriteLine(os.DataCadastro);
				Console.WriteLine(os.DataFechamento);
				Console.WriteLine(os.DataSupressao);
				Console.WriteLine(os.Destino);
				Console.WriteLine(os.Numero);
				Console.WriteLine(os.Origem);
				Console.WriteLine(os.Prefixo);
				Console.WriteLine(os.QtdeVagoesCarregados);
				Console.WriteLine(os.QtdeVagoesVazios);
				Console.WriteLine(os.Rota);
				Console.WriteLine(os.Situacao);
				Console.WriteLine(os.UsuarioCadastro);
				Console.WriteLine(os.UsuarioFechamento);
				Console.WriteLine(os.UsuarioSupressao);
				Console.WriteLine("---------------------------------");
			}
		}

		[Test]
		public void ObterPorTipoSituacaoEDataPartidaPrevistaOficial()
		{
			DateTime inicial = DateTime.Now.AddDays(-30);
			DateTime final = DateTime.Now.AddDays(2).AddMilliseconds(-1);
			IList<OrdemServico> ordensPorSituacao = Repository.ObterPorTipoSituacaoEDataPartidaPrevistaOficial((int)SituacaoOrdemServicoEnum.EmPlanejamento, "L10", "LIC", string.Empty, inicial, final);

			foreach (OrdemServico servico in ordensPorSituacao)
			{
			    try
			    {
                    OrdemServicoDto dto = servico.Map<OrdemServico, OrdemServicoDto>();
			    }
			    catch (Exception e)
			    {
			        throw e;
			    }

				// Assert.AreEqual(SituacaoOrdemServicoEnum.EmPlanejamento, servico.Situacao.Id);
			}
		}

        [Test]
        public void ObterPlanejamentoSerieLocomotivas()
        {
            OrdemServico ordemServico = Repository.ObterPorId(1394462);

            foreach (PlanejamentoSerieLocomotiva planejamento in ordemServico.PlanejamentoSerieLocomotivas)
            {
                Console.WriteLine("id: {0}", planejamento.Id);
                Console.WriteLine("local: {0}", planejamento.Local.Codigo);
                Console.WriteLine("id ordem servico: {0}", planejamento.OrdemServico.Id);
                Console.WriteLine("qtde anexadas: {0}", planejamento.QtdeAnexadas);
                Console.WriteLine("qtde desanexadas: {0}", planejamento.QtdeDesanexadas);
                Console.WriteLine("codigo da serie: {0}", planejamento.Serie.Codigo);
                Console.WriteLine("usuario: {0}", planejamento.UsuarioCadastro.Codigo);
                Console.WriteLine("----------------------------------");
            }
        }
	}
}