namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem
{
	using System;
	using ALL.Core.Dominio;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem;
	using Core.Domain.Model.Acesso;
	using Core.Domain.Model.Trem;
	using NUnit.Framework;

	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Locomotiva;

	[TestFixture]
	public class SituacaoPosicaoLocomotivaRepositoryTestCase : RepositoryTestCase<SituacaoPosicaoLocomotivaRepository, SituacaoPosicaoLocomotiva, int?>
	{
		[SetUp]
		public override void Setup()
		{
			base.Setup();
		}

		[Test]
		public void ConsegueObterPorComposicao()
		{
			Repository.ObterPorComposicaoLocomotiva(new ComposicaoLocomotiva { Id = 4646561 }, new DateTime(2010, 10, 08, 16, 55, 00));
		}
	}
}