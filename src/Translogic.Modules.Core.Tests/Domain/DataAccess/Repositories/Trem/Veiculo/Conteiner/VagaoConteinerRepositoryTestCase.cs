﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner
{
    using System.Collections.Generic;
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.Model.Trem.Veiculo.Conteiner;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Conteiner;

    [TestFixture]
    public class VagaoConteinerRepositoryTestCase : RepositoryTestCase<VagaoConteinerRepository, VagaoConteiner, int?>
    {

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            // _Configuration.AddAssembly(typeof(Usuario).Assembly);
        }

        [Test]
        public void ObterPorCodigo()
        {
            VagaoConteiner vagaoconteiner = Repository.ObterPorCodigo("8440140");
        }

        [Test]
        public void ObterTodos()
        {
            IList<VagaoConteiner> lista = Repository.ObterPorVagao(312915);
        }
    }
}
