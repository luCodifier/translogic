﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Linq;
    using ALL.TestsSupport.AcessoDados;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    [TestFixture]
    public class VagaoTaraEdiRepositoryTestCase : RepositoryTestCase<VagaoTaraEdiDescargaRepository, VagaoTaraEdiDescarga, int?>
    {

        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        [Test]
        public void ObterVagaoCarregamento()
        {
            try
            {
                var registros = Repository.ObterTodos().Where(x => x.CodVagao == "0527041");
                var soma = registros.Average(x => x.Tara);
                Console.Write(soma);
            }
            catch(Exception e)
            {
                Console.Write(e);
            }
        }

        [Test]
        public void ObterIdVagoesComPesagens()
        {
            try
            {
                var registros = Repository.ObterVagoesComPesagens();
                Console.Write(registros);
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
    }
}
