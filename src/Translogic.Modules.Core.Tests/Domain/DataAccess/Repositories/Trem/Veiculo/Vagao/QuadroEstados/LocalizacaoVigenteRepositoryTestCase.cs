﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados
{
    using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao.QuadroEstados;
    using Core.Domain.Model.Trem.Veiculo.Vagao.QuadroEstado;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    [TestFixture]
    public class LocalizacaoVagaoVigenteRepositoryTestCase : RepositoryTestCase<LocalizacaoVagaoVigenteRepository, LocalizacaoVagaoVigente, int>
    {

        [SetUp]
        public override void Setup()
        {
            base.Setup();
           // _Configuration.AddAssembly(typeof(Usuario).Assembly);
        }

        [Test]
        public void ObterVagaoCarregamento()
        {
            var vagao = Repository.ObterPorCodigoVagao("053");
        }
    }
}
