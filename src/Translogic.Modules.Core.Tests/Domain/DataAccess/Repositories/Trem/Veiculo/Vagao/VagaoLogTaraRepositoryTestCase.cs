﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Linq;
    using ALL.TestsSupport.AcessoDados;
    using NUnit.Framework;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    [TestFixture]
    public class VagaoLogTaraRepositoryTestCase : RepositoryTestCase<VagaoLogTaraRepository, VagaoLogTara, int?>
    {

        [SetUp]
        public override void Setup()
        {
            base.Setup();
        }

        [Test]
        public void ObterLogTara()
        {
            try
            {
                Repository.InserirOuAtualizar(new VagaoLogTara
                                                  {
                                                      CodVagao = "6216374",
                                                      DataCadastro = DateTime.Now,
                                                      TaraNova = 25,
                                                      TaraAntiga = 24,
                                                      Usuario = "Rafael"
                                                  });

                var registros = Repository.ObterTodos();
                Console.Write(registros);
            }
            catch(Exception e)
            {
                Console.Write(e);
            }
        }
    }
}
