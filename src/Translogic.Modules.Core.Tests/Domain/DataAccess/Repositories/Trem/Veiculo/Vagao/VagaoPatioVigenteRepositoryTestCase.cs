﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ALL.TestsSupport.AcessoDados;
    using NUnit.Framework;
    using Translogic.Core.Infrastructure.Web;
    using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
    using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

    [TestFixture]
    public class VagaoPatioVigenteRepositoryTestCase : RepositoryTestCase<VagaoPatioVigenteRepository, VagaoPatioVigente, int?>
    {

        [SetUp]
        public override void Setup()
        {
            base.Setup();
           // _Configuration.AddAssembly(typeof(Usuario).Assembly);
        }

        [Test]
        public void ObterVagaoCarregamento()
        {
			var vagao = Repository.ObterParaCarregamentoPorIdEstacaoMae(362, 88074, null);
        }

        [Test]
        public void ObterPorFiltro()
        {
            DetalhesPaginacaoWeb pagination = new DetalhesPaginacaoWeb {};

            var codigosDosVagoes = "247083,319563";

            string[] codigosDosVagoesSeparados =
                        codigosDosVagoes.ToUpper()
                                        .Trim()
                                        .Replace(" ", string.Empty)
                                        .Replace(",", ";")
                                        .Replace(".", ";")
                                        .Split(';')
                                        .Where(e => !string.IsNullOrWhiteSpace(e))
                                        .ToArray();

            var listaVagoes = Repository.ObterPorFiltro(
                        pagination,
                        "TRO-PT",
                        codigosDosVagoesSeparados);

            Console.Write(listaVagoes);
        }
    }
}
