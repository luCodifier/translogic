﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
	using Core.Domain.Model.Trem.Veiculo.Vagao;
	using NUnit.Framework;

	[TestFixture]
	public class FolhaEspecificacaoVagaoRepositoryTestCase : RepositoryTestCase<FolhaEspecificacaoVagaoRepository, FolhaEspecificacaoVagao, int>
	{
		[SetUp]
		public override void Setup()
		{
			base.Setup();
		}

		[Test]
		public void ObterPorId()
		{
			FolhaEspecificacaoVagao fev = Repository.ObterPorId(17141);
		}

	}
}