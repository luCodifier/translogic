﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Trem.Veiculo.Locomotiva;
	using Core.Domain.Model.Trem;
	using Core.Domain.Model.Trem.Veiculo.Vagao;
	using NUnit.Framework;

	[TestFixture]
	public class ComposicaoVagaoRepositoryTestCase : RepositoryTestCase<ComposicaoVagaoRepository, ComposicaoVagao, int>
	{
		[SetUp]
		public override void Setup()
		{
			base.Setup();
		}

		[Test]
		public void ObterPorComposicao()
		{
			Composicao composicao = new Composicao { Id = 2578873 };
			IList<ComposicaoVagao> lista = Repository.ObterPorComposicao(composicao);

		}

	}
}
