﻿namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.Model.Estrutura;
	using NUnit.Framework;
	using Translogic.Modules.Core.Domain.DataAccess.Repositories.Trem.Veiculo.Vagao;
	using Translogic.Modules.Core.Domain.Model.Trem.Veiculo.Vagao;

	[TestFixture]
	public class VagaoRepositoryTestCase : RepositoryTestCase<VagaoRepository, Vagao, int?>
	{

		[SetUp]
		public override void Setup()
		{
			base.Setup();
			// _Configuration.AddAssembly(typeof(Usuario).Assembly);
		}

		[Test]
		public void ObterPorCodigo()
		{
			Vagao vagao = Repository.ObterPorCodigo("6363636");
			IEmpresa empresa = vagao.EmpresaProprietaria;
			string sigla = empresa.Sigla;
		}

		[Test]
		public void ObterTodos()
		{
			IList<Vagao> lista = Repository.ObterTodos();
		}
	}
}
