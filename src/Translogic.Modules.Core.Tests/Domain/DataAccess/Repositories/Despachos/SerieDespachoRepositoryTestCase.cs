namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class SerieDespachoRepositoryTestCase : RepositoryTestCase<SerieDespachoRepository, SerieDespacho, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			SerieDespacho serieDespacho = Repository.ObterPorId(1129);
		}
	
	}
}