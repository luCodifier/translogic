namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.ConhecimentoTransporteEletronico;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class ComposicaoFreteCteRepositoryTestCase : RepositoryTestCase<ComposicaoFreteCteRepository, ComposicaoFreteCte, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			IList<ComposicaoFreteCte> listaComposicao = Repository.ObterTodos();
		}

		[Test]
		public void ObterComposicaoPorCte()
		{
			Cte cte = new Cte {Id = 126747};
			IList<ComposicaoFreteCte> lista = Repository.ObterComposicaoFreteCtePorCte(cte);
		}
		
	}
}