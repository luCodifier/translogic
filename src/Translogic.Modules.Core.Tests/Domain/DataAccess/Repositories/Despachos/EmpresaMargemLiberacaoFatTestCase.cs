using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
using Translogic.Modules.Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	[TestFixture]
	public class EmpresaMargemLiberacaoFatTestCase : RepositoryTestCase<EmpresaMargemLiberacaoFatRepository, EmpresaMargemLiberacaoFat, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterTodos()
		{
			var empresaMargemLiberacaoFat = Repository.ObterTodos();
		}
	}
}