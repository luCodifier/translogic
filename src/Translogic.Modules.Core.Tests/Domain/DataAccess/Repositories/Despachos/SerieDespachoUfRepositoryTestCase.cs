namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class SerieDespachoUfRepositoryTestCase : RepositoryTestCase<SerieDespachoUfRepository, SerieDespachoUf, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void TestarBusca()
		{
			string cnpj = "24962466000136";
      string uf = "MT";
			SerieDespachoUf serie = Repository.ObterPorCnpjUf(cnpj, uf);
		}
	}
}