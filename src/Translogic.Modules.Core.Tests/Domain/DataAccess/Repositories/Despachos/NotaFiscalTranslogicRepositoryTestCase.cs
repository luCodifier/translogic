namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.Trem.Veiculo.Vagao;
	using NUnit.Framework;

	[TestFixture]
	public class NotaFiscalTranslogicRepositoryTestCase : RepositoryTestCase<NotaFiscalTranslogicRepository, NotaFiscalTranslogic, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			NotaFiscalTranslogic notaFiscal = Repository.ObterPorId(9628215);
		}

		[Test]
		public void ObterPorDespacho()
		{
			DespachoTranslogic despacho = new DespachoTranslogic { Id = 7127914 };
			IList<NotaFiscalTranslogic> notas = Repository.ObterNotaFiscalPorDespacho(despacho);
		}

		[Test]
		public void ObterPorDespachoVagao()
		{
			DespachoTranslogic despacho = new DespachoTranslogic { Id = 7127914 };
			Vagao vagao = new Vagao {Id = 339590};
			IList<NotaFiscalTranslogic> notas = Repository.ObterNotaFiscalPorDespachoVagao(despacho, vagao);
			int i = notas.Count;
		}

		[Test]
		public void ObterPorCarregamento()
		{
			Carregamento carregamento = new Carregamento { Id = 7842073 };
			IList<NotaFiscalTranslogic> notas = Repository.ObterNotaFiscalPorCarregamento(carregamento);
		}


		[Test]
		public void ObterPorChave()
		{
			IList<NotaFiscalTranslogic> notas = Repository.ObterPorChaveNfeSemDespachosCancelados("43120289086144000116550010002530831417627156");
		}
	}
}