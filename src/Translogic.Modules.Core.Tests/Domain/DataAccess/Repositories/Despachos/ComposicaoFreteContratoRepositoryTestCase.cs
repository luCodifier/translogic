namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using System;
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class ComposicaoFreteContratoRepositoryTestCase : RepositoryTestCase<ComposicaoFreteContratoRepository, ComposicaoFreteContrato, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorNumeroContrato()
		{
			// DateTime dataRef = new DateTime(2011,9,19, 14, 52, 00);
			DateTime dataRef = new DateTime(2011,11,01, 00, 00, 00);

			// IList<ComposicaoFreteContrato> listacfc = Repository.ObterPorNumeroContrato("0040192382", "F", true);
			IList<ComposicaoFreteContrato> listacfc = Repository.ObterPorNumeroContrato("0040192843", dataRef, "T", true, "ZFR");
		}

		[Test]
		public void ObterPeloContrato()
		{
			string numeroContrato = "0040203682";
			IList<ComposicaoFreteContrato> lista = Repository.ObterPorNumeroContrato(numeroContrato);



			int i = lista.Count;
		}

		[Test]
		public void ObterComposicaoFerroviasGrupoPorContrato()
		{
			string numeroContrato = "0040192843";
			string condicaoTarifa = "ZFR";
			IList<ComposicaoFreteContrato> lista = Repository.ObterComposicaoFerroviasGrupoPorNumeroContrato(numeroContrato);
			// IList<ComposicaoFreteContrato> lista = Repository.ObterComposicaoFerroviasGrupoPorNumeroContrato(numeroContrato, condicaoTarifa);
		}
	}
}