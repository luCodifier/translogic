namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class DespachoTranslogicRepositoryTestCase : RepositoryTestCase<DespachoTranslogicRepository, DespachoTranslogic, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			DespachoTranslogic despacho;
			// despacho = Repository.ObterPorId(7093537);
			despacho = Repository.ObterPorId(7127914);
			string x = despacho.Observacao;
		}

		[Test]
        public void ObterDataDescargaNfe()
        {
            var data = Repository.ObterDataDescargaNfe(68864, "529");
        }

    }
}