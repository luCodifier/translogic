namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class CarregamentoRepositoryTestCase : RepositoryTestCase<CarregamentoRepository, Carregamento, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			Carregamento caregamento = Repository.ObterPorId(5501986);
		}
	}
}