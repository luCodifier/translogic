namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using System.Collections.Generic;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.Trem.Veiculo.Vagao;
	using NUnit.Framework;

	[TestFixture]
	public class ItemDespachoRepositoryTestCase : RepositoryTestCase<ItemDespachoRepository, ItemDespacho, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			ItemDespacho itemDespacho = Repository.ObterPorId(6797690);
		}


		[Test]
		public void ObterPorVagaoComPedidoVigente()
		{
			Vagao vagao = new Vagao { Id = 301285 };
			IList<ItemDespacho> itemDespacho = Repository.ObterPorVagaoComPedidoVigente(vagao);

		}

		[Test]
		public void ObterItemDespachoPorDespacho()
		{
			DespachoTranslogic desp = new DespachoTranslogic {Id = 6739182 };
			ItemDespacho item = Repository.ObterItemDespachoPorDespacho(desp);
		}
	}
}