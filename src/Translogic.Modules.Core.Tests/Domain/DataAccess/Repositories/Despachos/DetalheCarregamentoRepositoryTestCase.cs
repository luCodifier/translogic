namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.FluxosComerciais.Pedidos.Despachos;
	using Core.Domain.Model.FluxosComerciais.Pedidos.Despachos;
	using NUnit.Framework;

	[TestFixture]
	public class DetalheCarregamentoRepositoryTestCase : RepositoryTestCase<DetalheCarregamentoRepository, DetalheCarregamento, int?>
	{
		public override void TestSetup()
		{
			base.TestSetup();
		}

		[Test]
		public void ObterPorId()
		{
			DetalheCarregamento detalhe = Repository.ObterPorId(6920589);
		}
	}
}