﻿using System;
using ALL.TestsSupport.AcessoDados;
using NUnit.Framework;
using Translogic.Core.Infrastructure.Web;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Despacho;
using Translogic.Modules.Core.Domain.Model.Dto.Despacho;
using Translogic.Modules.Core.Domain.Model.Despacho;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Despachos
{
    [TestFixture]
    public class ConfiguracaoDespachoAutomaticoRepositoryTestCase : RepositoryTestCase<ConfiguracaoDespachoAutomaticoRepository, ConfiguracaoDespachoAutomatico, int>
    {
        [Test]
        public void PesquisaTestCase()
        {
            var valor = Repository.Pesquisar(new DetalhesPaginacaoWeb(), new ConfiguracaoDespachoAutomaticoFiltroDto() { DtInicial = DateTime.Now.Date.AddDays(-5).ToShortDateString(), DtFim = DateTime.Now.Date.ToShortDateString() });
            Assert.IsNotNull(valor);
        }

        [Test]
        public void InsertTestCase()
        {
            var valor = new ConfiguracaoDespachoAutomatico
                            {
                                Cliente = "ADM",
                                AreaOperacionaId = 388,
                                LocalizacaoId = 14,
                                Manual = "S",
                                Patio = "S",
                                Terminal = "S",
                                Segmento = "ACUCAR"
                            };

            Repository.Inserir(valor);
        }

        [Test]
        public void UpdateTestCase()
        {
            var valor = Repository.ObterPorId(15);
            valor.LocalizacaoId = 14;

            Repository.Atualizar(valor);
        }

        [Test]
        public void JaCadastradoTestCase()
        {
            var valor = Repository.JaCadastrado(23, "VIE", 429, "TRACKAGE");
            Assert.IsTrue(valor);
        }
    }
}
