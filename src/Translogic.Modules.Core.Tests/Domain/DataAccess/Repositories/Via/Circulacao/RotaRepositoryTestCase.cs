namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Via.Circulacao
{
	using System;
	using ALL.TestsSupport.AcessoDados;
    using Core.Domain.DataAccess.Repositories.Via.Circulacao;
	using Core.Domain.Model.Via;
	using Core.Domain.Model.Via.Circulacao;
    using NUnit.Framework;

    public class RotaRepositoryTestCase : RepositoryTestCase<RotaRepository, Rota, int>
    {
        [Test]
        public void ObterUmaRota()
        {
            Rota rota = Repository.ObterPrimeiro();
        }

		[Test]
		public void ObterPorOrigemDestino()
		{
			Rota rota = Repository.ObterPorOrigemDestino(new PatioCirculacao {Codigo = "LCM-PC"}, new PatioCirculacao {Codigo = "ZOU-PC"});

			Console.WriteLine(rota);
		}
    }
}