﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALL.TestsSupport.AcessoDados;
using Translogic.Modules.Core.Domain.DataAccess.Repositories.Via.Circulacao;
using Translogic.Modules.Core.Domain.Model.Via;
using Translogic.Modules.Core.Domain.Model.Via.Circulacao;
using NUnit.Framework;

namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Via.Circulacao
{
    [TestFixture]
    public class AreaOperacionalRepositoryTestCase : RepositoryTestCase<AreaOperacionalRepository, IAreaOperacional, int?>
    {
        [Test]
        public void ObterPorClienteTestCase()
        {
            var valor = Repository.ObterPorCliente("LLD");
            Assert.IsTrue(valor.Any());
        }
    }
}
