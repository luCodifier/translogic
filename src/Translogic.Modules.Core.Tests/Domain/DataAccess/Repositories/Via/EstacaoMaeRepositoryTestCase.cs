namespace Translogic.Modules.Core.Tests.Domain.DataAccess.Repositories.Via
{
	using System;
	using ALL.TestsSupport.AcessoDados;
	using Core.Domain.DataAccess.Repositories.Via;
	using Core.Domain.DataAccess.Repositories.Via.Circulacao;
	using Core.Domain.Model.Via;
	using Core.Domain.Model.Via.Circulacao;
    using NUnit.Framework;

    public class EstacaoMaeRepositoryTestCase : RepositoryTestCase<EstacaoMaeRepository, EstacaoMae, int?>
    {
        [Test]
		public void ObterPorOrigemDestino()
        {
            EstacaoMae estacaoMae = Repository.ObterPorId(421);

			Console.WriteLine(estacaoMae.EmpresaOperadora.Id);
		}
    }
}