namespace Translogic.Modules.Core.Tests.Domain.Model.Acesso
{
	using Core.Domain.Model.Acesso;
	using NUnit.Framework;

	[TestFixture]
	public class MenuTestCase
	{
		[Test]
		public void CanInstanceAndFill()
		{
			Menu menu = new Menu
			            	{
			            		Ordem = 1,
			            		Path = "/home",
								Chave = "Home",
			            		Transacao = "base_transact",
			            		Ativo = true
			            	};

			Assert.IsNotNull(menu);
		}
	}
}