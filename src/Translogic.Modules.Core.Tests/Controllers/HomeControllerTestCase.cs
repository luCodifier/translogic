﻿namespace Translogic.Modules.Core.Tests.Controllers
{
	using Castle.MicroKernel.Registration;

	using NUnit.Framework;

	using Translogic.Core.Infrastructure.Settings;
	using Translogic.Modules.Core.Controllers;
	using Translogic.Tests;

	public class HomeControllerTestCase : BaseTranslogicContainerTestCase
	{
		private AcessoController _controller;

		protected override void Setup()
		{
			TemTransacao = false;
			base.Setup();
			Container.Register(Component.For<ServerSettings>().Named("server.settings"));
			_controller = Resolve<AcessoController>();
		}

		[Test]
		public void Testar()
		{
			var x = _controller.Testar();
		}
	}
}