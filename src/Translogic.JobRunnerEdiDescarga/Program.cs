﻿namespace Translogic.JobRunnerEdiDescarga
{
    using Speed.Common;
    using System;
    using System.Linq;
    using System.Threading;
    using Translogic.Core;
    using Translogic.Modules.Core.Interfaces.Jobs;
    using Translogic.Modules.Core.Jobs.JobRunner;
    using Translogic.Modules.Core.Spd.Data.Enums;

    /// <summary>
    /// Classe que inicia o programa
    /// </summary>
    public static class Program
    {

        const string key = "edi.job.DescargaEDIJob";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static int Main(string[] args)
        {
            // DescargaEDIJob
            return JobRunnerProgram.Execute(EnumSistema.JobRunnerEdiDescarga, key, args);
        }
    }
}
