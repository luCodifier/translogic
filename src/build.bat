@ECHO OFF

REM Remover chave Platform caso notebook HP das Enviroment Variables

REM Aplicar patch caso Windows x64
REM KB2298853 - CLR 4.0: MSBuild fails with error MSB4014 when building VS 2010 proj
REM http://archive.msdn.microsoft.com/KB2298853/Release/ProjectReleases.aspx?ReleaseId=4955

@ECHO ON

if "%1" == "" (goto:x86) else (goto:x64)


:x86
BuildScripts\teamcity-build.bat 3.0.0 DEV x86

:x64
BuildScripts\teamcity-build.bat 3.0.0 DEV x64

Pause