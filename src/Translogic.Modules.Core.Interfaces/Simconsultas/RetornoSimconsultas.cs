namespace Translogic.Modules.Core.Interfaces.Simconsultas
{
    using System.Xml;

    /// <summary>
    /// Retorno SimConsultas
    /// </summary>
    public class RetornoSimconsultas
    {
        /// <summary>
        /// Indica se deu erro ao chamar o WS
        /// </summary>
        public bool Erro { get; set; }

        /// <summary>
        /// String do XmlNfe.
        /// </summary>
        public XmlNode Nfe { get; set; }

        /// <summary>
        /// Mensagem de Erro.
        /// </summary>
        public string MensagemErro { get; set; }
    }
}