namespace Translogic.Modules.Core.Interfaces.Simconsultas.Interfaces
{
    /// <summary>
	/// Interface para comunica��o entre o service do simconsultas e o core do translogic
	/// </summary>
	public interface ISimconsultasService
    {
        /// <summary>
        /// Faz a chamda no WS do simconsultas da Nfe
        /// </summary>
        /// <param name="chaveAcesso"> Chave de autoriza��o do Simconsultas. </param>
        /// <param name="chaveNfe"> Chave da Nfe. </param>
        /// <returns>Objeto de retorno do simconsultas</returns>
        RetornoSimconsultas ObterDadosNfe(string chaveAcesso, string chaveNfe);

        /// <summary>
        /// Faz a chamda no WS do simconsultas da Nfe com prioridade no nacional
        /// </summary>
        /// <param name="chaveAcesso"> Chave de autoriza��o do Simconsultas. </param>
        /// <param name="chaveNfe"> Chave da Nfe. </param>
        /// <returns>Objeto de retorno do simconsultas</returns>
        RetornoSimconsultas ObterDadosNfeNacional(string chaveAcesso, string chaveNfe);

        /// <summary>
        /// Faz a chamda no WS do simconsultas do Cte
        /// </summary>
        /// <param name="chaveAcesso"> Chave de autoriza��o do Simconsultas. </param>
        /// <param name="chaveCte"> Chave da Cte. </param>
        /// <returns>Objeto de retorno do simconsultas</returns>
        RetornoSimconsultas ObterDadosCte(string chaveAcesso, string chaveCte);

        /*
        /// <summary>
        /// Novo m�todo GetNfeXMLOriginal disponibilizado para atender a necessidade do Certificado Digital
        /// </summary>
        /// <param name="chaveAcesso">Chave de acesso exclusiva do cliente fornecida previamente pela SIM>Consultas</param>
        /// <param name="chaveNfe">N�mero da chave de acesso da NF-e a ser consultada com 44 posi��es</param>
        /// <returns>Objeto de retorno do simconsultas</returns>
        RetornoSimconsultas GetNfeXMLOriginal(string chaveAcesso, string chaveNfe);
        */
    }
}