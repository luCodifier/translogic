﻿namespace Translogic.Modules.Core.Interfaces.DadosFiscais
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Xml.Serialization;

	/// <summary>
	/// Classe  de CTe para interface com a SEFAS-MT
	/// </summary>
	public class CteFerroviarioDescarregado
	{
		private string ctesNovos;
		private string nfes;

		/// <summary>
		/// Chave do Cte gerado
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// Status do CTe
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// Lista de chaves geradas em caso de cancelamento
		/// </summary>
		[XmlArray("ChavesNovosCtes")]
		[XmlArrayItem("NovoCte")]
		public List<string> ChaveNovosCtes { get; set; }

		/// <summary>
		/// Lista de chaves geradas em caso de cancelamento
		/// </summary>
		[XmlArray("ChavesNfes")]
		[XmlArrayItem("Nfe")]
		public List<string> ChavesNfes { get; set; }

		/// <summary>
		/// Destino da mercadoria
		/// </summary>
		public string RecintoEntregaAlfandegado { get; set; }

		/// <summary>
		/// Peso entregue
		/// </summary>
		public decimal PesoEntregue { get; set; }

		/// <summary>
		/// Cidade de Embarque
		/// </summary>
		public string CidadeEmbarque { get; set; }

		/// <summary>
		/// Novos Ctes
		/// </summary>
		[XmlIgnore]
		public string CtesNovos
		{
			get
			{
				return this.ctesNovos;
			}

			set
			{
				this.ctesNovos = value;
				if (value != null)
				{
					string[] x = this.ctesNovos.Split(Convert.ToChar("|"));
					ChaveNovosCtes = x.ToList();
				}
			}
		}

		/// <summary>
		/// Notas fiscais
		/// </summary>
		[XmlIgnore]
		public string Nfes
		{
			get
			{
				return this.nfes;
			}

			set
			{
				this.nfes = value;
				if (value != null)
				{
					string[] x = this.nfes.Split(Convert.ToChar("|"));
					ChavesNfes = x.ToList();
				}
			}
		}
	}
}