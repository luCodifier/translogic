﻿namespace Translogic.Modules.Core.Interfaces.DadosFiscais
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Xml.Serialization;

	/// <summary>
	/// Classe de cte cancelado para a interface SEFAZ
	/// </summary>
	public class CteFerroviarioCancelado
	{
		private string nfes;

		/// <summary>
		/// Id do CTe...
		/// </summary>
		[XmlIgnore]
		public int? IdCte { get; set; }

		/// <summary>
		/// Chave do CTe
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// String das NFes
		/// </summary>
		[XmlIgnore]
		public string Nfes
		{
			get
			{
				return this.nfes;
			}

			set
			{
				this.nfes = value;
				if (value != null)
				{
					string[] x = this.nfes.Split(Convert.ToChar("|"));
					ListaNFes = x.ToList();
				}
			}
		}

		/// <summary>
		/// Lista de NFes
		/// </summary>
		public List<string> ListaNFes { get; set; }

		/// <summary>
		/// Lista de novos ctes gerados a partir deste
		/// </summary>
		public IList<CteFerroviarioCanceladoNovoCte> ListaNovosCtes { get; set; }
	}
}