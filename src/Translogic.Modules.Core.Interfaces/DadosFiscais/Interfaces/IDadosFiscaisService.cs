﻿namespace Translogic.Modules.Core.Interfaces.DadosFiscais.Interfaces
{
	using System;
	using System.Collections.Generic;
	using System.ServiceModel;

	/// <summary>
	/// Interface de dados fiscais
	/// </summary>
	[ServiceContract]
	public interface IDadosFiscaisService
	{
		/// <summary>
		/// Obtém os ctes gerados no dia passado como parametro
		/// </summary>
		/// <param name="data">Dia da consulta</param>
		/// <returns>Lista de Ctes gerados</returns>
		[OperationContract]
		IList<CteFerroviarioDescarregado> ObterCtesFerroviariosDescarregadosPorData(DateTime data);

		/// <summary>
		/// Obtém os ctes cancelados no dia passado como parametro
		/// </summary>
		/// <param name="data">Dia de consulta</param>
		/// <returns>Lista de ctes cancelados</returns>
		[OperationContract]
		IList<CteFerroviarioCancelado> ObterCtesCanceladosPorData(DateTime data);

		/// <summary>
		/// Obtém os ctes rodoviarios no dia passado como parametro
		/// </summary>
		/// <param name="data">Data de descarga</param>
		/// <returns>Lista de Ctes descarregados</returns>
		[OperationContract]
		IList<CteRodoviarioDescarregado> ObterCtesRodoviariosDescarregadosPorData(DateTime data);
	}
}