﻿namespace Translogic.Modules.Core.Interfaces.DadosFiscais
{
	using System;
	using System.Collections.Generic;
	using System.Xml.Serialization;

	/// <summary>
	/// Cte Rodoviario descarregado
	/// </summary>
	public class CteRodoviarioDescarregado
	{
		/// <summary>
		/// Gets or sets the chave cte.
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// Gets or sets the data descarga.
		/// </summary>
		public DateTime DataDescarga { get; set; }
		
		/// <summary>
		/// Gets or sets the placa.
		/// </summary>
		public string Placa { get; set; }

		/// <summary>
		/// Gets or sets the peso aferido.
		/// </summary>
		public decimal PesoAferido { get; set; }

		/// <summary>
		/// Gets or sets the peso total declarado.
		/// </summary>
		public decimal PesoTotalDeclarado { get; set; }

		/// <summary>
		/// Gets or sets the diferenca.
		/// </summary>
		public decimal Diferenca { get; set; }

		/// <summary>
		/// Gets or sets the lista nfes.
		/// </summary>
		[XmlArray("NFes")]
		[XmlArrayItem("NFe")]
		public IList<NfeCteRodoviario> ListaNfes { get; set; }
	}

	/// <summary>
	/// Nfe cte rodoviario
	/// </summary>
	public class NfeCteRodoviario
	{
		/// <summary>
		/// Gets or sets the chave nfe.
		/// </summary>
		[XmlAttribute]
		public string ChaveNfe { get; set; }

		/// <summary>
		/// Gets or sets the peso declarado.
		/// </summary>
		[XmlAttribute]
		public decimal PesoDeclarado { get; set; }
	}

	/// <summary>
	/// The cte nfe rodoviario descarregado.
	/// </summary>
	public class CteNfeRodoviarioDescarregado
	{
		/// <summary>
		/// Gets or sets the chave cte.
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// Gets or sets the chave nfe.
		/// </summary>
		public string ChaveNfe { get; set; }

		/// <summary>
		/// Gets or sets the data descarga.
		/// </summary>
		public DateTime DataDescarga { get; set; }

		/// <summary>
		/// Gets or sets the placa.
		/// </summary>
		public string Placa { get; set; }

		/// <summary>
		/// Gets or sets the peso aferido.
		/// </summary>
		public decimal PesoAferido { get; set; }

		/// <summary>
		/// Gets or sets the peso total declarado.
		/// </summary>
		public decimal PesoDeclarado { get; set; }
	}
}