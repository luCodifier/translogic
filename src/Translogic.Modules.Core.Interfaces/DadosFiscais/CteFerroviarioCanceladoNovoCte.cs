namespace Translogic.Modules.Core.Interfaces.DadosFiscais
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Xml.Serialization;

	/// <summary>
	/// Classe de cte novo a partir de um cancelamento
	/// </summary>
	public class CteFerroviarioCanceladoNovoCte
	{
		private string nfes;

		/// <summary>
		/// Chave do CTe
		/// </summary>
		public string ChaveCte { get; set; }

		/// <summary>
		/// String das NFes
		/// </summary>
		[XmlIgnore]
		public string Nfes
		{
			get
			{
				return this.nfes;
			}

			set
			{
				this.nfes = value;
				if (value != null)
				{
					string[] x = this.nfes.Split(Convert.ToChar("|"));
					ListaNFes = x.ToList();
				}
			}
		}

		/// <summary>
		/// Lista de nfes
		/// </summary>
		public List<string> ListaNFes { get; set; }
	}
}