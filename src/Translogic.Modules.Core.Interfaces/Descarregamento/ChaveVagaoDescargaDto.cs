﻿namespace Translogic.Modules.Core.Interfaces.Descarregamento
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Dados das notas carregadas
    /// </summary>
    [DataContract(Name = "NotaFiscal")]
    public class ChaveVagaoDescargaDto
    {
        #region PROPRIEDADES
        /// <summary>
        /// Chave da nf-e descarregada.
        /// </summary>
        [DataMember(Order = 1)]
        public string ChaveNfe { get; set; }
        #endregion
    }
}