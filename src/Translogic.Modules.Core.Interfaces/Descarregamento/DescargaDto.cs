﻿namespace Translogic.Modules.Core.Interfaces.Descarregamento
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Dados do descarregamento
    /// </summary>
    [DataContract(Name = "Descarregamento", Namespace = "http://all-logistica.net/v01/Descarregamento")]
    public class DescargaDto
    {
        #region PROPRIEDADES
        /// <summary>
        /// Cnpj da empresa correntista do fluxo.
        /// </summary>
        [DataMember(Order = 1)]
        public string CnpjCliente { get; set; }

        /// <summary>
        /// Código do terminal de descarga
        /// </summary>
        [DataMember(Order = 2)]
        public string CodigoTerminal { get; set; }

        /// <summary>
        /// Dados dos vagões descarregados
        /// </summary>
        [DataMember(Name = "Vagao", Order = 3)]
        public IList<VagaoDescargaDto> Vagao { get; set; }
        #endregion
    }
}