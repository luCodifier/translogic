﻿namespace Translogic.Modules.Core.Interfaces.Descarregamento
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    
    /// <summary>
    /// Dados do descarregamento
    /// </summary>
    [DataContract(Name = "Vagao")]
    public class VagaoDescargaDto
    {
        #region PROPRIEDADES
        /// <summary>
        /// Série do vagão
        /// </summary>
        [DataMember(Order = 1)]
        public string SerieVagao { get; set; }

        /// <summary>
        /// Código do vagão
        /// </summary>
        [DataMember(Order = 2)]
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Peso carregado em toneladas
        /// </summary>
        [DataMember(Order = 3)]
        public string PesoCarregado { get; set; }

        /// <summary>
        /// Peso tara em toneladas
        /// </summary>
        [DataMember(Order = 4)]
        public string PesoTara { get; set; }

        /// <summary>
        /// Data e hora da pesagem.
        /// </summary>
        [DataMember(Order = 5)]
        public string DataInicioPesagem { get; set; }

        /// <summary>
        /// Data e hora de fim da pesagem.
        /// </summary>
        [DataMember(Order = 6)]
        public string DataFimPesagem { get; set; }

        /// <summary>
        /// Dados das notas carregadas
        /// </summary>
        [DataMember(Name = "NotaFiscal", Order = 7)]
        public IList<ChaveVagaoDescargaDto> NotaFiscal { get; set; }
        #endregion
    }
}