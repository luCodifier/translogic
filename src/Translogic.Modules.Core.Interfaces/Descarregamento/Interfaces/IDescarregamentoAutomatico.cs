﻿namespace Translogic.Modules.Core.Interfaces.Descarregamento.Interfaces
{
    using System.ServiceModel;

    /// <summary>
    /// Interface de descarregamento
    /// </summary>
    [ServiceContract]
    public interface IDescarregamentoAutomatico
    {
        /// <summary>
        /// Informa os dados de descarregamento
        /// </summary>
        /// <param name="descarregamento">Dados do descarregamento</param>
        /// <returns>Protocolo de recebimento</returns>
        [OperationContract()]
        ResultadoDescarga InformarDescarregamento(DescargaDto descarregamento);
    }
}
