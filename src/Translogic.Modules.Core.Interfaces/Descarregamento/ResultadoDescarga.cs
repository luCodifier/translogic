﻿namespace Translogic.Modules.Core.Interfaces.Descarregamento
{
    /// <summary>
    /// Resultado ao informar o descarregamento
    /// </summary>
    public class ResultadoDescarga
    {
        /// <summary>
        /// Sucesso caso processo ocorra normalmente
        /// </summary>
        public bool Sucesso { get; set; }

        /// <summary>
        /// Detalhes caso ocorra falha
        /// </summary>
        public string Protocolo { get; set; }

        /// <summary>
        /// Detalhes caso ocorra falha
        /// </summary>
        public string Detalhes { get; set; }
    }
}
