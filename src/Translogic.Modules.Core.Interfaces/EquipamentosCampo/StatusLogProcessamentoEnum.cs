﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    /// <summary>
    /// Enum para os tipos de processamento dos resultados de listagens do Hotbox
    /// </summary>
    public enum StatusLogProcessamentoEnum
    {
        /// <summary>
        /// Inicia o Monitoramento
        /// </summary>
        Inicio = 1,

        /// <summary>
        /// Ao retornar informações, inicia o Processamento
        /// </summary>
        InicioProcessamento = 2,

        /// <summary>
        /// Quando está processando os dados obtidos
        /// </summary>
        ProcessamentoEquipamento = 3,

        /// <summary>
        /// Ocorreu erro no processamento ou Exception
        /// </summary>
        ErroProcessamentoEquipamento = 4,

        /// <summary>
        /// Finalizou o processamento do Trem
        /// </summary>
        TremProcessado = 5,

        /// <summary>
        /// Finalizou o processamento
        /// </summary>
        FimProcessamento = 6,

        /// <summary>
        /// Fim do monitoramento do HotBox
        /// </summary>
        Fim = 7,

        /// <summary>
        /// Status de envio de Email
        /// </summary>
        EnviarEmail,

        /// <summary>
        /// Erro ao tentar enviar email
        /// </summary>
        ErroEnviarEmail,

        /// <summary>
        /// Gravacao do arquivo com comando L
        /// </summary>
        GravacaoArquivoBruto,

        /// <summary>
        /// Gravacao do arquivo com comando D
        /// </summary>
        GravacaoArquivoDetalhado,

        /// <summary>
        /// Indica o pré processamento (Envio do comando L)
        /// </summary>
        InicioPreProcessamento,

        /// <summary>
        /// Fim do pré processamento (Retorno do Comando L)
        /// </summary>
        FimPreProcessamento,

        /// <summary>
        /// Execucao durante o pré processamento
        /// </summary>
        PreProcessamento,

        /// <summary>
        /// Não há processamento para equipamentos inativos
        /// </summary>
        EquipamentoInativoSemProcessamento
    }
}
