﻿
namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.HotBox
{
    /// <summary>
    /// Dto para locais do HotBox
    /// </summary>
    public class HotBoxLocalDto
    {
        /// <summary>
        /// Id do local
        /// </summary>
        public int IdLocal { get; set; }

        /// <summary>
        /// Código da área operacional
        /// </summary>
        public string CodigoLocal { get; set; }

        /// <summary>
        /// KM de instalação do equipamento
        /// </summary>
        public float KilometroInstalacao { get; set; }

        /// <summary>
        /// IP do equipamento
        /// </summary>
        public string IpEquipamento { get; set; }

        /// <summary>
        /// Porta do equipamento
        /// </summary>
        public int PortaEquipamento { get; set; }

        /// <summary>
        /// Indica S se ativo e N se inativo
        /// </summary>
        public string Ativo { get; set; }
    }
}