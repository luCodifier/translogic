﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    /// <summary>
    /// Enum para demonstrar os Status de leitura de equipamento
    /// </summary>
    public enum StatusLeituraEquipamentoEnum
    {
        /// <summary>
        /// Incio da leitura do Equipamento
        /// </summary>
        IniciandoLeitura,
        
        /// <summary>
        /// Realizando a leitura de dados do equipamento
        /// </summary>
        Lendodados,
        
        /// <summary>
        /// Leitura de dados realizada, fim do monitoramento
        /// </summary>
        LeituraRealizada,

        /// <summary>
        /// Indicado quando ocorre falha na comunicação com o equipamento
        /// </summary>
        FalhaComunicacao,

        /// <summary>
        /// Erro ao tentar obter o equipamento pelo id
        /// </summary>
        ErroLocalizarEquipamento,

        /// <summary>
        /// Ocorreu algum erro que não foi possível identificar, Exception
        /// </summary>
        Erro,

        /// <summary>
        /// Indica quando o equipamento está inativo
        /// </summary>
        EquipamentoInativo
    }
}
