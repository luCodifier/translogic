namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Detac
{
	/// <summary>
	/// Classe com as propriedades do eixo do Detac
	/// </summary>
	public class DetacFileEixo
	{
		/// <summary>
		/// Sequencia dos eixos
		/// </summary>
		public int Eixo { get; set; }

		/// <summary>
		/// Lado A do eixo
		/// </summary>
		public int SeveridadeA { get; set; }

		/// <summary>
		/// Lado B do eixo
		/// </summary>
		public int SeveridadeB { get; set; }

		/// <summary>
		/// o numero do carro
		/// </summary>
		public int Carro { get; set; }

		/// <summary>
		/// Tipo do veiculo detac (0 - Vagao, 1 - Locomotiva)
		/// </summary>
		public int TipoVeiculoDetac { get; set; }
	}
}