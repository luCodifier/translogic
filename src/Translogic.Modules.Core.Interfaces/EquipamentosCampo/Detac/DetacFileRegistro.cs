namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Detac
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Classe de registro dos Detac
	/// </summary>
	public class DetacFileRegistro
	{
		/// <summary>
		/// Construtor do DetacFileRegistro
		/// </summary>
		public DetacFileRegistro()
		{
			Eixos = new List<DetacFileEixo>();
		}

		/// <summary>
		/// N�mero de s�rie do equipamento
		/// </summary>
		public string NumeroSerieEquipamento { get; set; }

		/// <summary>
		/// Dire��o da composi��o
		/// </summary>
		public int DirecaoComposicao { get; set; }

		/// <summary>
		/// Tens�o da bateria / nobreak
		/// </summary>
		public float Tensao { get; set; }

		/// <summary>
		/// Data e hora de inicio
		/// </summary>
		public DateTime DataHoraInicio { get; set; }

		/// <summary>
		/// Dura��o da aquisi��o em Minutos
		/// </summary>
		public int DuracaoAquisicaoMinutos { get; set; }

		/// <summary>
		/// Dura��o da aquisi��o em segundos
		/// </summary>
		public int DuracaoAquisicaoSegundos { get; set; }

		/// <summary>
		/// Temperatura m�dia ambi�nte
		/// </summary>
		public float TemperaturaMediaAmbiente { get; set; }

		/// <summary>
		/// Temperatura m�dia case
		/// </summary>
		public float TemperaturaMediaCase { get; set; }

		/// <summary>
		/// Temperatura m�dia do equipamento
		/// </summary>
		public float TemperaturaMediaEquipamento { get; set; }

		/// <summary>
		/// Velocidade Minima da Composi��o
		/// </summary>
		public float VelocidadeMinimaComposicao { get; set; }

		/// <summary>
		/// Velovidade M�xima da Composi��o
		/// </summary>
		public float VelocidadeMaximaComposicao { get; set; }

		/// <summary>
		/// Velocidade M�dia da Composi��o
		/// </summary>
		public float VelocidadeMediaComposicao { get; set; }

		/// <summary>
		/// Numero Total de Eixos
		/// </summary>
		public int NumeroTotalEixos { get; set; }

		/// <summary>
		/// Lista com os eixos do Detac
		/// </summary>
		public IList<DetacFileEixo> Eixos { get; set; }
	}
}