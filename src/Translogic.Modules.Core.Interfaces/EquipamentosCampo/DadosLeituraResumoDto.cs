﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System;

    /// <summary>
    /// Dto com as informações do resumo (cabeçalho) lido do equipamento para determinado Trem
    /// </summary>
    public class DadosLeituraResumoDto
    {
        /// <summary>
        /// Id dos dados de leitura
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id do log de processamento que se obteve os dados
        /// </summary>
        public int IdLogProcessamentoEquipamento { get; set; }

        /// <summary>
        /// Sb que ocorreu a passagem
        /// </summary>
        public string LocalPassagem { get; set; }

        /// <summary>
        /// Km possível da instalação do HotBox
        /// </summary>
        public decimal KmLocalPassagem { get; set; }

        /// <summary>
        /// Direção que o trem estava
        /// </summary>
        public string DirecaoPassagem { get; set; }

        /// <summary>
        /// Velocidade que o Trem iniciou a passagem
        /// </summary>
        public int VelocidadeEntradaPassagem { get; set; }

        /// <summary>
        /// Velocidade que o Trem terminou a passagem
        /// </summary>
        public int VelocidadeSaidaPassagem { get; set; }

        /// <summary>
        /// Menor velocidade atingida pelo trem
        /// </summary>
        public int VelocidadeMenorPassagem { get; set; }

        /// <summary>
        /// Data de incio da passagem do trem
        /// </summary>
        public DateTime DataInicioPassagem { get; set; }

        /// <summary>
        /// Data que terminou a passagem
        /// </summary>
        public DateTime DataFimPassagem { get; set; }

        /// <summary>
        /// Temperatura ambiente do HotBox
        /// </summary>
        public string TemperaturaAmbiente { get; set; }

        /// <summary>
        /// Tamanho total da composição em metros
        /// </summary>
        public string TamanhoTotalPassagem { get; set; }

        /// <summary>
        /// Quantidade de rodantes lidos pelo equipamento
        /// </summary>
        public int QuantidadeRodantesPassagem { get; set; }

        /// <summary>
        /// Quantidade de carros que realizaram a passagem
        /// </summary>
        public int QuantidadeCarrosPassagem { get; set; }

        /// <summary>
        /// Quantidade de alarmes que ocorreram
        /// </summary>
        public int QuantidadeAlarmesPassagem { get; set; }
    }
}
