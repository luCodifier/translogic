﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System.Collections.Generic;
    using Translogic.Modules.Core.Interfaces.EquipamentosCampo.HotBox;

    /// <summary>
    /// Dto com as informações necessárias para envio de email
    /// </summary>
    public class MailMessageDto
    {
        /// <summary>
        /// HotBox que foi realizada a leitura ou a tentativa de leitura
        /// </summary>
        public HotBoxLocalDto HotBoxLocal { get; set; }

        /// <summary>
        /// Comando que foi enviado ao Equipamento
        /// </summary>
        public string Comando { get; set; }

        /// <summary>
        /// Lista de destinatários de email
        /// </summary>
        public IList<string> ListaEmails { get; set; }
        
        /// <summary>
        /// Tipo de envio de email
        /// </summary>
        public TipoEmailEquipamentoEnum TipoEmailEquipamento { get; set; }
    }
}
