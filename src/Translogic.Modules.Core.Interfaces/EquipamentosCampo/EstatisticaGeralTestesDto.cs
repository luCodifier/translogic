﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    /// <summary>
    /// Dto para passagem dos testes realizados de acordo com as estatísticas gerais
    /// </summary>
    public class EstatisticaGeralTestesDto
    {
        /// <summary>
        /// Relacionamento para saber a leitura de equipamento deste teste
        /// E pela leitura de equipamento, obter os rodantes
        /// </summary>
        public int IdEstatisticaGeral { get; set; }

        /// <summary>
        /// Relacionamento para saber a leitura de equipamento deste teste
        /// E pela leitura de equipamento, obter os rodantes
        /// </summary>
        public int IdLogProcessamento { get; set; }

        /// <summary>
        /// Indica se ocorreu ou não Alarme de Parada
        /// </summary>
        public string Teste1Alarme { get; set; }

        /// <summary>
        /// Indica se existe ou não freio agarrado no Trem
        /// Ou Falha no HotBox
        /// </summary>
        public string Teste2Freio { get; set; }

        /// <summary>
        /// Indica se ocorreu Erro na leitura de dirença dos canais
        /// </summary>
        public string Teste3Canais { get; set; }

        /// <summary>
        /// Indica se existe Falha de Tendência no CH1
        /// Ou Falha no HotBox
        /// </summary>
        public string Teste4RolamentosCanal1 { get; set; }

        /// <summary>
        /// Indica se existe Falha de Tendência no CH2
        /// Ou Falha no HotBox
        /// </summary>
        public string Teste5RolamentosCanal2 { get; set; }

        /// <summary>
        /// Indica se existe Falha na contagem de eixos
        /// </summary>
        public string Teste6Integridade { get; set; }

        /// <summary>
        /// Indica se existe Elevada dispersão entre os canais
        /// </summary>
        public string Teste7Dispersao { get; set; }
    }
}
