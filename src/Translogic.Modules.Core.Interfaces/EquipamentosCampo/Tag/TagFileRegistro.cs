namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Tag
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Classe de registro dos tags
	/// </summary>
	public class TagFileRegistro
	{
		/// <summary>
		/// Construtor do TagFileRegistro
		/// </summary>
		public TagFileRegistro()
		{
			TagsLeitura = new List<TagFileLeitura>();
		}

		/// <summary>
		/// N�mero do pacote
		/// </summary>
		public int NumeroPacote { get; set; }

		/// <summary>
		/// Identificador do Leitor
		/// </summary>
		public string IdLeitor { get; set; }

		/// <summary>
		/// Data e hora do inicio da leitura
		/// </summary>
		public DateTime DataHoraInicioLeitura { get; set; }

		/// <summary>
		/// Data e hora do fim da leitura
		/// </summary>
		public DateTime DataHoraFimLeitura { get; set; }

		/// <summary>
		/// Quantidade de Tag que foi lido
		/// </summary>
		public int QuantidadeTagLido { get; set; }

		/// <summary>
		/// Identificador da chave do CRC16 (Id do Leitor ~ horario final leitura do tag)
		/// </summary>
		public string Crc16 { get; set; }

		/// <summary>
		/// Lista dos Tags lidos
		/// </summary>
		public IList<TagFileLeitura> TagsLeitura { get; set; }
	}
}