namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Tag
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;

	/// <summary>
	/// Classe de tratamento dos arquivos de TAG
	/// </summary>
	public class TagFile
	{
		// private readonly byte BYTE_STX = 0x2;
		// private readonly byte BYTE_ETX = 0x3;
		private readonly char BYTE_SEPARADOR = ',';

		#region Constutores
		/// <summary>
		/// Construtor protegido do TagFile
		/// </summary>
		protected TagFile()
		{
			Registros = new List<TagFileRegistro>();
		}
		#endregion

		/// <summary>
		/// Propriedade com o nome do arquivo
		/// </summary>
		public string NomeArquivo { get; set; }

		/// <summary>
		/// Lista de registros do arquivo
		/// </summary>
		public IList<TagFileRegistro> Registros { get; set; }

		/// <summary>
		/// Cria uma inst�ncia da classe pelo arquivo
		/// </summary>
		/// <param name="nomeArquivo">Nome do arquivo de tag</param>
		/// <returns>Retorna a instancia da classe</returns>
		public static TagFile CreateFromFile(string nomeArquivo)
		{
			TagFile tagFile = new TagFile();
			tagFile.NomeArquivo = nomeArquivo;
			tagFile.ProcessarLeitura();
			return tagFile;
		}

    /// <summary>
    /// Processa a leitura do arquivo de Tag
    /// </summary>
		private void ProcessarLeitura()
		{
			TagFileRegistro tagRegistro;
			string linha;
			StreamReader reader = new StreamReader(NomeArquivo);
			try
			{
				while (!reader.EndOfStream)
				{
					linha = reader.ReadLine();
					if (InicioDoTexto(linha))
					{
						tagRegistro = new TagFileRegistro();
						ProcessaTagFileRegistroHeader(ref tagRegistro, linha);
						linha = reader.ReadLine();
						ProcessaTagFileRegistro(ref tagRegistro, linha);

						// Le os detalhes (leitura dos tags)
						int total = tagRegistro.QuantidadeTagLido;
						while (true)
						{
							linha = reader.ReadLine();
							// Verifica se � o fim do datalhe
							if (FimDoDetalhe(linha))
							{
								break;
							}

							ProcessaTagFileLeitura(ref tagRegistro, linha);
						}

						// Processa o Footer do registro de tag
						// linha = reader.ReadLine();
						ProcessaTagFileRegistroFooter(ref tagRegistro, linha);

						Registros.Add(tagRegistro);
					}

					if (FimDoTexto(linha))
					{
						// Verificar se ser� necess�rio algum tratamento
					}
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				reader.Close();
				reader.Dispose();
			}
		}

		/// <summary>
		/// Verifica se � o fim da linha de datalhe
		/// </summary>
		/// <param name="linha">Linha do arquivo </param>
		/// <returns>Retorna verdadeiro se for o fim do detalhe, caso contrario retorna falso</returns>
		private bool FimDoDetalhe(string linha)
		{
			char[] separador = new char[1] { BYTE_SEPARADOR };

			string[] aux = linha.Split(separador);

			// Verifica se contem uma "barra", caso sim assume que n�o est� mais nas linhas de detalhes
			return aux[0].Contains(@"/");
		}

		/// <summary>
		/// Verifica se � o inicio do texto
		/// </summary>
		/// <param name="linha">Linha de dados do arquivo</param>
		/// <returns>Retorna verdadeiro caso seja inicio do texto, sen�o retorna falso</returns>
		private bool InicioDoTexto(string linha)
		{
            /*
			if (linha[0] == BYTE_STX)
			{
				return true;
			}

            */
			return true;
		}

		/// <summary>
		/// Verifica se � o fim do texto
		/// </summary>
		/// <param name="linha">Linha de dadis do arquivo</param>
		/// <returns>Retorna verdadeiro caso seja o fim do texto, sen�o retorna falso</returns>
		private bool FimDoTexto(string linha)
		{
            /*
			if (linha[0] == BYTE_ETX)
			{
				return true;
			}

             */
			return true;
		}

		/// <summary>
		/// Processa o header do registro do arquivo
		/// </summary>
		/// <param name="tagRegistro">Inst�ncia do objeto registro</param>
		/// <param name="linha">Linha de dados do arquivo</param>
		private void ProcessaTagFileRegistroHeader(ref TagFileRegistro tagRegistro, string linha)
		{
			// Remove o primeiro caracter (STX)
			linha = linha.Remove(0, 1);
			tagRegistro.NumeroPacote = int.Parse(linha);
		}

		/// <summary>
		/// Processa o registro do arquivo
		/// </summary>
		/// <param name="tagRegistro">Inst�ncia do objeto registro</param>
		/// <param name="linha">Linha de dados do arquivo</param>
		private void ProcessaTagFileRegistro(ref TagFileRegistro tagRegistro, string linha)
		{
			DateTime dataHora;
			char[] separador = new char[1] { BYTE_SEPARADOR };

			string[] aux = linha.Split(separador);

			// Encontra a numero da leitura
			tagRegistro.IdLeitor = aux[0];

			// Encontra a data e hora
			string dataAux = aux[1] + " " + aux[2];
			dataHora = DateTime.ParseExact(dataAux, "dd/MM/yy HH:mm:ss", null);
			tagRegistro.DataHoraInicioLeitura = dataHora;

			// Quantidade de Tags lidas
			tagRegistro.QuantidadeTagLido = Int32.Parse(aux[3]);
		}

		/// <summary>
		/// Processa a leitura dos tags
		/// </summary>
		/// <param name="tagRegistro">Inst�ncia do objeto registro</param>
		/// <param name="linha">Linha de dados do arquivo</param>
		private void ProcessaTagFileLeitura(ref TagFileRegistro tagRegistro, string linha)
		{
			char[] separador = new char[1] { BYTE_SEPARADOR };

			string[] aux = linha.Split(separador);
			
			TagFileLeitura tagLeitura    = new TagFileLeitura();
			tagLeitura.IdAntena          = aux[0];
			tagLeitura.QuantidadeLeitura = Int32.Parse(aux[1]);
			tagLeitura.Milesegundos      = Int32.Parse(aux[2]);
			tagLeitura.IdTag             = aux[3];
			
			DateTime dataAux = tagRegistro.DataHoraInicioLeitura;
			dataAux = dataAux.AddMilliseconds(tagLeitura.Milesegundos);
			tagLeitura.DataLeituraTag = dataAux;
			
			tagRegistro.TagsLeitura.Add(tagLeitura);
		}

		/// <summary>
		/// Processa o footer do registro do arquivo
		/// </summary>
		/// <param name="tagRegistro">Inst�ncia do objeto registro</param>
		/// <param name="linha">Linha de dados do arquivo</param>
		private void ProcessaTagFileRegistroFooter(ref TagFileRegistro tagRegistro, string linha)
		{
			DateTime dataHora;
			char[] separador = new char[1] { BYTE_SEPARADOR };

			string[] aux = linha.Split(separador);

			// Encontra a data e hora
			string dataAux = aux[0] + " " + aux[1];
			dataHora = DateTime.ParseExact(dataAux, "dd/MM/yy HH:mm:ss", null);
			tagRegistro.DataHoraFimLeitura = dataHora;

			// Chave do CRC16
			tagRegistro.Crc16 = aux[2];
		}
	}
}