namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Tag
{
	using System;

	/// <summary>
	/// Classe com os dados da leitura do Tag
	/// </summary>
	public class TagFileLeitura
	{
		/// <summary>
		/// Identidicador da antena
		/// </summary>
		public string IdAntena { get; set; }

		/// <summary>
		/// Quantidade de leitura
		/// </summary>
		public int QuantidadeLeitura { get; set; }

		/// <summary>
		/// Identificador do Tag
		/// </summary>
		public string IdTag { get; set; }

		/// <summary>
		/// Milisegundos da leitura do Tag
		/// </summary>
		public int Milesegundos { get; set; }

		/// <summary>
		/// Data de leitura do tag
		/// </summary>
		public DateTime DataLeituraTag { get; set; }
	}
}