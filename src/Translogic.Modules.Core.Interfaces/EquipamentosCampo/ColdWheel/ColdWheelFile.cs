namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;

	/// <summary>
	/// Classe de tratamento dos arquivos de ColdWheel
	/// </summary>
	public class ColdWheelFile
	{
		private readonly char BYTE_SEPARADOR = ':';
		// private readonly char BYTE_SEPARADOR_DETALHE = '\t';

		/// <summary>
		/// Construtortor da classe 
		/// </summary>
		protected ColdWheelFile()
		{
			Registros = new List<ColdWheelFileRegistro>();
		}

		/// <summary>
		/// Propriedade com o nome do arquivo
		/// </summary>
		public string NomeArquivo { get; set; }

		/// <summary>
		/// Lista com os registros do arquivo
		/// </summary>
		public IList<ColdWheelFileRegistro> Registros { get; set; }

		/// <summary>
		/// Cria uma inst�ncia da classe pelo arquivo
		/// </summary>
		/// <param name="nomeArquivo">Nome do arquivo de tag</param>
		/// <returns>Retorna a instancia da classe</returns>
		public static ColdWheelFile CreateFromFile(string nomeArquivo)
		{
			ColdWheelFile coldWheelFile = new ColdWheelFile();
			coldWheelFile.NomeArquivo = nomeArquivo;
			coldWheelFile.ProcessarLeitura();
			coldWheelFile.AjustaTipoVeiculoEixos();
			return coldWheelFile;
		}

		/// <summary>
		/// Cria uma inst�ncia da classe pelo arquivo e tenta gerar o arquivo
		/// </summary>
		/// <param name="conteudo">Nome do arquivo de tag</param>
		/// <returns>Retorna a instancia da classe</returns>
		public static ColdWheelFileRegistro GerarArquivoColdWheel(string conteudo)
		{
			ColdWheelFile coldWheelFile = new ColdWheelFile();
			ColdWheelFileRegistro retorno;
			try
			{
				retorno = coldWheelFile.CriarColdWheelFileRegistroFromString(conteudo);
			}
			catch (Exception)
			{
				retorno = null;
			}

			return retorno;
		}

		/// <summary>
		/// Processa a leitura do arquivo de Tag
		/// </summary>
		private void ProcessarLeitura()
		{
			// string linha;
			StreamReader reader = new StreamReader(NomeArquivo);
			try
			{
				string conteudo = reader.ReadToEnd();
				ColdWheelFileRegistro coldWheelFileRegistro = CriarColdWheelFileRegistroFromString(conteudo);

				// Adiciona o registro do arquivo
				Registros.Add(coldWheelFileRegistro);
			}
			catch (Exception ex)
			{
				throw;
			}
			finally
			{
				reader.Close();
				reader.Dispose();
			}
		}

		private ColdWheelFileRegistro CriarColdWheelFileRegistroFromString(string conteudo)
		{
			ColdWheelFileRegistro coldWheelFileRegistro = new ColdWheelFileRegistro();
			StringReader reader = new StringReader(conteudo);
			string comando = reader.ReadLine();
			if (string.IsNullOrEmpty(comando) || !comando.StartsWith("D"))
			{
				throw new FormatException("Arquivo no formato inv�lido.");
			}

			ProcessarColdWheelFileRegistro(ref coldWheelFileRegistro, ref reader);

			string linha = null;
			// anda no arquivo at� onde come�a os detalhes que precisa ser coletado
			while ((linha = reader.ReadLine()) != null)
			{
				if (linha.StartsWith("Num"))
				{
					linha = reader.ReadLine();
					break;
				}
			}

			ProcessarDetalhes(ref coldWheelFileRegistro, ref reader);

			if (!coldWheelFileRegistro.Eixos.Count.Equals(coldWheelFileRegistro.QuantidadeEixos))
			{
				throw new IndexOutOfRangeException("Quantidade de eixos nos detalhes est� diferente da informada no header.");
			}

			return coldWheelFileRegistro;
		}

		private void ProcessarDetalhes(ref ColdWheelFileRegistro registro, ref StringReader reader)
		{
			// Le os detalhes (leitura dos ColdWheels)
			int contadorEixos = 0;
			int carro = 0;
			int ultimoEixo;
			string linha = null;
			while ((linha = reader.ReadLine()) != null)
			{
				if (!string.IsNullOrEmpty(linha))
				{
					ColdWheelFileEixo coldWheelFileEixo = ProcessarColdWheelFileEixo(linha);
					ultimoEixo = coldWheelFileEixo.Eixo.Value;
					
					if (coldWheelFileEixo.Carro.HasValue)
					{
						contadorEixos = 0;
						carro = coldWheelFileEixo.Carro.Value;
					}
					else
					{
						coldWheelFileEixo.Carro = carro;
					}

					coldWheelFileEixo.Eixo = ++contadorEixos;
					registro.Eixos.Add(coldWheelFileEixo);

					if (ultimoEixo.Equals(registro.QuantidadeEixos))
					{
						break;
					}
				}
			}
		}

		/// <summary>
		/// Processa os detalhes dos Detac (eixo)
        /// Car   Axle   Spacing   Ch1   Ch2   Ch3   Ch4
        /// Num    Num   (   M)    (C)   (C)   (C)   (C )   Alarms
        /// -------------------------------------------------------------- Heat Units = C
        ///   4     11        1.8   XXX   XXX     0     8  CW1  CW2
        ///         12 ^      3.1   XXX   XXX   112   142
        ///         13 ^      1.7   XXX   XXX    99   134
        ///         14 ^     11.1   XXX   XXX   134   164
        ///         15 ^      1.7   XXX   XXX   184   137
        ///         16 ^      2.3   XXX   XXX   238   176
		/// </summary>
		/// <param name="linha">Linha lida do arquivo</param>
		/// <returns> Objeto populado de <see cref="ColdWheelFileEixo"/></returns>
		private ColdWheelFileEixo ProcessarColdWheelFileEixo(string linha)
		{
			string carro = linha.Substring(0, 3).Trim();
			string eixo = linha.Substring(3, 7).Trim(); // Busca do numero do eixo desconsiderando poss�veis flags de alarme
			string temperaturaA = linha.Substring(33, 6).Trim();
			string temperaturaB = linha.Substring(39, 6).Trim();

			ColdWheelFileEixo coldWheelFileEixo = new ColdWheelFileEixo();
			if (!string.IsNullOrEmpty(carro))
			{
                int carroOk;
                coldWheelFileEixo.Carro = Int32.TryParse(carro, out carroOk) ? carroOk : (int?)null;
			}

			if (!string.IsNullOrEmpty(eixo))
			{
			    int eixoOk;
                coldWheelFileEixo.Eixo = Int32.TryParse(eixo, out eixoOk) ? eixoOk : (int?)null;
			}

			if (!string.IsNullOrEmpty(temperaturaA) && temperaturaA != "N/A")
			{
                int temperaturaAOk;
                coldWheelFileEixo.TemperaturaA = Int32.TryParse(temperaturaA, out temperaturaAOk) ? temperaturaAOk : (int?)null;
			}

			if (!string.IsNullOrEmpty(temperaturaB) && temperaturaB != "N/A")
			{
                int temperaturaBOk;
                coldWheelFileEixo.TemperaturaB = Int32.TryParse(temperaturaB, out temperaturaBOk) ? temperaturaBOk : (int?)null;
			}
			
			return coldWheelFileEixo;
		}

		/// <summary>
		/// Processa o registro do ColdWheel file
		/// </summary>
		/// <param name="registro">Registro do arquivo de coldWheel</param>
		/// <param name="texto">Texto do arquivo</param>
		private void ProcessarColdWheelFileRegistro(ref ColdWheelFileRegistro registro, ref StringReader texto)
		{
			bool headerEnd;
			string linha;

			// Le os detalhes (leitura dos ColdWheels)
			while ((linha = texto.ReadLine()) != null)
			{
				if (!string.IsNullOrEmpty(linha))
				{
					headerEnd = ProcessaLinhaHeader(ref registro, linha);
					if (headerEnd)
					{
						break;
					}
				}
			}
		}

		private bool ProcessaLinhaHeader(ref ColdWheelFileRegistro registro, string linha)
		{
			CultureInfo culture = new CultureInfo("en-US");
			string[] valores;
			TipoHeaderEnum tipo = ObterTipoHeader(linha);
			switch (tipo)
			{
				case TipoHeaderEnum.SiteNameSeqNumber:
					valores = ExtrairValores(linha);
					registro.Local = valores[0];
					registro.Sequencia = Int32.Parse(valores[1]);
					break;
				
				case TipoHeaderEnum.Kilometer:
					valores = ExtrairValores(linha);
					registro.Kilometro = float.Parse(valores[0], culture);
					registro.DbIndex = Int32.Parse(valores[1]);
					break;
				
				case TipoHeaderEnum.DirectionArrival:
					valores = ExtrairValores(linha);
					registro.Direcao = valores[0];
					registro.DataHoraInicio = DateTime.ParseExact(valores[1], "HH:mm  MM-dd-yyyy", null);
					break;
				
				case TipoHeaderEnum.AxlesLength:
					valores = ExtrairValores(linha);
					registro.QuantidadeEixos = Int32.Parse(valores[0]);
					registro.ComprimentoTrem = float.Parse(valores[1].Replace("M", string.Empty).Trim(), culture);
					break;
				
				case TipoHeaderEnum.AlarmsCars:
					valores = ExtrairValores(linha);
					registro.QuantidadeAlarmes = Int32.Parse(valores[0]);
					registro.QuantidadeCarros = Int32.Parse(valores[1]);

					break;
				
				case TipoHeaderEnum.IntegFails:
					valores = ExtrairValores(linha);
					registro.QuantidadeFalhasIntegridade = Int32.Parse(valores[0]);
					break;
				
				case TipoHeaderEnum.SystemWarn:
					valores = ExtrairValores(linha);
					registro.QuantidadeAvisosSistema = Int32.Parse(valores[0]);
					break;
				
				case TipoHeaderEnum.HighestCh3:
					return true;
			}

			return false;
		}

		private string[] ExtrairValores(string linha)
		{
			char[] separador = new char[1] { BYTE_SEPARADOR };
			string[] aux = linha.Split(separador);
			string valor1 = aux[1].Substring(1, 27).Trim();
			string valor2 = aux.Length > 3 ? (aux[2].Trim() + ":" + aux[3].Trim()).Trim() : aux[2].Trim();
			
			return new[] { valor1, valor2 };
		}

		private TipoHeaderEnum ObterTipoHeader(string linha)
		{
			if (linha.StartsWith("Site Name"))
			{
				return TipoHeaderEnum.SiteNameSeqNumber;
			}

			if (linha.StartsWith("Kilometer"))
			{
				return TipoHeaderEnum.Kilometer;
			}

			if (linha.StartsWith("Direction"))
			{
				return TipoHeaderEnum.DirectionArrival;
			}

			if (linha.StartsWith("Axles"))
			{
				return TipoHeaderEnum.AxlesLength;
			}

			if (linha.StartsWith("Alarms"))
			{
				return TipoHeaderEnum.AlarmsCars;
			}

			if (linha.StartsWith("Integ Fails"))
			{
				return TipoHeaderEnum.IntegFails;
			}

			if (linha.StartsWith("System Warn"))
			{
				return TipoHeaderEnum.SystemWarn;
			}

			if (linha.StartsWith("Highest Ch3"))
			{
				return TipoHeaderEnum.HighestCh3;
			}

			return TipoHeaderEnum.Invalido;
		}

		/// <summary>
		/// Ajusta os tipos do veiculo por eixos
		/// </summary>
		private void AjustaTipoVeiculoEixos()
		{
			foreach (ColdWheelFileRegistro registro in Registros)
			{
				int marcador = 0;
				int totalEixos = registro.Eixos.Count;
				int index = 0;
				bool locomotiva = false;
				while (index < totalEixos)
				{
					ColdWheelFileEixo eixo = registro.Eixos[index];
					if ((eixo.Eixo == 1) && (marcador > 0))
					{
						// Processar anterior 
						if (locomotiva)
						{
							while (marcador > 0)
							{
								eixo = registro.Eixos[index - marcador];
								eixo.TipoVeiculo = 1; // Tipo Locomotiva
								marcador--;
							}
						}

						marcador = 0;
						locomotiva = false;
						eixo = registro.Eixos[index];
					}

					if (eixo.Eixo > 4)
					{
						locomotiva = true;
					}

					index++;
					marcador++;
				}
			}
		}
	}
}