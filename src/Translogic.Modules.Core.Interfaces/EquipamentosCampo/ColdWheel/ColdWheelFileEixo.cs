namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel
{
	/// <summary>
	/// Classe com as propriedades do eixo do Detac
	/// </summary>
	public class ColdWheelFileEixo
	{
		/// <summary>
		/// o numero do carro
		/// </summary>
		public int? Carro { get; set; }

        /// <summary>
		/// Sequencia dos eixos
		/// </summary>
		public int? Eixo { get; set; }

		/// <summary>
		/// Lado A do eixo
		/// </summary>
		public int? TemperaturaA { get; set; }

		/// <summary>
		/// Lado B do eixo
		/// </summary>
		public int? TemperaturaB { get; set; }

		/// <summary>
		/// Tipo do veiculo detac (0 - Vagao, 1 - Locomotiva)
		/// </summary>
		public int TipoVeiculo { get; set; }
	}
}