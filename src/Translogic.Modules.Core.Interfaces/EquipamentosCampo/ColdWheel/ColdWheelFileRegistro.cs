namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Classe de registro dos ColdWheels
	/// </summary>
	public class ColdWheelFileRegistro
	{
		/// <summary>
		/// Construtor do DetacFileRegistro
		/// </summary>
		public ColdWheelFileRegistro()
		{
			Eixos = new List<ColdWheelFileEixo>();
		}

		/// <summary>
		/// Local em que o equipmaneto est� instalado
		/// </summary>
		public string Local { get; set; }

		/// <summary>
		/// Sequencia da leitura no equipamento
		/// </summary>
		public int Sequencia { get; set; }

		/// <summary>
		/// Kilometro em que o equipamento est� instalado
		/// </summary>
		public float Kilometro { get; set; }

		/// <summary>
		/// �ndice do arquivo para busca no equipamento
		/// </summary>
		public int DbIndex { get; set; }

		/// <summary>
		/// Direcao em que o trem est� indo.
		/// </summary>
		public string Direcao { get; set; }

		/// <summary>
		/// Data e hora de inicio
		/// </summary>
		public DateTime DataHoraInicio { get; set; }

		/// <summary>
		/// Quantidade de Eixos.
		/// </summary>
		public int QuantidadeEixos { get; set; }

		/// <summary>
		/// Comprimento do Trem.
		/// </summary>
		public double ComprimentoTrem { get; set; }

		/// <summary>
		/// Quantidade de Alarmes.
		/// </summary>
		public int QuantidadeAlarmes { get; set; }

		/// <summary>
		/// Quantidade de Carros.
		/// </summary>
		public int QuantidadeCarros { get; set; }

		/// <summary>
		/// Quantidade de falhas de integridade
		/// </summary>
		public int QuantidadeFalhasIntegridade { get; set; }

		/// <summary>
		/// Quantidade de avisos do sistema
		/// </summary>
		public int QuantidadeAvisosSistema { get; set; }

		/// <summary>
		/// Lista com os eixos do Detac
		/// </summary>
		public IList<ColdWheelFileEixo> Eixos { get; set; }
	}
}