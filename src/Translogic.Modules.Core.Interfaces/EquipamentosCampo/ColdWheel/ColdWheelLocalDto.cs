﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel
{
	/// <summary>
	/// Dto de locais de instalação do coldwheel
	/// </summary>
	public class ColdWheelLocalDto
	{
		/// <summary>
		/// Id do Local do ColdWheel.
		/// </summary>
		public int IdLocal { get; set; }

		/// <summary>
		/// Código do Local (estação)
		/// </summary>
		/// <exexample> ZWU </exexample>
		public string CodigoLocal { get; set; }

		/// <summary>
		/// Kilometro de Instalacao.
		/// </summary>
		public float KilometroInstalacao { get; set; }

		/// <summary>
		/// Ip do Equipamento.
		/// </summary>
		public string IpEquipamento { get; set; }

		/// <summary>
		/// Porta do Equipamento.
		/// </summary>
		public int PortaEquipamento { get; set; }
	}
}
