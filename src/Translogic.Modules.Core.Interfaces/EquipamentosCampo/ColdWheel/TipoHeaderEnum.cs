namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.ColdWheel
{
	/// <summary>
	/// Indica o tipo do header
	/// </summary>
	public enum TipoHeaderEnum
	{
		/// <summary>Site Name e sequencia </summary>
		SiteNameSeqNumber,

		/// <summary> Km de instala��o </summary>
		Kilometer,

		/// <summary> Dire��o e data passagem</summary>
		DirectionArrival,

		/// <summary> Qtde Eixos e comprimento </summary>
		AxlesLength,

		/// <summary> Qtde alarmes e carros </summary>
		AlarmsCars,

		/// <summary> Qtde de falhas de integridade </summary>
		IntegFails,

		/// <summary> Qtde avisos do sistema </summary>
		SystemWarn,

		/// <summary> �ltimo do header </summary>
		HighestCh3,

        /// <summary> Velocidade de entrada e sa�da </summary>
        SpeedInOut,

        /// <summary> Menor Velocidade atingida </summary>
        SlowSpeed,

        /// <summary> Temperatura ambiente </summary>
        AmbTemp,

        /// <summary> Final do Header HotBox </summary>
        HbdFilter,
        
		/// <summary> Quando n�o for nenhum que � utilizado </summary>
		Invalido
	}
}