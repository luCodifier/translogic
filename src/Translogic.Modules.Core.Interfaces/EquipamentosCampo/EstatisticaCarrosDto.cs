﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System.Collections.Generic;

    /// <summary>
    /// Dto para gravação das estatísticas e testes realizados com o carro
    /// </summary>
    public class EstatisticaCarrosDto
    {
        /// <summary>
        /// Id da estatística geral do equipamento
        /// </summary>
        public int IdEstatisticaGeral { get; set; }

        /// <summary>
        /// Id do log do processamento
        /// </summary>
        public int IdLogProcessamento { get; set; }

        /// <summary>
        /// Indicador do numero do carro em que se encontram os rodantes
        /// </summary>
        public int NumeroCarro { get; set; }

        /// <summary>
        /// Quantidade de rodantes do Carro
        /// </summary>
        public int QuantidadeDeRodantes { get; set; }

        /// <summary>
        /// Temperatura media dos rodantes do carro
        /// </summary>
        public double TemperaturaMedia { get; set; }

        /// <summary>
        /// Temperatura media desconsiderando a temperatura maxima dos rodantes do carro
        /// </summary>
        public double TemperaturaMediaSemMaxima { get; set; }

        /// <summary>
        /// Desvio padrao das temperaturas do rodante
        /// </summary>
        public double DesvioPadrao { get; set; }

        /// <summary>
        /// Temperatura critica dos rodantes do carro
        /// </summary>
        public double TemperaturaCritica { get; set; }

        /// <summary>
        /// Lista das temperaturas dos rodantes do carro
        /// </summary>
        public List<int> TemperaturasCarro { get; set; }

        /// <summary>
        /// Indica se existe Alarme de freio agarrado = S ou N
        /// </summary>
        public string AlarmeFreioAgarrado { get; set; }

        /// <summary>
        /// Indica se existe Alarme falso = S ou N
        /// </summary>
        public string AlarmeFalso { get; set; }

        /// <summary>
        /// Indica se existe Alarme de rolamento = S ou N
        /// </summary>
        public string AlarmeRolamento { get; set; }

        /// <summary>
        /// Indica se existe Alarme de tendencia = S ou N
        /// </summary>
        public string AlarmeTendencia { get; set; }
    }
}
