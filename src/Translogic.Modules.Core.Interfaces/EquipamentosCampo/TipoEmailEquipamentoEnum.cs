﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    /// <summary>
    /// Enum que demonstra qual o tipo de email deverá ser enviado
    /// </summary>
    public enum TipoEmailEquipamentoEnum
    {
        /// <summary>
        /// Problemas de conexão com o equipamento ou no envio de comando
        /// </summary>
        EquipamentoSemResposta = 1,

        /// <summary>
        /// Equipamento não retornou lista de Trem
        /// </summary>
        EquipamentoSemListagem = 2,

        /// <summary>
        /// Equipamento não retornou os dados detalhados do Trem
        /// </summary>
        EquipamentoSemDetalhesTrem = 3,

        /// <summary>
        /// Ocorreu algum alerta no equipamento
        /// </summary>
        AlertaNoEquipamento = 4
    }
}
