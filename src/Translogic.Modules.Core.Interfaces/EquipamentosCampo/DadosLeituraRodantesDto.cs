﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System;

    /// <summary>
    /// Dto da Leitura dos Rodantes da composição
    /// </summary>
    public class DadosLeituraRodantesDto
    {
        /// <summary>
        /// Log da leitura do equipamento
        /// </summary>
        public int IdDadosLeituraResumo { get; set; }

        /// <summary>
        /// Log da leitura do equipamento
        /// </summary>
        public int IdLogProcessamento { get; set; }

        /// <summary>
        /// Data de registro no Inicio da Leitura do Rodante
        /// </summary>
        public DateTime DataLeituraRodante { get; set; }

        /// <summary>
        /// Numero do carro que este rodante está
        /// </summary>
        public int NumeroCarro { get; set; }

        /// <summary>
        /// Numero do rodante no registro do Equipamento
        /// </summary>
        public int NumeroRodante { get; set; }

        /// <summary>
        /// Distancia entre rodantes registrada pelo HotBox
        /// </summary>
        public decimal DistanciaEntreRodantes { get; set; }

        /// <summary>
        /// Temperatura no Canal 1
        /// </summary>
        public int TemperaturaCh1 { get; set; }

        /// <summary>
        /// Temperatura no Canal 2
        /// </summary>
        public int TemperaturaCh2 { get; set; }

        /// <summary>
        /// Alarmes disparados pelo equipamento para o rodante
        /// </summary>
        public string Alarmes { get; set; }
    }
}
