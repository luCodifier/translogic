﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System;

    /// <summary>
    /// Dto para informar o Inicio e Fim da leitura do Equipamento
    /// </summary>
    public class LogLeituraEquipamentoDto
    {
        /// <summary>
        /// Id do log de leitura do equipamento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Data inicio da leitura do equipamento
        /// </summary>
        public DateTime DataIniLogLeituraEquipamento { get; set; }

        /// <summary>
        /// Data final da leitura do equipamento
        /// </summary>
        public DateTime? DataFimLogLeituraEquipamento { get; set; }

        /// <summary>
        /// Numero do ultimo Trem que passou pelo HotBox
        /// </summary>
        public int? NumeroUltimaPassagemTrem { get; set; }

        /// <summary>
        /// Numero do ultimo Trem que passou pelo HotBox e que foi processado
        /// </summary>
        public int? UltimoTremProcessado { get; set; }

        /// <summary>
        /// Numero do próximo trem a ser processado
        /// </summary>
        public int? ProximoTremAProcessar { get; set; }

        /// <summary>
        /// Status da leitura do equipamento <see cref="StatusLeituraEquipamentoEnum"/>
        /// </summary>
        public StatusLeituraEquipamentoEnum StatusLeituraEquipamento { get; set; }

        /// <summary>
        /// Id do equipamento que está sendo monitorado
        /// </summary>
        public int IdEquipamento { get; set; }

        /// <summary>
        /// Tipo do Equipamento <see cref="TipoEquipamentoEnum"/>
        /// </summary>
        public TipoEquipamentoEnum TipoEquipamento { get; set; }
    }
}
