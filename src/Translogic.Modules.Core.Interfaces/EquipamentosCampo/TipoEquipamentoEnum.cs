﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    /// <summary>
    /// Enum que demonstra a tipo de equipamento instalado na Sb
    /// </summary>
    public enum TipoEquipamentoEnum
    {
        /// <summary>
        /// HotBox instalado na via
        /// </summary>
        HotBox = 1,

        /// <summary>
        /// CooldWheel instalado na via
        /// </summary>
        ColdWheel = 2
    }
}
