﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System;

    /// <summary>
    /// Dto de estatísticas gerais do equipamento
    /// </summary>
    public class EstatisticaGeralDto
    {
        /// <summary>
        /// Id do Log da leitura dos dados resumidos do equipamento
        /// </summary>
        public int IdDadosLeituraResumo { get; set; }

        /// <summary>
        /// Id do Log do processamento
        /// </summary>
        public int IdLogProcessamento { get; set; }

        /// <summary>
        /// Data que gerou a estatística
        /// </summary>
        public DateTime DataGeracaoEstatistica { get; set; }

        /// <summary>
        /// Quantidade total de alarmes que ocorreram na passagem do Trem
        /// </summary>
        public int QuantidadeTotalAlarmes { get; set; }

        /// <summary>
        /// Quantidade de alarmes no Canal 1
        /// </summary>
        public int QuantidadeAlarmesCh1 { get; set; }

        /// <summary>
        /// Quantidade de alarmes no Canal 2
        /// </summary>
        public int QuantidadeAlarmesCh2 { get; set; }

        /// <summary>
        /// Indica se está OK ou NOK
        /// </summary>
        public string StatusAlarmes { get; set; }

        /// <summary>
        /// Temperatura padrão para o equipamento
        /// </summary>
        public decimal TemperaturaPadraoEquipamento { get; set; }

        /// <summary>
        /// Temperatura Crítica Média no Canal 1
        /// </summary>
        public decimal TemperaturaMediaCriticaCh1 { get; set; }

        /// <summary>
        /// Temperatura Crítica Média no Canal 2
        /// </summary>
        public decimal TemperaturaMediaCriticaCh2 { get; set; }

        /// <summary>
        /// Desvio Padrão da Temperatura Canal 1
        /// </summary>
        public decimal TemperaturaDesvioPadraoCh1 { get; set; }

        /// <summary>
        /// Desvio Padrão da Temperatura Canal 2
        /// </summary>
        public decimal TemperaturaDesvioPadraoCh2 { get; set; }

        /// <summary>
        /// Temperatura Crítica Canal 1
        /// </summary>
        public decimal TemperaturaCriticaCh1 { get; set; }

        /// <summary>
        /// Temperatura Crítica Canal 2
        /// </summary>
        public decimal TemperaturaCriticaCh2 { get; set; }

        /// <summary>
        /// Temperatura Máxima Canal 1
        /// </summary>
        public decimal TemperaturaMaximaCh1 { get; set; }

        /// <summary>
        /// Temperatura Máxima Canal 2
        /// </summary>
        public decimal TemperaturaMaximaCh2 { get; set; }

        /// <summary>
        /// Nível Sigma Canal 1
        /// </summary>
        public decimal NivelSigmaCh1 { get; set; }

        /// <summary>
        /// Nível Sigma Canal 2
        /// </summary>
        public decimal NivelSigmaCh2 { get; set; }
    }
}
