namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using ColdWheel;
    using Detac;
    using HotBox;
    using Tag;

    /// <summary>
	/// Interface para comunica��o entre o service e o servi�o windows
	/// </summary>
	[ServiceContract]
	public interface IEquipamentosCampoService
	{
		/// <summary>
		/// M�todo chamado para processar o arquivo de Tag
		/// </summary>
		/// <param name="tagFile">Inst�ncia do objeto tag</param>
		[OperationContract]
		void ProcessarArquivoTag(TagFile tagFile);

		/// <summary>
		/// M�todo chamado para processar o aquivo de Detac
		/// </summary>
		/// <param name="detacFile">Inst�ncia do objeto detac</param>
		[OperationContract]
		void ProcessarArquivoDetac(DetacFile detacFile);

		/// <summary>
		/// M�todo chamado para processar o aquivo de ColdWheel
		/// </summary>
		/// <param name="coldWheelFile">Inst�ncia do objeto ColdWheelFile</param>
		[OperationContract]
		void ProcessarArquivoColdWheel(ColdWheelFile coldWheelFile);

		/// <summary>
		/// Obt�m os locais em que o ColdWheel esta instalado
		/// </summary>
		/// <returns> Lista de DTOs de local </returns>
		[OperationContract]
        IList<ColdWheelLocalDto> ObterLocaisColdWheel();

	    /// <summary>
	    /// Obt�m os locais em que o HotBox esta instalado
	    /// </summary>
        /// <returns> Lista de DTOs de local </returns>
        [OperationContract]
	    IList<HotBoxLocalDto> ObterLocaisHotBox();

        /// <summary>
        /// Obt�m um HotBoxLocal pelo Id
        /// </summary>
        /// <param name="idHotBoxLocal">Id do hotbox para ser monitorado</param>
        /// <returns>Dto com informa��es</returns>
        [OperationContract]
	    HotBoxLocalDto ObterHotBoxLocalPorId(int idHotBoxLocal);

        /// <summary>
        /// Grava um log da leitura do equipamento
        /// </summary>
        /// <param name="logLeituraEquipamentoDto">Informa��es lidas do equipamento para leitura</param>
        /// <returns>Id do Log da leitura do equipamento gravado</returns>
        [OperationContract]
	    int GravarLogLeituraEquipamento(LogLeituraEquipamentoDto logLeituraEquipamentoDto);

        /// <summary>
        /// Insere um log do processamento a cada altera��o de status do processamento
        /// </summary>
        /// <param name="logProcessamentoEquipamentoDto">O dto com as informa��es a serem gravadas</param>
        /// <returns>Id do log do processamento persistido</returns>
        [OperationContract]
        int GravarLogProcessamento(LogProcessamentoEquipamentoDto logProcessamentoEquipamentoDto);

        /// <summary>
        /// Gravar informa��es resumidas da leitura
        /// </summary>
        /// <param name="dadosLeituraResumoDto">Dto com os dados a serem persistidos</param>
        /// <returns>Id do Model persistido</returns>
	    [OperationContract]
	    int GravarDadosLeituraResumo(DadosLeituraResumoDto dadosLeituraResumoDto);

        /// <summary>
        /// Grava um log da leitura do rodante
        /// </summary>
        /// <param name="dadosLeituraRodantesDto">Informa��es lidas do equipamento para leitura</param>
        /// <returns>Id do Dado de leitura de rodante persistido</returns>
        [OperationContract]
        int GravarDadosLeituraRodantes(DadosLeituraRodantesDto dadosLeituraRodantesDto);

        /// <summary>
        /// Gravar informa��es estatisticas gerais
        /// </summary>
        /// <param name="estatisticaGeralDto">Dto com os dados a serem persistidos</param>
        /// <returns>Id do Model persistido</returns>
	    [OperationContract]
	    int GravarEstatisticaGeral(EstatisticaGeralDto estatisticaGeralDto);

        /// <summary>
        /// Gravar informa��es testes baseados em estatisticas gerais
        /// </summary>
        /// <param name="estatisticaGeralTestesDto">Dto com os dados a serem persistidos</param>
        /// <returns>Id do Model persistido</returns>
	    [OperationContract]
	    int GravarEstatisticaGeralTestes(EstatisticaGeralTestesDto estatisticaGeralTestesDto);

        /// <summary>
        /// Gravar informa��es testes dos carros (Vag�es, trens, locomotivas)
        /// </summary>
        /// <param name="estatisticaCarrosDto">Dto com os dados a serem persistidos</param>
        /// <returns>Id do Model persistido</returns>
	    [OperationContract]
	    int GravarEstatisticaCarros(EstatisticaCarrosDto estatisticaCarrosDto);

        /// <summary>
        /// Ao finalizar o processamento dos rodantes atualiza o log de processamento do equipamento
        /// </summary>
        /// <param name="logProcessamentoEquipamentoDto">Dto com informa��es atualizadas</param>
        [OperationContract]
        void AtualizarLogProcessamentoEquipamento(LogProcessamentoEquipamentoDto logProcessamentoEquipamentoDto);

        /// <summary>
        /// Atualizar todos os logs de processamento inserindo o Id de dados de leitura
        /// </summary>
        /// <param name="idLogLeituraEquipamento">Id do log de leitura do qual ser�o atualizados os logs de processamento</param>
        /// <param name="idDadosLeitura">Id dos dados de leitura</param>
        [OperationContract]
        void AtualizarLogsIdDadosLeitura(int idLogLeituraEquipamento, int idDadosLeitura);

        /// <summary>
        /// Atualiza��es no status da leitura do equipamento
        /// </summary>
        /// <param name="logLeituraEquipamentoDto">Dto com informa��es atualizadas</param>
        [OperationContract]
        void AtualizarLogLeituraEquipamento(LogLeituraEquipamentoDto logLeituraEquipamentoDto);

        /// <summary>
        /// Obt�m a �ltima leitura do equipamento que foi processada
        /// </summary>
        /// <param name="idEquipamento">Id do equipamento que se deseja obter a ultima leitura</param>
        /// <returns>Log da leitura do ultimo registro no equipamento</returns>
        [OperationContract]
	    LogLeituraEquipamentoDto ObterUltmimoTremProcessado(int idEquipamento);

        /// <summary>
        /// Envia um email de acordo com os par�metros informados
        /// </summary>
        /// <param name="mailMessageDto">Dto com as informa��es para envio do Email</param>
	    [OperationContract]
	    void EnviarEmailEquipamento(MailMessageDto mailMessageDto);
	}
}