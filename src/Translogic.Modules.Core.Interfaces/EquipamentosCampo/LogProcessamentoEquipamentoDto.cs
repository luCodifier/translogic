﻿namespace Translogic.Modules.Core.Interfaces.EquipamentosCampo
{
    using System;

    /// <summary>
    /// Dto para Transferência de dados de processamento
    /// </summary>
    public class LogProcessamentoEquipamentoDto
    {
        /// <summary>
        /// Id do log de leitura do equipamento
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id da leitura resumida (Cabeçalho) realizada no equipamento para determinado Trem
        /// </summary>
        public int? IdDadosLeituraResumo { get; set; }

        /// <summary>
        /// Id do log da leitura do equipamento
        /// </summary>
        public int IdLogLeituraEquipamento { get; set; }

        /// <summary>
        /// Data de incio do processamento do equipamento
        /// </summary>
        public DateTime DataInicioProcessamento { get; set; }

        /// <summary>
        /// Data de fim do processamento do equipamento
        /// </summary>
        public DateTime? DataFimProcessamento { get; set; }

        /// <summary>
        /// Status do processamento atual
        /// </summary>
        public StatusLogProcessamentoEnum StatusProcessamento { get; set; }

        /// <summary>
        /// Mensagem, se ocorrer erro grava a Exception
        /// </summary>
        public string MensagemProcessamento { get; set; }

        /// <summary>
        /// Equipamento que est sendo conectado para obter as informaes
        /// </summary>
        public int? IdEquipamento { get; set; }
    }
}
