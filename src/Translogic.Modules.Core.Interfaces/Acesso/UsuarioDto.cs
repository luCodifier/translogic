﻿namespace Translogic.Modules.Core.Interfaces.Acesso
{
    /// <summary>
    /// DTO para Usuario
    /// </summary>
    public class UsuarioDto
    {
        /// <summary>
        /// Id do Usuário.
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Código do Usuário.
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Nome completo do usuário.
        /// </summary>
        public virtual string Nome { get; set; }
    }
}