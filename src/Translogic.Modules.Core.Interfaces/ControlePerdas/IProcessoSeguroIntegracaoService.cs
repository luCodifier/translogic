﻿namespace Translogic.Modules.Core.Interfaces.ControlePerdas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.Text;

    /// <summary>
    /// Contrato de Serviço para Integração com Controle de Perdas 
    /// </summary>
    [ServiceContract(Name = "ControlePerdas", Namespace = "http://www.all-logistica.com/translogic/controleperdas")]
    public interface IProcessoSeguroIntegracaoService
    {
        /// <summary>
        /// Método para geração de Processo de Seguro e respectivo dossiê
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a geração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GerarProcesso", ResponseFormat = WebMessageFormat.Json)]
        ProcessoSeguroIntegracaoResponse GerarProcesso(ProcessoSeguroIntegracaoRequest payload);

        /// <summary>
        /// Método para Atualizar de Processo de Seguro e respectivo dossiê
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Alteração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AtualizarProcesso", ResponseFormat = WebMessageFormat.Json)]
        ProcessoSeguroIntegracaoResponse AtualizarProcesso(ProcessoSeguroIntegracaoRequest payload);

        /// <summary>
        /// Método para Atualizar o TFA após o Aceite do TFA pelo usuário no CAALL
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Alteração de Processo de Seguro</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AtualizarDocumentoTfa", ResponseFormat = WebMessageFormat.Json)]
        AtualizarDocumentoTfaDto AtualizarDocumentoTfa(AtualizarDocumentoTfaDto payload);

         /// <summary>
        /// Método para Atualizar o Dossiê
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Atualização do Dossiê</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AtualizarDossie", ResponseFormat = WebMessageFormat.Json)]
        AtualizarDocumentoTfaDto AtualizarDossie(AtualizarDocumentoTfaDto payload);

        /// <summary>
        /// Cria o TFA automaticamente, de acordo com o número do Processo informado
        /// </summary>
        /// <param name="payload">Contrato com os parâmetros necessários para a Criação do Dossiê</param>
        /// <returns>Retorna o contrato com o status da operação (Sucesso ou Falha) e respectivos detalhes</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CriarTfaProcesso", ResponseFormat = WebMessageFormat.Json)]
        AtualizarDocumentoTfaDto CriarTfaProcesso(AtualizarDocumentoTfaDto payload);
    }
}
