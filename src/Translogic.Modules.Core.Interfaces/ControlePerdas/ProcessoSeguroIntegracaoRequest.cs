﻿namespace Translogic.Modules.Core.Interfaces.ControlePerdas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Classe de Parâmetros para criação de Processo de Seguro e TFA
    /// </summary>
    [DataContract(Namespace = "http://www.all-logistica.com/translogic/controleperdas")]
    public class ProcessoSeguroIntegracaoRequest
    {
        /// <summary>
        /// ID do TFA divergente, caso ModoEdicao = true e ProcessoGerado = false
        /// </summary>
        /// <value>ID do TFA divergente, caso ModoEdicao = true e ProcessoGerado = false</value>
        [DataMember(Name = "IdTfaCache", EmitDefaultValue = false)]
        public int? IdTfaCache { get; set; }

        /// <summary>
        /// Número do Vagão
        /// </summary>
        /// <value>Número do Vagão</value>
        [DataMember(Name = "NumeroVagao", EmitDefaultValue = false)]
        public string NumeroVagao { get; set; }

        /// <summary>
        /// Número do Despacho
        /// </summary>
        /// <value>Número do Despacho</value>
        [DataMember(Name = "NumeroDespacho", EmitDefaultValue = false)]
        public int? NumeroDespacho { get; set; }

        /// <summary>
        /// Serie do Despacho
        /// </summary>
        /// <value>Serie do Despacho</value>
        [DataMember(Name = "SerieDespacho", EmitDefaultValue = false)]
        public string SerieDespacho { get; set; }

        /// <summary>
        /// Peso na Origem
        /// </summary>
        /// <value>Peso na Origem</value>
        [DataMember(Name = "PesoOrigem", EmitDefaultValue = false)]
        public double? PesoOrigem { get; set; }

        /// <summary>
        /// Peso no Destino
        /// </summary>
        /// <value>Peso no Destino</value>
        [DataMember(Name = "PesoDestino", EmitDefaultValue = false)]
        public double? PesoDestino { get; set; }

        /// <summary>
        /// ID do Local de Vistoria
        /// </summary>
        /// <value>ID do Local de Vistoria</value>
        [DataMember(Name = "IdLocalVistoria", EmitDefaultValue = false)]
        public int? IdLocalVistoria { get; set; }

        /// <summary>
        /// ID da Causa do Dano
        /// </summary>
        /// <value>ID da Causa do Dano</value>
        [DataMember(Name = "IdCausaDano", EmitDefaultValue = false)]
        public int? IdCausaDano { get; set; }

        /// <summary>
        /// Código do Tipo de Lacre (1 - Lacres OK, 2 - Lacre Parcial, 3 - Lacres Ausentes)
        /// </summary>
        /// <value>Código do Tipo de Lacre (1 - Lacres OK, 2 - Lacre Parcial, 3 - Lacres Ausentes)</value>
        [DataMember(Name = "CodTipoLacre", EmitDefaultValue = false)]
        public int? CodTipoLacre { get; set; }

        /// <summary>
        /// Código do Tipo de Gambito (1 - Gambitos OK, 2 - Gambitos Parcial, 3 - Gambitos Ausentes)
        /// </summary>
        /// <value>Código do Tipo de Gambito (1 - Gambitos OK, 2 - Gambitos Parcial, 3 - Gambitos Ausentes)</value>
        [DataMember(Name = "CodTipoGambito", EmitDefaultValue = false)]
        public int? CodTipoGambito { get; set; }

        /// <summary>
        /// Indicador se o Processo é 1 - Devido , 2 - Indevido, 3 - Cancelado
        /// </summary>
        [DataMember(Name = "CodTipoConclusaoTfa", EmitDefaultValue = false)]
        public int? CodTipoConclusaoTfa { get; set; }
        
        /// <summary>
        /// Considerações Adicionais
        /// </summary>
        /// <value>Considerações Adicionais</value>
        [DataMember(Name = "ConsideracoesAdicionais", EmitDefaultValue = false)]
        public string ConsideracoesAdicionais { get; set; }

        /// <summary>
        /// Nome do Representante do Cliente
        /// </summary>
        /// <value>Nome do Representante do Cliente</value>
        [DataMember(Name = "NomeRepresentante", EmitDefaultValue = false)]
        public string NomeRepresentante { get; set; }

        /// <summary>
        /// Número do Documento do Representante do Cliente
        /// </summary>
        /// <value>Número do Documento do Representante do Cliente</value>
        [DataMember(Name = "DocumentoRepresentante", EmitDefaultValue = false)]
        public string DocumentoRepresentante { get; set; }

        /// <summary>
        /// Imagem (base64) da Assinatura do Representante do Cliente
        /// </summary>
        /// <value>Imagem (base64) da Assinatura do Representante do Cliente</value>
        [DataMember(Name = "ImagemAssinaturaRepresentante", EmitDefaultValue = false)]
        public string ImagemAssinaturaRepresentante { get; set; }

        /// <summary>
        /// Indica se o Responsável está ciente sobre o TFA (assinou no aplicativo)
        /// </summary>
        /// <value>Indica se o Responsável está ciente sobre o TFA (assinou no aplicativo)</value>
        [DataMember(Name = "ResponsavelCiente", EmitDefaultValue = false)]
        public bool? ResponsavelCiente { get; set; }

        /// <summary>
        /// Data e Hora da ocorrência do Sinistro em format string
        /// </summary>
        /// <value>Data e Hora da ocorrência do Sinistro</value>
        [DataMember(Name = "DataHoraSinistro", EmitDefaultValue = false)]
        public string DataHoraSinistro { get; set; }

        /// <summary>
        /// Data e Hora da realização da Vistoria em formato String
        /// </summary>
        /// <value>Data e Hora da realização da Vistoria</value>
        [DataMember(Name = "DataHoraVistoria", EmitDefaultValue = false)]
        public string DataHoraVistoria { get; set; }

        /// <summary>
        /// Matrícula do Vistoriador
        /// </summary>
        /// <value>Matrícula do Vistoriador</value>
        [DataMember(Name = "MatriculaVistoriador", EmitDefaultValue = false)]
        public string MatriculaVistoriador { get; set; }

        /// <summary>
        /// Nome do Vistoriador
        /// </summary>
        /// <value>Nome do Vistoriador</value>
        [DataMember(Name = "NomeVistoriador", EmitDefaultValue = false)]
        public string NomeVistoriador { get; set; }

        /// <summary>
        /// Indica se a Edição do Número de Despacho e Vagão foi feita manualmente
        /// </summary>
        /// <value>Indica se a Edição do Número de Despacho e Vagão foi feita manualmente</value>
        [DataMember(Name = "EdicaoManual", EmitDefaultValue = false)]
        public bool? EdicaoManual { get; set; }

        /// <summary>
        /// Código do Tipo de Liberador da Descarga (1 - Terminal, 2 - Cliente, 3 - Rumo)
        /// </summary>
        [DataMember(Name = "CodTipoLiberadorDescarga", EmitDefaultValue = false)]
        public virtual int? CodTipoLiberadorDescarga { get; set; }

        /// <summary>
        ///   Número da Sindicância
        /// </summary>
        /// <value>Número de Sindicância</value>
        [DataMember(Name = "Sindicancia", EmitDefaultValue = false)]
        public virtual long? Sindicancia { get; set; }
        
        /// <summary>
        /// Indica se o contrato de serviço está sendo enviado em modo de Edição e o Processo já foi gerado
        /// </summary>
        /// <value>Indica se o contrato de serviço está sendo enviado em modo de Edição e o Processo já foi gerado</value>
        [DataMember(Name = "ProcessoGerado", EmitDefaultValue = false)]
        public bool? ProcessoGerado { get; set; }

        /// <summary>
        /// Número do Processo à ser alterado, caso ModoEdicao e ProcessoGerado = true 
        /// </summary>
        /// <value>Número do Processo à ser alterado, caso ModoEdicao e ProcessoGerado = true </value>
        [DataMember(Name = "NumeroProcesso", EmitDefaultValue = false)]
        public int? NumeroProcesso { get; set; }

        /// <summary>
        /// Indica se o contrato de serviço está sendo enviado em modo de Edição
        /// </summary>
        /// <value>Indica se o contrato de serviço está sendo enviado em modo de Edição</value>
        [DataMember(Name = "ModoEdicao", EmitDefaultValue = false)]
        public bool? ModoEdicao { get; set; }

        /// <summary>
        /// Justificativa da Alteração do TFA/Processo Seguro, caso em modo de Edição
        /// </summary>
        /// <value>Justificativa da Alteração do TFA/Processo Seguro, caso em modo de Edição</value>
        [DataMember(Name = "Justificativa", EmitDefaultValue = false)]
        public string Justificativa { get; set; }

        /// <summary>
        /// Sobrescreve o método Object.ToString()
        /// </summary>
        /// <returns>retorna a string do objeto</returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();

            str.AppendFormat("IdTfaCache: {0}{1}", this.IdTfaCache, Environment.NewLine);
            str.AppendFormat("NumeroVagao: {0}{1}", this.NumeroVagao, Environment.NewLine);
            str.AppendFormat("NumeroDespacho: {0}{1}", this.NumeroDespacho, Environment.NewLine);
            str.AppendFormat("SerieDespacho: {0}{1}", this.SerieDespacho, Environment.NewLine);
            str.AppendFormat("PesoOrigem: {0}{1}", this.PesoOrigem, Environment.NewLine);
            str.AppendFormat("PesoDestino: {0}{1}", this.PesoDestino, Environment.NewLine);
            str.AppendFormat("IdLocalVistoria: {0}{1}", this.IdLocalVistoria, Environment.NewLine);
            str.AppendFormat("IdCausaDano: {0}{1}", this.IdCausaDano, Environment.NewLine);
            str.AppendFormat("CodTipoLacre: {0}{1}", this.CodTipoLacre, Environment.NewLine);
            str.AppendFormat("CodTipoGambito: {0}{1}", this.CodTipoGambito, Environment.NewLine);
            str.AppendFormat("CodTipoConclusaoTfa: {0}{1}", this.CodTipoConclusaoTfa, Environment.NewLine);
            str.AppendFormat("ConsideracoesAdicionais: {0}{1}", this.ConsideracoesAdicionais, Environment.NewLine);
            str.AppendFormat("NomeRepresentante: {0}{1}", this.NomeRepresentante, Environment.NewLine);
            str.AppendFormat("DocumentoRepresentante: {0}{1}", this.DocumentoRepresentante, Environment.NewLine);
            str.AppendFormat("ResponsavelCiente: {0}{1}", this.ResponsavelCiente, Environment.NewLine);
            str.AppendFormat("DataHoraSinistro: {0}{1}", this.DataHoraSinistro, Environment.NewLine);
            str.AppendFormat("DataHoraVistoria: {0}{1}", this.DataHoraVistoria, Environment.NewLine);
            str.AppendFormat("MatriculaVisto: {0}{1}", this.MatriculaVistoriador, Environment.NewLine);
            str.AppendFormat("MatriculaVistoriador: {0}{1}", this.NomeVistoriador, Environment.NewLine);
            str.AppendFormat("EdicaoManual: {0}{1}", this.EdicaoManual, Environment.NewLine);
            str.AppendFormat("CodTipoLiberadorDescarga: {0}{1}", this.CodTipoLiberadorDescarga, Environment.NewLine);
            str.AppendFormat("Sindicancia: {0}{1}", this.Sindicancia, Environment.NewLine);
            str.AppendFormat("ProcessoGerado: {0}{1}", this.ProcessoGerado, Environment.NewLine);
            str.AppendFormat("NumeroProcesso: {0}{1}", this.NumeroProcesso, Environment.NewLine);
            str.AppendFormat("ModoEdicao: {0}{1}", this.ModoEdicao, Environment.NewLine);
            str.AppendFormat("Justificativa: {0}{1}", this.Justificativa, Environment.NewLine);

            return str.ToString();
        }
    }
}
