﻿namespace Translogic.Modules.Core.Interfaces.ControlePerdas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Classe de Parâmetros para atualização de documento TFA
    /// </summary>
    [DataContract(Namespace = "http://www.all-logistica.com/translogic/controleperdas")]
    public class AtualizarDocumentoTfaDto
    {
        /// <summary>
        /// Número do Processo do TFA à ser atualizado
        /// </summary>
        /// <value>Número do Processo do TFA à ser atualizado</value>
        [DataMember(Name = "NumeroProcesso", EmitDefaultValue = false)]
        public int? NumeroProcesso { get; set; }

        /// <summary>
        /// Indica se o serviço deverá enviar e-mail de notificação de atualização do TFA
        /// </summary>
        /// <value>Indica se o serviço deverá enviar e-mail de notificação de atualização do TFA</value>
        [DataMember(Name = "EnviarEmail", EmitDefaultValue = false)]
        public bool? EnviarEmail { get; set; }

        /// <summary>
        /// Retorna atualização do TFA ocorreu com sucesso
        /// </summary>
        /// <value>Retorna atualização do TFA ocorreu com sucesso</value>
        [DataMember(Name = "TfaAtualizado", EmitDefaultValue = false)]
        public bool? TfaAtualizado { get; set; }

        /// <summary>
        /// Retorna a lista de erro(s) ocorridos durante a tentativa de Geração do Processo de Seguro
        /// </summary>
        /// <value>Retorna a lista de erro(s) ocorridos durante a tentativa de Geração do Processo de Seguro</value>
        [DataMember(Name = "ListaErros", EmitDefaultValue = false)]
        public List<string> ListaErros { get; set; }
    }
}
