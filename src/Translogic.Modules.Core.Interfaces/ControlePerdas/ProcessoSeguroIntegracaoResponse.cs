﻿namespace Translogic.Modules.Core.Interfaces.ControlePerdas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Objeto retorno na Geração de Processos de Seguro no módulo Controle de Perdas do Translogic
    /// </summary>
    [DataContract(Namespace = "http://www.all-logistica.com/translogic/controleperdas")]
    public partial class ProcessoSeguroIntegracaoResponse 
    {
        /// <summary>
        /// Retorna true caso o Processo de Seguro tenha sido gerado com sucesso
        /// </summary>
        /// <value>Retorna true caso o Processo de Seguro tenha sido gerado com sucesso</value>
        [DataMember(Name = "ProcessoGerado", EmitDefaultValue = false)]
        public bool? ProcessoGerado { get; set; }

        /// <summary>
        /// Retorna o Número do Processo, caso o Processo de Seguro tenha sido gerado com sucesso
        /// </summary>
        /// <value>Retorna o Número do Processo, caso o Processo de Seguro tenha sido gerado com sucesso</value>
        [DataMember(Name = "NumeroProcesso", EmitDefaultValue = false)]
        public int? NumeroProcesso { get; set; }

        /// <summary>
        /// Retorna o Nome do Cliente, caso o Processo de Seguro tenha sido gerado com sucesso
        /// </summary>
        /// <value>Retorna o Nome do Cliente, caso o Processo de Seguro tenha sido gerado com sucesso</value>
        [DataMember(Name = "NomeCliente", EmitDefaultValue = false)]
        public string NomeCliente { get; set; }

        /// <summary>
        /// Retorna o ID do TFA Divergente, quando ocorre erro de validação de dados
        /// </summary>
        /// <value>Retorna o ID do TFA Divergente, quando ocorre erro de validação de dados</value>
        [DataMember(Name = "IdTfaCache", EmitDefaultValue = false)]
        public int? IdTfaCache { get; set; }

        /// <summary>
        /// Código da Mercadoria
        /// </summary>
        /// <value>Código da Mercadoria</value>
        [DataMember(Name = "CodMercadoria", EmitDefaultValue = false)]
        public string CodMercadoria { get; set; }

        /// <summary>
        /// Retorna a lista de erro(s) ocorridos durante a tentativa de Geração do Processo de Seguro
        /// </summary>
        /// <value>Retorna a lista de erro(s) ocorridos durante a tentativa de Geração do Processo de Seguro</value>
        [DataMember(Name = "ListaErros", EmitDefaultValue = false)]
        public List<string> ListaErros { get; set; }
    }
}
