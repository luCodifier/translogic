﻿namespace Translogic.Modules.Core.Interfaces.Sap.CriacaoNotas
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;

	/// <summary>
	/// Classe de retorno de criação de notas do SAP
	/// </summary>
	public class RetornoCriarNotaSap
	{
		/// <summary>
		/// Erro indica se a chamada tem erro.
		/// </summary>
		public bool Erro { get; set; }

		/// <summary>
		/// Mensagem de erro ou sucesso.
		/// </summary>
		public string Mensagem { get; set; }
	}
}
