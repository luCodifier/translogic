namespace Translogic.Modules.Core.Interfaces.Trem.OnTime
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Situacao do Trem para o OnTime
    /// </summary>
    public enum SituacaoTrem
    {
        /// <summary> Situa��o de Cancelado </summary>
        [Description("CND")]
        Cancelado,

        /// <summary> Situa��o em Circulando </summary>
        [Description("CLO")]
        Circulando,

        /// <summary> Situa��o Encerrado </summary>
        [Description("ENC")]
        Encerrado,

        /// <summary> Situa��o de Indefinido </summary>
        [Description("IND")]
        Indefinido,

        /// <summary> Situa��o de Programado </summary>
        [Description("PGD")]
        Programado,

        /// <summary> Situa��o de Suprimido </summary>
        [Description("SPD")]
        Suprimido,

        /// <summary> Situa��o de PartidaTranslogic </summary>
        [Description("PTL")]
        PartidaTranslogic
    }

    /// <summary>
    /// Situa��o da ordem de servi�o para OnTime
    /// </summary>
    public enum SituacaoOS
    {
        /// <summary>
        /// Situa��o n�o configurada
        /// </summary>
        Indefinido = 0,

        /// <summary>
        /// Ordem de servi�o programada para partida
        /// </summary>
        Programada = 'P',

        /// <summary>
        /// Ordem de servi�o suprimida
        /// </summary>
        Suprimida = 'S',

        /// <summary>
        /// Ordem de servi�o cancelada no sistema
        /// </summary>
        Cancelada = 'C'
    }

    /// <summary>
    /// Representa uma Ordem de Servi�o de um Pedido
    /// </summary>
    public class PartidaTremOnTime
    {
        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Se o registro de OnTime foi alterado no Translogic
        /// </summary>
        public bool AlteradoTranslogic { get; set; }

        /// <summary>
        /// N�mero da OS
        /// </summary>
        public long Numero { get; set; }

        /// <summary>
        /// Prefixo do Trem
        /// </summary>
        public string Prefixo { get; set; }

        /// <summary>
        /// Area Operacional de Origem
        /// </summary>
        public string AreaOperacionalOrigem { get; set; }

        /// <summary>
        /// Unidade de Producao de Origem
        /// </summary>
        public string UnidadeProducaoOrigem { get; set; }

        /// <summary>
        /// Area Operacional de Destino
        /// </summary>
        public string AreaOperacionalDestino { get; set; }

        /// <summary>
        /// Unidade de Producao de Destino
        /// </summary>
        public string UnidadeProducaoDestino { get; set; }

        /// <summary>
        /// Data de libera��o do Trem
        /// </summary>
        public virtual DateTime? DataLiberacaoTrem { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista
        /// </summary>
        public virtual DateTime? DataPartidaPrevista { get; set; }

        /// <summary>
        /// Data de confirma��o da chegada prevista
        /// </summary>
        public virtual DateTime? DataChegadaPrevista { get; set; }

        /// <summary>
        /// Data em que a OS foi Suprimida
        /// </summary>
        public virtual DateTime? DataSupressao { get; set; }

        /// <summary>
        /// Data em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Matricula no cadastro
        /// </summary>
        public string MatriculaCadastro { get; set; }

        /// <summary>
        /// Data Partida em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime DataPartidaCadastro { get; set; }

        /// <summary>
        /// Data em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime? DataUltimaAlteracao { get; set; }

        /// <summary>
        /// Matricula no cadastro
        /// </summary>
        public string MatriculaUltimaAlteracao { get; set; }

        /// <summary>
        /// Data Partida em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime? DataPartidaUltimaAlteracao { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista anterior a editada
        /// </summary>
        public virtual DateTime? DataPartidaAnterior { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista grade
        /// </summary>
        public virtual DateTime? DataPartidaPrevistaGrade { get; set; }

        /// <summary>
        /// Data de edi��o da OS
        /// </summary>
        public virtual DateTime? DataEdicaoPartidaAnterior { get; set; }

        /// <summary>
        /// Situa��o do trem - <see cref="SituacaoTremOnTimeEnum"/>
        /// </summary>
        public SituacaoTrem Situacao { get; set; }

        /// <summary>
        /// Situa��o da ordem de servi�o - <see cref="SituacaoOSOnTimeEnum"/>
        /// </summary>
        public SituacaoOS SituacaoOS { get; set; }

        /// <summary>
        /// OS Cancelada pela tela 371
        /// </summary>
        public string CanceladoTela371 { get; set; }

        /// <summary>
        /// OS Suprimida pela tela 433
        /// </summary>
        public string SuprimidoTela433 { get; set; }

        /// <summary>
        /// Data chegada oficial do Trem
        /// </summary>
        public DateTime? DataChegadaOficial { get; set; }

        /// <summary>
        /// Data chegada real do Trem
        /// </summary>
        public DateTime? DataChegadaReal { get; set; }

        /// <summary>
        /// Atraso de chegada
        /// </summary>
        public TimeSpan? AtrasoChegada
        {
            get
            {
                if (DataChegadaOficial.HasValue && DataChegadaReal.HasValue)
                {
                    var ts = DataChegadaReal.Value - DataChegadaOficial.Value;
                    if (ts.TotalMinutes > 0)
                    {
                        return ts;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Data de partida oficial do Trem
        /// </summary>
        public DateTime? DataPartidaOficial { get; set; }

        /// <summary>
        /// Data de partida real do Trem
        /// </summary>
        public DateTime? DataPartidaReal { get; set; }

        /// <summary>
        /// Atraso de chegada
        /// </summary>
        public TimeSpan? AtrasoPartida
        {
            get
            {
                if (DataPartidaOficial.HasValue && DataPartidaReal.HasValue)
                {
                    var ts = DataPartidaReal.Value - DataPartidaOficial.Value;
                    if (ts.TotalMinutes > 0)
                    {
                        return ts;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Data de Partida do Trem no Translogic (Primeira movimenta��o)
        /// </summary>
        public DateTime? DataPartidaTranslogic { get; set; }

        /// <summary>
        /// Permitir Alteracao OnTime
        /// </summary>
        public bool PermitirAlteracaoOnTime { get; set; }
    }
}