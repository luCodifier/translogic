﻿namespace Translogic.Modules.Core.Interfaces.Trem.OnTime
{
    using System.Collections.Generic;

    /// <summary>
    /// Dados do OnTime
    /// </summary>
    public class DadosOnTime
    {
        /// <summary>
        /// Sucesso caso consulta ocorra normalmente
        /// </summary>
        public bool Sucesso { get; set; }

        /// <summary>
        /// Detalhes caso haja falha
        /// </summary>
        public string Detalhes { get; set; }

        /// <summary>
        /// Dados de indicadores de OnTime
        /// </summary>
        public IndicadoresOnTime Indicadores { get; set; }

        /// <summary>
        /// Partidas de trens OnTime
        /// </summary>
        public List<PartidaTremOnTime> PartidasTrens { get; set; }

        /// <summary>
        /// Dados de Performance OnTime
        /// </summary>
        public List<UpPerformanceOnTime> Performances { get; set; }
    }
}
