﻿namespace Translogic.Modules.Core.Interfaces.Trem.OnTime
{
    /// <summary>
    /// Relacao de Ups para performance
    /// </summary>
    public class UpPerformanceOnTime
    {
        /// <summary>
        /// Unidade de producao
        /// </summary>
        public string UP { get; set; }

        /// <summary>
        /// Partida horario
        /// </summary>
        public int PartidaHorario { get; set; }

        /// <summary>
        /// Partida de trem
        /// </summary>
        public int Partida { get; set; }

        /// <summary>
        /// Chegada Horario
        /// </summary>
        public int ChegadaHorario { get; set; }

        /// <summary>
        /// Chegada trem
        /// </summary>
        public int Chegada { get; set; }

        /// <summary>
        /// Partida de destino
        /// </summary>
        public string ChegadaDestino
        {
            get
            {
                return Chegada > 0 ? string.Format("{0}/{1}", ChegadaHorario, Chegada) : string.Empty;
            }
        }

        /// <summary>
        /// Partida de origem
        /// </summary>
        public string PartidaOrigem
        {
            get { return Partida > 0 ? string.Format("{0}/{1}", PartidaHorario, Partida) : string.Empty; }
        }

        /// <summary>
        /// Perc de origem
        /// </summary>
        public string PercOrigem
        {
            get
            {
                return Partida > 0 ? string.Format("{0}%", (((double)PartidaHorario / Partida) * 100).ToString("n2")) : string.Empty;
            }
        }

        /// <summary>
        /// Perc de destino
        /// </summary>
        public string PercDestino
        {
            get
            {
                return Chegada > 0 ? string.Format("{0}%", (((double)ChegadaHorario / Chegada) * 100).ToString("n2")) : string.Empty;
            }
        }
    }
}
