﻿namespace Translogic.Modules.Core.Interfaces.Trem.OnTime
{
    /// <summary>
    /// Indicadores de status das partidas
    /// </summary>
    public class IndicadoresOnTime
    {
        /// <summary>
        /// Total de itens
        /// </summary>
        public int TotalTrem { get; set; }

        /// <summary>
        /// Trens encerrados
        /// </summary>
        public int TremEncerrados { get; set; }

        /// <summary>
        /// Trens parados
        /// </summary>
        public int TremParados { get; set; }

        /// <summary>
        /// Trem com partida
        /// </summary>
        public int TremPartida { get; set; }

        /// <summary>
        /// Trens com chegada
        /// </summary>
        public int TremChegada { get; set; }

        /// <summary>
        /// Trens com chegada no horario
        /// </summary>
        public int TremChegadaHorario { get; set; }

        /// <summary>
        /// Trens com Chegada em atraso
        /// </summary>
        public int TremChegadaAtraso { get; set; }

        /// <summary>
        /// Trens suprimidos
        /// </summary>
        public int TremSuprimido { get; set; }

        /// <summary>
        /// Trens suprimidos 12 horas
        /// </summary>
        public int TremSuprimido12Horas { get; set; }

        /// <summary>
        /// Os Irregular
        /// </summary>
        public int OsIrregular { get; set; }

        /// <summary>
        /// Os Irregular
        /// </summary>
        public int OsRegular { get; set; }

        /// <summary>
        /// Trem Partida atraso
        /// </summary>
        public int TremPartidaAtraso { get; set; }

        /// <summary>
        /// Trem Partida atraso Os Regular
        /// </summary>
        public int TremPartidaAtrasoOsRegular { get; set; }

        /// <summary>
        /// Trem Partida atraso Os Irregular
        /// </summary>
        public int TremPartidaAtrasoOsIrregular { get; set; }

        /// <summary>
        /// Trem Partida atraso
        /// </summary>
        public int OsAposPartida { get; set; }

        /// <summary>
        /// Trem Partida atraso
        /// </summary>
        public int OsMenor2Horas { get; set; }

        /// <summary>
        /// Perda Mudanca
        /// </summary>
        public int PerdaMudanca { get; set; }

        /// <summary>
        /// Partida Prevista Grade
        /// </summary>
        public int PartidaPrevistaGrade { get; set; }

        /// <summary>
        /// Tempo Medio Atraso
        /// </summary>
        public string TempoMedioAtraso { get; set; }

        /// <summary>
        /// Atraso OnTime
        /// </summary>
        public string AtrasoOnTime { get; set; }

        /// <summary>
        /// Atraso OnTime Total
        /// </summary>
        public string AtrasoOnTimeTotal { get; set; }

        /// <summary>
        /// Horas De Multa
        /// </summary>
        public int HorasDeMulta { get; set; }

        /// <summary>
        /// Trens Suprimidos tela 433
        /// </summary>
        public int TrensSuprimidos433 { get; set; }

        /// <summary>
        /// Trens canc3elados tela 371
        /// </summary>
        public int TrensCancelador371 { get; set; }
    }
}
