﻿namespace Translogic.Modules.Core.Interfaces.Trem
{
    using System;
    using System.Collections.Generic;

    /// <summary>
	/// Representa o Trem
	/// </summary>
	public class DetalheFichaTremDto
	{
		#region PROPRIEDADES

		/// <summary>
		/// Codigo da Locomotiva
		/// </summary>
		public virtual string CodigoLocomotiva { get; set; }

		/// <summary>
		/// Data de Anexação
		/// </summary>
		public virtual DateTime? DataAnexacao { get; set; }

        /// <summary>
        /// Data de Desanexação
        /// </summary>
        public virtual DateTime? DataDesanexacao { get; set; }

        #endregion
	}
}