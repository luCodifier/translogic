namespace Translogic.Modules.Core.Interfaces.Trem
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;
    using OnTime;
    using OrdemServico;
    using Veiculo.Locomotiva;

    /// <summary>
    /// Contrato para o servi�o de OS
    /// </summary>
    [ServiceContract]
    public interface ICentroTremService
    {
        #region M�TODOS

        /// <summary>
        /// Obtem a lista de OSs para OnTime
        /// </summary>
        /// <param name="requisicao">Requisi��o de fim da pesquisa</param>
        /// <returns>OSs encontradas para o intervalo</returns>
        [OperationContract]
        ResultadoOrdemServicoOnTime ObterOrdensServicoOnTime(RequisicaoOrdemServicoOnTime requisicao);

        /// <summary>
        /// Obtem a lista de Partidas de OSs para OnTime
        /// </summary>
        /// <param name="requisicao">Requisicao de Dados de OnTime</param>
        /// <returns>Dados de OnTime</returns>
        [OperationContract]
        DadosOnTime ObterDadosOnTime(RequisicaoDadosOnTime requisicao);

        /// <summary>
        /// Obtem todas as OSs suprimidas no intervalo informado
        /// </summary>
        /// <param name="dataInicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="dataFinal">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        [OperationContract]
        IList<OrdemServicoDto> ObterOSSuprimidas(DateTime dataInicial, DateTime dataFinal);

        /// <summary>
        /// Obtem as OS por <see cref="SituacaoOrdemServicoEnum"/>
        /// </summary>
        /// <param name="situacao">Situa��o da OS</param>
        /// <param name="prefixo">Prefixo das ordens de servico</param>
        /// <param name="origem">Origem das ordens de servico</param>
        /// <param name="destino">Destino das ordens de servico</param>
        /// <param name="inicial">Data inicial (Data oficial de confirma��o da partida prevista)</param>
        /// <param name="final">Data final (Data oficial de confirma��o da partida prevista)</param>
        /// <returns>Lista de <see cref="OrdemServico"/></returns>
        [OperationContract]
        IList<OrdemServicoDto> ObterPorSituacaoEDataPartidaPrevistaOficial(SituacaoOrdemServicoEnum situacao, string prefixo, string origem, string destino, DateTime inicial, DateTime final);

        /// <summary>
        /// Insere uma nova ordem de servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� gravada</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        [OperationContract]
        OrdemServicoDto Inserir(OrdemServicoDto ordemServico);

        /// <summary>
        /// Insere uma nova ordem de servi�o
        /// </summary>
        /// <param name="idOs"> Id da Ordem de servi�o que ser� persistida</param>
        /// <param name="dataPartida"> Data Partida da Ordem de servi�o que ser� persistida</param>
        /// <param name="usuario">Usuario que solicitou a altera��o.</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        [OperationContract]
        ResultadoOrdemServico<OrdemServicoDto> AtualizarDataPartidaOficial(int idOs, DateTime dataPartida, string usuario);

        /// <summary>
        /// Retorna as OS que tiveram a data de partida alterada
        /// </summary>
        /// <param name="inicial">Data inicial da �ltima altera��o</param>
        /// <param name="final">Data final da �ltima altera��o</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        [OperationContract]
        ResultadoOrdemServico<LogAlteracaoOsDto> ObterAlteracoesDataPartida(DateTime inicial, DateTime? final);

        /// <summary>
        /// Fecha o planejamento e a programa��o de uma ordem de servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servi�o que ser� gravada</param>
        /// <returns>Ordem de servico com os dados atualizados</returns>
        [OperationContract]
        OrdemServicoDto FecharOrdemServico(OrdemServicoDto ordemServico);

        /// <summary>
        /// Fechar o planejamento de ordem servi�o da grade
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechado o planejamento</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        [OperationContract]
        OrdemServicoDto FecharPlanejamentoOrdemServico(OrdemServicoDto ordemServico);

        /// <summary>
        /// Fechar a programacao da ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� fechada a programa��o</param>
        /// <returns>Ordem de servi�o atualizada</returns>
        [OperationContract]
        OrdemServicoDto FecharProgramacaoOrdemServico(OrdemServicoDto ordemServico);

        /// <summary>
        /// Suprimir (cancelar) ordem servi�o
        /// </summary>
        /// <param name="ordemServico">Ordem de servico que ser� suprimida</param>
        [OperationContract]
        void SuprimirOrdemServico(OrdemServicoDto ordemServico);

        /// <summary>
        /// Alocar s�ries de locomotivas para o <see cref="OrdemServicoDto"/>
        /// </summary>
        /// <param name="idOrdemServico">Ordem de servico que receber� o planejamento de s�ries</param>
        /// <param name="series">Lista de s�ries de locomotivas planejadas <see cref="PlanejamentoSerieLocomotivaDto"/></param>
        [OperationContract]
        void AlocarSeriesLocomotivasOrdemServico(int idOrdemServico, IList<PlanejamentoSerieLocomotivaDto> series);

        /// <summary>
        /// Obtem todas as SeriesLocomotivas
        /// </summary>
        /// <returns>Lista contendo todas as SerieLocomotivaDTO</returns>
        [OperationContract]
        IList<SerieLocomotivaDto> ObterTodasSeriesLocomotivas();

        /// <summary>
        /// Obtem a ficha do trem
        /// </summary>
        /// <param name="idPreOs">Id da PreOS no TLFerro</param>
        /// <returns>Objeto FichaTremDto</returns>
        [OperationContract]
        FichaTremDto ObterFichaTrem(int idPreOs);

        /// <summary>
        /// Obtem a PreOS do trem por PreId
        /// </summary>
        /// <param name="idPreOs">PreId da OS para importar</param>
        /// <returns>OS importada</returns>
        [OperationContract]
        OrdemServicoDto ObterPreOSPorId(int idPreOs);

        /// <summary>
        /// Obtem a OS do trem por id
        /// </summary>
        /// <param name="idOs">Id da OS para importar</param>
        /// <returns>OS importada</returns>
        [OperationContract]
        OrdemServicoDto ObterOSPorId(int idOs);

        /// <summary>
        /// Obtem a OS do trem por numero
        /// </summary>
        /// <param name="numeroOrdemServico">Numero da OS para importar</param>
        /// <returns>OS importada</returns>
        [OperationContract]
        OrdemServicoDto ObterOSPorNumero(int numeroOrdemServico);

        /// <summary>
        /// Atualiza as datas de partida e chegada real 
        /// </summary>
        void AtualizarMovimentacaoTremReal();

        /// <summary>
        /// Obtem os dados das locomotivas da OS
        /// </summary>
        /// <param name="numeroOS">N�mero da OS</param>
        /// <returns>Lista das Locomotivas da OS</returns>
        [OperationContract]
        LocomotivasOSDto ObterDadosLocomotivasOS(int numeroOS);

        #endregion
    }
}