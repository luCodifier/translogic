namespace Translogic.Modules.Core.Interfaces.Trem.Veiculo.Locomotiva
{
    /// <summary>
    /// Representa a S�rie da Locomotiva
    /// </summary>
    public class SerieLocomotivaDto
    {
        #region PROPRIEDADES

		/// <summary>
		/// Id da serie locomotiva
		/// </summary>
		public virtual int Id { get; set; }

        /// <summary>
        /// C�digo da Serie
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Descri��o da Serie
        /// </summary>
        public virtual string Descricao { get; set; }

        /// <summary>
        /// N�mero de Eixos
        /// </summary>
        public virtual int? NumeroEixos { get; set; }

        /// <summary>
        /// Valor do KGF
        /// </summary>
        public virtual double? ValorKGF { get; set; }

        #endregion
    }
}