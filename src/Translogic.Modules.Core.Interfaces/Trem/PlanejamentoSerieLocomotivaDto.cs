namespace Translogic.Modules.Core.Interfaces.Trem
{
    using System;
    using Acesso;
    using OrdemServico;
    using Veiculo.Locomotiva;

    /// <summary>
    /// Planejamento de series de locomotivas no trem
    /// </summary>
    public class PlanejamentoSerieLocomotivaDto
    {
		/// <summary>
		/// Identificador do registro
		/// </summary>
		public virtual int Id { get; set; }
		
		/// <summary>
        /// Id da Ordem de servi�o <see cref="OrdemServico"/>
        /// </summary>
        public virtual int IdOrdemServico { get; set; }

        /// <summary>
        /// <see cref="EstacaoMaeDto"/> onde ser�o anexadas ou desanexadas as locos
        /// </summary>
        public virtual EstacaoMaeDto Local { get; set; }

        /// <summary>
        /// <see cref="SerieLocomotivaDto"/> do planejamento
        /// </summary>
        public virtual SerieLocomotivaDto Serie { get; set; }

        /// <summary>
        /// Quantidade de locomotivas anexadas no local
        /// </summary>
        public virtual int QtdeAnexadas { get; set; }

        /// <summary>
        /// Quantidade de locomotivas desanexadas no local
        /// </summary>        
        public virtual int QtdeDesanexadas { get; set; }

        /// <summary>
        /// Data de cadastro
        /// </summary>
        public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// <see cref="UsuarioDto"/> que gravou o planejamento
        /// </summary>
        public virtual UsuarioDto UsuarioCadastro { get; set; }
    }
}