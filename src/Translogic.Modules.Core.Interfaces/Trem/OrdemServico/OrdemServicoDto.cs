namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	using System;
	using System.Collections.Generic;
	using Acesso;
	using Estrutura;

    /// <summary>
	/// Representa o tipo da situa��o da OS
	/// </summary>
	public enum SituacaoOrdemServicoEnum
	{
		/// <summary>
		/// Representa o tipo de situa��o 'Compativel' - 2
		/// </summary>
		Compativel = 2,

		/// <summary>
		/// Representa o tipo de situa��o 'Reservado' - 3
		/// </summary>
		Reservado = 3,

		/// <summary>
		/// Representa o tipo de situa��o 'Confirmado' - 4
		/// </summary>
		Confirmado = 4,

		/// <summary>
		/// Representa o tipo de situa��o 'Fechado' - 5
		/// </summary>
		Fechado = 5,

		/// <summary>
		/// Representa o tipo de situa��o 'Encerrado' - 6
		/// </summary>
		Encerrado = 6,

		/// <summary>
		/// Representa o tipo de situa��o 'Cancelado' - 7
		/// </summary>
		Cancelado = 7,

		/// <summary>
		/// Representa o tipo de situa��o 'Em Planejamento' - 10
		/// </summary>
		EmPlanejamento = 10,

		/// <summary>
		/// Representa o tipo de situa��o 'Pendente' - 1
		/// </summary>
		Pendente = 1,

		/// <summary>
		/// Representa o tipo de situa��o 'Planejada' - 11
		/// </summary>
		Planejada = 11,

		/// <summary>
		/// Representa o tipo de situa��o 'Suprimida em Planejamento' - 12
		/// </summary>
		SuprimidaEmPlanejamento = 12,

		/// <summary>
		/// Representa o tipo de situa��o 'Em Programacao' - 13
		/// </summary>
		EmProgramacao = 13,

		/// <summary>
		/// Representa o tipo de situa��o 'Programada' - 14
		/// </summary>
		Programada = 14,

		/// <summary>
		/// Representa o tipo de situa��o 'Suprimida em Programacao' - 15
		/// </summary>
		SuprimidaEmProgramacao = 15,

		/// <summary>
		/// Representa o tipo de situa��o 'Em Programa - 16
		/// </summary>
		SuprimidaEmPrograma = 16
	}

	/// <summary>
	/// Representa uma Ordem de Servi�o de um Pedido
	/// </summary>
	public class OrdemServicoDto
	{
	    private IList<PlanejamentoSerieLocomotivaDto> _planejamentoSerieLocomotiva;
	    private IList<ParadaOrdemServicoProgramadaDto> _paradasProgramadas;
		// private IList<PlanejamentoQuantidadeLocomotivaDto> _planejamentoQuantidadeLocomotiva;

		/// <summary>
        /// Construtor padr�o
        /// </summary>
        public OrdemServicoDto()
        {
            _planejamentoSerieLocomotiva = new List<PlanejamentoSerieLocomotivaDto>();
            _paradasProgramadas = new List<ParadaOrdemServicoProgramadaDto>();
			// _planejamentoQuantidadeLocomotiva = new List<PlanejamentoQuantidadeLocomotivaDto>();
        }

		#region PROPRIEDADES

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// EstacaoMae de Destino
		/// </summary>
		public LocalDto Destino { get; set; }

		/// <summary>
		/// N�mero da OS
		/// </summary>
		public int? Numero { get; set; }

		/// <summary>
		/// EstacaoMae de Origem
		/// </summary>
        public LocalDto Origem { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public string Prefixo { get; set; }

		/// <summary>
		/// Quantidade de Vag�es carregados
		/// </summary>
		public int QtdeVagoesCarregados { get; set; }

		/// <summary>
		/// Quantidade de Vagoes vazios
		/// </summary>
		public int QtdeVagoesVazios { get; set; }

		/// <summary>
		/// <see cref="Rota"/> da OS
		/// </summary>
		public RotaDto Rota { get; set; }

		/// <summary>
		/// Situa��o da ordem de servi�o - <see cref="SituacaoOrdemServicoEnum"/>
		/// </summary>
		public SituacaoOrdemServicoEnum Situacao { get; set; }

        /// <summary>
        /// Dura��o da ordem de servi�o
        /// </summary>
        public virtual DateTime Duracao { get; set; }

        /// <summary>
        /// Empresa respons�vel pelo trem
        /// </summary>
        public virtual EmpresaFerroviaDto Empresa { get; set; }

        #region Datas Oficiais
        /// <summary>
        /// Data oficial de confirma��o da chegada prevista
        /// </summary>
        public virtual DateTime? DataChegadaPrevistaOficial { get; set; }

        /// <summary>
        /// Data oficial de confirma��o da partida prevista
        /// </summary>
        public virtual DateTime? DataPartidaPrevistaOficial { get; set; }
        #endregion

        #region Datas da grade
        /// <summary>
        /// Data confirma��o da chegada prevista
        /// </summary>
        public virtual DateTime? DataChegadaPrevistaGrade { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista
        /// </summary>
        public virtual DateTime? DataPartidaPrevistaGrade { get; set; }
        #endregion

        /// <summary>
        /// Usuario que est� persistindo
        /// </summary>
        public UsuarioDto Usuario { get; set; }

        /// <summary>
        /// Planejamento de locomotivas
        /// </summary>
        public virtual IList<PlanejamentoSerieLocomotivaDto> PlanejamentoSerieLocomotivas
        {
            get
            {
                return _planejamentoSerieLocomotiva;
            }

            set
            {
                _planejamentoSerieLocomotiva = value;
            }
        }

		/// <summary>
		/// Paradas programadas para esta Ordem de Servi�o
		/// </summary>
		public virtual IList<ParadaOrdemServicoProgramadaDto> ParadasProgramadas
		{
			get
			{
				return _paradasProgramadas;
			}

			set
			{
				_paradasProgramadas = value;
			}
		}

/*
		/// <summary>
		/// Paradas programadas para esta Ordem de Servi�o
		/// </summary>
		public virtual IList<PlanejamentoQuantidadeLocomotivaDto> PlanejamentoQuantidadeLocomotiva
		{
			get
			{
				return _planejamentoQuantidadeLocomotiva;
			}

			set
			{
				_planejamentoQuantidadeLocomotiva = value;
			}
		}
*/

        /// <summary>
        /// Indica se a ordem de servi�o foi suprimida (cancelada) ou n�o
        /// </summary>
        public virtual bool Suprimido
        {
            get
            {
                return DataSupressao.HasValue;
            }
        }

        /// <summary>
        /// Indica se foi fechada a programa��o da ordem de servi�o
        /// </summary>
        public virtual bool FechadaProgramacao
        {
            get
            {
                return DataFechamento.HasValue;
            }
        }

        /// <summary>
        /// Data de Fechamento da OS
        /// </summary>
        public virtual DateTime? DataFechamento { get; set; }

        /// <summary>
        /// Data em que a OS foi Suprimida
        /// </summary>
        public virtual DateTime? DataSupressao { get; set; }

        /// <summary>
        /// Indica se a ordem de servi�o deve ser cumprida pois � meta
        /// </summary>
        public virtual bool? Garantido { get; set; }

        /// <summary>
        /// Usu�rio que fechou a OS
        /// </summary>
        public virtual UsuarioDto UsuarioFechamento { get; set; }

        /// <summary>
        /// Usu�rio que suprimiu a OS
        /// </summary>
        public virtual UsuarioDto UsuarioSupressao { get; set; }

		#endregion
	}
}