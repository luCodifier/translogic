﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System;

    /// <summary>
    /// Representa o tempo que uma atividade leva para ser executada na Estação
    /// </summary>
    public class DuracaoAtividadeEstacaoDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// <see cref="Diversos.Atividade"/> na Estação
        /// </summary>
        public virtual AtividadeDto Atividade { get; set; }

        /// <summary>
        /// Tempo de duração
        /// </summary>
        /// <remarks>
        /// ao recuperar o valor pegar apenas as horas e minutos
        /// </remarks>
        public virtual DateTime Duracao { get; set; }

        /// <summary>
		/// <see cref="EstacaoMaeDto"/> da duração da Atividade
        /// </summary>
		public virtual EstacaoMaeDto Estacao { get; set; }

        #endregion
    }
}