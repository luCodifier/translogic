﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    /// <summary>
    /// Representa a sequencia da <see cref="AreaOperacionalTrecho"/>/<see cref="PatioCirculacao"/> na <see cref="Rota"/>
    /// </summary>
    public class AreaOperacionalRotaDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Area Operacional que pode ser <see cref="AreaOperacionalTrecho"/> ou <see cref="PatioCirculacao"/>
        /// </summary>
        public virtual LocalDto AreaOperacional { get; set; }

        /// <summary>
        /// Sequencia da Area Operacional na Rota
        /// </summary>
        public virtual int Sequencia { get; set; }

        #endregion
    }
}