﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	using System.Collections.Generic;

	/// <summary>
	/// Resultado da consulta OnTime
	/// </summary>
	/// <typeparam name="T">Tipo genérico.</typeparam>
    public class ResultadoOrdemServico<T>
    {
        /// <summary>
        /// Sucesso caso consulta ocorra normalmente
        /// </summary>
        public bool Sucesso { get; set; }

        /// <summary>
        /// Mensagem caso haja falha
        /// </summary>
        public string Mensagem { get; set; }

        /// <summary>
        /// Ordens encontradas
        /// </summary>
        public IList<T> OrdensServico { get; set; }
    }
}
