﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	using System;
	using System.Text;

	/// <summary>
	/// Requisição da consulta OnTime
	/// </summary>
	public class RequisicaoOrdemServicoOnTime
	{
		/// <summary>
		/// Data de inicio da edição
		/// </summary>
		public DateTime? DataInicialEdicao { get; set; }

		/// <summary>
		/// Data de fim da edição
		/// </summary>
		public DateTime? DataFinalEdicao { get; set; }

		/// <summary>
		/// Data de inicio da supressão
		/// </summary>
		public DateTime? DataInicialSupressao { get; set; }

		/// <summary>
		/// Data de fim da supressão
		/// </summary>
		public DateTime? DataFinalSupressao { get; set; }

		/// <summary>
		/// Data de inicio das pesquisas
		/// </summary>
		public DateTime? DataInicialPartida { get; set; }

		/// <summary>
		/// Data de fim da consulta
		/// </summary>
		public DateTime? DataFinalPartida { get; set; }

		/// <summary>
		/// Data de inicio das pesquisas
		/// </summary>
		public DateTime? DataInicialChegada { get; set; }

		/// <summary>
		/// Data de fim da consulta
		/// </summary>
		public DateTime? DataFinalChegada { get; set; }

		/// <summary>
		/// Data de inicio de cadastro
		/// </summary>
		public DateTime? DataInicialCadastro { get; set; }

		/// <summary>
		/// Data de fim do cadastro
		/// </summary>
		public DateTime? DataFinalCadastro { get; set; }

		/// <summary>
		/// Unidade de produção de origem
		/// </summary>
		public string UnidadeProducaoOrigem { get; set; }

		/// <summary>
		/// Unidade de produção de destino
		/// </summary>
		public string UnidadeProducaoDestino { get; set; }

		/// <summary>
		/// Unidade de produção de destino
		/// </summary>
		public string AreaOperacionalOrigem { get; set; }

		/// <summary>
		/// Unidade de produção de destino
		/// </summary>
		public string AreaOperacionalDestino { get; set; }

		/// <summary>
		/// Prefixo principal para consulta
		/// </summary>
		public string Prefixo { get; set; }

		/// <summary>
		/// Prefixo para inclusão
		/// </summary>
		public string PrefixosIncluir { get; set; }

		/// <summary>
		/// Prefixos para exclusão
		/// </summary>
		public string PrefixosExcluir { get; set; }

		/// <summary>
		/// Numero da OS para filtro
		/// </summary>
		public string NumeroOrdemServico { get; set; }

		/// <summary>
		/// Representação textual da requisição
		/// </summary>
		/// <returns>Texto representado a requisição</returns>
		public override string ToString()
		{
			var sb = new StringBuilder();

			sb.AppendFormat("OrdemServiço {0}\n", NumeroOrdemServico);
			sb.AppendFormat("Edição {0} a {1}\n", DataInicialEdicao, DataFinalEdicao);
			sb.AppendFormat("Partida {0} a {1}\n", DataInicialPartida, DataFinalPartida);
			sb.AppendFormat("Chegada {0} a {1}\n", DataInicialPartida, DataFinalPartida);
			sb.AppendFormat("Cadastro {0} a {1}\n", DataInicialCadastro, DataFinalCadastro);
			sb.AppendFormat("UPs {0} e {1}\n", UnidadeProducaoOrigem, UnidadeProducaoDestino);
			sb.AppendFormat("Estações {0} e {1}\n", AreaOperacionalOrigem, AreaOperacionalDestino);
			sb.AppendFormat("Prefixo {0} - Incluir {1} - Excluir {2}\n", Prefixo, PrefixosIncluir, PrefixosExcluir);

			return sb.ToString();
		}
	}
}