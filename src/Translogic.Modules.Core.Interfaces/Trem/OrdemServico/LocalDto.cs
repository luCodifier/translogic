namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	/// <summary>
	/// Representa uma esta��o do cadatro �nico
	/// </summary>
	public class LocalDto
	{
		#region PROPRIEDADES

        /// <summary>
        /// C�digo da �rea operacional
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// C�digo da �rea operacional
		/// </summary>
		public string Codigo { get; set; }

		/// <summary>
		/// Descri��o da �rea operacional
		/// </summary>
		public string Descricao { get; set; }

        /// <summary>
        /// Esta��o m�e
        /// </summary>
        public EstacaoMaeDto EstacaoMae { get; set; }

		#endregion
	}
}