﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    /// <summary>
    /// Representa uma Atividade que pode ser executada
    /// </summary>
    public class AtividadeDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Código da Atividade
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Descrição detalhada da Atividade
        /// </summary>
        public virtual string DescricaoDetalhada { get; set; }

        /// <summary>
        /// Descrição resumida da Atividade
        /// </summary>
        public virtual string DescricaoResumida { get; set; }

        /// <summary>
        /// Percentual do tempo da Atividade
        /// </summary>
        public virtual int Tolerancia { get; set; }

        #endregion
    }
}