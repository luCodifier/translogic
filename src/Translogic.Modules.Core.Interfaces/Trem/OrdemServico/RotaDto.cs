namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System.Collections.Generic;
    using System.ComponentModel;

	/// <summary> 
	/// Sentidos da Rota 
	/// </summary>
	public enum RotaSentido
	{
		/// <summary> 
		/// Sentido da Rota Subindo
		/// </summary>
		[Description("S")] 
		Subindo = 'S',

		/// <summary> 
		/// Sentido da Rota Descendo
		/// </summary>
		[Description("D")]
		Descendo = 'D'
	}

    /// <summary> 
    /// DTO para RotaSentido
    /// </summary>
    public enum RotaSentidoDto
    {
        /// <summary> 
        /// Sentido da Rota Subindo
        /// </summary>
        [Description("S")]
        Subindo = 'S',

        /// <summary> 
        /// Sentido da Rota Descendo
        /// </summary>
        [Description("D")]
        Descendo = 'D'
    }

	/// <summary>
	/// Representa as rotas que o Trem pode passar
	/// </summary>
	public class RotaDto
	{
		#region PROPRIEDADES

		/// <summary>
		/// C�digo da �rea operacional
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Sentido da Rota - <see cref="RotaSentido"/>
		/// </summary>
		public virtual RotaSentidoDto? Sentido { get; set; }

		/// <summary>
		/// C�digo da �rea operacional
		/// </summary>
		public string Codigo { get; set; }

		/// <summary>
		/// Descri��o da �rea operacional
		/// </summary>
		public string Descricao { get; set; }

		/// <summary>
		/// <see cref="EstacaoDto"/> de Destino
		/// </summary>
		public virtual LocalDto Destino { get; set; }

		/// <summary>
		/// <see cref="EstacaoDto"/> de Origem
		/// </summary>
		public virtual LocalDto Origem { get; set; }

        /// <summary>
        /// Lista de <see cref="IAreaOperacional"/> que formam esta rota
        /// </summary>
        public virtual IList<AreaOperacionalRotaDto> Detalhe { get; set; }

		#endregion
	}
}