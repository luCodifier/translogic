namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	/// <summary>
    /// Representa uma Estacao M�e do cadastro �nico
	/// </summary>
	public class EstacaoMaeDto
	{
		#region PROPRIEDADES

        /// <summary>
        /// C�digo da �rea operacional
        /// </summary>
        public int Id { get; set; }

		/// <summary>
		/// C�digo da �rea operacional
		/// </summary>
		public string Codigo { get; set; }

		/// <summary>
		/// Descri��o da �rea operacional
		/// </summary>
		public string Descricao { get; set; }

		#endregion
	}
}