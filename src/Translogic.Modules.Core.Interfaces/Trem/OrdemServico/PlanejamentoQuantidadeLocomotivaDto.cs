namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System;
    using System.Collections.Generic;
    using Veiculo.Locomotiva;

	/// <summary>
	/// Dto para PlanejamentoQuantidadeLocomotiva
    /// </summary>
    public class PlanejamentoQuantidadeLocomotivaDto
    {
        #region PROPRIEDADES

		/// <summary>
		/// Identificador do registro
		/// </summary>
		public virtual int Id { get; set; }

        /// <summary>
        /// Qyantidade de Vagoes anexados Carregados
        /// </summary>
        public virtual int Quantidade { get; set; }

		/// <summary>
		/// Id da Ordem de servi�o <see cref="OrdemServico"/>
		/// </summary>
		public virtual int IdOrdemServico { get; set; }

		/// <summary>
		/// <see cref="SerieLocomotivaDto"/> do planejamento
		/// </summary>
		public virtual SerieLocomotivaDto Serie { get; set; }

		/// <summary>
		/// Tipo da opera��o executada
		/// </summary>
		public virtual string TipoOperacao { get; set; }

		#endregion
    }
}