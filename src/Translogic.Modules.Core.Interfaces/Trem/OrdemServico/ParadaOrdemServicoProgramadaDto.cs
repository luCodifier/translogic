namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Dto para ParadaOrdemServicoProgramadaDto
    /// </summary>
    public class ParadaOrdemServicoProgramadaDto
    {
        #region PROPRIEDADES

		/// <summary>
		/// Identificador do registro
		/// </summary>
		public virtual int Id { get; set; }

        /// <summary>
        /// Data de Chegada
        /// </summary>
        public virtual DateTime DataChegada { get; set; }

        /// <summary>
        /// Data de Partida
        /// </summary>
        public virtual DateTime DataPartida { get; set; }

        /// <summary>
        /// Dura��o para realizar as atividades
        /// </summary>
        public virtual DateTime Duracao { get; set; }

        /// <summary>
        /// <see cref="LocalDto"/> da rota da OS
        /// </summary>
		public virtual EstacaoMaeDto Estacao { get; set; }

        /// <summary>
        /// Qyantidade de Vagoes anexados Carregados
        /// </summary>
        public virtual int QtdeVagoesAnexadosCarregado { get; set; }

        /// <summary>
        /// Quantidade de Vag�es Anexados vazios
        /// </summary>
        public virtual int QtdeVagoesAnexadosVazio { get; set; }

        /// <summary>
        /// Quantidade de Vagoes dexanexados carregados
        /// </summary>
        public virtual int QtdeVagoesDesanexadosCarregado { get; set; }

        /// <summary>
        /// Quantidade de Vagoes desanexados vazios
        /// </summary>
        public virtual int QtdeVagoesDesanexadosVazio { get; set; }

        /// <summary>
        /// Lista de atividades que ocorrer�o na parada programada
        /// </summary>
        public virtual IList<AtividadesParadaOrdemServicoProgramadaDto> Atividades { get; set; }

        #endregion
    }
}