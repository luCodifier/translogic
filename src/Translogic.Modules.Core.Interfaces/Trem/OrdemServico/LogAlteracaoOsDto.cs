﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System;

    /// <summary>
    /// Log de alteração da Ordem de Serviço
    /// </summary>
    public class LogAlteracaoOsDto
    {
        /// <summary>
        /// Identificador da Ordem de Serviço
        /// </summary>
        public decimal IdOs { get; set; }

        /// <summary>
        /// Identificador do usuário que realizou a alteração
        /// </summary>
        public string UsUsrIdu { get; set; }

        /// <summary>
        /// Data Anterior que foi alterada.
        /// </summary>
        public DateTime DataAnteriorPartida { get; set; }

        /// <summary>
        /// Nova data de partida.
        /// </summary>
        public DateTime NovaDataPartida { get; set; }

        /// <summary>
        /// Data de cadastro da atualização.
        /// </summary>
        public DateTime DataCadastro { get; set; }
    }
}