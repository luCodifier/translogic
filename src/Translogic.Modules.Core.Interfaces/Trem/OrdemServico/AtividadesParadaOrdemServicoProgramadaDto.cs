﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System;

    /// <summary>
    /// Representa o apontamento do período da Parada programada
    /// </summary>
    public class AtividadesParadaOrdemServicoProgramadaDto
    {
        #region PROPRIEDADES

		/// <summary>
		/// Identificador do registro
		/// </summary>
		public virtual int Id { get; set; }

        /// <summary>
        /// Data de Término de execução da Atividade
        /// </summary>
        public virtual DateTime DataFim { get; set; }

        /// <summary>
        /// Data de Início de execução da Atividade
        /// </summary>
        public virtual DateTime DataInicio { get; set; }

        /// <summary>
        /// Duração da Atividade que será executada na Estação <see cref="DuracaoAtividadeEstacao"/>
        /// </summary>
        public virtual DuracaoAtividadeEstacaoDto DuracaoAtividadeEstacao { get; set; }

        /// <summary>
        /// Programação da Parada da Ordem de Serviço <see cref="ParadaOrdemServicoProgramada"/>
        /// </summary>
        // public virtual ParadaOrdemServicoProgramadaDto ParadaOrdemServicoProgramada { get; set; }

        #endregion
    }
}