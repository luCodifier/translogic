﻿namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	using System.Collections.Generic;

	/// <summary>
	/// Resultado da consulta OnTime
	/// </summary>
	public class ResultadoOrdemServicoOnTime
	{
		/// <summary>
		/// Sucesso caso consulta ocorra normalmente
		/// </summary>
		public bool Sucesso { get; set; }

		/// <summary>
		/// Detalhes caso haja falha
		/// </summary>
		public string Detalhes { get; set; }

		/// <summary>
		/// Ordens encontradas
		/// </summary>
		public IList<OrdemServicoOnTimeDto> OrdensServico { get; set; }
	}
}
