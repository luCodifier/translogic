namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using Estrutura;
    
    /// <summary>
    /// Locomotiva presente na OS
    /// </summary>
    public class LocomotivaDto
    {
        /// <summary>
        /// C�digo da Locomotiva
        /// </summary>
        public string Locomotiva { get; set; }

        /// <summary>
        /// Ordem da locomotiva na composicao
        /// </summary>
        public int Sequencia { get; set; }

        /// <summary>
        /// Posi��o da locomotiva
        /// </summary>
        public string Posicao { get; set; }

        /// <summary>
        /// Tipo de comando da locomotiva
        /// </summary>
        public string Comando { get; set; }

        /// <summary>
        /// Tipo de tra��o da locomotiva
        /// </summary>
        public string Tracao { get; set; }
    }

    /// <summary>
    /// Representa uma Ordem de Servi�o de um Pedido
    /// </summary>
    public class LocomotivasOSDto
    {
        private IList<LocomotivaDto> _locomotivas;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public LocomotivasOSDto()
        {
            _locomotivas = new List<LocomotivaDto>();
        }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// N�mero da OS
        /// </summary>
        public int Numero { get; set; }

        /// <summary>
        /// Prefixo da OS
        /// </summary>
        public string Prefixo { get; set; }

        /// <summary>
        /// Planejamento de locomotivas
        /// </summary>
        public virtual IList<LocomotivaDto> Locomotivas
        {
            get
            {
                return _locomotivas;
            }

            set
            {
                _locomotivas = value;
            }
        }
    }
}