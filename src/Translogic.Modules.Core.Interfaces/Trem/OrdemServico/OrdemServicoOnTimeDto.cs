namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	using System;

	/// <summary>
	/// Situa��o da ordem de servi�o para OnTime
	/// </summary>
	public enum SituacaoOrdemServicoOnTimeEnum
	{
		/// <summary>
		/// Situa��o n�o configurada
		/// </summary>
		Indefinido,

		/// <summary>
		/// Ordem de servi�o programada para partida
		/// </summary>
		Programada,

		/// <summary>
		/// Ordem de servi�o suprimida
		/// </summary>
		Suprimida,

		/// <summary>
		/// Ordem de servi�o cancelada no sistema
		/// </summary>
		Cancelada
	}

	/// <summary>
	/// Representa uma Ordem de Servi�o de um Pedido
	/// </summary>
	public class OrdemServicoOnTimeDto
	{
		/// <summary>
		/// Id da ordem de servico
		/// </summary>
		public decimal Id { get; set; }

		/// <summary>
		/// N�mero da OS
		/// </summary>
		public decimal Numero { get; set; }

		/// <summary>
		/// Prefixo do Trem
		/// </summary>
		public string Prefixo { get; set; }

		/// <summary>
		/// Area Operacional de Origem
		/// </summary>
		public string AreaOperacionalOrigem { get; set; }

		/// <summary>
		/// Unidade de Producao de Origem
		/// </summary>
		public string UnidadeProducaoOrigem { get; set; }

		/// <summary>
		/// Area Operacional de Destino
		/// </summary>
		public string AreaOperacionalDestino { get; set; }

		/// <summary>
		/// Unidade de Producao de Destino
		/// </summary>
		public string UnidadeProducaoDestino { get; set; }

		/// <summary>
		/// Situa��o da ordem de servi�o - <see cref="SituacaoOrdemServicoOnTimeEnum"/>
		/// </summary>
		public decimal IdSituacao { get; set; }

        /// <summary>
        /// Data de libera��o do Trem
        /// </summary>
	    public virtual DateTime? DataLiberacaoTrem { get; set; }

		/// <summary>
		/// Data de confirma��o da partida prevista
		/// </summary>
		public virtual DateTime? DataPartidaPrevista { get; set; }

		/// <summary>
		/// Data de confirma��o da chegada prevista
		/// </summary>
		public virtual DateTime? DataChegadaPrevista { get; set; }

		/// <summary>
		/// Data em que a OS foi Suprimida
		/// </summary>
		public virtual DateTime? DataSupressao { get; set; }

		/// <summary>
		/// Data em que a OS foi cadastrada
		/// </summary>
		public virtual DateTime DataCadastro { get; set; }

        /// <summary>
        /// Matricula no cadastro
        /// </summary>
        public string MatriculaCadastro { get; set; }

        /// <summary>
        /// Data Partida em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime DataPartidaCadastro { get; set; }

        /// <summary>
        /// Data em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime? DataUltimaAlteracao { get; set; }

        /// <summary>
        /// Matricula no cadastro
        /// </summary>
        public string MatriculaUltimaAlteracao { get; set; }

        /// <summary>
        /// Data Partida em que a OS foi cadastrada
        /// </summary>
        public virtual DateTime? DataPartidaUltimaAlteracao { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista anterior a editada
        /// </summary>
        public virtual DateTime? DataPartidaAnterior { get; set; }

        /// <summary>
        /// Data de confirma��o da partida prevista grade
        /// </summary>
        public virtual DateTime? DataPartidaPrevistaGrade { get; set; }

        /// <summary>
		/// Data de edi��o da OS
		/// </summary>
		public virtual DateTime? DataEdicaoPartidaAnterior { get; set; }

		/// <summary>
		/// Situa��o da ordem de servi�o - <see cref="SituacaoOrdemServicoOnTimeEnum"/>
		/// </summary>
		public SituacaoOrdemServicoOnTimeEnum Situacao
		{
			get
			{
				return (SituacaoOrdemServicoOnTimeEnum)IdSituacao;
			}
		}

        /// <summary>
        /// OS Cancelada pela tela 371
        /// </summary>
        public string CanceladoTela371 { get; set; }

        /// <summary>
        /// OS Suprimida pela tela 433
        /// </summary>
        public string SuprimidoTela433 { get; set; }
	}
}