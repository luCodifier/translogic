namespace Translogic.Modules.Core.Interfaces.Trem.OrdemServico
{
	/// <summary>
	/// Classe Empresa Envolvida
	/// </summary>
	public class EmpresasEnvolvidasDto
	{
		/// <summary>
		/// C�digo sequencial da ferrovia na rota
		/// </summary>
		public decimal Seq { get; set; }

		/// <summary>
		/// C�digo da empresa
		/// </summary> 
		public decimal IdEmpresa { get; set; }

		/// <summary>
		/// Sigla do estado
		/// </summary> 
		public string SiglaEstado { get; set; }
	}
}