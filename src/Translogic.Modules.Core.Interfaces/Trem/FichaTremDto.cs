﻿namespace Translogic.Modules.Core.Interfaces.Trem
{
    using System.Collections.Generic;

    /// <summary>
	/// Representa o Trem
	/// </summary>
	public class FichaTremDto
	{
		#region CONSTRUTORES
		/// <summary>
		/// Construtor inicializando a lista de movimentações
		/// </summary>
		public FichaTremDto()
		{
            Detalhes = new List<DetalheFichaTremDto>();
		}

		#endregion

		#region PROPRIEDADES

		/// <summary>
		/// Nome do Maquinista
		/// </summary>
		public virtual string NomeMaquinista { get; set; }

		/// <summary>
		/// Matricula do Maquinista
		/// </summary>
		public virtual string MatriculaMaquinista { get; set; }

        /// <summary>
        /// Número de vagões carregados
        /// </summary>
        public virtual int QuantidadeVagoesCarregados { get; set; }

        /// <summary>
        /// Número de vagões vazios
        /// </summary>
        public virtual int QuantidadeVagoesVazios { get; set; }

        /// <summary>
        /// Peso do Trem
        /// </summary>
        public virtual double Tonelagem { get; set; }

        /// <summary>
        /// Comprimento do Trem
        /// </summary>
        public virtual double Comprimento { get; set; }

        /// <summary>
        /// Codigo do último vagão da composição
        /// </summary>
        public virtual string UltimoVagao { get; set; }

        /// <summary>
        /// Indica se possuí telemétrico de cauda
        /// </summary>
        public virtual bool Telemetrico { get; set; }

        /// <summary>
        /// Número do Telemétrico de Cauda
        /// </summary>
        public virtual string NumeroTelemetrico { get; set; }

        /// <summary>
        /// Codigo das Locomotivas anexadas
        /// </summary>
        public virtual string Locomotivas { get; set; }

        /// <summary>
        /// Detalhes da Ficha
        /// </summary>
        public virtual IList<DetalheFichaTremDto> Detalhes { get; set; }
        
        #endregion
	}
}