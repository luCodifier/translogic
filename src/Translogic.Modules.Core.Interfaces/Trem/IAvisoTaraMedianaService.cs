﻿namespace Translogic.Modules.Core.Interfaces.Trem
{
    /// <summary>
    /// Servico de envio de emails com as taras medianas dos vagoes
    /// </summary>
    public interface IAvisoTaraMedianaService
    {
        /// <summary>
        /// metodo de envio de emails com as taras medianas dos vagoes
        /// </summary>
        /// <returns>retorna booleano</returns>
        bool EnviarEmailAviso();
    }
}
