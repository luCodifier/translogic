﻿namespace Translogic.Modules.Core.Interfaces.Trem
{
    using Translogic.Modules.Core.Interfaces.EquipamentosCampo;
    
    /// <summary>
    /// Dto de informação do Equipamento instalado na Sb
    /// </summary>
    public class EquipamentoSbPassagemDto
    {
        /// <summary>
        /// Código da Sb que está instalado o equipamento
        /// </summary>
        public string CodSbInstalado { get; set; }

        /// <summary>
        /// Id do equipamento que se deve realizar a leitura
        /// </summary>
        public int IdEquipamento { get; set; }

        /// <summary>
        /// Identificador do tipo de Equipamento
        /// </summary>
        public TipoEquipamentoEnum TipoEquipamento { get; set; }
    }
}
