﻿namespace Translogic.Modules.Core.Interfaces.Trem
{
    using System;

    /// <summary>
    /// Dto de informação da passagem de um Trem na Sb
    /// </summary>
    public class PassagemTremSbDto
    {
        /// <summary>
        /// Equipamento que está instalado na Sb que se deseja realizar a leitura do Trem
        /// </summary>
        public EquipamentoSbPassagemDto Equipamento { get; set; }

        /// <summary>
        /// Prefixo do trem
        /// </summary>
        public string PrefixoTrem { get; set; }

        /// <summary>
        /// Sb onde apontou o Trem
        /// </summary>
        public string SbApontamento { get; set; }

        /// <summary>
        /// Data e Hora do posicionamento, envio da Macro
        /// </summary>
        public DateTime DataHoraPosicionamento { get; set; }
        
        /// <summary>
        /// Km aproximado do apontamento
        /// </summary>
        public decimal KmAproximado { get; set; }

        /// <summary>
        /// Número da OS do Trem quando houver
        /// </summary>
        public int? NumeroOs { get; set; }

        /// <summary>
        /// Origem do Trem
        /// </summary>
        public string Origem { get; set; }

        /// <summary>
        /// Destino do Trem
        /// </summary>
        public string Destino { get; set; }
    }
}
