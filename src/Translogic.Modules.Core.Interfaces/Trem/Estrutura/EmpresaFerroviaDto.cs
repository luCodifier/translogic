﻿namespace Translogic.Modules.Core.Interfaces.Trem.Estrutura
{
    /// <summary>
    /// Representa uma Empresa que faz "operações" nas linhas da ALL
    /// </summary>
    public class EmpresaFerroviaDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Id da empresa
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Sigla da empresa
        /// </summary>
        public virtual string Sigla { get; set; }

        #endregion
    }
}