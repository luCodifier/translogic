namespace Translogic.Modules.Core.Interfaces.Jobs
{
	/// <summary>
	/// Classe contendo constantes do JobRunner
	/// </summary>
	public static class JobConstants
	{
		/// <summary>
		/// Nome do EventSource
		/// </summary>
		public const string EventSource = "Translogic.JobRunner";
	}
}