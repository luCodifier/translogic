namespace Translogic.Modules.Core.Interfaces.Jobs
{
	/// <summary>
	/// Processo que ser� executado em background
	/// </summary>
	public interface IJob
	{
		/// <summary>
		/// Executa um processamento em background
		/// </summary>
		/// <param name="args">Argumentos para execu��o</param>
		/// <returns>Se o job foi executado com sucesso</returns>
		bool Execute(params object[] args);
	}
}