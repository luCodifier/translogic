namespace Translogic.Modules.Core.Interfaces.Jobs.Service
{
	using System.ServiceModel;

	/// <summary>
	/// Contrato do servi�o para execu��o de jobs
	/// </summary>
	[ServiceContract]
	public interface IJobRunnerService
	{
		/// <summary>
		/// Executa um <see cref="IJob"/>
		/// </summary>
		/// <param name="jobKey">Chave do job no IoC</param>
		/// <param name="args">Argumentos para execu��o do job</param>
		/// <returns>Se o job foi executado com sucesso</returns>
		[OperationContract]
		bool Execute(string jobKey, params string[] args);
	}
}