﻿namespace Translogic.Modules.Core.Interfaces.SefazNfe
{
    /// <summary>
    /// Dados de entrada para consumo do serviço
    /// </summary>
    public class DFeConsultaDtoRequest
    {
        /// <summary>
        /// Estado da empresa
        /// </summary>
        public virtual string Uf { get; set; }

        /// <summary>
        /// Cnpj da empresa
        /// </summary>
        public virtual string CnpjCertificado { get; set; }

        /// <summary>
        /// Id do ultimo registro baixado 
        /// </summary>
        public virtual string UltimoRegistro { get; set; }

        /// <summary>
        /// Cnpj do Certificado Digital
        /// </summary>
        public virtual string CnpjBusca { get; set; }
    }
}
