﻿namespace Translogic.Modules.Core.Interfaces.SefazNfe.Interfaces
{
    using System.Xml;

    /// <summary>
    /// Interface de acesso aos dados 
    /// </summary>
    public interface IDFeService
    {
        /// <summary>
        /// Metodo que retorna XML da Sefaz
        /// </summary>
        /// <param name="request">Objeto com parametros de entrada</param>
        /// <returns>Retorna xml com os dados do acesso e das notas</returns>
        XmlDocument ObterDadosDFe(DFeConsultaDtoRequest request);
    }
}
