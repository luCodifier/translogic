namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    /// <summary>
    /// Classe usada p/ popular Modelos e Series da tabela Equipamento
    /// </summary>
    public class ComboDto
    {
        /// <summary>
        /// Id do equipamento
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Descricao do combo
        /// </summary>
        public virtual string Descricao { get; set; }
    }
}