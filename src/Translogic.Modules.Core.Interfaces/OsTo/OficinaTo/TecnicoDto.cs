namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    /// <summary>
    /// Dto usado no relatorio por tecnico
    /// </summary>
    public class TecnicoDto
    {
        /// <summary>
        /// Id do Tecnico
        /// </summary>
        public virtual int TecnicoId { get; set; }

        /// <summary>
        /// Nome do Tecnico
        /// </summary>
        public virtual string Nome { get; set; }

        /// <summary>
        /// Codigo da OS
        /// </summary>
        public virtual string Os { get; set; }

        /// <summary>
        /// Tipo da Os
        /// </summary>
        public virtual string TipoOs { get; set; }
        
        /// <summary>
        /// Categoria do Equipamento
        /// </summary>
        public virtual string Categoria { get; set; }

        /// <summary>
        /// Modelo do Equipamento
        /// </summary>
        public virtual string Modelo { get; set; }

        /// <summary>
        /// Serial do Equipamento
        /// </summary>
        public virtual string Serial { get; set; }

        /// <summary>
        /// Defeito da Os
        /// </summary>
        public virtual string Defeito { get; set; }

        /// <summary>
        /// Solucao do Equipamento
        /// </summary>
        public virtual string Solucao { get; set; }

        /// <summary>
        /// Tempo de Reparo
        /// </summary>
        public virtual int? TempoReparo { get; set; }
    }
}