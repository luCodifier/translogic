namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    using System;

    /// <summary>
    /// Dto usado para o Relatorio de Aderencia
    /// </summary>
    public class AderenciaDto
    {
        private decimal _aderencia;

        /// <summary>
        /// Id da Oficina
        /// </summary>
        public virtual decimal OficinaId { get; set; }

        /// <summary>
        /// Codigo da Oficina
        /// </summary>
        public virtual string OficinaCodigo { get; set; }

        /// <summary>
        /// Total aberta
        /// </summary>
        public virtual decimal TotalAberta { get; set; }

        /// <summary>
        /// Total Fechada
        /// </summary>
        public virtual decimal TotalFechada { get; set; }

        /// <summary>
        /// Calculo de Aderencia
        /// </summary>
        public virtual decimal Aderencia
        {
            get
            {
                double total = Convert.ToDouble(TotalAberta + TotalFechada);
                
                if (total >= 1)
                {
                    double calculo = (Convert.ToDouble(TotalFechada) / total) * 100;

                    return Convert.ToInt64(calculo);
                }

                return 0;
            }

            set
            {
                _aderencia = value;
            }
        }
    }
}