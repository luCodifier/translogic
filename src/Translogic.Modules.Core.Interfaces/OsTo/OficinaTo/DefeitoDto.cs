namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    /// <summary>
    /// Lista de Defeitos
    /// </summary>
    public class DefeitoDto
    {
        /// <summary>
        /// Id da Falha
        /// </summary>
        public virtual int FalhaId { get; set; }
        
        /// <summary>
        /// Codigo da Falha
        /// </summary>
        public virtual string FalhaCodigo { get; set; }

        /// <summary>
        /// Defeito da Os
        /// </summary>
        public virtual string Defeito { get; set; }
        
        /// <summary>
        /// Solucao do Defeito
        /// </summary>
        public virtual string Solucao { get; set; }

        /// <summary>
        /// Serial do Equipamento
        /// </summary>
        public virtual string Serial { get; set; }

        /// <summary>
        /// Oficina da Os
        /// </summary>
        public virtual string Oficina { get; set; }

        /// <summary>
        /// Categoria do equipamento
        /// </summary>
        public virtual string Categoria { get; set; }

        /// <summary>
        /// Modelo do equipamento
        /// </summary>
        public virtual string Modelo { get; set; }

        /// <summary>
        /// Tecnico Responsavel
        /// </summary>
        public virtual string Tecnico { get; set; }
    }
}