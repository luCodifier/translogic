namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    using System;

    /// <summary>
    /// Dto das Ordem de servico TO
    /// </summary>
    public class OsOficinaDetalheDto
    {
        /// <summary>
        /// ID da Ordem de servico
        /// </summary>
        public decimal OsId { get; set; }

        /// <summary>
        /// Codigo da Ordem de servi�o
        /// </summary>
        public string Codigo { get; set; }

        /// <summary>
        /// Data de Abertura
        /// </summary>
        public DateTime DataAbertura { get; set; }

        /// <summary>
        /// Data Fim da Execucao
        /// </summary>
        public DateTime? DataFimExecucao { get; set; }

        /// <summary>
        /// Data Inicio da Execucao
        /// </summary>
        public DateTime? DataInicioExecucao { get; set; }

        /// <summary>
        /// Status da OS
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Id da Oficina
        /// </summary>
        public decimal OficinaId { get; set; }

        /// <summary>
        /// Codigo da Oficina
        /// </summary>
        public string OficinaCodigo { get; set; }

        /// <summary>
        /// Id do Tipo de Manutencao
        /// </summary>
        public decimal TipoManutencaoId { get; set; }

        /// <summary>
        /// Descricao do Tipo de Manutencao
        /// </summary>
        public string TipoManutencaoDescricao { get; set; }

        /// <summary>
        /// Id do tipo de Equipamento
        /// </summary>
        public decimal TipoEquipamentoId { get; set; }
        
        /// <summary>
        /// Descricao do Tipo de Equipamento
        /// </summary>
        public string TipoEquipamentoDescricao { get; set; }

        /// <summary>
        /// Id da Categoria
        /// </summary>
        public decimal CategoriaId { get; set; }

        /// <summary>
        /// Descricao da Categoria
        /// </summary>
        public string CategoriaDescricao { get; set; }

        /// <summary>
        /// Modelo do Equipamento
        /// </summary>
        public string EquipamentoModelo { get; set; }

        /// <summary>
        /// Serial do Equipamento
        /// </summary>
        public string EquipamentoSerial { get; set; }

        /// <summary>
        /// Id do Tecnico
        /// </summary>
        public decimal TecnicoId { get; set; }

        /// <summary>
        /// Nome do Tecnico
        /// </summary>
        public string TecnicoNome { get; set; }

        /// <summary>
        /// Id da Falha
        /// </summary>
        public string FalhaIdI { get; set; }

        /// <summary>
        /// Defeito da Falha
        /// </summary>
        public string FalhaDefeitoI { get; set; }

        /// <summary>
        /// Solucao da Falha
        /// </summary>
        public string FalhaSolucaoI { get; set; }

        /// <summary>
        /// Gets or sets FalhaClassificacaoI.
        /// </summary>
        public string FalhaClassificacaoI { get; set; }

        /// <summary>
        /// Id da Falha
        /// </summary>
        public string FalhaIdII { get; set; }

        /// <summary>
        /// Defeito da Falha
        /// </summary>
        public string FalhaDefeitoII { get; set; }

        /// <summary>
        /// Solucao da Falha
        /// </summary>
        public string FalhaSolucaoII { get; set; }

        /// <summary>
        /// Gets or sets FalhaClassificacaoII.
        /// </summary>
        public string FalhaClassificacaoII { get; set; }

        /// <summary>
        /// Id da Falha
        /// </summary>
        public string FalhaIdIII { get; set; }

        /// <summary>
        /// Defeito da Falha
        /// </summary>
        public string FalhaDefeitoIII { get; set; }

        /// <summary>
        /// Solucao da Falha
        /// </summary>
        public string FalhaSolucaoIII { get; set; }

        /// <summary>
        /// Gets or sets FalhaClassificacaoIII.
        /// </summary>
        public string FalhaClassificacaoIII { get; set; }

        /// <summary>
        /// Id da Etiqueta
        /// </summary>
        public decimal? EtiquetaId { get; set; }

        /// <summary>
        /// Data da Etiqueta
        /// </summary>
        public DateTime? EtiquetaData { get; set; }

        /// <summary>
        /// Loco da Etiqueta
        /// </summary>
        public string EtiquetaLoco { get; set; }

        /// <summary>
        /// KM da Etiqueta
        /// </summary>
        public decimal? EtiquetaKm { get; set; }

        /// <summary>
        /// Descricao da Etiqueta
        /// </summary>
        public string EtiquetaDescricao { get; set; }

        /// <summary>
        /// Responsavel pela etiqueta
        /// </summary>
        public string EtiquetaResponsavel { get; set; }

        /// <summary>
        /// Id da UP(Equipe)
        /// </summary>
        public decimal UpId { get; set; }

        /// <summary>
        /// Descricao da UP
        /// </summary>
        public string UpDescricao { get; set; }

        /// <summary>
        /// Data Anterior
        /// </summary>
        public DateTime? DataAnterior { get; set; }

        /// <summary>
        /// Gets or sets EquipamentoId.
        /// </summary>
        public decimal EquipamentoId { get; set; }

        /// <summary>
        /// Gets or sets Mtbf.
        /// </summary>
        public int Mtbf { get; set; }
    }
}