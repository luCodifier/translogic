namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo
{
    using System;
    using Enumeradores;

    /// <summary>
    /// Dto para a Os da Oficina TO
    /// </summary>
    public class OsOficinaDto
    {
        /// <summary>
        /// ID da Ordem de servi�o
        /// </summary>
        public virtual int IdOs { get; set; }
        
        /// <summary>
        /// Codigo da Os
        /// </summary>
        public virtual string Codigo { get; set; }

        /// <summary>
        /// Serial do Equipamento
        /// </summary>
        public virtual string Equipamento { get; set; }

        /// <summary>
        /// Data da Abertura
        /// </summary>
        public virtual DateTime DataAbertura { get; set; }

        /// <summary>
        /// Data Anterior da OS do equipamento
        /// </summary>
        public virtual DateTime? DataAnterior { get; set; }

        /// <summary>
        /// Oficina da Ordem de servi�o
        /// </summary>
        public virtual string Oficina { get; set; }

        /// <summary>
        /// Status da ordem de servi�o
        /// </summary>
        public virtual StatusEnum Status { get; set; }
    }
}