namespace Translogic.Modules.Core.Interfaces.OsTo.OficinaTo.Enumeradores
{
    using System.ComponentModel;

    /// <summary>
    /// Enumerador do Status da Ordem de Servico
    /// </summary>
    public enum StatusEnum
    {
        /// <summary>
        /// Ordem de servi�o Aberta
        /// </summary>
        [Description("A")] Aberta,

        /// <summary>
        /// Ordem de servi�o Fechada
        /// </summary>
        [Description("F")] Fechada,

        /// <summary>
        /// Ordem de servi�o Cancelada
        /// </summary>
        [Description("C")] Cancelado
    }
}