﻿namespace Translogic.Modules.Core.Interfaces.OsTo
{
    using System;
    using System.ServiceModel;

    /// <summary>
    /// Interface para Adicionar parada Ws
    /// </summary>
    [ServiceContract]
    public interface IOrdemServicoService
    {
        /// <summary>
        /// Método para acionar a parada
        /// </summary>
        /// <param name="dataParada">
        /// Data de parada
        /// </param>
        /// <param name="trem">
        /// Sigla do Trem
        /// </param>
        /// <param name="ordemServTrem">
        /// Ordem de serviço do trem
        /// </param>
        /// <param name="locomotiva">
        /// sigla locomotiva.
        /// </param>
        /// <param name="local">
        /// The local.
        /// </param>
        /// <returns>
        /// The adicionar parada.
        /// </returns>
        [OperationContract]
        string AdicionarParada(DateTime dataParada, string trem, string ordemServTrem, string locomotiva, int? local);
    }
}