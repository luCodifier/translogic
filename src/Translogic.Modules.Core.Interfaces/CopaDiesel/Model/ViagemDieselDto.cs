﻿namespace Translogic.Modules.Core.Interfaces.CopaDiesel.Model
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Viagem Diesel
    /// </summary>
    [DataContract(Name = "Viagem", Namespace = "http://www.all-logistica.com/translogic/copadiesel")]
    public class ViagemDieselDto
    {
        /// <summary>
        /// Identificador da viagem
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Identificador da viagem no CBL
        /// </summary>
        [DataMember]
        public int ViagemCbl { get; set; }

        /// <summary>
        /// Prefixo do Trem
        /// </summary>
        [DataMember]
        public string PrefixoTrem { get; set; }

        /// <summary>
        /// Estação de origem
        /// </summary>
        [DataMember]
        public string EstacaoOrigem { get; set; }

        /// <summary>
        /// Estação de destino
        /// </summary>
        [DataMember]
        public string EstacaoDestino { get; set; }

        /// <summary>
        /// Ano da viagem
        /// </summary>
        [DataMember]
        public int AnoViagem { get; set; }

        /// <summary>
        /// Trimestre da viagem
        /// </summary>
        [DataMember]
        public int Trimestre { get; set; }

        /// <summary>
        /// Data da partida da origem
        /// </summary>
        [DataMember]
        public DateTime DataPartida { get; set; }

        /// <summary>
        /// Data da chegada no destino
        /// </summary>
        [DataMember]
        public DateTime DataChegada { get; set; }

        /// <summary>
        /// Data da chegada no destino
        /// </summary>
        [DataMember]
        public DateTime DataBaixa { get; set; }

        /// <summary>
        /// Matricula do maquinista responsável pela viagem
        /// </summary>
        [DataMember]
        public string MatriculaMaquinista { get; set; }

        /// <summary>
        /// CPF do maquinista responsável pela viagem
        /// </summary>
        [DataMember]
        public long? CpfMaquinista { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string MatriculaInstruido { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public double Tonelagem { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public double Distancia { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public double Consumo { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco1 { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco2 { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco3 { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco4 { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco5 { get; set; }

        /// <summary>
        /// Matricula do maquinista instruido
        /// </summary>
        [DataMember]
        public string Loco6 { get; set; }

        /// <summary>
        /// Up do maquinista
        /// </summary>
        [DataMember]
        public string Up { get; set; }
    }
}
