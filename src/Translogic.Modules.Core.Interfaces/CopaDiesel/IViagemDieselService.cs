﻿namespace Translogic.Modules.Core.Interfaces.CopaDiesel
{
    using System;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using Model;

    /// <summary>
    /// Interface para serviço de Viagens da Copa Diesel
    /// </summary>
    [ServiceContract(Name = "ViagemService", Namespace = "http://www.all-logistica.com/translogic/copadiesel")]
    public interface IViagemDieselService
    {
        /// <summary>
        /// Lista as viagens efetuadas no periodo descriminado
        /// </summary>
        /// <param name="codUP">Código da UP</param>
        /// <param name="origem">Estação de origem</param>
        /// <param name="destino">Estação de destino</param>
        /// <param name="dataInicio">Data de inicio período</param>
        /// <param name="dataFim">Data de fim do período</param>
        /// <returns>Array com as viagens</returns>
        /// <returns>Array com as viagens</returns>
        [OperationContract]        
        ViagemDieselDto[] ListarViagens(string codUP, string origem, string destino, DateTime dataInicio, DateTime dataFim);

        /// <summary>
        /// Lista as viagens do cbl baixadas sem filtros no periodo descriminado
        /// </summary>
        /// <param name="codUp">Código da UP</param>
        /// <param name="origem">Estação de origem</param>
        /// <param name="destino">Estação de destino</param>
        /// <param name="dataInicio">Data de inicio período</param>
        /// <param name="dataFim">Data de fim do período</param>
        /// <returns>Array com as viagens</returns>
        /// <returns>Array com as viagens</returns>
        [OperationContract]
        ViagemDieselDto[] ListarViagensCbl(string codUp, string origem, string destino, DateTime dataInicio, DateTime dataFim);
    }
}
