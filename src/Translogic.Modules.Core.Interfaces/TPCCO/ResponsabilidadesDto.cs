namespace Translogic.Modules.Core.Interfaces.TPCCO
{
    /// <summary>
    /// Dto para grid de Responsabilidades da pessoa encarregada da parada
    /// </summary>
    public class ResponsabilidadesDto
    {
        /// <summary>
        /// Id da Pessoa
        /// </summary>
        public int IdPessoa { get; set; }

        /// <summary>
        /// Id do Motivo
        /// </summary>
        public int IdMotivo { get; set; }

        /// <summary>
        /// Motivo da Parada
        /// </summary>
        public string Motivo { get; set; }

        /// <summary>
        /// Lista de Ups
        /// </summary>
        public string UPs { get; set; }

        /// <summary>
        /// Niveis de Parada
        /// </summary>
        public string Niveis { get; set; }

        /// <summary>
        /// Descricao do Nivel de Acesso
        /// </summary>
        public string Acesso { get; set; }
    }
}