namespace Translogic.Modules.Core.Interfaces.TPCCO
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;

    /// <summary>
    /// Contrato para o servi�o de controle TPCO
    /// </summary>
    [ServiceContract]
    public interface ITPCCOService
    {
        #region M�TODOS

        /// <summary>
        /// Obtem todas as paradas n�o programadas do trem solicitado
        /// </summary>
        /// <param name="idTrem">Trem solicitado</param>
        /// <param name="inicio">Inicio das paradas</param>
        /// <param name="final">Limite de paradas</param>
        /// <returns>Paradas encontradas</returns>
        [OperationContract]
		IList<ParadaPrevisaoDto> ObterParadasPorTrem(int idTrem, DateTime inicio, DateTime final);

        /// <summary>
        /// Busca as ultimas paradas atualizadas
        /// </summary>
        /// <param name="atualizacao">Data da ultima atualizacao solicitada</param>
        /// <returns>Paradas atualizadas no periodo</returns>
        [OperationContract]
        IList<ParadaPrevisaoDto> ObterAtualizacao(DateTime? atualizacao);

        #endregion
    }
}