﻿namespace Translogic.Modules.Core.Interfaces.TPCCO
{
	using System;

    /// <summary>
    /// Objeto que identifica uma parada não programada e sua configuração
    /// </summary>
    public class ParadaPrevisaoDto
	{
		/// <summary>
		/// Macro relacionado a parada
		/// </summary>
		public virtual int IdMacro { get; set; }

		/// <summary>
		/// Versão do registro
		/// </summary>
		public virtual DateTime VersionDate { get; set; }

		/// <summary>
		/// Trem relacionado a parada
		/// </summary>
		public virtual int IdTrem { get; set; }

		/// <summary>
		/// Data da previsão da partida
		/// </summary>
		public virtual DateTime? DataPrevisao { get; set; }

        /// <summary>
        /// Data da Parada
        /// </summary>
		public virtual DateTime DataParada { get; set; }

        /// <summary>
        /// Flag de qual sistema se originou a PARADA.
        /// </summary>
		public virtual string Sistema { get; set; }

		/// <summary>
		/// Motivo da macro de parada
		/// </summary>
		public virtual int IdMotivo { get; set; }
    }
}