namespace Translogic.Modules.Core.Interfaces.TPCCO
{
    /// <summary>
    /// Representa o Tempo de Alerta por UP, Agrupa os Niveis
    /// </summary>
    public class TempoAlertaDto
    {
        /// <summary>
        /// ID da Unidade Producao
        /// </summary>
        public int IdUP { get; set; }

        /// <summary>
        /// Descricao da UP
        /// </summary>
        public string UP { get; set; }

        /// <summary>
        /// Tempo do Nivel 1
        /// </summary>
        public int Nivel1 { get; set; }

        /// <summary>
        /// Tempo do Nivel 2
        /// </summary>
        public int Nivel2 { get; set; }

        /// <summary>
        /// Tempo do Nivel 3
        /// </summary>
        public int Nivel3 { get; set; }

        /// <summary>
        /// Tempo do Nivel 4
        /// </summary>
        public int Nivel4 { get; set; }

        /// <summary>
        /// Tempo da Ficha de Acidente
        /// </summary>
        public int Ficha { get; set; }
    }
}