﻿namespace Translogic.Modules.Core.Interfaces.GestaoDocumentos
{
    using System.ServiceModel;
    using Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca;

    /// <summary>
    /// Implementação da interface de faturamento. 
    /// </summary>
    [ServiceContract]
    public interface IGestaoDocumentosService
    {
        /// <summary>
        /// Informa(cadastra) os dados do ticket da balança
        /// </summary>
        /// <param name="ticketBalanca">Dados do Ticket da balança</param>
        [OperationContract()]
        void InformarTicketBalanca(TicketDto ticketBalanca);
    }
}