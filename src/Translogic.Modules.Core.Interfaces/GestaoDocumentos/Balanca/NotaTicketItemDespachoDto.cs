﻿namespace Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca
{
    /// <summary>
    /// Nota carregado no caminhão com o seu item despacho
    /// </summary>
    public class NotaTicketItemDespachoDto
    {
        /// <summary>
        /// ID do Item Despacho
        /// </summary>
        public decimal IdItemDespacho { get; set; }

        /// <summary>
        /// chave da NFe
        /// </summary>
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Peso da NFe
        /// </summary>
        public double Peso { get; set; }

        /// <summary>
        /// Serie da NFe
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// Numero da NFe
        /// </summary>
        public int Numero { get; set; }

        /// <summary>
        /// Descricao do produto da NFe
        /// </summary>
        public string Produto { get; set; }

        /// <summary>
        /// Peso disponível = Peso Total = Peso Utilizado
        /// </summary>
        public double PesoDisponivel { get; set; }
    }
}