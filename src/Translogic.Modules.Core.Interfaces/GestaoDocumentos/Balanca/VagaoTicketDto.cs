﻿namespace Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Dados do Caminhão do ticket da balança
    /// </summary>
    [DataContract(Name = "Vagao")]
    public class VagaoTicketDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        [DataMember(Order = 1)]
        public string CodigoVagao { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        [DataMember(Order = 2)]
        public double PesoBruto { get; set; }
        
        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        [DataMember(Order = 3)]
        public double PesoTara { get; set; }

        /// <summary>
        /// Id da ordem de servico
        /// </summary>
        [DataMember(Order = 4)]
        public double PesoLiquido { get; set; }
        
        /// <summary>
        /// Data de saída do caminhão
        /// </summary>
        [DataMember(Name = "NotasCarregadas", Order = 5)]
        public IList<NotaTicketDto> NotasCarregadas { get; set; }

        #endregion
    }
}
