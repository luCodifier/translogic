﻿namespace Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Dados do ticket da balança
    /// </summary>
    [DataContract(Name = "Ticket", Namespace = "http://all-logistica.net/v01/GestaoDocumentos")]
    public class TicketDto
    {
        #region PROPRIEDADES

        /// <summary>
        /// Cnpj do Destino
        /// </summary>
        [DataMember(Order = 1)]
        public long CnpjDestino { get; set; }

        /// <summary>
        /// Código do terminal de descarga
        /// </summary>
        [DataMember(Order = 2)]
        public long CnpjOrigem { get; set; }

        /// <summary>
        /// Dados do caminhao
        /// </summary>
        [DataMember(Name = "Vagao", Order = 3)]
        public VagaoTicketDto Vagao { get; set; }

        /// <summary>
        /// Data de saída do caminhão
        /// </summary>
        [DataMember(Order = 4)]
        public virtual DateTime DataPesagem { get; set; }

        /// <summary>
        /// Vistor da balança
        /// </summary>
        [DataMember(Order = 5)]
        public virtual string Responsavel { get; set; }

        /// <summary>
        /// Vistor da balança
        /// </summary>
        [DataMember(Order = 6)]
        public virtual string Observacao { get; set; }
        #endregion
    }
}
