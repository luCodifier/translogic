﻿namespace Translogic.Modules.Core.Interfaces.GestaoDocumentos.Balanca
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;

    /// <summary>
    /// Nota carregado no caminhão
    /// </summary>
    [DataContract(Name = "Nota")]
    public class NotaTicketDto
    {
        /// <summary>
        /// chave da NFe
        /// </summary>
        [DataMember(Order = 1)]
        public string ChaveNfe { get; set; }

        /// <summary>
        /// Peso da NFe
        /// </summary>
        [DataMember(Order = 2)]
        public double Peso { get; set; }

        /// <summary>
        /// Serie da NFe
        /// </summary>
        public string Serie { get; set; }

        /// <summary>
        /// Numero da NFe
        /// </summary>
        public int Numero { get; set; }

        /// <summary>
        /// Descricao do produto da NFe
        /// </summary>
        public string Produto { get; set; }

        /// <summary>
        /// Peso disponível = Peso Total = Peso Utilizado
        /// </summary>
        public double PesoDisponivel { get; set; }
    }
}
