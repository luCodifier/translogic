﻿namespace Translogic.JobRunner.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.MicroKernel.Registration;
    using NUnit.Framework;
    using Translogic.Core;
    using Translogic.JobRunner;
    using Translogic.Modules.Core.Domain.Services.Edi;
    using Translogic.Modules.Core.Domain.Services.FluxosComerciais;
    using Translogic.Tests;

    public class ObterEmailsRealizarProcessamentoServiceTestCase
    {
        [Test]
        public void ExecutarGeracaoSingleThreadTestCase()
        {
            TranslogicStarter.Initialize();
            TranslogicStarter.SetupForTests();
            TranslogicContainer container = TranslogicStarter.Container;
            container.Register(Component.For<EdiReadNfeService>());
            container.Register(Component.For<NfeService>());

            // Inicia gerente de sender
            EdiReadNfeService srv = container.Resolve<EdiReadNfeService>();
            srv.ObterEmailsRealizarProcessamento(99, "DESC");
        }
    }
}
